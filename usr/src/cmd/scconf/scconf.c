/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scconf.c	1.108	08/05/20 SMI"

/*
 * scconf(1M)
 */
#include <scadmin/scconf.h>
#include <libdcs.h>
#include <devid.h>
#include <sys/didio.h>
#include <libdid.h>
#include <rgm/sczones.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <netdb.h>
#include <libintl.h>
#include <locale.h>

#include <sys/sol_version.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/fss.h>

#ifndef FSS_MAXSHARES
#define	BUG_6541238
#define	FSS_MAXSHARES   65535 /* XXX from sys/fss.h */
#endif

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>
#include <clcomm_cluster_vm.h>

/*
 * _KERNEL_ORB is defined when compiled in UNODE environment
 */
#ifdef _KERNEL_ORB
#include <orbtest/trans_tests/unode_trtests.h>
#define	exit(x) return (x)
#endif

#define	LABELW			50
#define	COLW20			20

#define	SUBOPT_CLUSTER		"cluster"
#define	SUBOPT_NODE		"node"
#define	SUBOPT_CHANGETO		"changeto"
#define	SUBOPT_TRTYPE		"trtype"
#define	SUBOPT_TYPE		"type"
#define	SUBOPT_ADAPTERS		"adapterlist"
#define	SUBOPT_NAME		"name"
#define	SUBOPT_TRANSPORT_TYPE	"transport_type"
#define	SUBOPT_STATE		"state"
#define	SUBOPT_ENDPOINT		"endpoint"
#define	SUBOPT_NOENABLE		"noenable"
#define	SUBOPT_NETADDR		"netaddr"
#define	SUBOPT_NETMASK		"netmask"
#define	SUBOPT_PRIVATEHOSTNAME	"privatehostname"
#define	SUBOPT_ZPRIVATEHOSTNAME	"zprivatehostname"
/* SC SLM addon start */
#define	SUBOPT_SCSLM_GLOBAL_ZONE_SHARES	"globalzoneshares"
#define	SUBOPT_SCSLM_DEFAULT_PSET_MIN	"defaultpsetmin"
/* SC SLM addon end */
#define	SUBOPT_GLOBALDEV	"globaldev"
#define	SUBOPT_MAINTSTATE	"maintstate"
#define	SUBOPT_RESET		"reset"
#define	SUBOPT_DEFAULTVOTE	"defaultvote"
#define	SUBOPT_AUTOCONFIG	"autoconfig"
#define	SUBOPT_NOOP		"noop"
#define	SUBOPT_INSTALLMODE	"installmode"
#define	SUBOPT_INSTALLMODEOFF	"installmodeoff"
#define	SUBOPT_IFNOTINSTALLMODE	"ifnotinstallmode"
#define	SUBOPT_NODELIST		"nodelist"
#define	SUBOPT_PREFERENCED	"preferenced"
#define	SUBOPT_FAILBACK		"failback"
#define	SUBOPT_NUMSECONDARIES	"numsecondaries"
#define	SUBOPT_AUTHTYPE		"authtype"
#define	SUBOPT_HB_TIMEOUT	"heartbeat_timeout"
#define	SUBOPT_HB_QUANTUM	"heartbeat_quantum"
#define	SUBOPT_UDP_TIMEOUT	"udp_session_timeout"
#define	SUBOPT_VLANID		"vlanid"
#define	SUBOPT_VLANID_BAD	"vlan_id"
#define	SUBOPT_MONITORING	"monitoring"
#define	SUBOPT_ALL		"all"

#define	STR_TRUE		"true"
#define	STR_FALSE		"false"
#define	STR_ENABLED		"enabled"
#define	STR_DISABLED		"disabled"
#define	STR_CLUSTER		"cluster"
#define	STR_FARM		"farm"

static char *progname;
static boolean_t iseuropa = B_FALSE;

/*
 * exitstatus is used in main to get the return code of scconf library
 * routines. It is declared global so it will be in scope when used in
 * exit processing
 */

static int exitstatus;

static void usage(FILE *out);

static scconf_errno_t suboption_usage(char *subopt, char *value,
    char *alreadyset);
static void suboption_unknown(char *suboption);

static void print_help_add(FILE *out);
static void print_help_change(FILE *out);
static void print_help_remove(FILE *out);
static void print_help_print(FILE *out);

/*lint -e793 */

static scconf_errno_t
    scconfcmd_add(int argc, char **argv, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_change(int argc, char **argv, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_remove(int argc, char **argv, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_print(int argc, char **argv, char **messages, uint_t uflag);

static scconf_errno_t
    scconfcmd_change_clustername(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_add_node(char *subopts, char **messages, uint_t uflag,
    scconf_cltr_handle_t handle);
scconf_errno_t
    scconfcmd_change_nodename(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_change_farm_monstate(char *subopts, char **messages,
    uint_t uflag);
static scconf_errno_t
    scconfcmd_rm_node(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_change_heartbeat(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_change_udp_timeout(char *subopts, char **messages, uint_t uflag);

static scconf_errno_t
    scconfcmd_add_cltr_adapter(char *subopts, char **messages, uint_t uflag,
    scconf_cltr_handle_t handle);
static scconf_errno_t
    scconfcmd_change_cltr_adapter(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_rm_cltr_adapter(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_add_cltr_cpoint(char *subopts, char **messages, uint_t uflag,
    scconf_cltr_handle_t handle);
static scconf_errno_t
    scconfcmd_change_cltr_cpoint(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_rm_cltr_cpoint(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_add_cltr_cable(char *subopts, char **messages, uint_t uflag,
    scconf_cltr_handle_t handle);
static scconf_errno_t
    scconfcmd_change_cltr_cable(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_rm_cltr_cable(char *subopts, char **messages, uint_t uflag);
scconf_errno_t
    scconfcmd_set_netaddr(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_set_privatehostname(char *subopts, char **messages,
    uint_t nochange, uint_t uflag);
static scconf_errno_t
    scconfcmd_clear_privatehostname(char *subopts, uint_t uflag);
/* SC SLM addon start */
static scconf_errno_t
    scconfcmd_set_scslm(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t scconfcmd_scslm_check_values(char **);
/* SC SLM addon end */
static scconf_errno_t
    scconfcmd_add_quorum_device(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_change_quorum_config(char *subopts, char **messages,
    uint_t uflag);
static scconf_errno_t
    scconfcmd_rm_quorum_device(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_add_ds(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_change_ds(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
    scconfcmd_rm_ds(char *subopts, char **messages, uint_t uflag);
static scconf_errno_t
scconfcmd_set_authentication(char *subopts, uint_t uflag, uint_t typeonly);
static scconf_errno_t
    scconfcmd_rm_authnodes(char *subopts, uint_t uflag);

static scconf_errno_t get_nodenames(char *nodelist, char ***dsnodes);
static int initialize_endpoint(scconf_cltr_epoint_t *epoint, char *endpoint);

static scconf_errno_t print_all(int verbose);
static void print_column(int width, char *string);
static void print_column_value(int width, char *value);
static void print_line(int margin, int labelw, char *prefix,
    char *label, char *value);
static char *dashes(char *buffer, char *string);
static void print_clusternodes(scconf_cfg_cluster_t *clconfig, int margin,
    int verbose);
static scconf_errno_t print_farmnodes(int margin, int verbose);
static void print_quorumdevices(scconf_cfg_cluster_t *clconfig, int margin,
    int verbose);
static void print_node_zones(scconf_cfg_node_t *cluster_node, int margin,
    int verbose);
static void print_cltr_adapters(scconf_cfg_node_t *cluster_node, int margin,
    int verbose);
static void print_adapter_ports(scconf_cfg_node_t *cluster_node,
    scconf_cfg_cltr_adap_t *cltr_adapter, int margin, int verbose);
static void print_cltr_cpoints(scconf_cfg_cluster_t *clconfig, int margin,
    int verbose);
static void print_cpoint_ports(scconf_cfg_cpoint_t *cltr_cpoint,
    int margin, int verbose);
static void print_cltr_cables(scconf_cfg_cluster_t *clconfig, int margin,
    int verbose);
static void get_cable_epoint(scconf_cltr_epoint_t *epoint, char *buffer);
static void print_devicegroups(scconf_cfg_ds_t *dsconfig, int margin,
    int verbose);
static int hide_dg(char *dgname, scconf_cfg_ds_t *dgconfig);
static boolean_t tunable_hb_is_available(void);

/*
 * main
 */
int
#ifndef _KERNEL_ORB
main(int argc, char **argv)
#else
scconf(int argc, char **argv)
#endif
{
	int c;
	int i, aflg, cflg, rflg, hflg, pflg, Hflg, verbose, otheroptions;
	scconf_errno_t (*scconfcmd)(int, char **, char **messages,
	    uint_t uflag);
	scconf_errno_t scconferr;
	scconf_errno_t scconferr1;
	scconf_errno_t scconferr2;
	char errbuff[BUFSIZ];
	char *msgbuffer = (char *)0;
	char *msg, *last;
	FILE *out;
	uint_t ismember;

	/* Initialize and demote to normal uid */
	cl_auth_init();
	cl_auth_demote();

	scconfcmd = NULL;
	/* Set the program name */
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	/* Do not allow this command in a non-global zone */
	if (sc_zonescheck()) {
		exit(SCCONF_ENGZONE);
	}

	/* check for basic options and for help flag */
	aflg = cflg = rflg = hflg = pflg = Hflg = verbose = otheroptions = 0;
	while ((c =
	    getopt(argc, argv, "acrpHvC:w:h:A:B:m:P:q:D:T:S:u:")) != EOF) {
		switch (c) {
		case 'a':
			aflg = 1;
			scconfcmd = scconfcmd_add;
			break;

		case 'c':
			cflg = 1;
			scconfcmd = scconfcmd_change;
			break;

		case 'r':
			rflg = 1;
			scconfcmd = scconfcmd_remove;
			break;

		case 'p':
			pflg = 1;
			scconfcmd = scconfcmd_print;
			break;

		case 'h':
			++hflg;
			++otheroptions;
			break;

		case 'H':
			++Hflg;
			break;

		case 'v':
			++verbose;
			break;

		case '?':
			usage(stderr);
			exit(SCCONF_EUSAGE);

		default:
			++otheroptions;
			break;
		}
	}

	/* If -r -h, force verbose mode */
	if (rflg && hflg)
		++verbose;

	/* Make sure there are no additional args */
	if (optind != argc) {
		usage(stderr);
		exit(SCCONF_EUSAGE);
	}

	/* reset optind */
	optind = 1;

	/* sum of basic options */
	i = aflg + cflg + rflg + pflg;

	/* only one form of the command allowed */
	if (i > 1) {
		usage(stderr);
		exit(SCCONF_EUSAGE);
	}

	/* if not print and no "other" options, print help */
	if (!pflg && !otheroptions) {
		++Hflg;

		/* if there are no basic flags, print help for everything */
		if (i == 0)
			aflg = cflg = rflg = pflg = Hflg = 1;

	/* if there are "others", but no basic options, usage error */
	} else if (i == 0) {
		usage(stderr);
		exit(SCCONF_EUSAGE);
	}

	/* promote to euid=0 */
	cl_auth_promote();

	/*
	 * While printing help messages we have to know whether Farm Management
	 * is enabled or not. Hence checking whether Farm Management is enabled
	 * or not here itself. But error handling needs to be done only in cases
	 * when help messages are not required. Hence, delaying the error
	 * handling part to a later point.
	 */
	ismember = 0;
	/* make sure that we are active in the cluster */
	scconferr1 = scconf_ismember(0, &ismember);

	/* Check if this an Europa cluster */
	if (!scconferr1 && ismember) {
		scconferr2 = scconf_iseuropa(&iseuropa);
	}

	/* if Hflg is set, just print help, and exit */
	if (Hflg) {

		/* if help everything, include usage message */
		if (aflg && cflg && rflg && pflg) {
			(void) putc('\n', stdout);
			usage(stdout);
		}

		/* print help */
		if (aflg)
			print_help_add(stdout);
		if (cflg)
			print_help_change(stdout);
		if (rflg)
			print_help_remove(stdout);
		if (pflg)
			print_help_print(stdout);

		exit(SCCONF_NOERR);
	}

	/* if not help and no basic options, usage error */
	if (i == 0) {
		usage(stderr);
		exit(SCCONF_EUSAGE);
	}

	/* Handle is_member error */
	if (scconferr1 != SCCONF_NOERR || !ismember) {
		switch (scconferr1) {
		case SCCONF_EPERM:
			scconf_strerr(errbuff, scconferr);
			(void) fprintf(stderr, "%s:  %s.\n",
			    progname, errbuff);
			exit(scconferr1);

		default:
			(void) fprintf(stderr, gettext(
			    "%s:  This node is not currently "
			    "in the cluster.\n"),
			    progname);

			exit(SCCONF_ENOCLUSTER);
		} /*lint !e788 */
	}

	/* Handle is_europa error. */
	if (scconferr2 != SCCONF_NOERR) {
		exit(scconferr2);
	}

	/* If Europa, dlopen libscxcfg, libfmm */
	if (iseuropa) {
		exitstatus = scconf_libscxcfg_open();
		if (exitstatus != SCCONF_NOERR) {
			fprintf(stderr, "Failed to open LIBSCXCFG\n");
			exit(exitstatus);
		}

		exitstatus = scconf_libfmm_open();
		if (exitstatus != SCCONF_NOERR) {
			fprintf(stderr, "Failed to open LIBFMM\n");
			exit(exitstatus);
		}
	}

	/* complete check for usage errors, then run the options */
	if ((exitstatus = scconfcmd(argc, argv, (char **)0, 1)) != 0)
		exit(exitstatus);

	/* Initialize cl_cmd_event library */
	if (aflg || cflg || rflg) {
		cl_cmd_event_init(argc, argv, &exitstatus);
	}

	/* run the command */
	exitstatus = scconfcmd(argc, argv, &msgbuffer, 0);

	/* dump the messages */
	if (msgbuffer != (char *)0) {
		/* If non-zero exitstatus, use stderr, else use stdout */
		out = (exitstatus) ? stderr : stdout;

		/* Pre-pend program name to each line */
		msg = strtok_r(msgbuffer, "\n", &last);
		while (msg != NULL) {
			(void) fprintf(out, "%s:  %s\n", progname, msg);
			msg = strtok_r(NULL, "\n", &last);
		}
	}
	if (msgbuffer)
		free(msgbuffer);

	/* If Europa, dlclose libfmm, libscxcfg */
	if (iseuropa) {
		(void) scconf_libfmm_close();
		(void) scconf_libscxcfg_close();
	}

	/* done */
	return (exitstatus);
}

/*
 * usage
 *
 *	Print a simple usage message to stderr.
 */
static void
usage(FILE *out)
{
	(void) fprintf(out, "%s:  %s -a|-c|-r|-p [-Hv[v]] [options]\n",
	    gettext("usage"),
	    progname);
	(void) fprintf(out, "\t%s [-H]\n",
	    progname);

	(void) putc('\n', out);
}

/*
 * suboption_usage
 *
 *	If "value" is NULL, print a suboption usage error indicating
 *	that the value is not set.  If "alreadyset" is NOT set to NULL
 *	and is not set to -1, print a suboption usage error indicating
 *	that the suboption has already been used.
 *
 *	Callers can set either "value" or "alreadyset" to -1 to bypass
 *	any checking.
 *
 *	This function returns either SCCONF_EUSAGE or SCCONF_NOERR.
 */
static scconf_errno_t
suboption_usage(char *subopt, char *value, char *alreadyset)
{
	scconf_errno_t scconferr = SCCONF_NOERR;

	/* If subopt is not set, just return */
	if (subopt == NULL)
		return (SCCONF_NOERR);

	/* If the option value is NULL, print a message */
	if (value == NULL || *value == '\0') {
		(void) fprintf(stderr, gettext(
		    "%s:  Suboption requires a value - "
		    "\"%s\".\n"), progname, subopt);
		scconferr = SCCONF_EUSAGE;
	}

	/* If alreadyset is not NULL and not -1, print message */
	if (alreadyset != (char *)-1 && alreadyset != NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  Suboption used more than once - \"%s\".\n"),
		    progname, subopt);
		scconferr = SCCONF_EUSAGE;
	}

	return (scconferr);
}

/*
 * suboption_unknown
 *
 * Print suboption unknown message.
 */
static void
suboption_unknown(char *suboption)
{
	char *s1, *s2;

	if (suboption == NULL || *suboption == '\0')
		return;

	/* Dup, so we don't modify the original */
	if ((s1 = strdup(suboption)) == NULL)
		return;

	/* Get rid of the value portion of the suboption */
	if ((s2 = strchr(s1, '=')) != NULL)
		*s2 = '\0';

	/* Print the message */
	(void) fprintf(stderr, gettext(
	    "%s:  Illegal or unrecognized suboption - \"%s\".\n"),
	    progname, s1);

	free(s1);
}

/*
 * print_help_add
 *
 *	Print a help message to "out" for the "add" form of the command.
 */
static void
print_help_add(FILE *out)
{
	(void) fprintf(out, "%s (%s -a):\n",
	    gettext("Add options"),
	    progname);

	/*
	 * If Farm Management is enabled, print europa specific
	 * help message, else print default error message.
	 */
	if (iseuropa) {
		(void) fprintf(out, "\t-h node=nodename"
		    "[,type=type,adapterlist=adapter1[:adapter2]]\n");
	} else {
		(void) fprintf(out, "\t-h node=nodename\n");
	}
	(void) fprintf(out, "\t-A trtype=type,node=node,name=name"
	    "[,vlanid=vlanid][,otheroptions]\n");
	(void) fprintf(out, "\t-B type=type,name=name[,otheroptions]\n");
	(void) fprintf(out, "\t-m endpoint=[node:]name[@port]"
	    ",endpoint=[node:]name[@port][,noenable]\n");
	(void) fprintf(out, "\t-P node=node[,privatehostname=hostalias]\n");
#if SOL_VERSION >= __s10
	(void) fprintf(out,
			"\t-P node=node:zone,zprivatehostname=hostalias\n");
#endif
	(void) fprintf(out, "\t-q [globaldev=devicename"
	    "[,node=node,node=node[,...]]] | autoconfig[,noop]]\n");
	(void) fprintf(out, "\t-q name=devicename,type=devicetype"
	    "[,node=node,node=node[,...]][,propertyoptions]\n");
	(void) fprintf(out, "\t-D type=type,name=name[,nodelist=node[:node]...]"
	    "[,preferenced={true | false}][,failback={enabled | disabled}]"
	    "[,numsecondaries={<n> | <>}],otheroptions\n");
	(void) fprintf(out, "\t-T node=nodename[,...]"
	    "[,authtype=authtype]\n");
	(void) fprintf(out, "\t-H\n");
	(void) fprintf(out, "\t-v\n");

	(void) putc('\n', out);
}

/*
 * print_help_change
 *
 *	Print a help message to "out" for the "change" form of the command.
 */
static void
print_help_change(FILE *out)
{
	(void) fprintf(out, "%s (%s -c):\n",
	    gettext("Change options"),
	    progname);

	/*
	 * If Farm Management is enabled, print europa specific
	 * help message.
	 */
	if (iseuropa) {
		(void) fprintf(out, "\t-h [nodelist=node[:node]...,]"
		"monitoring={enabled|disabled}\n");
	}
	(void) fprintf(out, "\t-C cluster=clustername\n");
	(void) fprintf(out, "\t-A node=node,name=name[,state=state]"
	    "[,otheroptions]\n");
	(void) fprintf(out, "\t-B name=name[,state=state][,otheroptions]\n");
	(void) fprintf(out, "\t-w heartbeat_timeout=heartbeat_timeout_value\n");
	(void) fprintf(out, "\t-w heartbeat_quantum=heartbeat_quantum_value\n");
	(void) fprintf(out, "\t-u "
	    "udp_session_timeout=udp_session_timeout_value\n");
	(void) fprintf(out, "\t-m endpoint=[node:]name[@port],state=state\n");
	(void) fprintf(out, "\t-P node=node[,privatehostname=hostalias]\n");
#if SOL_VERSION >= __s10
	(void) fprintf(out,
			"\t-P node=node:zone,zprivatehostname=hostalias\n");
#endif
/* SC SLM addon start */
	(void) fprintf(out, "\t-S node=node[,globalzoneshares=shares]");
	(void) fprintf(out, "[,defaultpsetmin=cpu]\n");
/* SC SLM addon end */
	(void) fprintf(out, "\t-q globaldev=devicename,maintstate\n");
	(void) fprintf(out, "\t-q name=devicename,maintstate\n");
	(void) fprintf(out, "\t-q globaldev=devicename,reset\n");
	(void) fprintf(out, "\t-q name=devicename,reset\n");
	(void) fprintf(out, "\t-q node=node,maintstate\n");
	(void) fprintf(out, "\t-q node=node,reset\n");
	(void) fprintf(out, "\t-q reset\n");
	(void) fprintf(out, "\t-q installmode\n");
	(void) fprintf(out, "\t-q installmodeoff\n");
	(void) fprintf(out, "\t-q name=devicename,propertyoptions\n");
	(void) fprintf(out, "\t-q autoconfig[,noop]\n");
	(void) fprintf(out, "\t-D name=name[,nodelist=node[:node]...]"
	    "[,preferenced={true | false}][,failback={enabled | disabled}]"
	    "[,numsecondaries={<n> | <>}],otheroptions\n");
	(void) fprintf(out, "\t-T authtype=authtype\n");
	(void) fprintf(out, "\t-H\n");
	(void) fprintf(out, "\t-v\n");

	(void) putc('\n', out);
}

/*
 * print_help_remove
 *
 *	Print a help message to "out" for the "remove" form of the command.
 */
static void
print_help_remove(FILE *out)
{
	(void) fprintf(out, "%s (%s -r):\n",
	    gettext("Remove options"),
	    progname);

	/*
	 * If Farm Management is enabled, print europa specific
	 * help message, else print default error message.
	 */
	if (iseuropa) {
		(void) fprintf(out, "\t-h node=nodename[,type=type]\n");
	} else {
		(void) fprintf(out, "\t-h node=nodename\n");
	}
	(void) fprintf(out, "\t-A name=name,node=node\n");
	(void) fprintf(out, "\t-B name=name\n");
	(void) fprintf(out, "\t-m endpoint=[node:]name[@port]\n");
#if SOL_VERSION >= __s10
	(void) fprintf(out, "\t-P node=node:zone\n");
#endif
	(void) fprintf(out, "\t-q globaldev=devicename\n");
	(void) fprintf(out, "\t-q name=devicename\n");
	(void) fprintf(out, "\t-D name=name\n");
	(void) fprintf(out, "\t-T {node=nodename | all}\n");
	(void) fprintf(out, "\t-H\n");
	(void) fprintf(out, "\t-v\n");

	(void) putc('\n', out);
}

/*
 * print_help_print
 *
 *	Print a help message to "out" for the "print" form of the command.
 */
static void
print_help_print(FILE *out)
{
	(void) fprintf(out, "%s (%s -p):\n",
	    gettext("Print options"),
	    progname);

	(void) fprintf(out, "\t-H\n");
	(void) fprintf(out, "\t-v\n");

	(void) putc('\n', out);
}

/*
 * scconfcmd_add
 *
 *	The "add" form of the command.
 *
 *	If "uflag" is set, just check for usage errors.
 */
static scconf_errno_t
scconfcmd_add(int argc, char **argv, char **messages, uint_t uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cltr_handle_t handle = (scconf_cltr_handle_t)0;
	int dohandle = 0;
	int nohandle = 0;
	int c;

	/* if adding transport only, get handle */
	optind = 1;
	while ((c = getopt(argc, argv, "acrpvh:A:B:m:P:q:D:T:")) != EOF) {
		switch (c) {
		case 'a':
			break;

		case 'c':
		case 'r':
		case 'p':
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;

		case 'h':
			cl_auth_check_command_opt_exit(*argv, "-a -h",
			    CL_AUTH_NODE_MODIFY, SCCONF_EPERM);
			++dohandle;
			break;

		case 'A':
			cl_auth_check_command_opt_exit(*argv, "-a -A",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);
			++dohandle;
			break;

		case 'B':
			cl_auth_check_command_opt_exit(*argv, "-a -B",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);
			++dohandle;
			break;

		case 'm':
			cl_auth_check_command_opt_exit(*argv, "-a -m",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);

			++dohandle;
			break;

		case 'P':
			cl_auth_check_command_opt_exit(*argv, "-a -P",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);
			break;

		case 'D':
			cl_auth_check_command_opt_exit(*argv, "-a -D",
			    CL_AUTH_DEVICE_MODIFY, SCCONF_EPERM);
			break;

		case 'T':
			cl_auth_check_command_opt_exit(*argv, "-a -T",
			    CL_AUTH_NODE_MODIFY, SCCONF_EPERM);
			break;
		case 'q':
			cl_auth_check_command_opt_exit(*argv, "-a -q",
			    CL_AUTH_QUORUM_MODIFY, SCCONF_EPERM);
			break;
		default:
			++nohandle;
			break;
		}
	}

	/*
	 * If we generate a handle here, loop until commit succeeds,
	 * or until a fatal error is encountered.
	 */
	for (;;) {
		/* If only adding transport stuff, batch update */
		if (!uflag && !nohandle && (dohandle > 1)) {

			/* Release old handle, if necessary */
			if (handle != (scconf_cltr_handle_t)0)
				scconf_cltr_releasehandle(handle);
			handle = (scconf_cltr_handle_t)0;

			/* Get handle */
			(void) scconf_cltr_openhandle(&handle);
		}

		optind = 1;
		while ((c = getopt(argc, argv, "acrpvh:A:B:m:P:q:D:T:")) !=
		    EOF) {
			switch (c) {
			case 'a':
				break;

			case 'c':
			case 'r':
			case 'p':
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;

			case 'h':
				if ((rstatus = scconfcmd_add_node(optarg,
				    messages, uflag, handle)) != 0)
					goto cleanup;
				break;

			case 'A':
				if ((rstatus = scconfcmd_add_cltr_adapter(
				    optarg, messages, uflag, handle)) != 0)
					goto cleanup;
				break;

			case 'B':
				if ((rstatus = scconfcmd_add_cltr_cpoint(
				    optarg, messages, uflag, handle)) != 0)
					goto cleanup;
				break;

			case 'm':
				if ((rstatus = scconfcmd_add_cltr_cable(
				    optarg, messages, uflag, handle)) != 0)
					goto cleanup;
				break;

			case 'P':
				if ((rstatus = scconfcmd_set_privatehostname(
				    optarg, messages, 1, uflag)) != 0)
					goto cleanup;
				break;

			case 'q':
				if ((rstatus = scconfcmd_add_quorum_device(
				    optarg, messages, uflag)) != 0)
					goto cleanup;
				break;

			case 'D':
				if ((rstatus = scconfcmd_add_ds(optarg,
				    messages, uflag)) != 0)
					goto cleanup;
				break;

			case 'T':
				if ((rstatus = scconfcmd_set_authentication(
				    optarg, uflag, 0)) != 0)
					goto cleanup;
				break;

			case 'v':
				break;

			default:
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
		}

		/* if handle, update */
		if (handle != (scconf_cltr_handle_t)0) {

			/* update */
			rstatus = scconf_cltr_updatehandle(handle, messages);

			/* if update returned okay, we are done */
			if (rstatus == SCCONF_NOERR) {
				break;

			/* else if outdated handle, continue to retry */
			} else if (rstatus != SCCONF_ESTALE) {
				goto cleanup;
			}

		/* else, if there is no handle, we are done */
		} else {
			break;
		}
	}

cleanup:
	/* Release the handle */
	if (handle != (scconf_cltr_handle_t)0)
		scconf_cltr_releasehandle(handle);

	/* Print usage, if usage error */
	if (rstatus == SCCONF_EUSAGE) {
		(void) putc('\n', stderr);
		usage(stderr);
		print_help_add(stderr);
	}

	return (rstatus);
}

/*
 * scconfcmd_change
 *
 *	The "change" form of the command.
 *
 *	If "uflag" is set, just check for usage errors.
 */
static scconf_errno_t
scconfcmd_change(int argc, char **argv, char **messages, uint_t uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	int c;

	optind = 1;
	while ((c = getopt(argc, argv, "acrpvh:C:w:A:B:m:P:q:D:T:S:u:"))
	    != EOF) {
		switch (c) {
		case 'c':
			break;

		case 'a':
		case 'r':
		case 'p':
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;

		case 'h':
			if (!iseuropa) {
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			cl_auth_check_command_opt_exit(*argv, "-c -h",
			    CL_AUTH_NODE_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_change_farm_monstate(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'C':
			cl_auth_check_command_opt_exit(*argv, "-c -C",
			    CL_AUTH_SYSTEM_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_change_clustername(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'w':
			cl_auth_check_command_opt_exit(*argv, "-c -G",
			    CL_AUTH_SYSTEM_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_change_heartbeat(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'A':
			cl_auth_check_command_opt_exit(*argv, "-c -A",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_change_cltr_adapter(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'B':
			cl_auth_check_command_opt_exit(*argv, "-c -B",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_change_cltr_cpoint(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'm':
			cl_auth_check_command_opt_exit(*argv, "-c -m",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_change_cltr_cable(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'P':
			cl_auth_check_command_opt_exit(*argv, "-c -P",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_set_privatehostname(optarg,
			    messages, 0, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

/* SC SLM addon start */
		case 'S':
			cl_auth_check_command_opt_exit(*argv, "-c -S",
			    CL_AUTH_NODE_MODIFY, SCCONF_EPERM);

			/* check all SC SLM numerical args on command line */
			if ((rstatus = scconfcmd_scslm_check_values(argv))
			    != SCCONF_NOERR) {
				goto cleanup;
			}

			if ((rstatus = scconfcmd_set_scslm(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;
/* SC SLM addon end */

		case 'q':
			cl_auth_check_command_opt_exit(*argv, "-c -q",
			    CL_AUTH_QUORUM_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_change_quorum_config(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'D':
			cl_auth_check_command_opt_exit(*argv, "-c -D",
			    CL_AUTH_DEVICE_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_change_ds(optarg, messages,
			    uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'T':
			cl_auth_check_command_opt_exit(*argv, "-c -T",
			    CL_AUTH_NODE_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_set_authentication(optarg,
			    uflag, 1)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'u':
			cl_auth_check_command_opt_exit(*argv, "-c -u",
			    CL_AUTH_NODE_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_change_udp_timeout(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'v':
			break;

		default:
			rstatus = SCCONF_EUSAGE;
			goto cleanup;
		}
	}

cleanup:
	/* Print usage, if usage error */
	if (rstatus == SCCONF_EUSAGE) {
		(void) putc('\n', stderr);
		usage(stderr);
		print_help_change(stderr);
	}

	return (rstatus);
}

/*
 * scconfcmd_remove
 *
 *	The "remove" form of the command.
 *
 *	If "uflag" is set, just check for usage errors.
 */
static scconf_errno_t
scconfcmd_remove(int argc, char **argv, char **messages, uint_t uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	int c;

	optind = 1;
#if SOL_VERSION >= __s10
	while ((c = getopt(argc, argv, "acrpvh:A:B:m:P:q:D:T:")) != EOF) {
#else
	while ((c = getopt(argc, argv, "acrpvh:A:B:m:q:D:T:")) != EOF) {
#endif
		switch (c) {
		case 'r':
			break;

		case 'a':
		case 'c':
		case 'p':
			rstatus = SCCONF_EUSAGE;
			goto cleanup;

		case 'h':
			cl_auth_check_command_opt_exit(*argv, "-r -h",
			    CL_AUTH_NODE_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_rm_node(optarg, messages,
			    uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'A':
			cl_auth_check_command_opt_exit(*argv, "-r -A",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_rm_cltr_adapter(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'B':
			cl_auth_check_command_opt_exit(*argv, "-r -B",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_rm_cltr_cpoint(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'm':
			cl_auth_check_command_opt_exit(*argv, "-r -m",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_rm_cltr_cable(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

#if SOL_VERSION >= __s10
		case 'P':
			cl_auth_check_command_opt_exit(*argv, "-r -P",
			    CL_AUTH_TRANSPORT_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_clear_privatehostname(optarg,
			    uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;
#endif

		case 'q':
			cl_auth_check_command_opt_exit(*argv, "-r -q",
			    CL_AUTH_QUORUM_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_rm_quorum_device(optarg,
			    messages, uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'D':
			cl_auth_check_command_opt_exit(*argv, "-r -D",
			    CL_AUTH_DEVICE_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_rm_ds(optarg, messages,
			    uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'T':
			cl_auth_check_command_opt_exit(*argv, "-r -T",
			    CL_AUTH_NODE_MODIFY, SCCONF_EPERM);

			if ((rstatus = scconfcmd_rm_authnodes(optarg,
			    uflag)) != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'v':
			break;

		default:
			rstatus = SCCONF_EUSAGE;
			goto cleanup;
		}
	}

cleanup:
	/* Print usage, if usage error */
	if (rstatus == SCCONF_EUSAGE) {
		(void) putc('\n', stderr);
		usage(stderr);
		print_help_remove(stderr);
	}

	return (rstatus);
}

/*
 * scconfcmd_print
 *
 *	The "print" form of the command.
 *
 *	If "uflag" is set, just check for usage errors.
 */
/*ARGSUSED2*/
static scconf_errno_t
scconfcmd_print(int argc, char **argv, char **messages, uint_t uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	int c;
	int verbose = 0;

	cl_auth_check_command_opt_exit(*argv, "-p",
	    CL_AUTH_DEVICE_READ, SCCONF_EPERM);
	cl_auth_check_command_opt_exit(*argv, "-p",
	    CL_AUTH_TRANSPORT_READ, SCCONF_EPERM);
	cl_auth_check_command_opt_exit(*argv, "-p",
	    CL_AUTH_NODE_READ, SCCONF_EPERM);
	cl_auth_check_command_opt_exit(*argv, "-p",
	    CL_AUTH_QUORUM_READ, SCCONF_EPERM);
	cl_auth_check_command_opt_exit(*argv, "-p",
	    CL_AUTH_RESOURCE_READ, SCCONF_EPERM);
	cl_auth_check_command_opt_exit(*argv, "-p",
	    CL_AUTH_SYSTEM_READ, SCCONF_EPERM);

	optind = 1;
	while ((c = getopt(argc, argv, "acrpv")) != EOF) {
		switch (c) {
		case 'p':
			break;

		case 'a':
		case 'c':
		case 'r':
			usage(stderr);
			return (SCCONF_EUSAGE);

		case 'v':
			++verbose;
			break;

		default:
			usage(stderr);
			return (SCCONF_EUSAGE);
		}
	}

	/* just check usage? */
	if (uflag)
		return (rstatus);

	/* Print the configuration */
	rstatus = print_all(verbose);

	return (rstatus);
}

/*
 * print_all
 *
 *	Print the cluster configuration.
 */
static scconf_errno_t
print_all(int verbose)
{
	scconf_errno_t scconferr;
	char errbuff[BUFSIZ];
	scconf_cfg_cluster_t *clconfig = NULL;
	scconf_cfg_ds_t *dsconfig = NULL;
	scconf_namelist_t *namelist;
	char *ptr;
	char tmp_ptr[BUFSIZ];
	int i;
	int margin = 0;

	/* Get a copy of the configuration */
	scconferr = scconf_get_clusterconfig(&clconfig);
	if (scconferr != SCCONF_NOERR) {
		scconf_strerr(errbuff, scconferr);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to get a copy of the configuration - %s.\n"),
		    progname, errbuff);
		return (scconferr);
	}

	/* Cluster name */
	if (clconfig->scconf_cluster_clustername != NULL)
		ptr = clconfig->scconf_cluster_clustername;
	else
		ptr = "<NULL>";
	print_line(margin, LABELW, NULL, gettext("Cluster name:"), ptr);

	/* Cluster ID */
	if (clconfig->scconf_cluster_clusterid != NULL)
		ptr = clconfig->scconf_cluster_clusterid;
	else
		ptr = "<NULL>";
	print_line(margin, LABELW, NULL, gettext("Cluster ID:"), ptr);

	/* Cluster installmode */
	if (clconfig->scconf_cluster_installmode != NULL)
		ptr = clconfig->scconf_cluster_installmode;
	else
		ptr = "disabled";
	print_line(margin, LABELW, NULL, gettext("Cluster install mode:"), ptr);

	/* Cluster private net addr */
	if (clconfig->scconf_cluster_privnetaddr != NULL)
		ptr = clconfig->scconf_cluster_privnetaddr;
	else
		ptr = "<NULL>";
	print_line(margin, LABELW, NULL, gettext("Cluster private net:"), ptr);

	/* Cluster private netmask */
	if (clconfig->scconf_cluster_privnetmask != NULL)
		ptr = clconfig->scconf_cluster_privnetmask;
	else
		ptr = "<NULL>";
	print_line(margin, LABELW, NULL, gettext("Cluster private netmask:"),
	    ptr);

	/* Maximum number of nodes supported with IP address range */
	if (clconfig->scconf_cluster_maxnodes != NULL)
		ptr = clconfig->scconf_cluster_maxnodes;
	else
		ptr = SCCONF_DEFAULT_MAXNODES_STRING;

	print_line(margin, LABELW, NULL,
		gettext("Cluster maximum nodes:"), ptr);

	/* Maximum number of private nets supported with IP address range */
	if (clconfig->scconf_cluster_maxprivnets != NULL)
		ptr = clconfig->scconf_cluster_maxprivnets;
	else
		ptr = SCCONF_DEFAULT_MAXPRIVNETS_STRING;

	print_line(margin, LABELW, NULL,
		gettext("Cluster maximum private networks:"), ptr);

	/* Cluster join authentication type */
	switch (clconfig->scconf_cluster_authtype) {
	case SCCONF_AUTH_UNIX:
		ptr = "unix";
		break;

	case SCCONF_AUTH_DES:
		ptr = "des";
		break;

	default:
		ptr = "<NULL>";
		break;
	}
	print_line(margin, LABELW, NULL,
	    gettext("Cluster new node authentication:"), ptr);

	/* Cluster join list */
	i = 0;
	print_line(margin, LABELW, NULL,
	    gettext("Cluster authorized-node list:"), NULL);
	if (clconfig->scconf_cluster_authlist) {
		namelist = clconfig->scconf_cluster_authlist;
		if (namelist->scconf_namelist_name &&
		    strcmp(namelist->scconf_namelist_name, ".") == 0 &&
		    namelist->scconf_namelist_next == NULL) {
			++i;
			(void) printf(gettext(
			    "<. - Exclude all nodes>"));
		} else {
			while (namelist) {
				if (namelist->scconf_namelist_name) {
					if (i)
						(void) putchar(' ');
					++i;
					(void) printf(
					    namelist->scconf_namelist_name);
				}
				namelist = namelist->scconf_namelist_next;
			}
		}
	}
	if (!i)
		(void) printf(gettext(
		    "<NULL - Allow any node>"));
	(void) putchar('\n');

	/* Cluster default transport heart beat timeout */
	if (clconfig->scconf_cluster_hb_timeout != NULL) {
		ptr = clconfig->scconf_cluster_hb_timeout;
	} else {
		ptr = "<NULL>";
	}
	print_line(margin, LABELW, NULL, gettext(
	    "Cluster transport heart beat timeout:"), ptr);

	/* Cluster default transport heart beat quantum */
	if (clconfig->scconf_cluster_hb_quantum != NULL) {
		ptr = clconfig->scconf_cluster_hb_quantum;
	} else {
		ptr = "<NULL>";
	}
	print_line(margin, LABELW, NULL, gettext(
	    "Cluster transport heart beat quantum:"), ptr);


	/* Cluster default UDP Session timeout */
	if (clconfig->scconf_cluster_udp_timeout != NULL) {
		ptr = clconfig->scconf_cluster_udp_timeout;
	} else {
		/* Print the default value */
		(void) sprintf(tmp_ptr, "%d", UDP_SESSION_TIMEOUT_DFLT);
		ptr = tmp_ptr;
	}

	print_line(margin, LABELW, NULL, gettext(
	    "Round Robin Load Balancing UDP session timeout:"), ptr);

	/* Cluster nodes */
	print_clusternodes(clconfig, margin, verbose);

	/* Farm nodes, only if Europa enabled */
	if (iseuropa) {
		scconferr = print_farmnodes(margin, verbose);
		if (scconferr != SCCONF_NOERR) {
			return (scconferr);
		}
	}

	/* Cluster off-node transport connection points */
	(void) putchar('\n');
	print_cltr_cpoints(clconfig, margin, verbose);

	/* Cluster transport cables */
	(void) putchar('\n');
	print_cltr_cables(clconfig, margin, verbose);

	/* Quorum devices */
	(void) putchar('\n');
	print_quorumdevices(clconfig, margin, verbose);

	/* Free the configuration */
	scconf_free_clusterconfig(clconfig);

	/* Get a copy of the device configuration */
	scconferr = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG, &dsconfig);
	if (scconferr != SCCONF_NOERR) {
		scconf_strerr(errbuff, scconferr);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to get a copy of the configuration - %s.\n"),
		    progname, errbuff);
		return (scconferr);
	}

	/* Device groups */
	print_devicegroups(dsconfig, margin, verbose);

	/* Free the device configuration */
	scconf_free_ds_config(dsconfig);
	return (SCCONF_NOERR);
}

/*
 * print_column
 *
 *	Print a column in a table using the given width.   If the
 *	"string" is NULL, print an empty column.
 */
static void
print_column(int width, char *string)
{
	int i;

	/* If string is NULL, just print spaces */
	if (string == NULL || *string == '\0') {
		for (i = 0;  i < width;	 ++i)
			(void) putchar(' ');

	/* If width is zero, just print the string */
	} else if (width < 1) {
		(void) fputs(string, stdout);

	/* Print left justified column of given width */
	} else {
		(void) printf("%-*s ", width - 1, string);
	}
}

/*
 * print_column_value
 *
 *	Print a column in a table using the given width.   If the
 *	"value" is NULL, print a "-" for the value.
 */
static void
print_column_value(int width, char *value)
{
	if (value == NULL || *value == '\0')
		value = "-";

	print_column(width, value);
}

/*
 * print_line
 *
 *	Print a line of configuration output.
 *
 *	If no prefix or prefix is NULL string, do not print prefix.
 *	If no value, do not print value or newline.
 */
static void
print_line(int margin, int labelw, char *prefix, char *label, char *value)
{
	int i;
	char labelbuff[BUFSIZ];

	/* check args */
	if (margin < 0 || labelw < margin || label == NULL || *label == '\0')
		return;

	/* Check for NULL strings */
	if (prefix && *prefix == '\0')
		prefix = NULL;
	if (value && *value == '\0')
		value = NULL;

	/* set up labelbuff, with or without prefix */
	if (prefix) {
		(void) sprintf(labelbuff, "%s %s", prefix, label);
	} else {
		(void) strcpy(labelbuff, label);
	}

	/* print margin */
	for (i = 0;  i < margin;  ++i)
		(void) putchar(' ');

	/* print label */
	(void) printf("%-*s ", labelw - margin, labelbuff);

	/* print value */
	if (value)
		(void) printf("%s\n", value);
}

/*
 * dashes
 *
 *	Fill the buffer with a bunch of dashes the same length as the string
 */
static char *
dashes(char *buffer, char *string)
{
	size_t i, len;
	char *ptr;

	if (string == NULL || *string == '\0')
		return (buffer);

	ptr = buffer;
	len = strlen(string);
	for (i = 0;  i < len;  ++i)
		*ptr++ = '-';

	*ptr = '\0';

	return (buffer);
}

/*
 * print_clusternodes
 *
 *	Print the cluster nodes configuration.
 */
static void
print_clusternodes(scconf_cfg_cluster_t *clconfig, int margin, int verbose)
{
	scconf_cfg_node_t *cluster_nodes;
	scconf_cfg_node_t *cluster_node;
	char prefix[BUFSIZ];
	char value[BUFSIZ];
	char *ptr;
	int i;

	/* check args */
	if (clconfig == NULL)
		return;

	/* set the nodelist */
	cluster_nodes = clconfig->scconf_cluster_nodelist;

	/* Cluster nodes */
	i = 0;
	print_line(margin, LABELW, NULL, gettext("Cluster nodes:"), NULL);
	if (cluster_nodes) {
		for (cluster_node = cluster_nodes;  cluster_node;
		    cluster_node = cluster_node->scconf_node_next) {
			if (cluster_node->scconf_node_nodename) {
				if (i)
					(void) putchar(' ');
				++i;
				(void) printf(
				    cluster_node->scconf_node_nodename);
			}
		}
	}
	if (!i)
		(void) printf("<NULL>");
	(void) putchar('\n');

	for (cluster_node = cluster_nodes;  cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {

		(void) putchar('\n');

		/* Cluster node name */
		if (cluster_node->scconf_node_nodename)
			ptr = cluster_node->scconf_node_nodename;
		else
			ptr = "<NULL>";
		print_line(margin, LABELW, NULL,
		    gettext("Cluster node name:"), ptr);

		/* verbose mode prefix (<name>) */
		*prefix = '\0';
		if (verbose) {
			if (cluster_node->scconf_node_nodename) {
				(void) sprintf(prefix, "(%s)",
				    cluster_node->scconf_node_nodename);
			}
		}

		/* Node ID */
		(void) sprintf(value, "%d", cluster_node->scconf_node_nodeid);
		print_line(margin + 2, LABELW, prefix,
		    gettext("Node ID:"), value);

		/* Node enabled */
		if (cluster_node->scconf_node_nodestate ==
		    SCCONF_STATE_ENABLED)
			ptr = gettext("yes");
		else
			ptr = gettext("no");
		print_line(margin + 2, LABELW, prefix,
		    gettext("Node enabled:"), ptr);

		/* Node private host name */
		if (cluster_node->scconf_node_privatehostname)
			ptr = cluster_node->scconf_node_privatehostname;
		else
			ptr = "<NULL>";
		print_line(margin + 2, LABELW, prefix,
		    gettext("Node private hostname:"), ptr);

		/* Node quorum vote count */
		(void) sprintf(value, "%d", cluster_node->scconf_node_qvotes);
		print_line(margin + 2, LABELW, prefix,
		    gettext("Node quorum vote count:"), value);

		/* Node default quorum vote count */
		if (cluster_node->scconf_node_qdefaultvotes != 1) {
			(void) sprintf(value, "%d",
			    cluster_node->scconf_node_qdefaultvotes);
			print_line(margin + 2, LABELW, prefix,
		    gettext("Node default quorum vote count:"), value);
		}

		/* Node reservation key */
		if (cluster_node->scconf_node_qreskey)
			ptr = cluster_node->scconf_node_qreskey;
		else
			ptr = "<NULL>";
		print_line(margin + 2, LABELW, prefix,
		    gettext("Node reservation key:"), ptr);

#if SOL_VERSION >= __s10
		/* Cluster node zones */
		print_node_zones(cluster_node, margin + 2, verbose);
#endif

		/* SC SLM addon end */

		(void) sprintf(value, "%u",
		    cluster_node->scslm_global_zone_shares);
		print_line(margin + 2, LABELW, prefix,
		    gettext("CPU shares for global zone:"),
		    value);

		(void) sprintf(value, "%u",
		    cluster_node->scslm_default_pset_min);
		print_line(margin + 2, LABELW, prefix,
		    gettext("Minimum CPU requested for global zone:"),
		    value);

		(void) putchar('\n');

		/* SC SLM addon end */

		/* Cluster transport adapters */
		print_cltr_adapters(cluster_node, margin + 2, verbose);
	}
}

/*
 * print_farmnodes
 *
 *	Print the farm nodes configuration.
 */
static scconf_errno_t
print_farmnodes(int margin, int verbose)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	scconf_farm_nodelist_t *farm_nodelist = (scconf_farm_nodelist_t *)0;
	scconf_farm_nodelist_t *farm_nodes;
	char prefix[BUFSIZ];
	char value[BUFSIZ];
	char *ptr;
	int i;

	/* Get the farm nodes list */
	scconferr = scconf_get_farmnodes(&farm_nodelist);
	if (scconferr != SCCONF_NOERR) {
		scconf_strerr(errbuff, scconferr);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to get a copy of the farm "
		    "configuration - %s.\n"), progname, errbuff);
		return (scconferr);
	}

	/* Farm nodes */
	i = 0;
	(void) putchar('\n');
	print_line(margin, LABELW, NULL, gettext("Farm nodes:"), NULL);
	if (farm_nodelist) {
		for (farm_nodes = farm_nodelist;  farm_nodes; farm_nodes =
		    farm_nodes->scconf_node_next) {
			if (farm_nodes->scconf_node_nodename) {
				if (i)
					(void) putchar(' ');
				++i;
				(void) printf(
				    farm_nodes->scconf_node_nodename);
			}
		}
	}
	if (!i)
		(void) printf("<NULL>");
	(void) putchar('\n');

	for (farm_nodes = farm_nodelist; farm_nodes;
	    farm_nodes = farm_nodes->scconf_node_next) {

		(void) putchar('\n');

		/* Farm node name */
		if (farm_nodes->scconf_node_nodename)
			ptr = farm_nodes->scconf_node_nodename;
		else
			ptr = "<NULL>";
		print_line(margin, LABELW, NULL,
		    gettext("Farm node name:"), ptr);

		/* verbose mode prefix (<name>) */
		*prefix = '\0';
		if (verbose) {
			if (farm_nodes->scconf_node_nodename) {
				(void) sprintf(prefix, "(%s)",
				    farm_nodes->scconf_node_nodename);
			}
		}

		/* Node ID */
		(void) sprintf(value, "%d", farm_nodes->scconf_node_nodeid);
		print_line(margin + 2, LABELW, prefix,
		    gettext("Node ID:"), value);

		/* Node enabled */
		if (farm_nodes->scconf_node_monstate ==
		    SCCONF_STATE_ENABLED) {
			ptr = gettext("enabled");
		} else {
			ptr = gettext("disabled");
		}
		print_line(margin + 2, LABELW, prefix,
		    gettext("Node monitoring:"), ptr);

		/* Node transport adapter */
		if (farm_nodes->scconf_node_adapters)
			ptr = farm_nodes->scconf_node_adapters;
		else
			ptr = "<NULL>";
		print_line(margin + 2, LABELW, prefix,
		    gettext("Node transport adapter:"), ptr);
	}

	scconf_free_farm_nodelist(farm_nodelist);

	return (scconferr);
}

/*
 * print_quorumdevices
 *
 *	Print the shared quorum devices configuration.
 */
static void
print_quorumdevices(scconf_cfg_cluster_t *clconfig, int margin, int verbose)
{
	scconf_cfg_qdev_t *cluster_qdevs;
	scconf_cfg_qdev_t *cluster_qdev;
	scconf_cfg_qdevport_t *portlist;
	scconf_cfg_prop_t *prop;
	char prefix[BUFSIZ];
	char value[BUFSIZ];
	char *ptr;
	int i;

	/* check args */
	if (clconfig == NULL)
		return;

	/* set the qdevslist */
	cluster_qdevs = clconfig->scconf_cluster_qdevlist;

	/* Cluster quorum devices */
	i = 0;
	print_line(margin, LABELW, NULL, gettext("Quorum devices:"),
	    NULL);
	if (cluster_qdevs) {
		for (cluster_qdev = cluster_qdevs;  cluster_qdev;
		    cluster_qdev = cluster_qdev->scconf_qdev_next) {
			if (cluster_qdev->scconf_qdev_name) {
				if (i)
					(void) putchar(' ');
				++i;
				(void) printf(
				    cluster_qdev->scconf_qdev_name);
			}
		}
	}
	if (!i)
		(void) printf("<NULL>");
	(void) putchar('\n');

	for (cluster_qdev = cluster_qdevs;  cluster_qdev;
	    cluster_qdev = cluster_qdev->scconf_qdev_next) {

		(void) putchar('\n');

		/* Shared quorum device name */
		if (cluster_qdev->scconf_qdev_name)
			ptr = cluster_qdev->scconf_qdev_name;
		else
			ptr = "<NULL>";
		print_line(margin, LABELW, NULL,
		    gettext("Quorum device name:"), ptr);

		/* verbose mode prefix (<name>) */
		*prefix = '\0';
		if (verbose) {
			if (cluster_qdev->scconf_qdev_name) {
				(void) sprintf(prefix, "(%s)",
				    cluster_qdev->scconf_qdev_name);
			}
		}

		/* Votes */
		(void) sprintf(value, "%d", cluster_qdev->scconf_qdev_votes);
		print_line(margin + 2, LABELW, prefix,
		    gettext("Quorum device votes:"), value);

		/* State */
		if (cluster_qdev->scconf_qdev_state ==
		    SCCONF_STATE_ENABLED)
			ptr = gettext("yes");
		else
			ptr = gettext("no");
		print_line(margin + 2, LABELW, prefix,
		    gettext("Quorum device enabled:"), ptr);

		/* Device */
		if (cluster_qdev->scconf_qdev_device)
			ptr = cluster_qdev->scconf_qdev_device;
		else
			ptr = "<NULL>";
		print_line(margin + 2, LABELW, prefix,
		    gettext("Quorum device name:"), ptr);

		/* Type */
		if (cluster_qdev->scconf_qdev_type)
			ptr = cluster_qdev->scconf_qdev_type;
		else
			ptr = "<NULL>";
		print_line(margin + 2, LABELW, prefix,
		    gettext("Quorum device type:"), ptr);

		/* Enabled ports */
		i = 0;
		print_line(margin + 2, LABELW, prefix, gettext(
		    "Quorum device hosts (enabled):"), NULL);
		if (cluster_qdev->scconf_qdev_ports) {
			for (portlist = cluster_qdev->scconf_qdev_ports;
			    portlist;
			    portlist = portlist->scconf_qdevport_next) {
				if (portlist->scconf_qdevport_nodename &&
				    portlist->scconf_qdevport_state ==
				    SCCONF_STATE_ENABLED) {
					if (i)
						(void) putchar(' ');
					++i;
					(void) printf(
					    portlist->scconf_qdevport_nodename);
				}
			}
		}
		(void) putchar('\n');

		/* Disabled ports */
		i = 0;
		print_line(margin + 2, LABELW, prefix, gettext(
		    "Quorum device hosts (disabled):"), NULL);
		if (cluster_qdev->scconf_qdev_ports) {
			for (portlist = cluster_qdev->scconf_qdev_ports;
			    portlist;
			    portlist = portlist->scconf_qdevport_next) {
				if (portlist->scconf_qdevport_nodename &&
				    portlist->scconf_qdevport_state !=
				    SCCONF_STATE_ENABLED) {
					if (i)
						(void) putchar(' ');
					++i;
					(void) printf(
					    portlist->scconf_qdevport_nodename);
				}
			}
		}
		(void) putchar('\n');

		/* access mode */
		if (cluster_qdev->scconf_qdev_accessmode) {
			ptr = cluster_qdev->scconf_qdev_accessmode;
			print_line(margin + 2, LABELW, prefix,
			    gettext("Quorum device access mode:"), ptr);
		}

		/* other properties */
		for (prop = cluster_qdev->scconf_qdev_propertylist;
		    prop; prop = prop->scconf_prop_next) {
			if (prop->scconf_prop_key) {
				(void) sprintf(value, gettext(
				    "Quorum device %s:"),
				    prop->scconf_prop_key);
				ptr = prop->scconf_prop_value ?
				    prop->scconf_prop_value : "<NULL>";

				print_line(margin + 2, LABELW, prefix,
				    value, ptr);
			}
		}
	}
}

/*
 * print_node_zones
 *
 *	Print cluster node zones.
 */
static void
print_node_zones(scconf_cfg_node_t *cluster_node, int margin, int verbose)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_zonelist_t *zonelist = (scconf_zonelist_t *)0;
	scconf_zonelist_t *zones;
	char prefix1[BUFSIZ];
	char prefix2[BUFSIZ];
	char value[BUFSIZ];
	char *ptr;
	int i;

	/* check args */
	if (cluster_node == NULL)
		return;

	/* Get the node zones list */
	scconferr = scconf_get_node_zones(
	    cluster_node->scconf_node_nodeid, &zonelist);
	if (scconferr != SCCONF_NOERR) {
		fprintf(stderr, "Failed to get node zone list\n");
		return;
	}

	/* verbose mode prefix (<name>) */
	*prefix1 = '\0';
	if (verbose) {
		if (cluster_node->scconf_node_nodename) {
			(void) sprintf(prefix1, "(%s)",
			    cluster_node->scconf_node_nodename);
		}
	}

	/* Node zone list */
	i = 0;
	print_line(margin, LABELW, prefix1,
	    gettext("Node zones:"), NULL);

	if (zonelist) {
		for (zones = zonelist; zones; zones =
		    zones->scconf_zonelist_next) {
			if (zones->scconf_zone_name) {
				if (i)
					(void) putchar(' ');
				++i;
				(void) printf(
				    zones->scconf_zone_name);
			}
		}
		(void) putchar('\n');
	}
	if (!i)
		(void) printf("<NULL>");
	(void) putchar('\n');

	for (zones = zonelist;  zones; zones = zones->scconf_zonelist_next) {
		/* Node zone list */
		if (zones->scconf_zone_name)
			ptr = zones->scconf_zone_name;
		else
			ptr = "<NULL>";
		print_line(margin, LABELW, prefix1,
		    gettext("Node zonename:"), ptr);

		/* verbose mode prefix (<name>:<adap>) */
		*prefix2 = '\0';
		if (verbose) {
			if (cluster_node->scconf_node_nodename &&
			    zones->scconf_zone_name) {
				(void) sprintf(prefix2, "(%s)",
				    zones->scconf_zone_name);
			}
		}

		/* Zone private hostname */
		if (zones->scconf_zone_privhostname)
			ptr = zones->scconf_zone_privhostname;
		else
			ptr = "<NULL>";
		print_line(margin + 2, LABELW, prefix2,
			gettext("Private hostname:"), ptr);
	}

	if (i)
		(void) putchar('\n');

	scconf_free_zonelist(zonelist);
}

/*
 * print_cltr_adapters
 *
 *	Print cluster transport adapters.
 */
static void
print_cltr_adapters(scconf_cfg_node_t *cluster_node, int margin, int verbose)
{
	scconf_cfg_cltr_adap_t *cltr_adapters;
	scconf_cfg_cltr_adap_t *cltr_adapter;
	scconf_cfg_prop_t *prop;
	char prefix1[BUFSIZ];
	char prefix2[BUFSIZ];
	char value[BUFSIZ];
	char *ptr;
	int i;

	/* check args */
	if (cluster_node == NULL)
		return;

	/* set the adapter list */
	cltr_adapters = cluster_node->scconf_node_adapterlist;

	/* verbose mode prefix (<name>) */
	*prefix1 = '\0';
	if (verbose) {
		if (cluster_node->scconf_node_nodename) {
			(void) sprintf(prefix1, "(%s)",
			    cluster_node->scconf_node_nodename);
		}
	}

	/* Node transport adapters */
	i = 0;
	print_line(margin, LABELW, prefix1,
	    gettext("Node transport adapters:"), NULL);
	if (cltr_adapters) {
		for (cltr_adapter = cltr_adapters;  cltr_adapter;
		    cltr_adapter = cltr_adapter->scconf_adap_next) {
			if (cltr_adapter->scconf_adap_adaptername) {
				if (i)
					(void) putchar(' ');
				++i;
				(void) printf(
				    cltr_adapter->scconf_adap_adaptername);
			}
		}
	}
	if (!i)
		(void) printf("<NULL>");
	(void) putchar('\n');

	for (cltr_adapter = cltr_adapters;  cltr_adapter;
	    cltr_adapter = cltr_adapter->scconf_adap_next) {

		(void) putchar('\n');

		/* Node transport adapter */
		if (cltr_adapter->scconf_adap_adaptername)
			ptr = cltr_adapter->scconf_adap_adaptername;
		else
			ptr = "<NULL>";
		print_line(margin, LABELW, prefix1,
		    gettext("Node transport adapter:"), ptr);

		/* verbose mode prefix (<name>:<adap>) */
		*prefix2 = '\0';
		if (verbose) {
			if (cluster_node->scconf_node_nodename &&
			    cltr_adapter->scconf_adap_adaptername) {
				(void) sprintf(prefix2, "(%s:%s)",
				    cluster_node->scconf_node_nodename,
				    cltr_adapter->scconf_adap_adaptername);
			}
		}

		/* Adapter enabled */
		if (cltr_adapter->scconf_adap_adapterstate ==
		    SCCONF_STATE_ENABLED)
			ptr = gettext("yes");
		else
			ptr = gettext("no");
		print_line(margin + 2, LABELW, prefix2,
		    gettext("Adapter enabled:"), ptr);

		/* Adapter transport type */
		if (cltr_adapter->scconf_adap_cltrtype)
			ptr = cltr_adapter->scconf_adap_cltrtype;
		else
			ptr = "<NULL>";
		print_line(margin + 2, LABELW, prefix2,
		    gettext("Adapter transport type:"), ptr);

		/* Adapter properties */
		for (prop = cltr_adapter->scconf_adap_propertylist;
		    prop;  prop = prop->scconf_prop_next) {
			(void) sprintf(value, "%s=%s",
			    prop->scconf_prop_key ?
			    prop->scconf_prop_key : "<NULL>",
			    prop->scconf_prop_value ?
			    prop->scconf_prop_value : "<NULL>");
			print_line(margin + 2, LABELW, prefix2,
			    gettext("Adapter property:"), value);
		}

		/* Adapter ports */
		print_adapter_ports(cluster_node, cltr_adapter, margin + 2,
		    verbose);
	}
}

/*
 * print_adapter_ports
 *
 *	Print cluster transport adapter ports.
 */
static void
print_adapter_ports(scconf_cfg_node_t *cluster_node,
    scconf_cfg_cltr_adap_t *cltr_adapter, int margin, int verbose)
{
	scconf_cfg_port_t *cltr_adap_ports;
	scconf_cfg_port_t *cltr_adap_port;
	char prefix1[BUFSIZ];
	char prefix2[BUFSIZ];
	char *ptr;
	int i;

	/* check args */
	if (cluster_node == NULL || cltr_adapter == NULL)
		return;

	/* set the ports list */
	cltr_adap_ports = cltr_adapter->scconf_adap_portlist;

	/* verbose mode prefix (<name>:<adap>) */
	*prefix1 = '\0';
	if (verbose) {
		if (cluster_node->scconf_node_nodename &&
		    cltr_adapter->scconf_adap_adaptername) {
			(void) sprintf(prefix1, "(%s:%s)",
			    cluster_node->scconf_node_nodename,
			    cltr_adapter->scconf_adap_adaptername);
		}
	}

	/* Adapter ports */
	i = 0;
	print_line(margin, LABELW, prefix1,
	    gettext("Adapter port names:"), NULL);
	if (cltr_adap_ports) {
		for (cltr_adap_port = cltr_adap_ports;	cltr_adap_port;
		    cltr_adap_port = cltr_adap_port->scconf_port_next) {
			if (cltr_adap_port->scconf_port_portname) {
				if (i)
					(void) putchar(' ');
				++i;
				(void) printf(
				    cltr_adap_port->scconf_port_portname);
			}
		}
	}
	if (!i)
		(void) printf("<NULL>");
	(void) putchar('\n');

	for (cltr_adap_port = cltr_adap_ports;	cltr_adap_port;
	    cltr_adap_port = cltr_adap_port->scconf_port_next) {

		(void) putchar('\n');

		/* Adapter port */
		if (cltr_adap_port->scconf_port_portname)
			ptr = cltr_adap_port->scconf_port_portname;
		else
			ptr = " ";
		print_line(margin, LABELW, prefix1,
		    gettext("Adapter port:"), ptr);

		/* verbose mode prefix (<name>:<adap>@<port>) */
		*prefix2 = '\0';
		if (verbose) {
			if (cluster_node->scconf_node_nodename &&
			    cltr_adapter->scconf_adap_adaptername) {
				(void) sprintf(prefix2, "(%s:%s",
				    cluster_node->scconf_node_nodename,
				    cltr_adapter->scconf_adap_adaptername);
				ptr = cltr_adap_port->scconf_port_portname;
				if (ptr) {
					(void) strcat(prefix2, "@");
					(void) strcat(prefix2, ptr);
				}
				(void) strcat(prefix2, ")");
			}
		}

		/* Port enabled */
		if (cltr_adap_port->scconf_port_portstate ==
		    SCCONF_STATE_ENABLED)
			ptr = gettext("yes");
		else
			ptr = gettext("no");
		print_line(margin + 2, LABELW, prefix2,
		    gettext("Port enabled:"), ptr);
	}
}

/*
 * print_cltr_cpoints
 *
 *	Print the off-node cluster transport connection points.
 */
static void
print_cltr_cpoints(scconf_cfg_cluster_t *clconfig, int margin, int verbose)
{
	scconf_cfg_cpoint_t *cltr_cpoints;
	scconf_cfg_cpoint_t *cltr_cpoint;
	scconf_cfg_prop_t *prop;
	char prefix[BUFSIZ];
	char value[BUFSIZ];
	char *ptr;
	int i;

	/* check args */
	if (clconfig == NULL)
		return;

	/* set the cpoint list */
	cltr_cpoints = clconfig->scconf_cluster_cpointlist;

	/* Cluster off-node connection points */
	i = 0;
	print_line(margin, LABELW, NULL,
	    gettext("Cluster transport switches:"), NULL);
	if (cltr_cpoints) {
		for (cltr_cpoint = cltr_cpoints;  cltr_cpoint;
		    cltr_cpoint = cltr_cpoint->scconf_cpoint_next) {
			if (cltr_cpoint->scconf_cpoint_cpointname) {
				if (i)
					(void) putchar(' ');
				++i;
				(void) printf(
				    cltr_cpoint->scconf_cpoint_cpointname);
			}
		}
	}
	if (!i)
		(void) printf("<NULL>");
	(void) putchar('\n');

	for (cltr_cpoint = cltr_cpoints;  cltr_cpoint;
	    cltr_cpoint = cltr_cpoint->scconf_cpoint_next) {

		(void) putchar('\n');

		/* Cluster Node name */
		if (cltr_cpoint->scconf_cpoint_cpointname)
			ptr = cltr_cpoint->scconf_cpoint_cpointname;
		else
			ptr = "<NULL>";
		print_line(margin, LABELW, NULL,
		    gettext("Cluster transport switch:"), ptr);

		/* verbose mode prefix (<cpoint>) */
		*prefix = '\0';
		if (verbose) {
			if (cltr_cpoint->scconf_cpoint_cpointname) {
				(void) sprintf(prefix, "(%s)",
				    cltr_cpoint->scconf_cpoint_cpointname);
			}
		}

		/* C-point enabled */
		if (cltr_cpoint->scconf_cpoint_cpointstate ==
		    SCCONF_STATE_ENABLED)
			ptr = gettext("yes");
		else
			ptr = gettext("no");
		print_line(margin + 2, LABELW, prefix,
		    gettext("Switch enabled:"), ptr);

		/* C-point type */
		if (cltr_cpoint->scconf_cpoint_type)
			ptr = cltr_cpoint->scconf_cpoint_type;
		else
			ptr = "<NULL>";
		print_line(margin + 2, LABELW, prefix,
		    gettext("Switch type:"), ptr);

		/* C-point properties */
		for (prop = cltr_cpoint->scconf_cpoint_propertylist;
		    prop;  prop = prop->scconf_prop_next) {
			(void) sprintf(value, "%s=%s",
			    prop->scconf_prop_key ?
			    prop->scconf_prop_key : "<NULL>",
			    prop->scconf_prop_value ?
			    prop->scconf_prop_value : "<NULL>");
			print_line(margin + 2, LABELW, prefix,
			    gettext("Switch property:"), value);
		}

		/* C-point ports */
		print_cpoint_ports(cltr_cpoint, margin + 2, verbose);
	}
}

/*
 * print_cpoint_ports
 *
 *	Print ports for an off-node cluster transport connection point.
 */
static void
print_cpoint_ports(scconf_cfg_cpoint_t *cltr_cpoint, int margin,
    int verbose)
{
	scconf_cfg_port_t *cltr_cpoint_ports;
	scconf_cfg_port_t *cltr_cpoint_port;
	char prefix1[BUFSIZ];
	char prefix2[BUFSIZ];
	char *ptr;
	int i;

	/* check args */
	if (cltr_cpoint == NULL)
		return;

	/* set the ports list */
	cltr_cpoint_ports = cltr_cpoint->scconf_cpoint_portlist;

	/* verbose mode prefix (<cpoint>) */
	*prefix1 = '\0';
	if (verbose) {
		if (cltr_cpoint->scconf_cpoint_cpointname) {
			(void) sprintf(prefix1, "(%s)",
			    cltr_cpoint->scconf_cpoint_cpointname);
		}
	}

	/* C-point ports */
	i = 0;
	print_line(margin, LABELW, prefix1,
	    gettext("Switch port names:"), NULL);
	if (cltr_cpoint_ports) {
		for (cltr_cpoint_port = cltr_cpoint_ports;  cltr_cpoint_port;
		    cltr_cpoint_port = cltr_cpoint_port->scconf_port_next) {
			if (cltr_cpoint_port->scconf_port_portname) {
				if (i)
					(void) putchar(' ');
				++i;
				(void) printf(
				    cltr_cpoint_port->scconf_port_portname);
			}
		}
	}
	if (!i)
		(void) printf(gettext("<NULL>"));
	(void) putchar('\n');

	for (cltr_cpoint_port = cltr_cpoint_ports;  cltr_cpoint_port;
	    cltr_cpoint_port = cltr_cpoint_port->scconf_port_next) {

		(void) putchar('\n');

		/* C-point port */
		if (cltr_cpoint_port->scconf_port_portname)
			ptr = cltr_cpoint_port->scconf_port_portname;
		else
			ptr = " ";
		print_line(margin, LABELW, prefix1,
		    gettext("Switch port:"), ptr);

		/* verbose mode prefix (<cpoint>@<port>) */
		*prefix2 = '\0';
		if (verbose) {
			if (cltr_cpoint->scconf_cpoint_cpointname) {
				(void) sprintf(prefix2, "(%s",
				    cltr_cpoint->scconf_cpoint_cpointname);
				ptr = cltr_cpoint_port->scconf_port_portname;
				if (ptr) {
					(void) strcat(prefix2, "@");
					(void) strcat(prefix2, ptr);
				}
				(void) strcat(prefix2, ")");
			}
		}

		/* Port enabled */
		if (cltr_cpoint_port->scconf_port_portstate ==
		    SCCONF_STATE_ENABLED)
			ptr = gettext("yes");
		else
			ptr = gettext("no");
		print_line(margin + 2, LABELW, prefix2,
		    gettext("Port enabled:"), ptr);
	}
}

/*
 * print_cltr_cables
 *
 *	Print cluster transport cables.
 */
/*ARGSUSED*/
static void
print_cltr_cables(scconf_cfg_cluster_t *clconfig, int margin, int verbose)
{
	scconf_cfg_cable_t *cltr_cables;
	scconf_cfg_cable_t *cltr_cable;
	char buffer[BUFSIZ];
	char e1[BUFSIZ];
	char e2[BUFSIZ];
	char *ptr;

	/* check args */
	if (clconfig == NULL)
		return;

	/* set the cable list */
	cltr_cables = clconfig->scconf_cluster_cablelist;

	/* print header */
	(void) putchar('\n');
	(void) puts(gettext("Cluster transport cables"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, gettext("Endpoint"));
	print_column(COLW20, gettext("Endpoint"));
	print_column(0, gettext("State"));
	(void) putchar('\n');

	print_column(COLW20, NULL);
	print_column(COLW20, dashes(buffer, gettext("Endpoint")));
	print_column(COLW20, dashes(buffer, gettext("Endpoint")));
	print_column(0, dashes(buffer, gettext("State")));
	(void) putchar('\n');

	/* Print a line for each cable */
	for (cltr_cable = cltr_cables;	cltr_cable;
	    cltr_cable = cltr_cable->scconf_cable_next) {

		/* Get endpoints */
		get_cable_epoint(&cltr_cable->scconf_cable_epoint1, e1);
		get_cable_epoint(&cltr_cable->scconf_cable_epoint2, e2);

		/* print label */
		print_column(COLW20, gettext("	Transport cable:"));

		/* print endpoint 1 */
		print_column_value(COLW20, e1);

		/* print endpoint 2 */
		print_column_value(COLW20, e2);

		/* print state */
		if (cltr_cable->scconf_cable_cablestate ==
		    SCCONF_STATE_ENABLED) {
			ptr = gettext("Enabled");
		} else {
			ptr = gettext("Disabled");
		}
		print_column_value(0, ptr);

		/* new line */
		(void) putchar('\n');
	}

	/* blank line */
	(void) putchar('\n');
}

/*
 * get_cable_epoint
 *
 *	Get cluster transport cable endpoint.
 */
static void
get_cable_epoint(scconf_cltr_epoint_t *epoint, char *buffer)
{
	/* initialize the buffer */
	*buffer = '\0';

	/* if adatper, pre-pend the hostname */
	if (epoint->scconf_cltr_epoint_type ==
	    SCCONF_CLTR_EPOINT_TYPE_ADAPTER &&
	    epoint->scconf_cltr_epoint_nodename != NULL)
		(void) sprintf(buffer, "%s:",
		    epoint->scconf_cltr_epoint_nodename);

	/* add the device name */
	if (epoint->scconf_cltr_epoint_devicename == NULL)
		return;
	(void) strcat(buffer, epoint->scconf_cltr_epoint_devicename);

	/* add the port name */
	if (epoint->scconf_cltr_epoint_portname) {
		(void) strcat(buffer, "@");
		(void) strcat(buffer, epoint->scconf_cltr_epoint_portname);
	}
}

/*
 * print_devicegroups
 *
 *	Print the device groups configuration.
 */
static void
print_devicegroups(scconf_cfg_ds_t *dsconfig, int margin, int verbose)
{
	scconf_cfg_ds_t *device_group;
	scconf_namelist_t *namelist;
	scconf_nodeid_t *current_nodeid;
	scconf_errno_t rstatus;
	char prefix[BUFSIZ];
	char value[BUFSIZ];
	char buffer[BUFSIZ];
	char devnamebuf[BUFSIZ];
	char *ptr;

	/* check args */
	if (dsconfig == NULL)
		return;

	for (device_group = dsconfig; device_group;
	    device_group = device_group->scconf_ds_next) {

		/* If not very-verbose, see if we should hide this group */
		if (verbose < 2 &&
		    hide_dg(device_group->scconf_ds_name, dsconfig))
			continue;

		/* blank line */
		(void) putchar('\n');

		/* Device Group name */
		if (device_group->scconf_ds_name)
			ptr = device_group->scconf_ds_name;
		else
			ptr = "<NULL>";
		print_line(margin, LABELW, NULL,
		    gettext("Device group name:"), ptr);

		/* verbose mode prefix (<name>) */
		*prefix = '\0';
		if (verbose) {
			if (device_group->scconf_ds_name &&
			    device_group->scconf_ds_type) {
				(void) sprintf(prefix, "(%s)",
				    device_group->scconf_ds_name);
			}
		}
		/* Device Type */
		if (device_group->scconf_ds_type)
			ptr = device_group->scconf_ds_type;
		else
			ptr = "<NULL>";
		print_line(margin + 2, LABELW, prefix,
		    gettext("Device group type:"), ptr);

		/* Failback enabled */
		if (device_group->scconf_ds_failback == 1)
			ptr = gettext("yes");
		else
			ptr = gettext("no");
		print_line(margin + 2, LABELW, prefix,
		    gettext("Device group failback enabled:"), ptr);

		bzero(value, sizeof (value));
		/* Node preferencelist */
		current_nodeid = device_group->scconf_ds_nodelist;
		ptr = NULL;
		if (current_nodeid != NULL) {
			char nodebuf[BUFSIZ];
			while (*current_nodeid) {
				rstatus = scconf_get_nodename(*current_nodeid,
				    &ptr);
				if (rstatus != SCCONF_NOERR) {
					/*
					 * Could not get the node name!  Print
					 * out the nodeid instead.
					 */
					(void) sprintf(nodebuf, "%d",
					    *current_nodeid);
					(void) strcat(value, nodebuf);
				} else {
					(void) strcat(value, ptr);
				}
				free(ptr);
				ptr = NULL;
				if ((*(current_nodeid+1)) != 0) {
					(void) strcat(value, ", ");
				}
				current_nodeid++;
			}
		}
		if (strlen(value) == 0) {
			(void) strcpy(value, "<NULL>");
		}

		print_line(margin + 2, LABELW, prefix,
		    gettext("Device group node list:"), value);

		/* Node preferencelist ordered */
		if (device_group->scconf_ds_preference == SCCONF_STATE_ENABLED)
			ptr = gettext("yes");
		else
			ptr = gettext("no");
		print_line(margin + 2, LABELW, prefix,
		    gettext("Device group ordered node list:"), ptr);

		/*
		 * Desired number of secondaries
		 *
		 * zero means default is used for this device group.
		 * numsecondaries can't be set to zero otherwise.
		 */

		if (device_group->scconf_ds_num_secs == 0 &&
		    !scconf_is_local_device_service(
		    device_group->scconf_ds_name)) {
			(void) sprintf(value, "%d",
			    DCS_DEFAULT_DESIRED_NUM_SECONDARIES);
		} else {
			(void) sprintf(value, "%d",
			    device_group->scconf_ds_num_secs);
		}

		print_line(margin + 2, LABELW, prefix,
		    gettext("Device group desired number of secondaries:"),
		    value);

		/* Device or diskgroup names */
		/*   buffer is the label at the beginning of the line */
		(void) sprintf(buffer, gettext("Device group %s:"),
			device_group->scconf_ds_label);
		/* devnamebuf accumulates the list of device names */
		devnamebuf[0] = '\0';
		for (namelist = device_group->scconf_ds_devvalues;
		    namelist; namelist = namelist->scconf_namelist_next) {
			if (!namelist->scconf_namelist_name)
				continue;
			/* add it to the list (devnamebuf) */
			if (strlen(devnamebuf) != 0)
				(void) strcat(devnamebuf, " ");
			(void) strcat(devnamebuf,
			    namelist->scconf_namelist_name);
		}
		if (strlen(devnamebuf) == 0)
			(void) strcpy(devnamebuf, "<NULL>");
		print_line(margin + 2, LABELW, prefix, buffer, devnamebuf);

		/* Replication type.  Will be displayed only if set. */
		ptr = scconf_get_repl_property(
		    device_group->scconf_ds_propertylist);
		if (ptr != NULL) {
			print_line(margin + 2, LABELW, prefix,
			    gettext("Device group replication type:"), ptr);
		}
	}
}

/*
 * hide_dg
 *
 *	Return non-zero if the disk group should be hidden in
 *	non-vv (very verbose) mode.   A group is hidden if the
 *	SCCONF_DS_PROP_AUTOGENERATED property is set.
 *
 *	Otherwise, return zero.
 */
static int
hide_dg(char *dgname, scconf_cfg_ds_t *dgconfig)
{
	scconf_cfg_ds_t *dgp;
	scconf_cfg_prop_t *prop;

	/* Check arguemnts */
	if (dgname == NULL || dgconfig == NULL)
		return (0);

	/* Find the device group in the list */
	for (dgp = dgconfig;  dgp;  dgp = dgp->scconf_ds_next) {
		if (dgp->scconf_ds_name &&
		    strcmp(dgname, dgp->scconf_ds_name) == 0)
			break;
	}

	/* If we cannot find the group, just return zero */
	if (dgp == NULL)
		return (0);

	/* Check to see if the "autogenerated" property is set */
	for (prop = dgp->scconf_ds_propertylist;  prop;
	    prop = prop->scconf_prop_next) {
		if (prop->scconf_prop_key &&
		    strcmp(SCCONF_DS_PROP_AUTOGENERATED,
		    prop->scconf_prop_key) == 0)
			return (1);
	}

	return (0);
}

/*
 * tunable_hb_is_available
 *
 * Check if tunable heart beat feature is availble.
 *
 * return: B_TRUE if tunable heart beat feature is availble.
 *	   B_FALSE otherwise.
 */
static boolean_t
tunable_hb_is_available(void) {
	clcomm_vp_version_t scconf_version;

	if (clcomm_get_running_version("scconf", &scconf_version)
	    != 0) {
		return (B_FALSE);
	}

	return ((boolean_t)(scconf_version.major_num >= 2));
}

/*
 * scconfcmd_change_clustername
 *
 *	Change the nodename of a given node.
 */
static scconf_errno_t
scconfcmd_change_clustername(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_CLUSTER,			/* 0 - new cluster name */
		(char *)0
	};
	char *cluster = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - new cluster name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, cluster);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			cluster = value;
			break;

		/* unknown suboptions */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (cluster == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for changing the "
		    "cluster name.\n"), progname, SUBOPT_CLUSTER);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* change the clustername */
	scconferr = scconf_change_clustername(cluster, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to change the cluster name - %s.\n"),
		    progname, errbuff);
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_add_node
 *
 *	Add the given "nodename" to the cluster configuration.
 */
static scconf_errno_t
scconfcmd_add_node(char *subopts, char **messages, uint_t uflag,
    scconf_cltr_handle_t handle)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NODE,			/* 0 - node name */
		SUBOPT_TYPE,			/* 1 - node type */
		SUBOPT_ADAPTERS,		/* 2 - adapter list */
		(char *)0
	};
	char *nodename = NULL;
	char *type = NULL;
	char *adapterlist = NULL;
	scconf_nodetype_t nodetype = SCCONF_NODE_CLUSTER;	/* default */

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* just "nodename" by itself is okay */
	if (strchr(dupopts, ',') == NULL && strchr(dupopts, '=') == NULL) {
		nodename = dupopts;
	} else {
		current = dupopts;
		while (*current) {
			optindex = getsubopt(&current, knownopts, &value);
			switch (optindex) {

			/* 0 - node name */
			case 0:
				scconferr = suboption_usage(knownopts[optindex],
				    value, nodename);
				if (scconferr != SCCONF_NOERR)
					goto cleanup;
				nodename = value;
				break;

			/* 1 - node type */
			case 1:
				if (!iseuropa) {
					suboption_unknown(value);
					scconferr = SCCONF_EUSAGE;
					goto cleanup;
				}
				scconferr = suboption_usage(knownopts[optindex],
				    value, type);
				if (scconferr != SCCONF_NOERR)
					goto cleanup;
				type = value;
				break;

			/* 2 - adapter list */
			case 2:
				if (!iseuropa) {
					suboption_unknown(value);
					scconferr = SCCONF_EUSAGE;
					goto cleanup;
				}
				scconferr = suboption_usage(knownopts[optindex],
				    value, adapterlist);
				if (scconferr != SCCONF_NOERR)
					goto cleanup;
				adapterlist = value;
				break;
			/* unknown suboptions */
			case -1:
				suboption_unknown(value);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;

			/* internal error */
			default:
				scconferr = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}
	}

	/* check for required suboptions */
	if (nodename == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for adding "
		    "a node.\n"), progname, SUBOPT_NODE);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* make sure that there is no ':' in the name */
	if (strchr(nodename, ':') || strchr(nodename, '@')) {
		(void) fprintf(stderr, gettext(
		    "%s:  The node name may not include "
		    "embedded %s or %s characters.\n"),
		    progname, "':'", "'@'");
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Make sure the type is either "farm" or "server" */
	if (iseuropa && type) {
		if (strcasecmp(type, STR_FARM) == 0) {
			nodetype = SCCONF_NODE_FARM;
		} else if (strcasecmp(type, STR_CLUSTER) != 0) {
			(void) fprintf(stderr, gettext(
			    "%s: Unknown node type.\n"), progname);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}

		/* The adapterlist is a must for farm nodes */
		if (nodetype == SCCONF_NODE_FARM && adapterlist == NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Suboption \"%s\" is required for \"%s\" "
			    "node type.\n"), progname, SUBOPT_ADAPTERS, type);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}

		/* No adapterlist for server nodes */
		if (nodetype == SCCONF_NODE_CLUSTER && adapterlist) {
			(void) fprintf(stderr, gettext(
			    "%s:  Suboption \"%s\" cannot be used with "
			    "\"%s\" node type.\n"),
			    progname, SUBOPT_ADAPTERS, type);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* add the nodename */
	if (nodetype == SCCONF_NODE_FARM) {
		scconferr = scconf_add_farmnode(handle, nodename,
		    adapterlist, messages);
	} else {
		scconferr = scconf_add_node(handle, nodename, messages);
	}
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EEXIST:
			(void) sprintf(errbuff, gettext(
			    "node is already configured"));
			break;

		case SCCONF_ENODEID:
			(void) sprintf(errbuff, gettext("invalid node name"));
			break;

		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		case SCCONF_EINVAL:
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (nodename != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add node (%s) - %s.\n"),
			    progname, nodename, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add node - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_change_nodename
 *
 *	Change the nodename of a given node.
 */
scconf_errno_t
scconfcmd_change_nodename(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NODE,			/* 0 - node name */
		SUBOPT_CHANGETO,		/* 1 - new node name */
		(char *)0
	};
	char *node = NULL;
	char *changeto = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - node name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, node);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			node = value;
			break;

		/* 1 - new node name */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, changeto);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			changeto = value;
			break;

		/* unknown suboptions */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (node == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for changing "
		    "a node name.\n"), progname, SUBOPT_NODE);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}
	if (changeto == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  The \"%s\" suboption is required for changing "
		    "a node name.\n"), progname, SUBOPT_CHANGETO);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* change the nodename */
	scconferr = scconf_change_nodename(changeto, node, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EEXIST:
			if (changeto != NULL) {
				(void) sprintf(errbuff, gettext(
				    "node name \"%s\" is already configured"),
				    changeto);
			} else {
				scconf_strerr(errbuff, scconferr);
			}
			break;

		case SCCONF_EBUSY:
			(void) fprintf(stderr, gettext(
			    "%s:  A node must be shutdown, "
			    "before it\'s name can be changed.\n"), progname);
			(void) sprintf(errbuff, gettext(
			    "node is an active cluster member"));
			break;

		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext("node is unknown"));
			break;

		case SCCONF_ENODEID:
			if (changeto != NULL) {
				(void) sprintf(errbuff, gettext(
				    "\"%s\" is an invalid node name"),
				    changeto);
			} else {
				scconf_strerr(errbuff, scconferr);
			}
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (node != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to change the node name (%s) - %s.\n"),
			    progname, node, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to change the node name - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_change_farm_monstate
 *
 *	Change the monitoring state for given farm node(s).
 */
scconf_errno_t
scconfcmd_change_farm_monstate(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NODELIST,		/* 0 - node name */
		SUBOPT_MONITORING,		/* 1 - new monitoring state */
		(char *)0
	};
	char *nodelist = NULL;
	char *monitoring = NULL;
	scconf_state_t state = SCCONF_STATE_UNCHANGED;
	uint_t allnodes = 0;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - node name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, nodelist);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			nodelist = value;
			break;

		/* 1 - new monitoring state */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, monitoring);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			monitoring = value;
			break;

		/* unknown suboptions */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* check for required suboptions */
	if (monitoring == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  The \"%s\" suboption is required for changing "
		    "the farm node(s) monitoring state.\n"),
		    progname, SUBOPT_MONITORING);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	} else {
		if ((strcmp(monitoring, STR_ENABLED) != 0) &&
			(strcmp(monitoring, STR_DISABLED) != 0)) {
			(void) fprintf(stderr, gettext(
			    "%s:  The \"%s\" suboption must be "
			    "set to \"%s\" or \"%s\".\n"),
			    progname, SUBOPT_MONITORING,
			    STR_ENABLED, STR_DISABLED);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
	}

	/* Set allnodes, if user didn't specify the nodelist */
	if (nodelist == NULL)
		allnodes = 1;

	/* change the monitoring state for given node(s) */
	scconferr = scconf_change_farm_monstate(nodelist, value,
	    allnodes, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext("node is unknown"));
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_rm_node
 *
 *	Remove the given "node" from the cluster configuration.
 */
static scconf_errno_t
scconfcmd_rm_node(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NODE,			/* 0 - node name */
		SUBOPT_TYPE,			/* 1 - node type */
		(char *)0
	};
	char *node = NULL;
	char *type = NULL;
	scconf_nodetype_t nodetype = SCCONF_NODE_CLUSTER;	/* default */

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* just "node" by itself is okay */
	if (strchr(dupopts, ',') == NULL && strchr(dupopts, '=') == NULL) {
		node = dupopts;
	} else {
		current = dupopts;
		while (*current) {
			optindex = getsubopt(&current, knownopts, &value);
			switch (optindex) {

			/* 0 - node name */
			case 0:
				scconferr = suboption_usage(knownopts[optindex],
				    value, node);
				if (scconferr != SCCONF_NOERR)
					goto cleanup;
				node = value;
				break;

			/* 1 - node type */
			case 1:
				if (!iseuropa) {
					suboption_unknown(value);
					scconferr = SCCONF_EUSAGE;
					goto cleanup;
				}
				scconferr = suboption_usage(knownopts[optindex],
				    value, type);
				if (scconferr != SCCONF_NOERR)
					goto cleanup;
				type = value;
				break;

			/* unknown suboptions */
			case -1:
				suboption_unknown(value);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;

			/* internal error */
			default:
				scconferr = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}
	}

	/* check for required suboptions */
	if (node == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for removing "
		    "a node.\n"), progname, SUBOPT_NODE);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Make sure the type is either "farm" or "server". */
	if (iseuropa && type) {
		if (strcasecmp(type, STR_FARM) == 0) {
			nodetype = SCCONF_NODE_FARM;
		} else if (strcasecmp(type, STR_CLUSTER) != 0) {
			(void) fprintf(stderr, gettext(
			    "%s: Unknown node type.\n"), progname);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
	}
	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* remove the node */
	if (nodetype == SCCONF_NODE_FARM) {
		scconferr = scconf_rm_farmnode(node, messages);
	} else {
		scconferr = scconf_rm_node(node, 0, messages);
	}
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext("node is unknown"));
			break;

		case SCCONF_EINUSE:
			(void) sprintf(errbuff, gettext(
			    "node is still cabled or otherwise in use"));
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (node != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to remove node (%s) - %s.\n"),
			    progname, node, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to remove node - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_change_heartbeat
 *
 *	Change or set the global heart beat parameter settings
 */
static scconf_errno_t
scconfcmd_change_heartbeat(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];

	/* Check for suboptions */
	if (subopts == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  At lease one suboption, \"%s\" or "
		    "\"%s\" is required for setting or "
		    "changing heart beat settings.\n"),
		    progname, SUBOPT_HB_TIMEOUT, SUBOPT_HB_QUANTUM);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Check if adding adapter is allowed */
	if (!tunable_hb_is_available()) {
		scconferr = SCCONF_VP_MISMATCH;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Change the heart beat settings */
	scconferr = scconf_change_heartbeat(subopts, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "node or adapter is not found"));
			break;

		case SCCONF_VP_MISMATCH:
			(void) sprintf(errbuff, gettext(
			    "cluster upgrade is not committed"));
			break;

		case SCCONF_TM_EBADOPTS:
			(void) sprintf(errbuff, gettext(
			    "bad or unknown adapter properties"));
			break;

		case SCCONF_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "invalid argument"));
			break;

		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */

		(void) fprintf(stderr, gettext(
		    "%s:  Failed to change transport heart beat "
		    "settings - %s.\n"), progname, errbuff);
	}

	return (scconferr);
}

/*
 * scconfcmd_change_udp_timeout
 *
 * Change or set the global udp session timeout settings
 */

static scconf_errno_t
scconfcmd_change_udp_timeout(char *subopts, char **messages, uint_t uflag)
{

	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];

	/* Check for suboptions */

	if (subopts == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  One suboption, \"%s\""
		    "is required for setting or "
		    "changing UDP session timeout.\n"),
		    progname, SUBOPT_UDP_TIMEOUT);
		    scconferr = SCCONF_EUSAGE;
		    goto cleanup;
	}

	/* just check usage? */

	if (uflag)
		goto cleanup;

	/* Change the UDP timeout settings */

	scconferr = scconf_change_udp_timeout(subopts, messages);

cleanup:
	/* Print additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
			case SCCONF_ENOEXIST:
				(void) sprintf(errbuff, gettext(
				    "property is not found"));
				break;
			case SCCONF_VP_MISMATCH:
				(void) sprintf(errbuff, gettext(
				    "cluster upgrade is not committed"));
				    break;
			case SCCONF_TM_EBADOPTS:
				(void) sprintf(errbuff, gettext(
				    "bad or unknown properties"));
				    break;
			case SCCONF_EINVAL:
				(void) sprintf(errbuff, gettext(
				    "invalid argument"));
				break;
			case SCCONF_ESETUP:
			case SCCONF_ENOCLUSTER:
				scconferr = SCCONF_EUNEXPECTED;
				/*FALLTHRU*/
			default:
				scconf_strerr(errbuff, scconferr);
				break;
		} /*lint !e788 */

		(void) fprintf(stderr, gettext(
		    "%s: Failed to change cluster UDP session "
		    "timeout settings - %s.\n"), progname, errbuff);
	}

	return (scconferr);
}

/*
 * scconfcmd_add_cltr_adapter
 *
 *	Add an adapter to the cluster configuration.
 */
static scconf_errno_t
scconfcmd_add_cltr_adapter(char *subopts, char **messages, uint_t uflag,
    scconf_cltr_handle_t handle)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *last, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_TRTYPE,			/* 0 - transport type */
		SUBOPT_NAME,			/* 1 - adapter name */
		SUBOPT_NODE,			/* 2 - node name or ID */
		SUBOPT_TRANSPORT_TYPE,		/* 3 - NOT LEGAL property */
		SUBOPT_VLANID,			/* 4 - optional vlanid */
		SUBOPT_VLANID_BAD,		/* 5 - INVALID: vlan_id */
		(char *)0
	};
	char *transport_type = NULL;
	char *adaptername = NULL;
	char *node = NULL;
	char *properties = NULL;
	char *svlanid = NULL;
	int vlanid = 0;
	long tmp_vlanid = 0;
	char *endp;
	uint_t noenable = 1;			/* default is disable */

	/* duplicate subopts and allocate space for properties */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (properties = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	bzero(properties, strlen(properties));

	/* Check if adding adapter is allowed */
	if (!tunable_hb_is_available()) {
		scconferr = SCCONF_VP_MISMATCH;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		last = current;
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - transport type */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, transport_type);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			transport_type = value;
			break;

		/* 1 - adapter name */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, adaptername);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			adaptername = value;
			break;

		/* 2 - node name or ID */
		case 2:
			scconferr = suboption_usage(knownopts[optindex],
			    value, node);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			node = value;
			break;

		/* 3 - NOT LEGAL property */
		case 3:
			(void) fprintf(stderr, gettext(
			    "%s: Use the \"%s\" suboption to set "
			    "the transport type when you add an adapter.\n"),
			    progname, SUBOPT_TRTYPE);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* 4 - vlan ID */
		case 4:
			scconferr = suboption_usage(knownopts[optindex],
			    value, svlanid);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			svlanid = value;

			tmp_vlanid = strtol(value, &endp, 10);
			if (*endp != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s: Invalid value %s given for "
				    "vlanid.\n"), progname, value);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			if ((tmp_vlanid < VLAN_ID_MIN) ||
			    (tmp_vlanid > VLAN_ID_MAX)) {
				(void) fprintf(stderr, gettext(
				    "%s: Value %s given for vlanid "
				    "is out of range.\n"), progname, value);
				scconferr = SCCONF_ERANGE;
				goto cleanup;
			}

			vlanid = (int)tmp_vlanid;
			break;

		/* 5 - vlan_id: not legal */
		case 5:
			(void) fprintf(stderr, gettext(
			    "%s: Use the \"%s\" suboption to specify "
			    "the VLAN ID when you add an adapter.\n"),
			    progname, SUBOPT_VLANID);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;


		/* properties */
		case -1:
			if (*properties != '\0')
				(void) strcat(properties, ",");
			(void) strcat(properties, last);
			break;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (transport_type == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for adding a "
		    "transport adapter.\n"), progname, SUBOPT_TRTYPE);
		scconferr = SCCONF_EUSAGE;
	}
	if (adaptername == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for adding a "
		    "transport adapter.\n"), progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
	}
	if (node == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for adding a "
		    "transport adapter.\n"), progname, SUBOPT_NODE);
		scconferr = SCCONF_EUSAGE;
	}
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

	/* make sure that there is no ':' in the name */
	if (adaptername != NULL &&
	    (strchr(adaptername, ':') || strchr(adaptername, '@'))) {
		(void) fprintf(stderr, gettext(
		    "%s:  The adapter name may not include "
		    "embedded %s or %s characters.\n"),
		    progname, "':'", "'@'");
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* add the adapter */
	scconferr = scconf_add_cltr_adapter(handle, noenable, node,
	    transport_type, adaptername, vlanid,
	    *properties ? properties : NULL, messages);

	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EUNKNOWN:
			(void) sprintf(errbuff, gettext(
			    "unknown adapter or transport type"));
			break;

		case SCCONF_VP_MISMATCH:
			(void) sprintf(errbuff, gettext(
			    "cluster upgrade is not committed"));
			break;

		case SCCONF_ENOEXIST:
			if (node != NULL) {
				(void) sprintf(errbuff, gettext(
				    "node \"%s\" is unknown"), node);
			} else {
				(void) sprintf(errbuff, gettext(
				    "node is unknown"));
			}
			break;

		case SCCONF_EEXIST:
			if (node != NULL && adaptername != NULL) {
				(void) sprintf(errbuff, gettext(
				    "node \"%s\" is already configured with "
				    "adapter \"%s\" or another adapter based "
				    "on the same physical device"), node,
				    adaptername);
			} else {
				(void) sprintf(errbuff, gettext(
				    "adapter is already configured"));
			}
			break;

		case SCCONF_TM_EBADOPTS:
			(void) sprintf(errbuff, gettext(
			    "bad or unknown adapter properties given"));
			break;

		case SCCONF_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "invalid argument"));
			break;

		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (node != NULL && adaptername != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add cluster transport "
			    "adapter (%s:%s) - %s.\n"), progname,
			    node, adaptername, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add cluster transport "
			    "adapter - %s.\n"), progname, errbuff);
		}
	}

	/* Free memory */
	if (properties)
		free(properties);
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_change_cltr_adapter
 *
 *	Change cluster transport adapter state or other properties.
 */
static scconf_errno_t
scconfcmd_change_cltr_adapter(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value = NULL;
	char *current, *last, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NAME,			/* 0 - adapter name */
		SUBOPT_NODE,			/* 1 - node name or ID */
		SUBOPT_STATE,			/* 2 - adapter state */
		SUBOPT_TRANSPORT_TYPE,		/* 3 - NOT LEGAL property */
		SUBOPT_TRTYPE,			/* 4 - NOT LEGAL property */
		SUBOPT_VLANID,			/* 5 - optional vlanid */
		SUBOPT_VLANID_BAD,		/* 6 - INVALID vlan_id */
		(char *)0
	};
	char *adaptername = NULL;
	char *node = NULL;
	char *sstate = NULL;
	scconf_cltr_adapterstate_t state = SCCONF_STATE_UNCHANGED;
	char *properties = NULL;
	char *svlanid = NULL;
	int vlanid = 0;
	long tmp_vlanid = 0;
	char *endp;
	uint_t cablechk = 0;

	/* duplicate subopts and allocate space for properties */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (properties = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	bzero(properties, strlen(properties));

	/* Check if adding adapter is allowed */
	if (!tunable_hb_is_available()) {
		scconferr = SCCONF_VP_MISMATCH;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		last = current;
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - adapter name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, adaptername);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			adaptername = value;
			break;

		/* 1 - node name or ID */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, node);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			node = value;
			break;

		/* 2 - state */
		case 2:
			scconferr = suboption_usage(knownopts[optindex],
			    value, sstate);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			sstate = value;
			break;

		/* 3 - NOT LEGAL property */
		/* 4 - NOT LEGAL property */
		case 3:
		case 4:
			(void) fprintf(stderr, gettext(
			    "%s:  The \"%s\" transport adapter property "
			    "cannot be changed.\n"),
			    progname, knownopts[optindex]);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* 5 - vlan ID */
		case 5:
			scconferr = suboption_usage(knownopts[optindex],
			    value, svlanid);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			svlanid = value;

			tmp_vlanid = strtol(value, &endp, 10);
			if (*endp != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s: Invalid value %s given for "
				    "vlanid.\n"), progname, value);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			if ((tmp_vlanid < VLAN_ID_MIN) ||
			    (tmp_vlanid > VLAN_ID_MAX)) {
				(void) fprintf(stderr, gettext(
				    "%s: Value %s given for vlanid "
				    "is out of range.\n"), progname, value);
				scconferr = SCCONF_ERANGE;
				goto cleanup;
			}

			vlanid = (int)tmp_vlanid;
			break;

		/* 6 - Invalid: vlan_id */
		case 6:
			(void) fprintf(stderr, gettext(
			    "%s: Use the \"%s\" suboption to specify "
			    "the VLAN ID when you change an adapter.\n"),
			    progname, SUBOPT_VLANID);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* properties */
		case -1:
			if (*properties != '\0')
				(void) strcat(properties, ",");
			(void) strcat(properties, last);
			break;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (adaptername == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for changing "
		    "transport adapter properties.\n"), progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
	}
	if (node == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for changing "
		    "transport adapter properties.\n"), progname, SUBOPT_NODE);
		scconferr = SCCONF_EUSAGE;
	}
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

	/* set the state */
	if (sstate != NULL) {
		if (value == NULL) {
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		if (strcmp(value, STR_ENABLED) == 0)
			state = SCCONF_STATE_ENABLED;
		else if (strcmp(value, STR_DISABLED) == 0)
			state = SCCONF_STATE_DISABLED;
		else {
			(void) fprintf(stderr, gettext(
			    "%s:  The \"%s\" suboption must be "
			    "set to \"%s\" or \"%s\".\n"),
			    progname, SUBOPT_STATE,
			    STR_ENABLED, STR_DISABLED);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* change the adapter properties */
	scconferr = scconf_change_cltr_adapter(NULL, node, adaptername, vlanid,
	    *properties ? properties : NULL, state, cablechk, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EBUSY:
			(void) fprintf(stderr, gettext(
			    "%s:  Disabling this adapter "
			    "could result in the isolation of "
			    "a cluster member.\n"), progname);
			(void) fprintf(stderr, gettext(
			    "%s:  Nodes should be shutdown before "
			    "they are isolated.\n"), progname);
			(void) sprintf(errbuff, gettext(
			    "the adapter is busy"));
			break;

		case SCCONF_VP_MISMATCH:
			(void) sprintf(errbuff, gettext(
			    "cluster upgrade is not committed"));
			break;

		case SCCONF_ENOEXIST:
			if (adaptername != NULL && node != NULL) {
				if (vlanid != 0) {
					(void) sprintf(errbuff, gettext(
					    "adapter \"%s\" on node \"%s\" "
					    "with vlanid \"%d\" is not "
					    "found"), adaptername, node,
					    vlanid);
				} else {
					(void) sprintf(errbuff, gettext(
					    "adapter \"%s\" on node \"%s\" "
					    "is not found"), adaptername,
					    node);
				}
			} else {
				(void) sprintf(errbuff, gettext(
				    "adapter is not found"));
			}
			break;

		case SCCONF_TM_EBADOPTS:
			(void) sprintf(errbuff, gettext(
			    "bad or unknown adapter properties given"));
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (node != NULL && adaptername != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to change properties for transport "
			    "adapter (%s:%s) - %s.\n"), progname,
			    node, adaptername, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to change properties for transport "
			    "adapter - %s.\n"), progname, errbuff);
		}
	}

	/* Free memory */
	if (properties)
		free(properties);
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_rm_cltr_adapter
 *
 *	Remove a cluster transport adapter.
 */
static scconf_errno_t
scconfcmd_rm_cltr_adapter(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NAME,			/* 0 - adapter name */
		SUBOPT_NODE,			/* 1 - node name or ID */
		SUBOPT_VLANID,			/* 2 - optional vlanid */
		SUBOPT_VLANID_BAD,		/* 4 - INVALID: vlan_id */
		(char *)0
	};
	char *adaptername = NULL;
	char *node = NULL;
	char *svlanid = NULL;
	int vlanid = 0;
	long tmp_vlanid = 0;
	char *endp;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - adapter name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, adaptername);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			adaptername = value;
			break;

		/* 1 - node name or ID */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, node);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			node = value;
			break;

		/* 2 - vlan ID */
		case 2:
			scconferr = suboption_usage(knownopts[optindex],
			    value, svlanid);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			svlanid = value;

			tmp_vlanid = strtol(value, &endp, 10);
			if (*endp != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s: Invalid value %s given for "
				    "vlanid.\n"), progname, value);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			if ((tmp_vlanid < VLAN_ID_MIN) ||
			    (tmp_vlanid > VLAN_ID_MAX)) {
				(void) fprintf(stderr, gettext(
				    "%s: Value %s given for vlanid "
				    "is out of range.\n"), progname, value);
				scconferr = SCCONF_ERANGE;
				goto cleanup;
			}

			vlanid = (int)tmp_vlanid;
			break;

		/* 3 - Invalid vlan_id */
		case 3:
			(void) fprintf(stderr, gettext(
			    "%s: Use the \"%s\" suboption to specify "
			    "the VLAN ID when you remove an adapter.\n"),
			    progname, SUBOPT_VLANID);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* unknown suboptions */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (adaptername == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for removing a "
		    "transport adapter.\n"), progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
	}
	if (node == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for removing a "
		    "transport adapter.\n"), progname, SUBOPT_NODE);
		scconferr = SCCONF_EUSAGE;
	}
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* remove the adapter */
	scconferr = scconf_rm_cltr_adapter(NULL, node, adaptername, vlanid,
	    messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EINUSE:
			(void) sprintf(errbuff, gettext(
			    "adapter is still cabled or otherwise in use"));
			break;

		case SCCONF_ENOEXIST:
			if (adaptername != NULL && node != NULL) {
				if (vlanid != 0) {
					(void) sprintf(errbuff, gettext(
					    "adapter \"%s\" on node \"%s\" "
					    "with vlanid \"%d\" is not "
					    "found"), adaptername, node,
					    vlanid);
				} else {
					(void) sprintf(errbuff, gettext(
					    "adapter \"%s\" on node \"%s\" "
					    "is not found"), adaptername, node);
				}
			} else {
				(void) sprintf(errbuff, gettext(
				    "adapter is not found"));
			}
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (adaptername != NULL && node != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to remove transport "
			    "adapter (%s:%s) - %s.\n"), progname,
			    node, adaptername, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to remove transport "
			    "adapter - %s.\n"), progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_add_cltr_cpoint
 *
 *	Add an "off-node" cluster transport connection point definition
 *	to the cluster configuration.
 */
static scconf_errno_t
scconfcmd_add_cltr_cpoint(char *subopts, char **messages, uint_t uflag,
    scconf_cltr_handle_t handle)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *last, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_TYPE,			/* 0 - cpoint type */
		SUBOPT_NAME,			/* 1 - cpoint name */
		(char *)0
	};
	char *cpoint_type = NULL;
	char *cpointname = NULL;
	char *properties = NULL;
	uint_t noenable = 1;			/* default is disable */

	/* duplicate subopts and allocate space for properties */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (properties = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	bzero(properties, strlen(properties));

	current = dupopts;
	while (*current) {
		last = current;
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - cpoint type */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, cpoint_type);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			cpoint_type = value;
			break;

		/* 1 - cpoint name */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, cpointname);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			cpointname = value;
			break;

		/* properties */
		case -1:
			if (*properties != '\0')
				(void) strcat(properties, ",");
			(void) strcat(properties, last);
			break;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (cpoint_type == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for adding a "
		    "transport switch.\n"), progname, SUBOPT_TYPE);
		scconferr = SCCONF_EUSAGE;
	}
	if (cpointname == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for adding a "
		    "transport switch.\n"), progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
	}
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

	/* make sure that there is no ':' in the name */
	if (cpointname != NULL && strchr(cpointname, ':')) {
		(void) fprintf(stderr, gettext(
		    "%s:  A switch name may not include "
		    "embedded %s characters.\n"),
		    progname, "':'");
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* add the connection point */
	scconferr = scconf_add_cltr_cpoint(handle, noenable, cpoint_type,
	    cpointname, *properties ? properties : NULL, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EUNKNOWN:
			if (cpoint_type != NULL) {
				(void) sprintf(errbuff, gettext(
				    "unknown transport switch type \"%s\""),
				    cpoint_type);
			} else {
				(void) sprintf(errbuff, gettext(
				    "unknown transport switch type "));
			}
			break;

		case SCCONF_EEXIST:
			(void) sprintf(errbuff, gettext(
			    "transport switch is already configured"));
			break;

		case SCCONF_TM_EBADOPTS:
			(void) sprintf(errbuff, gettext(
			    "bad or unknown switch properties given"));
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (cpointname != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add cluster transport "
			    "switch (%s) - %s.\n"),
			    progname, cpointname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add cluster transport "
			    "switch - %s.\n"), progname, errbuff);
		}
	}

	/* Free memory */
	if (properties)
		free(properties);
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_change_cltr_cpoint
 *
 *	Change an "off-node" cluster transport connection point state
 *	or other properties.
 */
static scconf_errno_t
scconfcmd_change_cltr_cpoint(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value = NULL;
	char *current, *last, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NAME,			/* 0 - cpoint name */
		SUBOPT_STATE,			/* 1 - state */
		SUBOPT_TYPE,			/* 2 - NOT LEGAL property */
		(char *)0
	};
	char *cpointname = NULL;
	char *sstate = NULL;
	scconf_cltr_cpointstate_t state = SCCONF_STATE_UNCHANGED;
	char *properties = NULL;
	uint_t cablechk = 0;

	/* duplicate subopts and allocate space for properties */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (properties = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	bzero(properties, strlen(properties));

	current = dupopts;
	while (*current) {
		last = current;
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - cpoint name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, cpointname);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			cpointname = value;
			break;

		/* 1 - state */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, sstate);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			sstate = value;
			break;

		/* 2 - NOT LEGAL property */
		case 2:
			(void) fprintf(stderr, gettext(
			    "%s:  The \"%s\" transport switch property "
			    "cannot be changed.\n"),
			    progname, knownopts[optindex]);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* properties */
		case -1:
			if (*properties != '\0')
				(void) strcat(properties, ",");
			(void) strcat(properties, last);
			break;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (cpointname == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for changing "
		    "transport switch properties.\n"), progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* set the state */
	if (sstate != NULL) {
		if (value == NULL) {
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		if (strcmp(value, STR_ENABLED) == 0)
			state = SCCONF_STATE_ENABLED;
		else if (strcmp(value, STR_DISABLED) == 0)
			state = SCCONF_STATE_DISABLED;
		else {
			(void) fprintf(stderr, gettext(
			    "%s:  The \"%s\" suboption must be "
			    "set to \"%s\" or \"%s\".\n"),
			    progname, SUBOPT_STATE,
			    STR_ENABLED, STR_DISABLED);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* change connection point properties */
	scconferr = scconf_change_cltr_cpoint(NULL, cpointname,
	    *properties ? properties : NULL, state, cablechk, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EBUSY:
			(void) fprintf(stderr, gettext(
			    "%s:  Disabling this transport switch "
			    "could result in the isolation of "
			    "a cluster member.\n"), progname);
			(void) fprintf(stderr, gettext(
			    "%s:  Nodes should be shutdown before "
			    "they are isolated.\n"), progname);
			(void) sprintf(errbuff, gettext(
			    "the transport switch is busy"));
			break;

		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "switch is unknown"));
			break;

		case SCCONF_TM_EBADOPTS:
			(void) sprintf(errbuff, gettext(
			    "bad or unknown switch properties given"));
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (cpointname != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to change properties for "
			    "transport switch (%s) - %s.\n"),
			    progname, cpointname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to change properties for "
			    "transport switch - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (properties)
		free(properties);
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_rm_cltr_cpoint
 *
 *	Remove an "off-node" cluster transport connection point definition
 *	from the cluster configuration.
 */
static scconf_errno_t
scconfcmd_rm_cltr_cpoint(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NAME,			/* 0 - cpoint name */
		(char *)0
	};
	char *cpointname = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - cpoint name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, cpointname);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			cpointname = value;
			break;

		/* unknown suboptions */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (cpointname == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for removing a "
		    "transport switch.\n"), progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* remove the connection point */
	scconferr = scconf_rm_cltr_cpoint(NULL, cpointname, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EINUSE:
			(void) sprintf(errbuff, gettext(
			    "one or more ports on the switch are still "
			    "cabled or otherwise in use"));
			break;

		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext("switch is unknown"));
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (cpointname != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to remove cluster transport "
			    "switch (%s) - %s.\n"), progname,
			    cpointname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to remove cluster transport "
			    "switch - %s.\n"), progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_add_cltr_cable
 *
 *	Add a cluster transport cable to the cluster configuration.
 */
static scconf_errno_t
scconfcmd_add_cltr_cable(char *subopts, char **messages, uint_t uflag,
    scconf_cltr_handle_t handle)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	scconf_cltr_epoint_t epoint1, epoint2;
	int optindex;

	char *knownopts[] = {
		SUBOPT_ENDPOINT,		/* 0 - cable endpoint */
		SUBOPT_NOENABLE,		/* 1 - noenable flag */
		(char *)0
	};
	char *endpoint1 = NULL;
	char *endpoint2 = NULL;
	uint_t noenable = 0;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - cable endpoint */
		case 0:
			/* check for value, but not already used */
			scconferr = suboption_usage(knownopts[optindex],
			    value, NULL);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* check for already used TWICE */
			if (endpoint1 != NULL && endpoint2 != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  Suboption used more "
				    "than twice - \"%s\".\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			/* set endpoint */
			if (endpoint1 == NULL)
				endpoint1 = value;
			else
				endpoint2 = value;
			break;

		/* 1 - noenable flag */
		case 1:
			noenable = 1;
			break;

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions and initialize endpoints */
	if (endpoint1 == NULL || endpoint2 == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  Two \"%s\" suboptions are required for adding a "
		    "transport cable.\n"), progname, SUBOPT_ENDPOINT);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}
	if (initialize_endpoint(&epoint1, endpoint1)) {
		(void) fprintf(stderr, gettext(
		    "%s:  The first \"%s\" suboption value has a bad "
		    "format.\n"), progname, SUBOPT_ENDPOINT);
		scconferr = SCCONF_EUSAGE;
	}
	if (initialize_endpoint(&epoint2, endpoint2)) {
		(void) fprintf(stderr, gettext(
		    "%s:  The second \"%s\" suboption value has a bad "
		    "format.\n"), progname, SUBOPT_ENDPOINT);
		scconferr = SCCONF_EUSAGE;
	}
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* add the cable */
	scconferr = scconf_add_cltr_cable(handle, noenable, &epoint1, &epoint2,
	    messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EEXIST:
			(void) sprintf(errbuff, gettext(
			    "already configured"));
			break;

		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "a node, adapter, or switch on one of the cable "
			    "endpoints is not found or is unknown"));
			break;

		case SCCONF_EINUSE:
			(void) sprintf(errbuff, gettext(
			    "one of the endpoints is already in use"));
			break;

		case SCCONF_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "invalid cable endpoint specification"));
			break;

		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add cluster transport cable - %s.\n"),
		    progname, errbuff);
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_change_cltr_cable
 *
 *	Change the state of the cable and its two endpoints.
 */
static scconf_errno_t
scconfcmd_change_cltr_cable(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value = NULL;
	char *current, *dupopts = (char *)0;
	scconf_cltr_epoint_t epoint;
	int optindex;

	char *knownopts[] = {
		SUBOPT_ENDPOINT,		/* 0 - cable endpoint */
		SUBOPT_STATE,			/* 1 - state */
		(char *)0
	};
	char *endpoint = NULL;
	char *sstate = NULL;
	scconf_cltr_cpointstate_t state = SCCONF_STATE_UNCHANGED;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - cable endpoint */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, endpoint);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			endpoint = value;
			break;

		/* 1 - state */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, sstate);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			sstate = value;
			break;

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (endpoint == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  An \"%s\" suboption is required for changing "
		    "transport cable state.\n"), progname, SUBOPT_ENDPOINT);
		scconferr = SCCONF_EUSAGE;
	}
	if (sstate == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for changing "
		    "transport cable state.\n"), progname, SUBOPT_STATE);
		scconferr = SCCONF_EUSAGE;
	}
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

	/* initialize endpoint */
	if (initialize_endpoint(&epoint, endpoint)) {
		(void) fprintf(stderr, gettext(
		    "%s:  The \"%s\" suboption value has a bad "
		    "format.\n"), progname, SUBOPT_ENDPOINT);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* set the state */
	if (sstate != NULL) {
		if (value == NULL) {
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		if (strcmp(value, STR_ENABLED) == 0)
			state = SCCONF_STATE_ENABLED;
		else if (strcmp(value, STR_DISABLED) == 0)
			state = SCCONF_STATE_DISABLED;
		else {
			(void) fprintf(stderr, gettext(
			    "%s:  The \"%s\" suboption must be "
			    "set to \"%s\" or \"%s\".\n"),
			    progname, SUBOPT_STATE,
			    STR_ENABLED, STR_DISABLED);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* change the cable state */
	scconferr = scconf_change_cltr_cable(NULL, &epoint, NULL, state,
	    messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EBUSY:
			(void) fprintf(stderr, gettext(
			    "%s:  Disabling this cable "
			    "could result in the isolation of "
			    "a cluster member.\n"), progname);
			(void) fprintf(stderr, gettext(
			    "%s:  Nodes should be shutdown before "
			    "they are isolated.\n"), progname);
			(void) sprintf(errbuff, gettext(
			    "the cable is busy"));
			break;

		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "the cable endpoint is not found or is unknown"));
			break;

		case SCCONF_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "invalid cable endpoint specification"));
			break;

		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to change cluster transport cable "
		    "state - %s.\n"),
		    progname, errbuff);
	}

	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_rm_cltr_cable
 *
 *	Remove a cable.
 */
static scconf_errno_t
scconfcmd_rm_cltr_cable(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	scconf_cltr_epoint_t epoint;
	int optindex;
	int chk_state = 0;

	char *knownopts[] = {
		SUBOPT_ENDPOINT,		/* 0 - cable endpoint */
		(char *)0
	};
	char *endpoint = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - cable endpoint */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, endpoint);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			endpoint = value;
			break;

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (endpoint == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  An \"%s\" suboption is required for removing a "
		    "transport cable.\n"), progname, SUBOPT_ENDPOINT);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* initialize endpoint */
	if (initialize_endpoint(&epoint, endpoint)) {
		(void) fprintf(stderr, gettext(
		    "%s:  The \"%s\" suboption value has a bad "
		    "format.\n"), progname, SUBOPT_ENDPOINT);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* remove the cable */
	scconferr = scconf_rm_cltr_cable(NULL, &epoint, NULL, chk_state,
	    messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EBUSY:
			(void) fprintf(stderr, gettext(
			    "%s:  Removing this cable "
			    "could result in the isolation of "
			    "a cluster member.\n"), progname);
			(void) fprintf(stderr, gettext(
			    "%s:  Nodes should be shutdown before "
			    "they are isolated.\n"), progname);
			(void) sprintf(errbuff, gettext(
			    "the cable is busy"));
			break;

		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "the cable endpoint is not found or is unknown"));
			break;

		case SCCONF_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "invalid cable endpoint specification"));
			break;

		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to remove cluster transport "
		    "cable - %s.\n"), progname, errbuff);
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * initialize_endpoint
 *
 *	Initialize endpoint structure.	The "endpoint" string is modified.
 *
 *	Return zero upon successful.
 */
static int
initialize_endpoint(scconf_cltr_epoint_t *epoint, char *endpoint)
{
	char *node, *name, *port;

	if (epoint == NULL)
		return (1);

	bzero(epoint, sizeof (scconf_cltr_epoint_t));

	/* isolate the node name from the device name */
	name = strchr(endpoint, ':');
	if (name != NULL) {
		node = endpoint;
		*name++ = '\0';
		if (*name == '\0' || strchr(name, ':'))
			return (1);
	} else {
		node = NULL;
		name = endpoint;
	}

	/* isolate the device name from the port */
	port = strchr(name, '@');
	if (port) {
		*port++ = '\0';
		if (*port == '\0' || strchr(port, '@'))
			return (1);
	}

	epoint->scconf_cltr_epoint_type = (node == NULL) ?
	    SCCONF_CLTR_EPOINT_TYPE_CPOINT :
	    SCCONF_CLTR_EPOINT_TYPE_ADAPTER;
	epoint->scconf_cltr_epoint_nodename = node;
	epoint->scconf_cltr_epoint_devicename = name;
	epoint->scconf_cltr_epoint_portname = port;

	return (0);
}

/*
 * scconfcmd_set_netaddr
 *
 *	Set the cluster transport network number.
 */
scconf_errno_t
scconfcmd_set_netaddr(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	struct netent inetent;
	struct netent *netentp;
	char buffer[BUFSIZ];
	int optindex;

	char *knownopts[] = {
		SUBOPT_NETADDR,			/* 0 - net address */
		SUBOPT_NETMASK,			/* 1 - netmask */
		(char *)0
	};
	char *snetaddr = NULL;
	ulong_t netaddr = 0;
	char *snetmask = NULL;
	ulong_t netmask = 0;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - net address */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, snetaddr);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			snetaddr = value;
			break;

		/* 1 - netmask */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, snetmask);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			snetmask = value;
			break;

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (snetaddr == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for setting the "
		    "private network number.\n"), progname, SUBOPT_NETADDR);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* convert the netaddr string to a long;  a network name is okay */
	if ((netaddr = inet_network(snetaddr)) == (ulong_t)-1) {
		netentp = getnetbyname_r(snetaddr, &inetent, buffer,
		    sizeof (buffer));
		if (netentp == (struct netent *)0) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to set the private "
			    "network number - unknown network name.\n"),
			    progname);
			scconferr = SCCONF_ENOEXIST;
			goto cleanup;
		}
		netaddr = netentp->n_net;
	}

	/* if netmask was given */
	if (snetmask != NULL) {

		/* convert the netmask string to a long */
		if ((netmask = inet_network(snetmask)) == (ulong_t)-1) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to set the private "
			    "network number - bad \"%s\" suboption.\n"),
			    progname, SUBOPT_NETMASK);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
	}

	/* set the network number */
	scconferr = scconf_set_netaddr(netaddr, netmask, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "bad or illegal network address or netmask"));
			break;

		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to set the private "
		    "network number - %s.\n"), progname, errbuff);
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_set_privatehostname
 *
 *	Set the private host name for a node or a zone.
 */
static scconf_errno_t
scconfcmd_set_privatehostname(char *subopts, char **messages,
    uint_t nochange, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NODE,			/* 0 - node name or ID */
		SUBOPT_PRIVATEHOSTNAME,		/* 1 - private hostname */
#if SOL_VERSION >= __s10
		SUBOPT_ZPRIVATEHOSTNAME,	/* 2 - zone private hostname */
#endif
		(char *)0
	};

	char *nodename = NULL;
	char *privatehostname = NULL;
	char *zprivatehostname = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - node name or ID */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, nodename);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			nodename = value;
			break;

		/* 1 - private link name */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, privatehostname);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			privatehostname = value;
			break;

#if SOL_VERSION >= __s10
		/* 2 - zone private link name */
		case 2:
			scconferr = suboption_usage(knownopts[optindex],
			    value, zprivatehostname);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			zprivatehostname = value;
			break;
#endif

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (nodename == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for setting the "
		    "private hostname.\n"), progname, SUBOPT_NODE);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

#if SOL_VERSION >= __s10
	/* check for required suboptions */
	if (privatehostname && zprivatehostname) {
		(void) fprintf(stderr, gettext(
		    "%s:  Suboptions \"%s\" and \"%s\" cannot be "
		    "used together.\n"), progname,
		    SUBOPT_PRIVATEHOSTNAME, SUBOPT_ZPRIVATEHOSTNAME);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/*
	 * If nodename is a logical nodename (node:zone), then check
	 * for the required zprivatehostname.
	 */
	if (strchr(nodename, ':')) {
		if (privatehostname) {
			(void) fprintf(stderr, gettext(
			    "%s:  Invalid suboption \"%s\" specified.\n"),
			    progname, SUBOPT_PRIVATEHOSTNAME);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}

		if (zprivatehostname == NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  A \"%s\" suboption is required for setting "
			    "the zone private hostname.\n"),
			    progname, SUBOPT_ZPRIVATEHOSTNAME);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
	} else {
		if (zprivatehostname) {
			(void) fprintf(stderr, gettext(
			    "%s:  Invalid suboption \"%s\" specified.\n"),
			    progname, SUBOPT_ZPRIVATEHOSTNAME);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
	}
#endif

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	if (nochange) {
		/* Add the zone/node private host name */
		if (zprivatehostname) {
			scconferr = scconf_set_privatehostname(nodename,
					zprivatehostname, 1, messages);
		} else {
			scconferr = scconf_set_privatehostname(nodename,
					privatehostname, 1, messages);
		}
	} else {
		/* Change the zone/node private host name */
		if (zprivatehostname) {
			scconferr = scconf_set_privatehostname(nodename,
					zprivatehostname, 0, messages);
		} else {
			scconferr = scconf_set_privatehostname(nodename,
					privatehostname, 0, messages);
		}
	}

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EINUSE:
			if (privatehostname == NULL) {
				(void) sprintf(errbuff, gettext(
				    "the name is already in use"));
			} else {
				(void) sprintf(errbuff, gettext(
				    "\"%s\" is already in use"),
				    privatehostname);
			}
			break;

		case SCCONF_EEXIST:
			(void) sprintf(errbuff, gettext(
			    "\"%s\" is already configured"),
			    nodename);
			break;

		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "\"%s\" is not configured"),
			    nodename);
			break;

		case SCCONF_EINVAL:
			scconferr = SCCONF_EUSAGE;
			break;

		case SCCONF_ESETUP:
			(void) sprintf(errbuff, gettext(
			    "private IP address not available for \"%s\""),
			    nodename);
			break;

		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (nochange) {
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add private hostname - %s.\n"),
		    progname, errbuff);
		} else {
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to change private hostname - %s.\n"),
		    progname, errbuff);
		}
	}

	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_clear_privatehostname
 *
 *	Clear the private host name for a local zone.
 */
static scconf_errno_t
scconfcmd_clear_privatehostname(char *subopts, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NODE,			/* 0 - node name or ID */
		(char *)0
	};
	char *nodename = NULL;
	char *tmp_nodename = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - node name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, nodename);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			nodename = value;
			break;

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* make sure that there is ':' in the node name */
	if (strchr(nodename, ':') == NULL) {
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Save nodename to use it in cleanup */
	tmp_nodename = strdup(nodename);

	/* remove the zone private hostname */
	scconferr = scconf_clear_privatehostname(nodename);

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EINVAL:
			scconferr = SCCONF_EUSAGE;
			break;

		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "\"%s\" is not configured"),
			    tmp_nodename);
			break;

		/*FALLTHRU*/
		default:
			break;
		} /*lint !e788 */
		if (scconferr != SCCONF_ENOEXIST)
			scconf_strerr(errbuff, scconferr);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to remove private hostname - %s.\n"),
		    progname, errbuff);
	}

	/* Free memory */
	if (tmp_nodename)
		free(tmp_nodename);
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_add_quorum_device
 *
 *	Add a quorum device.  Each quorum disk must have at
 *	least two node-to-device paths.  It is not possible to
 *	add or remove individual paths once the device has been
 *	configured.
 *
 *	Note we accept two formats of adding scsi disks:
 *		globaldev=<global_dev_name>[,node=node...]
 *		name=<global_dev_name>,type=scsi,[,node=node...]
 */
static scconf_errno_t
scconfcmd_add_quorum_device(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *last, *dupopts = (char *)0;
	int optindex;
	scconf_name_qdev_t *qd_to_add = NULL;
	scconf_name_qdev_t *curr_qd = NULL;

	char *knownopts[] = {
		SUBOPT_GLOBALDEV,		/* 0 - global device name */
		SUBOPT_NODE,			/* 1 - node name or ID */
		SUBOPT_TYPE,			/* 2 - device type */
		SUBOPT_NAME,			/* 3 - device name */
		SUBOPT_AUTOCONFIG,		/* 4 - autoconfig option */
		SUBOPT_NOOP,			/* 5 - noop option */
		(char *)0
	};
	char *globalname = NULL;
	char *devicename = NULL;
	char *devicetype = NULL;
	char **nodeslist = (char **)0;
	int numnodes = 0;
	char *properties = NULL;
	char *qd_name = NULL;
	char did_path[MAXPATHLEN];
	int auto_flag = 0;
	int noop_flag = 0;
	char buffer[SCCONF_MAXSTRINGLEN];
	did_repl_t repl_status;
	did_repl_list_t repl_data;
	int libinit;

	/* libdid intialisation.  Needed for check_for_repl_device */
	libinit = did_initlibrary(1, DID_NONE, NULL);
	if (libinit != DID_INIT_SUCCESS) {
		scconferr = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* duplicate subopts and allocate space for properties */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (properties = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	bzero(properties, strlen(properties));

	current = dupopts;
	while (*current) {
		last = current;
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - global device name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* check for illegal suboption combinations */
			if (auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for adding quorum device.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\" to add quorum devices.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			globalname = value;
			break;

		/* 1 - node name or ID */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* check for illegal suboption combinations */
			if (auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for adding quorum device.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\" to add quorum devices.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			++numnodes;
			nodeslist = (char **)realloc(nodeslist,
			    ((size_t)numnodes + 1) * sizeof (char *));
			if (nodeslist == (char **)0) {
				scconferr = SCCONF_ENOMEM;
				goto cleanup;
			}
			nodeslist[numnodes] = (char *)0;
			if ((nodeslist[numnodes - 1] = strdup(value))
			    == NULL) {
				scconferr = SCCONF_ENOMEM;
				goto cleanup;
			}
			break;

		/* 2 - device type */
		case 2:
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* check for illegal suboption combinations */
			if (auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for adding quorum device.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\" to add quorum devices.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			if ((devicetype = strdup(value)) == NULL) {
				scconferr = SCCONF_ENOMEM;
				goto cleanup;
			}
			break;

		/* 3 - device name */
		case 3:
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* check for illegal suboption combinations */
			if (auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for adding quorum device.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\" to add quorum devices.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			devicename = value;
			break;

		/* 4 - autoconfig option */
		case 4:
			/* check for illegal value */
			if (value != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A value cannot be given with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			/* check for illegal suboption combinations */
			if (nodeslist != NULL || devicename != NULL ||
			    globalname != NULL || devicetype != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for adding quorum device.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\" or \"%s\" to add quorum "
				    "devices.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_NODE, SUBOPT_GLOBALDEV);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			auto_flag = 1;
			break;

		/* 5 - noop option */
		case 5:
			/* check for illegal value */
			if (value != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A value cannot be given with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			noop_flag = 1;
			break;

		/* properties */
		case -1:
			if (*properties != '\0')
				(void) strcat(properties, ",");
			(void) strcat(properties, last);
			break;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (auto_flag == 0) {
		if (globalname == NULL) {
			if (devicename == NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A suboption \"%s\" or \"%s\" or "
				    "\"%s\" is required for adding a quorum "
				    "device.\n"), progname,
				    SUBOPT_GLOBALDEV, SUBOPT_NAME,
				    SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			if ((qd_name = strdup(devicename)) == NULL) {
				scconferr = SCCONF_ENOMEM;
				goto cleanup;
			}
			if (devicetype == NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A suboption \"%s\" is required with "
				    "\"%s\" for adding a quorum device.\n"),
				    progname, SUBOPT_TYPE, SUBOPT_NAME);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
		} else {
			if (devicename != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  Suboption \"%s\" and \"%s\" cannot "
				    "be used together for adding a quorum "
				    "device.\n"), progname, SUBOPT_GLOBALDEV,
				    SUBOPT_NAME);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			if ((qd_name = strdup(globalname)) == NULL) {
				scconferr = SCCONF_ENOMEM;
				goto cleanup;
			}
			if (devicetype != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  The suboption \"%s\" is unknown with "
				    " \"%s\". \n"), progname,
				    SUBOPT_TYPE, SUBOPT_GLOBALDEV);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			} else if ((devicetype = strdup("scsi")) == NULL) {
				scconferr = SCCONF_ENOMEM;
				goto cleanup;
			}
		}
	}

	if ((qd_name == NULL) && (auto_flag == 0)) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\" or \"%s\" or \"%s\" is required "
		    "for adding a quorum device.\n"), progname,
		    SUBOPT_GLOBALDEV, SUBOPT_NAME, SUBOPT_AUTOCONFIG);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* don't allow just one node */
	if (numnodes == 1) {
		(void) fprintf(stderr, gettext(
		    "%s:  Each quorum device must be ported to at least "
		    "two nodes.\n"), progname);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/*
	 * noop can be given only with autoconfig and we already verified
	 * that autoconfig was not given with any other suboption.
	 */
	if (noop_flag && !auto_flag) {
		(void) fprintf(stderr, gettext(
		    "%s:  The \"%s\" suboption can be provided with only "
		    "\"%s\" suboption and no other suboption while "
		    "adding a quorum device.\n"), progname,
		    SUBOPT_NOOP, SUBOPT_AUTOCONFIG);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/*
	 * Quorum device cannot be a replicated device
	 */

	/*lint -save -e668 : devicetype will not be null here */
	if (!auto_flag && qd_name != NULL && strcmp(devicetype, "scsi") == 0) {
		if (scconf_didname(qd_name, did_path)) {
			scconferr = SCCONF_EINVAL;
			goto cleanup;
		}

		/*lint -restore */
		repl_status = check_for_repl_device(did_path, &repl_data);
		switch (repl_status) {
		case DID_REPL_TRUE:
			/* Replicated device */
			(void) fprintf(stderr, gettext(
			    "%s: \"%s\" is a replicated device. Replicated "
			    "devices cannot be configured as quorum"
			    "devices.\n"), progname, qd_name);
			    scconferr = SCCONF_EINVAL;
			goto cleanup;

		case DID_REPL_FALSE:
			/* Non-replicated device */
			break;

		case DID_REPL_BAD_DEV:
			/* non-existent did device */
			scconferr = SCCONF_ENOEXIST;
			goto cleanup;

		case DID_REPL_ERROR:
		default:
			/* internal error */
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}


	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* add the quorum device or do automatic configuration */
	if (!auto_flag) {
		scconferr = scconf_add_quorum_device(qd_name, nodeslist,
		    devicetype, *properties ? properties : NULL,
		    messages);
		if (scconferr != SCCONF_NOERR)
			goto cleanup;
	} else {
		scconferr = scconf_get_optimal_quorum_config(&qd_to_add,
		    (scconf_name_qdev_t **)NULL, QUORUM_ADD_OP, messages);
		if (scconferr != SCCONF_NOERR)
			goto cleanup;

		curr_qd = qd_to_add;

		if (messages != (char **)0) {
			scconf_addmessage(dgettext(TEXT_DOMAIN,
			    "Will attempt to add the following devices "
			    "as quorum devices:\n"), messages);
			while (curr_qd) {
				(void) sprintf(buffer,
				    dgettext(TEXT_DOMAIN,
				    "/dev/did/rdsk/%.256ss2\n"),
				    curr_qd->scconf_name_qdevname);
				scconf_addmessage(buffer, messages);
				curr_qd = curr_qd->scconf_name_qdevnext;
			}
		}

		curr_qd = qd_to_add;

		if (noop_flag)
			goto cleanup;

		while (curr_qd) {
			scconferr = scconf_add_quorum_device(
			    curr_qd->scconf_name_qdevname, (char **)0, "scsi",
			    NULL, messages);
			if (scconferr != SCCONF_NOERR) {
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Attempt to add device "
					    "/dev/did/rdsk/%.256ss2 as "
					    "quorum device failed, "
					    "aborting autoconfig.\n"),
					    curr_qd->scconf_name_qdevname);
					scconf_addmessage(buffer, messages);
				}
				goto cleanup;
			} else {
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Attempt to add device "
					    "/dev/did/rdsk/%.256ss2 as "
					    "quorum device succeeded.\n"),
					    curr_qd->scconf_name_qdevname);
					scconf_addmessage(buffer, messages);
				}
			}
			curr_qd = curr_qd->scconf_name_qdevnext;
		}
	}

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EEXIST:
			(void) sprintf(errbuff, gettext(
			    "device is already configured"));
			break;

		case SCCONF_ENOEXIST:
			if (numnodes) {
				(void) sprintf(errbuff, gettext(
				    "global device is not found or is "
				    "not connected to node(s)"));
			} else {
				(void) sprintf(errbuff, gettext(
				    "global device is not found"));
			}
			break;

		case SCCONF_EIO:
			(void) sprintf(errbuff, gettext(
			    "unable to scrub the device"));
			break;

		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		case SCCONF_EQUORUM:
		case SCCONF_EBADVALUE:
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (qd_name != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add quorum device (%s) - %s.\n"),
			    progname, qd_name, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add quorum device - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);
	if (properties)
		free(properties);
	if (devicetype)
		free(devicetype);
	if (qd_name)
		free(qd_name);

	if (nodeslist) {
		char **tmp;
		for (tmp = nodeslist;  *tmp;  tmp++)
			free(*tmp);
		free(nodeslist);
	}

	if (qd_to_add)
		scconf_free_name_qdevlist(qd_to_add);

	return (scconferr);
}

/*
 * scconfcmd_change_quorum_config
 *
 *	Change the quorum configuration in one of the following ways:
 *
 *		- put a node into maintenance state
 *		- reset the vote count for a node
 *		- put a quorum device into maintenance state
 *		- reset the vote count for a quorum device
 *		- reset vote counts for all nodes and quorum devices;
 *		   also, resets "installmode".
 *		- set the "installmode" flag.
 *		- clear the "installmode" flag.
 *		- set the defaultvote for a node (undocumented)
 *		- change the properties of a NAS quorum
 *		- Do automated quorum configuration
 *
 *	Two formats are supported:
 *		globaldev=<device_name>
 *		name=<device_name>
 */
static scconf_errno_t
scconfcmd_change_quorum_config(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *last, *dupopts = (char *)0;
	uint_t installflag = 0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_GLOBALDEV,		/* 0 - global device name */
		SUBOPT_NODE,			/* 1 - node name or ID */
		SUBOPT_MAINTSTATE,		/* 2 - maintenance state */
		SUBOPT_RESET,			/* 3 - reset */
		SUBOPT_INSTALLMODE,		/* 4 - install mode */
		SUBOPT_INSTALLMODEOFF,		/* 5 - install mode */
		SUBOPT_IFNOTINSTALLMODE,	/* 6 - if not install mode */
		SUBOPT_DEFAULTVOTE,		/* 7 - set defaultvote */
		SUBOPT_NAME,			/* 8 - device name */
		SUBOPT_AUTOCONFIG,		/* 9 - autoconfig option */
		SUBOPT_NOOP,			/* 10 - noop option */
		(char *)0
	};
	char *globalname = NULL;
	char *devicename = NULL;
	char *nodename = NULL;
	uint_t maintstate = 0;
	uint_t reset = 0;
	uint_t installmode = 0;
	uint_t installmodeoff = 0;
	uint_t ifnotinstallmode = 0;
	uint_t set_properties = 0;
	char *defaultvote = NULL;
	uint_t votenumber = 0;
	char *endp;
	char *properties = NULL;
	char *qd_name = NULL;
	int auto_flag = 0;
	int noop_flag = 0;
	scconf_name_qdev_t *qd_to_add = NULL, *qd_to_del = NULL;
	scconf_name_qdev_t *curr_qd;
	char buffer[SCCONF_MAXSTRINGLEN];

	/* duplicate suboptions and allocate space for properties */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (properties = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	bzero(properties, strlen(properties));

	current = dupopts;
	while (*current) {
		last = current;
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - global device name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* check for illegal suboption combinations */
			if (nodename != NULL ||
			    installmode || installmodeoff ||
			    ifnotinstallmode || auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for changing quorum state.\n"),
				    progname);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			globalname = value;
			if ((globalname != NULL) && (devicename != NULL)) {
				(void) fprintf(stderr, gettext(
				    "%s:  Suboption \"%s\" and \"%s\" cannot "
				    "be used together for changing a quorum "
				    "device.\n"), progname,
				    SUBOPT_GLOBALDEV, SUBOPT_NAME);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			if (globalname != NULL) {
				if ((qd_name = strdup(globalname)) == NULL) {
					scconferr = SCCONF_ENOMEM;
					goto cleanup;
				}
			}
			break;

		/* 1 - node name or ID */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, nodename);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* check for illegal suboption combinations */
			if (qd_name != NULL || installmode ||
			    installmodeoff || auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for changing quorum state.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\", \"%s\", \"%s\", \"%s\", or "
				    "\"%s\" to change quorum state.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_GLOBALDEV, SUBOPT_NAME,
				    SUBOPT_INSTALLMODE, SUBOPT_INSTALLMODEOFF,
				    SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			nodename = value;
			break;

		/* 2 - maintenance state */
		case 2:
			/* check for illegal value */
			if (value != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A value cannot be given with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			/* check for illegal suboption combinations */
			if (reset || installmode || installmodeoff ||
			    ifnotinstallmode || auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for changing quorum state.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\", \"%s\", \"%s\", \"%s\", "
				    "or \"%s\" to change quorum state.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_RESET, SUBOPT_INSTALLMODE,
				    SUBOPT_INSTALLMODEOFF,
				    SUBOPT_IFNOTINSTALLMODE, SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			++maintstate;
			break;

		/* 3 - reset */
		case 3:
			/* check for illegal value */
			if (value != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A value cannot be given with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			/* check for illegal suboption combinations */
			if (maintstate || installmode || installmodeoff ||
			    auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for changing quorum state.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\", \"%s\", \"%s\", or \"%s\" to "
				    "change quorum state.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_MAINTSTATE, SUBOPT_INSTALLMODE,
				    SUBOPT_INSTALLMODEOFF, SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			++reset;
			break;

		/* 4 - install mode */
		case 4:
			/* check for illegal value */
			if (value != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A value cannot be given with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			/* check for illegal suboption combinations */
			if (nodename != NULL || qd_name != NULL ||
			    maintstate || reset || installmodeoff ||
			    ifnotinstallmode || auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for changing quorum state.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\", \"%s\", \"%s\", \"%s\", "
				    "\"%s\", \"%s\", \"%s\", or \"%s\" to "
				    "change quorum state.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_NODE, SUBOPT_GLOBALDEV,
				    SUBOPT_NAME, SUBOPT_MAINTSTATE,
				    SUBOPT_RESET, SUBOPT_INSTALLMODEOFF,
				    SUBOPT_IFNOTINSTALLMODE, SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			++installmode;
			break;

		/* 5 - install mode off */
		case 5:
			/* check for illegal value */
			if (value != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A value cannot be given with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			/* check for illegal suboption combinations */
			if (nodename != NULL || qd_name != NULL ||
			    maintstate || reset || installmode ||
			    ifnotinstallmode || auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for changing quorum state.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\", \"%s\", \"%s\", \"%s\", "
				    "\"%s\", \"%s\", \"%s\", or \"%s\" to "
				    "change quorum state.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_NODE, SUBOPT_GLOBALDEV,
				    SUBOPT_NAME, SUBOPT_MAINTSTATE,
				    SUBOPT_RESET, SUBOPT_INSTALLMODE,
				    SUBOPT_IFNOTINSTALLMODE, SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			++installmodeoff;
			break;

		/* 6 - if not install mode */
		case 6:
			/* check for illegal value */
			if (value != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A value cannot be given with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			/* check for illegal suboption combinations */
			if (qd_name != NULL || maintstate ||
			    installmode || installmodeoff || auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for changing quorum state.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption "
				    "\"%s\" with \"%s\", \"%s\", \"%s\", "
				    "\"%s\", \"%s\" or \"%s\" to change quorum "
				    "state.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_GLOBALDEV, SUBOPT_NAME,
				    SUBOPT_MAINTSTATE, SUBOPT_INSTALLMODE,
				    SUBOPT_INSTALLMODEOFF, SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			++ifnotinstallmode;
			break;

		/* 7 - Setting defaultvote for a node */
		case 7:
			/* check for illegal value */
			if (value == NULL) {
				(void) fprintf(stderr, gettext(
				    "%s: Must give a value with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			/* check for illegal suboption combinations */
			if (qd_name != NULL || maintstate ||
			    reset || installmode ||
			    installmodeoff || ifnotinstallmode || auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s: Bad combination of suboptions given "
					"for changing quorum state.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\", \"%s\", \"%s\", \"%s\", "
				    "\"%s\", \"%s\", \"%s\", or \"%s\" to "
				    "change quorum state.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_GLOBALDEV, SUBOPT_NAME,
				    SUBOPT_RESET, SUBOPT_MAINTSTATE,
				    SUBOPT_INSTALLMODE,
				    SUBOPT_INSTALLMODEOFF,
				    SUBOPT_IFNOTINSTALLMODE, SUBOPT_AUTOCONFIG);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			++defaultvote;
			votenumber = (uint_t)strtol(value, &endp, 10);
			if (*endp != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s: Invalid string %s given for "
				    "defaultvote.\n"), progname, value);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			break;

		/* 8 - device name */
		case 8:
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* check for illegal suboption combinations */
			if (nodename != NULL ||
			    installmode || installmodeoff ||
			    ifnotinstallmode || auto_flag) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for changing quorum state.\n"),
				    progname);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			devicename = value;
			if ((globalname != NULL) && (devicename != NULL)) {
				(void) fprintf(stderr, gettext(
				    "%s:  Suboption \"%s\" and \"%s\" cannot "
				    "be used together for changing a quorum "
				    "device.\n"), progname,
				    SUBOPT_GLOBALDEV, SUBOPT_NAME);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			if (devicename != NULL) {
				if ((qd_name = strdup(devicename)) == NULL) {
					scconferr = SCCONF_ENOMEM;
					goto cleanup;
				}
			}
			break;

		/* 9 - automated quorum configuration */
		case 9:
			/* check for illegal value */
			if (value != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A value cannot be given with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			/* check for illegal suboption combinations */
			if (nodename != NULL || qd_name != NULL ||
			    maintstate || reset || installmode ||
			    ifnotinstallmode || installmodeoff || devicename) {
				(void) fprintf(stderr, gettext(
				    "%s:  Bad combination of suboptions given "
				    "for changing quorum configuration.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  Cannot combine suboption \"%s\" "
				    "with \"%s\", \"%s\", \"%s\", \"%s\", "
				    "\"%s\", \"%s\", or \"%s\" to change "
				    "quorum configuration.\n"),
				    progname, knownopts[optindex],
				    SUBOPT_NODE, SUBOPT_GLOBALDEV,
				    SUBOPT_MAINTSTATE, SUBOPT_RESET,
				    SUBOPT_INSTALLMODE,
				    SUBOPT_IFNOTINSTALLMODE,
				    SUBOPT_INSTALLMODEOFF, SUBOPT_NAME);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			auto_flag++;
			break;

		/* 10 - noop flag */
		case 10:
			/* check for illegal value */
			if (value != NULL) {
				(void) fprintf(stderr, gettext(
				    "%s:  A value cannot be given with the "
				    "\"%s\" suboption.\n"),
				    progname, knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			noop_flag++;
			break;

		/* properties */
		case -1:
			if (*properties != '\0')
				(void) strcat(properties, ",");
			(void) strcat(properties, last);
			++set_properties;
			break;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/*
	 * Check suboptions
	 *
	 *	- one of the six flags (maintstate, reset, installmode,
	 *		installmodeoff, defaultvote, auto_flag) must be given
	 *	- and, the maintstate flag can only be used with
	 *	    nodes and devices
	 *	- the reset flag may be used alone or with nodes or devices
	 *	- the defaultvote flag can only be used with nodes
	 *
	 *   Above, we have already verified that ...
	 *
	 *	- the four flags (mainstate, reset, installmode, defaultvote)
	 *	    are mutually exclusive and may never be given together
	 *	- "node" and "globaldev" are never given together
	 *	- the ifnotinstallmode flag can only be used to reset nodes
	 *	- the installmode flag always stands by itself
	 * 	- the auto_flag always stands by itself
	 */
	if (!maintstate && !reset && !installmode && !installmodeoff &&
	    !defaultvote && !set_properties && !auto_flag) {
		(void) fprintf(stderr, gettext(
		    "%s:  \"%s\", \"%s\", \"%s\", \"%s\", or \"%s\" must be "
		    "used to control changes to the quorum state. For changing "
		    "properties, one or more properties must be "
		    "specified..\n"), progname, SUBOPT_MAINTSTATE,
		    SUBOPT_RESET, SUBOPT_INSTALLMODE, SUBOPT_INSTALLMODEOFF,
		    SUBOPT_AUTOCONFIG);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}
	if (maintstate && nodename == NULL && qd_name == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  Either a \"%s\", \"%s\", or \"%s\" suboption "
		    "must be given to set quorum maintenance state.\n"),
		    progname, SUBOPT_NODE, SUBOPT_NAME, SUBOPT_GLOBALDEV);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}
	if (set_properties && qd_name == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  \"%s\" must be given to change quorum device "
		    "properties, or unknown properties are given.\n"),
		    progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/*
	 * The noop flag can be specified with only autoconfig suboption,
	 * and we already verified that autoconfig can not be specified with
	 * any other suboption.
	 */
	if (noop_flag && !auto_flag) {
		(void) fprintf(stderr, gettext(
		    "%s:  The \"%s\" suboption can be provided with only "
		    "\"%s\" suboption and no other suboption while "
		    "changing quorum configuration.\n"), progname,
		    SUBOPT_NOOP, SUBOPT_AUTOCONFIG);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	if (qd_name != NULL) {
		if (maintstate) {
			scconferr = scconf_maintstate_quorum_device(qd_name);
			if (scconferr != SCCONF_NOERR) {
				switch (scconferr) {
				case SCCONF_ENOEXIST:
					(void) sprintf(errbuff, gettext(
					    "quorum device is not found"));
					break;

				case SCCONF_EINVAL:
				case SCCONF_ESETUP:
				case SCCONF_ENOCLUSTER:
					scconferr = SCCONF_EUNEXPECTED;

				/*FALLTHRU*/
				case SCCONF_EQUORUM:
				case SCCONF_EBADVALUE:
				default:
					scconf_strerr(errbuff, scconferr);
					break;
				} /*lint !e788 */
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to set maintstate for "
				    "quorum device (%s) - %s.\n"),
				    progname, qd_name, errbuff);
				goto cleanup2;
			}
		} else if (reset) {
			scconferr = scconf_reset_quorum_device(qd_name);
			if (scconferr != SCCONF_NOERR) {
				switch (scconferr) {
				case SCCONF_ENOEXIST:
					(void) sprintf(errbuff, gettext(
					    "quorum device is not found"));
					break;

				case SCCONF_EINVAL:
				case SCCONF_ESETUP:
				case SCCONF_ENOCLUSTER:
					scconferr = SCCONF_EUNEXPECTED;

				/*FALLTHRU*/
				case SCCONF_EQUORUM:
				case SCCONF_EBADVALUE:
				default:
					scconf_strerr(errbuff, scconferr);
					break;
				} /*lint !e788 */
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to reset "
				    "quorum device (%s) - %s.\n"),
				    progname, qd_name, errbuff);
				goto cleanup2;
			}
		} else if (set_properties) {
			scconferr = scconf_set_qd_properties(qd_name,
			    properties, messages);
			if (scconferr != SCCONF_NOERR) {
				switch (scconferr) {
				case SCCONF_ENOEXIST:
					(void) sprintf(errbuff, gettext(
					    "quorum device is not found"));
					break;

				case SCCONF_ENOMEM:
				case SCCONF_EUNKNOWN:
					scconferr = SCCONF_EUNEXPECTED;
					break;

				case SCCONF_EINVAL:
				case SCCONF_EPERM:
				default:
					scconf_strerr(errbuff, scconferr);
					break;
				} /*lint !e788 */
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to change properties "
				    "of quorum device (%s) - %s.\n"),
				    progname, qd_name, errbuff);
				goto cleanup2;
			}
		} else {
			/* internal error */
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	} else if (nodename != NULL) {
		if (maintstate) {
			scconferr = scconf_maintstate_quorum_node(nodename);
			if (scconferr != SCCONF_NOERR) {
				switch (scconferr) {
				case SCCONF_ENOEXIST:
					(void) sprintf(errbuff, gettext(
					    "node \"%s\" is unknown"),
					    nodename);
					break;

				case SCCONF_EBUSY:
					(void) fprintf(stderr, gettext(
					    "%s:  A node must be shutdown "
					    "before it\'s vote count can "
					    "be set to zero.\n"),
					    progname);
					(void) sprintf(errbuff, gettext(
					    "node is an active cluster "
					    "member"));
					break;

				case SCCONF_EINVAL:
				case SCCONF_ESETUP:
				case SCCONF_ENOCLUSTER:
				case SCCONF_EINUSE:
					scconferr = SCCONF_EUNEXPECTED;

				/*FALLTHRU*/
				case SCCONF_EQUORUM:
				case SCCONF_EBADVALUE:
				default:
					scconf_strerr(errbuff, scconferr);
					break;
				} /*lint !e788 */
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to set quorum maintstate for "
				    "node (%s) - %s.\n"),
				    progname, nodename, errbuff);
				goto cleanup2;
			}
		} else if (reset) {
			if (ifnotinstallmode) {
				scconferr = scconf_get_quorum_installflag(
				    &installflag);
				if (scconferr != SCCONF_NOERR) {
					scconf_strerr(errbuff, scconferr);
					(void) fprintf(stderr, gettext(
					    "%s:  Unable to check "
					    "install mode - %s"),
					    progname, errbuff);
					(void) fprintf(stderr, gettext(
					    "%s:  Failed to reset "
					    "quorum state for node (%s).\n"),
					    progname, nodename);
					goto cleanup2;
				}

				/* if install mode is enabled, we are done */
				if (installflag)
					goto cleanup;
			}
			scconferr = scconf_reset_quorum_node(nodename);
			if (scconferr != SCCONF_NOERR) {
				switch (scconferr) {
				case SCCONF_ENOEXIST:
					(void) sprintf(errbuff, gettext(
					    "node \"%s\" is unknown"),
					    nodename);
					break;
				case SCCONF_EINUSE:
					(void) sprintf(errbuff, gettext(
					    "conflicting operation, "
					    "try again later"));
					break;

				case SCCONF_EINVAL:
				case SCCONF_ESETUP:
				case SCCONF_ENOCLUSTER:
					scconferr = SCCONF_EUNEXPECTED;

				/*FALLTHRU*/
				case SCCONF_EQUORUM:
				case SCCONF_EBADVALUE:
				default:
					scconf_strerr(errbuff, scconferr);
					break;
				} /*lint !e788 */
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to reset quorum state for "
				    "node (%s) - %s.\n"),
				    progname, nodename, errbuff);
				goto cleanup2;
			}
		} else if (defaultvote) {
			scconferr = scconf_set_node_defaultvote(nodename,
			    votenumber);
			if (scconferr != SCCONF_NOERR) {
				switch (scconferr) {
				case SCCONF_ENOEXIST:
					(void) sprintf(errbuff, gettext(
					    "node \"%s\" is unknown"),
					    nodename);
					break;
				case SCCONF_EINUSE:
					(void) sprintf(errbuff, gettext(
					    "conflicting operation, "
					    "try again later"));
					break;
				case SCCONF_EINVAL:
					(void) sprintf(errbuff, gettext(
					    "vote value out of range"));
					break;

				case SCCONF_ESETUP:
				case SCCONF_ENOCLUSTER:
					scconferr = SCCONF_EUNEXPECTED;

				/*FALLTHRU*/
				case SCCONF_EQUORUM:
				case SCCONF_EBADVALUE:
				default:
					scconf_strerr(errbuff, scconferr);
					break;
				} /*lint !e788 */
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to change quorum defaultvote "
				    "for node (%s) - %s.\n"),
				    progname, nodename, errbuff);
			}
		} else {
			/* internal error */
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	} else if (installmode) {
		scconferr = scconf_set_quorum_installflag();
		if (scconferr != SCCONF_NOERR) {
			switch (scconferr) {
			case SCCONF_EINVAL:
			case SCCONF_ESETUP:
			case SCCONF_ENOCLUSTER:
				scconferr = SCCONF_EUNEXPECTED;

			/*FALLTHRU*/
			default:
				scconf_strerr(errbuff, scconferr);
				break;
			} /*lint !e788 */
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to set install mode - %s.\n"),
			    progname, errbuff);
			goto cleanup2;
		}
	} else if (installmodeoff) {
		scconferr = scconf_clear_quorum_installflag();
		if (scconferr != SCCONF_NOERR) {
			switch (scconferr) {
			case SCCONF_EINVAL:
			case SCCONF_ESETUP:
			case SCCONF_ENOCLUSTER:
				scconferr = SCCONF_EUNEXPECTED;

			/*FALLTHRU*/
			default:
				scconf_strerr(errbuff, scconferr);
				break;
			} /*lint !e788 */
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to clear install mode - %s.\n"),
			    progname, errbuff);
			goto cleanup2;
		}
	} else if (reset) {
		scconferr = scconf_reset_quorum();
		if (scconferr != SCCONF_NOERR) {
			switch (scconferr) {
			case SCCONF_EQUORUM:
				scconf_strerr(errbuff, scconferr);
				(void) fprintf(stderr, gettext(
				    "%s:  Minimum node and quorum device "
				    "requirements must be met before the "
				    "quorum state can be reset.\n"),
				    progname);
				(void) fprintf(stderr, gettext(
				    "%s:  You may need to add a quorum "
				    "device, or check the state of quorum "
				    "devices, before attempting a reset.\n"),
				    progname);
				break;

			case SCCONF_EINVAL:
			case SCCONF_ESETUP:
			case SCCONF_ENOCLUSTER:
				scconferr = SCCONF_EUNEXPECTED;

			/*FALLTHRU*/
			case SCCONF_EBADVALUE:
			default:
				scconf_strerr(errbuff, scconferr);
				break;
			} /*lint !e788 */
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to reset quorum state - %s.\n"),
			    progname, errbuff);
			goto cleanup2;
		}
	} else if (auto_flag) {
		scconferr = scconf_get_optimal_quorum_config(&qd_to_add,
		    &qd_to_del, QUORUM_CHANGE_OP, messages);
		if (scconferr != SCCONF_NOERR)
			goto cleanup;

		curr_qd = qd_to_add;

		if (messages != (char **)0) {
			if (curr_qd) {
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "Will attempt to add the following devices "
				    "as quorum devices:\n"), messages);
				while (curr_qd) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "/dev/did/rdsk/%.256ss2\n"),
					    curr_qd->scconf_name_qdevname);
					scconf_addmessage(buffer, messages);
					curr_qd = curr_qd->scconf_name_qdevnext;
				}
			} else {
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "No device to add as quorum device to the "
				    "current configuration.\n"), messages);
			}
		}

		curr_qd = qd_to_del;

		if (messages != (char **)0) {
			if (curr_qd) {
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "Will attempt to remove the following "
				    "devices as quorum devices:\n"), messages);
				while (curr_qd) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "/dev/did/rdsk/%.256ss2\n"),
					    curr_qd->scconf_name_qdevname);
					scconf_addmessage(buffer, messages);
					curr_qd = curr_qd->scconf_name_qdevnext;
				}
			} else {
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "No device to remove as quorum device from "
				    "current configuration.\n"), messages);
			}
		}

		if (noop_flag)
			goto cleanup;

		curr_qd = qd_to_add;

		while (curr_qd) {
			scconferr = scconf_add_quorum_device(
			    curr_qd->scconf_name_qdevname, (char **)0, "scsi",
			    NULL, messages);
			if (scconferr != SCCONF_NOERR) {
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Attempt to add device "
					    "/dev/did/rdsk/%.256ss2 as "
					    "quorum device failed, "
					    "aborting autoconfig.\n"),
					    curr_qd->scconf_name_qdevname);
					scconf_addmessage(buffer, messages);
				}
				goto cleanup;
			} else {
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Attempt to add device "
					    "/dev/did/rdsk/%.256ss2 as quorum "
					    "device succeeded.\n"),
					    curr_qd->scconf_name_qdevname);
					scconf_addmessage(buffer, messages);
				}
			}
			curr_qd = curr_qd->scconf_name_qdevnext;
		}

		curr_qd = qd_to_del;
		while (curr_qd) {
			scconferr = scconf_rm_quorum_device(
			    curr_qd->scconf_name_qdevname, 0, messages);
			if (scconferr != SCCONF_NOERR) {
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Attempt to remove device "
					    "/dev/did/rdsk/%.256ss2 as "
					    "quorum device failed, "
					    "aborting autoconfig.\n"),
					    curr_qd->scconf_name_qdevname);
					scconf_addmessage(buffer, messages);
				}
				goto cleanup;
			} else {
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Attempt to remove device "
					    "/dev/did/rdsk/%.256ss2 as quorum "
					    "device succeeded.\n"),
					    curr_qd->scconf_name_qdevname);

					scconf_addmessage(buffer, messages);
				}
			}
			curr_qd = curr_qd->scconf_name_qdevnext;
		}
	} else {
		/* internal error */
		scconferr = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to change quorum state - %s.\n"),
		    progname, errbuff);
	}

cleanup2:
	/* Free memory */
	if (dupopts)
		free(dupopts);
	if (properties)
		free(properties);
	if (qd_name)
		free(qd_name);

	if (qd_to_add)
		scconf_free_name_qdevlist(qd_to_add);

	if (qd_to_del)
		scconf_free_name_qdevlist(qd_to_del);

	return (scconferr);
}

/*
 * scconfcmd_rm_quorum_device
 *
 *	Remove a quorum device.
 *
 *	Two formats are supported:
 *		globaldev=<device_name>
 *		name=<device_name>
 */
static scconf_errno_t
scconfcmd_rm_quorum_device(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_GLOBALDEV,		/* 0 - global device name */
		SUBOPT_NAME,			/* 1 - device name */
		(char *)0
	};
	char *devicename = NULL;
	char *globalname = NULL;
	char *qd_name = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - global device name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			globalname = value;
			break;

		/* 1 - device name */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			devicename = value;
			break;

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if ((devicename != NULL) && (globalname != NULL)) {
		(void) fprintf(stderr, gettext(
		    "%s:  Suboption \"%s\" and \"%s\" cannot be used "
		    "together for removing a quorum device.\n"),
		    progname, SUBOPT_GLOBALDEV, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	if (globalname != NULL) {
		if ((qd_name = strdup(globalname)) == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}
	} else if (devicename != NULL) {
		if ((qd_name = strdup(devicename)) == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	if (qd_name == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for removing a "
		    "quorum device.\n"), progname, SUBOPT_GLOBALDEV);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* remove quorum device */
	scconferr = scconf_rm_quorum_device(qd_name, 0, messages);
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "quorum device is not found"));
			break;

		case SCCONF_EQUORUM:
			scconf_strerr(errbuff, scconferr);
			(void) fprintf(stderr, gettext(
			    "%s:  Make sure you are not attempting "
			    "to remove the last required quorum device.\n"),
			    progname);
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		case SCCONF_EBADVALUE:
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (qd_name != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to remove quorum device (%s) - %s.\n"),
			    progname, qd_name, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to remove quorum device - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);
	if (qd_name)
		free(qd_name);

	return (scconferr);
}

/*
 * Convert an array of nodeids to an array of nodenames.
 */
static scconf_errno_t
get_nodenames(char *nodelist, char ***out_dsnodes)
{
	char *tmp;
	int numnodes;
	char **dsnodes = NULL;
	char **curr;
	int i, j;
	nodeid_t nodeid;

	if (nodelist == NULL) {
		*out_dsnodes = dsnodes;
		return (SCCONF_NOERR);
	}

	/* count the number of nodes and allocate memory for dsnodes */
	numnodes = 0;
	for (tmp = nodelist; tmp; tmp = strchr(tmp + 1, ':'))
		++numnodes;

	dsnodes = (char **)calloc((size_t)numnodes + 1, sizeof (char *));
	if (dsnodes == (char **)0) {
		return (SCCONF_ENOMEM);
	}

	/* Terminate the nodelist with a NULL */
	dsnodes[numnodes] = NULL;

	/* Fill out dsnodes from the nodelist */
	curr = dsnodes;
	*curr = strtok(nodelist, ":");
	/*lint -save -e796 -e797 */
	while (*curr)
		*(++curr) = strtok(NULL, ":");

	*out_dsnodes = dsnodes;

	/* Look for duplicate nodes */
	for (i = 0; dsnodes[i] != NULL; ++i) {

		/* do some basic checking on the node name */
		if (scconf_get_nodeid(dsnodes[i], &nodeid) != SCCONF_NOERR) {
			free(dsnodes);
			return (SCCONF_EINVAL);
		}

		for (j = i + 1; dsnodes[j] != NULL;  ++j) {
			if (strcmp(dsnodes[i], dsnodes[j]) == 0) {
				free(dsnodes);
				return (SCCONF_EINUSE);
			}
		}
	}

	/*lint -restore */
	return (SCCONF_NOERR);
}

/*
 * scconfcmd_add_ds
 *
 *	Add a device group or change device group configuration.
 */
static scconf_errno_t
scconfcmd_add_ds(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *last, *dupopts = (char *)0;
	char **dsnodes = (char **)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_TYPE,			/* 0 - ds type */
		SUBOPT_NAME,			/* 1 - ds name */
		SUBOPT_NODELIST,		/* 2 - ds node list */
		SUBOPT_PREFERENCED,		/* 3 - ds preference flag */
		SUBOPT_FAILBACK,		/* 4 - ds failback flag */
		SUBOPT_NUMSECONDARIES,		/* 5 - ds desired secs */
		(char *)0
	};
	char *dstype = NULL;
	char *dsname = NULL;
	char *nodelist = NULL;
	char *sdsfailback = NULL;
	char *sdspreference = NULL;
	char *sdsnumsecondaries = NULL;
	scconf_state_t dsfailback = SCCONF_STATE_DISABLED;
	scconf_state_t dspreference = SCCONF_STATE_UNCHANGED;
	unsigned int dsnumsecondaries = SCCONF_NUMSECONDARIES_UNSET;
	long tmp_sec;
	char *dsoptions = NULL;

	/* duplicate subopts and allocate space for dsoptions */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (dsoptions = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	bzero(dsoptions, strlen(dsoptions));

	current = dupopts;
	while (*current) {
		last = current;
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - ds type */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, dstype);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			dstype = value;
			break;

		/* 1 - ds name */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, dsname);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			dsname = value;
			break;

		/* 2 - ds node list */
		case 2:
			scconferr = suboption_usage(knownopts[optindex],
			    value, nodelist);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			nodelist = value;
			break;

		/* 3 - ds preference flag */
		case 3:
			scconferr = suboption_usage(knownopts[optindex],
			    value, sdspreference);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			if (strcmp(value, STR_TRUE) != 0 &&
			    strcmp(value, STR_FALSE) != 0) {
				(void) fprintf(stderr, gettext(
				    "%s:  The \"%s\" suboption must be "
				    "set to \"%s\" or \"%s\".\n"),
				    progname, knownopts[optindex],
				    STR_TRUE, STR_FALSE);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			sdspreference = value;
			if (strcmp(value, STR_TRUE) == 0)
				dspreference = SCCONF_STATE_ENABLED;
			else if (strcmp(value, STR_FALSE) == 0)
				dspreference = SCCONF_STATE_DISABLED;
			break;

		/* 4 - ds failback flag */
		case 4:
			scconferr = suboption_usage(knownopts[optindex],
			    value, sdsfailback);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			if (strcmp(value, STR_ENABLED) != 0 &&
			    strcmp(value, STR_DISABLED) != 0) {
				(void) fprintf(stderr, gettext(
				    "%s:  The \"%s\" suboption must be "
				    "set to \"%s\" or \"%s\".\n"),
				    progname, knownopts[optindex],
				    STR_ENABLED, STR_DISABLED);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			sdsfailback = value;
			if (strcmp(value, STR_ENABLED) == 0)
				dsfailback = SCCONF_STATE_ENABLED;
			else if (strcmp(value, STR_DISABLED) == 0)
				dsfailback = SCCONF_STATE_DISABLED;
			break;

		/* 5 - ds numsecondaries option */
		case 5:
			scconferr = suboption_usage(knownopts[optindex], "-1",
			    sdsnumsecondaries);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			if ((value == NULL) || (*value == '\0')) {
				sdsnumsecondaries = "0";
				dsnumsecondaries =
				    SCCONF_NUMSECONDARIES_UNSET;
			} else {
				tmp_sec = strtol(value, (char **)NULL, 10);
				if ((tmp_sec <= 0) ||
				    (tmp_sec > SCCONF_NODEID_MAX)) {
					(void) fprintf(stderr, gettext(
					    "%s:  %s is not a valid value "
					    "for the suboption \"%s\".\n"),
					    progname, value,
					    knownopts[optindex]);
					scconferr = SCCONF_EUSAGE;
					goto cleanup;
				}
				sdsnumsecondaries = value;
				dsnumsecondaries = (unsigned int) tmp_sec;
			}

			break;

		/* other options */
		case -1:
			if (*dsoptions != '\0')
				(void) strcat(dsoptions, ",");
			(void) strcat(dsoptions, last);
			break;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (dsname == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required with the "
		    "device group add operation.\n"), progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
	}
	if (dstype == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required with the "
		    "device group add operation.\n"), progname, SUBOPT_TYPE);
		scconferr = SCCONF_EUSAGE;
	}
	if (scconferr != SCCONF_NOERR)
		goto cleanup;

	/* just check usage? */
	if (uflag)
		goto cleanup;

	scconferr = get_nodenames(nodelist, &dsnodes);
	if (scconferr != SCCONF_NOERR) {
		(void) fprintf(stderr, gettext(
		    "%s:  A bad \"%s\" option was given with the "
		    "device group add operation.\n"),
		    progname, SUBOPT_NODELIST);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* add the device service */
	scconferr = scconf_add_ds_vers2(dstype, dsname,
	    dsnodes, dspreference, NULL, dsfailback, NULL,
	    dsoptions, dsnumsecondaries, messages);
	if (scconferr != SCCONF_NOERR) {
		if (scconferr == SCCONF_EUSAGE) {
			(void) fprintf(stderr, gettext(
			    "%s:  \"Add\" operation is not supported "
			    "for this device group type.\n"), progname);
			scconferr = SCCONF_EINVAL;
		}
		goto cleanup;
	}

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EEXIST:
			(void) sprintf(errbuff, gettext(
			    "device group is already configured"));
			break;

		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "a diskset, diskgroup, other device, or node "
			    "is not found or is unknown"));
			break;

		case SCCONF_EINUSE:
			(void) sprintf(errbuff, gettext(
			    "a diskset, diskgroup, or other device "
			    "is already in use in another device group"));
			break;

		case SCCONF_EBUSY:
			(void) sprintf(errbuff, gettext(
			    "the device group is in a busy state; "
			    "try again later"));
			break;

		case SCCONF_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "bad suboption(s) given "
			    "for this device group type"));
			break;

		case SCCONF_EUNKNOWN:
			(void) sprintf(errbuff, gettext(
			    "unknown device group type"));
			break;

		case SCCONF_DS_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "inconsistencies detected in "
			    "device configuration"));
			break;

		case SCCONF_ESETUP:
			(void) sprintf(errbuff, gettext(
			    "the volume manager or other device management "
			    "software was not installed or set up properly"));
			break;

		case SCCONF_ERANGE:
			(void) sprintf(errbuff, gettext(
			    "the value specified for \"numsecondaries\" "
			    "suboption is too large for the device group "
			    "configuration"));
			break;

		case SCCONF_EMAJOR:
			(void) sprintf(errbuff, gettext(
			    "Major number of vxio/did driver is not "
			    "consistent across nodes.\nCheck the major number "
			    "of the vxio/did driver in file : %s on specified "
			    "nodes"), SCCONF_NAME_TO_MAJOR_FILE);
			break;

		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (dsname != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to perform an add operation on a "
			    "device group (%s) - %s.\n"),
			    progname, dsname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to perform an add operation on a "
			    "device group - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);
	if (dsoptions)
		free(dsoptions);
	if (dsnodes)
		free(dsnodes);

	return (scconferr);
}

/*
 * scconfcmd_change_ds
 *
 *	Change device group configuration.
 */
static scconf_errno_t
scconfcmd_change_ds(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *last, *dupopts = (char *)0;
	char **dsnodes = (char **)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NAME,			/* 0 - ds name */
		SUBOPT_NODELIST,		/* 1 - ds node list */
		SUBOPT_PREFERENCED,		/* 2 - ds nopreference flag */
		SUBOPT_FAILBACK,		/* 3 - ds failback flag */
		SUBOPT_NUMSECONDARIES,		/* 4 - ds desired secs */
		(char *)0
	};
	char *dsname = NULL;
	char *nodelist = NULL;
	char *sdsfailback = NULL;
	char *sdspreference = NULL;
	char *sdsnumsecondaries = NULL;
	scconf_state_t dsfailback = SCCONF_STATE_UNCHANGED;
	scconf_state_t dspreference = SCCONF_STATE_UNCHANGED;
	unsigned int dsnumsecondaries = SCCONF_NUMSECONDARIES_UNSET;
	long tmp_sec;
	char *dsoptions = NULL;

	/* duplicate subopts and allocate space for dsoptions */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (dsoptions = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	bzero(dsoptions, strlen(dsoptions));

	current = dupopts;
	while (*current) {
		last = current;
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - ds name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, dsname);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			dsname = value;
			break;

		/* 1 - ds node list */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, nodelist);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			nodelist = value;
			dspreference = SCCONF_STATE_ENABLED;
			break;

		/* 2 - ds preference flag */
		case 2:
			scconferr = suboption_usage(knownopts[optindex],
			    value, sdspreference);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			if (strcmp(value, STR_TRUE) != 0 &&
			    strcmp(value, STR_FALSE) != 0) {
				(void) fprintf(stderr, gettext(
				    "%s:  The \"%s\" suboption must be "
				    "set to \"%s\" or \"%s\".\n"),
				    progname, knownopts[optindex],
				    STR_TRUE, STR_FALSE);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			sdspreference = value;
			if (strcmp(value, STR_TRUE) == 0)
				dspreference = SCCONF_STATE_ENABLED;
			else if (strcmp(value, STR_FALSE) == 0)
				dspreference = SCCONF_STATE_DISABLED;
			break;

		/* 3 - ds failback flag */
		case 3:
			scconferr = suboption_usage(knownopts[optindex],
			    value, sdsfailback);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			if (strcmp(value, STR_ENABLED) != 0 &&
			    strcmp(value, STR_DISABLED) != 0) {
				(void) fprintf(stderr, gettext(
				    "%s:  The \"%s\" suboption must be "
				    "set to \"%s\" or \"%s\".\n"),
				    progname, knownopts[optindex],
				    STR_ENABLED, STR_DISABLED);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			sdsfailback = value;
			if (strcmp(value, STR_ENABLED) == 0)
				dsfailback = SCCONF_STATE_ENABLED;
			else if (strcmp(value, STR_DISABLED) == 0)
				dsfailback = SCCONF_STATE_DISABLED;
			break;

		/* 4 - ds numsecondaries option */
		case 4:
			scconferr = suboption_usage(knownopts[optindex], "-1",
			    sdsnumsecondaries);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			if ((value == NULL) || (*value == '\0')) {
				sdsnumsecondaries = "0";
				dsnumsecondaries =
				    SCCONF_NUMSECONDARIES_SET_DEFAULT;
			} else {
				tmp_sec = strtol(value, (char **)NULL, 10);
				if ((tmp_sec <= 0) ||
				    (tmp_sec > SCCONF_NODEID_MAX)) {
					(void) fprintf(stderr, gettext(
					    "%s:  %s is not a valid value "
					    "for the suboption \"%s\".\n"),
					    progname, value,
					    knownopts[optindex]);
					scconferr = SCCONF_EUSAGE;
					goto cleanup;
				}
				sdsnumsecondaries = value;
				dsnumsecondaries = (unsigned int) tmp_sec;
			}

			break;

		/* other options */
		case -1:
			if (*dsoptions != '\0')
				(void) strcat(dsoptions, ",");
			(void) strcat(dsoptions, last);
			break;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (dsname == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for changing "
		    "device group properties.\n"), progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	if (scconf_is_local_device_service(dsname)) {
		(void) fprintf(stderr, gettext(
		    "%s: \"Change\" operation is not supported "
		    "for this device group type.\n"), progname);
		scconferr = SCCONF_EINVAL;
		goto cleanup;
	}

	/* get nodenames */
	scconferr = get_nodenames(nodelist, &dsnodes);
	if (scconferr != SCCONF_NOERR) {
		(void) fprintf(stderr, gettext(
		    "%s:  A bad \"%s\" option was given with the "
		    "device group change operation.\n"),
		    progname, SUBOPT_NODELIST);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* change the device service */
	scconferr = scconf_change_ds_vers2(dsname, dsnodes, dspreference,
	    NULL, dsfailback, NULL, *dsoptions ? dsoptions : NULL,
	    dsnumsecondaries, messages);
	if (scconferr != SCCONF_NOERR) {
		if (scconferr == SCCONF_EUSAGE) {
			(void) fprintf(stderr, gettext(
			    "%s:  \"Change\" operation is not supported "
			    "for this device group type.\n"), progname);
			scconferr = SCCONF_EINVAL;
		}
		goto cleanup;
	}

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "a device group, device, or node "
			    "is not found or is unknown"));
			break;

		case SCCONF_EINUSE:
			(void) sprintf(errbuff, gettext(
			    "device is already in use in another "
			    "device group"));
			break;

		case SCCONF_EBUSY:
			(void) sprintf(errbuff, gettext(
			    "the device group is in a busy state; "
			    "try again later"));
			break;

		case SCCONF_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "bad suboption(s) given "
			    "for this device group type"));
			break;

		case SCCONF_EUNKNOWN:
			(void) sprintf(errbuff, gettext(
			    "unknown device group type"));
			break;

		case SCCONF_DS_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "inconsistencies detected in "
			    "device configuration"));
			break;

		case SCCONF_ERANGE:
			(void) sprintf(errbuff, gettext(
			    "the value specified for \"numsecondaries\" "
			    "suboption is too large for the device group "
			    "configuration"));
			break;

		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (dsname != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to change device group (%s) - %s.\n"),
			    progname, dsname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to change device group - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);
	if (dsoptions)
		free(dsoptions);
	if (dsnodes)
		free(dsnodes);

	return (scconferr);
}

/*
 * scconfcmd_rm_ds
 *
 *	Remove device group.
 */
static scconf_errno_t
scconfcmd_rm_ds(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *last, *dupopts = (char *)0;
	char **dsnodes = (char **)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NAME,			/* 0 - ds name */
		SUBOPT_NODELIST,		/* 1 - ds node list */
		(char *)0
	};
	char *dsname = NULL;
	char *nodelist = NULL;
	char *dsoptions = NULL;

	/* duplicate subopts and allocate space for dsoptions */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (dsoptions = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	bzero(dsoptions, strlen(dsoptions));

	current = dupopts;
	while (*current) {
		last = current;
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - ds name */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, dsname);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			dsname = value;
			break;

		/* 1 - ds node list */
		case 1:
			scconferr = suboption_usage(knownopts[optindex],
			    value, nodelist);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			nodelist = value;
			break;

		/* other options */
		case -1:
			if (*dsoptions != '\0')
				(void) strcat(dsoptions, ",");
			(void) strcat(dsoptions, last);
			break;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (dsname == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for a "
		    "device group remove operation.\n"), progname, SUBOPT_NAME);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* get nodenames */
	scconferr = get_nodenames(nodelist, &dsnodes);
	if (scconferr != SCCONF_NOERR) {
		(void) fprintf(stderr, gettext(
		    "%s:  A bad \"%s\" option was given with the "
		    "device group remove operation.\n"),
		    progname, SUBOPT_NODELIST);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* remove the device service */
	scconferr = scconf_rm_ds_vers2(dsname, dsnodes, NULL, NULL, dsoptions,
	    messages);
	if (scconferr != SCCONF_NOERR) {
		if (scconferr == SCCONF_EUSAGE) {
			(void) fprintf(stderr, gettext(
			    "%s:  \"Remove\" operation is not supported "
			    "for this device group type.\n"), progname);
			scconferr = SCCONF_EINVAL;
		}
		goto cleanup;
	}

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_ENOEXIST:
			(void) sprintf(errbuff, gettext(
			    "a device group, device, or node "
			    "is not found or is unknown"));
			break;

		case SCCONF_EBUSY:
			if ((dsnodes == NULL) && (!*dsoptions)) {
				(void) sprintf(errbuff, gettext(
				    "the device group should be brought "
				    "offline before it can be removed"));
			} else {
				(void) sprintf(errbuff, gettext(
				    "the device group is in a busy state; "
				    "try again later"));
			}
			break;

		case SCCONF_EINVAL:
			(void) sprintf(errbuff, gettext(
			    "bad suboption(s) given "
			    "for this device group type"));
			break;

		case SCCONF_EUNKNOWN:
			(void) sprintf(errbuff, gettext(
			    "unknown device group type"));
			break;

		case SCCONF_ERANGE:
			(void) sprintf(errbuff, gettext(
			    "the value of \"numsecondaries\" property for this "
			    "device group needs to be lowered before this "
			    "operation can succeed"));
			break;

		case SCCONF_EINUSE:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		if (dsname != NULL) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to perform remove operation on a "
			    "device group (%s) - %s.\n"),
			    progname, dsname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to perform remove operation on a "
			    "device group - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);
	if (dsoptions)
		free(dsoptions);

	return (scconferr);
}

/*
 * scconfcmd_set_authentication
 *
 *	Set authentication options.   If "typeonly" is not zero,
 *	only authentication type may be set.
 */
static scconf_errno_t
scconfcmd_set_authentication(char *subopts, uint_t uflag, uint_t typeonly)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex, event_flag = 0;

	char *knownopts[] = {
		SUBOPT_AUTHTYPE,		/* 0 - authentication type */
		SUBOPT_NODE,			/* 1 - node name to add */
		(char *)0
	};
	char *sauthtype = NULL;
	scconf_authtype_t authtype;
	char *node = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - authentication */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, sauthtype);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			sauthtype = value;
			break;

		/* 1 - node name to add */
		case 1:
			if (typeonly) {
				suboption_unknown(knownopts[optindex]);
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* add the node to the list */
			node = value;
			if (!uflag) {
					/* Generate event */
				event_flag = 1;
				(void) cl_cmd_event_gen_start_event();
				(void) atexit(cl_cmd_event_gen_end_event);

				scconferr = scconf_addto_secure_joinlist(node);
				if (scconferr != SCCONF_NOERR) {
					switch (scconferr) {
					case SCCONF_EEXIST:
						(void) sprintf(errbuff, gettext(
						    "node \"%s\" is already "
						    "in the authentication "
						    "list"), node);
						break;

					case SCCONF_EINVAL:
					case SCCONF_ESETUP:
					case SCCONF_ENOCLUSTER:
						scconferr = SCCONF_EUNEXPECTED;

					/*FALLTHRU*/
					default:
						scconf_strerr(errbuff,
						    scconferr);
						break;
					} /*lint !e788 */
					(void) fprintf(stderr, gettext(
					    "%s:  Failed to update "
					    "authentication list - %s.\n"),
					    progname, errbuff);
					goto cleanup2;
				}
			}
			break;

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if ((!typeonly && node == NULL) &&
	    sauthtype == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  Either a \"%s\" or \"%s\" suboption is required "
		    "with \"%s\".\n"),
		    progname, SUBOPT_NODE, SUBOPT_AUTHTYPE, "-aT");
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* set authentication type, if requested */
	if (sauthtype != NULL) {
		if (strcmp(sauthtype, "sys") == 0)
			authtype = SCCONF_AUTH_SYS;
		else if (strcmp(sauthtype, "unix") == 0)
			authtype = SCCONF_AUTH_SYS;
		else if (strcmp(sauthtype, "des") == 0)
			authtype = SCCONF_AUTH_DH;
		else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to update authentication"
			    " - unknown authtype\n"),
			    progname);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}

		/* Generate event */
		if (event_flag == 0) {
			(void) cl_cmd_event_gen_start_event();
			(void) atexit(cl_cmd_event_gen_end_event);
		}

		scconferr = scconf_set_secure_authtype(authtype);
		if (scconferr != SCCONF_NOERR) {
			switch (scconferr) {
			case SCCONF_EINVAL:
			case SCCONF_ESETUP:
			case SCCONF_ENOCLUSTER:
				scconferr = SCCONF_EUNEXPECTED;

			/*FALLTHRU*/
			default:
				scconf_strerr(errbuff, scconferr);
				break;
			} /*lint !e788 */
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to update authentication "
			    "type - %s.\n"), progname, errbuff);
			goto cleanup2;
		}
	}

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to update authentication - %s.\n"),
		    progname, errbuff);
	}

cleanup2:
	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}

/*
 * scconfcmd_rm_authnodes
 *
 *	Remove one or all nodes from the list of auth nodes.
 */
static scconf_errno_t
scconfcmd_rm_authnodes(char *subopts, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex;

	char *knownopts[] = {
		SUBOPT_NODE,			/* 0 - node name to remove */
		SUBOPT_ALL,			/* 1 - remove all */
		(char *)0
	};
	char *node = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - node name to remove */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, (char *)-1);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;

			/* remove the node from the list */
			node = value;
			if (!uflag) {
				/* Generate event */
				(void) cl_cmd_event_gen_start_event();
				(void) atexit(cl_cmd_event_gen_end_event);

				scconferr = scconf_rmfrom_secure_joinlist(node);
				if (scconferr != SCCONF_NOERR)
					goto cleanup;
			}

			break;

		/* 1 - clear the list */
		case 1:
			if (value != NULL) {
				scconferr = SCCONF_EUSAGE;
				goto cleanup;
			}

			/* clear the list */
			if (!uflag) {
				/* Generate event */
				(void) cl_cmd_event_gen_start_event();
				(void) atexit(cl_cmd_event_gen_end_event);

				scconferr = scconf_clear_secure_joinlist();
				if (scconferr != SCCONF_NOERR)
					goto cleanup;
			}

			break;

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_ENOEXIST:
			if (node != NULL) {
				(void) sprintf(errbuff, gettext(
				    "node \"%s\" is unknown"), node);
			} else {
				scconferr = SCCONF_EUNEXPECTED;
				scconf_strerr(errbuff, scconferr);
			}
			break;

		case SCCONF_EINVAL:
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to update authentication - %s.\n"),
		    progname, errbuff);
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}


/* SC SLM addon start */

/*
 * scconfcmd_set_scslm
 */
static scconf_errno_t
scconfcmd_set_scslm(char *subopts, char **messages, uint_t uflag)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	int optindex = 0;
	int shares = 0;

	char *knownopts[] = {
		SUBOPT_NODE,
		SUBOPT_SCSLM_GLOBAL_ZONE_SHARES,
		SUBOPT_SCSLM_DEFAULT_PSET_MIN,
		(char *)0
	};
	char *nodename = NULL;
	char *str_shares = NULL;
	char *str_cpu = NULL;
	char *str;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		optindex = getsubopt(&current, knownopts, &value);
		switch (optindex) {

		/* 0 - node name or ID */
		case 0:
			scconferr = suboption_usage(knownopts[optindex],
			    value, nodename);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			nodename = value;
			break;

		case 1:
		case 2:
			str = NULL;
			if (strcmp(knownopts[optindex],
			    SUBOPT_SCSLM_GLOBAL_ZONE_SHARES) == 0) {
				str = str_shares;
			}
			if (strcmp(knownopts[optindex],
			    SUBOPT_SCSLM_DEFAULT_PSET_MIN) == 0) {
				str = str_cpu;
			}
			scconferr = suboption_usage(knownopts[optindex],
			    value, str);
			if (scconferr != SCCONF_NOERR)
				goto cleanup;
			if (strcmp(knownopts[optindex],
			    SUBOPT_SCSLM_GLOBAL_ZONE_SHARES) == 0) {
				str_shares = value;
			}
			if (strcmp(knownopts[optindex],
			    SUBOPT_SCSLM_DEFAULT_PSET_MIN) == 0) {
				str_cpu = value;
			}
			break;

		/* unknown suboption */
		case -1:
			suboption_unknown(value);
			scconferr = SCCONF_EUSAGE;
			goto cleanup;

		/* internal error */
		default:
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (nodename == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s:  A \"%s\" suboption is required for setting the "
		    "Service Level Mngt properties.\n"), progname, SUBOPT_NODE);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}
	if (str_shares == NULL && str_cpu == NULL) {
		(void) fprintf(stderr, gettext(
		    "%s: provide at least one Service Level Mngt propertie.\n"),
		    progname);
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* just check usage? */
	if (uflag)
		goto cleanup;

	/* check input values */
	if (str_shares != NULL) {
		shares = atoi(str_shares);
		if (shares < 1 || shares > FSS_MAXSHARES) {
			(void) fprintf(stderr, gettext(
			    "%s: invalid globalzoneshares value "
			    "(must be from 1 to %u)\n"),
			    progname, FSS_MAXSHARES);
			scconferr = SCCONF_EINVAL;
			goto cleanup;
		}
	}
	if (str_cpu != NULL) {
		shares = atoi(str_cpu);
		if (shares < 1) {
			(void) fprintf(stderr, gettext(
			    "%s: invalid defaultpsetmin value "
			    "(must be >= 1)\n"), progname);
			scconferr = SCCONF_EINVAL;
			goto cleanup;
		}
	}

	if (str_shares != NULL) {
		shares = atoi(str_shares);
		scconferr = scconf_set_scslm(nodename,
		    SCCONF_NODE_SCSLM_GLOBAL_SHARES, shares, NULL, messages);
		if (scconferr != SCCONF_NOERR)
			goto cleanup;
	}
	if (str_cpu != NULL) {
		shares = atoi(str_cpu);
		scconferr = scconf_set_scslm(nodename,
		    SCCONF_NODE_SCSLM_DEFAULT_PSET, shares, NULL, messages);
		if (scconferr != SCCONF_NOERR)
			goto cleanup;
	}

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

cleanup:
	/* Print any additional error messages */
	if (scconferr != SCCONF_NOERR) {
		switch (scconferr) {
		case SCCONF_EINVAL:
			scconf_strerr(errbuff, scconferr);
			break;
		case SCCONF_ESETUP:
		case SCCONF_ENOCLUSTER:
			scconferr = SCCONF_EUNEXPECTED;

		/*FALLTHRU*/
		default:
			scconf_strerr(errbuff, scconferr);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to set Service Level Mngt conf - %s.\n"),
		    progname, errbuff);
	}

	/* Free memory */
	if (dupopts)
		free(dupopts);

	return (scconferr);
}


/*
 * check all SC SLM numerical args on command line
 */
static scconf_errno_t
scconfcmd_scslm_check_values(char **argv)
{
	char		saved;
	char		*end;
	char		*ptr;
	char		*ptr_cpu;	/* defaultpsetmin=X */
	char		*ptr_share;	/* globalzoneshares=X */
	unsigned int	arg_value;	/* value of X above */
	boolean_t	cpu;		/* cpu or share value ? */

	if (argv == NULL) {
		return (SCCONF_NOERR);
	}
	while (*argv != NULL) {
		ptr = (char *)(*argv);
		while (B_TRUE) {
			ptr_cpu = strstr(ptr, "defaultpsetmin=");
			ptr_share = strstr(ptr, "globalzoneshares=");
			ptr = NULL;
			if (ptr_cpu != NULL) {
				if (ptr_share != NULL) {
					if (ptr_cpu > ptr_share) {
						ptr = ptr_share;
						cpu = B_FALSE;
					} else {
						ptr = ptr_cpu;
						cpu = B_TRUE;
					}
				} else {
					ptr = ptr_cpu;
					cpu = B_TRUE;
				}
			} else {
				ptr = ptr_share;
				cpu = B_FALSE;
			}
			if (ptr == NULL) {
				break;
			}
			end = ptr;
			while (*end++ != '=') {
			}
			ptr = end;
			while (*end != ',' && *end != '\0') {
				end++;
			}
			saved = *end;
			*end = '\0';
			arg_value = (unsigned int) atoi(ptr);
			*end = saved;
			if (cpu == B_FALSE &&
			    (arg_value < 1 || arg_value > FSS_MAXSHARES)) {
				(void) fprintf(stderr, gettext(
				    "%s: invalid globalzoneshares value "
				    "(must be from 1 to %u)\n"),
				    progname, FSS_MAXSHARES);
				return (SCCONF_EINVAL);
			}
			if (cpu == B_TRUE && arg_value < 1) {
				(void) fprintf(stderr, gettext(
				    "%s: invalid defaultpsetmin value "
				    "(must be >= 1)\n"), progname);
				return (SCCONF_EINVAL);
			}
			ptr = end;
		}
		argv++;
	}
	return (SCCONF_NOERR);
}
/* SC SLM addon end */
