/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scnasdir.c	1.9	08/05/20 SMI"

/*
 * scnasdir(1M)
 */

#include <scnas.h>
#include <sys/cl_cmd_event.h>
#include <sys/stat.h>
#include <sys/cl_auth.h>
#include <rgm/sczones.h>

static char *progname;

static void usage(FILE *out);

static void print_help_add(FILE *out);
static void print_help_remove(FILE *out);
static void print_help_print(FILE *out);
static scnas_errno_t
    read_directories_file(char *input_file, scnas_filer_prop_t **proplist_p);

static scnas_errno_t
    scnasdircmd_add(int argc, char **argv, char **messages, uint_t uflag);
static scnas_errno_t
    scnasdircmd_remove(int argc, char **argv, char **messages, uint_t uflag);
static scnas_errno_t
    scnasdircmd_print(int argc, char **argv, char **messages, uint_t uflag);

/*
 * main
 */
int
main(int argc, char **argv)
{
	int c;
	uint_t i;
	uint_t aflg, rflg, Hflg, pflg, unflg;
	scnas_errno_t scnas_err;
	scnas_errno_t (*scnasdircmd)(int, char **, char **, uint_t uflag);
	char *messages = (char *)0;
	char *msg, *last;
	uint_t ismember;
	int exitstatus;
	FILE *out;
	scconf_errno_t scconf_rstatus;
	char errbuf[SCNAS_MAXSTRINGLEN];

	if (sc_zonescheck() != 0)
		return (1);

	/* Initialize and demote to normal uid */
	cl_auth_init();
	cl_auth_demote();

	scnasdircmd = NULL;
	aflg = rflg = pflg = Hflg = unflg = 0;

	/* Set the program name */
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	/* Process options */
	while ((c = getopt(argc, argv, "arpnHh:d:f:t:"))
	    != EOF) {
		switch (c) {
		case 'a':
			cl_auth_check_command_opt_exit(*argv, "-a",
			    CL_AUTH_DEVICE_MODIFY, SCNAS_EPERM);
			aflg = 1;
			scnasdircmd = scnasdircmd_add;
			break;

		case 'r':
			cl_auth_check_command_opt_exit(*argv, "-r",
			    CL_AUTH_DEVICE_MODIFY, SCNAS_EPERM);
			rflg = 1;
			scnasdircmd = scnasdircmd_remove;
			break;

		case 'p':
			cl_auth_check_command_opt_exit(*argv, "-p",
			    CL_AUTH_DEVICE_READ, SCNAS_EPERM);
			pflg = 1;
			scnasdircmd = scnasdircmd_print;
			break;

		case 'H':
			++Hflg;
			break;

		case 'n':
			unflg = 1;
			break;

		case '?':
			usage(stderr);
			exit(SCNAS_EUSAGE);

		default:
			break;
		}
	}

	/* Make sure no additional args */
	if (optind != argc) {
		usage(stdout);
		exit(SCNAS_EUSAGE);
	}

	/* reset optind */
	optind = 1;

	/* Check the basic options */
	i = aflg + rflg + pflg;
	if (Hflg) {
		/* if no basic flags, print help for everything */
		if (i == 0) {
			aflg = rflg = pflg = 1;
		}
	} else {
		if ((i == 0) || (i > 1)) {
			usage(stdout);
			exit(SCNAS_EUSAGE);
		}
	}

	/* if Hflg is set, just print help, and exit */
	if (Hflg) {

		/* if help everything, include usage message */
		if (aflg && rflg && pflg) {
			(void) putc('\n', stdout);
			usage(stdout);
		}

		/* Print help */
		if (aflg) {
			print_help_add(stdout);
		}

		if (rflg) {
			print_help_remove(stdout);
		}

		if (pflg) {
			print_help_print(stdout);
		}

		exit(SCNAS_NOERR);
	}

	/* promote to euid=0 */
	cl_auth_promote();

	/* Make sure we are active in the cluster */
	ismember = 0;
	scconf_rstatus = scconf_ismember(0, &ismember);
	scnas_err = scnas_conv_scconf_err(scconf_rstatus);

	if (scnas_err != SCNAS_NOERR) {
		scnas_strerr(errbuf, scnas_err);
		(void) fprintf(stderr, "%s:  %s.\n",
		    progname, errbuf);
		exit(scnas_err);
	}

	if (!ismember) {
		(void) fprintf(stderr, gettext(
		    "%s:  This node is not currently "
		    "in the cluster.\n"), progname);
		exit(scconf_rstatus);
	}

	/* check for usage errors, then run the options */
	if (scnasdircmd == NULL) {
		usage(stdout);
		exit(SCNAS_EUSAGE);
	}

	/* Initialize cl_cmd_event library */
	if (!unflg && (!aflg || rflg)) {
		cl_cmd_event_init(argc, argv, &exitstatus);
	}

	/* Run the command */
	exitstatus = scnasdircmd(argc, argv, &messages, unflg);

	/* Print msgbuffer */
	if ((messages != (char *)0) && (exitstatus)) {
		/* If non-zero exitstatus, use stderr, else use stdout */
		out = (exitstatus) ? stderr : stdout;

		msg = strtok_r(messages, "\n", &last);
		while (msg != NULL) {
			(void) fprintf(out, "%s:    %s\n", progname, msg);
			msg = strtok_r(NULL, "\n", &last);
		}
	}

	if (messages) {
		free(messages);
	}

	return (exitstatus);
}

/*
 * usage
 *
 *	Print a simple usage message to stderr.
 */
static void
usage(FILE *out)
{
	(void) fprintf(out, "%s:  %s -a|-r|-p [-H] [-n] [options]\n",
	    gettext("usage"), progname);
	(void) fprintf(out, "\t%s [-H]\n",
	    progname);

	(void) putc('\n', out);
}

/*
 * print_help_add
 *
 *	Print a help message to "out" for the "add" form of the command.
 */
static void
print_help_add(FILE *out)
{
	(void) fprintf(out, "%s (%s -a):\n",
	    gettext("Add options"), progname);

	(void) fprintf(out, "\t-h filer_name -d directory [-d ..]\n");
	(void) fprintf(out, "\t-h filer_name -f input_file\n");
	(void) fprintf(out, "\t-h filer_name -d directory [-d ..] "
	    "-f input_file\n");
	(void) fprintf(out, "\t-n -h filer_name -f input_file\n");
	(void) fprintf(out, "\t-n -h filer_name -d directory [-d ..] "
	    "-f input_file\n");
	(void) fprintf(out, "\t-H\n");

	(void) putc('\n', out);
}

/*
 * print_help_remove
 *
 *	Print a help message to "out" for the "remove" form of the command.
 */
static void
print_help_remove(FILE *out)
{
	(void) fprintf(out, "%s (%s -r):\n",
	    gettext("Remove options"), progname);

	(void) fprintf(out, "\t-h filer_name -d directory [-d ..]\n");
	(void) fprintf(out, "\t-h filer_name -d all\n");
	(void) fprintf(out, "\t-h filer_name -f input_file\n");
	(void) fprintf(out, "\t-n -h filer_name -f input_file\n");
	(void) fprintf(out, "\t-H\n");

	(void) putc('\n', out);
}

/*
 * print_help_print
 *
 *	Print a help message to "out" for the "print" form of the command.
 */
static void
print_help_print(FILE *out)
{
	(void) fprintf(out, "%s (%s -p):\n",
	    gettext("Print options"), progname);

	(void) fprintf(out, "\t-h filer_name -t filer_type\n");
	(void) fprintf(out, "\t-h filer_name\n");
	(void) fprintf(out, "\t-t filer_type\n");
	(void) fprintf(out, "\t[ no options ]\n");

	(void) fprintf(out, "\t-H\n");
}

/*
 * read_directories_file
 *
 *	Process the input file for the directories. Put the directories
 *	read from the file to a scnas_filer_prop_t list.
 */
static scnas_errno_t
read_directories_file(char *input_file, scnas_filer_prop_t **proplist_p)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	FILE *fp;
	char input_buf[SCNAS_MAXSTRINGLEN];
	char *ptr, *tmp_p;
	char *directory;

	/* Check args */
	if (!input_file || proplist_p == NULL) {
		return (SCNAS_EINVAL);
	}

	/* Open the file */
	if ((fp = fopen(input_file, "r")) == 0) {
		return (SCNAS_ENOFILE);
	}

	/* Read the file */
	while (fgets(input_buf, sizeof (input_buf), fp)) {
		/* Remove new line */
		tmp_p = (char *)strtok(input_buf, "\n");
		ptr = tmp_p;

		/* Skip empty line */
		if (ptr == NULL || *ptr == '\0' || *ptr == '#')
			continue;

		/* Remove leading spaces/Tabs */
		do {
			if (ptr == NULL)
				break;
			if (*ptr != ' ' && *ptr != '\t')
				break;
		} while ((*ptr++ = *tmp_p++) != '\0');

		/* Skip lines only has spaces/Tabs or comments */
		if (ptr == NULL || *ptr == '\0' || *ptr == '#')
			continue;

		/* Remove comments */
		directory = tmp_p;
		directory = (char *)strtok(directory, "#");
		if (!directory)
			continue;

		/* Remove the space/Tabs */
		directory = (char *)strtok(directory, " ");
		if (!directory)
			continue;
		directory = (char *)strtok(directory, "\t");
		if (!directory)
			continue;

		/* Add the directory into property list */
		scnas_err = scnas_add_one_prop(NAS_DIRECTORIES,
		    directory, proplist_p);
		if (scnas_err != SCNAS_NOERR)
			break;
	}

	/* Close the file */
	if (fp) {
		(void) fclose(fp);
	}

	return (scnas_err);
}

/*
 * scnasdircmd_add
 *
 *	The "add" form of scnasdir command.
 *
 *	If "uflag" is set, just check for usage errors.
 */
scnas_errno_t
scnasdircmd_add(int argc, char **argv, char **messages, uint_t uflag)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char errbuff[SCNAS_MAXSTRINGLEN];
	char *filer_name = NULL;
	char *input_file = NULL;
	char *directories = NULL;
	scnas_filer_prop_t *proplist = (scnas_filer_prop_t *)0;
	int c;

	/* Process the optiosn */
	optind = 1;
	while ((c = getopt(argc, argv, "arpnh:d:f:")) != EOF) {
		switch (c) {
		case 'a':
			break;

		case 'r':
		case 'p':
			scnas_err = SCNAS_EUSAGE;
			goto cleanup;

		case 'n':
			/* Check usage */
			break;

		case 'h':
			/* Filer name */
			filer_name = optarg;
			break;

		case 'f':
			/* Input file */
			input_file = optarg;
			break;

		case 'd':
			/* directories */
			directories = optarg;
			if (strcmp(directories, NASDIR_ALL_DIR)
			    == 0) {
				scnas_err = SCNAS_EINVALPROP;
				goto cleanup;
			}

			/* Add to the input property list */
			scnas_err = scnas_add_one_prop(
			    NAS_DIRECTORIES, directories, &proplist);
			if (scnas_err != SCNAS_NOERR)
				goto cleanup;

			break;

		default:
			scnas_err = SCNAS_EUSAGE;
			goto cleanup;
		}
	}

	/* Check for required arguments */
	if (!filer_name) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\" is required for adding "
		    "directories. \n"), progname, SUBOPT_FILERNAME);
		scnas_err =  SCNAS_EUSAGE;
		goto cleanup;
	}

	/* Process the input file */
	if (input_file) {
		scnas_err = read_directories_file(input_file,
		    &proplist);
		if (scnas_err != SCNAS_NOERR)
			goto cleanup;
	}

	/* Check for directories */
	if (proplist == (scnas_filer_prop_t *)0) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\", or an input file containing "
		    "the exported directories, is required for "
		    "adding directories.\n"), progname,
		    SUBOPT_DIRECTORIES);
		scnas_err =  SCNAS_EUSAGE;
		goto cleanup;
	}

	/* Only check usage? */
	if (uflag) {
		if (input_file) {
			scnas_err = scnas_display_filer_props(stdout,
			    filer_name, NULL, proplist, messages,
			    SCNASDIR_CMD_ADD);
		}
		goto cleanup;
	}

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Change the filer */
	scnas_err = scnasdir_set_filer_prop(filer_name,
	    proplist, messages, NULL, SCNASDIR_CMD_ADD);

	if (scnas_err != SCNAS_NOERR) {
		goto cleanup;
	}

cleanup:
	/* Print any additional error messages */
	if (scnas_err != SCNAS_NOERR) {
		scnas_strerr(errbuff, scnas_err);
		if (uflag) {
			(void) fprintf(stderr, gettext(
			    "%s:  Adding directories will fail - %s.\n"),
			    progname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add directories to "
			    "filer - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (proplist != (scnas_filer_prop_t *)0) {
		scnas_free_proplist(proplist);
	}

	return (scnas_err);
}

/*
 * scnasdircmd_remove
 *
 *	The "remove" form of scnasdir command.
 *
 *	If "uflag" is set, just check for usage errors.
 */
scnas_errno_t
scnasdircmd_remove(int argc, char **argv, char **messages, uint_t uflag)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char errbuff[BUFSIZ];
	char *filer_name = NULL;
	char *input_file = NULL;
	char *directories = NULL;
	char *all_dir = NULL;
	scnas_filer_prop_t *proplist = (scnas_filer_prop_t *)0;
	int c;

	/* Process the optiosn */
	optind = 1;
	while ((c = getopt(argc, argv, "arpnh:d:f:")) != EOF) {
		switch (c) {
		case 'r':
			break;

		case 'a':
		case 'p':
			scnas_err = SCNAS_EUSAGE;
			break;

		case 'n':
			/* Check usage */
			break;

		case 'h':
			/* Filer name */
			filer_name = optarg;
			break;

		case 'f':
			/* Input file */
			input_file = optarg;
			break;

		case 'd':
			/* directories */
			directories = optarg;

			if (strcmp(directories, NASDIR_ALL_DIR)
			    == 0) {
				if ((all_dir = strdup(
				    NASDIR_ALL_DIR)) == NULL) {
					scnas_err = SCNAS_ENOMEM;
					goto cleanup;
				}

				break;
			}
			/* Add to the input property list */
			scnas_err = scnas_add_one_prop(
			    NAS_DIRECTORIES, directories, &proplist);
			if (scnas_err != SCNAS_NOERR)
				goto cleanup;

			break;

		default:
			scnas_err = SCNAS_EUSAGE;
			goto cleanup;
		}
	}

	/* Check required parameters */
	if (!filer_name) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\" is required for removing "
		    "directories. \n"), progname, SUBOPT_FILERNAME);
		scnas_err = SCNAS_EUSAGE;
		goto cleanup;
	}

	/* Process other options */
	if (input_file && !all_dir) {

		/* Read the input file */
		scnas_err = read_directories_file(input_file,
		    &proplist);
		if (scnas_err != SCNAS_NOERR)
			goto cleanup;
	}

	/* Check directories */
	if ((all_dir == NULL) &&
	    (proplist == (scnas_filer_prop_t *)0)) {
		(void) fprintf(stderr, gettext(
		    "%s:  A suboption \"%s\", or an input file containing "
		    "the directories, is required for "
		    "removing directories.\n"), progname,
		    SUBOPT_DIRECTORIES);
		scnas_err =  SCNAS_EUSAGE;
		goto cleanup;
	}

	/* Only check usage? */
	if (uflag) {
		if (input_file && (all_dir == NULL)) {
			scnas_err = scnas_display_filer_props(stdout,
			    filer_name, NULL, proplist, messages,
			    SCNASDIR_CMD_RM);
		}
		goto cleanup;
	}

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Change the filer */
	scnas_err = scnasdir_set_filer_prop(filer_name,
	    proplist, messages, all_dir, SCNASDIR_CMD_RM);

	if (scnas_err != SCNAS_NOERR) {
		goto cleanup;
	}

cleanup:
	/* Print any additional error messages */
	if (scnas_err != SCNAS_NOERR) {
		scnas_strerr(errbuff, scnas_err);
		if (uflag) {
			(void) fprintf(stderr, gettext(
			    "%s:  Removing directories will "
			    "fail - %s.\n"),
			    progname, errbuff);
		} else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to remove directories - %s.\n"),
			    progname, errbuff);
		}
	}

	/* Free memory */
	if (proplist != (scnas_filer_prop_t *)0) {
		scnas_free_proplist(proplist);
	}

	if (all_dir) {
		free(all_dir);
	}

	return (scnas_err);
}

/*
 * scnasdircmd_print
 *
 *	The "print" format of scnasdir command.
 */
/*ARGSUSED2*/
static scnas_errno_t
scnasdircmd_print(int argc, char **argv, char **messages, uint_t uflag)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char *filer_name = NULL;
	char *filer_type = NULL;
	char errbuff[BUFSIZ];
	int c;

	/* Process the options */
	optind = 1;
	while ((c = getopt(argc, argv, "arpnh:t:f:")) != EOF) {
		switch (c) {
		case 'p':
			break;

		case 'a':
		case 'r':
			scnas_err = SCNAS_EUSAGE;
			break;

		case 'n':
			/* Check usage */
			break;

		case 'h':
			/* Filer name */
			filer_name = optarg;
			break;

		case 't':
			/* Filer type */
			filer_type = optarg;
			break;

		default:
			scnas_err = SCNAS_EUSAGE;
			break;
		}

		if (scnas_err != SCNAS_NOERR) {
			return (scnas_err);
		}
	}

	/* just check usage? */
	if (uflag)
		return (scnas_err);

	/* Print the filer properties */
	if (filer_name) {
		scnas_err = scnas_print_one_filer(filer_name,
		    SCNASDIR_CMD);
	} else if (filer_type) {
		scnas_err = scnas_print_filer_by_type(filer_type,
		    SCNASDIR_CMD);
	} else {
		scnas_err = scnas_print_filer_all(SCNASDIR_CMD);
	}

	/* Print any additional error messages */
	if (scnas_err != SCNAS_NOERR) {
		scnas_strerr(errbuff, scnas_err);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to print the directories "
		    " configuration  - %s.\n"),
		    progname, errbuff);
	}

	return (scnas_err);
}
