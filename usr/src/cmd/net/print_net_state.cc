//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)print_net_state.cc	1.38	08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <h/network.h>
#include <nslib/ns.h>
#include <sys/rm_util.h>
#include <cl_net/netlib.h>
#include <scadmin/scconf.h>

#if !defined(_KERNEL) && defined(_KERNEL_ORB)
#define	UNODE
#else
#undef UNODE
#include <orb/infrastructure/orb.h>
#endif

#if SOL_VERSION >= __s10
#define	SCTP_RGM_SUPPORT
#endif

static nodeid_t get_nodeid(const char *nodeid_str);

static network::PDTServer_ptr get_pdt_server(void);

static void print_usage(const char *format, ...);
static void print_error(const char *format, ...);

static void print_services(network::PDTServer_ptr);
static void print_pdt(network::PDTServer_ptr);
static void print_hash(void);
static void print_fwd(void);
static void print_bind(void);

static char *ipaddr_to_string(const network::inaddr_t &);
static char *lbpolicy_to_string(network::lb_policy_t policy);
static char *protocol_to_string(uint8_t proto);
static char *bindstate_to_string(uint32_t state);

#ifdef UNODE

static void unode_reset(void);

#endif

//
// The name of the service to query about.
//
static char *servicename = NULL;

//
// fwdnode is the specific node to get forwarding information for.  A nodeid
// of '0' is a wildcard to indicate get forwarding information for all nodes.
//
static nodeid_t	fwdnode = 0;

static nodeid_t	proxynode = 0;

//
// Get information from specified GIFnode rather than from PDT.
//
static nodeid_t gifnode = 0;
static network::mcobj_ptr mcobj = NULL;

//
// Get information from specified primary/secondary rather than
// from PDTS ha object.
//
static nodeid_t pdtnode = 0;

//
// Output verbose PDT and forwarding list information.
//
static bool verbose = false;

//
// Flags to map command line arguments.
//
#define	GNS_PRINT_PDT		0x01
#define	GNS_PRINT_FWD		0x02
#define	GNS_PRINT_SERVICES	0x04
#define	GNS_PRINT_HASH		0x08
#define	GNS_PRINT_BIND		0x10

// Maximum length of a string representation of a nodeid (counting '\0')
#define	NODEID_BUFSZ		64

//
// Number of PDT hash table elemets to print per line
// (in verbose mode).
//
#define	NUM_HASH_PER_LINE	40

static char *progname;

static FILE *outfp = stdout;
static FILE *errfp = stderr;

int
main(int argc, char *argv[])
{
	network::mcobjs_t	*mcobjs = NULL;
	int			print_flags = 0;
	Environment		e;
	int			c;
	network::PDTServer_var pdts = NULL;
	FILE			*fp;
	int			ret = 0;
	nodeid_t		target;

#ifdef UNODE
	progname = argv[0] = "print_net_state";

	unode_reset();
#else
	if ((progname = strrchr(argv[0], '/')) == NULL)
		progname = argv[0];
	else
		progname++;
#endif

	//
	// Parse the command line switches
	//
	while ((c = getopt(argc, argv, "S:P:G:N:hf:bspvo:")) != EOF) {
		switch (c) {
		case 'S':
			servicename = optarg;
			break;
		case 's':	// Service group data
			print_flags |= GNS_PRINT_SERVICES;
			break;
		case 'p':	// PDT table data
			print_flags |= GNS_PRINT_PDT;
			break;
		case 'f':	// Forwarding info
			print_flags |= GNS_PRINT_FWD;
			fwdnode = get_nodeid(optarg);
			if (fwdnode > NODEID_MAX) {
				print_usage(
				    "forwarding node id (%d) out of range\n",
				    fwdnode);
				ret = -1;
				goto done;
			}
			break;
		case 'b':	// Binding info
			print_flags |= GNS_PRINT_BIND;
			break;
		case 'P':	// PDT server
			pdtnode = get_nodeid(optarg);
			if (pdtnode > NODEID_MAX) {
				print_usage(
				    "PDTServer node id (%d) out of range\n",
				    pdtnode);
				ret = -1;
				goto done;
			}
			break;
		case 'G':	// gif node
			gifnode = get_nodeid(optarg);
			if (gifnode > NODEID_MAX) {
				print_usage(
				    "GIFnode id (%d) out of range\n",
				    gifnode);
				ret = -1;
				goto done;
			}
			break;
		case 'N':	// proxy node
			proxynode = get_nodeid(optarg);
			if (proxynode > NODEID_MAX) {
				print_usage(
				    "Proxy node id (%d) out of range\n",
				    proxynode);
				ret = -1;
				goto done;
			}
			break;
		case 'h':	// Hash table data
			print_flags |= GNS_PRINT_HASH;
			break;
		case 'v':
			verbose = true;
			break;
		case 'o':
			fp = fopen(optarg, "w");
			if (fp != NULL) {
				outfp = fp;
				errfp = fp;
			} else
				perror(optarg);
			break;
		default:
			print_usage(NULL);
			ret = -1;
			goto done;
		}
	}

#ifndef UNODE
	if (ORB::initialize() != 0) {
		print_error("ORB::initialize failed\n");
		ret = -1;
		goto done;
	}
#endif

	//
	// Get the PDT server.
	//
	pdts = get_pdt_server();
	if (CORBA::is_nil(pdts)) {
		print_error("Cannot get an object reference to PDT Service\n");
		ret = -1;
		goto done;
	}

	if (print_flags & GNS_PRINT_BIND) {
		// -b needs a nodeid given by either -N or -G
		if (proxynode == 0 && gifnode == 0) {
			print_usage(
			    "missing -N or -G, or invalid <node> with -b\n");
			ret = -1;
			goto done;
		}
	} else {
		// -N is supported with -b only
		if (proxynode != 0) {
			print_usage("-N only applicable with -b\n");
			ret = -1;
			goto done;
		}
	}

	//
	// Set up mcobj point to whoever we want to talk to.
	// If gifnode is set, we want to talk to the GIF node.
	//
	if (gifnode || proxynode) {
		target = (gifnode? gifnode: proxynode);

		//
		// Obtain the cluster node mcobj references
		//
		pdts->gns_mcobjs(mcobjs, e);
		if (e.exception()) {
			sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

			if (ex_op) {
				errno = ex_op->error;
				perror("gns_mcobjs");
			} else {
				e.exception()->print_exception("gns_mcobjs");
			}
			e.clear();
			ret = -1;
			goto done;
		}

		if (CORBA::is_nil((*mcobjs)[target])) {
			print_error("no mcobj on node %d.\n", target);
			ret = -1;
			goto done;
		}
		mcobj = (*mcobjs)[target];
	}

	//
	// If we've specified a specific PDT server node, get the
	// provider reference.
	//
	if (pdtnode) {
		print_usage("-P option not currently supported\n");
		ret = -1;
		goto done;
	}

	//
	// Process the flags
	//
	if (print_flags & GNS_PRINT_SERVICES)
		print_services(pdts);
	if (print_flags & GNS_PRINT_PDT)
		print_pdt(pdts);
	if (print_flags & GNS_PRINT_HASH)
		print_hash();
	if (print_flags & GNS_PRINT_FWD)
		print_fwd();
	if (print_flags & GNS_PRINT_BIND)
		print_bind();

done:
	(void) fflush(outfp);
	(void) fflush(errfp);
	if (outfp != stdout)
		(void) fclose(outfp);
	if (errfp != stderr && errfp != outfp)
		(void) fclose(errfp);

	return (ret);
}

//
// Concert a nodename into nodeid. If the nodename given can't be
// concerted, it is assumed to be a string form of nodeid and atoi()
// is used to concert it.
//
static nodeid_t
get_nodeid(const char *nodeid_str)
{
#ifndef	UNODE
	scconf_errno_t err;
	scconf_nodeid_t nodeid;
	char nodeid_buf[NODEID_BUFSZ];

	(void) strcpy(nodeid_buf, nodeid_str);
	err = scconf_get_nodeid(nodeid_buf, &nodeid);

	return (err? (nodeid_t)atoi(nodeid_str): nodeid);
#else
	return ((nodeid_t)atoi(nodeid_str));
#endif
}

static void
print_error(const char *format, ...)
{
	char _buf[MAXPATHLEN];

	va_list v;
	va_start(v, format);	/*lint !e40 */
	(void) vsprintf(_buf, format, v);
	(void) fprintf(errfp, "%s: %s\n", progname, _buf);
	va_end(v);
}

static void
print_usage(const char *format, ...)
{
	char _buf[MAXPATHLEN];

	if (format) {
		va_list v;
		va_start(v, format);	//lint !e40
		(void) vsprintf(_buf, format, v);
		(void) fprintf(errfp, "%s: %s\n", progname, _buf);
		va_end(v);
	}

	(void) fprintf(errfp, "Usage: %s\n"
	    "\t-p [-P <node>] [-G <node>] -S <service>\t\t- print PDT table\n"
	    "\t-s [-P <node>] [-G <node>] [-S <service>]\t- print service(s)\n"
	    "\t-f <node> [-v] -G <node> -S <service>\t\t- print frwd lists\n"
	    "\t-b -N <node> | -G <node> -S <service>\t\t- print bind table\n"
	    "\t-h -G <node>\t\t\t\t\t- print hash tables\n"
	    "\t\t-P <node>\tspecify PDT server node\n"
	    "\t\t-G <node>\tspecify GIF node\n"
	    "\t\t-N <node>\tspecify proxy node\n"
	    "\t\t-S <service>\tspecify service name\n"
	    "\t\t-v\t\tprint verbose\n",
	    progname);
}

//
// Effects:  Return an object reference to the PDT service if the service
//   is known to the system.  Otherwise, returns NULL.
//
static network::PDTServer_ptr
get_pdt_server()
{
	Environment			e;
	replica::rm_admin_var		rm_ref;
	replica::service_admin_var	controlp;
	CORBA::Object_var		nobjv;

	//
	// Get a reference to PDTServer object.
	//
	rm_ref = rm_util::get_rm();
	ASSERT(!CORBA::is_nil(rm_ref));
	controlp = rm_ref->get_control(network::PDTServer::NS_NAME, e);
	if (e.exception()) {
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op) {
			errno = ex_op->error;
			perror("get_pdt_server: Can't find control object\n");
		} else {
			e.exception()->print_exception("get_pdt_server: "
			    "Can't find control object\n");
		}
		e.clear();
		return (NULL);
	}

	//
	// Return the object reference of the current primary.
	//
	nobjv = controlp->get_root_obj(e);
	if (e.exception()) {
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op) {
			errno = ex_op->error;
			perror("get_pdt_server: Can't find root object\n");
		} else {
			e.exception()->print_exception("get_pdt_server: "
			    "Can't find root object\n");
		}
		e.clear();
		return (NULL);
	}

	//
	// Coerce corba object to PDTServer object
	//
	network::PDTServer_ptr pdtsp = network::PDTServer::_narrow(nobjv);
	if (CORBA::is_nil(pdtsp)) {
		e.clear();
		print_error("Cannot coerce to PDTService obj ref\n");
		return (NULL);
	}
	ASSERT(is_not_nil(pdtsp));

	return (pdtsp);
}

static void
print_fwd()
{
	network::fwdseq_t_var fwdinfo;
	network::group_t gname;
	Environment e;
	uint_t i, j;

	//
	// Forwarding information only resides on the gifnode
	//
	if (gifnode == 0) {
		print_usage(
		    "no gifnode specified for forwarding information.\n");
		return;
	}

	//
	// We currently only print out information for single services.
	//
	if (servicename == NULL) {
		print_usage(
		    "no service specified for forwarding information.\n");
		return;
	}
	(void) strlcpy(gname.resource, servicename,
	    (size_t)network::resource_name_len);

	//
	// Get the forwarding information.  If fwdnode is 0 we retrieve
	// forwarding information for all the nodes (fwdinfo is a sequence).
	//
	mcobj->mc_gns_fwd(fwdinfo, fwdnode, gname, e);
	if (e.exception() != NULL) {
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op) {
			errno = ex_op->error;
			perror(gname.resource);
		} else {
			e.exception()->print_exception(gname.resource);
		}
		e.clear();
		return;
	}

	if (fwdinfo.length() == 0)
		(void) fprintf(outfp, "\tNo forwarding lists.\n");
	else
		(void) fprintf(outfp, "%d forwarding lists\n",
		    fwdinfo.length());

	for (j = 0; j < fwdinfo.length(); j++) {
		//
		// We'll may get back empty sequence elements if we
		// specified a fwdnode (the prior elements will be empty).
		//
		if ((fwdnode && (fwdnode != fwdinfo[j].fw_node)) ||
		    (fwdinfo[j].fw_ncid == 0))
			continue;

		(void) fprintf(outfp, "\t%d cids in forwarding list for "
		    "node %d\n", fwdinfo[j].fw_ncid, fwdinfo[j].fw_node);

		if (verbose) {
			for (i = 0; i < (uint_t)fwdinfo[j].fw_ncid; i++) {
				(void) fprintf(outfp, "[%s, %u] -> "
				    "%d:[%s, %u]\n",
				    ipaddr_to_string(
					fwdinfo[j].fw_cidseq[i].ci_faddr),
				    fwdinfo[j].fw_cidseq[i].ci_fport,
				    fwdinfo[j].fw_node,
				    ipaddr_to_string(
					fwdinfo[j].fw_cidseq[i].ci_laddr),
				    fwdinfo[j].fw_cidseq[i].ci_lport);
			}
		}
	}
}

static void
print_bind()
{
	network::gns_bindseq_t_var bindseq;
	network::group_t gname;
	Environment e;
	bool as_gif;
	int32_t timeout;
	uint_t i;

	if (servicename == NULL || strlen(servicename) == 0) {
		print_usage("Invalid service name\n");
		return;
	}

	if (gifnode != 0 && proxynode != 0) {
		print_usage("Can't specify both -G and -N with option -b\n");
		return;
	}

	(void) strlcpy(gname.resource, servicename,
	    (size_t)network::resource_name_len);

	as_gif = (gifnode != 0? true: false);
	mcobj->mc_gns_bind(gname, as_gif, bindseq, timeout, e);
	if (e.exception()) {
		print_error("mc_gns_bind() failed\n");
		return;
	}

	if (!as_gif) {
		(void) fprintf(outfp, "Binding table for %s on node %d "
		    "as proxy node; timeout %d secs\n",
		    servicename, (gifnode? gifnode: proxynode),
		    timeout);

		if (bindseq.length() == 0) {
			(void) fprintf(outfp, "\t(No binding table found)\n");
			return;
		}

		(void) fprintf(outfp, "\t%-36s%-7s%-10s%-10s\n",
		    "clientaddr", "nodeid", "count", "state");

		for (i = 0; i < bindseq.length(); i++) {
			(void) fprintf(outfp, "\t%-36s%-7d%-10d%-10s\n",
			    ipaddr_to_string(bindseq[i].gb_faddr),
			    bindseq[i].gb_nodeid,
			    bindseq[i].gb_count,
			    bindstate_to_string(bindseq[i].gb_state));
		}
	} else {
		(void) fprintf(outfp, "Binding table for %s on node %d "
		    "as GIF node\n",
		    servicename, (gifnode? gifnode: proxynode));

		if (bindseq.length() == 0) {
			(void) fprintf(outfp, "\t(No binding table found)\n");
			return;
		}

		(void) fprintf(outfp, "\t%-36s%-7s\n",
		    "clientaddr", "nodeid");

		for (i = 0; i < bindseq.length(); i++) {
			(void) fprintf(outfp, "\t%-36s%-7d\n",
			    ipaddr_to_string(bindseq[i].gb_faddr),
			    bindseq[i].gb_nodeid);
		}
	}
}

static void
print_hash()
{
	network::seqlong_t_var	vanilla;
	network::seqlong_t_var	stickywildcard;
	Environment		e;
	uint_t			i;
	bool			found = false;

	//
	// Fastlookup hash table information only resides on the gifnode.
	//
	if (gifnode == 0) {
		print_usage(
		    "no gifnode specified for hashtable information.\n");
		return;
	}

	//
	// Obtain the hash table chain lengths for all hash table elements.
	//
	mcobj->mc_gns_ht(vanilla, stickywildcard, e);
	if (e.exception() != NULL) {
		sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

		if (ex_op) {
			errno = ex_op->error;
			perror("Error in mc_gns_ht()");
		} else {
			e.exception()->print_exception("Error in mc_gns_ht()");
		}
		e.clear();
		return;
	}

	//
	// Print out only the non-zero elements.
	//
	found = false;
	(void) fprintf(outfp, "vanilla_ht hash table:\n");
	if (vanilla.length()) {
		for (i = 0; i < vanilla.length(); i++)
			if (vanilla[i]) {
				(void) fprintf(outfp, "\t%d: %d\n", i,
				    vanilla[i]);
				found = true;
			}
	}
	if (!found)
		(void) fprintf(outfp, "\tempty\n");

	found = false;
	(void) fprintf(outfp, "stickywildcard_ht hash table:\n");
	if (stickywildcard.length()) {
		for (i = 0; i < stickywildcard.length(); i++)
			if (stickywildcard[i]) {
				(void) fprintf(outfp, "\t%d: %d\n",
				    i, stickywildcard[i]);
				found = true;
			}
	}
	if (!found)
		(void) fprintf(outfp, "\tempty\n");
}

static void
print_pdt(network::PDTServer_ptr pdts)
{
	Environment e;
	network::pdtinfo_t_var pdtinfo;
	network::group_t gname;
	uint_t len, i;
	nodeid_t nodearray[NODEID_MAX + 1];
	nodeid_t maxnodeid = 0;

	//
	// We only allow printing out a specific services PDT.
	//
	if (servicename == NULL) {
		print_usage("no service specified.\n");
		return;
	}
	(void) strlcpy(gname.resource, servicename,
	    (size_t)network::resource_name_len);

	//
	// Get the PDT from the GIFnode if specified, otherwise get
	// it from the PDTServer.
	//
	if (mcobj) {
		(void) fprintf(outfp, "PDTinfo from GIFnode %d for "
		    "service %s\n", gifnode, servicename);
		mcobj->mc_gns_pdt(pdtinfo, gname, e);
		if (e.exception() != NULL) {
			sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

			if (ex_op) {
				errno = ex_op->error;
				perror(gname.resource);
			} else {
				e.exception()->print_exception(
				    gname.resource);
			}
			e.clear();
			return;
		}
	} else if (pdts) {
		(void) fprintf(outfp, "PDTinfo from PDTServer %d for "
		    "service %s\n", pdtnode, servicename);
		pdts->gns_pdt(pdtinfo, gname, e);
		if (e.exception() != NULL) {
			sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

			if (ex_op) {
				errno = ex_op->error;
				perror(gname.resource);
			} else {
				e.exception()->print_exception(
				    gname.resource);
			}
			e.clear();
			return;
		}
	} else {
		print_usage("no service specified.\n");
		return;
	}

	//
	// Calculate node distribution summary information.
	//
	len = pdtinfo->hashinfoseq.length();
	bzero(nodearray, sizeof (nodeid_t) * (NODEID_MAX + 1));
	for (i = 0; i < len; i++) {
		if (pdtinfo->hashinfoseq[i].old_hashval > maxnodeid)
			maxnodeid = pdtinfo->hashinfoseq[i].old_hashval;
		nodearray[pdtinfo->hashinfoseq[i].old_hashval]++;
	}
	(void) fprintf(outfp, "\tPDT summary:\n");
	for (i = 1; i <= maxnodeid; i++) {
		if (nodearray[i] != 0)
			(void) fprintf(outfp, "\t\tnode %d: %d buckets\n",
			    i, nodearray[i]);
	}

	//
	// Print out the entire table if asked.
	//
	if (verbose) {
		int j;
		(void) fprintf(outfp, "\tPDT contents:\n");
		for (i = 0, j = 1; i < len; i++, j++) {
			(void) fprintf(outfp, "%d ",
			    pdtinfo->hashinfoseq[i].old_hashval);
			if (j == NUM_HASH_PER_LINE) {
				(void) fprintf(outfp, "\n");
				j = 0;
			}
		}
		(void) fprintf(outfp, "\n");
	}
}

static void
print_services(network::PDTServer_ptr pdts)
{
	Environment e;
	network::grpinfoseq_t_var sinfo;
	network::group_t gname;
	uint_t i, j, k;
	uint_t count, instcount;

	//
	// servicename can be NULL here to specify that
	// we print out all services.
	//
	if (servicename) {
		(void) strlcpy(gname.resource, servicename,
		    (size_t)network::resource_name_len);
	} else {
		*gname.resource = NULL;
	}

	//
	// Get the services from the GIFnode if specified, otherwise get
	// them from the PDTServer.
	//
	if (mcobj) {
		(void) fprintf(outfp, "Services from mcobj on node %d:\n",
		    gifnode);
		mcobj->mc_gns_service(sinfo, gname, e);
		if (e.exception() != NULL) {
			sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

			if (ex_op) {
				errno = ex_op->error;
				perror(gname.resource);
			} else {
				e.exception()->print_exception(
				    gname.resource);
			}
			e.clear();
			return;
		}
	} else if (pdts) {
		(void) fprintf(outfp, "Services from PDTServer on node %d:\n",
		    pdtnode);
		pdts->gns_service(sinfo, gname, e);
		if (e.exception() != NULL) {
			sol::op_e *ex_op = sol::op_e::_exnarrow(e.exception());

			if (ex_op) {
				errno = ex_op->error;
				perror(gname.resource);
			} else {
				e.exception()->print_exception(
				    gname.resource);
			}
			e.clear();
			return;
		}
	} else {
		print_usage("no service specified.\n");
		return;
	}

	//
	// Parse the service information sequence and print it out.
	//
	for (i = 0; i < sinfo->length(); i++) {

		(void) fprintf(outfp, "group name = %s",
		    sinfo[i].srv_group.resource);

		//
		// If we're talking to the PDTServer, we can print
		// out the config/group instance lists.  Otherwise
		// we're talking to the GIFnode, which returns empty
		// lists for those elements.
		//
		if (!mcobj) {
			count = sinfo[i].srv_config->length();
			(void) fprintf(outfp, ", cfginst");
			if (count) {
				for (j = 0; j < count; j++)
					(void) fprintf(outfp, " %d",
					    sinfo[i].srv_config[j]);
			} else {
				(void) fprintf(outfp, " 0");
			}

			count = sinfo[i].srv_grpinst->length();
			(void) fprintf(outfp, ", grpinst");
			if (count) {
				for (j = 0; j < count; j++)
					(void) fprintf(outfp, " %d",
					    sinfo[i].srv_grpinst[j]);
			} else {
				(void) fprintf(outfp, " 0");
			}
		}
		(void) fprintf(outfp, "\n");

		if (!sinfo[i].srv_pdt) {
			(void) fprintf(outfp, "\tNO PDT, ");
		} else {
			(void) fprintf(outfp, "\t%d PDT buckets, ",
			    sinfo[i].srv_nhash);
		}

		(void) fprintf(outfp, "policy %s",
		    lbpolicy_to_string(sinfo[i].srv_policy));

		if (sinfo[i].srv_policy == network::LB_ST ||
		    sinfo[i].srv_policy == network::LB_SW) {
			char st_buf[128] = "";
			ulong_t st_flags;

			st_flags = sinfo[i].srv_stconfig.st_flags;

			(void) sprintf(st_buf, "%s%s",
			    (st_flags & network::ST_UDP? "U": ""),
			    (st_flags & network::ST_WEAK? "W": ""));

			if (strlen(st_buf) != 0)
				(void) fprintf(outfp, " (%s)", st_buf);
		}

		count = sinfo[i].srv_lb_weights->length();
		if (count > 1) {
			//
			// We skip the first element since it may
			// currently contain garbage (see marshal_sgrp).
			// Nodes which do not have a weight are also
			// suppressed, since we do not know whether their
			// weight is zero or they are down.
			//
			for (j = 1; j < count; j++) {
				if (sinfo[i].srv_lb_weights[j]) {
					(void) fprintf(outfp, " n%d(%d)", j,
					    sinfo[i].srv_lb_weights[j]);
				}
			}
		}

		(void) fprintf(outfp, "\n");

		count = sinfo[i].srv_obj.length();
		for (j = 0; j < count; j++) {
			(void) fprintf(outfp, "    %d. protocol=%s, ipaddr=%s, "
			    "port=%d, gifnode=%d", j + 1,
			    protocol_to_string(
				sinfo[i].srv_obj[j].srv_sap.protocol),
			    ipaddr_to_string(
				sinfo[i].srv_obj[j].srv_sap.laddr),
			    sinfo[i].srv_obj[j].srv_sap.lport,
			    sinfo[i].srv_obj[j].srv_gifnode);
			if (!mcobj) {
				(void) fprintf(outfp, ", srvinst");
				instcount =
				    sinfo[i].srv_obj[j].srv_inst->length();
				if (instcount) {
					for (k = 0; k < instcount; k++)
						(void) fprintf(outfp, " %d",
					    sinfo[i].srv_obj[j].srv_inst[k]);
				} else {
					(void) fprintf(outfp, " 0");
				}
			}
			(void) fprintf(outfp, "\n");
		}
		(void) fprintf(outfp, "\n");
	}
}

//
// Effects:  Given the protocol "proto" return the character string
//   that represents that protocol.  Returns the empty string ""
//   if "proto" corresponds to no known protocol.
//
static char *
protocol_to_string(uint8_t proto)
{
	switch (proto) {
	case IPPROTO_ICMP:
		return ("ICMP");
	case IPPROTO_GGP:
		return ("GGP");
	case IPPROTO_TCP:
		return ("TCP");
	case IPPROTO_EGP:
		return ("EGP");
	case IPPROTO_PUP:
		return ("PUP");
	case IPPROTO_UDP:
		return ("UDP");
	case IPPROTO_IDP:
		return ("IDP");
	case IPPROTO_RSVP:
		return ("RSVP");
	case IPPROTO_HELLO:
		return ("HELLO");
	case IPPROTO_ND:
		return ("ND");
	case IPPROTO_EON:
		return ("EON");
	case IPPROTO_RAW:
		return ("RAW");
#ifdef SCTP_RGM_SUPPORT
	case IPPROTO_SCTP:
		return ("SCTP");
#endif
	default:
		print_error("protocol=0x%x\n", proto);
		return ("UNKNOWN PROTOCOL");
	}
}

//
// Effects:  Given the load-balancing policy "policy" return the
//   character string that represents that policy.
//
static char *
lbpolicy_to_string(network::lb_policy_t policy)
{
	switch (policy) {
		case network::LB_WEIGHTED:
			return ("LB_WEIGHTED");
		case network::LB_PERF:
			return ("LB_PERF");
		case network::LB_USER:
			return ("LB_USER");
		case network::LB_ST:
			return ("LB_ST");
		case network::LB_SW:
			return ("LB_SW");
		default:
			return ("UNKNOWN POLICY");
	} // end switch
}


char *
bindstate_to_string(uint32_t state)
{
	char *retval = NULL;

	switch (state) {
	case network::BI_CONNECTED:
		retval = "CONNECTED";
		break;
	case network::BI_CLOSED:
		retval = "CLOSED";
		break;
	case network::BI_DEPRECATED:
		retval = "DEPRECATED";
		break;
	default:
		retval = "???";
		break;
	}
	return (retval);
}


//
// Returns the string representation for the given IP address.
//
// If address is V4-mapped, the string is the dotted decimal form, as in
// 192.9.76.112.
//
// If address is not V4-mapped, but V6-unspec (all zeros), then "::" is
// returned.
//
// Otherwise, the 128-bit address is broken up into 4 32-bit integers,
// printed as hex in the string returned, separated by ":".
//
// NOTE: inet_ntop() is not used so that we can see exactly what values
// are in the address without confusion, which is important for a
// diagnostic program like this one.
//
// NOTE: This function does not allocate memory. The caller can call this
//   function at MOST twice in a single shot. For example:
//
//	printf("<%s, %s>", ipaddr_to_string(a1), ipaddr_to_string(a2));
//
//   works as expected. Adding one more address though will
//   cause one of the string buffers for addr1 or addr2 to be overwritten.
//
static char *
ipaddr_to_string(const network::inaddr_t &addr)
{
	static char b[2][64];		// allocate 2 buffers
	static uint_t index = 0;	// index flips between 0 and 1
	char *bp;

	//
	// Use b[index] for this current conversion. Flip 'index'
	// so that the other buffer will be used for the next conversion.
	//
	bp = b[index];
	index ^= 1;

	if (IN6_IS_ADDR_V4MAPPED((in6_addr_t *)&addr)) {
		uchar_t *cp = (uchar_t *)(&addr.v6[3]);

		(void) sprintf(bp, "%d.%d.%d.%d",
		    cp[0], cp[1], cp[2], cp[3]);
	} else if (IN6_IS_ADDR_UNSPECIFIED((in6_addr_t *)&addr)) {
		(void) sprintf(bp, "::");
	} else {
		//
		// The "preferrable" form according to RFC 2373 is
		// 8 16-bit hex separated by ':'.
		//
		uint16_t *up = (uint16_t *)&addr.v6[0];
		(void) sprintf(bp, "%x:%x:%x:%x:%x:%x:%x:%x",
		    up[0], up[1], up[2], up[3], up[4], up[5], up[6], up[7]);
	}

	return (bp);
}


#ifdef UNODE

int
unode_init()
{
	(void) printf("print_net_state loaded\n");

	return (0);
}

// The first time print_net_state is loaded with unode_load it
// will have all its global variables initialized and unode_init
// called, but the module stays resident and subsequent loads
// won't call unode_init or re-initialize the globals, so we
// have main call unode_reset to reset the globals.

static void
unode_reset()
{
	servicename = NULL;
	fwdnode = 0;
	gifnode = 0;
	mcobj = NULL;
	pdtnode = 0;
	verbose = false;
	outfp = stdout;
	errfp = stderr;

	// reset variables used by getopt
	// (from usr/src/lib/libc/port/gen/opt_data.c)

	opterr = 1;
	optind = 1;
	optopt = 0;
	optarg = 0;

	extern int _sp;
	_sp = 1;	// not in opt_data.c, but in getopt.c
}

#endif
