#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)config_ipv6.sh	1.6	08/05/27 SMI"

#
# /etc/system variable ifk_disable_v6 must be set to 0 in order to run
# IPv6 scalable service on a cluster node. This script can be run to avoid
# a reboot.
#
# This script looks up the CCR for a list of transport adapters and
# configures an IPv6 interface with link-local address. This allows IPv6
# packets to be forwarded over the cluster interconnect to support IPv6
# scalable services.
#
# The -u option can be used to undo IPv6 plumbing done by this script.
#

# only allowed from global zone
/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi



# Exit if not in cluster


/usr/sbin/clinfo >/dev/null
if [ $? -ne 0 ]; then
	echo "Error: Not in cluster mode"
	exit 1
fi

PATH=/usr/bin:/usr/sbin
CCRFILE=/etc/cluster/ccr/global/infrastructure

while getopts ":u" opt; do
	case $opt in
		u)	do_unconfig=true ;;
		*)	echo "Usage: $0 [-u]"
			exit 1
			;;
	esac
done

#
# Check for correct ifk_disable_v6 setting in /etc/system
#
if [ -n "$do_unconfig" ]; then
	egrep -e "^set cl_comm:ifk_disable_v6=0" /etc/system >/dev/null
	if [ $? -eq 0 ]; then
		echo "Error: ifk_disable_v6 must not be set to 0 in /etc/system"
		exit 1
	fi
else
	egrep -e "^set cl_comm:ifk_disable_v6=0" /etc/system >/dev/null
	if [ $? -ne 0 ]; then
		echo "Error: ifk_disable_v6 must be set to 0 in /etc/system"
		exit 1
	fi
fi

#
# Determine my node id. Then read CCR to find all transport adapters.
#
nodeid=`/usr/sbin/clinfo -n`
search_key="nodes.${nodeid}.adapters.[0-9]+.name"
adapter_list=`egrep "${search_key}" $CCRFILE | awk '{ print $2 }'`

if [ "$adapter_list" = "" ]; then
	echo "Error: No cluster transport adapters found"
	exit 1
fi

#
# For every transport adapter, bring up an IPv6 interface with link-local
# address.
# Remove IPv6 interface if -u is used.
#
for i in $adapter_list; do
	/sbin/ifconfig $i >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		if [ -n "$do_unconfig" ]; then
			/sbin/ifconfig $i inet6 >/dev/null 2>&1
			if [ $? -eq 0 ]; then
				# Unplumb if interface is configured
				/sbin/ifconfig $i inet6 unplumb
				processed_list="$processed_list $i"
			fi
		else
			/sbin/ifconfig $i inet6 >/dev/null 2>&1
			if [ $? -ne 0 ]; then
				# Plumb if interface is not configured
				/sbin/ifconfig $i inet6 plumb up
				processed_list="$processed_list $i"
			fi
		fi
	fi
done

#
# Add IPv6 loopback interface if not yet configured.
# This is done by Solaris during boot up, but since we're
# bringing up interfaces dynamically above, we need to be sure.
# Not having loopback IPv6 could hang apps connecting to localhost.
#
# Remove loopback if -u is used.
#
if [ -n "$do_unconfig" ]; then
	/sbin/ifconfig lo0 inet6 >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		/sbin/ifconfig lo0 inet6 unplumb
		processed_list="$processed_list lo0"
	fi
else
	/sbin/ifconfig lo0 inet6 >/dev/null 2>&1
	if [ $? -ne 0 ]; then
		/sbin/ifconfig lo0 inet6 plumb ::1 up
		processed_list="$processed_list lo0"
	fi
fi


if [ "$processed_list" != "" ]; then
	if [ -n "$do_unconfig" ]; then
		echo "Unconfigured IPv6 interfaces on $processed_list"
	else
		echo "Configured IPv6 interfaces on $processed_list"
	fi
fi

exit 0

