#! /usr/xpg4/bin/sh -p

#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)ips_package_uninstall.ksh	1.4	09/02/20 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

PROGNAME=ips_package_uninstall

# test that we've got exactly two args:
#	$SC_SCADMINDIR, which will be different at install and remove times
#	logfile name

if [[ $# -ne 2 ]]; then
	exit ${IPS_FAIL}  # early exit
fi

SCADMINDIR=$1
LOG=$2

IPS_LIB=${SCADMINDIR}/lib/ips_package_common

echo "${PROGNAME}: --ENTER-- $(date)" >> ${LOG}

# hold signature files from group-packages
SC_IPS_GP_SIGFILES_DIR=${SC_SCADMINDIR}/ips/sig-gp
echo "\tSC_IPS_GP_SIGFILES_DIR: ${SC_IPS_GP_SIGFILES_DIR}" >> ${LOG}

# holds signature files from "loose" packages
SC_IPS_PKG_SIGFILES_DIR=${SC_SCADMINDIR}/ips/sig-pkg
echo "\tSC_IPS_PKG_SIGFILES_DIR: ${SC_IPS_PKG_SIGFILES_DIR}" >> ${LOG}

TMPF=/tmp/.scinstall.${PROGNAME}.$$

# load IPS package processing library
. ${IPS_LIB}
if [[ $? != 0 ]]; then
    printf "$(gettext '%s: Unable to load \"%s\"')\n" "${PROGNAME}" "${IPS_LIB}" >&2
    echo "\tUnable to load: ${IPS_LIB}" >> ${LOG}
    return 1
fi

# test scadmindir
if [[ ! -d ${SCADMINDIR} ]]; then
	exit ${IPS_FAIL}  # early exit
fi

result=${IPS_OK}

# ...and do the bulk uninstall
echo "\t${PROGNAME}: removing packages..."
echo "\n/usr/bin/pkg uninstall ha-cluster*\n" >> ${LOG}

/usr/bin/pkg uninstall ha-cluster* | /usr/bin/tee -a ${LOG} 2>&1
result=$?

if [[ $result -eq ${IPS_OK} ]]; then
    echo "${PROGNAME}: --EXIT success-- $(date)" >> ${LOG}
    echo "\t${PROGNAME}: done"
else
    echo "${PROGNAME}: --EXIT failure-- $(date)" >> ${LOG}
fi

exit ${result}
