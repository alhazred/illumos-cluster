#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)scinstall_ql_interactive.ksh	1.6	08/08/14 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

####################################################
#
# interactive_help_ql_plan
#
# 	Print help information for the QL 
#	plan submenu.
#
####################################################
interactive_help_ql_plan()
{
	typeset sctxt_title_help_ql_plan="$(gettext '*** Help Screen - Display and select possible partitioning schemes ***')"

	typeset sctxt_para_1="$(gettext '
		Use this option to display and optionally select
		partitioning schemes for a dual-partition upgrade.
	')"

	(
		sc_print_title "${sctxt_title_help_ql_plan}"
		sc_print_para "${sctxt_para_1}"

	) | more

	return 0
}

####################################################
#
# interactive_help_ql_begin
#
# 	Print help information for the QL 
#	begin submenu.
#
####################################################
interactive_help_ql_begin()
{
	typeset sctxt_title_help_ql_begin="$(gettext '*** Help Screen - Begin the dual-partition upgrade process ***')"

	typeset sctxt_para_1="$(gettext '
		Use this option to prepare the cluster for a
		dual-partition upgrade.
	')"

	(
		sc_print_title "${sctxt_title_help_ql_begin}"
		sc_print_para "${sctxt_para_1}"

	) | more


	return 0
}

####################################################
#
# interactive_help_ql_apply
#
# 	Print help information for the QL 
#	apply submenu.
#
####################################################
interactive_help_ql_apply()
{
	typeset sctxt_title_help_ql_apply="$(gettext '*** Help Screen - Apply dual-partition upgrade changes ***')"

	typeset sctxt_para_1="$(gettext '
		Use this option to continue a dual-partition upgrade
		after all software on the nodes in this partition have
		been upgraded.
	')"

	(
		sc_print_title "${sctxt_title_help_ql_apply}"
		sc_print_para "${sctxt_para_1}"

	) | more

	return 0
}

####################################################
#
# interactive_help_ql_status
#
# 	Print help information for the QL 
#	status submenu.
#
####################################################
interactive_help_ql_status()
{
	typeset sctxt_title_help_ql_status="$(gettext '*** Help Screen - Display status of the upgrade ***')"

	typeset sctxt_para_1="$(gettext '
		Use this option to display the status of the
		dual-partition upgrade.
	')"

	(
		sc_print_title "${sctxt_title_help_ql_status}"
		sc_print_para "${sctxt_para_1}"

	) | more

	return 0
}

####################################################
#
# interactive_ql_help
#
# 	Print help information for the QL submenu.
#
####################################################
interactive_ql_help()
{
	typeset sctxt_title_help_ql_upgrade="$(gettext '*** Help Screen - Upgrade Sun Cluster software on this cluster node ***')"

	while true
	do
		case $(interactive_get_ql_menuoption help) in
		'1')	# Option 1
			interactive_help_ql_plan
			;;

		'2')	# Option 2
			interactive_help_ql_begin
			;;

		'3')	# Option 3
			sc_print_title "${sctxt_title_help_ql_upgrade}"
			interactive_help_main_upgrade
			;;

		'4')	# Option 4	
			interactive_help_ql_apply
			;;

		'5')	# Option 5
			interactive_help_ql_status
			;;

		'q')	# Return
			break
			;;
		esac

		echo
		sc_print_prompt "$(gettext 'Press Enter to return to the previous menu:')"
		if [[ $? -ne 0 ]]; then
			return 1
		fi
		read
		echo
	done

	return 0
}

####################################################
#
# interactive_get_ql_menuoption() [help]
#
#	help	- if given, print the install help menu
#
#	Print the ql menu, and return the selected option.
#	If the "help" option is given, the menu is processed
#	as the ql help menu.
#
#	This function always returns zero.
#
####################################################
interactive_get_ql_menuoption()
{
	typeset help=${1}

	typeset option
	typeset select_1=S
	typeset select_2=S
	typeset select_3=S
	typeset select_4=S
	typeset select_5=S
	typeset retval

	typeset sctxt_title_1="$(gettext '*** Manage a Dual-Partition Upgrade Menu ***')"
	typeset sctxt_title_2="$(gettext 'Select from any one of the following (*) options:')"
	typeset sctxt_title_1_help="$(gettext '*** Manage a Dual-Partition Upgrade Menu Help **')"
	typeset sctxt_title_2_help="$(gettext 'Select from any one of the following HELP options:')"
	typeset sctxt_option_001="$(gettext 'Display and select possible partitioning schemes')"
	typeset sctxt_option_002="$(gettext 'Begin the dual-partition upgrade process')"
	typeset sctxt_option_003="$(gettext 'Upgrade Sun Cluster software on this cluster node')"
	typeset sctxt_option_004="$(gettext 'Apply dual-partition upgrade changes')"
	typeset sctxt_option_005="$(gettext 'Display status of the upgrade')"
	typeset sctxt_option_help="$(gettext 'Help with menu options')"
	typeset sctxt_option_return_ql="$(gettext 'Return to the Manage Dual-Partition Upgrade Menu')"
	typeset sctxt_option_return_main="$(gettext 'Return to the Main Menu')"

	if [[ -n "${help}" ]]; then
		option=$(sc_get_menuoption \
			"T1+++${sctxt_title_1_help}" \
			"T2+++${sctxt_title_2_help}" \
			"S+0+1+${sctxt_option_001}" \
			"S+0+2+${sctxt_option_002}" \
			"S+0+3+${sctxt_option_003}" \
			"S+0+4+${sctxt_option_004}" \
			"S+0+5+${sctxt_option_005}" \
			"R+++" \
			"S+0+q+${sctxt_option_return_ql}" \
		)
	else

		ql_get_internal_status
		retval=$?
		case $retval in
			'1')
				# PROG_ERR
				# Some error has occured. The QL script is unable
				# to return a status.
				# Disable all the menus except status
				select_1=N
				select_2=N
				select_3=N
				select_4=N
				select_5=S
				;;
			'2')
				# NO_UPGRADE_STATE
				# Enable the plan, apply, update and status
				# Disable apply
				select_1=S
				select_2=S
				select_3=S
				select_4=N
				select_5=S
				;;
			'3')
				# UPGRADE_COMPLETE_STATE
				# Enable the plan, apply, update and status
				# Disable apply
				select_1=S
				select_2=S
				select_3=S
				select_4=N
				select_5=S
				;;
			'4')
				# UPGRADE_PENDING_COMPLETE_STATE
				# Some QL command is still executing
				# Disable all the menus except status
				select_1=N
				select_2=N
				select_3=N
				select_4=N
				select_5=S
				;;
			'5')
				# UPGRADE_BEGIN_STATE
				# The apply/begin command has been executed
				# Enable update, apply, status
				# Disable apply, begin
				select_1=N
				select_2=N
				select_3=S
				select_4=S
				select_5=S
				;;
			'6')
				# UPGRADE_STATE_PRE_PARTB_SHUTDOWN
				# Upgrade is in progress
				# Disable all the menus except status
				select_1=N
				select_2=N
				select_3=N
				select_4=N
				select_5=S
				;;
			'7')
				# UPGRADE_PARTA_STATE
				# apply command has been executed on partition 1
				# Enable update, apply, status
				# Disable plan, begin
				select_1=N
				select_2=N
				select_3=S
				select_4=S
				select_5=S
				;;
			'8')
				# UPGRADE_ERR_STATE
				# Error has occured during upgrade
				# Disable all the menus except status
				select_1=N
				select_2=N
				select_3=N
				select_4=N
				select_5=S
				;;
		esac

		#
		# If run from the CDROM, disable the apply option
		#
		if [[ "${scinstalldir}" != "${SC_BINDIR}" ]]; then
			select_4=N
		fi

		option=$(sc_get_menuoption \
			"T1+++${sctxt_title_1}" \
			"T2+++${sctxt_title_2}" \
			"${select_1}+1+1+${sctxt_option_001}" \
			"${select_2}+1+2+${sctxt_option_002}" \
			"${select_3}+1+3+${sctxt_option_003}" \
			"${select_4}+1+4+${sctxt_option_004}" \
			"${select_5}+1+5+${sctxt_option_005}" \
			"R+++" \
			"S+1+\?+${sctxt_option_help}" \
			"S+1+q+${sctxt_option_return_main}" \
		)
	fi

	echo "${option}"

	return 0
}

####################################################
#
# interactive_ql()
#
#	This is the quantum leap submenu.
#
#	Return values:
#		0	- proceed
#		1	- return to main menu
#
####################################################
interactive_ql()
{
	typeset retval

	# Loop around the main menu
	while true
	do
		case $(interactive_get_ql_menuoption) in
		'1')	interactive_ql_plan
			retval=$?
			case $retval in
				'0')
					# Stay in QL Menu
					continue
					;;
				'1')
					# Will go to Main Menu
					break
					;;
				*)
					# Error
					;;
			esac
			;;

		'2')	interactive_ql_begin
			retval=$?
			case $retval in
				'0')
					# Stay in QL Menu
					continue
					;;
				'1')
					# Will go to Main Menu
					break
					;;
				*)
					# Error
					;;
			esac
			;;
		'3')	interactive_upgrade || return 1 ;;
		'4')	interactive_ql_apply || return 1 ;;
		'5')	interactive_ql_status || return 1 ;;
		'?')	interactive_ql_help ;;
		'q')	break ;;
		esac
	done
	echo

	# Done
	return 0
}

####################################################
#
# interactive_ql_plan()
#
#	This is the quantum leap plan.
#
#	Return values:
#		0	- proceed - stay in QL MENU
#		1	- return to main menu
#
####################################################
interactive_ql_plan()
{
	typeset -i ql_num_nodes
	typeset retval

	typeset sctxt_title="$(gettext '
		>>> Display and Select Possible Partitioning Schemes <<<
	')"

	typeset sctxt_description_p1="$(gettext '
		Use this option to display and, optionally, select
		partitioning schemes for a dual-partition upgrade.
		This option is only useful for clusters of three or
		more nodes. Single-node clusters are not candidates for
		dual-partition upgrade. Two-node clusters only have one
		possible partitioning option.
	')"
	typeset sctxt_description_p2="$(gettext '
		As part of the dual-partition upgrade process,
		you must divide the cluster into two partitions, or
		subclusters. Each partition must be able to provide
		all client services while the partiton is active. The
		nodes in the first partition are booted into
		non-cluster mode, then upgraded, while the nodes in the
		second partition remain active.  Once upgraded, the
		nodes in first partition take over, so that the nodes
		of the second partition can then be upgraded.
	')"
	typeset sctxt_description_p3="$(gettext '
		To maximize availability, each of the two partitions
		that you create must be able to provide all client
		services on its own.
	')"
	typeset sctxt_description_p4="$(gettext '
		Since Sun Cluster does not require applications to
		register their storage or data access requirements with
		the cluster, the list of possible partitioning schemes
		presented here might include partitions which would
		fail to meet availability requirements.  Exercise care
		in selecting a partitioning scheme that will support
		full data access to each partition.  Make sure that the
		partitioning scheme that you use meets all of your data
		access requirements.
	')"

	typeset sctxt_description_two_node="$(gettext '
		This is a two-node cluster. Each node of a two-node
		cluster becomes its own partition.
	')"

	typeset sctxt_membership_message="$(gettext '
		This option requires all the nodes to be in cluster
		mode.  Use the \"clnode status\" command to see which
		nodes are offline.  Bring those nodes online and try
		again.
	')"

	clear
	{
		sc_print_title "${sctxt_title}"
		sc_print_para  "${sctxt_description_p1}"
		sc_print_para  "${sctxt_description_p2}"
		sc_print_para  "${sctxt_description_p3}"
		sc_print_para  "${sctxt_description_p4}"
	} | more

	answer=$(sc_prompt_yesno "$(gettext 'Do you want to continue?')" "${YES}") || return 2
	if [[ "${answer}" != "yes" ]]; then
		# Return to QL Menu
		return 0
	fi


	# Check if the all the node are in cluster mode
	ql_check_cluster_membership
	if [[ $? -eq ${SC_FALSE} ]]; then
		sc_print_para "${sctxt_membership_message}"
		sc_prompt_pause
		# Return to QL Menu
		return 0		
	fi

	# Check for the number of nodes in the cluster 
	ql_num_nodes="$(ql_get_numnodes)"

	
	case ${ql_num_nodes} in
	
	'1')	
		# Single node cluster
		sc_print_line "$(gettext 'This is a single-node cluster.')"
		printf "\n"
		sc_print_line "$(gettext 'Single-node clusters are not candidates for dual-partition upgrade.')"
		printf "\n\n\a"
		sc_prompt_pause
		# Return to Main Menu
		return 1
		;;

	'2') 	
		# Two Node cluster
		sc_print_para "${sctxt_description_two_node}"
		answer=$(sc_prompt_yesno "$(gettext 'Do you want to begin the dual-partition upgrade?')" "${YES}") || return 2
		if [[ "${answer}" != "yes" ]]; then
			# Return to QL Menu
			return 0
		fi

		#
		# Process the 2 node cluster dual partition upgrade
		#
		ql_2node_upgrade_begin
		retval=$?
		case $retval in
			'0')
				# Return to QL Menu
				return 0
				;;
			'1')
				# Return to Main Menu
				return 1
				;;
			*)
				# Error
				;;
		esac
		;;
	*)	
		ql_node_upgrade_begin
		retval=$?
		case $retval in
			'0')
				# Return to QL Menu
				return 0
				;;
			'1')
				# Return to Main Menu
				return 1
				;;
			*)
				# Error
				;;
		esac
		;;
	esac

	
	# Return to QL Menu
	return 0

}

####################################################
#
# ql_node_upgrade_begin()
#
#	This function will be used to process the
#	begin command in interactive mode for a
#	3 or more node cluster.  It should be called when
#	the node is in cluster mode.
#
#	Return values:
#		0 	- go back to QL Menu
#		1 	- return to main menu
#
####################################################
ql_node_upgrade_begin()
{
	typeset ptool_op="/var/cluster/run/ptool_op.txt"
	typeset answer=
	typeset -i num_option
	typeset retval

	typeset sctxt_question_1="$(gettext 'Do you want to select a set of partitions for upgrade?')"
	typeset sctxt_question_2="$(gettext 'Partitioning option')"

	typeset sctxt_description_many_node="$(gettext 'The following is a list of possible cluster partitioning options:')"


	# More than 2 node cluster
	clear
	sc_print_para "${sctxt_description_many_node}"

	#
	# Execute the partition tool and store the o/p in a file
	#
	if [[ "${scinstalldir}" != "${SC_BINDIR}" ]]; then
        	# Running from Tools directory
               	${SC_SCLIBDIR}/${SC_QL_UPGRADE_PTOOL} > ${ptool_op}
        else
		# Running from Host
               	${SC_QL_BINDIR}/${SC_QL_UPGRADE_PTOOL} > ${ptool_op}	
	fi

	#
	# Display the output of the partition tool
	#
	cat ${ptool_op} | more
	printf "\n"

	#
	# Ask him if he wants to select a partition
	#
	answer=$(sc_prompt_yesno "${sctxt_question_1}" "${YES}") || return 2
	if [[ "${answer}" != "yes" ]]; then
		# Return to QL Menu
		return 0
	fi

	#
	# User has selected Yes i.e He/She wants to select a partition
	#
	num_option="$(ql_get_num_options)"

	while true
	do
		answer=$(ql_prompt_select_partition_option "${sctxt_question_2}" "${num_option}" "list")
		case ${answer} in
			LIST)
				#
				# Re list the output of the partition tool
				#
				clear
				sc_print_para "${sctxt_description_many_node}"
				cat ${ptool_op} | more
				printf "\n"
				answer=
				;;
			QUIT)
				# Return to QL Menu
				return 0
				;;

			CUSTOM)
				ql_process_custom_partition
				retval=$?
				case $retval in
					'0')
						# Return to QL Menu
						return 0
						;;
					'1')
						# Return to Main Menu
						return 1
						;;
					'5')
						# Stay in the option selection prompt
						# Which partitioning option do you select (1... ?
						continue
						;;
					*)
						# Error
						;;
				esac
				;;

			*)
				#
				# User has selected an option
				# Do the necessary processing
				#
				ql_process_partition_option ${answer}
				retval=$?
				case $retval in
					'0')
						# Return to QL Menu
						return 0
						;;
					'1')
						# Return to Main Menu
						return 1
						;;
					'5')
						# Stay in the option selection prompt
						# Which partitioning option do you select (1... ?
						continue
						;;
					*)
						# Error
						;;
				esac
				;;
		esac
	done

	sc_prompt_pause

	# Return to QL Menu
	return 0

}

####################################################
#
# ql_process_custom_partition()
#
#	This function will display one by one all the
#	nodes of the cluster and ask the user to select
#	the partition in which he wants to assign the node to.
#
#	Return values:
#		0	- return to QL Menu
#		1	- return to main menu
#		5	- go to partition option selection prompt
#		*	- error
#
####################################################
ql_process_custom_partition()
{
	typeset node=
	typeset node_name_list=
	typeset partition_1=
	typeset partition_2=
	typeset partition_comma_list_1=
	typeset partition_comma_list_2=
	typeset partition_comma_list=
	typeset -i part_1_num=0
	typeset -i part_2_num=0
	typeset -i flip_flag=0
	typeset option=
	typeset answer=

	typeset sctxt_title="$(gettext '>>> Dual Partition Upgrade of Multi-Node Cluster <<<')"

	typeset sctxt_description_p1="$(gettext '
		As part of the dual-partition upgrade process for
		a multi-node cluster, each of the nodes is assigned to
		a partition, or subcluster.  The node in the first
		partition is booted into non-cluster mode, then
		upgraded, while the node in the second partition
		remains active.  Once upgraded, the nodes in the first
		partition will take over, so that the nodes in the
		second partition can then be upgraded.
	')"
	typeset sctxt_description_p2="$(gettext '
		The nodes assigned to the first partition will be
		booted first.
	')"

	typeset sctxt_partition="$(gettext 'Select the partition that you want to assign to')"
	typeset sctxt_partition_1="$(gettext 'has been assigned to the first partition.')"
	typeset sctxt_partition_2="$(gettext 'has been assigned to the second partition.')"

	typeset sctxt_prepare="$(gettext 'Preparing the cluster for dual-partition upgrade ... ')"

	typeset sctxt_option_001="$(gettext 'First Partition')"
	typeset sctxt_option_002="$(gettext 'Second Partition')"

	typeset sctxt_error_1="$(gettext '
		No nodes has been assigned to the first partition.
		Each partition must have at least one node.
		')"

	typeset sctxt_error_2="$(gettext '
		No nodes has been assigned to the second partition.
		Each partition must have at least one node.
		')"

	clear
	{
		sc_print_title "${sctxt_title}"
		sc_print_para  "${sctxt_description_p1}"
		sc_print_para  "${sctxt_description_p2}"
	} | more

	# Get the list of node names belonging to the cluster
	node_name_list="$(ql_get_nodelist)"

	for node in ${node_name_list}
	do
		prompt="${sctxt_partition}""\"""${node}""\":"		
		option="$(
			sc_get_menuoption	\
	 		"T2+++${prompt}" \
    			"S+0+1+${sctxt_option_001}" \
    			"S+0+2+${sctxt_option_002}" \
		)"

		case ${option} in
			'1')
				partition_1="${partition_1}${node} "
				partition_comma_list_1="${partition_comma_list_1}${node},"
				part_1_num=part_1_num+1
				;;
			'2')
				partition_2="${partition_2}${node} "
				partition_comma_list_2="${partition_comma_list_2}${node},"
				part_2_num=part_2_num+1
				;;
		esac
	done

	if [[ ${part_1_num} == 0 ]]; then
		sc_print_para "${sctxt_error_1}"
		sc_prompt_pause
		# Will go to prompt
		# " Which partitioning option do you select (1-2,..."
		return 5
	fi

	if [[ ${part_2_num} == 0 ]]; then
		sc_print_para "${sctxt_error_2}"
		sc_prompt_pause
		# Will go to prompt
		# " Which partitioning option do you select (1-2,..."
		return 5 
	fi

	sc_print_para "$(gettext 'You selected the following partitioning option:\n\n')"

	ql_display_selected_partition_option "${partition_1}" "${partition_2}" "CUSTOM"

	answer=$(sc_prompt_yesno "$(gettext 'Is this the correct option?')" "${YES}") || return 2
	if [[ "${answer}" != "yes" ]]; then
		# Return to "Which partitioning option you want to select"
		return 5
	fi
	
	answer=$(sc_prompt_yesno "$(gettext 'Do you want to begin the dual-partition upgrade?')" "${YES}")
	if [[ "${answer}" != "yes" ]]; then
		# Return to QL Menu
		return 0
	fi


	# Set remote method (SSH/RSH)
	ql_set_remote_method
	if [[ $? -eq "1" ]]; then
		# Return to QL Menu
		return 0
	fi

	sc_print_para "${sctxt_prepare}"

	partition_comma_list=`echo ${partition_comma_list_1} | sed 's/[,]*$//'`
	ql_call_begin_script "${partition_comma_list}"

	# Return to QL Menu
	return 0
}

####################################################
#
# ql_process_partition_option() <option_num>
#
#	This function will be used to process the
#	begin command in interactive mode for a
#	3 or more node cluster.  It should be called when the
#	node is in cluster mode.
#
#	Return values:
#		0	- return to QL Menu
#		1	- return to main menu
#		5	- go to partition option selection prompt
#		*	- error
#
####################################################
ql_process_partition_option()
{
	typeset option="Option ""${1}"
	typeset ptool_op="/var/cluster/run/ptool_op.txt"
	typeset answer=
	typeset node=
	typeset partition_1=
	typeset partition_2=
	typeset partition_comma_list_1=
	typeset partition_comma_list_2=
	typeset partition_comma_list=
	
	typeset -i part_1_num=0
	typeset -i part_2_num=0
	typeset -i flip_flag=0

	typeset sctxt_part_1="$(gettext 'First partition')"
	typeset sctxt_part_2="$(gettext 'Second partition')"

	typeset sctxt_prompt="$(gettext 'The nodes in the first partition are shut down and upgraded first.  Do you want to use this partition order or reverse it?')"

	typeset sctxt_option_1="$(gettext 'Use this partition order')"

	typeset sctxt_option_2="$(gettext 'Reverse the partition order')"


	typeset sctxt_prepare="$(gettext 'Preparing the cluster for dual-partition upgrade ... ')"

	#
	# ----------- LOGIC ---------------------------
	#
	# Read the output file
	# Till you get "Option ${1}
	# Verify that the next line is "First Partition"
	# Go on reading the next lines and storing them in partition_1
	# Till you get "Second Partition"
	# Go on reading the next lines and storing them in partition_2
	# Till you get a blank line or end of file is reached
	#

	typeset process_flag="BEGIN"

	while read line
	do
		#echo $line
		case ${process_flag} in
			BEGIN)
				#
				# Check to see if you have reached "Option ${1}"
				#
				if [[ "${line}" == "${option}" ]]; then
					process_flag="PART_1"
				fi
				;;
			PART_1)
				#
				# Verify that the line is "First Partition"
				#
				if [[ "${line}" == "${sctxt_part_1}" ]]; then
					process_flag="PART_1_HOST"
				else
					process_flag="ERROR"
				fi
				;;

			PART_1_HOST)

				#
				# Store the nodes of partition 1
				#
				if [[ "${line}" == "${sctxt_part_2}" ]]; then
					process_flag="PART_2"
				else
					partition_1="${partition_1}${line} "
					partition_comma_list_1="${partition_comma_list_1}${line},"
					part_1_num=part_1_num+1
				fi
				;;
			PART_2)
				#
				# Store the nodes of partition 2
				#
				if [[ -z  "${line}" ]]; then
					break
				fi
					partition_2="${partition_2}${line} "
					partition_comma_list_2="${partition_comma_list_2}${line},"
					part_2_num=part_2_num+1
				;;
			
		esac

	done < ${ptool_op}

	sc_print_para "$(gettext 'You selected the following partitioning option:\n\n')"

	#
	# Display the selected option in the same format as the partition tool
	#
	ql_display_selected_partition_option "${partition_1}" "${partition_2}" "${1}"

	answer=$(sc_prompt_yesno "$(gettext 'Is this the correct option?')" "${YES}") || return 2
	if [[ "${answer}" != "yes" ]]; then
		# Return to "Which partitioning option you want to select"
		return 5
	fi

	#
	# Ask for reversing the partitions
	#
	option_menu="$(
		sc_get_menuoption	\
	 	"T2+++${sctxt_prompt}" \
    		"D+0+1+${sctxt_option_1}" \
    		"S+0+2+${sctxt_option_2}" \
	)"

	flip_flag=0

	# User has choosen to interchange the partitions
	if [[ ${option_menu} -eq "2" ]]; then
		flip_flag=1
		sc_print_para "$(gettext 'The partitions have been reversed\n\n')"
		ql_display_selected_partition_option "${partition_2}" "${partition_1}" "${1}"

	fi
	
	answer=$(sc_prompt_yesno "$(gettext 'Do you want to begin the dual-partition upgrade?')" "${YES}") || return 2
	if [[ "${answer}" != "yes" ]]; then
		# Return to QL Menu
		return 1
	fi


	# Set remote method (SSH/RSH)
	ql_set_remote_method
	if [[ $? -eq "1" ]]; then
		# Return to QL Menu
		return 1
	fi

	#
	# Call the begin script
	# 
	case ${flip_flag} in
		'0')
			# Remove the last ","
			partition_comma_list=`echo ${partition_comma_list_1} | sed 's/[,]*$//'`
			;;
		'1')
			# Remove the last ","
			partition_comma_list=`echo ${partition_comma_list_2} | sed 's/[,]*$//'`
			;;
	esac


	sc_print_para "${sctxt_prepare}"

	# Make the call to begin script
	ql_call_begin_script "${partition_comma_list}"


	# Return to QL Menu
	return 0
}

####################################################
#
# ql_call_begin_script() <first_partition>
#
# 	first_partition : Comma separated list of nodes
#			  belonging to the first partition
#
#	This function will call the "begin" script
#
#	Return values:
#		0	- success
#
####################################################
ql_call_begin_script()
{
	typeset retval
	typeset partition_1_nodes
	typeset -i num_nodes=0
	typeset ql_part_op="/tmp/ql_part_op"

	typeset sctxt_prompt_1="$(gettext 'The following node is in the first partition:')"
	typeset sctxt_prompt_2="$(gettext 'The following nodes are in the first partition:')"

	typeset sctxt_message_single_1="$(gettext '
		This node will now be halted. Once it is halted, you
		must boot it into noncluster mode from the console.
	')"
	typeset sctxt_message_single_2="$(gettext '
		After the node has come up into noncluster mode, you
		can upgrade Sun Cluster, Solaris, or any other software
		that is installed on the node.  When all software
		upgrade steps have been completed, you must restart
		scinstall on the upgraded node.   Select the option
		from the dual-partition upgrade menu to \"Continue the
		dual-partition upgrade on the first partition\".
	')"
	typeset sctxt_message_double_1="$(gettext '
		These nodes will be halted.  Once these nodes are
		halted, you must boot each of them into non-cluster
		mode from their consoles.
	')"
	typeset sctxt_message_double_2="$(gettext '
		After the nodes have come up into noncluster mode, you
		can upgrade Sun Cluster, Solaris, or any other software
		on these nodes.  When all software upgrade steps have
		been completed, you must restart scinstall on one of
		the nodes in this partition. Select the option from the
		dual-partition upgrade menu to \"Continue the
		dual-partition upgrade on the first partition\".
	')"

	typeset sctxt_custom_prompt_1="$(gettext 'Press Enter to halt')"
	typeset sctxt_custom_prompt_2="$(gettext 'Press Enter to halt all the nodes in the first partition:')"

	rm -f $ql_part_op

	# implemet the actual call to split_mode_upgrade_begin 
	# scinstall -u begin
	if [[ "${scinstalldir}" != "${SC_BINDIR}" ]]; then
		# Running from Tools directory
		${SC_SCLIBDIR}/${SC_QL_UPGRADE_BEGIN} -n ${1} -i
	else
		# Running from Host
		${SC_QL_BINDIR}/${SC_QL_UPGRADE_BEGIN} -n ${1} -i
	fi

	retval=$?
	case $retval in
		'1')
			# Error has occured
			# Error message will be displayed 
			# from the split_mode_upgrade_begin script
			;;
		'0')
			# The split_mode_upgrade_begin script has
			# has executed successfully
			# Now halt the nodes one by one

			# Convert comma separated node list to 
			# space separated node list
			partition_1_nodes="$(IFS=, ; echo ${1})"

			# Count the number of nodes
			for node in $partition_1_nodes
			do
				num_nodes=num_nodes+1
				echo "      $node" >> $ql_part_op
			done

			printf "\n"

			if [[ $num_nodes == 1 ]]; then
				sc_print_para "${sctxt_prompt_1}"
				printf "      $node\n\n"
				sc_print_para "${sctxt_message_single_1}"
				sc_print_para "${sctxt_message_single_2}"
				sctxt_custom_prompt_1="${sctxt_custom_prompt_1}"" \"""${node}""\":"
				ql_custom_prompt "${sctxt_custom_prompt_1}"
			else
				sc_print_para "${sctxt_prompt_2}"
				cat $ql_part_op | more
				printf "\n"
				sc_print_para "${sctxt_message_double_1}"
				sc_print_para "${sctxt_message_double_2}"
				ql_custom_prompt "${sctxt_custom_prompt_2}"

			fi

			# Cleanup the temporary file
			rm -f $ql_part_op

			# Halt partition one nodes
			ql_halt_partition "1"

			if [[ $? != 0 ]]; then
				printf "$(gettext 'Failed to halt some of the nodes.')\n"  >&2
				printf "$(gettext 'Manually halt the nodes of the first partition.')\n\n\a"  >&2
			else
				sc_print_line "$(gettext 'Successfully halted all the nodes of the first partition.')"
				printf "\n"
			fi
	esac
	
	sc_prompt_pause
	
	return 0
}

####################################################
#
# ql_halt_partition() $partition_num
#
#	partition_number: The partion to halt
#
#	This function takes input as the partition number
#	and halts each node in the partition.
#
# 	It may so happen that this function tries
#       to halt the node from which it is run.
#       In that case the node will go down.
#
#	Return values:
#		0	- if all nodes are halted successfully
#		1	- if some error occurs while halting the node
#
####################################################
ql_halt_partition()
{
	typeset partition_1
	typeset partition_2
	typeset token_1
	typeset token_2

	while read line
	do
		token_1=`echo $line | awk '{print $1}'`
		token_2=`echo $line | awk '{print $2}'`
		case $token_1 in
			partition_a_node)
				partition_1="${partition_1}${token_2} "
				;;
			partition_b_node)
				partition_2="${partition_2}${token_2} "
				;;
		esac
	
	done < ${SC_QL_STATE_FILE}

	if [[ $1 == 1 ]]; then
		ql_halt_nodes "${partition_1}"
	else
		ql_halt_nodes "${partition_2}"
	fi

	return $?
}

####################################################
#
# ql_halt_nodes() $node_list
#
#	node_list: list of nodes to be halted separated by a space
#
#	This function will halt the nodes in nodelist.
#
#	Return values:
#		0	- if all nodes are halted successfully
#		1	- if some error occurs while halting the node
#
####################################################
ql_halt_nodes()
{
	typeset node
	typeset retval
	typeset local_flag=0
	typeset error_flag=0
	typeset node_list="${1}"
	typeset shutdown_cmd="/usr/sbin/shutdown -y -g 0 -i 0"
	typeset local_node=`/usr/bin/hostname`

	if [[ -z "${node_list}" ]]; then
		return 0
	fi

	for node in ${node_list}
	do
		if [[ "${node}" == "${local_node}" ]]; then
			local_flag=1
			continue
		fi

		ql_execute_remote_command $node $shutdown_cmd
		retval=$?

		if [[ $retval -eq "1" ]]; then
			printf "$(gettext 'Failed to shutdown %s')\n\n\a" ${node} >&2
			error_flag=1
		fi	

	done

	if [[ $local_flag == 1 ]]; then
		sc_print_line "$(gettext 'Shutting down this node') ${local_node}" >&2
		printf "\n" >&2
		sc_prompt_pause
		ql_execute_remote_command $local_node $shutdown_cmd

	fi
	
	return $error_flag
}

####################################################
#
# ql_reboot_partition()
#
#	This function will detect on which partition it
#	is executed on and then reboot the nodes of
#	that partition. The node from which this
#	function is executed will be rebooted last
#
#	Return values:
#		0	- if all nodes are rebooted successfully
#		1	- if some error occurs while rebooting the node
#
####################################################
ql_reboot_partition()
{
	typeset partition_1
	typeset partition_2
	typeset token_1
	typeset token_2
	typeset local_node=`/usr/bin/hostname`
	typeset -i partition_flag

	typeset sctxt_heading_1="$(gettext 'The following nodes are in the first partition:')"
	typeset sctxt_heading_2="$(gettext 'The following nodes are in the second partition:')"

	typeset sctxt_part_1_message_1="$(gettext '
		These nodes will be rebooted into cluster mode. Once
		they are successfully booted as active cluster members,
		the nodes in the second partition will be halted.
	')"
	typeset sctxt_part_1_message_2="$(gettext '
		These nodes will be halted. Once they are halted, you
		must boot them into noncluster mode from their
		console.
	')"
	typeset sctxt_part_1_message_3="$(gettext '
		After they have come up into non-cluster mode, you can
		upgrade Sun Cluster, Solaris, or any other software
		that is installed on the nodes.  When all software
		upgrade steps have been completed, you must restart
		scinstall on one of the nodes. Select the  option
		\"Apply dual-partition upgrade changes\" for the
		second partition.
	')"

	typeset sctxt_part_2_message_1="$(gettext '
		This node will now be rebooted into cluster mode.
	')"	

	typeset sctxt_prompt_1="$(gettext 'Press Enter to reboot and halt the cluster nodes:')"
	typeset sctxt_prompt_2="$(gettext 'Press Enter to reboot the cluster nodes:')"

	while read line
	do
		token_1=`echo $line | awk '{print $1}'`
		token_2=`echo $line | awk '{print $2}'`
		case $token_1 in
			partition_a_node)
				partition_1="${partition_1}${token_2} "
				if [[ "${local_node}" == "${token_2}" ]]; then
					partition_flag=1
				fi
				;;
			partition_b_node)
				partition_2="${partition_2}${token_2} "
				if [[ "${local_node}" == "${token_2}" ]]; then
					partition_flag=2
				fi
				;;
		esac
	
	done < ${SC_QL_STATE_FILE}

	if [[ $partition_flag == 1 ]]; then
		# Display the nodes 
		sc_print_para "${sctxt_heading_1}"
		for node in ${partition_1}
		do
			printf "      $node\n"
		done
		printf "\n"
		sc_print_para "${sctxt_part_1_message_1}"
		sc_print_para "${sctxt_heading_2}"
		for node in ${partition_2}
		do
			printf "      $node\n"
		done
		printf "\n"	
		sc_print_para "${sctxt_part_1_message_2}"
		sc_print_para "${sctxt_part_1_message_3}"

		ql_custom_prompt "${sctxt_prompt_1}"

		# Reboot the nodes of this partition
		ql_reboot_nodes "${partition_1}"
	else
		sc_print_para "${sctxt_heading_2}"
		for node in ${partition_2}
		do
			printf "        $node\n"
		done
		printf "\n"
		sc_print_para "${sctxt_part_2_message_1}"

		ql_custom_prompt "${sctxt_prompt_2}"

		# Reboot the nodes of this partition
		ql_reboot_nodes "${partition_2}"
	fi

	return $?
}

####################################################
#
# ql_reboot_nodes() $node_list
#
#	node_list: list of nodes to be rebooted separated by a space.
#
#	This function will reboot the nodes in nodelist.
#
#	Return values:
#		0	- successfully rebooted the nodes
#		1	- error occured while rebooting the nodes
#
####################################################
ql_reboot_nodes()
{
	typeset node
	typeset retval
	typeset local_flag=0
	typeset error_flag=0
	typeset node_list="${1}"
	typeset reboot_cmd="/usr/sbin/reboot"
	typeset local_node=`/usr/bin/hostname`

	if [[ -z "${node_list}" ]]; then
		return 0
	fi

	if [[ ! -x $reboot_cmd ]]; then
                printf "$(gettext 'reboot not found')\n\n\a" >&2
                return 1
        fi

	for node in ${node_list}
	do
		if [[ "${node}" == "${local_node}" ]]; then
			local_flag=1
			continue
		fi
		
		ql_execute_remote_command $node $reboot_cmd
	done

	if [[ $local_flag == 1 ]]; then
		sc_print_line "$(gettext 'rebooting this node') ${local_node}"
		printf "\n"
		sc_prompt_pause
		ql_execute_remote_command $local_node $reboot_cmd
	fi
	
	return 0
}

####################################################
#
# ql_execute_remote_command $node $command $cmd_args
#
#	node:		the remote host on which to execute the command.
#	command:	the command to execute
#	cmd_args:	the command arguments
#
#	This will execute the command on a remote node
#	using SSH or RSH.Before calling this function,
#	make sure that the SC_REMOTE_METHOD is set to
#	either RSH or SSH. This function should be used
#	only during QL upgrade.
#
#	Return values:
#		0	- success
#		1	- error
#
####################################################
ql_execute_remote_command()
{
	typeset nodename
        typeset command
        typeset args
        typeset method
        typeset tmpfile
        typeset tmperr

	integer pid=$$

        nodename=$1
        command=$2
        shift 2
        args="$*"

        tmpfile="ql_rem_cmd.$pid.$pid"
        tmperr="ql_rem_cmd.$pid$pid"

	rm -f /tmp/$tmpfile
        rm -f /tmp/$tmperr
	
        if [[ -z $nodename ]]; then
                printf "$(gettext 'Remote node is Null')\n\n\a" >&2
                return 1
        fi

	if [[ -z $command ]]; then
                printf "$(gettext 'Remote command is Null')\n\n\a" >&2
                return 1
        fi

	if [[ -z $SC_REMOTE_METHOD ]]; then
		printf "$(gettext 'Remote method is not set')\n\n\a" >&2
		return 1
	fi

	if [[ "${SC_REMOTE_METHOD}" == "scrcmd" ]]; then
		printf "$(gettext 'Dual-partition upgrade does not support scrcmd')\n\n\a" >&2
		return 1
	fi

	method=$SC_REMOTE_METHOD
	case ${method} in
		ssh)
			cmd="ssh root@${nodename} -o \"BatchMode yes\" -n \"/bin/sh -c '${command} ${args}; /bin/echo SC_COMMAND_STATUS=\\\$?'\""
			;;
		rsh)
			cmd="rsh ${nodename} -n \"/bin/sh -c '${command} ${args}; /bin/echo SC_COMMAND_STATUS=\\\$?'\""
			;;
		local)
			cmd="/usr/bin/sh -c '${command} ${args}; /bin/echo SC_COMMAND_STATUS=\\\$?'"
			;;
	esac

	eval "${cmd}" >/tmp/${tmpfile} 2>/tmp/${tmperr}

	result=$(sed -n 's/^SC_COMMAND_STATUS=//p' /tmp/${tmpfile})	

	if [[ -z $result ]]; then
                result=1
        fi
        if [[ "$result" != "0" ]]; then
                return 1
        fi
        rm -f /tmp/$tmpfile
        rm -f /tmp/$tmperr

			
	return 0
}

####################################################
#
# ql_display_selected_partition_option $first_part $second_part $option_num
#
#	first_part : List of nodes of the first partition.
#		     Node names are separated by a space
#
#	second_part: List of nodes of the second partition.
#		     Node names are separated by a space
#
#	option_num: The option selected
#		    This could be a number or "CUSTOM"
#
#	This function will print the option in the 
#	partition tool format.
#
#	This function always returns zero.
#
####################################################
ql_display_selected_partition_option()
{
	typeset node=
	typeset partition_1="${1}"
	typeset partition_2="${2}"
	typeset option="Option ""${3}"

	typeset sctxt_part_1="$(gettext 'First partition')"
	typeset sctxt_part_2="$(gettext 'Second partition')"

	typeset sctxt_custom="$(gettext 'Custom Option')"

	if [[ "${3}" == "CUSTOM" ]]; then
		option="${sctxt_custom}"
	fi


	printf "  ${option}\n"
	printf "    ${sctxt_part_1}\n"
	for node in ${partition_1}
	do
		printf "      ${node}\n"
	done
	printf "    ${sctxt_part_2}\n"
	for node in ${partition_2}
	do
		printf "      ${node}\n"
	done
	printf "\n"

	return 0
}

####################################################
#
# ql_prompt_select_partition_option "prompt" "max_value" "default"
#
#	prompt    : The prompt that will be displayed
#	max_value : The max range 
#		    e.g  1 - 5 then max_value = 5
#	default   : The default option
#
#	It echos
#		1: LIST 
#		2: QUIT
#		3: 1/2/../max_value
#	
#
#	e.g
#	Which number do you select (1-5, list, quit) [list]?
#	
#	then 
#	prompt="Which number do you select"
#	max_value=5
#	default="list"
#
#	This function always returns zero.
#
####################################################
ql_prompt_select_partition_option()
{
	typeset prompt="${1}"
	typeset max_value="${2}"
	typeset default="${3}"

	typeset answer=

	integer i

	# if there is a terminating ? and ${YES} is yes and ${NO} is no,
	# add (yes/no) and shift the terminating "?"
	#if [[ "${YES}" = "yes" ]] &&
	#    [[ "${NO}" = "no" ]] &&
	#    [[ "${prompt}" = *\? ]]; then
	#	prompt="${prompt%\?}"
	#	prompt="${prompt} (yes/no)?"
	#fi

	prompt="${prompt} (1-""${max_value}"", custom, list, quit)?"
	
	let i=0
	while true
	do
		# If this is not the first time through, beep
		[[ ${i} -gt 0 ]] && echo "\a\c" >&4
		let i=1

		# Prompt and get response
		answer=$(sc_prompt "${prompt}" "${default}" "nonl")

		# Return 1 on EOF
		if [[ $? -eq 1 ]]; then
			echo >&4
			return 1
		fi

		# I18N sensative answer always returns "yes" or "no" string
		case ${answer} in
		list | LIST | l | L )
			answer="LIST"
			break
			;;
		custom | CUSTOM | Custom | Cus | cus | CUS | C | c )
			answer="CUSTOM"
			break
			;;
		[1-$max_value] )
			break
			;;

		quit | QUIT | q | Q)
			answer="QUIT"
			break
			;;

		*)
			answer=
			;;
		esac
	done
	echo >&4

	echo "${answer}"

	return 0
}

####################################################
#
# ql_2node_upgrade_begin()
#
#	This function will be used to do a 2 node upgrade.
#	It should be called when the node is in cluster mode.
#
#	Return values:
#		0	- return to QL Menu
#		1	- return to main menu
#		2	- error
#
####################################################
ql_2node_upgrade_begin()
{
	typeset node
	typeset node_name_list
	typeset option
	typeset retval

	typeset sctxt_title="$(gettext '
		>>> Dual Partition Upgrade of a Two-Node Cluster <<<
	')"

	typeset sctxt_description_p1="$(gettext '
		As part of the dual-partition upgrade process for
		a two-node cluster, each of the two nodes is assigned
		to a partition, or subcluster.  The node in the first
		partition is booted into non-cluster mode, then
		upgraded, while the node in the second partition
		remains active.  Once upgraded, the node in the first
		partition will take over, so that the node in the
		second partition can then be upgraded.
	')"
	typeset sctxt_description_p2="$(gettext '
		The node that is assigned to the first partition will
		be booted first.
	')"
	typeset sctxt_partition="$(gettext '
		Select the partition that you want to assign to 
	')"

	typeset sctxt_partition_1="$(gettext '
		has been assigned to the first partition.
	')"
	typeset sctxt_partition_2="$(gettext '
		has been assigned to the second partition.
	')"
	typeset sctxt_prepare="$(gettext '
		Preparing the cluster for dual-partition upgrade ...
	')"

	typeset sctxt_option_001="$(gettext 'First Partition')"
	typeset sctxt_option_002="$(gettext 'Second Partition')"

	node_name_list="$(ql_get_nodelist)"

	typeset node_1
	typeset node_2

	node_1=`echo $node_name_list | awk '{print $1}'`
	node_2=`echo $node_name_list | awk '{print $2}'`

	clear
	{
		sc_print_title "${sctxt_title}"
		sc_print_para  "${sctxt_description_p1}"
		sc_print_para  "${sctxt_description_p2}"
	} | more

	prompt="${sctxt_partition}""\"""${node_1}""\":"		
	option="$(
		sc_get_menuoption	\
	 	"T2+++${prompt}" \
    		"D+0+1+${sctxt_option_001}" \
    		"S+0+2+${sctxt_option_002}" \
	)"

	if [[ ${option} -eq "1" ]]; then
		prompt="\"""${node_1}""\" ""${sctxt_partition_1}"
		sc_print_line "${prompt}"
		printf "\n"
		prompt="\"""${node_2}""\" ""${sctxt_partition_2}"
		sc_print_line "${prompt}"
		printf "\n\n\a"
	fi

	if [[ ${option} -eq "2" ]]; then
		prompt="\"""${node_2}""\" ""${sctxt_partition_1}"
		sc_print_line "${prompt}"
		printf "\n"
		prompt="\"""${node_1}""\" ""${sctxt_partition_2}"
		sc_print_line "${prompt}"
		printf "\n\n\a"
	fi
	
	answer=$(sc_prompt_yesno "$(gettext 'Do you want to continue?')" "${YES}") || return 2
	if [[ "${answer}" != "yes" ]]; then
		# Return to QL Menu
		return 0
	fi

	# Set remote method (SSH/RSH)
	ql_set_remote_method
	if [[ $? -eq "1" ]]; then
		# Return to QL Menu
		return 0
	fi

	sc_print_para "${sctxt_prepare}"

	# Execute the split mode upgrade begin script with
	if [[ ${option} -eq "1" ]]; then
		ql_call_begin_script "${node_1}"
	else
		ql_call_begin_script "${node_2}"
	fi

	retval=$?

	# Return to QL Menu
	return 0
}

####################################################
#
# ql_get_nodelist()
#
#	This will return the node names.
#
#	This function always returns zero.
#
####################################################
ql_get_nodelist()
{
	(
		LC_ALL=C; export LC_ALL
		/usr/cluster/bin/scconf -p | sed -n 's/^ *Cluster nodes:[ 	]*\(.*\)/\1/p'
	)

	return 0
}

####################################################
#
# ql_get_numnodes()
#
#	This will return the number of nodes in the
#	cluster. This function should be called
#	only when the node is in cluster mode.
#
#	This function always returns zero.
#
####################################################
ql_get_numnodes()
{
	(
		LC_ALL=C; export LC_ALL
		/usr/cluster/bin/scstat -n | grep "Cluster node:" |  wc -l
	)

	return 0
}

####################################################
#
# ql_get_num_online_nodes()
#
#	This will return the number of online nodes in the
#	cluster. This function should be called only when
#	the node is in cluster mode.
#
#	This function always returns zero.
#
####################################################
ql_get_num_online_nodes()
{
	(
		LC_ALL=C; export LC_ALL
		/usr/cluster/bin/scstat -n | grep "Cluster node:" | awk '{print $4}' | grep "Online" | wc -l
	)

	return 0
}

####################################################
#
# ql_check_cluster_membership()
#
#	This will check whether all the nodes are
#	in cluster mode or not
#
#	Return values:
#		${SC_TRUE}	- all nodes are in cluster mode
#		${SC_FALSE}	- at least one node is not in cluster mode
#
####################################################
ql_check_cluster_membership()
{
	typeset -i ql_num_nodes
	typeset -i ql_num_online_nodes

	#
	# Check if the current node is running in 
	# cluster mode or not.
	#
	is_cluster_member
	if [[ $? -eq ${SC_FALSE} ]]; then
		return ${SC_FALSE}
	fi

	#
	# Check if all the other nodes are in
	# cluster mode or not
	#
	ql_num_nodes="$(ql_get_numnodes)"
	ql_num_online_nodes="$(ql_get_num_online_nodes)"

	if [[ $ql_num_nodes == $ql_num_online_nodes ]]; then
		return ${SC_TRUE}
	else
		return ${SC_FALSE}
	fi
}

####################################################
#
# ql_get_num_options()
#
#	This will calculate the number of partition
#	options discovered by the partition tool
#	Should be used only after running the partition
#	tool. 
#
#	This function should be called
#	only when the node is in cluster mode.
#
#	This function always returns zero.
#
####################################################
ql_get_num_options()
{
	typeset node_name_list
	typeset node

	#
	# Logic:
	# Any node in the cluster has to be present
	# ONLY once each option. Hence we calculate
	# the occurence of the first node of the cluster
	#
	node_name_list="$(ql_get_nodelist)"
	node=`echo $node_name_list | awk '{print $1}'`
	cat "${ptool_op}" | grep "${node}" | wc -l

	return 0
}

####################################################
#
# ql_custom_prompt() $prompt
#
#	Print the $prompt ,
#	then wait for a response from the keyboard.
#
#	Return values:
#		0	- proceed
#		1	- ^D was typed
#
####################################################
ql_custom_prompt()
{
	# Pause until they hit Enter
	sc_print_prompt "\n${1}"
	read
	if [[ $? -ne 0 ]]; then
       		echo
		return 1
	fi
	echo

	return 0
}

####################################################
#
# ql_get_internal_status()
#
#	This function will return the internal states
#	of a QL upgrade.
#
#	Return values correspond to the values
#	returned by the underlying QL script.
#
#	Return values:
#		1	- PROG_ERR
#		2	- NO_UPGRADE_STATE
#		3	- UPGRADE_COMPLETE_STATE
#		4	- UPGRADE_PENDING_COMPLETE_STATE
#		5	- UPGRADE_BEGIN_STATE
#		6	- UPGRADE_STATE_PRE_PARTB_SHUTDOWN
#		7	- UPGRADE_PARTA_STATE
#		8	- UPGRADE_ERR_STATE
#
####################################################
ql_get_internal_status()
{

	if [[ "${scinstalldir}" != "${SC_BINDIR}" ]]; then
		# Running from Tools directory
		${SC_SCLIBDIR}/${SC_QL_INTERNAL_STATUS} > /dev/null 2>&1
	else
		# Running from Host
		${SC_QL_BINDIR}/${SC_QL_INTERNAL_STATUS} > /dev/null 2>&1
	fi

	return $?
}

####################################################
#
# interactive_ql_begin()
#
#	This is the quantum leap begin.
#
#	Return values:
#		0	- return to QL Menu
#		1	- return to main menu
#
####################################################
interactive_ql_begin()
{
	typeset answer
	typeset retval
	typeset -i ql_num_nodes
	typeset old_sc_ilog

	typeset sctxt_title="$(gettext '
		>>> Begin the Dual-Partition Upgrade Process <<<
	')"
	typeset sctxt_description_p1="$(gettext '
		Use this option to prepare the cluster for a
		dual-partition upgrade.
	')"
	typeset sctxt_description_p2="$(gettext '
		Before selecting this option, you must configure either
		"rsh" or "ssh" root access among all nodes in the
		cluster.
	')"
	typeset sctxt_description_p3="$(gettext '
		As part of the dual-partition upgrade process,
		you must divide the cluster into two partitions, or
		subclusters. Each partition must be able to provide
		all client services while it is active.
	')"
	typeset sctxt_description_p4="$(gettext '
		This "begin" step will alter the cluster quorum
		configuration, then halt all of the nodes in the first
		of these two partitions.
	')"
	typeset sctxt_membership_message="$(gettext '
		This option requires all the nodes to be in cluster
		mode.  Use the \"clnode status\" command to see which
		nodes are offline.  Bring those nodes online and try
		again.
	')"

	clear
	{
		sc_print_title "${sctxt_title}"
		sc_print_para  "${sctxt_description_p1}"
		sc_print_para  "${sctxt_description_p2}"
		sc_print_para  "${sctxt_description_p3}"
		sc_print_para  "${sctxt_description_p4}"
	} | more

	answer=$(sc_prompt_yesno "$(gettext 'Do you want to continue?')" "${YES}") || return 2
	if [[ "${answer}" != "yes" ]]; then
		# Return to QL Menu
		return 0
	fi

	# Check if the all the node are in cluster mode
	ql_check_cluster_membership
	if [[ $? -eq ${SC_FALSE} ]]; then
		sc_print_para "${sctxt_membership_message}"
		sc_prompt_pause
		# Return to QL Menu
		return 0		
	fi

	# Check for the number of nodes in the cluster 
	ql_num_nodes="$(ql_get_numnodes)"

	case ${ql_num_nodes} in
	
	'1')	
		# Single node cluster
		sc_print_line "$(gettext 'This is a single-node cluster.')"
		printf "\n"
		sc_print_line "$(gettext 'Single-node clusters are not candidates for dual-partition upgrade.')"
		printf "\n\n\a"
		sc_prompt_pause
		# Return to Main Menu
		return 1
		;;

	'2')
		# 2 node cluster partitioning
		ql_2node_upgrade_begin
		retval=$?
		case $retval in
			'0')
				# Return to QL Menu
				return 0
				;;
			'1')
				# Return to Main Menu
				return 1
				;;
			*)
				# Error
				;;
		esac
		;;

	*)	

		# Custom partitioning
		ql_process_custom_partition
		retval=$?
		case $retval in
			'0')
				# Return to QL Menu
				return 0
				;;
			'1')
				# Return to Main Menu
				return 1
				;;
			'5')
				# Return to QL Menu
				return 0
				;;
			*)
				# Error
				;;
		esac
		;;
	esac

	# Return to QL Menu
	return 0
}

####################################################
#
# interactive_ql_apply()
#
#	This is the quantum leap apply.
#
#	Return values:
#		0	- return to QL menu
#		1	- return to main menu
#
####################################################
interactive_ql_apply()
{
	typeset retval
	typeset answer

	typeset err_file=/tmp/ql_apply_err.$$
	typeset  op_file=/tmp/ql_apply_op.$$

	rm -f $err_file
	rm -f $op_file

	typeset sctxt_title="$(gettext '
	    >>> Apply Dual-Partition Upgrade Changes To This Partition <<<
	')"

	typeset sctxt_description_p1="$(gettext '
		Use this option to continue a dual-partition upgrade
		after all software on the nodes in this partition have
		been upgraded.
	')"
	typeset sctxt_description_p2="$(gettext '
		This step will alter the cluster quorum configuration
		and reboot all nodes of this partition into cluster
		mode.  If this is the first partition, then after the
		first partition nodes come up in cluster mode, the
		second partition nodes are halted.
	')"
	
	typeset sctxt_apply_message="$(gettext 'Updating the cluster quorum configuration ... ')"

	clear
	{
		sc_print_title "${sctxt_title}"
		sc_print_para  "${sctxt_description_p1}"
		sc_print_para  "${sctxt_description_p2}"
	} | more

	answer=$(sc_prompt_yesno "$(gettext 'Do you want to continue?')" "${YES}") || return 2
	if [[ "${answer}" != "yes" ]]; then
		# Return to QL Menu
		return 0
	fi

	#
	# Check if we are running in cluster mode.
	# Apply command should be run in non cluster mode.
	#
	is_cluster_member
	if [[ $? -eq ${SC_TRUE} ]]; then
		#
		# Apply command should not be executed
		# in cluster mode.
		#
		printf "$(gettext 'Cannot perform apply operation on active cluster node.')\n\n\a"
		sc_prompt_pause || return 1

		# Return to QL Menu
		return 0
	fi

	# Set the remote method (RSH/SSH)
	# This is set becuase we will be rebooting nodes
	ql_set_remote_method
	if [[ $? -eq "1" ]]; then
		# Return to QL Menu
		return 0
	fi

	sc_print_line "${sctxt_apply_message}"
	
	if [[ "${scinstalldir}" != "${SC_BINDIR}" ]]; then
		# Running from Tools directory is not allowed
		printf "$(gettext 'Command should not be executed from the media image.')\n\n\a"
		sc_prompt_pause || return 1

		# Return to QL Menu
		return 0

	else
		# Running from Host
		${SC_QL_BINDIR}/${SC_QL_UPGRADE_PARTITION} -i 1>$op_file 2>$err_file
		retval=$?
	fi

	if [[ $retval -eq "0" ]]; then
		printf "$(gettext 'done')\n\n"
		cat $op_file | more
		# Reboot the current partition
		ql_reboot_partition
	else
		printf "$(gettext 'failed')\n\n\a"
		cat $err_file | more
	fi

	rm -f $err_file
	rm -f $op_file

	sc_prompt_pause

	return 0
}

####################################################
#
# interactive_ql_status()
#
#	This is the quantum leap status.
#	This will say if QL upgrade is going on
#	or not.
#
#	This function always returns zero.
#
####################################################
interactive_ql_status()
{
	typeset retval

	typeset err_file=/tmp/ql_status_err.$$
	typeset  op_file=/tmp/ql_status_op.$$

	typeset sctxt_prompt="$(gettext 'Retrieving the Dual-Partition Upgrade Status ... ')"

	sc_print_line "${sctxt_prompt}"

	rm -f $err_file
	rm -f $op_file


	if [[ "${scinstalldir}" != "${SC_BINDIR}" ]]; then
		# Running from Tools directory
		${SC_SCLIBDIR}/${SC_QL_UPGRADE_STATUS} 1>$op_file 2>$err_file
	else
		# Running from Host
		${SC_QL_BINDIR}/${SC_QL_UPGRADE_STATUS} 1>$op_file 2>$err_file

	fi
	
	retval=$?
	if [[ $retval -eq "0" ]]; then
		printf "$(gettext 'done')\n\n"
		cat $op_file | more
	else
		printf "$(gettext 'failed')\n\n\a"
		cat $err_file | more
	fi

	rm -f $err_file
	rm -f $op_file

	sc_prompt_pause

	# Return to QL Menu
	return 0
}

####################################################
#
# ql_set_remote_method
#
#	Sets the remote method during QL
#	QL supports only SSH and RSH.
#
#	Return values:
#		0	- success
#		1	- failure
#
####################################################
ql_set_remote_method()
{
	typeset retval=0
	typeset answer

	typeset partition_1
	typeset partition_2
	typeset token_1
	typeset token_2
	typeset local_node=`/usr/bin/hostname`
	typeset -i partition_flag
	typeset -i part_1_num=0
	typeset -i part_2_num=0
	typeset -i num_nodes=0

	typeset sctxt_para_1="$(gettext '
		Before selecting this option, you must configure either
		"rsh" or "ssh" root access between all nodes in the
		cluster.  Configure remote access and try again.
	')"

	#
	# QL supports only SSH and RSH
	# Hence we need to set the global QL flag before
	# trying to set the remote method (SSH/RSH)
	# If we are in non cluster mode and the final nodelist
	# has just 1 node, then we will set the remote method as
	# LOCAL. This is because, we are doing the "apply"
	#

	#		
	# Set the global QL flag
	# This is done so that remote method doesn't
	# get set to scrcmd
	#
	SC_QL_FLAG=1

	# During QL we dont use the SC_ILOG
	old_sc_ilog=$SC_ILOG
	SC_ILOG=/dev/null

	#
	# Need to set the host names before calling
	# interactive_config_remote_method()
	#
	is_cluster_member
	if [[ $? -eq ${SC_TRUE} ]]; then
		set -A SC_ARGVAR_NODENAME "$(ql_get_nodelist)"
	else
		#
		# Not a cluster member
		# Read the QL state file and find out the list of nodes
		#
		if [[ ! -f ${SC_QL_STATE_FILE} ]]; then
			printf "$(gettext 'Dual-Partition Upgrade state file not found')\n\n\a" >&2
			return 1
		fi

		while read line
		do
			token_1=`echo $line | awk '{print $1}'`
			token_2=`echo $line | awk '{print $2}'`
			case $token_1 in
				partition_a_node)
					partition_1="${partition_1}${token_2} "
					part_1_num=part_1_num+1
					if [[ "${local_node}" == "${token_2}" ]]; then
						partition_flag=1
					fi
					;;
				partition_b_node)
					partition_2="${partition_2}${token_2} "
					part_2_num=part_2_num+1
					if [[ "${local_node}" == "${token_2}" ]]; then
						partition_flag=2
					fi
					;;
			esac
	
		done < ${SC_QL_STATE_FILE}

		if [[ $partition_flag == 1 ]]; then
			set -A SC_ARGVAR_NODENAME "${partition_1}"
			num_nodes=part_1_num
		else
			set -A SC_ARGVAR_NODENAME "${partition_2}"
			num_nodes=part_2_num
		fi
	fi

	#
	# If there is only one node then it has to be the
	# local node. This is becuase, in cluster mode,
	# the num nodes will be more than one.
	# Set the remote method to "local"
	# else, find out the remote method (rsh/ssh)
	#
	if [[ $num_nodes == 1 ]]; then
		SC_REMOTE_METHOD=local
		SC_ARGVAR_REMOTE_METHOD=${SC_REMOTE_METHOD}
		# echo "SC_REMOTE_METHOD set to local" >> ${install_log}
	else
		while true
		do
			# serach and set the remote method
			interactive_config_remote_method

			# If we didn't find a suitable remote execution method,
			# return to main menu
			if [[ $? -eq "1" ]]; then
				# Return to Main Menu
				retval=1
				sc_print_para "${sctxt_para_1}"
				answer=$(sc_prompt_yesno "$(gettext 'Do you want to try again now?')" "${YES}")
				if [[ "${answer}" != "yes" ]]; then
					break
				fi
			else
				retval=0
				break
			fi
		done
	fi

	# Unset the variable
	unset SC_ARGVAR_NODENAME

	# Restore the SC_ILOG
	SC_ILOG=$old_sc_ilog

	# Unset the global QL flag
	SC_QL_FLAG=0

	return $retval
}
