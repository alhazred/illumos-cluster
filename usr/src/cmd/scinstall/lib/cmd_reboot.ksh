#! /usr/xpg4/bin/sh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# pragma ident	"@(#)cmd_reboot.ksh	1.9	08/09/12 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# cmd_reboot
#
#	Reboot the node with a re-configuration reboot.   The
#	/etc/cluster/nodeid must exist, but the
#	/etc/cluster/ccr/global/did_instances table must not.
#
#	Possible exit codes:
#
#		2 error
#		(this command waits for reboot, if successful)
#
#####################################################

#####################################################
#
# Constant Globals
#
#####################################################

# Files
typeset -r SCRCMD_NODEID_FILE=/etc/cluster/nodeid
typeset -r SC_DID_INSTANCES=/etc/cluster/ccr/global/did_instances

# Program name
typeset -r PROG=${0##*/}

# Set the PATH
typeset -r SC_BINDIR=/usr/cluster/bin
typeset -r SC_BINDIRS=${SC_BINDIR}:/usr/cluster/lib/sc
PATH=${SC_BINDIRS}:/bin:/usr/bin:/sbin:/usr/sbin; export PATH

# I18N
typeset -x TEXTDOMAIN=TEXT_DOMAIN
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

#####################################################
#
# print_usage()
#
#       Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "$(gettext 'usage'):  ${PROG}" >&2
}  

#####################################################
#
# Main
#
#####################################################
main()
{
	# Check arguments
	if [[ $# -ne 0 ]]; then
		print_usage
		return 2
	fi

	# Make sure that this node has been configured
	if [[ ! -f "${SCRCMD_NODEID_FILE}" ]]; then
		printf "$(gettext 'This node has not yet been configured.')\n"
		return 2
	fi

	# Make sure that we have not already tried to boot into the cluster
	if [[ -f "${SC_DID_INSTANCES}" ]]; then
		printf "$(gettext 'This node has already attempted to boot into the cluster.')\n"
		return 2
	fi

	# Make sure that it is a reconfiguration reboot
	touch /reconfigure

	#
	# Reboot after 20 seconds.   The 20 second delay gives us the
	# opportunity to cleanly return before rebooting.   Otherwise,
	# rsh can give us a problem.
	#
	(sleep 20; /usr/sbin/reboot) 0</dev/null 1>/dev/null 2>/dev/null &

	return 0
}

	main $*
	exit $?
