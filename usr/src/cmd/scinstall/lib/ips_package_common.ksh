#! /usr/xpg4/bin/sh -p

#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# pragma ident	"@(#)ips_package_common.ksh	1.4	09/02/26 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

PATH=/usr/bin:/usr/sbin
export PATH
typeset -r SVCADM=/usr/sbin/svcadm
typeset -r SVCCFG=/usr/sbin/svccfg
typeset -r MFSTSCAN=/lib/svc/bin/mfstscan
typeset -r SVCPROP=/usr/bin/svcprop


RBAC_DIR=/var/cluster/ips/rbac

IPS_OK=0
IPS_FAIL=1
IPS_MP_PREFIX=ha-cluster-

SC_MAXNODEID=64



######################################################
######################################################
#
# framework support
#
#



#
# exit_usage()
#
exit_usage() {
    echo "usage: $0 <scadmindir> <logfile> <|postinstall|preremove|postrmove>"
    exit 1
}





######################################################
######################################################
#
# processing support
#


#
# starttag(); endtag()
#
#	generate the start and end tags for specified package
#
starttag() {
    echo "Start of lines added by $1"
}

endtag() {
    echo "End of lines added by $1"
}


###########################################
#
# append_line_to_file <filename> <text line>
#
###########################################
append_line_to_file()
{
    echo "$2" >> $1
}

###########################################
#
# remove_SC_edits <filename>
#
#	Back out all lines from the given ascii <filename>
#	indicated by the package-specific comment tag lines.
#	Packagename is embedded in $STARTTAG and $ENDTAG
#
###########################################
remove_SC_edits()
{
	update_file=$1
	endpat=\$

	# If the file does not exist, return without error
	if [ ! -f "${update_file}" ]; then
		return 0
	fi

	while :
	do
		/usr/bin/grep "${STARTTAG}$endpat" ${update_file} >/dev/null 2>&1
		case $? in
		2)	# error reading file
			echo "${PROG}:  error reading ${update_file}"
			return 1
			;;

		0)	# grep found the string, so get rid of the entries
			/usr/bin/ed -s ${update_file} << EOF >/dev/null 2>&1
/${STARTTAG}$endpat/,/${ENDTAG}$endpat/d
w
q
EOF
			if [ $? -ne 0 ]; then
				echo "${PROG}: problem updating ${update_file}"
				return 1
			fi
			;;

		*)
			break
			;;
		esac
	done

	return 0
}

###########################################
#
# rbac_add:
#	merge rbac entries into relevant files
#	invokes the rbac class action script from
#	SVR4 packaging.
#
# ** Note that there is no rbac_remove. SVR4's **
#	r.rbac is deliberately a no-op:
# "It is not safe to try and undo what i.rbac has done."
#
# Input:
# $1: name of file of lines to merge into system file
#	basename must match Solaris basenames
#	file is delivered into $RBAC_DIR by pkg(1) manifests
#
# return 0 success or 1 if error
#
###########################################
rbac_add()
{
    infile=${RBAC_DIR}/$1

    # recognized basename?
    basnam=`/usr/bin/basename ${infile}`
    case ${basnam} in
	"prof_attr") ;;
	"exec_attr") ;;
	"auth_attr") ;;
	*) # unrecognized
	    echo "$0.rbac_add(): unrecognized filetype: ${infile}"
	    return 1
	    ;;
    esac

    # i.rbac expects to read two words from stdin for
    # each rbac file to operate on: name of file of lines to 
    # merge in; name of file to merge into
    solaris_file="/etc/security/${basnam}"
    echo "${infile} ${solaris_file}" | /usr/sadm/install/scripts/i.rbac
    status=$?

    return ${status}
}

###########################################
#
# smf_synch_enable
#
#	The specified service is to be enabled synchronously at next boot
#
###########################################
smf_synch_enable()
{
    upgrade_file=/var/svc/profile/upgrade

    if [ ! -f ${upgrade_file} ]; then
	/usr/bin/touch ${upgrade_file}
    fi

    echo "/usr/sbin/svcadm enable -s $1" >> ${upgrade_file}

    return 0
}

###########################################
#
# smf_manifest_rm:
#	invokes the remove manifest class action script from
#	SVR4 packaging.
#
#	** Note that the install class action script is not
#	to be used because it installs the manifest file
#	in the filesystem as well as importing it. **
#
# Input:
# $1: name of service to disable
# $2: name of smf manifest file
#
# return 0 success or 1 if error
#
###########################################
smf_manifest_rm()
{
    svc=$1
    mfstfile=$2

    # disable the service
    echo "\tdisabling ${svc}"
    ${SVCADM} disable ${svc}  2>&1

    # r.manifest expects to read smf manifest
    # filename from sdtin
    echo "\tuninstalling ${mfstfile}"
    echo "${mfstfile}" | /usr/sadm/install/scripts/r.manifest
    status=$?

    return ${status}
}

###########################################
#
# Cleanup any temporary files on exit/interrupt
#	note: must 'return', not 'exit'
#
###########################################
cleanup()
{
	exit_code=$1

	if [ -f "${TMPFILE}" ]; then
		rm -f ${TMPFILE}
	fi

	if [ -f "${TMPFILE1}" ]; then
		rm -f ${TMPFILE1}
	fi

	if [ -f "${TMPFILE2}" ]; then
		rm -f ${TMPFILE2}
	fi

	return ${exit_code}
}

##########################################
###########################################
