#! /usr/xpg4/bin/sh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident "@(#)cmd_showrev.ksh	1.3	08/08/14 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# cmd_showrev [-pv]
#
#	Print revision information by running scinstall -p [-v].
#
#	Options:
#		-p			# option okay, but ignored
#		-v			# print verbose
#
#	Possible exit codes:
#		0  success
#		1  failure
#
#####################################################

#####################################################
#
# Constant Globals
#
#####################################################
# Program name
typeset -r PROG=${0##*/}

# Set the PATH
typeset -r SC_BINDIR=/usr/cluster/bin
typeset -r SC_BINDIRS=${SC_BINDIR}:/usr/cluster/lib/sc
PATH=${SC_BINDIRS}:/bin:/usr/bin:/sbin:/usr/sbin; export PATH

# I18N
typeset -x TEXTDOMAIN=TEXT_DOMAIN
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

#####################################################
#
# print_usage()
#
#	Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "$(gettext 'usage'):  ${PROG} [-pv]" >&2
}

#####################################################
#
# Main
#
#####################################################
main()
{
	typeset c
	typeset verbose=

	integer result

	# Check options
	OPTIND=1
	while getopts pv c
	do
		case ${c} in
		p)    # ignore
			;;

		v)    # verbose
			verbose="-v"
			;;

		*)    # error
			print_usage
			return 1
			;;
		esac
	done

	# Run the command
	scinstall -p ${verbose}
	if [[ $? -ne 0 ]]; then
		return 1
	fi

	# Done
	return 0
}

	main $*
	exit $?
