#! /usr/xpg4/bin/sh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident "@(#)cmd_install.ksh	1.4	08/08/14 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# cmd_install
#
#	Run "scinstall -i" with either -F or -N.   The arguments
#	are the same as "scisntall -i", except that a "-logfile"
#	argument is also accepted.
#
#	Possible exit codes are the same as for "scinstall -i"
#
#####################################################

#####################################################
#
# Constant Globals
#
#####################################################

# Files
typeset -r SCINSTALL_LOCKFILE=/var/cluster/run/scinstall/scinstall.lock

# Program name
typeset -r PROG=${0##*/}

# Set the PATH
typeset -r SC_BINDIR=/usr/cluster/bin
typeset -r SC_BINDIRS=${SC_BINDIR}:/usr/cluster/lib/sc:/usr/cluster/lib/scadmin/lib
PATH=${SC_BINDIRS}:/bin:/usr/bin:/sbin:/usr/sbin; export PATH

# I18N
typeset -x TEXTDOMAIN=TEXT_DOMAIN
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

# Variables recognized by scinstall and scpatchadm
typeset -x SC_INSTALL_LOG
typeset -x SC_PATCH_INSTALL_LOG

#####################################################
#
# print_usage()
#
#       Print usage message to stderr
#
#####################################################
print_usage()
{
        echo "$(gettext 'usage'):  ${PROG} [-logfile <logfile>] <scinstall_install_arguments>" >&2
}   

#####################################################
#
# process_arguments() <get|strip> <argument> <argument_list>
#
#	Get or strip my argument and it's argument option from the
#	<argument_list> and return on success by printing the either
#	the stripped list or the argument option.
#
#	Possible return values:
#
#		0	- argument found
#		1	- argument not found
#
#####################################################
process_arguments()
{
	typeset function=${1}
	typeset argument=${2}
	shift 2
	typeset argument_list="${*}"

	typeset argument_option
	integer result=1

	if [[ "${function}" != "get" && "${function}" != "strip" ]] ||
	    [[ -z "${argument}" ]] ||
	    [[ -z "${argument_list}" ]]; then
		return ${result}
	fi

	typeset argument_option
	typeset remaining_arguments

	remaining_arguments=
	argument_option=
	let found=0
	set -- ${argument_list}
	while [[ $# -gt 0 ]]
	do
		# our option?
		if [[ "${1}" == "${argument}" ]]; then
			shift 1
			if [[ "${1}" != -* ]]; then
				argument_option="${1}"
				shift 1
			fi
			let result=0

		# all other arguments
		else
			remaining_arguments="${remaining_arguments} ${1}"
			shift 1
		fi
	done

	# Print back the argument
	case ${function} in
	get)
		echo ${argument_option}
		;;

	strip)
		echo ${remaining_arguments}
		;;

	*)
		;;
	esac

	# Done
	return ${result}
}
	
#####################################################
#
# Main
#
#####################################################
main()
{
	typeset args="${*}"

	typeset pid
	typeset pids
	typeset foo
	typeset patchargs
	typeset leftoverargs
	typeset cmd
	typeset logfile

	integer result
	integer isrunning
	integer result

	logfile="$(process_arguments "get" "-logfile" "${args}")"
	args="$(process_arguments "strip" "-logfile" "${args}")"

	#
	# Check usage
	#

	# -i is not allowed
	if process_arguments "get" "-i" "${args}" >/dev/null;  then
		print_usage
		return 1
	fi

	# If we are just installing patches, do it
	patchargs="$(process_arguments "get" "-M" "${args}")"
	leftoverargs="$(process_arguments "strip" "-M" "${args}")"
	if [[ -n "${patchargs}" ]] && [[ -z "${leftoverargs}" ]]; then
		SC_PATCH_INSTALL_LOG=${logfile}
		cmd="scpatchadm -M ${patchargs}"
		echo
		echo ${cmd}
		eval ${cmd}
		result=$?
		echo
		if [[ -f "${SC_PATCH_INSTALL_LOG}" ]]; then
			cat ${SC_PATCH_INSTALL_LOG}
		fi

		return ${result}
	fi

	# Must have -F or -N for scinstall
	if ! process_arguments "get" "-F" "${args}" >/dev/null &&
	    ! process_arguments "get" "-N" "${args}" >/dev/null;  then
		print_usage
		return 1
	fi

	# Remove the lock file, if the process is not running
	if [[ -f "${SCINSTALL_LOCKFILE}" ]]; then
		read pid foo <${SCINSTALL_LOCKFILE}
		pids="$(pgrep scinstall)"

		let isrunning=0
		for foo in ${pids}
		do
			if [[ "${pid}" == "${foo}" ]]; then
				let isrunning=1
			fi
		done
		if [[ ${isrunning} -eq 0 ]]; then
			rm -f ${SCINSTALL_LOCKFILE}
		fi
	fi

	# Run scinstall
	SC_INSTALL_LOG=${logfile}
	scinstall -i ${args}
	result=$?

	# Done
	return ${result}
}

	main $*
	exit $?
