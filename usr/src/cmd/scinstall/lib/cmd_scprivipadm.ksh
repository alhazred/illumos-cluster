#! /bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)cmd_scprivipadm.ksh	1.10	09/04/20 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# cmd_scprivipadm <command>
#
#	<command> is:
#
#		isnoncluster		# /usr/sbin/clinfo
#		isconfigpresent		# /usr/bin/ls /etc/cluster/ccr/global/infrastructure
#		ismodifyippresent	# /usr/bin/ls /usr/cluster/lib/sc/modify_ip
#		getchksum		# /usr/bin/cksum /etc/cluster/ccr/global/infrastructure
#		runccradm		# /usr/cluster/lib/sc/ccradm -i /etc/cluster/ccr/global/infrastructure.new -o
#		runtouchkpt		# /usr/bin/touch /etc/cluster/ccr/checkpoint
#		runtestchkpt		# /usr/bin/test -f /etc/cluster/ccr/checkpoint
#		runmvconfig		# /usr/bin/mv /etc/cluster/ccr/global/infrastructure.new /etc/cluster/ccr/global/infrastructure
#		runrmchkpt		# /usr/bin/rm /etc/cluster/ccr/checkpoint
#		runcpconfig		# /usr/bin/cp /etc/cluster/ccr/global/infrastructure /etc/cluster/ccr/global/infrastructure.bak
#		runpingcmd		# /usr/sbin/ping <host>
#		hostnamecheck		# Checks if the hostname has a mapping in /etc/inet/ipnodes and /etc/inet/hosts
#
#####################################################

#####################################################
#
# Constant Globals
#
#####################################################

# Command Set
typeset -r SC_CLINFO=/usr/sbin/clinfo
typeset -r SC_CCRADM=/usr/cluster/lib/sc/ccradm
typeset -r SC_MODIFY_IP=/usr/cluster/lib/sc/modify_ip
typeset -r CKSUM=/usr/bin/cksum
typeset -r LS=/usr/bin/ls
typeset -r CP=/usr/bin/cp
typeset -r TOUCH=/usr/bin/touch
typeset -r TEST=/usr/bin/test
typeset -r MV=/usr/bin/mv
typeset -r RM=/usr/bin/rm
typeset -r PING_CMD=/usr/sbin/ping
typeset -r GREP=/usr/bin/grep

# The base directory
typeset -r SC_BASEDIR=/                                 # Base directory

# The CCR infrastructure file.
typeset SC_CONFIG=${SC_BASEDIR}/etc/cluster/ccr/global/infrastructure
typeset SC_CONFIG_NEW=${SC_BASEDIR}/etc/cluster/ccr/global/infrastructure.new
typeset SC_CONFIG_BAK=${SC_BASEDIR}/etc/cluster/ccr/global/infrastructure.bak

# The CCR directory.
typeset SC_CONFIG_DIR=${SC_CONFIG_DIR:-${SC_BASEDIR}/etc/cluster/ccr/global}

# Local file names
typeset IPNODES_FILE="/etc/inet/ipnodes"
typeset HOSTS_FILE="/etc/inet/hosts"

# Exit codes
integer -r SC_FALSE=0
integer -r SC_TRUE=1
typeset -r SC_ERROR=2 

# Program name
typeset -r PROG=${0##*/}

# Set the PATH
typeset -r SC_BINDIR=/usr/cluster/bin
typeset -r SC_BINDIRS=${SC_BINDIR}:/usr/cluster/lib/sc
PATH=${SC_BINDIRS}:/bin:/usr/bin:/sbin:/usr/sbin; export PATH

#####################################################
#
# print_usage()
#
#	Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "$(gettext 'usage'):  ${PROG} isnoncluster|isconfigpresent|ismodifyippresent|getchksum|runccradm|runtouchkpt|runtestchkpt|runmvconfig|runrmchkpt|runcpconfig|runpingcmd|hostnamecheck" >&2
}

#####################################################
#
# Main
#
#####################################################
main()
{
	typeset command=$1

	case $command in
	isnoncluster)
		/usr/sbin/clinfo > /dev/null 2>&1
		return $?
		;;

	isconfigpresent)
		result=$(/usr/bin/ls ${SC_CONFIG})
		ret=$?
		echo ${result}
		return ${ret}
		;;

	ismodifyippresent)
		result=$(/usr/bin/ls /usr/cluster/lib/sc/modify_ip)
		ret=$?
		echo ${result}
		return ${ret}
		;;

	getchksum)
		result=$(/usr/bin/cksum ${SC_CONFIG})
		ret=$?
		echo "${result}"
		return ${ret}
		;;

	runccradm)
		result=$(/usr/cluster/lib/sc/ccradm -i ${SC_CONFIG_NEW} -o)
		ret=$?
		echo "${result}"
		return ${ret}
		;;

	runtouchkpt)
		result=$(/usr/bin/touch /etc/cluster/ccr/checkpoint)
		ret=$?
		echo ${result}
		return ${ret}
		;;

	runtestchkpt)
		result=$(/usr/bin/test -f /etc/cluster/ccr/checkpoint)
		ret=$?
		echo ${result}
		return ${ret}
		;;

	runmvconfig)
		result=$(/usr/bin/mv ${SC_CONFIG_NEW} ${SC_CONFIG})
		ret=$?
		echo ${result}
		return ${ret}
		;;

	runrmchkpt)
		result=$(/usr/bin/rm /etc/cluster/ccr/checkpoint)
		ret=$?
		echo ${result}
		return ${ret}
		;;

	runcpconfig)
		result=$(/usr/bin/cp ${SC_CONFIG} ${SC_CONFIG_BAK})
		ret=$?
		echo ${result}
		return ${ret}
		;;

	runpingcmd)
		HOSTNAME=${2}
		if [[ -z ${HOSTNAME} ]]
		then
			return ${SC_ERROR}
		fi

		result=$(${PING_CMD} ${HOSTNAME})
		ret=$?
		echo ${result}
		return ${ret}
		;;

	hostnamecheck)
		HOSTNAME=${2}
		if [[ -z ${HOSTNAME} ]]
		then
			return ${SC_ERROR}
		fi

		result1=$(${GREP} -w ${HOSTNAME} ${IPNODES_FILE} | ${GREP} -v "^#")
		ret1=$?

		result2=$(${GREP} -w ${HOSTNAME} ${HOSTS_FILE} | ${GREP} -v "^#")
		ret2=$?

		if [[ ${ret1} -eq 0 || ${ret2} -eq 0 ]]
		then
			return 0
		fi

		return 1
		;;

	*)
		print_usage
		return ${SC_ERROR}
		;;

	esac

	print_usage
	return ${SC_ERROR}
}

#####################################################
#
# Main
#
#####################################################

typeset command=$*

#
# Make sure we dont allow any other command
#

result=$(main ${command})
ret=$?
		
echo "${result}"

return ${ret}

