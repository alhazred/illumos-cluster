#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)update_zones_nsswitch.ksh	1.4	08/07/30 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

SCLIB=/usr/cluster/lib/sc
typeset -r NSS_FILE="/var/cluster/nss_updated"
typeset -r file=/etc/nsswitch.conf
PROG=$(basename $0)
typeset -r updfile="/tmp/nsswitch_tmp_file"
typeset db
typeset numlines


typeset db_files='
		passwd
		group
		hosts
		ipnodes
		networks
		protocols
		rpc
		ethers
		netmasks
		bootparams
		publickey
		automount
		aliases
		services
		auth_attr
		prof_attr
		project
		sendmailvars
		'

typeset db_cluster='
		hosts
		netmasks
		'
typeset ipnodes_line


####################################################
#
# get_new_ipnodes_line()
#       Iterates thru all the sources listed in the
#       input ipnodes line and prepends the status
#       string with TRYAGAIN=0.
#
#       Return values:
#               always 0
#
####################################################
get_new_ipnodes_line()
{
        typeset ipnodes_line=${1}
        typeset -r source_list='nis nisplus dns ldap'
        typeset new_ipnodes_line

        # Check args
        if [[ $# -lt 1 ]] || [[ -z "${ipnodes_line}" ]]; then
		#
		# SCMSGS
		# @explanation
		# An internal error has occurred while updating nsswitch.conf 
		# files on the zone cluster nodes.
		# @user_action
		# Save the /var/adm/messages and the output of nsswitch.conf 
		# file # on the zone cluster nodes and contact your authorized
		# Sun service provider for further assistance.
		#
		${SCLIB}/scds_syslog -p error -t INITSCNGZONES -m \
                "${PROG}:  Internal error - bad call to get_new_ipnodes_line()."
                return 1
        fi

        for source in $source_list
        do
                # Look whether the specified source is available
                echo $ipnodes_line | grep -iw "${source}" >/dev/null 2>&1
                if [[ $? -ne 0 ]]; then
                        # This source is not available, look for the
                        # next source from the list
                        continue
                fi

                # get the list of status for this source.
                # status is the list of things that are mentioned
                # next to the source like [NOTFOUND=return]
                status=$(expr "${ipnodes_line}" : ".*$source[ ]*\[\(\([a-zA-Z=0-9\ ]\)*\)")
                space=" "

                # No status is available for this source,
                # create the status string with TRYAGAIN=0
                try_str="TRYAGAIN=0"
                if [[ -z $status ]]; then
                        new_status="[$try_str]"
                        new_ipnodes_line=`echo $ipnodes_line | sed "s/$source/$source $new_status/"`
                else
                        # Status is listed, look for the TRYAGAIN
                        echo $status | grep -iw "TRYAGAIN" >/dev/null 2>&1
                        if [[ $? -ne  0 ]]; then
                                # TRYAGAIN is not there, prepend the
                                # status string with TRYAGAIN=0
                                new_status="[${try_str}${space}${status}]"
                        else
                                # TRYAGAIN is there, but it may not be the
                                # first rule. Remove the existing and
                                # prepend the status string with
                                # TRYAGAIN=0
                                temp_str=`echo $status | sed -e 's/TRYAGAIN=[0-9a-zA-Z]*//'`
                                new_status="[${try_str}${space}${temp_str}]"
                        fi

                        # New status string with TRYAGAIN entry
                        # is stored in $new_status. Replace the
                        # existing status string with the new status
                        new_ipnodes_line=`echo $ipnodes_line | sed -e "s/$source[ ]*\[$status\]/${source}${space}${new_status}/"`
                fi
                ipnodes_line=${new_ipnodes_line}
        done

        #
        # If the above listed sources like nis are available in
        # the input ipnodes line, then ipnodes_line contains the
        # modified ipnodes line. Otherwise contains the original
        # ipnodes line.
        #
        echo ${ipnodes_line}
        return 0
}

#####################################################################
#
# This program updates the nsswitch.conf file in the non-global zones.
#
# Ensures that the local "files" are refered before remote queries for
# most databases in "nsswitch.conf"
#
# Ensures that the "cluster" is set as the first switch for "hosts" and 
# "netmasks" databases in "nsswitch.conf" 
#
#####################################################################

	# Look for NSS_FILE, if the file is available
	# nsswitch.conf file is already modified. No
	# further changes are needed.

	if [[ -a ${NSS_FILE} ]]; then
		return 0
	fi

	#
	# SCMSGS
	# @explanation
	# The nsswitch.conf(4) configuration file is being updated.
	# @user_action
	# This is an informational message. No user action is required.
	#
	${SCLIB}/scds_syslog -p info -t INITSCZONES -m \
	    "Updating nsswitch.conf ..."

	cp /dev/null ${updfile}
	while read line
	do
		dbline=${line%%:*}
		found=0
		switches=
		space=
		for db in $db_cluster
		do
			if [[ "${db}" == "${dbline}" ]];then
				found=1
				switches="cluster"
				space=" "
				break
			fi
		done

		
		for db in $db_files
		do
			if [[ "${db}" == "${dbline}" ]];then
				found=1
				switches="${switches}${space}files"
				break
			fi
		done
		if [[ ${found} -eq 0 ]];then
			echo "$line" >> ${updfile}
			continue
		fi
		pointer=prefix
		prefix=
		suffix=
		for switch in ${line##*:}
		do
			if [[ "${switch}" == cluster ]];then
				continue
			fi
			if [[ "${switch}" == files ]];then
				continue
			fi
			switches="${switches} ${switch}"
                done

                # Update the status string with TRYAGAIN=0 for all
                # the sources listed for ipnodes database. Otherwise,
                # the address resoultion will hang at the source when
                # the public net is down since the default is
                # TRYAGAIN=forever
                if [[ "${db}" == "ipnodes" ]];then
                        ipnodes_line=${switches}
                        switches=$(get_new_ipnodes_line "${ipnodes_line}")
                fi

                new_dbline=`echo "${line}"|sed -e "s/\(.*:[    ]*\).*/\1${switches}/"`

                # Removing tabs and multiple spaces from the input line
                temp_line=`echo "${line}" | sed 's/[    ][      ]*/ /g' | sed 's/[ ][ ]*/ /g'`

                # Removing tabs and multiple spaces from the updated db line
                temp_new_dbline=`echo "${new_dbline}" | sed 's/[        ][      ]*//g' | sed 's/[ ][ ]*/ /g'`

                # Commenting out the input line in the output file if updated line is different from it
                if [[ "${temp_line}" != "${temp_new_dbline}" ]] ; then

                        # Avoiding duplicate db lines in the output file while commenting out the existing line
                        if ! cat ${updfile} | sed 's/[  ][      ]*/ /g' | sed 's/[ ][ ]*/ /g' | fgrep -e "#${temp_line}" >/dev/null 2>&1; then

                                echo "#$line" >> ${updfile}
                        fi
                fi

                # Adding the db line to the output file
                if ! cat ${updfile} | grep -v '^#' | sed 's/[   ][      ]*/ /g' | sed 's/[ ][ ]*/ /g' | fgrep -e "${temp_new_dbline}" \>/dev/null 2>&1
                then

                        echo "${new_dbline}" >> ${updfile}
                fi

        done < ${file}

        # Make sure that there is a single "db" entry
        for db in $db_cluster
        do
                numlines=$(grep -c '^'${db}':' ${file} 2>/dev/null)
                case ${numlines} in
                0)      # No lines
			#
			# SCMSGS
			# @explanation
			# The nsswitch.conf file on the zone cluster does not
			# have this entry already.
			# A new entry has been added for this.
			# @user_action
			# No user action is required.
			#
			${SCLIB}/scds_syslog -p warning -t INITSCNGZONES -m \
                            "${PROG}: WARNING: ${file} does not include a ${db} entry"

			# Add a new line which includes both the files and cluster switches
                        space=" "
                        switches="cluster${space}files${space}nis"
                        echo "${db}:   ${switches}" >> ${updfile}
                        ;;

                1)      # One line is good
                        ;;

                *)      # More than one line is not good
			#
			# SCMSGS
			# @explanation
			# The nsswitch.conf file on the zone cluster has more
			# than one entry for this.
			# @user_action
			# Remove the duplicate entries from the nsswitch.conf 
			# file of the zone cluster nodes.
			#

			${SCLIB}/scds_syslog -p warning -t INITSCNGZONES -m \
                            "${PROG}: WARNING: ${file} has ${numlines} ${db} entries."
                        ;;
                esac
        done

        for db in $db_files
        do
                numlines=$(grep -c '^'${db}':[  ]' ${file} 2>/dev/null)
                case ${numlines} in
                0|1)    # No lines or One line
                        ;;

                *)      # More than one line is not good

			${SCLIB}/scds_syslog -p warning -t INITSCNGZONES -m \
                            "${PROG}: WARNING: ${file} has ${numlines} ${db} entries."
                        ;;
                esac
        done

        cp ${updfile} ${file} || return 1
        rm -f ${updfile}

        # Done
	#	
	# SCMSGS
	# @explanation
	# The nsswitch.conf file is successfully updated on the zone cluster.
	# @user_action
	# No user action is required.

	${SCLIB}/scds_syslog -p info -t INITSCNGZONES -m \
	    "Successfully updated nsswitch.conf file." 
	
	# Touch this file to know that nsswitch.conf file has been udpated
	touch ${NSS_FILE}

        return 0
