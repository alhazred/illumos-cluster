#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)scinstall_upgrade.ksh	1.89	08/12/03 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# upgd_framework cdimagebasedir
#
#       cdimagebasedir          location of .cdtoc file
#
#       Upgrade the framework packages.
#
#       This function removes old framework packages and installs new ones
#       on the node from which it is called.
#
#	If the file /etc/cluster/pnmconfig exists, this function may modify
#       /etc/hostname.<if> files on the node from which it is called.
#	Those /etc/hostname.<if> files which already include ifconfig group
#	parameters are left unmodified.
#
#	This function creates and removes temporary files under /var/cluster.
#	It also	leaves behind a log file under /var/cluster/logs/install.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
upgd_framework()
{
	# check arg
        if [[ $# != 1 ]]  then
                printf "%s:  $(gettext 'Internal error - bad call to upgd_framework()')\n" ${PROG} >&2
                return 2
        fi

        typeset -r cdimagebasedir=$1
        typeset upgradeflag
	typeset installed_rac
	typeset installed_scm
	integer cacao_files=${SC_FALSE}
	typeset -r upgd_txt_p1="$(gettext '
		The release of \"Sun Cluster Support for Oracle Real Application
		Clusters\" must correspond to the release of Sun Cluster software
		that you just upgraded to. Upgrade \"Sun Cluster Support for
		Oracle Real Application Clusters\" to the corresponding release.
	')"

	# Check that the node is currently not upgrading other Cluster software.
        upgradeflag=$(upgd_get_upgradeflag)
        if [[ $? -eq ${SC_TRUE} ]] &&
	   [[ "${upgradeflag}" != "framework" ]];then
                printf "\n\n%s:  $(gettext 'Cannot upgrade Cluster framework.')\n" ${PROG} | logerr
                printf "%s:  $(gettext 'A %s upgrade is still in progress.')\n" ${PROG} ${upgradeflag} | logerr
                return 1
	fi

	# Validate hardware configuration for upgrade to current release.
	if [[ ${upgrade_skip_hw} -eq ${SC_TRUE} ]]; then
		upgd_validate || return 1
	fi

	printf "\n\n$(gettext 'Starting upgrade of Sun Cluster framework software')\n" | logmsg

	# Check that the given IPMP configurations and -S option is valid.
        upgd_nafo_to_ipmp  "check" || return 1

	# Check to see if Sun Web Console, JDMK, and Cacao are installed
	sc_is_ips
	if [[ $? -ne 0 ]]; then
		# Only check it if it is not IPS
		checkforprereqs ${cdimagebasedir} || return 1
	fi

	# Check the arhcitecture of the new packages, if does not match current arch then fail.
	checkpkgarch ${cdimagebasedir} || return 1

	# Check whether Sun Cluster Manager is present in this cluster or not
	installed_scm=$(isSCMinstalled)

	# Check that the package being upgraded does not have a higher 
	# version patch installed on the node. 

	check_patchversion_foreach_package ${cdimagebasedir} || return 1

	# Save cluster configurations for idempotent.
        upgd_save_config ${cdimagebasedir} || return 1

	set_noclustermode ${SC_TRUE} || return 1

	# Convert NAFO adapter configurations to IPMP groups.
        upgd_nafo_to_ipmp  "convert" || return 1

	# Save /etc/cluster/ccr directory.
        upgd_save_ccr || return 1

	# Check whether cacao security files exist
	CERTSDIR=${SC_BASEDIR}/etc/cacao/instances/default/security
	if [ -f ${CERTSDIR}/jsse/keystore ] ; then
		cacao_files=${SC_TRUE}
	fi

	# Save a list of locales currently installed, so that upgrade can add
	# the same locales again.
	save_locale_settings || return 1

	# Check for SUNWscucm inetd.conf and rpc file entries, if they
	# exist, then we need to change the start and end tag entries
	# and then revert them after the upgrade.
	#
	# This is a workaround for CR 6199128.  After 3.2 has been released
	# it should be safe to remove this code because upgrades will no
	# longer be supported from the original code containing the problem.
	SUNWscucm_file_edits "save" || return 1

	# Ensure the webserver is stopped, and then restart it after the
	# packages have been added again.  This will improve the speed at
	# which upgrades are performed.
	if [[ -z "${SC_BASEDIR}" ]] || [[ "${SC_BASEDIR}" = "/" ]]; then
		sc_is_ips
		if [[ $? -ne 0 ]]; then
			/usr/sbin/smcwebserver stop 2>&1 >/dev/null
		fi
	fi

	# Change the "cluster" brand config file to a "null" one so that
	# there is no problem upgrading software in cluster branded
	# non-global zones.
	save_cluster_brand_def || return 1

	# Remove all the framework packages installed on the node.
	uninstallframework "" ${SC_UPGD_FMPKGS_FILE} all || return 1

	# Old SUNWscspmu remove scripts will restart the webserver after 
	# package removal.  So, we need to stop it again once the packages
	# have been uninstalled, before the new installation.
	if [[ -z "${SC_BASEDIR}" ]] || [[ "${SC_BASEDIR}" = "/" ]]; then
		sc_is_ips
		if [[ $? -ne 0 ]]; then
			/usr/sbin/smcwebserver stop 2>&1 >/dev/null
		fi
	fi

	# Restore the SUNWscucm config file entries 
	SUNWscucm_file_edits "restore" || return 1

	# Install the required framework packages on the node.
	installframework ${cdimagebasedir} ${installed_scm} || return 1
		
	# Install other/optional cluster packages on the node.
	installothers ${cdimagebasedir} "$(cat ${SC_UPGD_FMCLS_FILE})" || return 1

	# Restore cluster brand definition file
	restore_cluster_brand_def || return 1

	# Now start the webserver again
	if [[ -z "${SC_BASEDIR}" ]] || [[ "${SC_BASEDIR}" = "/" ]]; then
		sc_is_ips
		if [[ $? -ne 0 ]]; then
			/usr/sbin/smcwebserver start 2>&1 >/dev/null
		fi
	fi

	# Set eeprom for sparc
	if [[ "${SC_ARCH}" == "sparc" ]]; then
		printf "\n" | logmsg
		set_eeprom || return 1
	fi

	# Check and rename ntp.conf file
	upgd_ntp_config || return 1

	# Restore saved configurations
	upgd_restore_config || return 1

	# For upgrades from SC < 3.1u3 to SC >= 3.1u3
	# Create a warning message about
	# manually resyncing the key files
	if [ ${cacao_files} -eq ${SC_FALSE} ]; then
	    printf "\n\n$(gettext 'ERROR: Security keys for the common agent container do not match on all nodes.')\n" | logmsg
	    printf "$(gettext 'After the upgrade is complete and all nodes are rebooted, manually copy the\nsecurity keys from any node that you choose to all other nodes in the cluster.')\n" | logmsg
	    printf "$(gettext 'To copy the security keys, copy all files under the\n/etc/cacao/instances/default/security directory.')\n" | logmsg
	fi

	# Unset no-clustermode to allow the node to boot into cluster.
	set_noclustermode ${SC_FALSE} || return 1

	# Perform CCR transformation
	transform_ccr || return 1

	# Set the name of current Live Upgrade BE
	save_LU_BE_name || return 1

	printf "\n\n$(gettext 'Completed Sun Cluster framework upgrade')\n" | logmsg

	installed_rac=$(check_installed_rac_types)
	if [[ $? -eq ${SC_TRUE} ]]; then
		printf "\n\n" | logmsg
		sc_print_para "${upgd_txt_p1}" | logmsg
	fi

	return 0

}


################################################################
# change_ccr_layout
#
# Covert the old CCR layout to the new layered CCR layout if it
# is not so already.
#
#
# 	Return:
#		zero	:	success
#		non-zero:	failure
################################################################
change_ccr_layout()
{
	if [[ ! -d ${SC_BASEDIR}/etc/cluster/ccr/global ]]; then
		mkdir ${SC_BASEDIR}/etc/cluster/ccr/global 2>/dev/null
		if [[ $? -ne 0 ]]; then
			return 1
		fi

		chmod 0755 ${SC_BASEDIR}/etc/cluster/ccr/global || return 1

		#
		# Move all the tables to the global directory.
		#
		for table in `ls ${SC_BASEDIR}/etc/cluster/ccr`
		do
			if [[ ! -d "${SC_BASEDIR}/etc/cluster/ccr/${table}" ]] &&
			    [[ "${table}" != "cluster_directory" ]]; then
				mv ${SC_BASEDIR}/etc/cluster/ccr/${table} ${SC_BASEDIR}/etc/cluster/ccr/global 2>/dev/null || return 1
			fi
		done
		#
		# The cluster_directory table has been moved to /
		# during the upgrade. Move it back to the proper place.
		#
		mv ${SC_BASEDIR}/cluster_directory ${SC_BASEDIR}/etc/cluster/ccr 2>/dev/null || return 1
	fi
	#
	# We have to remove this anyway if we did not have to do the
	# CCR conversion.
	#
	rm -f ${SC_BASEDIR}/cluster_directory 2>/dev/null
	return 0
}

#######################################################
# transform_ccr
#
# 	This function will apply the ccr transformation 
# 	after the new sun cluster packages are installed
# 	ONLY in a non QL and non LU upgrade scenario.
#
#	If LU (Live Upgrade) is going on, then touch a
#	file (transform_ccr) so that in next boot the
#	RGM ccr transformations will be applied.
#
# 	This function does not take any parameters.
#
# 	Return:
#		zero	:	success
#		non-zero:	failure
#######################################################
transform_ccr()
{
	typeset -i retval=0

	#
	# Change the CCR layout. We have to do this before checking for
	# QL.
	#
	change_ccr_layout

	if [[ $? -ne 0 ]]; then
		printf \
		    "$(gettext '%s:  Failed to change CCR layout')\n" \
		    "${PROG}" >> ${install_log}
		return 1
	fi

	#
	# Get the newer RT files
	#
	for table in `ls ${SC_BASEDIR}/etc/cluster/original/ccr/global`
	do
		# Only need to get newer RT files
		if [[ "${table}" == rgm_rt_SUNW* ]] &&
		    [[ ! -f "${SC_BASEDIR}/etc/cluster/ccr/global/${table}" ]]; then
			cp ${SC_BASEDIR}/etc/cluster/original/ccr/global/${table} ${SC_BASEDIR}/etc/cluster/ccr/global || return 1
			chmod 0600 ${SC_BASEDIR}/etc/cluster/ccr/global/$table || return 1
		fi
	done

	#
	# Check if LU is going on. If LU is going on, then create
	# a transform_ccr file so that in next cluster mode bootup,
	# the RGM ccr transformation would take place.
	#
	if [[ ! -z "${SC_BASEDIR}" ]] && [[ "${SC_BASEDIR}" != "/" ]]; then
		#
		# Alternate root is set. Either Live Upgrade
		# or a jumpstart upgrade is going on.
		# RGM CCR transformation needs to be done on the
		# next cluster mode boot. Hence create the
		# transform_ccr file.
		#
		/usr/bin/touch ${SC_QL_TRANSFORM_CCR_FILE}
		retval=$?
		if [[ ${retval} != 0 ]]; then
			printf \
			    "$(gettext '%s:  Failed to create %s Retval = %d')\n" \
			    "${PROG}" "${SC_QL_TRANSFORM_CCR_FILE}" "${retval}" >> ${install_log}
			printf \
			    "$(gettext '%s:  Failed to create Cluster Configuration Repository transformation file.')\n" \
			    "${PROG}" | logerr
			return 1
		fi

		printf \
		    "$(gettext '%s:  Created file %s successfully.')\n" \
		    "${PROG}" "${SC_QL_TRANSFORM_CCR_FILE}" >> ${install_log}
		return 0
	fi

	#
	# Check if QL upgrade is going on
	#
	if [[ -f ${SC_QL_STATE_FILE} ]]; then
		#
		# QL upgrade is in progress.
		# No need to do CCR transformation
		#
		printf \
		    "$(gettext '%s:  CCR transformation not required')\n" \
		    "${PROG}" >> ${install_log}
		return 0
	fi

	#
	# Execute the transformation utility
	#
	if [[ -x ${SC_QL_DATA_CHANGE} ]]; then
		printf "$(gettext '%s:  Executing %s')\n" "${PROG}" \
		    "${SC_QL_DATA_CHANGE}" >> ${install_log}
		${SC_QL_DATA_CHANGE} > /dev/null 2>&1
		retval=$?
		if [[ ${retval} -ne 0 ]]; then
			printf \
			    "$(gettext '%s:  %s returned %d')\n" \
			    "${PROG}" "${SC_QL_DATA_CHANGE}" "${retval}" >> ${install_log}
			printf \
			    "$(gettext '%s:  Failed to upgrade the Cluster Configuration Repository.')\n" \
			    "${PROG}" | logerr
			return 1
		fi
		printf "$(gettext 'Successfully upgraded the Cluster Configuration Repository.')\n" | logmsg 
	fi

	# Success
	return 0
}


#######################################################
#
# save_LU_BE_name()
#
#	Saves the name of the current Live Upgrade
#	boot environment (BE) so that after the LU, the
#	system will be able to copy CCR files and other
#	critical system files to the new BE.
#
#       Return:
#               zero    :       sucess
#               non-zero:       failure
#######################################################
save_LU_BE_name()
{
	typeset current_be_dev

	if [[ -z "${SC_BASEDIR}" ]] || [[ "${SC_BASEDIR}" = "/" ]]; then
		return 0
	fi

	# Get the name of the current BE
	CURRENT_BE=`lumount 2>/dev/null | grep '/ *$' | awk '{print $1}'`
	if [[ -z "${CURRENT_BE}" ]]; then
		printf "$(gettext '%s: Unable to determine the name of the current Live Upgrade Boot Environment'.)\n" "${PROG}" | logerr
		return 1
	else
		# In case the current BE is also from LU
		current_be_dev=`lufslist ${CURRENT_BE} 2>/dev/null | grep '/ ' | awk '{if ($4 == "/") print $1}'`
		if [[ -z "${current_be_dev}" ]]; then
			printf "$(gettext '%s: Unable to determine the device of the current Live Upgrade Boot Environment'.)\n" "${PROG}" | logerr
			return 1
		fi
		echo "${CURRENT_BE} ${current_be_dev}" > ${SC_LUBENAME_FILE}
		if [[ $? -ne 0 ]]; then
			printf "$(gettext '%s: Unable to save the device of the current Live Upgrade Boot Environment'.)\n" "${PROG}" | logerr
			return 1
		fi

		return 0
	fi
}

#######################################################
#       get_max_revision patchlist patch_id
#
#       patchlist       list of patches associated with a package
#       patch_id        patch id that need to be searched in list
#
#       This function searchs the patchlist for maximum
#       revision of patch whose id is patch_id
#
#       Return:
#               zero    :       sucess
#               non-zero:       failure
########################################################

get_max_revision()
{
        typeset patch_list
        typeset patch_id
        typeset max_revision=0
        typeset i

        #check args
        if [[ $# -lt 2 ]]; then
                printf "$(gettext '%s:  Internal error - bad call to get_max_revision()')\n" ${PROG} >&2
                return 1
        fi

        # read arguments
        patch_list=$@
        patch_id=$1

        i=0

        for patch in ${patch_list}
        do
                if [ $i -eq 0 ] ; then
                        #skip first argument as it contains patch_id
                        i=1
                        continue
                fi
                #get patch_id
                lst_patch_id=${patch%-*}
                if [ $lst_patch_id = $patch_id ] ; then
                        #found the patch.see if it is of higher version
                        lst_patch_rev=${patch##*-}
                        if [ $lst_patch_rev -ge $max_revision ] ; then
                                max_revision=$lst_patch_rev
                        fi
                fi
        done
                echo "$max_revision"
                return 0
}


#####################################################
#
#  check_patchversion_foreach_package cdimagebasedir
#
#       cdimagebasedir          location of .cdtoc file
#
#       Check if the existing package has a higher patch version installed
#
#       This function checks if the package being upgraded has a higher version
#	of patch already installed on the node. If that is the case, the function
#	returns an error.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################

check_patchversion_foreach_package()
{ 
	typeset -r cdimagebasedir=$1

	typeset realcdimage
        typeset productdir
        typeset productrel
        typeset pkglist
	typeset rootarg=

	typeset patchlist_old
	typeset patchlist_new

        # Check arg
        if [[ $# -ne 1 ]]; then
                printf "$(gettext '%s:  Internal error - bad call to check_patchversion_foreach_package()')\n" ${PROG} >&2
                return 1
        fi

	if [[ -n "${SC_BASEDIR}" ]] && [[ "${SC_BASEDIR}" != "/" ]]; then
                rootarg="-R ${SC_BASEDIR}"
        fi

        #
        # Note that the cdimagebasedir may be given as either the
        # directory containing the .cdtoc we are looking for OR
        # the directory above that.  This call to find_cdimagebasedir()
        # will reset the cdimagebasedir to be the directory containing
        # our .cdtoc.
        #
        realcdimage=$(find_cdimagebasedir "${cdimagebasedir}" "${SC_PRODUCT}" "${SC_CLUSTER}") || return 1

        # get the name of the product directory
        productdir=$(getproduct ${realcdimage}/${SC_CDTOC} "${SC_PRODUCT}" ${SC_CLUSTER} "dir") || return 1

        # get the product release
        productrel=$(getproduct ${realcdimage}/${SC_CDTOC} "${SC_PRODUCT}" ${SC_CLUSTER} "rel") || return 1

        # get the list of packages
        pkglist="$(print_clustertoc ${productdir}/${SC_CLUSTERTOC} ${SC_CLUSTER} "packages")" || return 1

        # order the list of packages
        pkglist="$(order_packages ${productdir}/${SC_ORDER} "${pkglist}")" || return 1

	set -A pkglist $pkglist

	# for each package
	let i=0
	while [[ -n "${pkglist[i]}" ]]
        do
                # Make sure we can find the package
                if [[ ! -d ${productdir}/${pkglist[i]} ]]; then
                        printf "$(gettext '%s:  Unable to find \"%s\"')\n" "${PROG}" "${pkglist[i]}" | logerr
                        return 1
                fi
		
		# compare the patchlist
		# patchlist_new is a list of patches that would be installed
		# patchlist_old is a list of patches already installed
		patchlist_new=$(pkgparam -d ${productdir} ${pkglist[i]} PATCHLIST 2>/dev/null)
		patchlist_old=$(pkgparam ${rootarg} ${pkglist[i]} PATCHLIST 2>/dev/null)

		if [[ -z "${patchlist_new}" || -z "${patchlist_old}" ]]; then
			#Skip it
			((i += 1))
			continue
		fi

		# compare the rev of the latest patch applied to the package
		for patch_new in ${patchlist_new}
		do
			for patch_old in ${patchlist_old}
			do
				# Every patch has a patchid and a rev (patchid-rev).
				pbase_new=${patch_new%-*}
				pbase_old=${patch_old%-*}

				if [[ "${pbase_new}" = "${pbase_old}" ]]; then

					#get maximum revisions of this patch_id
					prev_old="$(get_max_revision $pbase_old ${patchlist_old[@]})"
					prev_new="$(get_max_revision $pbase_new ${patchlist_new[@]})"

					if [[ "${prev_new}" -lt "${prev_old}" ]]; then
						printf "\n\n$(gettext '%s: The upgrade is trying to install patch %s for package %s which would overwrite the patch %s already installed on the node. Please remove the patch %s manually and then try to install it again after the upgrade has finished.')\n" "${PROG}" "${patch_new}" "${pkglist[i]}" "${patch_old}" "${patch_old}"| logerr
						return 1
					fi
				fi
			done
		done
		#next
		((i += 1))
	done #while
	
	# no error was found. Return 0
	return 0
}
	
#####################################################
#
# upgd_dataservices cdimagebasedir "services"
#
#       cdimagebasedir          location of .cdtoc file
#       "services"              list of services
#
#       Upgrade the data services packages.
#	This function removes old data service packages and
#	installs new ones on the node from which it is called.
#
#	This function creates and removes temporary files under /var/cluster.
#	It also leaves behind a log file under /var/cluster/logs/install.
#
#	In the event that a data services upgrade is interrupted, this function
#	is designed to be capable of restarting the upgrade. As such, an upgrade
#	flag file is kept on disk to prevent other upgrade operations
#	(e.g., framework) from starting. For data services, this upgrade flag
#	file is called "upgrading_dataservices"; and, when an individual data
#	service is in the process of being upgraded, this file contains the
#	resource type name for that service.  Also, a list of software
#	packages discovered in the PKGLIST for resource type is saved to disk
#	until all the old packages are removed.  This enables a restart of the
#	dataservices upgrade process.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################

upgd_dataservices()
{
	# check arg
        if [[ $# != 2 ]]  then
                printf "%s:  $(gettext 'Internal error - bad call to upgd_dataservices()')\n" ${PROG} >&2
                return 2
        fi

        typeset -r cdimagebasedir=$1
        typeset -r services=$2

        typeset  service
        typeset  rtname
        typeset  upgradable_rtlist
        typeset  upgradable_rt
        typeset  upgrade_rtlist
        typeset  upgrade_rt
        typeset  uninstall_pkglist
        typeset  uninstall_pkg
        typeset  upgradeflag
	typeset	 dir
	typeset  pkglist
        integer  badservices=0
        integer  found
	typeset  pkg
        typeset  installed_rac

	# Check that the node is currently not upgrading other Cluster software.
	upgradeflag=$(upgd_get_upgradeflag)
	if [[ $? -eq ${SC_TRUE} ]] &&
	   [[ $upgradeflag != "dataservices" ]];then
		printf "\n\n%s:  $(gettext 'Cannot upgrade Sun Cluster data services.')\n" ${PROG} | logmsg
		printf "%s:  $(gettext 'A Sun Cluster %s upgrade is still in progress.')\n" ${PROG} ${upgradeflag} | logmsg
		return 1
	fi

	# List of all installed and Upgradable services on the node.
	upgradable_rtlist=$(get_installed_services "${cdimagebasedir}") || return 1

	# Check if any services can be upgraded with the current CDROM
	if [[ -z ${upgradable_rtlist} ]];then
		printf "\n\n%s:  $(gettext 'No upgradable data services found.')\n" ${PROG} | logerr
		printf "%s:  $(gettext 'Please, load the correct data services CD.')\n" ${PROG} | logerr
		return 1
	fi

	# Check for RAC
	for service in $(upgd_get_current_dataservice) ${services}
	do
		if [[ "${service}" == "${ORACLE_RAC_SERVICE}" ]]; then
			installed_rac=$(check_installed_rac_types)
			break
		fi
	done

	for service in ${upgradable_rtlist}
	do
		if [[ "${service}" == "${ORACLE_RAC_SERVICE}" ]] ||
		   [[ "${service}" == rac_* ]]; then
			SC_SERVICE=${SC_RAC}
		fi
   	
		# Get the directory
		dir="$(getproduct ${cdimagebasedir}/${SC_CDTOC} "" ${SC_SERVICE}${service} "dir")"
		if [[ -z "${dir}" ]]; then
			printf "$(gettext 'Cannot find the directory for data service \"%s\".')\n" "${service}"
			printf "$(gettext 'Skipping \"%s\".')\n\n\a" "${service}"
			return 1 
		fi
	
		# Check for the dataservice package architecture
		checkpkgarchds ${dir} ${service}
		if [[ $? -ne 0 ]]; then
			return 1
		fi

		SC_SERVICE="SUNWC_DS_"
	done

	# Assemble a list of services selected for the current upgrade.
	if [[ "${services}" == "all" ]]; then
		upgrade_rtlist=${upgradable_rtlist}
	else
		upgrade_rtlist=
		for service in $(upgd_get_current_dataservice) ${services} ${installed_rac}
		do
			let found=0
			for upgradable_rt in ${upgradable_rtlist}
			do
				if [[ "${upgradable_rt}" == "${service}" ]];then
					let found=1
					break
				fi
			done
			if [[ ${found} -eq 0 ]];then
				printf "\n%s:  $(gettext 'Cannot Upgrade data service "%s" with current CDROM')\n" ${PROG} ${service} | logerr
				printf "%s:  $(gettext 'Service "%s" either not installed or not-upgradable')\n" ${PROG} ${service} | logerr
				(( badservices += 1 ))
			else
				let found=0
				for upgrade_rt in ${upgrade_rtlist}
				do
					if [[ "${upgrade_rt}" == "${service}" ]];then
						let found=1
						break
					fi
				done
				if [[ ${found} -eq 0 ]];then
					upgrade_rtlist="${upgrade_rtlist} ${service}"
				fi
		  	fi
		done
		if [[ ${badservices} -gt 0 ]];then
			printf "\n\n%s:  $(gettext 'One or more dataservices specified are invalid.')\n" ${PROG} | logerr
			return 1
		fi
	fi

	printf "\n\n$(gettext 'Starting upgrade of Sun Cluster data services agents')\n" | logmsg
		
	# Output list of services and those selected for upgrade.
	printf "\n\n$(gettext 'List of upgradable data services agents:')\n" | logmsg
	printf "  $(gettext '(*) indicates selected for upgrade.')\n\n" | logmsg
	for upgradable_rt in ${upgradable_rtlist}
	do
		let found=0
		for upgrade_rt in ${upgrade_rtlist}
		do
			if [[ "${upgradable_rt}" == "${upgrade_rt}" ]];then
				let found=1
				break
			fi
		done
		if [[ $found -eq 0 ]];then
			printf "	  $(gettext '%s')\n" "${upgradable_rt}"| logmsg
		else
			printf "	$(gettext '* %s')\n" "${upgradable_rt}"| logmsg
		fi
	done
			
	# Assemble a array of localization packages installed on the node
	# Packagename V/s SUNW_PKGLIST  for the specific L10n package.
	set -A SC_L10N_PKG_MAP
	i=0
	for pkg in $(pkginfo | awk '{print $2}')
	do
		pkglist="$(pkgparam ${pkg} SUNW_PKGLIST)"
		if [[ -n "${pkglist}" ]];then
			SC_L10N_PKG_MAP[i]=${pkg}
			SC_L10N_PKG_MAP[i+1]="${pkglist}"
			(( i += 2 ))
		fi
	done

	printf "\n\n$(gettext 'Upgrading Sun Cluster data services agents software')\n" | logmsg

	# Node must not boot into clustermode until this completes.
	set_noclustermode ${SC_TRUE} || return 1

	# Set the type of upgrade.
	upgd_set_upgradeflag ${SC_TRUE} dataservices || return 1

	for rtname in ${upgrade_rtlist}
	do
		# Sets the service being upgraded.
		upgd_set_current_dataservice ${rtname}  || return 1

		# Create or get the list of packages for the service.
		if [[ ! -f ${SC_UPGD_DSPKGS_FILE} ]];then
			uninstall_pkglist=$(get_installed_service_packages ${rtname}) || return 1
			cp /dev/null ${SC_UPGD_DSPKGS_FILE} || return 1
			for uninstall_pkg in ${uninstall_pkglist}
			do
				echo ${uninstall_pkg} >> ${SC_UPGD_DSPKGS_FILE}
			done
		else
			uninstall_pkglist=$(cat ${SC_UPGD_DSPKGS_FILE})
		fi

		# Remove the service and the packages listed for the service.
		if [[ -n "${uninstall_pkglist}" ]];then
	 		uninstallservice ${rtname} "${uninstall_pkglist}" || return 1
		fi

		# Remove the list of packages for the service.
                rm -f ${SC_UPGD_DSPKGS_FILE}

		# Install the service from the CDROM
		SC_SERVICE="SUNWC_DS_"
		if [[ "${rtname}" == "${ORACLE_RAC_SERVICE}" ]] ||
		   [[ "${rtname}" == rac_* ]]; then
			SC_SERVICE=${SC_RAC}
		fi
		installservices ${cdimagebasedir} ${rtname} || return 1

		# Clear the current service name.
		upgd_set_current_dataservice || return 1

	done

	# Unset the type of upgrade.
	upgd_set_upgradeflag ${SC_FALSE} dataservices || return 1

	# Node may boot into clustermode
	set_noclustermode ${SC_FALSE} || return 1

	# Reset the SC_SERVICE to Data Service.
        SC_SERVICE="SUNWC_DS_"


	printf "\n\n$(gettext 'Completed upgrade of Sun Cluster data services agents')\n" | logmsg

	return 0
}

#####################################################
#
# upgd_get_upgradeflag
#
#	This function determines whether or not either a "framework" or
#	"dataservices" upgrade is or was in progress.  In the event that an
#	upgrade is interrupted,	this file helps to provide status to scinstall
#	when it is re-started by the user.
#
#	This function checks for the presence of either ${SC_UPGD_UPFM_FILE}
#	or ${SC_UPGD_UPDS_FILE}.  If either of these two files exist, this
#	function returns ${SC_TRUE} and prints either "framework" or
#	"dataservices".  If neither of the two files exist, ${SC_FALSE} is
#	returned.
#
#       Return:
#               ${SC_TRUE}      Upgrade flag file exists
#               ${SC_FALSE}     Upgrade flag file is missing
#
#####################################################
upgd_get_upgradeflag()
{
	if [[ -f ${SC_UPGD_UPFM_FILE} ]] ; then
		echo "framework"
		return ${SC_TRUE}
	else
		if [[ -f ${SC_UPGD_UPDS_FILE} ]]; then
			echo "dataservices"
			return ${SC_TRUE}
		else
			return ${SC_FALSE}
		fi
	fi
}


#####################################################
#
# upgd_set_upgradeflag createflag type
#
#       createflag      ${SC_TRUE}  	Creates the flagfile
#                       ${SC_FALSE} 	Removes the flagfile
#
#       type            Type of flagfile to create "framework" or "dataservices"
#
#	This function creates or removes an upgrade flag file. This file is used
#	by upgd_get_upgradeflag() to determine whether or not either a
#	"framework" or "dataservices" upgrade is or was in progress.  In the
#	event that an upgrade is interrupted, this file helps to provide status
#	to scinstall when it is re-started by the user.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
upgd_set_upgradeflag()
{
	# check arg
        if [[ $# != 2 ]]  then
                printf "%s:  $(gettext 'Internal error - bad call to upgd_set_upgradeflag()')\n" ${PROG} >&2
                return 2
        fi
        integer -r createflag=$1
        typeset -r type=$2

        typeset gottype
	

	case "${createflag}" in

	${SC_TRUE})
		gottype=$(upgd_get_upgradeflag)
		if [[ $? -eq ${SC_TRUE} ]] &&
	   	   [[ "${type}" != "${gottype}" ]]; then
               		printf "\n%s:  $(gettext 'Cannot upgrade %s.')\n" ${PROG} ${type} | logerr
			printf "%s:  $(gettext 'A %s upgrade is still in progress.')\n" ${PROG} ${gottype} | logerr
			return 2
		fi
		case "${type}" in

		framework)
			touch ${SC_UPGD_UPFM_FILE} || return 1
			;;
		dataservices)
			touch ${SC_UPGD_UPDS_FILE} || return 1
			;;
		*)
			printf "%s:  $(gettext 'Internal error - Bad call to upgd_set_upgradeflag():type %s')\n" ${PROG} ${type} >&2
			return 1
			;;
		esac	
		;;

	${SC_FALSE})
		case "${type}" in
		framework)
			rm -f ${SC_UPGD_UPFM_FILE}
			;;
		dataservices)
			rm -f ${SC_UPGD_UPDS_FILE}
			;;
		*)
			printf "%s:  $(gettext 'Internal error - Bad call to upgd_set_upgradeflag():type %s')\n" ${PROG} ${type} >&2
			return 1
			;;
		esac	
		;;

	*)
		printf "%s:  $(gettext 'Internal error - Bad call to upgd_set_upgradeflag()')\n" ${PROG} >&2
		return 1
		;;

	esac	
	return 0
}


#####################################################
#
# upgd_get_current_dataservice
#
#	This function prints the name of the resource type, or data service,
#	currently being upgraded.  For data service upgrades, the name of the
#	resource type being upgraded is stored in the data services upgrade flag
#	file, ${SC_UPGD_UPDS_FILE}. In the event that an upgrade is interrupted,
#	this information helps to provide status to scinstall when it is
#	re-started by the user.
#
#       Return:
#               zero            Always
#
#####################################################
upgd_get_current_dataservice()
{
	if [[ -f ${SC_UPGD_UPDS_FILE} ]];then
		(cat ${SC_UPGD_UPDS_FILE})
	fi
	return 0
}

#####################################################
#
# upgd_set_current_dataservice [service]
#
#       [service]              Service name to set ( Optional )
#
#	This function sets the name of the resource type, or data service,
#	currently being upgraded to the given data service name.  For data
#	service upgrades, the name of the resource type being upgraded is
#	stored in the data services upgrade flag file, ${SC_UPGD_UPDS_FILE}.
#	In the event that an upgrade is interrupted, this information helps to
#	provide status to scinstall when it is re-started by the user.
#
#	Whenever the current data service name is either cleared or set to a
#	new service name, the ${SC_UPGD_DSPKGS_FILE} is first also removed,
#	if it exists.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
upgd_set_current_dataservice()
{
        typeset -r service=$1

	typeset gotservice

	if [[ ! -f ${SC_UPGD_UPDS_FILE} ]];then
               	printf "\n%s:  $(gettext 'Cannot locate file %s')\n" ${PROG} ${SC_UPGD_UPDS_FILE} | logerr
		return 1
	fi

	if [[ -n ${service} ]];then
		gotservice=$(cat ${SC_UPGD_UPDS_FILE})
		if [[ -n ${gotservice} ]] &&
		   [[ "${gotservice}" != "${service}" ]];then
               		printf "\n%s:  $(gettext 'Cannot upgrade data service "%s"')\n" ${PROG} ${service} | logerr
               		printf "%s:  $(gettext 'Upgrade of dataservice "%s" is in progress')\n" ${PROG} ${gotservice} | logerr
			return 1
		fi
		echo ${service} > ${SC_UPGD_UPDS_FILE} || return 1
	else
		cat /dev/null > ${SC_UPGD_UPDS_FILE} || return 1
	fi

	return 0
}

#####################################################
#
# upgd_validate cdimagebasedir
#
#       cdimagebasedir          location of .cdtoc file
#
#	This function verify that the existing cluster may be legally upgraded
#	to the version of Sun Cluster on the CD.
#
#	Validate hardware support for the upgrade.
#
#	This function currently does nothing as there are no Hardware
#	restrictions for the upgrade to take place.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
upgd_validate()
{
        typeset -r cdimagebasedir=$1

	return 0
}


#####################################################
#
# upgd_save_config
#
#       cdimagebasedir          location of .cdtoc file
#
#	This function creates several files under the Upgrade directory
#	${SC_UPGD_DIR}(/var/cluster/upgrade). Finally, it calls
#	upgd_set_upgradeflag() to set the "framework" upgrade flag file.
#
#	The purpose of this function is to preserve important framework
#	configuration data throughout the course of a framework upgrade.
#	It also saves copies of the dot.cdtoc, dot.order, and dot.clustertoc
#	files from the installed cluster.
#
#	And, it creates the ${SC_UPGD_FMPKGS_FILE} and ${SC_UPGD_FMCLS_FILE}
#	files. These two files list framework packages and software clusters,
#	respectively, installed on the node at the time that a framework upgrade
#	is first begun. In the event that an upgrade is interrupted and must be
#	restarted, these two files preserve the original list of framework
#	packages and software clusters to upgrade.
#
#	Lastly upgd_set_upgradeflag() is called to create the "framework"
#	upgrade flag file.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
upgd_save_config()
{
	# check arg
        if [[ $# != 1 ]]  then
                printf "%s:  $(gettext 'Internal error - bad call to upgd_save_config()')\n" ${PROG} >&2
                return 2
        fi

        typeset -r cdimagebasedir=$1

        typeset realcdimage
        typeset productdir
        typeset pkglist

	typeset	gottype
	typeset	clstr
	typeset pkg
	integer i
	integer j
	integer k
	integer found
	integer usebuiltin=1
        typeset dot_order		# Array of Packages in the dot.order file
	typeset dot_cltoc		# Array of Clusters in the dot.clustertoc
	typeset dot_cltoc_pkgs		# Array of Packages for each dot_cltoc
	typeset installed_package	# Array of installed packages
	typeset installed_cluster	# Array of installed clusters
	typeset pkg_in_cluster		# Array of Cluster for installed_package
	typeset embedded_clusters	# List of Clusters embedded in others

	# locate directory containing the product directory
	realcdimage=$(find_cdimagebasedir "${cdimagebasedir}" "${SC_PRODUCT}" "${SC_CLUSTER}") || return 1

	# get the name of the product directory
	productdir=$(getproduct ${realcdimage}/${SC_CDTOC} "${SC_PRODUCT}" ${SC_CLUSTER} "dir") || return 1

	gottype=$(upgd_get_upgradeflag)
	if [[ $? -eq ${SC_TRUE} ]];then
		if [[ "${gottype}" == "framework" ]];then
			printf "\n\n$(gettext 'Resuming the upgrade using saved configurations')\n" | logmsg
			return 0
		else
			return 1
		fi
	fi

	printf "\n\n$(gettext 'Saving current Sun Cluster configuration')\n" | logmsg

	rm -f ${SC_UPGD_DIR}/dot.cdtoc
	rm -f ${SC_UPGD_DIR}/dot.order
	rm -f ${SC_UPGD_DIR}/dot.clustertoc
	rm -f ${SC_UPGD_FMPKGS_FILE}
	rm -f ${SC_UPGD_FMCLS_FILE}

	cp ${SC_BASEDIR}/usr/cluster/lib/scadmin/dot.cdtoc ${SC_UPGD_DIR} || return 1
	cp ${SC_BASEDIR}/usr/cluster/lib/scadmin/dot.order ${SC_UPGD_DIR} || return 1
	cp ${SC_BASEDIR}/usr/cluster/lib/scadmin/dot.clustertoc ${SC_UPGD_DIR} || return 1

	#
	# The procedure below creates these two files
	#
        # SC_UPGD_FMPKGS_FILE : List of installed packages in correct order.
	# SC_UPGD_FMCLS_FILE  : List of Software-Clusters to install for
	#			Upgrade
	#
	# using the information in files  /usr/cluster/lib/scadmin/dot.order
	# and /usr/cluster/lib/scadmin/dot.clustertoc augmented using a
	# builtin listing of packages.
	#
	#

	touch ${SC_UPGD_FMPKGS_FILE} || return 1
	touch ${SC_UPGD_FMCLS_FILE} || return 1

        set -A dot_order $(cat ${SC_UPGD_DIR}/dot.order)

	# Compiling List of CLUSTERs from  dot.clustertoc file
	# METACLUSTERs are not being included in this array.

	set -A dot_cltoc $(nawk -F= '/^CLUSTER=/{print $2}' ${SC_UPGD_DIR}/dot.clustertoc)
	set -A dot_cltoc_pkgs
	set -A installed_package
	set -A installed_cluster
	set -A pkg_in_cluster

	let i=0
	while [[ ${i} -lt ${#dot_cltoc[*]} ]]
	do
		dot_cltoc_pkgs[i]=$(print_clustertoc ${SC_UPGD_DIR}/dot.clustertoc ${dot_cltoc[i]} "packages" )
		embedded_clusters="${embedded_clusters} $(print_clustertoc ${SC_UPGD_DIR}/dot.clustertoc ${dot_cltoc[i]} "clusters")"
        	(( i += 1 ))
	done

	let i=0
        while [[ ${i} -lt ${#dot_cltoc[*]} ]]
        do
		for clstr in ${embedded_clusters}
		do
			if [[ "${dot_cltoc[i]}" == "${clstr}" ]];then
				dot_cltoc[i]=
				dot_cltoc_pkgs[i]=
				break
			fi
		done
        (( i += 1 ))
	done

        #
        #Check if all Packages in the Dot Order file are present
        #in the builtin order list. If any package is newer we
        #use the order specified in dot_order instead of builtin
        #
        for pkg in ${dot_order[*]}
        do
		let found=0
		let i=0
		while [[ $i -lt ${#SC_UPGD_PKG_MAP[*]} ]]
		do
			if [[ "${pkg}" == "${SC_UPGD_PKG_MAP[i]}" ]];then
				let found=1
				break
			fi
			(( i += 2 ))
		done
                if [ $found -eq 0 ];then
                        let usebuiltin=0
                fi
        done

        #
        #If the Dot Order list is greater or equivalent to
        #the builtin order list. we use the order specified in
        #dot_order instead of builtin
        #
        if [[ ${usebuiltin} -eq 1 ]] &&
           [[ $(expr ${#SC_UPGD_PKG_MAP[*]} / 2) -le ${#dot_order[*]} ]];then
                let usebuiltin=0
        fi
        if [[ ${usebuiltin} -eq 0 ]];then
		pkglist=${dot_order[*]}
	else
		pkglist=
		let i=0
		while [[ $i -lt ${#SC_UPGD_PKG_MAP[*]} ]]
		do
			pkglist="${pkglist} ${SC_UPGD_PKG_MAP[i]}"
                	(( i += 2 ))
		done
	fi

	let j=0
	for pkg in ${pkglist}
	do
        	pkginfo -q ${pkg}
        	if [[ $? -eq 0 ]]; then
                	installed_package[j]=${pkg};
                	(( j += 1 ))
        	fi
	done

	let i=0
	while [[ $i -lt ${#installed_package[*]} ]]
	do
        	let j=0
        	let found=0
        	while [[ $j -lt ${#dot_cltoc[*]} ]]
                do
			for pkg in ${dot_cltoc_pkgs[j]}
			do
                       		if [[ "${pkg}" == "${installed_package[i]}" ]];then
                               		pkg_in_cluster[i]=${dot_cltoc[j]};
					let found=1
                               		break 2
                        	fi
			done
                        (( j += 1 ))
                done
        	if [[ $found -eq 0 ]];then
                	let j=0
                	while [[ $j -lt ${#SC_UPGD_PKG_MAP[*]} ]]
                	do
                        	if [[ "${SC_UPGD_PKG_MAP[j]}" == "${installed_package[i]}" ]];then
                                	let found=1
                                	pkg_in_cluster[i]=${SC_UPGD_PKG_MAP[j+1]};
                                	break
                        	fi
                        	(( j += 2 ))
                	done
        	fi
        	if [[ $found -eq 0 ]];then
                	printf "%s:  $(gettext 'Unidentified package %s')\n" ${PROG} ${installed_package[i]} | logerr
                	return 1
        	fi
        	(( i += 1 ))
	done

	let j=0
	installed_cluster[j]="SUNWCsc"
	let i=0
	while [[ ${i} -lt  ${#pkg_in_cluster[*]} ]]
	do
		let found=0
		for clstr in ${installed_cluster[*]}
		do
        		if [[  "${clstr}" == "${pkg_in_cluster[i]}" ]];then
				let found=1
				break
			fi
		done
        	if [[ ${found} -eq 0 ]];then
			(( j += 1 ))
                	installed_cluster[j]=${pkg_in_cluster[i]}
        	fi
        	(( i += 1 ))
	done

	let k=0
	while [[ $k -lt ${#SC_RETIRED_PKG_MAP[*]} ]]
	do
        	pkginfo -q ${SC_RETIRED_PKG_MAP[k]}
        	if [[ $? -eq 0 ]]; then
			let found=0
			for pkg in ${installed_package[*]}
			do
        			if [[  "${pkg}" == "${SC_RETIRED_PKG_MAP[k]}" ]];then
					let found=1
					break
				fi
			done
        		if [[ ${found} -eq 0 ]];then
				installed_package[i]=${SC_RETIRED_PKG_MAP[k]}
        			(( i += 1 ))
        		fi

			let found=0
			for clstr in ${installed_cluster[*]}
			do
        			if [[  "${clstr}" == "${SC_RETIRED_PKG_MAP[k+1]}" ]];then
					let found=1
					break
				fi
			done
        		if [[ ${found} -eq 0 ]];then
				(( j += 1 ))
                		installed_cluster[j]=${SC_RETIRED_PKG_MAP[k+1]}
        		fi
        	fi
        	(( k += 2 ))
	done

	let i=0
	while [[ ${i} -lt ${#installed_cluster[*]} ]]
	do
		# get the list of packages
		print_clustertoc ${productdir}/${SC_CLUSTERTOC} ${installed_cluster[i]} "description" > /dev/null
		if [[ $? -eq 0 ]];then
			echo ${installed_cluster[i]} >> ${SC_UPGD_FMCLS_FILE}
		else
			missing_clusters="${missing_clusters} ${installed_cluster[i]}"
		fi
		(( i += 1 ))
	done


	let i=0
	while [[ ${i} -lt ${#installed_package[*]} ]]
	do
		let found=0
		for clstr in ${missing_clusters}
		do
			if [[ "${pkg_in_cluster[i]}" == "${clstr}" ]];then
				let found=1
				break
			fi
		done
		if [[ ${found} -eq 0 ]];then
			echo ${installed_package[i]} >> ${SC_UPGD_FMPKGS_FILE}
		else
        		printf "\n%s:  $(gettext 'Cannot upgrade package "%s" using current CDROM.')\n" ${PROG} ${installed_package[i]} | logerr
		fi
		(( i += 1 ))
	done

        upgd_set_upgradeflag ${SC_TRUE} "framework"

	return $?
}

#####################################################
#
# upgd_save_ccr
#
#	This function saves the cluster ccr by renaming :
#	/etc/cluster/ccr to etc/cluster/ccr.upgrade
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
upgd_save_ccr()
{
	if [[ ! -d "${SC_BASEDIR}/etc/cluster/ccr.upgrade" ]];then
		if [[ -d "${SC_BASEDIR}/etc/cluster/ccr" ]];then
			mv ${SC_BASEDIR}/etc/cluster/ccr ${SC_BASEDIR}/etc/cluster/ccr.upgrade || return 1
			printf "\n$(gettext 'Renamed "%s" to "%s".')\n" "${SC_BASEDIR}/etc/cluster/ccr" "${SC_BASEDIR}/etc/cluster/ccr.upgrade" | logmsg
		else
			printf "\n%s:  $(gettext 'Missing Cluster Configuration : "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/cluster/ccr" | logerr
			return 1
		fi
	fi

	return 0
}

#####################################################
#
# upgd_restore_config
#
#	This function restores the cluster ccr by renaming :
#	/etc/cluster/ccr.upgrade to etc/cluster/ccr
#
#	And, it calls upgd_set_upgradeflag() to clear the "framework" upgrade
#	flag file.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
upgd_restore_config()
{
	# check arg
        if [[ $# != 0 ]]  then
                printf "%s:  $(gettext 'Internal error - bad call to upgd_restore_config()')\n" ${PROG} >&2
                return 2
        fi

	if [[ -d "${SC_BASEDIR}/etc/cluster/ccr.upgrade" ]];then
		if [[ -d "${SC_BASEDIR}/etc/cluster/ccr" ]];then
			#
			# Before getting rid of the CCR, save the 
			# cluster_directory table so that we avoid
			# regenerating it.
			# This table is removed once the CCR transformation
			# is complete.
			#
			mv ${SC_BASEDIR}/etc/cluster/ccr/cluster_directory ${SC_BASEDIR}/ || return 1
			rm -fr ${SC_BASEDIR}/etc/cluster/ccr
			#
			# rm may fail to remove the directory if the current
			# working directory is on a NFS mounted file system.
			# Please see CR 6446368.
			#
			result=$?
			if [[ $result != 0 ]]; then
				mv ${SC_BASEDIR}/etc/cluster/ccr ${SC_BASEDIR}/etc/cluster/ccr.obsolete || return 1
			fi
		fi
		mv ${SC_BASEDIR}/etc/cluster/ccr.upgrade ${SC_BASEDIR}/etc/cluster/ccr || return 1
		#
		# Walk through all the files and directories in
		# /etc/cluster/ccr. If it is a directory, change permission 
		# to 0755 else change it to 0600.
		#
		for zc in `ls ${SC_BASEDIR}/etc/cluster/ccr`
		do
			if [[ -d ${BASEDIR}/etc/cluster/ccr/$zc ]]; then
				chmod 0755 ${SC_BASEDIR}/etc/cluster/ccr/$zc || return 1
				for table in `ls ${SC_BASEDIR}/etc/cluster/ccr/$zc`
				do
					chmod 0600 ${SC_BASEDIR}/etc/cluster/ccr/$zc/$table || return 1
				done
			else
				chmod 0600 ${SC_BASEDIR}/etc/cluster/ccr/$zc || return 1
			fi
		done
		printf "\n$(gettext 'Restored  %s to %s')\n" "${SC_BASEDIR}/etc/cluster/ccr.upgrade" "${SC_BASEDIR}/etc/cluster/ccr" | logmsg
	else
		printf "\n%s:  $(gettext 'Error - cannot restore "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/cluster/ccr" | logerr
		return 1
	fi

        upgd_set_upgradeflag ${SC_FALSE} "framework"

	return $?
}

#####################################################
#
# upgd_nafo_to_ipmp
#
#	The purpose of this function is to use the NAFO data found in the
#	/etc/cluster/pnmconfig file to create new IPMP groups.  The new IPMP
#	group names will be the same as the old NAFO group names.  Since each
#	adapter in an IPMP group requires one test address, users must supply
#	test addresses for each of the adapters in each of the new IPMP groups.
#	Users specify the addresses with "-S testaddr" command line options or
#	interactively by using "-S interact".
#
#	This function may modify one or more /etc/hostname.<if> files.  It will
#	also rename :-
#
#	/etc/cluster/pnmconfig to /etc/cluster/pnmconfig.obsolete.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
upgd_nafo_to_ipmp()
{
	# check arg
        if [[ $# != 1 ]]  then
                printf "%s:  $(gettext 'Internal error - bad call to upgd_nafo_to_ipmp()')\n" ${PROG} >&2
                return 2
        fi
        typeset -r runtype=$1

	if [[ ! -f ${SC_BASEDIR}/etc/cluster/pnmconfig ]]; then
		if [[ ${SC_UPGD_TST_ASKME} -eq ${SC_TRUE} ]] ||
		   [[ -n ${SC_UPGD_TST_IF} ]];then
			if [[ "${runtype}" == "check" ]];then
				printf "$(gettext '"%s" file does not exist ...')" "${SC_BASEDIR}/etc/cluster/pnmconfig" | logmsg
				printf "$(gettext '...option -S ignored')\n" ${PROG} | logmsg
			fi
		fi
		return 0
	fi
	typeset ip_script=${SC_SCLIBDIR}/${SC_IP_PERL}
	typeset testaddr_reqd		# Check to see if testaddress is required
	typeset adapter_count_per_group
	typeset nafo			# Array of NAFO GROUPS in pnmconfig
	typeset nafoip			# Array of IPs for each Nafo group
	typeset nafonetmask		# Array of netmasks for nafo group
	typeset adapter			# Adapters in pnmconfig
	typeset ipmpgroup		# Array of IPMP group for Adapter
	typeset pnmgroup		# Array of NAFO/PNM group for Adapter
	typeset adapter_config 		# Array of adapter_config for Adapter
	typeset testaddr 		# Array of Test-address for Adapter
	typeset address 		# Array of Base-IP-Address for Adapter
	set -A pnmgroup
	set -A nafo
	set -A nafoip
	set -A nafonetmask
	set -A ipmpgroup
	set -A adapter
	set -A adapter_config
	set -A testaddr
	set -A address
	set -A testaddr_reqd
	set -A adapter_count_per_group
	integer i
	integer j
	integer k
	integer found
	typeset entry
	typeset entry_1
	typeset entry_2
	typeset	read_flag
	typeset	okay
	typeset	-l lnetmask
	integer	testaddr_flag
	integer	deprecated_flag
	integer	count_testaddr
	integer	wordcount
	integer	read_addr
	integer	incomplete
	integer	config_errors
	integer auto_create
	typeset sctxt_p1="$(gettext '
		The upgrade of Sun Cluster software on this node
		requires conversion of NAFO adapter groups to IP Network
		Multipathing (IPMP) groups. IPMP uses test addresses to detect
		network adapter failures. As part of this process, you will
		be asked to provide a test address for each adapter which does
		not already have a test address assigned. The test address that
		you assign to an adapter must be a unique unused IP address in
		the same subnet as the group that adapter belongs to.
	')"


	if [[ "${runtype}" == "check" ]];then
		printf "\n\n$(gettext 'Checking IP Multipathing configuration in "%s" files')\n" "${SC_BASEDIR}/etc/hostname.<adapter>" | logmsg
	fi

	let j=0
	let i=0
	let k=0
	auto_create=$(${ip_script} check_if_autocreate_supported)
	while read nafo[j] adapter_list
	do
		let k=0
		for entry in $adapter_list
		do
			adapter[i]=${entry}
			adapter_j[i]=${j}
			pnmgroup[i]=${nafo[j]}
			nafoip[j]=${nafoip[j]:-$(${ip_script} get_adapter_ip ${adapter[i]})}
			nafonetmask[j]=${nafonetmask[j]:-$(${ip_script} get_adapter_netmask ${adapter[i]})}
			(( i += 1 ))
			(( k += 1 ))
		done
		adapter_count_per_group[j]=${k}
		(( j += 1 ))
	done < ${SC_BASEDIR}/etc/cluster/pnmconfig


	let i=0		
	while [[ ${i} -lt ${#adapter[*]} ]]
	do
		j=${adapter_j[i]}
		if [[ "${auto_create}" -eq 1 ]] &&
			[[ "${adapter_count_per_group[j]}" -eq 1 ]]; then
			testaddr_reqd[i]=0;
		else
			testaddr_reqd[i]=1;
		fi
		(( i += 1 ))
	done


	# Parse /etc/hostname.<adapter> files
	let i=0
	while [[ ${i} -lt ${#adapter[*]} ]]
	do
		let testaddr_flag=0
		let deprecated_flag=0
		let count_testaddr=0
		let wordcount=0
		read_flag="addr"
		let read_addr=0
		adapterconfig=$(nawk -F# '{print $1}' ${SC_BASEDIR}/etc/hostname.${adapter[i]} 2> /dev/null)
		for word in ${adapterconfig}
		do
			(( wordcount += 1 ))
			case ${word} in
			group)
				read_flag="group"
				;;
			addif)
                		if [[ $testaddr_flag -eq 1 ]] ;then
                               		((count_testaddr += 1))
                       			if [[ $deprecated_flag -eq 1 ]] ; then
                               			testaddr[i]=${address[i]}
                               			testaddr_ip[i]=$(${ip_script} get_ip_in_hosts ${testaddr[i]})
                       			else
		
                               			adapter_config[i]="wrong"
                               			printf "\n%s:  $(gettext 'Configuration error in "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/hostname.${adapter[i]}" | logerr
                               			printf "%s:  $(gettext 'test address (-failover) must be deprecated.')\n" ${PROG} | logerr
                       			fi
                		fi
				let testaddr_flag=0
				let deprecated_flag=0
				read_flag="addr"
				;;
			-failover)
				let testaddr_flag=1
				;;
			deprecated)
				let deprecated_flag=1
				;;
			netmask|broadcast|destination|index|metric|mtu)
				read_flag="skip"
				;;
			\\|-*|up|down)
				;;
			*)
				case ${read_flag} in
				group)
					ipmpgroup[i]=${word}
					read_flag=
					;;
				addr)
					address[i]=${word}
					address_ip=$(${ip_script} get_ip_in_hosts ${testaddr[i]})
					if [[ -z $address_ip ]];then
                               			printf "\n%s:  $(gettext 'Configuration error in "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/hostname.${adapter[i]}" | logerr
						printf "%s:  $(gettext 'Hostname "%s" is not in "%s".')\n" ${PROG} ${word} "${SC_BASEDIR}/etc/hosts" | logerr
						adapter_config[i]="wrong"
					fi

					read_flag=
					let read_addr=1
					;;
				skip)
					read_flag=
					;;
				*)
					;;
				esac
				if [[ ${read_addr} -eq 0 ]];then
					read_flag="addr"
				fi
				;;
			esac
		done
		if [[ $testaddr_flag -eq 1 ]] ;then
			((count_testaddr += 1))
			if [[ $deprecated_flag -eq 1 ]] ; then
				testaddr[i]=${address[i]}
				testaddr_ip[i]=$(${ip_script} get_ip_in_hosts ${testaddr[i]})
			else
				# don't fail here because incase of a singleton IPMP
				# group, the data address could be used as a test address
				# in which case it would not be deprecated
                        	if [[ "${testaddr_reqd[i]}" -eq 1 ]]; then 
					adapter_config[i]="wrong"
                               		printf "\n%s:  $(gettext 'Configuration error in "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/hostname.${adapter[i]}" | logerr
                                	printf "%s:  $(gettext 'testaddress (-failover) must be deprecated.')\n" ${PROG} | logerr
				fi
			fi
		fi

		case ${wordcount}  in
		0)
			adapter_config[i]="missing";
			# for a singleton NAFO group which does not have 
			# an /etc/hostname.<adaptername> we need to prompt 
			# the user for a test address 
			testaddr_reqd[i]=1;
			;;
		1)
			adapter_config[i]="needed";
			;;
		*)
			case ${count_testaddr} in
			0)
				adapter_config[i]="wrong"
				printf "\n%s:  $(gettext 'File "%s" contains pre-configured options.')\n" ${PROG} "${SC_BASEDIR}/etc/hostname.${adapter[i]}" | logerr
				printf "%s:  $(gettext 'Please add IPMP test address configuration to "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/hostname.${adapter[i]}" | logerr
				;;
			1)
				if [[ "${ipmpgroup[i]}" != "${pnmgroup[i]}" ]];then
                               		printf "\n%s:  $(gettext 'Configuration error in "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/hostname.${adapter[i]}" | logerr
                                	printf "%s:  $(gettext 'Groupname must be set to %s.')\n" ${PROG} ${pnmgroup[i]} | logerr
					adapter_config[i]="wrong"
				else
					if [[ "${adapter_config[i]}" != "wrong" ]];then
						adapter_config[i]="complete"
					fi
				fi
				;;
			*)
				adapter_config[i]="wrong"
                               	printf "\n%s:  $(gettext 'Configuration error in "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/hostname.${adapter[i]}" | logerr
                                printf "%s:  $(gettext 'Only one "-failover" address can be configured on an adapter.')\n" ${PROG} | logerr
				;;
			esac
			;;
		esac
		if [[ -n "${readflag}" ]] then
				adapter_config[i]="wrong"
		fi

	(( i += 1 ))
	done


	if [[ "${runtype}" == "check" ]] &&
		[[ -n "${SC_UPGD_TST_IF}" ]] ;then
		printf "\n\n$(gettext 'Checking "-S" option IP Multipathing test addresses')\n" ${PROG} | logmsg
	fi

	for entry in ${SC_UPGD_TST_IF}
	do
		let found=0
		let i=0
		while [[ ${i} -lt ${#adapter[*]} ]]
		do
			entry_1=$(IFS=@ ;set -- ${entry};echo $1)
			entry_2=$(IFS=@ ;set -- ${entry};echo $2)
			# if user has specified a test address for an
			# adapter in singleton group do not use it
			if [[ "${entry_2}" == "${adapter[i]}" ]]; then
				if [[ "${testaddr_reqd[i]}" -eq 1 ]]; then 
					if  [[ "${adapter_config[i]}" == "complete" ]]||
		    	    		[[ "${adapter_config[i]}" == "wrong" ]] ;then
						if [[ "${runtype}" == "check" ]];then
                               				printf "\n%s:  $(gettext 'Adapter already configured in %s/etc/hostname.%s')\n" ${PROG} "${SC_BASEDIR}" ${adapter[i]} | logmsg
                               				printf "%s:  $(gettext 'Command-line test address will be ignored for adapter %s.')\n\n" ${PROG} ${adapter[i]}| logmsg
						fi
					else
						testaddr[i]=${entry_1}
						testaddr_ip[i]=$(${ip_script} get_ip_in_hosts ${testaddr[i]})
						ipmpgroup[i]=${pnmgroup[i]}
					fi
				fi
				let found=1
				break
			fi
			(( i += 1 ))
		done
		if [[ ${found} -eq 0 ]];then
                       	printf "\n%s:  $(gettext 'Invalid Adapter in -S option: %s')\n" ${PROG} ${entry_2} | logerr
                       	printf "%s:  $(gettext 'Adapter %s is not in any nafo group')\n" ${PROG} ${entry_2} | logerr
			return 1
		fi
	done

	let i=0
	let config_errors=0
	let incomplete=0
	while [[ ${i} -lt ${#adapter[*]} ]]
	do
		case ${adapter_config[i]} in

		"wrong")
			(( config_errors += 1 ))
			;;
		"missing"|"needed"|"complete")
			if [[ "${adapter_config[i]}" != "complete" ]];then
				let incomplete=1
			fi
                        if [[ "${testaddr_reqd[i]}" -eq 1 ]]; then
				if [[ -z "${testaddr[i]}" ]] ;then
	   				if [[ ${SC_UPGD_TST_ASKME} -eq ${SC_FALSE} ]] ;then
						printf "\n%s:  $(gettext 'Configuration incomplete for adapter "%s".')\n" ${PROG} ${adapter[i]}| logmsg
						printf "%s:  $(gettext 'Use "-S interact" option to configure all test addresses.')\n" ${PROG} | logmsg
						(( config_errors += 1 ))
					fi
					(( i += 1 ))
					continue
				fi
				j=${adapter_j[i]}
			
				gotaddr="${testaddr[i]}"
				gotaddr_ip=$(${ip_script} get_ip_in_hosts ${gotaddr})

				if [[ -z "${gotaddr_ip}" ]];then
					printf "\n%s:  $(gettext 'Invalid test address for adapter(%s) "%s".')\n" ${PROG} ${adapter[i]} ${testaddr[i]} | logerr
					printf "%s:  $(gettext 'Cannot resolve address "%s" using "%s" file.')\n" ${PROG} "${gotaddr}" "${SC_BASEDIR}/etc/hosts" | logerr
					gotaddr=
				fi

				if [[ "${gotaddr_ip}" == "${nafoip[j]}" ]];then
					printf "\n%s:  $(gettext 'Invalid test address for adapter(%s) "%s".')\n" ${PROG} ${adapter[i]} ${testaddr[i]} | logerr
					printf "%s:  $(gettext 'Test address must be different than adapter address.')\n" ${PROG} | logerr
					gotaddr=
				fi

				if [[ -n "${gotaddr}" ]];then
					let k=0
					while [[ ${k} -lt ${#adapter[*]} ]]
					do
						if [[ "${gotaddr_ip}" == "${testaddr_ip[k]}" ]] &&\
					   	[[ ${i} -ne ${k} ]];then
							printf "\n%s:  $(gettext 'Invalid test address for adapter(%s) "%s".')\n" ${PROG} ${adapter[i]} ${testaddr[i]} | logerr
							printf "%s:  $(gettext 'Test address is already in use on adapter %s.')\n" ${PROG} ${adapter[k]} | logerr
							gotaddr=
							break
						fi
						(( k += 1 ))
					done
				fi

				if [[ -n "${gotaddr}" ]];then
					let k=0
					while [[ ${k} -lt ${#nafoip[*]} ]]
					do
						if [[ "${gotaddr_ip}" == "${nafoip[k]}" ]];then
							printf "\n%s:  $(gettext 'Invalid test address for adapter(%s) "%s".')\n" ${PROG} ${adapter[i]} ${testaddr[i]} | logerr
							printf "%s:  $(gettext 'Test address is already in use on group %s.')\n" ${PROG} ${nafo[k]} | logerr
							gotaddr=
							break
						fi
						(( k += 1 ))
					done
				fi
				if [[ -n "${gotaddr}" ]];then
					gotaddrsubnet=$(${ip_script} calculate_subnet ${gotaddr_ip} ${nafonetmask[j]})
					nafosubnet=$(${ip_script} calculate_subnet ${nafoip[j]} ${nafonetmask[j]})
					if [[ ${gotaddrsubnet} -ne ${nafosubnet} ]];then
						printf "\n%s:  $(gettext 'Invalid test address for adapter(%s) "%s".')\n" ${PROG} ${adapter[i]} ${testaddr[i]} | logerr
						printf "%s:  $(gettext 'Test address for adapter %s must be in the same subnet as:')\n" ${PROG} ${adapter[i]} | logerr
						lnetmask=${nafonetmask[j]}
						printf "%s:  $(gettext '	Group:%s IP:%s Netmask:%s')\n" ${PROG} ${nafo[j]} ${nafoip[j]} ${lnetmask} | logerr
						gotaddr=
					fi
				fi
				if [[ -z "${gotaddr}" ]];then
					(( config_errors += 1 ))
				fi
			fi
			;;
		*)
			printf "\n%s:  $(gettext 'Internal Error : Invalid adapter_config for %s.')\n" ${PROG} ${adapter[i]} ${testaddr[i]} >&2
			return 2
			;;
		esac
	(( i += 1 ))
	done

	if [[ ${config_errors} -gt 0 ]];then
		printf "\n%s:  $(gettext 'Check above IP Multipathing requirements/errors before upgrade.')\n" ${PROG} | logerr
		return 1
	fi

	if [[ ${incomplete} -eq 0 ]];then
		mv ${SC_BASEDIR}/etc/cluster/pnmconfig ${SC_BASEDIR}/etc/cluster/pnmconfig.obsolete || return 1
		return 0
	fi

	while [[ ${SC_UPGD_TST_ASKME} -eq ${SC_TRUE} ]]
	do

		sc_print_title "$(gettext 'Conversion of NAFO to IP Network Multipathing groups.')"

		sc_print_para "${sctxt_p1}"

		printf "    $(gettext 'Here is a list of public network adapters on this node:')\n\n"

		printf "%10.10s %-8s %-8s %-18s %-14s %-16s\n" "" "$(gettext 'Adapter')" "$(gettext 'Group')" "$(gettext 'Failover Address')" "$(gettext 'Netmask')" "$(gettext 'Test Address')"
		printf "%10.10s %-8s %-8s %-18s %-14s %-16s\n\n" "" "=======" "=====" "================" "=======" "============"
		let i=0
		while [[ ${i} -lt ${#adapter[*]} ]]
		do
			j=${adapter_j[i]}
			lnetmask=${nafonetmask[j]}
			if [[ -z "${testaddr[i]}" ]];then
				if [[ "${testaddr_reqd[i]}" -eq 1 ]]; then
					printf "%10.10s %-8s %-8s %-18s %-14s %-16s\n" "" "${adapter[i]}" "${pnmgroup[i]}" "${nafoip[j]}" "${lnetmask}" "<none>"
				else 
					printf "%10.10s %-8s %-8s %-18s %-14s %-16s\n" "" "${adapter[i]}" "${pnmgroup[i]}" "${nafoip[j]}" "${lnetmask}" "${address[i]}"
				fi
			else
				printf "%10.10s %-8s %-8s %-18s %-14s %-16s\n" "" "${adapter[i]}" "${pnmgroup[i]}" "${nafoip[j]}" "${lnetmask}" "${testaddr[i]}"
			fi
			(( i += 1 ))
		done

		let i=0
		while [[ ${i} -lt ${#adapter[*]} ]]
		do
			if [[ -n "${testaddr[i]}" ]] || 
				[[ "${testaddr_reqd[i]}" -eq 0 ]];then
				(( i += 1 ))
				continue
			fi

			ipmpgroup[i]=${pnmgroup[i]}
			gotaddr=
			while [[ -z "${gotaddr}" ]]
			do
				printf "\n"
				gotaddr=$(sc_prompt "$(gettext 'What test address do you want to assign to') \"${adapter[i]}\"?" "") || return 1
				gotaddr_ip=$(${ip_script} get_ip_in_hosts ${gotaddr})

				if [[ -z "${gotaddr_ip}" ]];then
					printf "$(gettext 'Invalid test address : cannot resolve "%s" using "%s" file.')\n" "${gotaddr}" "${SC_BASEDIR}/etc/hosts"
					gotaddr=
					continue
				fi

				j=${adapter_j[i]}

				if [[ "${gotaddr}" == "${nafoip[j]}" ]];then
					printf "$(gettext 'Invalid test address : must be different than adapter address.')\n"
					gotaddr=
					continue
				fi

				let k=0
				while [[ ${k} -lt ${#adapter[*]} ]]
				do
					if [[ "${gotaddr_ip}" == "${testaddr_ip[k]}" ]];then
						printf "$(gettext 'Test address "%s" is already in use on adapter %s.')\n" "${gotaddr}" "${adapter[k]}"
						gotaddr=
						continue 2
					fi
					(( k += 1 ))
				done

				let k=0
				while [[ ${k} -lt ${#nafoip[*]} ]]
				do
					if [[ "${gotaddr_ip}" == "${nafoip[k]}" ]];then
						printf "$(gettext 'Test Address "%s" is already in use on group %s.')\n" "${gotaddr}" "${nafo[k]}"
						gotaddr=
						continue 2
					fi
					(( k += 1 ))
				done

				gotaddrsubnet=$(${ip_script} calculate_subnet ${gotaddr_ip} ${nafonetmask[j]})
				nafosubnet=$(${ip_script} calculate_subnet ${nafoip[j]} ${nafonetmask[j]})
				if [[ ${gotaddrsubnet} -ne ${nafosubnet} ]];then
					printf "$(gettext 'Invalid test address : must provide address in the same subnet as the group.')\n"
					lnetmask=${nafonetmask[j]}
					printf "$(gettext 'Adapter %s belongs to Group %s (Address %s Netmask %s)')\n" "${adapter[i]}" "${nafo[j]}" "${nafoip[j]}" "${lnetmask}"
					gotaddr=
					continue
				fi
			done
			testaddr[i]=${gotaddr}
			testaddr_ip[i]=${gotaddr_ip}
			(( i += 1 ))
		done

		printf "    $(gettext 'This is the list of test addresses you entered:')\n\n"
		printf "%10.10s %-8s %-8s %-18s\n" "" "$(gettext 'Adapter')" "$(gettext 'Group')" "$(gettext 'Test Address')"
		printf "%10.10s %-8s %-8s %-18s\n\n" "" "=======" "=====" "============"
		let i=0
		while [[ ${i} -lt ${#adapter[*]} ]]
		do
			if [[ ${adapter_config[i]} == "missing" ]] || \
			   [[ ${adapter_config[i]} == "needed" ]];then
				if [[ "${testaddr_reqd[i]}" -eq 1 ]]; then
					printf "%10.10s %-8s %-8s %-18s \n" "" "${adapter[i]}" "${pnmgroup[i]}" "${testaddr[i]}"
				else
					printf "%10.10s %-8s %-8s %-18s \n" "" "${adapter[i]}" "${pnmgroup[i]}" "${address[i]}"
				fi
			fi
			(( i += 1 ))
		done

		# Confirm before continuuing
		echo
		okay=$(sc_prompt_yesno "$(gettext 'Is this list correct?')" "${YES}") || return 1

		# Zero out the test address to re-prompt addresses.
		if [[ ${okay} != "yes" ]];then
			let i=0
			while [[ ${i} -lt ${#adapter[*]} ]]
			do
				if [[ ${adapter_config[i]} == "missing" ]] || \
			   	   [[ ${adapter_config[i]} == "needed" ]];then
					testaddr[i]=
					testaddr_ip[i]=
				fi
				(( i += 1 ))
			done
		else
			let i=0
			while [[ ${i} -lt ${#adapter[*]} ]]
			do
				if [[ ${adapter_config[i]} == "missing" ]] || \
	  		 	   [[ ${adapter_config[i]} == "needed" ]];then
					SC_UPGD_TST_IF="${SC_UPGD_TST_IF} ${testaddr[i]}@${adapter[i]}"
				fi
				(( i += 1 ))
			done
			SC_UPGD_TST_ASKME=${SC_FALSE}
		fi

	done


	if [[ "${runtype}" == "check" ]];then
		return 0
	fi



	let i=0
	while [[ ${i} -lt ${#adapter[*]} ]]
	do
		case ${adapter_config[i]} in
		"missing")
			printf "\n$(gettext 'Creating "%s".')\n" "${SC_BASEDIR}/etc/hostname.${adapter[i]}" | logmsg
			echo "${testaddr[i]} deprecated -failover netmask + broadcast + group ${ipmpgroup[i]} up" > ${SC_BASEDIR}/etc/hostname.${adapter[i]} || return 1
			;;
		"needed")
			printf "\n$(gettext 'Updating "%s".')\n" "${SC_BASEDIR}/etc/hostname.${adapter[i]}" | logmsg
			if [[ "${testaddr_reqd[i]}" -eq 0 ]]; then
				echo "${address[i]} -failover netmask + broadcast + group ${pnmgroup[i]} up" > ${SC_BASEDIR}/etc/hostname.${adapter[i]} || return 1
			else 	
				echo "${address[i]} netmask + broadcast + group ${ipmpgroup[i]} up \\" > ${SC_BASEDIR}/etc/hostname.${adapter[i]} || return 1
				echo "addif ${testaddr[i]} deprecated -failover netmask + broadcast + up" >> ${SC_BASEDIR}/etc/hostname.${adapter[i]} || return 1
			fi
			;;
		*)
			;;
		esac
	(( i += 1 ))
	done
	mv ${SC_BASEDIR}/etc/cluster/pnmconfig ${SC_BASEDIR}/etc/cluster/pnmconfig.obsolete
	return 0
}

#####################################################
#
# upgd_ntp_config
#
#	The purpose of this function is to rename the default SC3.0 ntp.conf
#	files to their new ntp.conf.cluster names.  The rc script for starting
#	the NTP daemon changed in Solaris 9 in such a way that the default
#	ntp.conf file which was shipped with SC3.0[u[1,2]] causes cluster
#	startup to hang . The current fix to this problem is a new NTP rc
#	startup script for Sun Cluster.
#
#	If ntp.conf exists, this new startup script does nothing, allowing the
#	standard startup script to do its job.
#
#	But, if the file does not exist, it will startup NTP using
#	ntp.conf.cluster, if that file exists.
#
#	Users who want to use their own ntp.conf files are normally instructed
#	to add them as /etc/inet/ntp.conf. However, since this function has no
#	way of knowing whether the original ntp.conf file was derived from the
#	Sun Cluster default file or not, this function always renames ntp.conf
#	to ntp.conf.cluster whenever ntp.conf.cluster is not present.  This
#	means that user-written ntp.conf files will also end up being called
#	ntp.conf.cluster after the upgrade.  This will not hurt anything, since
#	the new Sun Cluster rc script is derived directly from the Solaris 8
#	NTP rc script.
#
#	On the other hand, it is far safer to take this action than to risk
#	hanging the node on the next boot attempt.  A message is printed to the
#	user indicating that ntp.conf has been renamed to ntp.conf.cluster
#
#	The documentation instructs users who are using their own ntp.conf files
#	on how to prevent the upgrade process from renaming their NTP config
#	files.
#
#	If /etc/inet/ntp.cluster is copied to /etc/inet/ntp.conf.cluster prior
#	to upgrade, the original ntp.conf file is left untouched.  The
#	documentation also includes a description of the change which was made
#	to the Solaris 9 NTP rc script.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
upgd_ntp_config()
{
        if [[ $# != 0 ]]  then
                printf "%s:  $(gettext 'Internal error - bad call to upgd_ntp_config()')\n" ${PROG} >&2
                return 2
        fi

        if [[ ! -f "${SC_BASEDIR}/etc/inet/ntp.conf.cluster" ]] &&
           [[ -f "${SC_BASEDIR}/etc/inet/ntp.conf" ]];then
		mv ${SC_BASEDIR}/etc/inet/ntp.conf ${SC_BASEDIR}/etc/inet/ntp.conf.cluster
		if [[ $? -ne 0 ]];then
			printf "\n%s:  $(gettext 'Error moving "%s" to "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/inet/ntp.conf" "${SC_BASEDIR}//etc/inet/ntp.conf.cluster" | logerr
			return 1
		else
			printf "\n%s:  $(gettext 'Renamed "%s" to "%s".')\n" ${PROG} "${SC_BASEDIR}/etc/inet/ntp.conf" "${SC_BASEDIR}/etc/inet/ntp.conf.cluster" | logmsg
		fi
	fi

	return 0
}

#####################################################
#
# SUNWscucm_file_edits() "action"
#
#	action can be either "save" or "restore"
#
#####################################################
SUNWscucm_file_edits()
{
	typeset action=$1
	typeset pkg=SUNWscucm

        if [[ ${action} != "save" && ${action} != "restore" ]];  then
                printf "%s:  $(gettext 'Internal error - bad call to SUNWscucm_file_edits()')\n" ${PROG} >&2
                return 2
        fi

	# Original Tags
	typeset starttag="Start of lines added by ${pkg}" 
	typeset   endtag="End   of lines added by ${pkg}" 

	# New Tags
	typeset newstarttag="Start of lines added by ---${pkg}---"
	typeset   newendtag="End   of lines added by ---${pkg}---" 

	# SUNWscucm edits /etc/inet/inetd.conf and /etc/rpc
	typeset inetd=${BASEDIR}/etc/inet/inetd.conf
	typeset rpc=${BASEDIR}/etc/rpc
	
	if [[ ${action} = "save" ]]; then
		grep "${starttag}" ${inetd} >/dev/null 2>&1
		if [[ $? -eq 0 ]]; then
			# starttag exists, now change it
			replaceString "${starttag}" "${newstarttag}" ${inetd} || return 1
			replaceString "${endtag}" "${newendtag}" ${inetd} || return 1
		fi

		grep "${starttag}" ${rpc} >/dev/null 2>&1
                if [[ $? -eq 0 ]]; then
                        # starttag exists, now change it
			replaceString "${starttag}" "${newstarttag}" ${rpc} || return 1
			replaceString "${endtag}" "${newendtag}" ${rpc} || return 1
                fi
	elif [[ ${action} = "restore" ]]; then
		grep "${newstarttag}" ${inetd} >/dev/null 2>&1
		if [[ $? -eq 0 ]]; then
			# new starttag exists, change it back
			replaceString "${newstarttag}" "${starttag}" ${inetd} || return 1
			replaceString "${newendtag}" "${endtag}" ${inetd} || return 1
		fi

		grep "${newstarttag}" ${rpc} >/dev/null 2>&1
                if [[ $? -eq 0 ]]; then
                        # new starttag exists, change it back
			replaceString "${newstarttag}" "${starttag}" ${rpc} || return 1
			replaceString "${newendtag}" "${endtag}" ${rpc} || return 1
                fi
	fi

	return 0
}

##########################################################
#
# replaceString() current_string new_string filename
#
# 	a generic file pattern replacement operation
#
##########################################################
replaceString()
{
	typeset current=$1
	typeset new=$2
	typeset file=$3

	while :
	do
		grep "${current}" ${file} >/dev/null 2>&1
		case $? in
                2)      # error reading file
			printf "%s:  $(gettext 'Problem reading file %s')\n" ${PROG} ${file} >&2
                        return 1
                        ;;

                0)      # grep found the string, so get rid of the entries
                        ed -s ${file} << EOF >/dev/null 2>&1
1,\$s/${current}/${new}/g
w
q
EOF
                        if [ $? -ne 0 ]; then
        			printf "%s:  $(gettext 'Problem updating file %s')\n" ${PROG} ${file} >&2
                                return 1
                        fi
                        ;;

                *)
                        break
                        ;;
                esac
        done

	return 0
}


#####################################################
#
# save_locale_settings()
#
#	Saves a list of the locales currently installed
#	on the system in the SC_INSTALLED_LOCALES variable
#
#####################################################
save_locale_settings()
{
	# Check if the variable is already set, if so, then
	# only add those values which are in the variable already
	if [[ -n ${SC_INSTALLED_LOCALES} ]]; then
		return 0
	fi

	for pkg in `cat ${SC_DOT_ORDER}`; do
		typeset locale=`pkgparam ${pkg} SUNW_LOC 2>/dev/null`
		if [[ -n ${locale} ]]; then
			# Some pkgparam locale strings actually contain many locales
			# in a comma-separated list.  Split that list and search each one.
			for pkg_locale in `echo ${locale} | awk -F, '{ for (i = 1; i <= NF; i++) print $i }'`; do

				typeset found=false
				# Make sure this locale hasn't already been added
				echo "${SC_INSTALLED_LOCALES}" | grep ${pkg_locale} >/dev/null
				if [[ $? -eq 0 ]]; then
					found=true
				fi

				# Add the locale to the list
				if [[ ${found} = "false" ]]; then
					SC_INSTALLED_LOCALES="${SC_INSTALLED_LOCALES} ${pkg_locale}"
				fi
			done
		fi
	done

	# If the list is still empty, add an entry to install none
	if [[ -z ${SC_INSTALLED_LOCALES} ]]; then
		SC_INSTALLED_LOCALES="none"
	fi

	return 0
}

#########################################################
#
# save_cluster_brand_def()
#
#     Renames /usr/lib/brand/cluster/config.xml to
#     /usr/lib/brand/cluster/config.xml.orig, and renames
#     /usr/lib/brand/cluster/config.xml.upgrade to
#     /usr/lib/brand/cluster/config.xml
#
#########################################################
save_cluster_brand_def()
{
	# If OS version is earlier than s10u4, there's nothing to do.
	# Likewise, if we're upgrading from pre-sc32u2, there's nothing to do
	if [[ ! -d ${SC_BRAND_DIR} ]]; then
		return 0
	fi

	mv ${SC_BRAND_DIR}/config.xml ${SC_BRAND_DIR}/config.xml.orig
	mv ${SC_BRAND_DIR}/config.xml.upgrade ${SC_BRAND_DIR}/config.xml

	return 0
}
 
#########################################################
#
# restore_cluster_brand_def()
#
#     Renames /usr/lib/brand/cluster/config.xml.orig to
#     /usr/lib/brand/cluster/config.xml
#
#########################################################
restore_cluster_brand_def()
{
	# Nothing to do if OS version is earlier than s10u4
	if [[ ! -d ${SC_BRAND_DIR} ]]; then
		return 0
	fi

	mv ${SC_BRAND_DIR}/config.xml.orig ${SC_BRAND_DIR}/config.xml
 
	return 0
}
