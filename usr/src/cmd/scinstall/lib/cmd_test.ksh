#! /usr/xpg4/bin/sh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# pragma ident	"@(#)cmd_test.ksh	1.7	08/12/03 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# cmd_test <command>
#
#	<command> is:
#
#		isfullyinstalled	# /etc/cluster/.installed
#		isinstalling		# is scinstall running
#		isconfigured		# /etc/cluster/nodeid
#		hasbooted		# /etc/cluster/ccr/global/did_instances
#		isclustermember		# active cluster member
#		isnwamenabled		# svc:/network/physical:nwam online
#
#	Possible exit codes:
#
#		0  false
#		1  true
#		2  error
#
#####################################################

#####################################################
#
# Constant Globals
#
#####################################################

# Files
typeset -r SCRCMD_NODEID_FILE=/etc/cluster/nodeid
typeset -r SCRCMD_INSTALLED_FILE=/etc/cluster/.installed
typeset -r SCINSTALL_LOCKFILE=/var/cluster/run/scinstall/scinstall.lock
typeset -r SC_DID_INSTANCES=/etc/cluster/ccr/global/did_instances

# Exit codes
integer -r SC_FALSE=0
integer -r SC_TRUE=1
typeset -r SC_ERROR=2 

# Program name
typeset -r PROG=${0##*/}

# Set the PATH
typeset -r SC_BINDIR=/usr/cluster/bin
typeset -r SC_BINDIRS=${SC_BINDIR}:/usr/cluster/lib/sc
PATH=${SC_BINDIRS}:/bin:/usr/bin:/sbin:/usr/sbin; export PATH

# I18N
typeset -x TEXTDOMAIN=TEXT_DOMAIN
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

#####################################################
#
# print_usage()
#
#	Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "$(gettext 'usage'):  ${PROG} isfullyinstalled|isinstalling|isconfigured|hasbooted|isclustermember" >&2
}

#####################################################
#
# Main
#
#####################################################
main()
{
	typeset command=$1

	typeset pid
	typeset pids
	typeset foo
	typeset nwam_state

	case $command in
	isfullyinstalled)
		if [[ -f "${SCRCMD_INSTALLED_FILE}" ]]; then
			printf "$(gettext 'The Sun Cluster framework software is installed.')\n"
			return ${SC_TRUE}
		else
			printf "$(gettext 'The Sun Cluster framework software is not fully installed.')\n"
			return ${SC_FALSE}
		fi
		;;

	isinstalling)
		if [[ -f "${SCINSTALL_LOCKFILE}" ]]; then
			read pid foo <${SCINSTALL_LOCKFILE}
			if [[ -z "${pid}" ]] || [[ -n "${foo}" ]]; then
				printf "$(gettext 'Bad lock file.')\n"
				return ${SC_ERROR}
			fi
			pids="$(pgrep scinstall)"
			for foo in ${pids}
			do
				if [[ "${pid}" == "${foo}" ]]; then
					printf "$(gettext '\"%s\" is running.')\n"
					return ${SC_TRUE}
				fi
			done
		fi
		printf "$(gettext '\"%s\" is not running.')\n"
		return ${SC_FALSE}
		;;

	hasbooted)
		if [[ -f "${SC_DID_INSTANCES}" ]]; then
			printf "$(gettext 'This node has already been booted as a cluster node.')\n"
			return ${SC_TRUE}
		else
			printf "$(gettext 'This node has not yet been booted as a cluster node.')\n"
			return ${SC_FALSE}
		fi
		;;

	isconfigured)
		if [[ -f "${SCRCMD_NODEID_FILE}" ]]; then
			printf "$(gettext 'Sun Cluster is configured.')\n"
			return ${SC_TRUE}
		else
			printf "$(gettext 'Sun Cluster is not configured.')\n"
			return ${SC_FALSE}
		fi
		;;

	isclustermember)
		if [[ -f "${SCRCMD_NODEID_FILE}" ]] &&
		    [[ -x /usr/sbin/clinfo ]]; then
			/usr/sbin/clinfo > /dev/null 2>&1
			if [[ $? -eq 0 ]]; then
				printf "$(gettext 'This node is a cluster member.')\n"
				return ${SC_TRUE}
			fi
		fi
		printf "$(gettext 'This node is a not cluster member.')\n"
		return ${SC_FALSE}
		;;

	isnwamenabled)
		nwam_state=`LC_ALL=C; export LC_ALL; svcs -H -o state svc:/network/physical:nwam 2>/dev/null`
		if [[ "${nwam_state}" == "online" ]] ||
		    [[ "${nwam_state}" == "enabled" ]]; then
			return ${SC_TRUE}
		fi
		return ${SC_FALSE}
		;;
	*)
		print_usage
		return ${SC_ERROR}
		;;
	esac

	print_usage
	return ${SC_ERROR}
}

	main $*
	exit $?
