#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)scinstall_argvars.ksh	1.24	09/05/03 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#     
#

####################################################
#
# argvar_append_tofile_nodename() index filename
#
#	Append SC_ARGVAR_NODENAME[index]
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_nodename()
{
	typeset index=${1}
	typeset filename=${2}
	integer i

	set -f

	# append SC_ARGVAR_NODENAME[index] to the filename
	echo "SC_ARGVAR_NODENAME[${index}]=\"$(echo ${SC_ARGVAR_NODENAME[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_jumpstartdir() filename
#
#	Append SC_ARGVAR_JUMPSTARTDIR to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_jumpstartdir()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_JUMPSTARTDIR to the filename
	echo "SC_ARGVAR_JUMPSTARTDIR=\"$(echo ${SC_ARGVAR_JUMPSTARTDIR})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_sponsornode() index filename
#
#	Append SC_ARGVAR_SPONSORNODE[index]
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_sponsornode()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_SPONSORNODE[index] to the filename
	echo "SC_ARGVAR_SPONSORNODE[${index}]=\"$(echo ${SC_ARGVAR_SPONSORNODE[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_clustername() filename
#
#	Append SC_ARGVAR_CLUSTERNAME to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_clustername()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_CLUSTERNAME to the filename
	echo "SC_ARGVAR_CLUSTERNAME=\"$(echo ${SC_ARGVAR_CLUSTERNAME})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_onenode() filename
#
#	Append SC_ARGVAR_ONENODE
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_onenode()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_ONENODE to the filename
	echo "SC_ARGVAR_ONENODE=\"$(echo ${SC_ARGVAR_ONENODE})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_authlist() filename
#
#	Append SC_ARGVAR_AUTHLIST to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_authlist()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_AUTHLIST to the filename
	echo "SC_ARGVAR_AUTHLIST=\"$(echo ${SC_ARGVAR_AUTHLIST})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_authtype() filename
#
#	Append SC_ARGVAR_AUTHTYPE to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_authtype()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_AUTHTYPE to the filename
	echo "SC_ARGVAR_AUTHTYPE=\"$(echo ${SC_ARGVAR_AUTHTYPE})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_netaddr() filename
#
#	Append SC_ARGVAR_NETADDR and SC_ARGVAR_NETMASK
#	and SC_ARGVAR_MAXNODES and SC_ARGVAR_MAXPRIVATENETS
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_netaddr()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_NETADDR to the filename
	echo "SC_ARGVAR_NETADDR=\"$(echo ${SC_ARGVAR_NETADDR})\"" >>${filename} || return 1

	# append SC_ARGVAR_NETMASK to the filename
	echo "SC_ARGVAR_NETMASK=\"$(echo ${SC_ARGVAR_NETMASK})\"" >>${filename} || return 1

	# append SC_ARGVAR_MAXNODES to the filename
	echo "SC_ARGVAR_MAXNODES=\"$(echo ${SC_ARGVAR_MAXNODES})\"" >>${filename} || return 1

	# append SC_ARGVAR_MAXPRIVATENETS to the filename
	echo "SC_ARGVAR_MAXPRIVATENETS=\"$(echo ${SC_ARGVAR_MAXPRIVATENETS})\"" >>${filename} || return 1

	# append SC_ARGVAR_VIRTUALCLUSTERS to the filename
	echo "SC_ARGVAR_VIRTUALCLUSTERS=\"$(echo ${SC_ARGVAR_VIRTUALCLUSTERS})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_directconnect() filename
#
#	Append SC_ARGVAR_TWONODES and SC_ARGVAR_DIRECT
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_directconnect()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_TWONODES to the filename
	echo "SC_ARGVAR_TWONODES=\"$(echo ${SC_ARGVAR_TWONODES})\"" >>${filename} || return 1

	# append SC_ARGVAR_DIRECT to the filename
	echo "SC_ARGVAR_DIRECT=\"$(echo ${SC_ARGVAR_DIRECT})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_junctions() index filename
#
#	Append SC_ARGVAR_JUNCTIONS[index] and SC_ARGVAR_JUNCTYPES
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_junctions()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_JUNCTIONS[index] to the filename
	echo "SC_ARGVAR_JUNCTIONS[${index}]=\"$(echo ${SC_ARGVAR_JUNCTIONS[index]})\"" >>${filename} || return 1

	# append SC_ARGVAR_JUNCTYPES[index] to the filename
	echo "SC_ARGVAR_JUNCTYPES[${index}]=\"$(echo ${SC_ARGVAR_JUNCTYPES[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_adapters() index filename
#
#	Append SC_ARGVAR_ADAPTERS[index] and 
#	SC_ARGVAR_ADAPTERS_VLAN[index] to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_adapters()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_ADAPTERS[index] to the filename
	echo "SC_ARGVAR_ADAPTERS[${index}]=\"$(echo ${SC_ARGVAR_ADAPTERS_NICS[index]})\"" >>${filename} || return 1

	# append SC_ARGVAR_ADAPTERS_VLAN[index] to the filename
	echo "SC_ARGVAR_ADAPTERS_VLAN[${index}]=\"$(echo ${SC_ARGVAR_ADAPTERS_VLAN[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_etheradap() index filename
#
#	Append SC_ARGVAR_ETHERADAP[index]
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_etheradap()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_ETHERADAP[index] to the filename
	echo "SC_ARGVAR_ETHERADAP[${index}]=\"$(echo ${SC_ARGVAR_ETHERADAP[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_trtypes() index filename
#
#	Append SC_ARGVAR_TRTYPES[index]
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_trtypes()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_TRTYPES[index] to the filename
	echo "SC_ARGVAR_TRTYPES[${index}]=\"$(echo ${SC_ARGVAR_TRTYPES[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_cables() index filename
#
#	Append SC_ARGVAR_E2CABLES[index]
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_cables()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_E2CABLES[index] to the filename
	echo "SC_ARGVAR_E2CABLES[${index}]=\"$(echo ${SC_ARGVAR_E2CABLES[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_ports() index filename
#
#	Append both SC_ARGVAR_E1PORTS[index] and
#	SC_ARGVAR_E2PORTS[index] to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_ports()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_E1PORTS[index] to the filename
	echo "SC_ARGVAR_E1PORTS[${index}]=\"$(echo ${SC_ARGVAR_E1PORTS[index]})\"" >>${filename} || return 1

	# append SC_ARGVAR_E2PORTS[index] to the filename
	echo "SC_ARGVAR_E2PORTS[${index}]=\"$(echo ${SC_ARGVAR_E2PORTS[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_lofi() filename
#
#	Append SC_ARGVAR_LOFI to the given "filename".
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_lofi()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_LOFI to the filename
	echo "SC_ARGVAR_LOFI=\"$(echo ${SC_ARGVAR_LOFI})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_globaldir() index filename
#
#	Append SC_ARGVAR_GDIR[index]
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_globaldir()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_GDIR[index] to the filename
	echo "SC_ARGVAR_GDIR[${index}]=\"$(echo ${SC_ARGVAR_GDIR[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_patchdir() filename
#
#	Append SC_ARGVAR_PATCHDIR and 
#	SC_ARGVAR_RESPONSE_PATCHDIR to the given 
#	"filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_patchdir()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_PATCHDIR to the filename
	echo "SC_ARGVAR_PATCHDIR=\"$(echo ${SC_ARGVAR_PATCHDIR})\"" >>${filename} || return 1

	# append SC_ARGVAR_RESPONSE_PATCHDIR to the filename
	echo "SC_ARGVAR_RESPONSE_PATCHDIR=\"$(echo ${SC_ARGVAR_RESPONSE_PATCHDIR})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_patchfile() filename
#
#	Append SC_ARGVAR_PATCHFILE to the given 
#	"filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_patchfile()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_PATCHFILE to the filename
	echo "SC_ARGVAR_PATCHFILE=\"$(echo ${SC_ARGVAR_PATCHFILE})\"" >>${filename} || return 1
	return 0
}

####################################################
#
# argvar_append_tofile_remote_method() filename
#
#	Append SC_ARGVAR_REMOTE_METHOD to the given 
#	"filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_remote_method()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_REMOTE_METHOD to the filename
	echo "SC_ARGVAR_REMOTE_METHOD=\"$(echo ${SC_ARGVAR_REMOTE_METHOD})\"" >>${filename} || return 1
	return 0
}

####################################################
#
# argvar_append_tofile_services() index filename
#
#	Append SC_ARGVAR_SERVICES[index]
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_services()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_SERVICES[index] to the filename
	echo "SC_ARGVAR_SERVICES[${index}]=\"$(echo ${SC_ARGVAR_SERVICES[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_state() index filename
#
#	Append SC_ARGVAR_STATE[index]
#	to the given "filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_state()
{
	integer index=${1}
	typeset filename=${2}

	set -f

	# append SC_ARGVAR_STATE[index] to the filename
	echo "SC_ARGVAR_STATE[${index}]=\"$(echo ${SC_ARGVAR_STATE[index]})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_global_fencing
#
#	Append SC_ARGVAR_GLOBAL_FENCING to the given
#	 "filename".
#
#	Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_global_fencing()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_GLOBAL_FENCING to the filename
	echo "SC_ARGVAR_GLOBAL_FENCING=\"$(echo ${SC_ARGVAR_GLOBAL_FENCING})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_tofile_autoquorumconfig() filename
#
#	Append SC_ARGVAR_AUTOQUORUMCONFIG_* to the given 
#	"filename".
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_tofile_autoquorumconfig()
{
	typeset filename=${1}

	set -f

	# append SC_ARGVAR_AUTOQUORUMCONFIG_TASKNAME to the filename
	echo "SC_ARGVAR_AUTOQUORUMCONFIG_TASKNAME=\"$(echo ${SC_ARGVAR_AUTOQUORUMCONFIG_TASKNAME})\"" >>${filename} || return 1

	# append SC_ARGVAR_AUTOQUORUMCONFIG_STATE to the filename
	echo "SC_ARGVAR_AUTOQUORUMCONFIG_STATE=\"$(echo ${SC_ARGVAR_AUTOQUORUMCONFIG_STATE})\"" >>${filename} || return 1

	return 0
}

####################################################
#
# argvar_append_all_tofile() "indices" filename
#
#	Append SC_ARGVAR_ argument variables to "filename"
#	for all given "indices".   If "indices" is NULL,
#	0-7 is used.
#
#       Return:
#		zero		Success
#		non-zero	Failure
#
####################################################
argvar_append_all_tofile()
{
	typeset indices="${1}"
	typeset filename=${2}

	typeset i

	# If "indices" is NULL, do all ${SC_MAXNODES}
	if [[ -z "${indices}" ]]; then
		i=0
		while [[ ${i} -lt ${SC_MAXNODES} ]]
		do
			indices="${indices} ${i}"
			((i += 1))
		done
	fi

	# Array variables
	for i in ${indices}
	do
		argvar_append_tofile_nodename "${i}" ${filename} || return 1
		argvar_append_tofile_sponsornode "${i}" ${filename} || return 1
	done

	# Non-array variables
	argvar_append_tofile_jumpstartdir ${filename} || return 1
	argvar_append_tofile_clustername ${filename} || return 1
	argvar_append_tofile_onenode ${filename} || return 1
	argvar_append_tofile_authlist ${filename} || return 1
	argvar_append_tofile_authtype ${filename} || return 1
	argvar_append_tofile_netaddr ${filename} || return 1
	argvar_append_tofile_directconnect ${filename} || return 1
	argvar_append_tofile_patchdir ${filename} || return 1
	argvar_append_tofile_patchfile ${filename} || return 1
	argvar_append_tofile_remote_method ${filename} || return 1
	argvar_append_tofile_autoquorumconfig ${filename} || return 1
	argvar_append_tofile_lofi ${filename} || return 1
	argvar_append_tofile_global_fencing ${filename} || return 1

	# Array variables
	for i in ${indices}
	do
		argvar_append_tofile_junctions "${i}" ${filename} || return 1
		argvar_append_tofile_adapters "${i}" ${filename} || return 1
		argvar_append_tofile_etheradap "${i}" ${filename} || return 1
		argvar_append_tofile_trtypes "${i}" ${filename} || return 1
		argvar_append_tofile_cables "${i}" ${filename} || return 1
		argvar_append_tofile_ports "${i}" ${filename} || return 1
		argvar_append_tofile_globaldir "${i}" ${filename} || return 1
		argvar_append_tofile_services "${i}" ${filename} || return 1
		argvar_append_tofile_state "${i}" ${filename} || return 1
	done

	return 0
}

#############################################################
#
# argvar_print_SC_ARGVARS() "index" "command" ["added_options"]
#
#	Print an scinstall command from SC_VARARGS.
#	The form of the command is given in "command" and
# 	options not contained in ARGVARs are provided in
#	"added_options".
#
#	This function always returns zero.
#
##############################################################
argvar_print_SC_ARGVARS()
{
	typeset index="${1}"
	typeset command="${2}"
	typeset added_options="${3}"

	integer N

	# Set SC_ARGVARS from the SC_ARGVAR_ set of variables
	set -A SC_ARGVARS
	argvars_to_SC_ARGVARS "${index}" "${command}"

	# Print the first line
	printf "      %s" "${SC_ARGVARS[0]}"

	# Print the remaining lines
	N=1
	while [[ -n "${SC_ARGVARS[N]}" ]]
	do
		printf " \\ \n"
		printf "           %s" "${SC_ARGVARS[N]}"
		((N += 1))
	done

	if [[ -n "${added_options}" ]]; then
		printf " \\ \n"
		printf "           %s" "${added_options}"
	fi

	printf "\n\n"

	return 0
}

####################################################
#
# argvar_set_indices()
#
#	Set SC_INDICES to the list of all indices found in
#	SC_ARGVAR_NODNAME[*].
#
#	This function always returns zero.
#	
####################################################
argvar_set_indices()
{
	integer index
	typeset node
	typeset sc_indices

	# Set sc_indices
	sc_indices=
	for node in ${SC_ARGVAR_NODENAME[*]}
	do
		index=0
		while [[ "${node}" != "${SC_ARGVAR_NODENAME[index]}" ]]
		do
			((index += 1))
		done
		sc_indices="${sc_indices} ${index}"
	done

	# Set SC_INDICES
	SC_INDICES=
	SC_INDICES="$(
	    (
	    	for index in ${sc_indices}
		do
			echo ${index}
		done
	    ) | sort -n -)"

	return 0
}

####################################################
#
# argvars_to_SC_ARGVARS "index" "variables"
#
#	Inspect the SC_ARGVAR variables and
#	insert them into the SC_ARGVARS array
#	as options to scinstall.
#
#	The first element of the SC_ARGVARS array
#	will consist of "variables".  The second two
#	should always be the clustername and sponsornode.
#	Additional elements are formed from the
#	SC_ARGVAR_* variable settings.
#
#	This function always returns zero.
#
####################################################
argvars_to_SC_ARGVARS()
{
	typeset index="${1}"
	typeset variables="${2}"

	typeset adapter
	typeset adapters
	typeset trtypes
	typeset junctions
	typeset junctypes
	typeset cables
	typeset e1ports
	typeset e2ports
	typeset var
	typeset service
	typeset argvar_patchdir=
	typeset tasks
	typeset task
	typeset states


	integer N=0
	integer i
	integer first
	integer newcluster=0

	# clear SC_ARGVARS
	set -A SC_ARGVARS

	# Set "newcluster" flag, if this is a new cluster
	if [[ "${SC_ARGVAR_SPONSORNODE[index]}" == "${SC_ARGVAR_NODENAME[index]}" ]]; then
		newcluster=1
	fi

	#
	# Set SC_ARGVARS[N] to variables passed to this func
	#
	if [[ -n "${variables}" ]]; then
		SC_ARGVARS[N]="${variables}"
		((N += 1))
	fi

	#
	# Set SC_ARGVARS[N] to CLUSTERNAME
	#
	if [[ -n "${SC_ARGVAR_CLUSTERNAME}" ]]; then
		SC_ARGVARS[N]="-C ${SC_ARGVAR_CLUSTERNAME}"
		((N += 1))
	fi

	#
	# Set SC_ARGVARS[N] to -F or -N SPONSORNODE
	#
	if [[ ${newcluster} -eq 1 ]]; then
		SC_ARGVARS[N]="-F"
		((N += 1))

		#
		# Single node cluster?
		#
		if [[ "${SC_ARGVAR_ONENODE}" == "1" ]]; then
			SC_ARGVARS[N]="-o"
			((N += 1))
		fi

	elif [[ -n "${SC_ARGVAR_SPONSORNODE[index]}" ]]; then
		SC_ARGVARS[N]="-N ${SC_ARGVAR_SPONSORNODE[index]}"
		((N += 1))
	fi

	#
	# Add global devices file system, if not the default
	#
	if [[ -n "${SC_ARGVAR_GDIR[index]}" ]] &&
	    [[ "${SC_ARGVAR_GDIR[index]}" != "${SC_DFLT_GDIR}" ]]; then
		SC_ARGVARS[N]="-G ${SC_ARGVAR_GDIR[index]}"
		((N += 1))
	fi

	#
	# Add patch directory, if not the default
	#
	if [[ -n "${SC_ARGVAR_PATCHDIR}" ]]; then
		argvar_patchdir="${SC_ARGVAR_PATCHDIR}"
	elif [[ -n "${SC_ARGVAR_RESPONSE_PATCHDIR}" ]]; then
		argvar_patchdir="${SC_ARGVAR_RESPONSE_PATCHDIR}"
	fi

	if [[ -n "${argvar_patchdir}" ]]; then
		if [[ -n "${SC_ARGVAR_PATCHFILE}" ]]; then
			SC_ARGVARS[N]="-M patchdir=${argvar_patchdir},patchlistfile=${SC_ARGVAR_PATCHFILE}"
		else
			SC_ARGVARS[N]="-M patchdir=${argvar_patchdir}"
		fi
		((N += 1))
	fi

	#
	# Set SC_ARGVARS[N] to SERVICES
	#
	for service in ${SC_ARGVAR_SERVICES[index]}
	do
		if [[ -n "${SC_ARGVARS[N]}" ]]; then
			SC_ARGVARS[N]="${SC_ARGVARS[N]},${service}"
		else
			SC_ARGVARS[N]="-s ${service}"
		fi
	done
	if [[ -n "${SC_ARGVARS[N]}" ]]; then
		((N += 1))
	fi

	#
	# Set SC_ARGVARS[N] to AUTHLIST and AUTHTYPE (only for new cluster)
	#
	if [[ "${SC_ARGVAR_ONENODE}" != "1" ]]; then
		if [[ ${newcluster} -eq 1 ]]; then
			first=1
			if [[ -n "${SC_ARGVAR_AUTHLIST}" ]]; then
				for var in ${SC_ARGVAR_AUTHLIST}
				do
					if [[ ${first} -eq 1 ]]; then
						SC_ARGVARS[N]="-T node=${var}"
						first=0
					else
						SC_ARGVARS[N]="${SC_ARGVARS[N]},node=${var}"
					fi
				done
				if [[ -n "${SC_ARGVAR_AUTHTYPE}" ]]; then
					SC_ARGVARS[N]="${SC_ARGVARS[N]},authtype=${SC_ARGVAR_AUTHTYPE}"
				fi
			fi
			if [[ -n "${SC_ARGVARS[N]}" ]]; then
				((N += 1))
			fi
		fi
	fi

	#
	# Set SC_ARGVARS[N] to NETADDR and NETMASK (only for new cluster)
	#
	if [[ "${SC_ARGVAR_ONENODE}" != "1" ]]; then
		if [[ ${newcluster} -eq 1 ]]; then
			SC_ARGVAR_NETADDR=${SC_ARGVAR_NETADDR:-${SC_DFLT_NETADDR}}
			SC_ARGVAR_NETMASK=${SC_ARGVAR_NETMASK:-${SC_DFLT_NETMASK}}
			SC_ARGVAR_VIRTUALCLUSTERS=${SC_ARGVAR_VIRTUALCLUSTERS:-${SC_DFLT_VIRTUALCLUSTERS}}
			
			if [[ "${SC_ARGVAR_NETADDR}" != "${SC_DFLT_NETADDR}" ]] || [[ "${SC_ARGVAR_NETMASK}" != "${SC_DFLT_NETMASK}" ]] ||  [[ "${SC_ARGVAR_SC_ARGVAR_VIRTUALCLUSTERS}" != "${SC_DFLT_VIRTUALCLUSTERS}" ]] ; then 
				SC_ARGVARS[N]="-w netaddr=${SC_ARGVAR_NETADDR}"
				if [[ "${SC_ARGVAR_NETMASK}" != "${SC_DFLT_NETMASK}" ]]; then
				SC_ARGVARS[N]="${SC_ARGVARS[N]},netmask=${SC_ARGVAR_NETMASK},maxnodes=${SC_ARGVAR_MAXNODES},maxprivatenets=${SC_ARGVAR_MAXPRIVATENETS},numvirtualclusters=${SC_ARGVAR_VIRTUALCLUSTERS}"
				fi
			fi
			if [[ -n "${SC_ARGVARS[N]}" ]]; then
				((N += 1))
			fi
		fi
	fi

	#
	# Set SC_ARGVARS[N] to the adapter option lists
	#
	if [[ "${SC_ARGVAR_ONENODE}" != "1" ]]; then
		set -A adapters ${SC_ARGVAR_ADAPTERS[index]}
		set -A adapters_vlan ${SC_ARGVAR_ADAPTERS_VLAN[index]}
		set -A trtypes  ${SC_ARGVAR_TRTYPES[index]}
		i=-1
		for adapter in ${adapters[*]}
		do
			((i += 1))

			if [[ -z "${trtypes[i]}" ]]; then
				continue
			fi
			vlanid=${adapters_vlan[i]}
			SC_ARGVARS[N]="${SC_ARGVARS[N]} -A trtype=${trtypes[i]},name=${adapters[i]}"
			if [[ -n "${vlanid}" ]] && [[ "${vlanid}" != "0" ]]; then
				SC_ARGVARS[N]="${SC_ARGVARS[N]},vlanid=${adapters_vlan[i]}"
			fi
		done
		if [[ -n "${SC_ARGVARS[N]}" ]]; then
			set -f
			SC_ARGVARS[N]="$(echo ${SC_ARGVARS[N]})"
			set +f
			((N += 1))
		fi
	fi

	#
	# Set SC_ARGVARS[N] to the junction option lists (only for new clusters)
	#
	if [[ "${SC_ARGVAR_ONENODE}" != "1" ]]; then
		if [[ "${SC_ARGVAR_DIRECT}" == "1" ]]; then
			SC_ARGVARS[N]="-B type=direct"

		elif [[ ${newcluster} -eq 1 ]]; then
			set -A junctions ${SC_ARGVAR_JUNCTIONS[index]}
			set -A junctypes ${SC_ARGVAR_JUNCTYPES[index]}
			i=-1
			for adapter in ${adapters[*]}
			do
				((i += 1))

				if [[ -z "${junctions[i]}" ]] ||
				    [[ -z "${junctypes[i]}" ]]; then
					continue
				fi
				SC_ARGVARS[N]="${SC_ARGVARS[N]} -B type=${junctypes[i]},name=${junctions[i]}"
			done
		fi
		if [[ -n "${SC_ARGVARS[N]}" ]]; then
			set -f
			SC_ARGVARS[N]="$(echo ${SC_ARGVARS[N]})"
			set +f
			((N += 1))
		fi
	fi

	#
	# One cable per SC_ARGVARS[N] line (not on new direct connect clusters)
	#
	# Note that new clusters which are directly connected will
	# not have any cables.
	#
	# E1 ports are for the adapters on this end of each endpoint,
	# and E2 ports are for the other end.
	#
	typeset adap_name_field
	typeset vnic_link
	if [[ "${SC_ARGVAR_ONENODE}" != "1" ]]; then
		if [[ ${newcluster} -eq 0 ]] ||
		    [[ "${SC_ARGVAR_DIRECT}" != "1" ]]; then
			set -A adapters ${SC_ARGVAR_ADAPTERS[index]}
			set -A cables   ${SC_ARGVAR_E2CABLES[index]}
			set -A e1ports    ${SC_ARGVAR_E1PORTS[index]}
			set -A e2ports    ${SC_ARGVAR_E2PORTS[index]}
			i=-1
			for adapter in ${adapters[*]}
			do
				((i += 1))

				if [[ -z "${cables[i]}" ]] ||
				    [[ "${cables[i]}" == "@" ]]; then
					((i += 1))
					continue
				fi

				# Check whether vnic feature is supported
				is_vnic_capable
				integer is_vnic_supported=$?

				# Check for the pattern "*%*%*" which means we are about to
				# create vnics. Then parse to get the vnic name
				# Format - "name=#<nic_link>%<macaddr>%<vnic_link>#"
				adap_name_field=$(echo ${adapters[i]} | /usr/bin/awk -F"#" '{print $2}')
				if [[ ${is_vnic_supported} -eq ${SC_TRUE} ]] &&
					[[ ${adap_name_field} == *%*%* ]]; then
					vnic_link=$(echo ${adap_name_field} | /usr/bin/awk -F"%" '{print $3}')
					SC_ARGVARS[N]="-m endpoint=:${vnic_link}"
				else
					SC_ARGVARS[N]="-m endpoint=:${adapters[i]}"
				fi

				if [[ -n "${e1ports[i]}" ]] &&
				    [[ "${e1ports[i]}" != "@" ]]; then
					SC_ARGVARS[N]="${SC_ARGVARS[N]}@${e1ports[i]}"
				fi
				SC_ARGVARS[N]="${SC_ARGVARS[N]},endpoint=${cables[i]}"
				if [[ -n "${e2ports[i]}" ]] &&
				    [[ "${e2ports[i]}" != "@" ]]; then
					SC_ARGVARS[N]="${SC_ARGVARS[N]}@${e2ports[i]}"
				fi
				((N += 1))
			done
		fi
	fi

	#
	# Set SC_ARGVARS[N] to the postconfig task list (only for new clusters)
	#
	if [[ ${newcluster} -eq 1 ]]; then
		if [[ -n ${SC_ARGVAR_AUTOQUORUMCONFIG_TASKNAME} ]] &&
		    [[ -n ${SC_ARGVAR_AUTOQUORUMCONFIG_STATE} ]]; then
			SC_ARGVARS[N]="${SC_ARGVARS[N]} -P task=${SC_ARGVAR_AUTOQUORUMCONFIG_TASKNAME},state=${SC_ARGVAR_AUTOQUORUMCONFIG_STATE}"
		fi

		if [[ -n "${SC_ARGVARS[N]}" ]]; then
			set -f
			SC_ARGVARS[N]="$(echo ${SC_ARGVARS[N]})"
			set +f
			((N += 1))
		fi

		if [[ -n ${SC_ARGVAR_GLOBAL_FENCING} ]]; then
			SC_ARGVARS[N]="${SC_ARGVARS[N]} -e global_fencing=${SC_ARGVAR_GLOBAL_FENCING}"
		fi

		if [[ -n "${SC_ARGVARS[N]}" ]]; then
			set -f
			SC_ARGVARS[N]="$(echo ${SC_ARGVARS[N]})"
			set +f
			((N += 1))
		fi
	fi

	return 0
}

####################################################
#
# argvar_typical_argvars() filename
#
#	Clear all non-typical SC_ARGVAR_ argument variables,
#	and reset the "filename" with the new values.
#
#       Return:
#		zero		Success
#		non-zero	Failure
#	
####################################################
argvar_typical_argvars()
{
	typeset filename=${1}

	#
	# Reset all variables except the following:
	#
	#	SC_INDICES
	#	SC_ARGVAR_NODENAME
	#	SC_ARGVAR_CLUSTERNAME
	#	SC_ARGVAR_AUTHLIST
	#	SC_ARGVAR_ADAPTERS
	#	SC_ARGVAR_JUMPSTARTDIR
	#
	set -A SC_ARGVAR_ONENODE
	set -A SC_ARGVAR_AUTHTYPE
	set -A SC_ARGVAR_NETADDR
	set -A SC_ARGVAR_NETMASK
	set -A SC_ARGVAR_VIRTUALCLUSTERS
	set -A SC_ARGVAR_TWONODES
	set -A SC_ARGVAR_DIRECT
	set -A SC_ARGVAR_SPONSORNODE
	set -A SC_ARGVAR_JUNCTIONS
	set -A SC_ARGVAR_JUNCTYPES
	set -A SC_ARGVAR_TRTYPES
	set -A SC_ARGVAR_E2CABLES
	set -A SC_ARGVAR_E1PORTS
	set -A SC_ARGVAR_E2PORTS
	set -A SC_ARGVAR_LOFI
	set -A SC_ARGVAR_GDIR
	set -A SC_ARGVAR_SERVICES
	set -A SC_ARGVAR_PATCHDIR
	set -A SC_ARGVAR_RESPONSE_PATCHDIR
	set -A SC_ARGVAR_PATCHFILE
	set -A SC_ARGVAR_REMOTE_METHOD
	set -A SC_ARGVAR_STATE
	set -A SC_ARGVAR_AUTOQUORUMCONFIG_TASKNAME
	set -A SC_ARGVAR_AUTOQUORUMCONFIG_STATE
	set -A SC_ARGVAR_GLOBAL_FENCING

	# Remove the old file and create the new one
	rm -f ${filename}
	argvar_append_all_tofile "${SC_INDICES}" ${filename} || return 1

	return 0
}

####################################################
#
# argvar_clear_all_argvars()
#
#	Clear all SC_ARGVAR_ argument variables
#
#	This function always returns zero.
#	
####################################################
argvar_clear_all_argvars()
{
	set -A SC_ARGVAR_NODENAME
	set -A SC_ARGVAR_JUMPSTARTDIR
	set -A SC_ARGVAR_CLUSTERNAME
	set -A SC_ARGVAR_ONENODE
	set -A SC_ARGVAR_AUTHLIST
	set -A SC_ARGVAR_AUTHTYPE
	set -A SC_ARGVAR_NETADDR
	set -A SC_ARGVAR_NETMASK
	set -A SC_ARGVAR_TWONODES
	set -A SC_ARGVAR_DIRECT
	set -A SC_ARGVAR_SPONSORNODE
	set -A SC_ARGVAR_JUNCTIONS
	set -A SC_ARGVAR_JUNCTYPES
	set -A SC_ARGVAR_ADAPTERS
	set -A SC_ARGVAR_ADAPTERS_VLAN
	set -A SC_ARGVAR_TRTYPES
	set -A SC_ARGVAR_E2CABLES
	set -A SC_ARGVAR_E1PORTS
	set -A SC_ARGVAR_E2PORTS
	set -A SC_ARGVAR_LOFI
	set -A SC_ARGVAR_GDIR
	set -A SC_ARGVAR_SERVICES
	set -A SC_ARGVAR_PATCHDIR
	set -A SC_ARGVAR_RESPONSE_PATCHDIR
	set -A SC_ARGVAR_PATCHFILE
	set -A SC_ARGVAR_REMOTE_METHOD
	set -A SC_ARGVAR_STATE
	set -A SC_ARGVAR_AUTOQUORUMCONFIG_TASKNAME
	set -A SC_ARGVAR_AUTOQUORUMCONFIG_STATE
	set -A SC_ARGVAR_GLOBAL_FENCING

	return 0
}

