#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)sc_common.ksh	1.36	09/02/26 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

####################################################
#
# Global constants
#
####################################################
AWK=/usr/bin/awk

# For private net address calculations
SC_PNET_DFLT_MAXNODES=64
SC_PNET_MIN_MAXNODES=2
SC_PNET_MAX_MAXNODES=64
SC_PNET_DFLT_MAXPRIVATENETS=10
SC_PNET_MIN_MAXPRIVATENETS=2
SC_PNET_MAX_MAXPRIVATENETS=128
SC_PNET_DFLT_NETADDR=172.16.0.0
SC_PNET_DFLT_NETMASK=255.255.240.0
SC_PNET_DFLT_VIRTUALCLUSTERS=12

# indicates SC was installed via IPS packages
IPS_FLAG_FILE=/var/cluster/ips/.ips

# name of minimal (pun intended) required meta-package
# may have been installed directly or as dependency
# of ha-cluster-framework-full
IPS_PKG_MINIMAL=ha-cluster-framework-minimal

####################################################
#
# gettext() [domain] "msgid"
#
#	domain	- Optional argument specifying domain for internationalization.
#	msgid   - Text Message or Message id to be passed to "gettext"
#
#	This function masks the command "/usr/bin/gettext" to enable
#	proper concatenation of multiline text messages into a single
#	contiguous text string.
#
#	This function returns same results as from /usr/bin/gettext command.
#
####################################################
gettext()
{
	typeset -x domain
	typeset -x msgid

	if [[ $# -eq 2 ]];then
		domain=$1
		shift
	else
		if [[ $# -ne 1 ]];then
			/usr/bin/gettext $*
			return $?
		fi
	fi

	msgid=$1

	/usr/bin/gettext ${domain} "$(/bin/echo "${msgid}" | sed "s/		*/ /g" | sed "s/^  */ /g" | tr -d "\n")"

	return $?
}

####################################################
#
# sc_print_title() "text"
#
#	text	- the title
#
#	Print the given "text" as the first title
#	on the page.  This title is both preceeded
#	and followed by a newline.
#
#	This function always returns zero.
#
####################################################
sc_print_title()
{
	set -f
	typeset text="$(echo $*)"
	set +f

	clear
	echo
	printf "  %s\n" "${text}"
	echo

	return 0
}

####################################################
#
# sc_print_prompt() text [space]
#
#	text	- the prompt
#	space	- preserve whitespace
#
#	Print the given "text" as a prompt.
#
#	This function always returns zero.
#
####################################################
sc_print_prompt()
{
	set -f
	typeset text="${1}"
	typeset space="${2}"

	if [[ -z "${space}" ]]; then
		text="$(echo ${text})"
	fi
	set +f

	printf "    %s  " "${text}"

	return 0
}

####################################################
#
# sc_print_line() text
#
#	text	- the line
#
#	Print the given "text" as a line with the same left margin
#	as a prompt.
#
#	This function always returns zero.
#
####################################################
sc_print_line()
{
	typeset text="${1}"

	printf "    %s" "${text}"

	return 0
}

####################################################
#
# sc_print_para() text
#
#	text	- the text of the paragraph
#
#	Print the given "text" as a formatted paragraph to stdout.
#
#	This function always returns zero.
#
####################################################
sc_print_para()
{
	set -f
	typeset text="$(echo $*)"

	${SC_SCADMINLIBDIR}/scprt 4 70 "${text}" 2>/dev/null
	echo

	return 0
}

####################################################
#
# sc_print_ctrld_message() [menuname]
#
#	menuname	- if not given, "Main Menu" is used in the message
#
#	Print the "Control-d" message.
#
#	This function always returns zero.
#
####################################################
sc_print_ctrld_message()
{
	typeset menuname=$1

	typeset sctxt_ctrld_message="$(gettext SUNW_SC_INSTALL '
		Press Control-d at any time to return to the %s.
	')"

	if [[ -z "${menuname}" ]]; then
		menuname="$(gettext SUNW_SC_INSTALL 'Main Menu')"
	fi

	sc_print_para "$(printf "${sctxt_ctrld_message}" "${menuname}")"
}

####################################################
#
# sc_prompt() "prompt" [default [nonl [space]]]
#
#	prompt	- the prompt string
#	default	- the default value
#	nonl	- no extra newline after answer
#	space	- preserve whitespace
#
#	Display the prompt and return the user's answer.
#
#	If the trailing character of the prompt is ? or :, and
#	there is a "default", the trailer is re-positioned after
#	the "default".
#
#	The prompt is printed on file descriptor 4, and the answer
#	is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	Return values:
#		0	- proceed
#		1	- ^D was typed
#
####################################################
sc_prompt()
{
	set -f
	typeset prompt="${1}"
	typeset default="${2}"
	typeset nonl="${3}"
	typeset space="${4}"

	if [[ -z "${space}" ]]; then
		prompt="$(echo ${prompt})"
	fi
	set +f

	typeset answer=
	typeset trailer
	typeset foo

	integer i

	# 
	# The caller of this function should have already opened
	# descriptor 4 as a dup of the original stdout.
	#
	# Dup this function's stdout to descriptor 5.
	# Then, re-direct stdout to descriptor 4.
	#
	# So, the default stdout from this function will go to
	# the original stdout (probably the tty).  Descriptor 5
	# has the answer printed to it.
	#
	exec 5>&1
	exec 1>&4

	# if there is a default value, shift the terminating ? or : character
	if [[ -n "${default}" ]]; then
		trailer=
		if [[ "${prompt}" = *\? ]]; then
			trailer="?"
		elif [[ "${prompt}" = *: ]]; then
			trailer=":"
		fi
		if [[ -n "${trailer}" ]]; then
			prompt="${prompt%${trailer}}"
			prompt="${prompt} [${default}]${trailer}"
		else
			prompt="${prompt} [${default}]"
		fi
	fi

	#
	# Display the prompt and get the user's response
	# Loop until an answer is given.   Or, if there is a
	# default, the user need not supply an answer.
	#
	# It is not legal for the user to supply more than one value.
	#
	let i=0
	while true
	do
		# If this is not the first time through, beep
		[[ ${i} -gt 0 ]] && echo "\a\c"
		let i=1

		# Prompt and get response
		sc_print_prompt "${prompt}" "${space}"
		read answer foo

		# Return 1 on EOF
		if [[ $? -eq 1 ]]; then
			echo
			return 1
		fi

		# If more than one arg, repeat prompt
		if [[ -n "${foo}" ]]; then
			continue
		fi

		# If no answer and default, default is the answer
		if [[ -z "${answer}" ]] && [[ -n "${default}" ]]; then
			answer="${default}"
		fi

		# If still no answer, repeat prompt
		if [[ -z "${answer}" ]]; then
			continue
		fi

		# Okay
		break
	done

	# Unless "nonl" was given, print extra newline
	if [[ -z "${nonl}" ]]; then
		echo
	fi

	echo "${answer}" >&5

	return 0
}

####################################################
#
# sc_prompt_yesno() "prompt" [default]
#
#	prompt	- the prompt string
#	default	- the default value
#
#	Display the yes/no prompt and return the user's answer.
#
#	The user may y, yes, Y, YES, n, no, N, or NO.
#	Function will always print "yes" or "no" on stdout.
#
#	The prompt is printed on file descriptor 4, and the answer
#	is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	Return values:
#		0	- proceed
#		1	- ^D was typed
#
####################################################
sc_prompt_yesno()
{
	typeset prompt="${1}"
	typeset default="${2}"

	typeset answer=

	integer i

	# if there is a terminating ? and ${YES} is yes and ${NO} is no,
	# add (yes/no) and shift the terminating "?"
	if [[ "${YES}" = "yes" ]] &&
	    [[ "${NO}" = "no" ]] &&
	    [[ "${prompt}" = *\? ]]; then
		prompt="${prompt%\?}"
		prompt="${prompt} (yes/no)?"
	fi

	let i=0
	while true
	do
		# If this is not the first time through, beep
		[[ ${i} -gt 0 ]] && echo "\a\c" >&4
		let i=1

		# Prompt and get response
		answer=$(sc_prompt "${prompt}" "${default}" "nonl")

		# Return 1 on EOF
		if [[ $? -eq 1 ]]; then
			echo >&4
			return 1
		fi

		# I18N sensative answer always returns "yes" or "no" string
		case ${answer} in
		${YES} | yes | y | YES | Y)
			answer="yes"
			break
			;;

		${NO} | no | n | NO | N)
			answer="no"
			break
			;;

		*)
			answer=
			;;
		esac
	done
	echo >&4

	echo "${answer}"

	return 0
}

####################################################
#
# sc_prompt_pause()
#
#	Print message asking user to type the ENTER key,
#	then wait for a response from the keyboard.
#
#	Return values:
#		0	- proceed
#		1	- ^D was typed
#
####################################################
sc_prompt_pause()
{
	# Pause until they hit ENTER
	sc_print_prompt "\n$(gettext SUNW_SC_INSTALL 'Press Enter to continue:')"
	read
	if [[ $? -ne 0 ]]; then
       		echo
		return 1
	fi
	echo

	return 0
}

####################################################
#
# sc_get_menuoption() "menuitem" [...]
#
#	menuitem	- a menu item
#
#	Print the menu described by the list of menu
#	items, and return the selected option.
#
#	Each menuitem has the following format:
#
#	    "<type>+<printstar>+<optionletter>+<text>"
#
#		<type>:		T1/T2	for title
#				N	for non-selectable menu option
#				S	for selectable menu option
#				D	for default menu option
#				R	for newline
#
#		<printstar>:	0 to NOT print *
#				1 to print *
#
#       The menu is printed on file descriptor 4, and the selected
#       option is printed to stdout, in lower case.  File descriptor 4
#       should be dupped to the original stdout before this function is
#	called.
#
#	This function always returns zero.
#
####################################################
sc_get_menuoption()
{
	typeset type
	typeset printstar
	typeset optionletter
	typeset text
	typeset param=

	typeset star=
	typeset default=
	typeset -l selectable=			# lower case
	typeset -l answer=			# lower case
	typeset -l option=			# lower case

	# 
	# The caller of this function should have already opened
	# descriptor 4 as a dup of the original stdout.
	#
	# Dup this function's stdout to descriptor 5.
	# Then, re-direct stdout to descriptor 4.
	#
	# So, the default stdout from this function will go to
	# the original stdout (probably the tty).  Descriptor 5
	# has the option letter printed to it.
	#
	exec 5>&1
	exec 1>&4

	# Process each menu item in the arg list
	while [[ $# -ne 0 ]]
	do
		# Each menu item has 4 fields;  the field seperator is "+"
		param="${1}"
		type="$(echo "${param}" | awk -F'+' '{ print $1 }')"
		printstar="$(echo "${param}" | awk -F'+' '{ print $2 }')"
		optionletter="$(echo "${param}" | awk -F'+' '{ print $3 }')"
		text="$(echo "${param}" | awk -F'+' '{ print $4 }')"

		# Switch on item type
		case ${type} in
		'T1')	# Title 1
			sc_print_title "${text}"
			;;

		'T2')	# Title 2
			sc_print_para "${text}"
			;;

		'N')	# Non-selectble menu item
			printf "      %1.1s%2.2s) %s\n" "" "${optionletter##*\\}" "${text}"
			;;

		'S'|'D') # Selectble menu item
			star=
			if [[ "${printstar}" = 1 ]];  then
				star="*"
			fi
			printf "      %1.1s%2.2s) %s\n" "${star}" "${optionletter##*\\}" "${text}"
			selectable="${selectable} ${optionletter}"
			if [[ "${type}" == "D" ]]; then
				default="${optionletter##*\\}"
			fi
			;;

		'R')	# Newline
			echo
			;;
		esac
		shift
	done

	# Get the selected menu option
	answer=
	if [[ -n "${selectable}" ]]; then
		echo
		while [[ -z "${answer}" ]]
		do
			answer=$(sc_prompt "$(gettext SUNW_SC_INSTALL 'Option:')" ${default})
			for option in ${selectable}
			do
				if [[ "${answer}" = "${option##*\\}" ]]; then
					break 2
				fi
			done
			answer=

			# beep
			echo "\a\c"
		done
	fi

	echo "${answer}" >&5

	return 0
}

####################################################
#
# sc_get_scrolling_menuoptions() title header1 header2 minchoices
#    maxchoices numwords "option" [...]
#
#	title		- if not NULL, print a menu title
#	header1		- if not NULL, print 1st column header
#	header2		- if not NULL, print 2nd column header
#	minchoices	- if not NULL, cannot quit until min options selected
#	maxchoices	- if not NULL, maximum number of choices
#	numwords	- if not NULL, number of words to print in choice
#	option		- text of menu option item
#
#	Print a menu using the list of "choices".  If the menu length
#	exceeds a certain value, the user may select "next" (and "previous").
#
#	Any one of the "option" arguments may be given as ":<filename>",
#	where the <filename> contains one option per line.
#
#	If "minchoices" is given, the user may not quit the menu until
#	at list "minchoices" options have been selected.   The "quit"
#	prompt will not be displayed until "minchoices" options have been
#	selected.
#
#	If "maxchoices" is given, the menu automatically quits as soon
#	as "maxchoices" options have been selected.   If "maxchoices"
#	is not given or is given as 0, the user may select an unlimited
#	number of options.   The caller should always set "maxchoices"
#	to 1, if only one option is allowed.
#
#	If "numwords" is given and is greater than 1, the users
#	choices are printed as <word>:<word>:..., where the number of
#	words printed is the number of words given in "numwords".
#
#	The menu is printed on file descriptor 4, and the selected
#	choice(s) is printed to stdout.  File descriptor 4 should be
#	dupped to the original stdout before this function is called.
#
#	This function always returns zero.
#
####################################################
sc_get_scrolling_menuoptions()
{
	typeset title="${1}"
	typeset header1="${2}"
	typeset header2="${3}"
	integer minchoices="${4}"
	integer maxchoices="${5}"
	integer numwords="${6}"

	# We expect at least four arguments
	if [[ $# -lt 5 ]]; then
		return 0
	fi
	shift 6

	typeset option_array
	typeset newline_array
	typeset word_array
	typeset -l choice			# lower case
	typeset chosen_list
	typeset -l chosen			# lower case
	typeset foo
	typeset q="q"
	typeset n="n"
	typeset p="p"
	typeset filename
	typeset prompt

	integer linemax=10
#	integer line
	integer list_count
	integer num_options
	integer i
	integer j
	integer first
	integer err
	integer nl
	integer newscreen
	integer q_okay=0
	integer n_okay=0
	integer p_okay=0

	# 
	# The caller of this function should have already opened
	# descriptor 4 as a dup of the original stdout.
	#
	# Dup this function's stdout to descriptor 5.
	# Then, re-direct stdout to descriptor 4.
	#
	# So, the default stdout from this function will go to
	# the original stdout (probably the tty).  Descriptor 5
	# has the option letter printed to it.
	#
	exec 5>&1
	exec 1>&4

	# construct the array of options
	set -A option_array
	set -A newline_array
	let num_options=0
	while [[ $# -ne 0 ]]
	do
		if [[ "${1}" == :* ]]; then
			filename=$(echo ${1} | awk -F':' '{ print $1 }')
			filename=$(echo ${filename})
			if [[ -n "${filename}" ]] &&
			    [[ -r ${filename} ]]; then
				while read foo
				do
					if [[ -z "${foo}" ]]; then
						newline_array[num_options]=1
					else
						option_array[num_options]="${foo}"
						((num_options += 1))
					fi
				done <${filename}
			fi
		else
			option_array[num_options]="${1}"
			((num_options += 1))
		fi
		shift
	done
	if [[ ${num_options} -eq 0 ]]; then
		return 0
	fi

	# check min and max
	if [[ ${maxchoices} -gt 0 ]] &&
	    [[ ${minchoices} -gt ${maxchoices} ]]; then
		return 0
	fi
	if [[ ${num_options} -lt ${minchoices} ]] ||
	    [[ ${num_options} -lt ${maxchoices} ]]; then
		return 0
	fi

	# If there is only one option, we are done when they select it.
	if [[ ${num_options} -eq 1 ]]; then
		let maxchoices=1
	fi

	# Set the options prompt
	if [[ ${maxchoices} -eq 1 ]]; then
		prompt="$(gettext SUNW_SC_INSTALL 'Option')"
	else
		prompt="$(gettext SUNW_SC_INSTALL 'Option(s)')"
	fi

	# loop until the option or options are selected
	choice_list=
	let list_count=0
	let i=0
	let first=0
	while true
	do
		# Reset the line count
		let line=0

		# Initialize
		let q_okay=0
		let n_okay=0
		let p_okay=0
		let nl=0

		# Blank line
		if [[ ${first} -gt 0 ]]; then
			echo
		fi

		# Print the title, if there is one
		if [[ -n "${title}" ]];  then
			printf "%4.4s%s\n" "" "${title}"
			echo
		fi

		# Print the headers, if specified
		if [[ -n "${header1}" ]];  then
			printf "%11.11s%s\n" "" "${header1}"
			if [[ -n "${header2}" ]];  then
				printf "%11.11s%s\n" "" "${header2}"
			fi
			echo
		fi

		# Print "screenfull" of choices
		while [[ ${i} -lt ${num_options} ]] &&
		    [[ ${line} -lt ${linemax} ]]
		do
			if [[ -n "${newline_array[i]}" ]]; then
				echo
			fi
			printf "%5.5s%4.4s) %s\n" "" "$((i+1))" "${option_array[i]}"
			((i += 1))
			((line += 1))
		done

		# Add "Next" or "Previous", if necessary
		if [[ ${i} -eq ${num_options} ]] ||
		    [[ ${line} -eq ${linemax} ]];  then

			if [[ ${i} -gt ${linemax} ]];  then
				if [[ ${nl} -eq 0 ]]; then
					echo
					let nl=1
				fi

				printf "%7.7s%2.2s) %s\n" "" "${p}" "$(gettext SUNW_SC_INSTALL '< Previous')"
				let p_okay=1
			fi
			if [[ ${i} -lt ${num_options} ]]; then
				if [[ ${nl} -eq 0 ]]; then
					echo
					let nl=1
				fi

				printf "%7.7s%2.2s) %s\n" "" "${n}" "$(gettext SUNW_SC_INSTALL 'Next >')"
				let n_okay=1
			fi
		fi

		# If it is okay to quit, print it
		if [[ ${minchoices} -le ${list_count} ]]; then
			if [[ ${nl} -eq 0 ]]; then
				echo
			fi

			printf "%7.7s%2.2s) %s\n" "" "${q}" "$(gettext SUNW_SC_INSTALL 'Done')"
			let q_okay=1
		fi


		# Reset i
		((i -= ${line}))

		# Get choice or choices
		echo
		while true
		do
			# Initialize
			chosen=

			# Not the first time through?
			if [[ ${first} -gt 0 ]]; then
				
				# Print choice_list, if there is one
				if [[ ${list_count} -gt 0 ]]; then

					# Re-print the menu with done option?
					if [[ ${minchoices} -le ${list_count} ]] && [[ ${q_okay} -eq 0 ]]; then
						continue 2
					fi

					sc_print_prompt " $(gettext SUNW_SC_INSTALL 'Selected:')" "space"
					let j=0
					for choice in ${choice_list}
					do
						if [[ ${j} -gt 0 ]]; then
							echo ",\c"
						fi
						echo "${choice}\c"
						((j += 1))
					done
					echo
					echo

				# Otherwise, beep
				elif [[ ${newscreen} -eq 0 ]]; then
					echo "\a\c"
				fi
			fi
			((first += 1))

			# Get the answer
			let newscreen=0
			sc_print_prompt "${prompt}:"
			read chosen foo

			# Ctrl-D
			if [[ $? -ne 0 ]]; then
				return 0
			fi

			# If no answer, repeat
			if [[ -z "${chosen}" ]]; then
				continue 2
			fi

			# If more than one answer, make sure it is allowed
			if [[ ${maxchoices} -ne 1 ]]; then
				chosen="${chosen} ${foo}"

				# Get rid of any commas
				chosen="$(echo ${chosen} | awk -F',' '{ print $1 }')"

			elif [[ -n "${foo}" ]]; then
				printf "\n$(gettext SUNW_SC_INSTALL 'Please select one option.')\n\n\a"
				continue
			fi

			# Make sure that all options are valid
			for choice in ${chosen}
			do
				# Quit?
				if [[ "${choice}" == "${q}" ]] &&
				    [[ ${q_okay} -gt 0 ]]; then
					break 3
				fi

				# Next?
				if [[ "${choice}" == "${n}" ]] &&
				    [[ ${n_okay} -gt 0 ]]; then
					((i += ${line}))
					let newscreen=1
					continue 3
				fi

				# Previous?
				if [[ "${choice}" == "${p}" ]] &&
				    [[ ${p_okay} -gt 0 ]]; then
					((i -= ${linemax}))
					if [[ ${i} -lt 0 ]]; then
						let i=0
					fi
					let newscreen=1
					continue 3
				fi

				# Valid option?
				let err=0
				is_numeric "${choice}"
				if [[ $? -ne 0 ]]; then
					((err += 1))
				elif [[ ${choice} -lt 1 ]] ||
				    [[ ${choice} -gt ${num_options} ]]; then
					((err += 1))
				fi

				if [[ ${err} -gt 0 ]]; then
					if [[ ${maxchoices} -eq 1 ]]; then
						printf "\n$(gettext SUNW_SC_INSTALL 'Unrecognized option - \"%s\".')\n\n\a" ${choice}
						continue 2
					else
						printf "$(gettext SUNW_SC_INSTALL 'Ignoring unrecognized option - \"%s\".')\n\a" ${choice}
						continue
					fi
				fi

				# Make sure that it is not already given
				for foo in ${choice_list}
				do
					if [[ ${foo} == ${choice} ]]; then
						printf "$(gettext SUNW_SC_INSTALL 'Ignoring duplicate option - \"%s\".')\n\a" ${choice}
						continue 2
					fi
				done

				# Add it to the list of choices
				choice_list="${choice_list} ${choice}"
				((list_count += 1))

				# If we hit the maximum, we are done
				if [[ ${list_count} -eq ${maxchoices} ]]; then
					break 3
				fi
			done
		done
	done

	# Newline
	echo

	#
	# Print the first word, or "numwords" words, of each choice
	# from the list of choices
	#
	let first=0
	for choice in ${choice_list}
	do
		if [[ ${first} -ne 0 ]]; then
			echo " \c"
		fi
		set -A word_array ${option_array[(choice - 1)]}
		if [[ ${numwords} -lt 2 ]]; then
			echo "${word_array[0]}\c"
		else
			let i=0
			while [[ ${i} -lt ${numwords} ]]
			do
				if [[ ${i} -ne 0 ]]; then
					echo ":\c"
				fi
				echo "${word_array[i]}\c"
				((i += 1))
			done
		fi
		((first += 1))
	done >&5
	echo >&5

	# Done
	return 0
}

#####################################################
#
# is_numeric() value
#
#	Return 0 if the given value is numeric.
#
#	Return values:
#		0	- the "value" is numeric
#		1	- the "value" is not numeric
#	
#####################################################
is_numeric()
{
	typeset value=${1}

	# If "value" is not given, it is not numeric
	if [[ -z "${value}" ]]; then
		return 1
	fi

	# If not numeric, return 1
	if [[ $(expr "${value}" : '[0-9]*') -ne ${#value} ]]; then
		return 1
	fi

	# Numeric
	return 0
}

#####################################################
#
# is_ipv4_dot_notation() value
#
#	Return 0 if the given value is in a valid
#	V4 dot IP adddress format.
#
#	All four octets must be specified.
#
#	The first octet must be greater than 0.
#
#	Each octet must be less than 256.
#
#	Return values:
#		0	- the "value" is passes
#		1	- the "value" is not valid
#	
#####################################################
is_ipv4_dot_notation()
{
	typeset value=${1}

	typeset octets
	typeset octet

	integer i

	set -A octets $(echo ${value} | awk -F'.' '{ print $1 }')
	let i=$(set -- ${octets[*]};  echo $#)

	# There must be four octets
	if [[ ${i} -ne 4 ]]; then
		return 1
	fi

	# Each must be numeric and less than 256
	for octet in ${octets[*]}
	do
		is_numeric ${octet} || return 1
		if [[ ${octet} -gt 255 ]]; then
			return 1
		fi
	done

	# The first must not be zero
	if [[ ${octets[0]} -eq 0 ]];  then
		return 1
	fi

	# Okay
	return 0
}

#####################################################
#
# verify_isroot()
#
#	Print an error message and return non-zero
#	if the user is not root.
#
#####################################################
verify_isroot()
{
	# check for root
	if [[ $(id -u) -ne 0 ]]; then
		printf "$(gettext SUNW_SC_INSTALL '%s:  Must be root')\n" "${PROG}" >&2
		return 1
	fi

	return 0
}

####################################################
#
# sc_ipv4_dot_to_hex() ipv4_dot
#
#	ipv4_dot	- ipv4 address, in decimal dot notation
#
#	This function accepts an IPv4 address or netmask
#	in dot decimal notation and prints it's decimal value.
#
#	This function always returns zero.
#
####################################################
sc_ipv4_dot_to_hex()
{
	typeset ipv4_dot=${1}

	typeset octets

	integer i
	integer ipv4_hex

	# Convert the dot notation
	set -A octets $(IFS='.' ; set -- ${ipv4_dot};  echo $*)
	let i=0
	while [[ ${i} -lt 4 ]] 
	do
		if [[ -z "${octets[i]}" ]]; then
			octets[i]="0"
		fi
		((i += 1))
	done
	ipv4_hex=$((
		(${octets[0]} << 24) +
		(${octets[1]} << 16) +
		(${octets[2]} << 8) +
		 ${octets[3]}
	))

	# Print the result
	echo ${ipv4_hex}

	return 0
}

####################################################
#
# sc_ipv4_hex_to_dot() ipv4_hex
#
#	ipv4_hex	- ipv4 address, in hex format
#
#	This function accepts an IPv4 address or netmask
#	as a hex value and prints it value in decimal dot notation.
#
#	This function always returns zero.
#
####################################################
sc_ipv4_hex_to_dot()
{
	typeset -i16 ipv4_hex=${1}

	integer octet1
	integer octet2
	integer octet3
	integer octet4

	# Convert the hex address into decimal dot notation
	octet1=$(((ipv4_hex >> 24) & 0xFF))
	octet2=$(((ipv4_hex >> 16) & 0xFF))
	octet3=$(((ipv4_hex >> 8) & 0xFF))
	octet4=$((ipv4_hex & 0xFF))

	# Print the result
	echo "${octet1}.${octet2}.${octet3}.${octet4}"

	return 0
}


####################################################
#
# sc_first_subnet_ip() netaddr maxnodes
#
#	netaddr		- ipv4 address, in dotted decimal notation
#	maxnodes	- maximum number of possible nodes, user selected
#
#	This function accepts network address and max nodes and
#	prints the ip address of the last possible node the first subnet
#	under the given network address.
#
#	This function always returns 0.
#
####################################################
sc_first_subnet_ip()
{
	typeset netaddr=${1}
	typeset -i maxnodes=${2}

	typeset octets
	set -A octets $(IFS='.' ; set -- ${netaddr};  echo $*)

	typeset -i bits_nodes=1

	while [ $maxnodes -ge 2 ]
	do
		maxnodes=$((maxnodes / 2))
		bits_nodes=$((bits_nodes + 1))
	done

	typeset -i two_multiple=1
	typeset -i maxnodes_possible=1
	typeset -i bits_counter=${bits_nodes}
	while [ $bits_counter -gt 1 ]
	do
		two_multiple=$((two_multiple * 2))
		maxnodes_possible=$((maxnodes_possible + two_multiple))
		bits_counter=$((bits_counter - 1))
	done

	typeset -i final_two_multiple=$((two_multiple * 2))
	typeset -i subnet1_max_nodeid=$((maxnodes_possible + final_two_multiple - 1))

	typeset octet1=${octets[0]}
	typeset octet2=${octets[1]}
	typeset octet3=0
	typeset octet4=${subnet1_max_nodeid}
	typeset dot=.
	
	typeset subnet1_maxnode_ip=$octet1$dot$octet2$dot$octet3$dot$octet4

	echo $subnet1_maxnode_ip

	return 0 
}


####################################################
#
# sc_generate_netmask() maxnodes
#
#	maxnodes	- maximum number of possible nodes, user selected
#
#	This function accepts max nodes and generate the subnet mask
#	to mask the required bits to provide the range of addresses.
#
#	Calculates the number of bits required to hold the addresses and
#	sets remaining bits to all '1' for netmask calculation
#
#	This function always returns 0.
#
####################################################
sc_generate_netmask()
{
	typeset -i maxnodes=${1}

	typeset -i bits_nodes=1
	while [ $maxnodes -ge 2 ]
	do
		maxnodes=$((maxnodes / 2))
		bits_nodes=$((bits_nodes + 1))
	done

	typeset -i octet4_mask=0
	typeset -i two_dividend=128
	typeset -i loopcount=$((8 - bits_nodes))
	while [ $loopcount -gt 0 ]
	do
		octet4_mask=$((octet4_mask + two_dividend))
		two_dividend=$((two_dividend / 2))
		loopcount=$((loopcount - 1))
	done
	
	typeset netmask=255.255.255.${octet4_mask}

	echo ${netmask}

	return 0
}


####################################################
#
# sc_parse_ping_output() ping_output first_subnet_ip
#
#	ping_output	- 'ping -s -t 3 <broadcast>' output
#	first_subnet_ip	- ip calculated in the sc_first_subnet_ip() function
#
#	This function accepts ping output and subnet ip in order to
#	check whether the output has any other ip address other than
#	subnet ip mentioned above. If yes, then someone else is using
#	the same broadcast which results in duplicate subnet usage.
#
#	Prints DUP if duplicate, otherwise prints NOTDUP
#
#	This function always returns 0.
#
####################################################
sc_parse_ping_output()
{
	typeset ping_output=${1}
	typeset first_subnet_ip=${2}

	typeset output_array
	set -A output_array $(IFS=' '; set -- ${ping_output}; echo $*)

	typeset eachElem
	for eachElem in ${output_array[*]}
	do
		if [[ ${eachElem} != 'data' ]]; then
			if [[ $(sc_isValidSubnetIp ${eachElem} ${first_subnet_ip}) == 'TRUE' ]]; then
				set -A privIp $(IFS=: ; set -- ${eachElem}; echo $*)
				if [[ ${privIp[0]} != ${first_subnet_ip} ]]; then
					echo DUP
					return 0
				fi
			fi
		fi
	done
	
	echo NOTDUP

	return 0
}


####################################################
#
# sc_isValidSubnetIp() input_ip first_subnet_ip
#
#	input_ip	- ip being parsed from the ping output
#	first_subnet_ip	- ip calculated in the sc_first_subnet_ip() function
#
#	This function accepts an ip address and first_subnet_ip to
#	check whether the given ip is valid subnet ip in the same subnet
#	but is different from the first_subnet_ip.
#
#	Prints TRUE if valid and different, otherwise prints FALSE
#
#	This function always returns 0.
#
####################################################
sc_isValidSubnetIp()
{
	typeset input_ip=${1}
	typeset first_subnet_ip=${2}

	typeset input_ip_octets
	set -A input_ip_octets $(IFS='.'; set -- ${input_ip}; echo $*)

	typeset subnet_ip_octets
	set -A subnet_ip_octets $(IFS='.'; set -- ${first_subnet_ip}; echo $*)

	if [[ ${input_ip_octets[0]} -eq ${subnet_ip_octets[0]} ]] && [[ ${input_ip_octets[1]} -eq ${subnet_ip_octets[1]} ]] && [[ ${input_ip_octets[2]} -eq ${subnet_ip_octets[2]} ]]; then
		echo TRUE
	else
		echo FALSE
	fi

	return 0
}


####################################################
#
# sc_check_ipsubnet_conflict iflag
#
#	Check for the IP subnet conflict between the cluster nodes
#	(typically in an LDOMs setup).
#
#	iflag 0		- non-interactive mode of install & config
#	iflag 1		- establish new cluster w/ this machine
#	iflag 2		- add this machine to an established cluster
#	iflag 3		- custom JumpStart client set up
#	iflag 4		- centralized config
#
#	Logic in brief:
#		Use one of the non-used IP address from the subnet range
#	provided by the user as the local ip, assign netmask for the subnet
#	and try to ping the broadcast address. If ping gets response, 
#	someone else is using the subnet range and hence conflict. 
#		To arrive at the subnet ip to plumb, use the user selected
#	private network address and max nodes. Calculate the first subnet that
#	can be assigned to the first adapter and pick up the last possible ip
#	in the subnet (to make sure no one else is using it) to plumb on the
#	interfaces (user selects adapters before this screen).
#		To arrive at the subnet mask, use the max nodes value provided
#	by the user and calculate the max bits required to hold the ip address
#	range. The remaining bits set to '1' gives the subnet mask.
#		Use 'ifconfig <interface> plumb <subnet-ip> netmask <netmask>'
#	command to plumb the ip and try pinging the broadcast.
#	  
#	Return values:
#		0	- Success (ip subnet not duplicate)
#		1	- Failure (ip subnet duplicate)
#
####################################################
sc_check_ipsubnet_conflict()
{
	typeset iflag=${1}
	typeset LOCAL_NETADDR=${2}
	typeset LOCAL_MAXNODES=${3}
	typeset LOCAL_ADAPTERS

	typeset sctxt_check_dup="$(gettext 'Plumbing network address %s on adapter %s >> ')"

	typeset -i maxnodes_possible
	typeset first_subnet_ip
	typeset broadcast_octets
	typeset octet1
	typeset octet2
	typeset octet3
	typeset octet4
	typeset dot
	typeset subnet_broadcast
	typeset subnet_mask
	typeset my_tmp_file1
	typeset my_tmp_file2
	typeset adapters_string=${4}
	typeset -i ping_cnt
	typeset ping_output
	typeset parse_ret
	typeset return_func

	# Nothing to do for jumpstart
	if [[ ${iflag} -eq 3 ]]; then
		return 0
	fi

	# This function is being called both in Interactive & Non-interactive mode of scinstall
	# setting this global flag to 'yes' will avoid the function to be called twice
	IS_IPCONFLICT_CHECK_DONE="yes"

	# maxnodes plus localhost and broadcast
	maxnodes_possible=$((${LOCAL_MAXNODES} + 2))

	# get the maximum ip that can be assigned for the first subnet
	first_subnet_ip=$(sc_first_subnet_ip ${LOCAL_NETADDR} ${maxnodes_possible})

	set -A broadcast_octets $(IFS='.'; set -- ${first_subnet_ip}; echo $*)

	octet1=${broadcast_octets[0]}
	octet2=${broadcast_octets[1]}
	octet3=${broadcast_octets[2]}
	octet4=$((${broadcast_octets[3]} + 1))
	dot=.

	# first subnet ip plus 1 gives the broadcast address
	subnet_broadcast=$octet1$dot$octet2$dot$octet3$dot$octet4

	# bits to hold max nodes if set to 0, gives the netmask 
	subnet_mask=$(sc_generate_netmask ${maxnodes_possible})

	my_tmp_file1=${tmpfile2}.mytmpfile1
	my_tmp_file2=${tmpfile2}.mytmpfile2

	set -A LOCAL_ADAPTERS $(IFS=' '; set -- ${adapters_string}; echo $*)

	# loop through the user-selected network adapters to plumb the ip
	for adapter in ${LOCAL_ADAPTERS[*]}
	do
		# handling indentation for displaying messages in interactive & non-interactive modes
		if [[ $iflag -ne 0 ]]; then
			printf "    ${sctxt_check_dup}" "${LOCAL_NETADDR}" "${adapter}" | logmsg
		else
			printf "${sctxt_check_dup}" "${LOCAL_NETADDR}" "${adapter}" | logmsg
		fi

		# plumb the ip & netmask to the interface
		ifconfig ${adapter} plumb ${first_subnet_ip} netmask ${subnet_mask} 2>/dev/null
		ifconfig ${adapter} up 2>/dev/null
		sleep 1
	
		# pinging the broadcast - ping 2 times with an interval of 2 secs for each packet
		ping_cnt=2
		while [ ${ping_cnt} -gt 0 ]
		do
			ping -s -t 3 ${subnet_broadcast} > ${my_tmp_file1} 2>/dev/null &
			sleep 4
			pkill ping 2>/dev/null

			# collecting ping output, if any other node responds then conflict
			cat ${my_tmp_file1} | $AWK '{ print $4 }' > ${my_tmp_file2}
			ping_output=`cat ${my_tmp_file2} | grep -v ${first_subnet_ip}`

			# remove the tmp files
			rm -rf ${my_tmp_file1} 2>/dev/null
			rm -rf ${my_tmp_file2} 2>/dev/null

			# get whether DUP or NOTDUP
			parse_ret=$(sc_parse_ping_output "${ping_output}" ${first_subnet_ip})

			if [[ $parse_ret == 'DUP' ]]; then
				break
			fi
			
			ping_cnt=$((ping_cnt - 1))
		done

		# unplumb the interfaces in any case
		ifconfig ${adapter} down 2>/dev/null
		ifconfig ${adapter} unplumb 2>/dev/null

		if [[ $parse_ret == 'DUP' ]]; then
			# handling indentation for displaying messages in interactive & non-interactive modes
			if [[ $iflag -ne 0 ]]; then
				echo "DUPLICATE ... failed" | logmsg
			else
				printf "DUPLICATE ... failed\n" | logmsg
			fi
		
			IPSUBNET_DUPLICATE="DUP "${adapter}
			return 1
		else
			# handling indentation for displaying messages in interactive & non-interactive modes
			if [[ $iflag -ne 0 ]]; then
				echo "NOT DUPLICATE ... done" | logmsg
			else
				printf "NOT DUPLICATE ... done\n" | logmsg
			fi
		fi
	done

	IPSUBNET_DUPLICATE="NOTDUP"
	return 0
}


####################################################
#
# sc_netmask_min netaddr_hex
#
#	netaddr_hex	- network address
#
#	Print the minimum netmask, without holes, required to
#	support the given network address.
#
#	This function always returns zero.
#
####################################################
sc_netmask_min()
{
	integer netaddr_hex=${1}

	integer min_netmask_hex
	integer count

	# Check argument
	if [[ ${netaddr_hex} -eq 0 ]]; then
		echo 0
		return 0
	fi

	# Get the maxinum number of possible host bits
	let count=0
	while ((! (netaddr_hex & 1)))
	do
		((netaddr_hex >>= 1))
		((count += 1))
	done

	# Generate the minimum netmask
	let min_netmask_hex=$((0xFFFFFFFF >> count))
	let min_netmask_hex=$((min_netmask_hex << count))

	# Print the result
	echo ${min_netmask_hex}

	# Done
	return 0
}

####################################################
#
# sc_netmask_check netmask_hex
#
#	netmask_hex	- netmask
#
#	Make sure that the given netmask is not too small and
#	has no holes.
#
#	Return values:
#		0	- success
#		1	- failure
#
####################################################
sc_netmask_check()
{
	integer netmask_hex=${1}

	integer min_netmask_hex
	integer holes

	# If netmask is too small, we are done
	if ((netmask_hex < 0x80000000));  then
		return 1
	fi

	let min_netmask_hex=$(sc_netmask_min ${netmask_hex})
	let holes=$((netmask_hex^min_netmask_hex))

	# If netmask has holes, return error
	if [[ ${holes} -ne 0 ]]; then
		return 1
	fi

	# Netmask is okay
	return 0
}

####################################################
#
# sc_netmask_generate_private() maxnodes maxprivnets
#
#	maxnodes	- maximum number of nodes
#	maxprivnets	- maximum number of private networks
#
#	Print the netmask, in IPv4 dot notation, required to support
#	the given number of maximum nodes and maximum private networks.
#	If "maxnodes" is greater than SC_PNET_MAX_MAXNODES,
#	SC_PNET_MAX_MAXNODES is used.   And, if "maxprivnets" is greater than
#	SC_PNET_MAX_MAXPRIVATENETS, SC_PNET_MAX_MAXPRIVATENETS is used.
#
#	If only "maxnodes" is given, generate a netmask
#	based only on "maxnodes".
#
#	Return values:
#		0	- success
#		1	- failure;  nothing is printed
#
####################################################
sc_netmask_generate_private()
{
	typeset maxnodes=${1}
	typeset maxprivnets=${2}
	typeset zc_cnt=${3}

	integer i
	integer x
	typeset -i16 nodemask
	typeset -i16 netmask

	# Check arguments
	if [[ -z "${maxnodes}" ]]; then
		return 1
	fi
	is_numeric ${maxnodes} || return 1

	# Check for maximum maxnodes
	if [[ ${maxnodes} -gt ${SC_PNET_MAX_MAXNODES} ]]; then
		maxnodes=${SC_PNET_MAX_MAXNODES}
	fi


	# Generate a netmask based on max nodes
	let nodemask=$((0xFFFFFFFF))
	let i=$((maxnodes + 2))
	let x=1
	while [[ ${x} -lt ${i} ]]
	do
		nodemask=$((nodemask << 1))
		x=$((x << 1))	
	done

	# If "maxprivnets" was not given, return a mask based on "maxnodes"
	if [[ -z "${maxprivnets}" ]]; then
		sc_ipv4_hex_to_dot ${nodemask}
		return 0
	fi

	# Check for maximum maxprivnets
	is_numeric ${maxprivnets} || return 1
	if [[ ${maxprivnets} -gt ${SC_PNET_MAX_MAXPRIVATENETS} ]]; then
		maxprivnets=${SC_PNET_MAX_MAXPRIVATENETS}
	fi

	# Factor in max private nets
	let netmask=$((0xFFFFFFFF))
	if [[ -z $zc_cnt ]]; then
		let i=$((((4 * (maxprivnets + 2) / 3)) * (~nodemask + 1 )))
	else
		let i=$((((4 * (maxprivnets + 2) / 3) + $zc_cnt) * (~nodemask + 1 )))
	fi
		
	let x=1
	while [[ ${x} -lt ${i} ]]
	do
		netmask=$((netmask << 1))
		x=$((x << 1))
	done

	# Print the netmask in decimal dot notation
	sc_ipv4_hex_to_dot ${netmask}

	# Done
	return 0
}

#######################################################
#
# sc_print_ccr_node_count()
#
#	Print the node count directly from the CCR file.
#
#	This function always returns zero.
#
#######################################################
sc_print_ccr_node_count()
{
	typeset configfile=/etc/cluster/ccr/global/infrastructure

	grep -c '^cluster\.nodes\.[0-9]*\.name' ${configfile} 2>/dev/null

	return 0
}

#######################################################
#
# sc_print_ccr_privnet_count()
#
#	Print a count of the number of privatenets directly
#	from the CCR file.
#
#	This function always returns zero.
#
#######################################################
sc_print_ccr_privnet_count()
{
	typeset configfile=/etc/cluster/ccr/global/infrastructure

	integer switch_count

	switch_count=$(grep -c '^cluster\.blackboxes\.[0-9]*\.name' ${configfile} 2>/dev/null)

	# If there are no switches, this must be direct connect
	if [[ ${switch_count} -eq 0 ]]; then
		grep -c '^cluster\.cables\.[0-9]*\.properties.end1' ${configfile} 2>/dev/null
	else
		echo ${switch_count}
	fi

	return 0
}

#######################################################
#
# sc_print_ccr_zc_count()
#
#	Print the number of zone clusters from the CCR file.
#
#	This function always returns zero.
#
#######################################################
sc_print_ccr_zc_count()
{
	typeset configfile=/etc/cluster/ccr/global/infrastructure

	integer zc_count

	zc_count=$(cat $configfile | grep "zoneclusters" | $AWK '{print $2}')
	echo $zc_count

	return 0
}

#######################################################
#
# sc_is_ips
#
#       Check if the cluster is installed in IPS packages.
#
#	Return values:
#		0	- Installed in IPS
#		1	- Not in IPS package
#
#######################################################
sc_is_ips()
{
	typeset SC_BASEDIR=${SC_BASEDIR:-}

	if [[ -f "${SC_BASEDIR}/${IPS_FLAG_FILE}" ]]; then
		# In IPS packages
		return 0
	else
		return 1
	fi

}

#######################################################
#
# lib_test_java_version
#
#      Locate an appropriate version of java.
#       Major & minor defined in /etc/default/sccheck
#       'dot-dot' not considered
#
#       Returns values:
#		0	- Find appropriate version
#		1	- Find not java version
#
#######################################################
lib_test_java_version()
{
	typeset MIN_JAVA_MAJOR_VER=${MIN_JAVA_MAJOR_VER:-1}
	sc_is_ips
	if [[ $? -eq 0 ]]; then
		typeset MIN_JAVA_MINOR_VER=${MIN_JAVA_MINOR_VER:-6}
	else
		typeset MIN_JAVA_MINOR_VER=${MIN_JAVA_MINOR_VER:-5}
	fi
	typeset -r MIN_JAVA_VER=${MIN_JAVA_MAJOR_VER}.${MIN_JAVA_MINOR_VER}

	# user can override via /etc/default/ config file
	typeset JAVA_HOME=${JAVA_HOME:-/usr/java}
	typeset JAVA=${JAVA:-${JAVA_HOME}/bin/java}

	# Map to a number we can do a numerical comparison on.
	weWant=`versionString2Num $MIN_JAVA_VER`
	weHave=$(gettext 'no java found')
	versionsFound=""

	# JAVA_HOME environment variable overrides everything
	JAVA_LOC="${JAVA_HOME:+${JAVA_HOME}/bin/java}"

	# Followed by along PATH and the standard location.
	javaOnPath=`which java 2>&1`
	if [ $? -eq 0 ]; then
		JAVA_LOC="$JAVA_LOC ${javaOnPath} ${DEFAULT_JAVA}"
	else
		JAVA_LOC="$JAVA_LOC ${DEFAULT_JAVA}"
	fi

	JAVA_HOME=""
	for i in ${JAVA_LOC}
	do
		prog=`resolve_link ${i}`
		if [ -x ${prog} ]; then
			# Get version, and map to a number we can do a
			# numerical comparison on.
			version=`${i} -version 2>&1 | head -1`
			version=`echo $version | $AWK '{print $3}' | sed -e "s/\"//g"`
			if [ -z "${versionsFound}" ]; then
				versionsFound=${version}
			else
				versionsFound="${versionsFound}, ${version}"
			fi

			weHave=`versionString2Num $version`

			if [ ${weHave} -ge ${weWant} ]; then
				JAVA_HOME=`dirname \`dirname ${prog}\``
				export JAVA_HOME
				break
			fi
		fi
	done

	sc_is_ips
	if [[ $? -ne 0 ]]; then
		#
		# For IPS installation, Java version is checked above by checking
		# JAVA_HOME and /usr/java etc. Now check for SVR4 package.
		# It is possible we didn't find one if it was installed in a non-default
		# location and the user neglected to set JAVA_HOME or include it along
		# PATH.  In this case, we try to derive it by querying the pkg database.
		#
		if [ ! -n "${JAVA_HOME}" ]; then
			jdk=SUNWj6rt
			/usr/bin/pkginfo $jdk >/dev/null 2>&1
			if [ "$?" -eq 0 ]; then
				# Get version, and map to a number we can do a
				# numerical comparison on.
				version=`env LANG=C LC_ALL=C /usr/bin/pkgparam $jdk SUNW_PRODVERS \
				    | $AWK -F/ '{print $1}'`
				if [ -z "${versionsFound}" ]; then
					versionsFound=${version}
				else
					versionsFound="${versionsFound}, ${version}"
				fi
				weHave=`versionString2Num $version`

				if [ ${weHave} -ge ${weWant} ]; then
					j2basedir=`env LANG=C LC_ALL=C /usr/bin/pkgparam ${jdk} BASEDIR`
					JAVA_HOME=${j2basedir}/jdk/jdk${version}
					export JAVA_HOME
				fi
			fi
		fi

		if [ ! -n "${JAVA_HOME}" ]; then
			jdk=SUNWj5rt
			/usr/bin/pkginfo $jdk >/dev/null 2>&1
			if [ "$?" -eq 0 ]; then
				# Get version, and map to a number we can do a
				# numerical comparison on.
				version=`env LANG=C LC_ALL=C /usr/bin/pkgparam $jdk VERSION \
				    | $AWK -F, '{print $1}'`
				if [ -z "${versionsFound}" ]; then
					versionsFound=${version}
				else
					versionsFound="${versionsFound}, ${version}"
				fi
				weHave=`versionString2Num $version`

				if [ ${weHave} -ge ${weWant} ]; then
					j2basedir=`env LANG=C LC_ALL=C /usr/bin/pkgparam ${jdk} BASEDIR`
					JAVA_HOME=${j2basedir}/j2se
					export JAVA_HOME
				fi
			fi
		fi
	fi

	if [ ! -n "${JAVA_HOME}" ]; then
		printf "$(gettext 'java version(s) found: %s. Must be %s or newer.')\n" "${versionsFound}" ${MIN_JAVA_VER}
		return 1
	else
		JAVA=${JAVA_HOME}/bin/java
		return 0
	fi
}

########################################################
#
# versionString2Num
#
# 	Convert a java version string to an int
#	that may be compared to other ints.
#
# (Lifted from Lockhart: /usr/sbin/smcwebserver 2.2.1)
#
########################################################
versionString2Num()
{
	# Minor and micro default to 0 if not specified.
	major=`echo $1 | $AWK -F. '{print $1}'`
	minor=`echo $1 | $AWK -F. '{print $2}'`
	if [ ! -n "$minor" ]; then
		minor="0"
	fi
	micro=`echo $1 | $AWK -F. '{print $3}'`
	if [ ! -n "$micro" ]; then
		micro="0"
	fi

	#
	# The micro version may further be extended to include a patch number.
	# This is typically of the form <micro>_NN, where NN is the 2-digit
	# patch number.  However it can also be of the form <micro>-XX, where
	# XX is some arbitrary non-digit sequence (eg., "rc").  This latter
	# form is typically used for internal-only release candidates or
	# development builds.
	#
	# For these internal builds, we drop the -XX and assume a patch number
	# of "00".  Otherwise, we extract that patch number.
	#
	patch="00"
	dash=`echo $micro | grep "-"`
	if [ $? -eq 0 ]; then
		# Must be internal build, so drop the trailing variant.
		micro=`echo $micro | $AWK -F- '{print $1}'`
	fi

	underscore=`echo $micro | grep "_"`
	if [ $? -eq 0 ]; then
		# Extract the seperate micro and patch numbers, ignoring anything
		# after the 2-digit patch.
		patch=`echo $micro | $AWK -F_ '{print substr($2, 1, 2)}'`
		micro=`echo $micro | $AWK -F_ '{print $1}'`
	fi

	echo "${major}${minor}${micro}${patch}"
}

####################################################
#
# resolve_link
#
#	Resolve a link to it's target.
#
####################################################
resolve_link()
{
	prg=$1

	if [ -h "${prg}" ]; then
		# Must cd to where the symbolic link is located as a starting point
		# to follow the link.
		cd `dirname ${prg}`

		# Resolve link to conclusion
		while [ -h "${prg}" ]
		do
			prg=`ls -al ${prg} | $AWK '{print $NF}'`
			cd `dirname ${prg}`
		done

		# Resolve to full path, in case it's relative.
		prg=`pwd`/`basename ${prg}`
	fi

	echo $prg
}

#####################################################
#
# test_ips_install()
#
#	Return 1 if the software was installed via IPS
#	packaging system is else return 0.
#
#	Return values map to SC_TRUE & SC_FALSE
#	in scinstall scripts.
#####################################################
test_ips_install()
{
	/usr/bin/pkg list ${IPS_PKG_MINIMAL} > /dev/null 2>&1
	if [[ $? -eq 0 ]]; then
	    return 0
	fi
	
	printf "$(gettext SUNW_SC_INSTALL '%s: %s is not installed.')\n" ${PROG} ${IPS_PKG_MINIMAL} >&2

	return 1
} # test_ips_install

#####################################################
#
#	dir_clone <src_dir> <dest_dir>
#
#	make a clone of src_dir via piped tars
#	symlinks are preserved; timestamps are not
#
#	always return 0
#
#####################################################
dir_clone() {
    SRC=$1
    DEST=$2
    
    /usr/bin/mkdir -p $DEST
    pwd=`pwd`
    cd ${SRC}; /usr/bin/tar cf - . | (cd ${DEST};  /usr/bin/tar xf -)
    cd ${pwd}

} # dir_clone
