#! /usr/xpg4/bin/sh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident "@(#)cmd_autodiscovery.ksh	1.6	08/08/14 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# cmd_autodiscovery <vlans> <token> <adaptercount> <timeout>
#
#	Possible exit codes are the same as for "scrconf -n"
#
#####################################################

#####################################################
#
# Constant Globals
#
#####################################################

# Program name
typeset -r PROG=${0##*/}

# Files
typeset -r SC_DEFAULTS=/usr/cluster/lib/scadmin/defaults

# Set the PATH
typeset -r SC_BINDIR=/usr/cluster/bin
typeset -r SC_BINDIRS=${SC_BINDIR}:/usr/cluster/lib/sc
PATH=${SC_BINDIRS}:/bin:/usr/bin:/sbin:/usr/sbin; export PATH

# I18N
typeset -x TEXTDOMAIN=TEXT_DOMAIN
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

# Other
typeset -r mynodename=$(uname -n)


#####################################################
#
# Variables which will be set from the scinstall defaults file
#
#####################################################
typeset SC_DFLT_KNOWN_ETHERNET_ADAPTERS
typeset SC_DFLT_KNOWN_INFINIBAND_ADAPTERS

#####################################################
#
# print_usage()
#
#	Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "$(gettext 'usage'):  ${PROG} <vlans> <token> <adaptercount> <timeout>" >&2
}

#####################################################
#
# Main
#
#####################################################
main()
{
	typeset vlans=${1}
	typeset token=${2}
	typeset adaptercount=${3}
	typeset recvto=${4}

	typeset available_ladapters	# all available local adapters
	typeset arg_adaptypes
	typeset adaptype
	typeset adapter
	typeset ladapterlist		# colon seperated list - receive probe

	typeset known_autodisc_adaptypes	# List of all adapter types that
						# autodiscovery supports

	integer result

	# Check usage
	if [[ $# -ne 4 ]]; then
		print_usage
		return 1
	fi

	# Set the list of known adapter types
	known_autodisc_adaptypes=
	. ${SC_DEFAULTS}

	# Support autodiscovery for both Ethernet and Infiniband
	autodisc_list="${SC_DFLT_KNOWN_ETHERNET_ADAPTERS} ${SC_DFLT_KNOWN_INFINIBAND_ADAPTERS}"

	for adaptype in ${autodisc_list}
	do
                if [[ -z "${known_autodisc_adaptypes}" ]]; then
                        known_autodisc_adaptypes=${adaptype}
                else
                        known_autodisc_adaptypes=${known_autodisc_adaptypes}:${adaptype}
                fi
        done

	# Set the adaptypes argument for use w/ scrconf
	arg_adaptypes=
	if [[ -n "${known_autodisc_adaptypes}" ]]; then
		arg_adaptypes=",adaptypes=${known_autodisc_adaptypes}"
	fi

	# Get the list of available adapters on this machine
	available_ladapters="$(scrconf -n cmd=print_adapters_available${arg_adaptypes})"
	if [[ $? -ne 0 ]]; then
		printf "$(gettext 'Cannot get list of available adapters on \"%s\".')\n" "${mynodename}" >&2
		return 1
	fi

	# No available adapters
	if [[ -z "${available_ladapters}" ]]; then
		printf "$(gettext 'There are no available adapters on \"%s\".')\n" "${mynodename}" >&2
		return 1
	fi

	# Only one available adapter?
	let i=$(set -- ${available_ladapters};  echo $#)
	if [[ ${i} -eq 1 ]]; then
		printf "$(gettext '\"%s\" is the only available adapter on \"%s\".')\n" "${available_ladapters}" "${mynodename}" >&2
		return 1
	fi

	# Set ladapterlist
	ladapterlist=
	for adapter in ${available_ladapters}
	do
                if [[ -z "${ladapterlist}" ]]; then
                        ladapterlist=${adapter}
                else
                        ladapterlist=${ladapterlist}:${adapter}
                fi
        done

	# Look for incoming probes on this machine
	scrconf -n cmd=discover_receive,adapters=${ladapterlist},vlans=${vlans},token=${token},recvto=${recvto},nowait,waitcount=${adaptercount}
	result=$?

	# Done
	return ${result}
}

	main $*
	exit $?
