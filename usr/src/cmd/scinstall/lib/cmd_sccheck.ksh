#! /usr/xpg4/bin/sh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident "@(#)cmd_sccheck.ksh	1.5	08/08/14 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# cmd_sccheck  (updated to run 'cluster check')
#
#	Run 'cluster check'.
#
#	Possible exit codes are the same as for "cluster check"
#
#####################################################

#####################################################
#
# Constant Globals
#
#####################################################

# Program name
typeset -r THIS_PROG=${0##*/}

#
# Reset PROG. $PROG is exported in scinstall, and is also
# overwriten in 'cluster check'. Reset it so 'cluster check' will print
# the right program name.
#
PROG= 

# Set the PATH
typeset -r SC_BINDIR=/usr/cluster/bin
typeset -r SC_BINDIRS=${SC_BINDIR}:/usr/cluster/lib/sc
PATH=${SC_BINDIRS}:/bin:/usr/bin:/sbin:/usr/sbin; export PATH

# Log file directories
typeset -r SC_LOGDIR=/var/cluster/logs/install
typeset -r SC_CLCHECK_RPTDIR=${SC_LOGDIR}/cluster_check

typeset -r SC_CLCHECK_EXITCODELOG=${SC_CLCHECK_RPTDIR}/cluster_check_exit_code.log
typeset -r SC_CLCHECK="cluster check -X -k installtime -v -o ${SC_CLCHECK_RPTDIR}"
	# cluster check: no cacao; installtime keyword only; verbose; specified output dir

# I18N
typeset -x TEXTDOMAIN=TEXT_DOMAIN
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

#####################################################
#
# print_usage()
#
#       Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "$(gettext 'usage'):  ${THIS_PROG}" >&2
}  

#####################################################
#
# Main
#
#####################################################
main()
{
	# Check arguments
	if [[ $# -ne 0 ]]; then
		print_usage
		return 1
	fi

	# Remove any already existing log file directory
	rm -rf ${SC_CLCHECK_RPTDIR}

	# Run cluster check
	echo "${SC_CLCHECK}"
	eval  ${SC_CLCHECK} 2>&1
	status=$? # program return value
	exitcode=`cat ${SC_CLCHECK_EXITCODELOG}` # written to file by cluster check

	# Print the report
	cat ${SC_CLCHECK_RPTDIR}/*.txt 2>/dev/null

	# whip up a sensible return code for scinstall
	# status non-0 and exitcode should never overlap
	if [[ ${status} -ne 0 ]]; then
	    # cmd returned error
	    return ${status}
	elif [[ ${exitcode} -eq 100 ]]; then
	    # no error and no violated checks
	    return 0
	else
	    # no error but violated checks
	    return ${exitcode}
	fi
}

	main $*
	exit $?
