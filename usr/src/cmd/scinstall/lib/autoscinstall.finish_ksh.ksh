#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)autoscinstall.finish_ksh.ksh	1.16	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

umask 022

#####################################################
#
# Constants and globals
#
#####################################################
PATH=/usr/bin:/bin:/etc:/sbin:/usr/sbin; export PATH
PROG=$(basename $0)

typeset -r SC_AUTOSCINSTALL_D=autoscinstall.d
typeset -r SC_AUTOSCCONFIGDIR=${SC_AUTOSCINSTALL_D}/nodes
typeset -r SC_AUTOSCHOSTDIR=${SI_CONFIG_DIR}/${SC_AUTOSCCONFIGDIR}/${SI_HOSTNAME}
typeset -r SC_AUTOSCDATA=autoscinstall.data
typeset -r SC_AUTOINSTALL_PATCHLIST=patchlist
typeset -r SC_AUTOINSTALL_PATCHLIST_SOLARIS=patchlist.solaris
typeset -r SC_AUTOINSTALL_PATCHLIST_SUNCLUSTER=patchlist.suncluster

typeset SC_SCLIBDIR
typeset -r SC_LIB_SC=sc_common
typeset -r SC_LIB_COMMON=scinstall_common
typeset -r SC_LIB_ARGVARS=scinstall_argvars

typeset -x SC_BASEDIR=/a

typeset -r autoscinstalldir=/a/autoscinstalldir
typeset autoscinstalltool

typeset -r TMP_PATCHMNTPT=/tmp/patchmntpt.$$
typeset PATCH_OPT
typeset PRINT_PATCH_OPT

typeset -r SC_PATCH_CMD=/usr/sbin/patchadd
typeset patchlist=
typeset cmd=
typeset file=

integer skip_patching

# TRANSLATION_NOTE
#
# "done" is used to denote the successful completion of a task.
# e.g., when a task starts the user will see:
#
#     Initializing cluster name to "boston" ... 
#
# and when the task successfully completes the user will see:
#
#     Initializing cluster name to "boston" ... done
#
# SC_DONE is used to eliminate the task of having to substitute this
# word throughout this command's functions.
#
typeset -r SC_DONE=$(gettext 'done')

# TRANSLATION_NOTE
#
# "failed" is used to denote the unsuccessful completion of a task.
# e.g., when a task starts the user will see:
#
#     Initializing cluster name to "boston" ... 
#
# or if the task did not complete the user will see:
#
#     Initializing cluster name to "boston" ... failed
#
# SC_FAILED is used to eliminate the task of having to substitute this
# word throughout this command's functions.
#
typeset -r SC_FAILED=$(gettext 'failed')

#####################################################
#
# cleanup() [exitstatus]
#
#####################################################
cleanup()
{
	integer exitstatus=$1

	if [[ -z "$1" ]]; then
		let exitstatus=0
	fi

	if [[ -d ${autoscinstalldir} ]]; then
		umount ${autoscinstalldir}
		rmdir ${autoscinstalldir}
	fi

	if [[ -d ${TMP_PATCHMNTPT} ]]; then
		umount ${TMP_PATCHMNTPT}
		rmdir ${TMP_PATCHMNTPT}
	fi

	exit ${exitstatus}
}

#####################################################
#
# main
#
#####################################################
printf "\n$(gettext 'Performing setup for Sun Cluster autoinstall ... ')"

if [[ ! -f "${SC_AUTOSCHOSTDIR}/${SC_AUTOSCDATA}" ]]; then
	printf "%s\n\n" ${SC_FAILED}
	printf "$(gettext '%s:  \"%s\" not found')\n" "${PROG}" "${SC_AUTOSCCONFIGDIR}/${SC_AUTOSCDATA}"
	cleanup 1
fi

#
# The autohaconfig file is used to set certain
# environment variables used by this script.
#
. ${SC_AUTOSCHOSTDIR}/${SC_AUTOSCDATA}
if [[ $? -ne 0 ]]; then
	printf "%s\n\n" ${SC_FAILED}
	printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" "${SC_AUTOSCDATA}"
	cleanup 1
fi

#
# Concatenate the hosts file
#
if [[ -s "${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_ARCHIVE}/etc/inet/hosts" ]]; then
	cat "${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_ARCHIVE}/etc/inet/hosts" >>/etc/inet/hosts
fi

#
# Make sure that an installhost and installdir are specified.
# The installdir holds all of the cdrom images needed to
# complete the HA install.
#
if [[ -z "${SC_AUTOINSTALL_HOST}" ]] ||
    [[ -z "${SC_AUTOINSTALL_DIR}" ]]; then
	printf "%s\n\n" ${SC_FAILED}
	printf "$(gettext '%s:  Bad \"%s\" file')\n" "${PROG}" "${SC_AUTOSCDATA}"
	cleanup 1
fi

#
# The installdir must be an absolute path
#
if [[ "${SC_AUTOINSTALL_DIR}" != /* ]]; then
	printf "%s\n\n" ${SC_FAILED}
	printf "$(gettext '%s:  Bad \"%s\" in \"%s\"')\n" "${PROG}" "SC_AUTOINSTALL_DIR" "${SC_AUTOSCDATA}"
	cleanup 1
fi

#
# Create the mount point for our installdir ...
#
mkdir -m 0755 ${autoscinstalldir}
if [ $? -ne 0 ]; then
	printf "%s\n\n" ${SC_FAILED}
	printf "$(gettext '%s:  Cannot create \"%s\"')\n" "${PROG}" "${autoscinstalldir}"
	cleanup 1
fi

#
# ... and, nfs mount it.
#
mount -F nfs -o ro ${SC_AUTOINSTALL_HOST}:${SC_AUTOINSTALL_DIR} ${autoscinstalldir}
if [[ $? -ne 0 ]]; then
	printf "%s\n\n" ${SC_FAILED}
	printf "$(gettext '%s:  Failed to mount \"%s:%s\"')\n" "${PROG}" "${SC_AUTOINSTALL_HOST}" "${SC_AUTOINSTALL_DIR}"
	cleanup 1
fi


#
# Set pointer to the Tools directory and SC_SCLIBDIR
#
autoscinstalltools=${autoscinstalldir}/${SC_AUTOINSTALL_TOOLSDIR}
SC_SCLIBDIR=${autoscinstalltools}/lib

#
# Load the libraries which will turn our SC_ARGVAR_ variables from
# SC_AUTOSCDATA into command line options for scinstall.
#
for lib in ${SC_LIB_SC} ${SC_LIB_COMMON} ${SC_LIB_ARGVARS}
do
	. ${SC_SCLIBDIR}/${lib}
	if [[ $? -ne 0 ]]; then
		printf "%s\n\n" ${SC_FAILED}
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" "${SC_SCLIBDIR}/${lib}"
		cleanup 1
	fi
done

# Translate the patch directory specific options, if specific patch directory
# had been specified, ignore stuff in the default "patches" directory.
# Also, to make debugging easier, we won't talk about the temporary directory
# in the logs.

if [[ -n "${SC_AUTOINSTALL_PATCHHOST}" ]]; then

	SC_AUTOINSTALL_PATCHES=
	skip_patching=0

	mkdir -m 0755 ${TMP_PATCHMNTPT}
	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s:  WARNING:  Cannot create \"%s\"')\n" "${PROG}" "${TMP_PATCHMNTPT}"
		printf "\n"
		skip_patching=1
	fi

	if [[ ${skip_patching} -eq 0 ]]; then
		mount -F nfs -o ro ${SC_AUTOINSTALL_PATCHHOST}:${SC_AUTOINSTALL_PATCHDIR} ${TMP_PATCHMNTPT}
		if [[ $? -ne 0 ]]; then
			printf "$(gettext '%s:  WARNING:  Failed to mount \"%s:%s\"')\n" "${PROG}" "${SC_AUTOINSTALL_PATCHHOST}" "${SC_AUTOINSTALL_PATCHDIR}"
			printf "\n"
			skip_patching=1
		fi
	fi

	if [[ ${skip_patching} -eq 0 ]]; then
		if [[ -n ${SC_ARGVAR_PATCHFILE} ]]; then
			PATCH_OPT="-M patchdir=${TMP_PATCHMNTPT},patchlistfile=${SC_ARGVAR_PATCHFILE}"
			PRINT_PATCH_OPT="-M patchdir=${SC_ARGVAR_RESPONSE_PATCHDIR},patchlistfile=${SC_ARGVAR_PATCHFILE}"
		else
			PATCH_OPT="-M patchdir=${TMP_PATCHMNTPT}"
			PRINT_PATCH_OPT="-M patchdir=${SC_ARGVAR_RESPONSE_PATCHDIR}"
		fi
	else
		printf "$(gettext '%s:  WARNING:  Patches from \"%s:%s\" will not be installed')\n" "${PROG}" "${SC_AUTOINSTALL_PATCHHOST}" "${SC_AUTOINSTALL_PATCHDIR}"
		printf "\n"
	fi
		
	# Since we translate the patch options for scinstall -i here itself
	# we don't want the argvar routine to do it again
	SC_ARGVAR_RESPONSE_PATCHDIR=
fi	

#
# Done performing setup for autoinstall
#
printf "%s\n\n" ${SC_DONE}

#
# Autoinstall anything in the archive
#
if [[ -n "${SC_AUTOINSTALL_ARCHIVE}" ]] &&
    [[ -d "${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_ARCHIVE}" ]]; then
	printf "\n"
	printf "\n$(gettext 'Installing archive ...')\n\n"
	(
	 cd ${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_ARCHIVE}
	 find . ! -type d -depth -print | cpio -pduv /a | grep -v '/\.$'
	)
	printf "\n"
fi

#
# Autoinstall any Solaris patches in the patches directory.
# If there are any patchlist.* files, use only those.  Note
# that if patchlist.suncluster exists, but patchlist.solaris
# does not, no Solaris patches are installed.  "patchlist" may
# be used in place of "patchlist.solaris".
#
if [[ -n "${SC_AUTOINSTALL_PATCHES}" ]] &&
    [[ -d "${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_PATCHES}" ]]; then

	# Initialize the patchlist
	patchlist=

	# If there is a "patchlist" file, use that
	if [[ -s ${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_PATCHES}/${SC_AUTOINSTALL_PATCHLIST} ]]; then
		patchlist=${SC_AUTOINSTALL_PATCHLIST}
	fi

	# Or, if there is a "patchlist.solaris" file, use that instead
	if [[ -s ${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_PATCHES}/${SC_AUTOINSTALL_PATCHLIST_SOLARIS} ]]; then
		if [[ -n "${patchlist}" ]]; then
			printf "\n"
			printf "$(gettext '%s:  WARNING:  found both \"%s\" and \"%s\"')\n" "${PROG}" "${SC_AUTOINSTALL_PATCHLIST}" "${SC_AUTOINSTALL_PATCHLIST_SOLARIS}"
			printf "$(gettext '%s:  WARNING:  ignoring \"%s\"')\n" "${PROG}" "${SC_AUTOINSTALL_PATCHLIST}"
			printf "$(gettext '%s:  WARNING:  using \"%s\"')\n" "${PROG}" "${SC_AUTOINSTALL_PATCHLIST_SOLARIS}"
			printf "\n"
		fi
		patchlist=${SC_AUTOINSTALL_PATCHLIST_SOLARIS}
	fi

	# If there are any other "patchlist.*" files, we are done
	if [[ -z "${patchlist}" ]]; then
		for file in ${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_PATCHES}/${SC_AUTOINSTALL_PATCHLIST}.*
		do
			if [[ -s "${file}" ]]; then
				patchlist=NOSOLARIS
				break
			fi
		done
	fi

	# If no patchlist files, just grab all the patches
	if [[ -z "${patchlist}" ]]; then
		for file in ${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_PATCHES}/*
		do
			if [[ -d "${file}" ]]; then
				file=$(basename ${file})
				patchlist="${patchlist} ${file}"
			fi
		done
	fi

	# Get rid of our placeholder
	if [[ "${patchlist}" = NOSOLARIS ]]; then
		patchlist=
	fi

	# Install the patches
	if [[ -n "${patchlist}" ]]; then
		printf "\n"
		printf "\n$(gettext 'Installing Solaris patches ...')\n\n"
		cmd="${SC_PATCH_CMD} -R ${SC_BASEDIR} -M ${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_PATCHES} ${patchlist}"
		printf "${cmd}\n"
		${cmd}
		printf "\n"
	fi
fi

#
# Print the scinstall command we intend to run
#
printf "\n"
printf "$(gettext 'Running %s ...')\n\n" "scinstall"
argvar_print_SC_ARGVARS "0" "scinstall -iI" "${PRINT_PATCH_OPT}"

#
# Set the argument variables from the SC_ARGVAR_ variables, then run scinstall
#
set -A SC_ARGVARS
argvars_to_SC_ARGVARS "0"

#
# Run scinstall in a subshell
#
# Note that by using the -d option, the correct version of the operating
# environment is selected at Jumpstart time, even if scinstall itself was
# run from the wrong OE.  The Tools directory contents are identical for
# each OE, so it does not really matter which OE ${autoscinstalltools} uses,
# as long as -d is used for the actual install.
#
# Using -iI to get around new no-install-pkgs limitation
#
( 
	cd ${autoscinstalltools} || return 1
	./scinstall -iI -d ${autoscinstalldir} ${SC_ARGVARS[*]} ${PATCH_OPT} || return 1

	return 0
) || cleanup 1
printf "\n"

#
# Autoinstall any Sun Cluster patches in the patches directory.
#
patchlist=${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_PATCHES}/${SC_AUTOINSTALL_PATCHLIST_SUNCLUSTER}
if [[ -n "${SC_AUTOINSTALL_PATCHES}" ]] && [[ -s "${patchlist}" ]]; then
	printf "\n"
	printf "\n$(gettext 'Installing Sun Cluster patches ...')\n\n"
	cmd="${SC_PATCH_CMD} -R ${SC_BASEDIR} -M ${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_PATCHES} ${SC_AUTOINSTALL_PATCHLIST_SUNCLUSTER}"
	printf "${cmd}\n"
	${cmd}
	printf "\n"
fi

#
# Run any additional finish script
#
if [[ -n "${SC_AUTOINSTALL_FINISH}" ]] &&
    [[ -x "${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_FINISH}" ]]; then
	printf "\n"
	printf "$(gettext 'Running custom %s ...')\n\n" "finish"
	echo ${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_FINISH}
	${SC_AUTOSCHOSTDIR}/${SC_AUTOINSTALL_FINISH}
	printf "\n"
fi

cleanup
