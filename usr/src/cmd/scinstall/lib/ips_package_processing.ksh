#! /usr/xpg4/bin/sh -p

#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)ips_package_processing.ksh	1.3	09/01/14 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

PROGNAME=ips_package_processing
IPS_LIB=ips_package_common

# test that we've got exactly three args:
#	$SC_SCADMINDIR, which will be different at install and remove times
#	logfile name
#	processing mode

if [[ $# -ne 3 ]]; then
    exit_usage
fi

SC_SCADMINDIR=$1
LOG=$2
MODE=$3

IPS_LIB=${SC_SCADMINDIR}/lib/ips_package_common

SCRIPT_SUFFIX=ips-processing.ksh

echo "${PROGNAME}: --ENTER-- $(date)" >> ${LOG}

# IPS packages will drop their processing scripts into SC_IPS_INSTALL_SCRIPTSDIR
# which will have been relocated for remove mode

SC_IPS_INSTALL_SCRIPTSDIR=${SC_SCADMINDIR}/ips/install-scripts
echo "\tSC_IPS_INSTALL_SCRIPTSDIR: ${SC_IPS_INSTALL_SCRIPTSDIR}" >> ${LOG}

# load IPS package processing library
. ${IPS_LIB}
if [[ $? -ne 0 ]]; then
    printf "$(gettext '%s: Unable to load \"%s\"')\n" "${PROGNAME}" "${IPS_LIB}" >&2
    echo "${PROGNAME}: Unable to load: ${IPS_LIB}" >> ${LOG}
    return 1
fi

#
# $MODE is one of the SVR4 install processes with 'ips_' prefixed:
#	ips_postinstall, ips_preremove, ips_postremove
#	(preinstall can't be supported under IPS)
#


#
# test $MODE
#
case ${MODE} in 
    'ips_postinstall')
	;;
    'ips_preremove')
	;;
    'ips_postremove')
	;;
     *)
	exit_usage  # exit
	;;
esac


# test scadmindir
if [[ ! -d ${SC_SCADMINDIR} ]]; then
	exit_usage  # early exit
fi

# If no scripts dir then no scripts to run
# scripts dir is only created by IPS if files delivered
if [[ ! -d ${SC_IPS_INSTALL_SCRIPTSDIR} ]]; then
	exit IPS_OK  # early exit
fi

result=$IPS_OK

#
# make list of package script files
# roll the list calling $MODE method
#
echo "${PROGNAME}: ${MODE}..."

TMPF=/tmp/.${PROGNAME}.$$
/usr/bin/find ${SC_IPS_INSTALL_SCRIPTSDIR} -name "*.${SCRIPT_SUFFIX}" -print > $TMPF
while read script
do
	scriptname=`/usr/bin/basename ${script}`
	echo "START ${scriptname} :: ${MODE}" >> ${LOG}

	if [[ -r ${script} ]]; then
	    (. ${script}; ${MODE} ) >> ${LOG} 2>&1
	    status=$?
	    if [[ $status -eq 127 ]]; then
		echo "\t${scriptname} does not contain function \"${MODE}\"" >> ${LOG}
		status=$IPS_OK # should have been a placeholder method, but...
	    elif [[ $status -ne 0 ]]; then
		result=$IPS_FAIL
	    fi

	else
	    echo "\t${scriptname} not found or not readable" >> ${LOG}
	fi
	echo "END ${scriptname} :: ${MODE}: ${status} \n" >> ${LOG} 

done < ${TMPF}
/usr/bin/rm -f ${TMPF}

if [[ ${result} -eq $IPS_OK ]]; then
    echo "${PROGNAME}: --EXIT success-- $(date)" >> ${LOG}
    echo "${PROGNAME}: ${MODE} done"
else
    echo "${PROGNAME}: --EXIT failure-- $(date)" >> ${LOG}
fi

exit ${result}
