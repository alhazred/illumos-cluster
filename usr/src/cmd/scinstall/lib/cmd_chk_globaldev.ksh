#! /usr/xpg4/bin/sh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)cmd_chk_globaldev.ksh	1.5	08/08/14 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# cmd_chk_globaldev <type> <lofifile>|<mount_point>|<special_device>
#
#	<type> is:
#
#		lofi			# loopback file
#		fs			# mount_point
#		device			# special_device
#
#	Possible exit codes are the same as the result codes from
#	is_globalfs_okay() and is_globalcspecial_okay() (depending
#       on given "type").
#
#####################################################

#####################################################
#
# Constant Globals
#
#####################################################

# Program name
typeset -r PROG=${0##*/}

# Set the PATH
typeset -r SC_BINDIR=/usr/cluster/bin
typeset -r SC_BINDIRS=${SC_BINDIR}:/usr/cluster/lib/sc
PATH=${SC_BINDIRS}:/bin:/usr/bin:/sbin:/usr/sbin; export PATH

# I18N
typeset -x TEXTDOMAIN=TEXT_DOMAIN
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

#####################################################
#
# print_usage()
#
#	Print usage message to stderr
#
#####################################################
print_usage()
{
	echo "$(gettext 'usage'):  ${PROG} lofi|fs|device <lofifile>|<mount_point>|<special_device> [lofifilesize]" >&2
}

#####################################################
#
# Main
#
#####################################################
main()
{
	typeset type=${1}
	typeset globaldev=${2}
	typeset lofifilesize=${3}

	typeset mydir

	#
	# "mydir" is the directory from which this script is run.
	# Make sure that it is set with an absolute path;  and, remove
	# trailing dots.
	#
	mydir=$(dirname $0)
	if [[ "${mydir}" != /* ]]; then
		mydir=$(/usr/bin/pwd)/${mydir}
	fi
	while [[ "${mydir##*/}" = "." ]]
	do
		mydir=$(dirname ${mydir})
	done             

	#
	# scinstall_common is expected to be in the same directory as
	# this program.
	#
	. ${mydir}/scinstall_common
	if [[ $? -ne 0 ]]; then
		printf "$(gettext '%s:  Unable to load \"%s\"')\n" "${PROG}" "scinstall_common" >&2  
		return 10
	fi

	case ${type} in
	lofi)	# lofi method
		# There is no filesystem to check as lofi will create one 
                # So just return a 0
		return 0
		;;

	fs)	# file system
		is_globalfs_okay "${globaldev}" 3
		return $?
		;;

	device)	# special device
		is_globalcspecial_okay "${globaldev}" 3
		return $?
		;;

	*)	# usage error
		print_usage
		return 10
		;;
	esac

	print_usage
	return 10
}

	main $*
	exit $?
