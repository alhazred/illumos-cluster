//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)failfastd.cc	1.9	08/07/17 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <sys/os.h>
#include <repl_pxfs.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <h/ff.h>
#include <cmm/cmm_ns.h>
#include <cmm/ff_signal.h>
#include <signal.h>
#include <stropts.h>
#include <fcntl.h>
#include <st_ff_server.h>
#include <sys/cladm_int.h>
#include <sys/cladm_debug.h>

#include <stdarg.h>

#include <synch.h>
#include <thread.h>

//
// Post S10, failfastd is run in all zones (global and non-global)
// Since, /etc/cluster is not present in non-global zones,
// put the .failfastd file in /var/cluster, post S10
//
#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)
#define	FAILFASTD_FILE	"/var/cluster/.failfastd"
#else
#define	FAILFASTD_FILE	"/etc/cluster/.failfastd"
#endif

static void daemonize(void);
static void block_allsignals(void);
static void wait_for_daemon(void);
static void do_failfastd(void);
static void do_log(const char *, va_list);
static void do_exit(int, const char *, ...);
static void create_and_arm_failfast(void);
void failfast_log(int level, const char *, ...);
static char *this_prog_name = NULL;
static int pipe_fds[2];
static mutex_t dbg_mutex;


ff::failfast_var	death_ff_v;

//
// failfastd daemon main routine
//
int
main(int, char *argv[])
{
	(void) mutex_init(&dbg_mutex, USYNC_THREAD, 0);
	failfast_log(CLADM_GREEN, "failfastd(%d):start\n", getpid());
	this_prog_name = strdup(argv[0]);
	if (!this_prog_name) {
		this_prog_name = "failfastd";
		do_exit(1, SC_SYSLOG_MESSAGE
		    ("strdup returned " "%d.  Exiting."),
		    errno);
	}

	block_allsignals();

	// Create a communication pipe with the daemon process
	if (pipe(pipe_fds) != 0) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("pipe returned %d. Exiting."),
		    errno);
	}

	// Create the failfastd daemon
	daemonize();
	failfast_log(CLADM_GREEN, "failfastd(%d):done\n", getpid());

	// Wait until the failfastd daemon is ready
	wait_for_daemon();
	failfast_log(CLADM_GREEN, "failfastd(%d):exit\n", getpid());

	return (0);
}


//
// Wait that the deamon is ready
//
static void
wait_for_daemon(void)
{
	ssize_t res;
	char c = (char)-1;

	// Block until the deamon has write to the pipe
	res = read(pipe_fds[0], (void *)&c, 1);
	if (res != 1) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("problem waiting the deamon,"
		    " read errno %d"), errno);
	} else {
		ASSERT(c == 0);
	}
	(void) close(pipe_fds[0]);
	failfast_log(CLADM_GREEN, "failfastd(%d):read pipe\n", getpid());
}


//
// Become a deamon
//
static void
daemonize(void)
{
	int ret;
	int err;
	failfast_log(CLADM_GREEN, "failfastd(%d):fork1\n", getpid());
	(void) mutex_lock(&dbg_mutex);
	ret = fork1();
	err = errno;
	(void) mutex_unlock(&dbg_mutex);
	failfast_log(CLADM_GREEN, "failfastd(%d):fork1\n", getpid());
	switch (ret) {
	case ((pid_t)-1) :
		do_exit(err, SC_SYSLOG_MESSAGE("fork1 returned %d."
		    "  Exiting."), err);
		/* NOTREACHED */
	case 0:
		/* In the child */
		(void) close(pipe_fds[0]);
		do_failfastd();
		/* NOTREACHED */
	default:
		/* In the father */
		(void) close(pipe_fds[1]);
		return;
	}
}


//
// Failfast proxy server
//
static void
do_failfastd(void)
{
	ssize_t res;
	char c = 0;

	block_allsignals();

	// Daemonize.
	(void) close(0);
	(void) close(1);
	(void) close(2);

	(void) setsid();

	if (ORB::initialize(false) != 0) {
		do_exit(1, SC_SYSLOG_MESSAGE("Could not initialize the ORB.  "
		    "Exiting."));
	}

	//
	// Start the failfast proxy server
	//
	int fferr = 0;

	if ((fferr = start_failfast_server()) != 0) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("Error %d from "
		    "start_failfast_server"), fferr);
	}

	//
	// Create and arm a failfast object so that if failfastd dies, the
	// node will be forced to panic.
	//
	create_and_arm_failfast();

	failfast_log(CLADM_GREEN, "failfastd(%d):ready\n", getpid());
	//
	// Now create a file, indicating to the launcher
	// script that the feature is up and running.
	//
	if (creat(FAILFASTD_FILE, 0600) < 0) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("unable to create file"
		" %s errno %d"), FAILFASTD_FILE, errno);
	}
	failfast_log(CLADM_GREEN, "failfastd(%d):synchro file\n", getpid());

	// Now unblock the parent process, waiting for failfastd to be ready
	res = write(pipe_fds[1], (void *)&c, 1);
	if (res != 1) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("Unblock parent,"
		    " write errno %d"), errno);
	}
	(void) close(pipe_fds[1]);
	failfast_log(CLADM_GREEN, "failfastd(%d):write pipe\n", getpid());


	thr_exit(0);
	// not reached
}


//
// Helper function used by do_exit().
//
static void
do_log(const char *format, va_list v)
{
	char work[MAXPATHLEN];
	(void) mutex_lock(&dbg_mutex);
	os::sc_syslog_msg msg("", this_prog_name, NULL);
	(void) vsprintf(work, format, v);
	(void) mutex_unlock(&dbg_mutex);
	failfast_log(CLADM_RED, "failfastd%d:do_log %s\n", getpid(), work);
	(void) mutex_lock(&dbg_mutex);
	(void) msg.log(SC_SYSLOG_ERROR, MESSAGE, NO_SC_SYSLOG_MESSAGE(work));
	(void) mutex_unlock(&dbg_mutex);
}


//
// Helper function to log errors and exit.
//
static void
do_exit(int exit_code, const char *format, ...)
{
	va_list v;		/* CSTYLED */
	va_start(v, format);	/*lint !e40 */
	do_log(format, v);
	va_end(v);
	failfast_log(CLADM_RED, "failfastd%d:exit %d\n", getpid(), exit_code);
	exit(exit_code);
}


//
// Create and arm a failfast object so that the node dies when failfastd dies.
//
static void
create_and_arm_failfast(void)
{

	ff::failfast_admin_var  ff_admin_ptr;
	Environment 		e;

	ff_admin_ptr = cmm_ns::get_ff_admin();

	if (CORBA::is_nil(ff_admin_ptr)) {
		//
		// Given the lack of an error code to represent what happened
		// we choose ECOMM to best represent why we failed.
		//

		//
		// SCMSGS
		// @explanation
		// Unable to get the reference for the failfast_admin object
		// from the name server.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(ECOMM, SC_SYSLOG_MESSAGE("create_and_arm_failfast : "
		    "Unable to get ff_admin_ptr"));
	}

	//
	// Use IDL invocation on kernel ff_admin object to set my priority
	//
	// Lint complains if we receive the return value in a variable and
	// then do not use it. Currently, the method being called on the
	// object does all the error checks, and we do not need to use the
	// return value here. However, the return value might be used by the
	// client doing the IDL invocation, and hence we keep the return type
	// of the interface as 'int' rather than changing it to 'void'.
	//
	(void) ff_admin_ptr->set_rt_priority((int)getpid(), 0, e);

	//
	// Use FFM_DEFERRED_PANIC to allow enough time for a core
	// dump of this process.
	//
	death_ff_v = ff_admin_ptr->ff_create("failfastd",
	    ff::FFM_DEFERRED_PANIC, e);

	if (e.exception() || CORBA::is_nil(death_ff_v)) {

		//
		// Given the lack of an error code to represent what happened
		// we choose ECOMM to best represent why we failed.
		//

		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(ECOMM, SC_SYSLOG_MESSAGE("unable to create "
		    "failfast object."));
	} else {

		// Generate a failfast only when this process dies.

		death_ff_v->arm(ff::FF_INFINITE, e);
		if (e.exception()) {
			//
			// SCMSGS
			// @explanation
			// Internal error.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(ECOMM, SC_SYSLOG_MESSAGE("unable to "
			    "arm failfast."));
		}
		// Trigger the failfast upon receipt of these signals
		ff_register_signal_handler(SIGILL, &death_ff_v);
		ff_register_signal_handler(SIGBUS, &death_ff_v);
		ff_register_signal_handler(SIGSEGV, &death_ff_v);
	}
}

static void
block_allsignals(void)
{
	sigset_t s_mask;

	//
	// Ignore SIGHUP and SIGTERM so the transition from rcS to rc2 doesn't
	// kill us.  Block the signals so that sending signals to a
	// user program that calls 'failfastd' does not lead to our handling the
	// signal in unexpected ways.
	//
	if (sigfillset(&s_mask) == -1) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("failfastd: sigfillset "
		    "returned %d.  Exiting."), errno);
	}
	if (sigdelset(&s_mask, SIGILL) == -1) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("failfastd: sigdelset "
		    "returned %d.  Exiting."), errno);
	}
	if (sigdelset(&s_mask, SIGBUS) == -1) {
		do_exit(errno, SC_SYSLOG_MESSAGE("failfastd: sigdelset "
		    "returned %d.  Exiting."), errno);
	}
	if (sigdelset(&s_mask, SIGSEGV) == -1) {
		do_exit(errno, SC_SYSLOG_MESSAGE("failfastd: sigdelset "
		    "returned %d.  Exiting."), errno);
	}
	if (thr_sigsetmask(SIG_BLOCK, &s_mask, NULL) != 0)
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("failfastd: thr_sigsetmask "
		    "returned %d.  Exiting."), errno);
}


//
// Add a printf-style message to whichever debug logs we're currently using.
//
void
failfast_log(int level, const char *fmt, ...)
{
	va_list ap;
	cladmin_log_t *tolog;

	(void) mutex_lock(&dbg_mutex);

	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, fmt);
	/*lint +e40 Undeclared identifier (__builtin_va_alist) */

	tolog = (cladmin_log_t *)malloc(sizeof (cladmin_log_t));
	if (tolog == NULL) {
		va_end(ap);
		(void) mutex_unlock(&dbg_mutex);
		return;
	}
	tolog->level = level;
	(void) vsnprintf(tolog->str, CLADMIN_LOG_SIZE - 1, fmt, ap);
	(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG, (void *)tolog);
	free(tolog);
	va_end(ap);
	(void) mutex_unlock(&dbg_mutex);
}
