#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)dofsck.ksh	1.5	08/05/20 SMI"

# Input arguments are:
# $1	special device file name used in mount command.
# $2	file system type name.
# $3	mount options returned from mount system call.

# Do nothing for UFS since UFS and SDS logging don't require an fsck.


/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit 1
fi

# Vxfs requires an fsck.
if [ "$2" = vxfs ]; then
	fsck -F vxfs -o mounted $1
	if [ $? != 0 ]; then
		exit 1
	fi
fi

exit 0
