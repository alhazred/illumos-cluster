//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)st_ff_server.cc	1.17	08/05/20 SMI"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <door.h>
#include <stropts.h>
#include <h/ff.h>
#include <sys/os.h>
#include <cmm/cmm_ns.h>
#include <cmm/ff_signal.h>
#include <st_ff_client.h>
#include <st_ff_server.h>
#include <sys/cladm_debug.h>


#define	MAX_PROCESS_NAME	32

// to trace errors
extern void failfast_log(int, const char *, ...);

// The door descriptor of the failfast proxy server
static int	cl_failfast_door = -1;

st_ff::st_ff() :
	armed(false),
	ddesc(-1)
{
}

st_ff::~st_ff()
{
	// Release the failfast object and revoke the door in the destructor
	CL_PANIC(lock_held());
	// os::printf("Destroying st_ff, armed = %s", armed ? "true" : "false")

	if (ddesc != -1) {
		(void) door_revoke(ddesc);
		ddesc = -1;
	}
}

int
st_ff::initialize(const char *pff_name)
{
	Environment e;

	CL_PANIC(lock_held());

	// Use the failfast_admin object to create a failfast object in the
	// kernel.
	ff::failfast_admin_var ff_adminp = cmm_ns::get_ff_admin();

	if (CORBA::is_nil(ff_adminp)) {
		failfast_log(CLADM_RED,
		    "st_ff_server:st_ff::initialize"
		    " CORBA::is_nil(ff_adminp)\n");
		return (EACCES);
	}

	death_ff_v = ff_adminp->ff_create(pff_name, ff::FFM_PROXY_PANIC, e);

	if (CORBA::is_nil(death_ff_v)) {
		failfast_log(CLADM_RED,
		    "st_ff_server:st_ff::initialize"
		    " CORBA::is_nil(ffp)\n");
		return (EACCES);
	}

	if (e.exception()) {
		failfast_log(CLADM_RED,
		    "st_ff_server:st_ff::initialize"
		    " exception\n");
		e.clear();
		death_ff_v = ff::failfast::_nil();
		return (EACCES);
	}

	//
	// This software can enable and disable the failfast.
	//
	// This software is built with failfastd, which handles
	// the registration for signals. Thus do not register for
	// signal handling here.
	//

	return (0);
}

void
st_ff::d_server(void *cookie, char *arg, size_t arglen, door_desc_t *,
    uint_t)
{
	st_ff *stffp = (st_ff *) cookie;
	stffp->entry(arg, arglen);
	// syslog here (we should never reach this point)
}

//
// The entry point in the object for the door_call.  If we're unreferenced,
// commit suicide.  If we're being asked to arm or disarm ourselves, make
// the appropriate calls on the failfast object.
//
void
st_ff::entry(char *arg, size_t arglen)
{
	int err = 0;
	Environment e;

	lock();

	CL_PANIC(!CORBA::is_nil(death_ff_v) && (ddesc >= 0));

	if (arg == DOOR_UNREF_DATA) {

		delete this;
		(void) door_return(NULL, 0, NULL, 0);
		return;

	} else if (strncmp(arg, ST_FF_ARM, arglen - 1) == 0) {

		if (!armed) {
			death_ff_v->arm(ff::FF_INFINITE, e);
			if (e.exception()) {
				failfast_log(CLADM_RED,
				    "st_ff_server:st_ff::entry"
				    " exception\n");
				e.clear();
				err = EIO;
			} else {
				armed = true;
			}
		}

	} else if (strncmp(arg, ST_FF_DISARM, arglen - 1) == 0) {

		if (armed) {
			death_ff_v->disarm(e);
			if (e.exception()) {
				failfast_log(CLADM_RED,
				    "st_ff_server:st_ff::entry"
				    " exception\n");
				e.clear();
				err = EIO;
			} else {
				armed = false;
			}
		}

	} else {
		err = EINVAL;
	}

	unlock();
	(void) door_return((char *)&err, sizeof (int), NULL, 0);
}

void
st_ff::set_desc_num(int dnum)
{
	CL_PANIC(lock_held());
	ddesc = dnum;
}

void
st_ff::lock()
{
	st_ff_lock.lock();
}

void
st_ff::unlock()
{
	st_ff_lock.unlock();
}

bool
st_ff::lock_held()
{
	return ((bool)st_ff_lock.lock_held());
}

int
failfast_unlink(char *file)
{
	int err;
retry_unlink:
	if (unlink(file) == -1) {
		err = errno;
		if (err == EINTR || err == EAGAIN) {
			failfast_log(CLADM_AMBER,
			    "st_ff_server:start_failfast_server"
			    " unlink %s errno %d\n",
			    file, err);
			goto retry_unlink;
		}
		if (err != ENOENT) {
			failfast_log(CLADM_RED,
			    "st_ff_server:start_failfast_server"
			    " unlink %s errno %d\n",
			    file, err);
			return (err);
		}
	}
	return (0);
}

// Start the failfast server and fattach() it to ST_FF_DOORNAME
int
start_failfast_server()
{
	int door_file_fd;
	int err;

	if ((err = failfast_unlink(ST_FF_DOORNAME)) != 0) {
		return (err);
	}

	if ((door_file_fd = open(ST_FF_DOORNAME, O_RDWR | O_CREAT, 0600)) < 0) {
		err = errno;
		failfast_log(CLADM_RED,
		    "st_ff_server:start_failfast_server"
		    " open %s errno %d\n",
		    ST_FF_DOORNAME, errno);
		return (err);
	}
	(void) close(door_file_fd);

	if ((cl_failfast_door = door_create(st_ff_factory, NULL, 0)) < 0) {
		err = errno;
		failfast_log(CLADM_RED,
		    "st_ff_server:start_failfast_server"
		    " door_create %s errno %d\n",
		    ST_FF_DOORNAME, err);
		(void) failfast_unlink(ST_FF_DOORNAME);
		return (err);
	}

	if (fattach(cl_failfast_door, ST_FF_DOORNAME) < 0) {
		err = errno;
		failfast_log(CLADM_RED,
		    "st_ff_server:start_failfast_server"
		    " fattach %s errno %d\n",
		    ST_FF_DOORNAME, err);
		(void) failfast_unlink(ST_FF_DOORNAME);
		return (err);
	}

	return (0);
}

void
stop_failfast_server()
{
	if (cl_failfast_door >= 0) {
		(void) door_revoke(cl_failfast_door);
		(void) fdetach(ST_FF_DOORNAME);
		(void) failfast_unlink(ST_FF_DOORNAME);
	}
}

void
st_ff_factory(void *, char *argp, size_t arg_size, door_desc_t *, uint_t)
{
	int ff_door;
	int err;
	size_t len;
	size_t process_name_size;
	door_desc_t ddesc;
	st_ff *stffp = NULL;
	door_cred_t dcred;
	char process_name[MAX_PROCESS_NAME + 1];

	len = strlen(ST_FF_REQUEST);

	// Make sure only root can get a failfast proxy.

	if (door_cred(&dcred) != 0) {
		err = EINVAL;
		(void) door_return((char *)&err, sizeof (int), NULL, 0);
		return;
	}

	if ((dcred.dc_euid != 0) || (dcred.dc_ruid != 0)) {
		err = EPERM;
		(void) door_return((char *)&err, sizeof (int), NULL, 0);
		return;
	}

	// Make sure the proper request string is being passed

	if (strncmp(argp, ST_FF_REQUEST, len) != 0) {
		err = EINVAL;
		(void) door_return((char *)&err, sizeof (int), NULL, 0);
		return;
	}

	if ((arg_size < (len + 3)) || (arg_size > (len + ST_FF_NAMESIZE + 2))) {
		err = EINVAL;
		(void) door_return((char *)&err, sizeof (int), NULL, 0);
		return;
	}

	// Extract the name passed with the process

	process_name_size = arg_size - (len + 1);
	(void) strncpy(process_name, argp + len + 1,
	    process_name_size >= MAX_PROCESS_NAME ?
	    MAX_PROCESS_NAME : process_name_size);

	process_name[MAX_PROCESS_NAME] = 0; // Just in case

	// Create the object and it's associated door, passing a pointer
	// to the object as the cookie.
	stffp = new st_ff();

	if (stffp == NULL) {
		err = ENOMEM;
		(void) door_return((char *)&err, sizeof (int), NULL, 0);
		return;
	}

	stffp->lock();
	if ((err = stffp->initialize(process_name)) != 0) {
		delete stffp;
		(void) door_return((char *)&err, sizeof (int), NULL, 0);
		return;
	}

	if ((ff_door = door_create(st_ff::d_server, (void *)stffp,
	    DOOR_UNREF)) < 0) {
		err = errno;
		delete stffp;
		(void) door_return((char *)&err, sizeof (int), NULL, 0);
		return;
	}

	stffp->set_desc_num(ff_door);
	stffp->unlock();

	// Return a reference to the door to the caller.

	ddesc.d_attributes = DOOR_DESCRIPTOR;
	ddesc.d_data.d_desc.d_descriptor = ff_door;
	err = 0;
	(void) door_return((char *)&err, sizeof (int), &ddesc, 1);
}
