//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)node_info.cc	1.24	08/05/20 SMI"

#include <thread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#include <sys/os.h>
#include <sys/cladm_int.h>

static char	*progname;

void
usage()
{
	(void) fprintf(stderr, "Usage: %s [option]\n", progname);
	(void) fprintf(stderr, "Options:\n");

	(void) fprintf(stderr, "   -?         print this message\n");
	(void) fprintf(stderr, "   -c         return with exit value 0"
	    "if system is booted as a cluster\n");
	(void) fprintf(stderr, "   -h         print highest nodeid\n");
	(void) fprintf(stderr, "   -n         print current nodeid\n");
	(void) fprintf(stderr, "   -s         print the cluster size\n");
	(void) fprintf(stderr, "   -C         print the cluster name\n");
	(void) fprintf(stderr, "   -N <nid>   print nodename of node with "
	    "nodeid 'nid'\n");
	(void) fprintf(stderr, "   -l         print nodenames of all nodes\n");
	(void) fprintf(stderr, "If no option is specified, -c is assumed\n");
}

int
main(int argc, char **argv)
{
	int c;
	uint_t i;
	nodeid_t nid, highest, n;
	char name[64] = "";
	clcluster_name_t clname;
	clnode_name_t nodename;
	char *endp;
	long l;

	progname = argv[0];			/* save program name */
	(void) sigset(SIGSYS, SIG_IGN);

	while ((c = getopt(argc, argv, "nhrcsCN:l?")) != EOF) {
		switch (c) {
		case 'n':	// print our node number
			if (os::cladm(CL_CONFIG, CL_NODEID, &nid) != 0) {
				perror("_cladm: CL_NODEID");
				return (1);
			}
			(void) printf("%u\n", nid);
			return (0);

		case 'h':	/* print the highest node number */
			if (os::cladm(CL_CONFIG, CL_HIGHEST_NODEID, &highest)
			    != 0) {
				perror("_cladm: CL_HIGHEST_NODEID");
				return (1);
			}
			(void) printf("%u\n", highest);
			return (0);

		case 'c':	// return true if booted as a cluster
			if (os::cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &c)
			    != 0) {
				perror("_cladm: CL_GET_BOOTFLAG");
				return (1);
			}
			return (c & CLUSTER_BOOTED ? 0 : 1);

		case 's':	/* print the cluster size */
			if (os::cladm(CL_CONFIG, CL_HIGHEST_NODEID, &highest)
			    != 0) {
				perror("_cladm: CL_HIGHEST_NODEID");
				return (1);
			}
			for (n = 0, i = 1; i <= highest; i++) {
				nodename.nodeid = i;
				name[0] = '\0';
				nodename.name = name;
				nodename.len = sizeof (name);
				if (os::cladm(CL_CONFIG, CL_GET_NODE_NAME,
				    &nodename) != 0) {
					/* error in syscall */
					perror("_cladm: CL_GET_NODE_NAME");
					return (1);
				}
				if (nodename.name[0] != '\0') {
					n++;
				}
			}
			(void) printf("%u\n", n);
			return (0);

		case 'C':	/* print the cluster name */
			clname.name = name;
			clname.len = sizeof (name);
			if (os::cladm(CL_CONFIG, CL_GET_CLUSTER_NAME,
			    &clname)) {
				perror("_cladm: CL_GET_CLUSTER_NAME");
				return (1);
			}
			(void) printf("%s\n", clname.name);
			return (0);

		case 'N':	/* print name of a specified node */
			/* convert optarg to nodeid */
			l = strtol(optarg, &endp, 10);
			if (endp[0] != '\0' || l < 0) {
				(void) fprintf(stderr, "Invalid nodeid: %s\n",
					optarg);
				return (1);
			}

			nodename.nodeid = (unsigned long)l;
			nodename.name = name;
			nodename.len = sizeof (name);
			if (os::cladm(CL_CONFIG, CL_GET_NODE_NAME, &nodename)) {
				perror("_cladm: CL_GET_NODE_NAME");
				return (1);
			}
			(void) printf("%s\n", nodename.name);
			return (0);

		case 'l':	/* print the cluster size */
			if (os::cladm(CL_CONFIG, CL_HIGHEST_NODEID, &highest)
			    != 0) {
				perror("_cladm: CL_HIGHEST_NODEID");
				return (1);
			}
			for (i = 1; i <= highest; i++) {
				nodename.nodeid = i;
				name[0] = '\0';
				nodename.name = name;
				nodename.len = sizeof (name);
				if (os::cladm(CL_CONFIG, CL_GET_NODE_NAME,
				    &nodename) != 0) {
					/* error in syscall */
					perror("_cladm: CL_GET_NODE_NAME");
					return (1);
				}
				if (name[0] != '\0') {
					(void) printf("Node %d %s\n", i, name);
				}
			}
			return (0);

		case '?':	/* print usage or unknown option */
			if (optopt == '?') {
				usage();
				return (0);
			}
			return (1);

		default:
			return (1);
		}
	}

	/* If we get to here that means no options were given; do default */
	/* behavior: return with 0 if we're booted as a cluster. */
	if (os::cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &c) != 0) {
		perror("_cladm: CL_GET_BOOTFLAG");
		return (1);
	}
	return (c & CLUSTER_BOOTED ? 0 : 1);
}
