/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_NOORB_FF_SERVER_H
#define	_NOORB_FF_SERVER_H

#pragma ident	"@(#)st_ff_server.h	1.9	08/05/20 SMI"

#include <door.h>
#include <stropts.h>
#include <strings.h>
#include <h/ff.h>

//
// These routines start and stop the failfast server itself.  The failfast
// server fattach()s its door in /etc to make itself available to other
// processes.  It will only accept connections from processes with root
// priviledges.
//

int start_failfast_server();
void stop_failfast_server();

//
// The door function for the failfast proxy factory server (st is for single-
// threaded).  The client functions for this server live in libclst.
//

extern "C" void st_ff_factory(void *, char *, size_t, door_desc_t *,
    uint_t);

class st_ff {
public:
	st_ff();
	~st_ff();
	// Post construction initialization
	int initialize(const char *);
	// The door server entry point for the proxy.
	static void d_server(void *, char *, size_t, door_desc_t *, uint_t);
	// The in-object entry point from a door-call (args 2 and 3 from above)
	void entry(char *, size_t);
	// We only know our descriptor number after the door is created, so
	// a separate function is provided to set it.
	void set_desc_num(int);
	void lock();
	void unlock();
private:
	bool lock_held();
	os::mutex_t st_ff_lock;
	bool armed;

	ff::failfast_var	death_ff_v;

	// The door this object is associated with
	int ddesc;
};

#endif	/* _NOORB_FF_SERVER_H */
