//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clexecd.cc	1.78	08/05/20 SMI"

#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <orb/infrastructure/clusterproc.h>
#include <sys/os.h>
#include <repl_pxfs.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <h/ff.h>
#include <cmm/cmm_ns.h>
#include <cmm/ff_signal.h>
#include <signal.h>
#include <stropts.h>
#include <fcntl.h>
#include <st_ff_server.h>
#include <sys/cladm_int.h>
#include <sys/cladm_debug.h>
#include <rgm/sczones.h>

#include <sys/wait.h>
#include <sys/mnttab.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <sys/priocntl.h>
#include <sys/rtpriocntl.h>
#include <sys/tspriocntl.h>
#include <synch.h>
#include <thread.h>

#define	EXECIT_MAX_CMD_LEN	(MAXPATHLEN * 2)
#define	CLEXECD_FILE		"/etc/cluster/.clexecd"

static mutex_t dbg_mutex;

static void clexec_log(int, const char *, ...);
static void do_exit(int exit_code, const char *format, ...);
static void log_signal_emitter(pid_t pid);
static void create_end_file(void);

class mounter : public McServerof<repl_pxfs::ha_mounter> {

	void _unreferenced(unref_t cookie)
	{
		(void) _last_unref(cookie);
		clexec_log(CLADM_GREEN, "_unreferenced exit\n");
		exit(0);
	}

/* ha_mounter */
	void mount(const char *spec, const char *fstype, const char *mntoptions,
	    Environment &_environment);
	void exec_program(const char *cmd, Environment &_environment);
	// new interface for exec_program()
	void exec_program_with_opts(const char *cmd, bool rt_prio,
		bool run_in_foreground, bool log,
		Environment &_environment);
	bool is_alive(Environment &_environment);
};

// lint complains about no default constructor
//lint -e1712
struct execit_buf_t {
public:
	// no default constructor, which lint complains about
	execit_buf_t(const bool rto, const bool fg, const bool lg,
			const char* const cmd);
	~execit_buf_t();

	uint_t transmission_length() const;

	const bool& get_rt_prio() const;
	const bool& get_run_in_foreground() const;
	const bool& get_log() const;

	const char *get_cmdbuf() const;
	char *get_cmdbuf();
private:
	bool rt_prio;
	bool run_in_foreground;
	bool log;
	char filler; // to have an even sizeof execit_buf_t
	char _cmdbuf[EXECIT_MAX_CMD_LEN];

}; // execit_buf_t
//lint +e1712

// XXX these trivial methods should probably be moved to a new _in.h file.
// Same for forward declarations and class definitions:  new .h file.

execit_buf_t::execit_buf_t(const bool rt0, const bool fg, const bool lg,
		const char* const cmd) :
	rt_prio(rt0),
	run_in_foreground(fg),
	log(lg)
{
	filler = 0; // to make lint happy
	if (strlen(cmd) >= sizeof (_cmdbuf)) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a problem with a client
		// requesting a too big command.
		// @user_action
		// clexecd program will exit and node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE(
		    "clexecd: too big cmd size %d cmd <%s>"),
		    strlen(cmd), cmd);
	}

	(void) strcpy(_cmdbuf, cmd);
} // ctor

execit_buf_t::~execit_buf_t()
{} // dtor

inline uint_t
execit_buf_t::transmission_length() const
{
	const uint_t	result = sizeof (execit_buf_t) -
		sizeof (_cmdbuf) + strlen(_cmdbuf) + 1;
	return (result);
} // transmission_length

inline const bool&
execit_buf_t::get_rt_prio() const
{
	return (rt_prio);
}

inline const bool&
execit_buf_t::get_run_in_foreground() const
{
	return (run_in_foreground);
}

inline const bool&
execit_buf_t::get_log() const
{
	return (log);
}

inline const char*
execit_buf_t::get_cmdbuf() const
{
	return (_cmdbuf);
}

inline char*
execit_buf_t::get_cmdbuf()
{
	return (_cmdbuf);
}

// Forward declarations.
static int execit(const char *str, const bool rt_prio,
	const bool run_in_foreground, const bool log, int &ret_val);
static int worker_fd;
static int parent_fd;
static void set_ndelay(int fd);
static char *this_prog_name;
static void log_error(const char *format, ...);
static int common_stdin_fd;
static pid_t worker_pid;
static void worker_process(void);
static void read_fd(os::sc_syslog_msg &msg, const char *prefix, int fd,
	int logpri);
static void *worker_thread(void *);
static ff::failfast_admin_var  ff_admin_ptr;

static ff::failfast_var	death_ff_v;

static void create_and_arm_failfast(void);
static void daemonize(void);
static void daemon_process(void);
static void wait_for_daemon(void);
static void create_process_pair(void);
static void block_allsignals(void);
static void * wait_for_child(void *);
static void * wait_for_signals(void *);
static char *worker_process_name = "work_process";
static char *daemon_process_name = "daemon_process";

// global defs
pri_t clexecd_rt_sched_priority = 0;
pri_t clexecd_ts_sched_priority = -20; // higher than default TS
pri_t clexecd_max_ts_sched_priority = -20; // higher than default TS
int wait_for_signal_ready = 0; // wait_for_signal_thread ready ?
int wait_for_child_ready = 0; // wait_for_child thread ready ?

static idtype_t
get_classid(char *classname)
{
	pcinfo_t pc_info;

	(void) strcpy(pc_info.pc_clname, classname);

	if ((priocntl(P_PID, P_MYID, PC_GETCID, (char *)&pc_info)) == -1) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed priocltl(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// clexecd program will exit and node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: priocntl "
		    "returned %d.  Exiting."), errno);
		// NOTREACHABLE
	}

	return ((idtype_t)pc_info.pc_cid);
}

static void
set_rt_prio()
{
	idtype_t	rt_classid  = get_classid("RT");
	pcparms_t	pc_parms;

	rtparms_t *rtp = (rtparms_t *)(pc_parms.pc_clparms);
	pc_parms.pc_cid	= rt_classid;
	rtp->rt_pri	= clexecd_rt_sched_priority;

	// set the timeslice to 100 ms
	rtp->rt_tqnsecs	= 100000000;
	rtp->rt_tqsecs	= 0;

	if ((priocntl(P_PID, P_MYID, PC_SETPARMS,
		(char *)&pc_parms)) == -1) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed priocltl(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// clexecd program will exit and node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: priocntl "
		    "to set rt returned %d.  Exiting."), errno);
		// NOTREACHED
	}
}

static void
ts_setprio()
{
	idtype_t	ts_classid  = get_classid("TS");
	pcparms_t	pc_parms;

	tsparms_t *tsp = (tsparms_t *)(pc_parms.pc_clparms);
	pc_parms.pc_cid = ts_classid;

	// Use the TS priorities, which are settable.

	tsp->ts_uprilim = clexecd_max_ts_sched_priority;
	tsp->ts_upri	= clexecd_ts_sched_priority;

	if (priocntl(P_PID, P_MYID, PC_SETPARMS,
		(char *)&pc_parms) != 0) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed priocltl(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// clexecd program will exit and node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: priocntl "
		    "to set ts returned %d.  Exiting."), errno);
		// NOTREACHED
	}
}

int
main(int, char *argv[])
{
	if (sc_zonescheck() != 0)
		return (1);
	(void) mutex_init(&dbg_mutex, USYNC_THREAD, 0);
	clexec_log(CLADM_GREEN, "main\n");

	// Initialize 'this_prog_name'
	this_prog_name = strdup(argv[0]);

	if (!this_prog_name) {
		this_prog_name = "clexecd";
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed strdup(3C) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// If the error number is 12 (ENOMEM), install more memory,
		// increase swap space, or reduce peak memory consumption. If
		// error number is something else, contact your authorized Sun
		// service provider to determine whether a workaround or patch
		// is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("clexecd: strdup returned "
		    "%d.  Exiting."), errno);
	}

	block_allsignals();

	//
	// The function daemonize() will set the two processes of this
	// daemon (and, of course, all threads and subprocesses they
	// fork when there's work to do) to RT.
	//

	clexec_log(CLADM_GREEN, "daemonize\n");

	daemonize();

	clexec_log(CLADM_GREEN, "wait_for_daemon\n");

	wait_for_daemon();

	create_end_file();

	clexec_log(CLADM_GREEN, "main_end\n");

	return (0);
}

static void
block_allsignals(void)
{
	sigset_t s_mask;

	//
	// Ignore SIGHUP and SIGTERM so the transition from rcS to rc2 doesn't
	// kill us.  Block the signals so that sending signals to a
	// user program that calls 'clexecd' does not lead to our handling the
	// signal in unexpected ways.
	//
	if (sigfillset(&s_mask) == -1)
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed sigfillset(3C)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: sigfillset "
		    "returned %d.  Exiting."), errno);

	if (thr_sigsetmask(SIG_BLOCK, &s_mask, NULL) != 0)
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed
		// thr_sigsetmask(3THR) system call. The error message
		// indicates the error number for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: thr_sigsetmask "
		    "returned %d.  Exiting."), errno);
}

static void
daemonize(void)
{
	// Set  up a communication pipe with the worker process.
	int ret;
	int fds[2];
	if (pipe(fds) != 0) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed pipe(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: pipe returned %d."
		    "  Exiting."), errno);
	}

	worker_fd = fds[0];
	parent_fd = fds[1];

	(void) mutex_lock(&dbg_mutex);
	ret = fork1();
	(void) mutex_unlock(&dbg_mutex);
	switch (ret) {
	case ((pid_t)-1) :
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed fork1(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// If the error number is 12 (ENOMEM), install more memory,
		// increase swap space, or reduce peak memory consumption. If
		// error number is something else, contact your authorized Sun
		// service provider to determine whether a workaround or patch
		// is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: fork1 returned %d."
		    "  Exiting."), errno);
		/* NOTREACHED */

	case 0:
		//
		// both worker and daemon processes will run in RT --
		// part of fix for BugId 4337278
		//
		set_rt_prio();
		create_process_pair();
		/* NOTREACHED */
	default:
		clexec_log(CLADM_GREEN, "daemonize fork\n");
		return;
	}
}

static void
wait_for_daemon(void)
{

	// Close the workerd end of the pipe.
	(void) close(parent_fd);

	//
	// Both parent and child initialize the ORB.
	// We pass in a parameter to ORB::initialize() that causes it not to
	// increase the limit on open files to RLIM_INFINITY.  We need to do
	// this because one of the programs ha_mounter exec's is the SDS
	// metaset command.  The SDS metaset command tries to cleanup all open
	// fds by closing fds from 3 to the maximum possible.  If
	// ORB::initialize() sets the file limit to RLIM_INFINITY, then this
	// takes the better part of the day to do.
	//
	if (ORB::initialize(false) != 0) {
		//
		// SCMSGS
		// @explanation
		// clexecd program was unable to initialize its interface to
		// the low-level clustering software.
		// @user_action
		// This might occur because the operator has attempted to
		// start clexecd program on a node that is booted in
		// non-cluster mode. If the node is in non-cluster mode, boot
		// it into cluster mode. If the node is already in cluster
		// mode, contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("Could not initialize the ORB.  "
		    "Exiting."));
	}


	repl_pxfs::ha_mounter_var mount;

	char name[20];
	os::sprintf(name, "ha_mounter.%d", orb_conf::node_number());
	naming::naming_context_var ctxp = ns::root_nameserver();
	CORBA::Object_var obj;
	Environment e;
	CORBA::Exception *ex;


	//
	// The parent process waits for the child to register an
	// IDL object with the name server and then exits so
	// the shell will think the command is finished.
	//

	for (;;) {
		obj = ctxp->resolve(name, e);
		ex = e.exception();
		if (ex != NULL) {
			if (naming::not_found::_exnarrow(ex) == NULL) {
				//
				// SCMSGS
				// @explanation
				// clexecd program was unable to start due to
				// an error in registering itself with the
				// low-level clustering software.
				// @user_action
				// Contact your authorized Sun service
				// provider to determine whether a workaround
				// or patch is available.
				//
				do_exit(1, SC_SYSLOG_MESSAGE("Could not resolve"
				    " '%s' in the name server.  Exiting."),
				    name);
			}
			// Wait and then try again.
			e.clear();
		} else {
			//
			// Test to see that the object we got from the
			// name server is active. If not, wait for
			// the child process to register the new
			// object.
			//
			mount = repl_pxfs::ha_mounter::_narrow(obj);
			ASSERT(!CORBA::is_nil(mount));
			bool alive = mount->is_alive(e);
			if (e.exception() == NULL && alive) {
				break;
			}
			e.clear();
		}
		os::usecsleep(100000);
		clexec_log(CLADM_GREEN, "wait ha_mounter\n");
	}
	clexec_log(CLADM_GREEN, "nameserver resolved\n");
}

static void
create_process_pair(void)
{
	int err;
	// fork worker process
	clexec_log(CLADM_GREEN, "create_process_pair fork1\n");
	(void) mutex_lock(&dbg_mutex);
	worker_pid = fork1();
	err = errno;
	(void) mutex_unlock(&dbg_mutex);

	if (worker_pid == ((pid_t)-1)) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed fork1(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// If the error number is 12 (ENOMEM), install more memory,
		// increase swap space, or reduce peak memory consumption. If
		// error number is something else, contact your authorized Sun
		// service provider to determine whether a workaround or patch
		// is available.
		//
		do_exit(err, SC_SYSLOG_MESSAGE("fork1 returned %d.  "
		    "Exiting."), err);
	}

	if (worker_pid > 0) {
		daemon_process();
	} else {
		worker_process();
		/* NOTREACHED */
	}
}

static void
worker_process(void)
{
	thread_t tid;
	struct strrecvfd recvfd;
	struct rlimit fdlim;
	unsigned int sleep_in_seconds = 10;
	int err;
	thread_t sig_tid;

	clexec_log(CLADM_GREEN, "worker_process\n");

	block_allsignals();
	//
	// Create a thread that does a wait on the worker
	// process so that we'll know when it exits.
	//

	if (thr_create(NULL, NULL, wait_for_signals,
	    (void *)worker_process_name, THR_DETACHED, &sig_tid) != 0) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed thr_create(2)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("Unable to create thread.  "
		    "Exiting."));
	}
	// Wait that thread is running
	err = 0;
	while (wait_for_signal_ready == 0) {
		os::usecsleep(100000);
		clexec_log(CLADM_GREEN, "wait signal thread\n");
		if (++err > 100) {
			//
			// SCMSGS
			// @explanation
			// clexecd program has encountered a problem with the
			// worker_process thread at initialization time.
			// @user_action
			// clexecd program will exit and node will be halted
			// or rebooted to prevent data corruption. Contact
			// your authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			do_exit(1, SC_SYSLOG_MESSAGE(
			    "clexecd: wait_for_ready worker_process"));
		}
	}

	//
	// Child process.  Close off worker_fd in the child.
	//
	if (close(worker_fd)) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed thr_create(3THR)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: close returned %d."
		    "  Exiting."), errno);
	}
	(void) setsid();
	//
	// Increase the max. number of fds we can open, we are going to consume
	// a lot of them.
	//
	if (getrlimit(RLIMIT_NOFILE, &fdlim) < 0) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed getrlimit(2)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: getrlimit returned"
		    " %d"), errno);
	}
	fdlim.rlim_cur = fdlim.rlim_max;
	if (setrlimit(RLIMIT_NOFILE, &fdlim) < 0) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed setrlimit() system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: setrlimit returned"
		    " %d"), errno);
	}

	//
	// This program gets a control fd from the parent process, and then
	// starts up a thread to handle the rest.
	//
	for (;;) {
		do {
			err = ioctl(parent_fd, I_RECVFD, &recvfd);
		} while ((err < 0) && (errno == EINTR));
		if (err < 0) {
			//
			// SCMSGS
			// @explanation
			// Parent process in the clexecd program is dead.
			// @user_action
			// If the node is shutting down, ignore the message.
			// If not, the node on which this message is seen,
			// will shutdown to prevent to prevent data
			// corruption. Contact your authorized Sun service
			// provider to determine whether a workaround or patch
			// is available.
			//
			do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: Exiting "
			    "as error '%s' occured while receiving control fd "
			    "from parent. Parent could be dead."),
			    strerror(errno));
		}

		//
		// Set close on exec flag, so that worker_thread's execl() call
		// do not inherit this file descriptor.
		//
		(void) fcntl(recvfd.fd, F_SETFD, FD_CLOEXEC);

		while (thr_create(NULL, NULL, worker_thread, (void *)recvfd.fd,
		    THR_DETACHED, &tid) != 0) {
			//
			// SCMSGS
			// @explanation
			// clexecd program has encountered a failed
			// thr_create() system call. The error message
			// indicates the error number for the failure. It will
			// retry the system call after specified time.
			// @user_action
			// If the message is seen repeatedly, contact your
			// authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			log_error(SC_SYSLOG_MESSAGE("clexecd: Could not create "
			    "thread.  Error: %d.  Sleeping for %d seconds and "
			    "retrying."), errno, sleep_in_seconds);
			(void) sleep(sleep_in_seconds);
		}
	}
	/* NOTREACHED */
}

static void
daemon_process(void)
{
	int err;

	clexec_log(CLADM_GREEN, "daemon_process\n");

	// Close the workerd end of the pipe.
	(void) close(parent_fd);

	block_allsignals();

	//
	// Both parent and child initialize the ORB.
	// We pass in a parameter to ORB::initialize() that causes it not to
	// increase the limit on open files to RLIM_INFINITY.  We need to do
	// this because one of the programs ha_mounter exec's is the SDS
	// metaset command.  The SDS metaset command tries to cleanup all open
	// fds by closing fds from 3 to the maximum possible.  If
	// ORB::initialize() sets the file limit to RLIM_INFINITY, then this
	// takes the better part of the day to do.
	//
	if (ORB::initialize(false) != 0) {
		do_exit(1, SC_SYSLOG_MESSAGE("Could not initialize the ORB.  "
		    "Exiting."));
	}

	repl_pxfs::ha_mounter_var mount;

	char name[20];
	os::sprintf(name, "ha_mounter.%d", orb_conf::node_number());
	naming::naming_context_var ctxp = ns::root_nameserver();
	CORBA::Object_var obj;
	Environment e;
	CORBA::Exception *ex;

	//
	// The child process checks to see if there is already a process
	// running and doesn't start another.
	//
	obj = ctxp->resolve(name, e);
	ex = e.exception();
	if (ex != NULL) {
		if (naming::not_found::_exnarrow(ex) == NULL) {
			do_exit(1, SC_SYSLOG_MESSAGE("Could not resolve '%s' "
			    "in the name server.  Exiting."), name);
		}
		e.clear();
	} else {
		//
		// Test to see that the object we got from the
		// name server is active. If yes, don't start a new process.
		//
		mount = repl_pxfs::ha_mounter::_narrow(obj);
		ASSERT(!CORBA::is_nil(mount));
		bool alive = mount->is_alive(e);
		if (e.exception() == NULL && alive) {
			//
			// SCMSGS
			// @explanation
			// An active instance of clexecd program is already
			// running on the node.
			// @user_action
			// This would usually happen if the operator tries to
			// start the clexecd program by hand on a node which
			// is booted in cluster mode. If thats not the case,
			// contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(1, SC_SYSLOG_MESSAGE("Found another active "
			    "instance of clexecd.  Exiting daemon_process."));
		}
		e.clear();
	}

	//
	// Create and arm a failfast object so that if clexecd dies, the
	// node will be forced to panic.
	//
	create_and_arm_failfast();

	//
	// Get an fd to '/dev/zero' to pass to all exec'ing processes.
	// We should do this before binding with the nameserver because as soon
	// as the bind is done, we may be executing programs.
	//
	if ((common_stdin_fd = open("/dev/zero", O_RDONLY)) == -1) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed open(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: Error %d from "
		    "open(/dev/zero).  Exiting."), errno);
	}

	// Daemonize.
	(void) close(0);
	(void) close(1);
	(void) close(2);
	(void) setsid();

	thread_t ntid;
	thread_t sig_tid;

	//
	// Create a thread that does a wait on the worker
	// process so that we'll know when it exits
	//

	if (thr_create(NULL, NULL, wait_for_signals,
	    (void *)daemon_process_name, THR_DETACHED, &sig_tid) != 0) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed thr_create(2)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("Unable to create thread. "
		    "Exiting.\n"));
	}

	//
	// Create a thread that does a wait on the worker
	// process so that we'll know when it exits.
	//

	if (thr_create(NULL, NULL, wait_for_child,
			NULL, THR_DETACHED, &ntid) != 0) {
		do_exit(errno, SC_SYSLOG_MESSAGE("Unable to create thread. "
		    "Exiting.\n"));
	}

	// Wait until both thread are running
	err = 0;
	while (wait_for_signal_ready == 0 && wait_for_child_ready == 0) {
		os::usecsleep(100000);
		clexec_log(CLADM_GREEN, "wait for threads\n");
		if (++err > 100) {
			//
			// SCMSGS
			// @explanation
			// clexecd program has encountered a problem with the
			// wait_for_ready thread at initialization time.
			// @user_action
			// clexecd program will exit and node will be halted
			// or rebooted to prevent data corruption. Contact
			// your authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			do_exit(1, SC_SYSLOG_MESSAGE(
			    "clexecd: wait_for_ready daemon"));
		}
	}


	//
	// Now we are ready to accept connection.
	// The child process registers an IDL object with the name server
	// and stays around to process requests.
	//
	mount = (new mounter)->get_objref();
	ctxp->rebind(name, mount, e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// clexecd program was unable to start because of some
		// problems in the low-level clustering software.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("Error binding '%s' in the name "
		    "server.  Exiting."), name);
	}
	clexec_log(CLADM_GREEN, "daemon_process ready\n");
	thr_exit(0);
	// not reached
}
//
//
//
// Wait until child dies. It is used to kill the parent when this happens.
//

void *
wait_for_child(void *)
{
	int status;
	pid_t child_pid;

	//
	// wait can return with EINTR when a thread forks.
	//
	wait_for_child_ready++;
	do {
		child_pid = wait(&status);
	} while ((child_pid < 0) && (errno == EINTR));

	// Just make sure it's the worker process that died

	if (child_pid == worker_pid) {
		//
		// SCMSGS
		// @explanation
		// Child process in the clexecd program is dead.
		// @user_action
		// If this message is seen when the node is shutting down,
		// ignore the message. If thats not the case, the node will
		// halt or reboot itself to prevent data corruption. Contact
		// your authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		do_exit(EINTR, SC_SYSLOG_MESSAGE("clexecd: Daemon exiting "
		    "because child died."));
	}

	return (NULL);
}		

//
// Helper function to send an FD across a pipe.
//
static int
send_fd(int channel, int fd)
{
#define	NUM_SENDFD_RETRIES	10
	int num_retries = 0;

	if ((channel < 0) || (fd < 0)) {
		clexec_log(CLADM_RED, "send_fd %d %d\n", channel, fd);
		return (1);
	}
	for (;;) {
		if (ioctl(channel, I_SENDFD, fd) < 0) {
			// If this is retryable, wait a short time & try again
			if (errno != EAGAIN) {
				// The worker process is dead - exit.
				do_exit(errno,
					//
					// SCMSGS
					// @explanation
					// There was some error in setting up
					// interprocess communication in the
					// clexecd program.
					// @user_action
					// Contact your authorized Sun service
					// provider to determine whether a
					// workaround or patch is available.
					//
					SC_SYSLOG_MESSAGE("clexecd: Sending fd "
					"to workerd returned %d.  Exiting."),
					errno);
			} else {
				if (num_retries++ == NUM_SENDFD_RETRIES)
					break;
				else
					(void) sleep(1);
			}
		}
		else
			return (0);
	}
	// The worker process is always busy - exit.
	do_exit(errno,
		//
		// SCMSGS
		// @explanation
		// clexecd has tried repeatedly to set up interprocess
		// communcation, but each time the error EAGAIN occured.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		SC_SYSLOG_MESSAGE("clexecd: Sending fd to workerd "
		    "repeatedly busy. Attempted %d times.  Exiting."),
		    num_retries);
	/* NOTREACHED */
}

//
// Helper function that reads from 'fd' and syslogs the message.
//
// XXX This function needs cleanup.  See comments below.
//
static void
read_fd(os::sc_syslog_msg &msg, const char *prefix, int fd, int logpri)
{
	ssize_t len;
	char rdbuf[MAXPATHLEN];
	size_t buf_len = MAXPATHLEN;

	//
	// Read data up to MAXPATHLEN - 1 (save enough space to add
	// a NULL to the rdbuf) with each pass through the outer loop.
	// Send the buffer to syslog with each pass.
	// XXX If we make more than one pass through the outer loop,
	// the syslog messages will be chopped off into arbitrary
	// pieces, which will make them difficult to read.  This should
	// be fixed.
	//
	do {
		do {
			len = read(fd, rdbuf, buf_len - 1);
		} while ((len == -1) && (errno == EINTR));
		if (len == -1) {
			//
			// SCMSGS
			// @explanation
			// An error was encountered in the clexecd program
			// while reading the data from the worker process.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			log_error(SC_SYSLOG_MESSAGE("(%s): Error %d from read"),
			    prefix, errno);
			return;
		}

		if (len == 0) {
			// No more data - return.
			return;
		}

		rdbuf[len] = 0;
		//
		// Log stderr with SC_SYSLOG_ERROR so that it
		// will go to /dev/console (depends on /etc/syslog.conf,
		// but default is to send *.err to /dev/console) and
		// /var/adm/messages.
		//
		clexec_log(CLADM_GREEN, "stdout: %s\n", rdbuf);
		(void) mutex_lock(&dbg_mutex);
		(void) msg.log(logpri, MESSAGE, NO_SC_SYSLOG_MESSAGE("%s: %s"),
		    prefix, rdbuf);
		(void) mutex_unlock(&dbg_mutex);
	} while (len == (ssize_t)buf_len);
}

//
// Helper function used to set O_NDELAY on the file descriptor 'fd'.
//
static void
set_ndelay(int fd)
{
	int flags = fcntl(fd, F_GETFD, NULL);
	if (flags == -1) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed fcntl(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: Error %d in "
		    "fcntl(F_GETFD).  Exiting."), errno);
	}
	flags |= O_NDELAY;
	if (fcntl(fd, F_SETFD, flags) == -1) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed fcntl(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: Error %d from "
		    "F_SETFD.  Exiting."), errno);
	}
}

//
// This routine sends the program to be executed to the worker process, and
// waits for the result to be sent back.
// It can be easily enhanced to take the stdin, stdout and stderr streams of
// the program to be run as arguments.
// The new interface is part of fix for BugId 43372778.  Note that
// rt_prio is assumed to be false at this time, and run_in_fore_ground
// and log are ignored.  (Some run_reserve commands reset their
// priorities in a separate script.)
//
static int
execit(const char *str, const bool rt_prio, const bool run_in_foreground,
    const bool log, int &ret_val)
{
	int err;
	int result;
	int flags;
	struct pollfd pfds[3];
	int control_fds[2];
	int stdout_fds[2];
	int stderr_fds[2];
	execit_buf_t *execit_msg;
	struct strbuf data;

	//
	// This is the check to make sure we don't overflow the execit_t
	// buffer.
	//
	if (strlen(str) > EXECIT_MAX_CMD_LEN) {
		return (E2BIG);
	}

	execit_msg = new execit_buf_t(rt_prio, run_in_foreground, log, str);
	if (execit_msg == NULL) {
		//
		// SCMSGS
		// @explanation
		// Could not allocate memory. Node is too low on memory.
		// @user_action
		// clexecd program will exit and node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		log_error(SC_SYSLOG_MESSAGE(
		    "clexecd: can allocate execit_msg"));
		return (ENOMEM);
	}

	// Set up a syslog handler for this program.
	(void) mutex_lock(&dbg_mutex);
	os::sc_syslog_msg msg("", str, NULL);
	(void) mutex_unlock(&dbg_mutex);

	//
	// Set up a control pipe to hand off to the worker process.
	//
	if (pipe(control_fds) != 0) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed pipe(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		log_error(SC_SYSLOG_MESSAGE("clexecd: Error %d from pipe"),
		    err);
		delete execit_msg;
		return (err);
	}

	//
	// Create a listener fd and thread for stdout of the process to be
	// executed.
	//
	if (pipe(stdout_fds) != 0) {
		err = errno;
		log_error(SC_SYSLOG_MESSAGE("clexecd: Error %d from pipe"),
		    err);
		delete execit_msg;
		return (err);
	}
	set_ndelay(stdout_fds[0]);

	//
	// Create a listener fd and thread for stderr of the process to be
	// executed.
	//
	if (pipe(stderr_fds) != 0) {
		err = errno;
		log_error(SC_SYSLOG_MESSAGE("clexecd: Error %d from pipe"),
		    err);
		delete execit_msg;
		return (err);
	}
	set_ndelay(stderr_fds[0]);

	//
	// Send the other end of the control pipe to the worker thread.
	//
	if (send_fd(worker_fd, control_fds[1])) {
		// The worker process is dead - exit.

		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed fcntl(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// The node will halt or reboot itself to prevent data
		// corruption. Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("clexecd: Sending fd on "
		    "common channel returned %d.  Exiting."), errno);
	}
	(void) close(control_fds[1]);


	data.buf = (char *)execit_msg;
	data.len = (int)(execit_msg->transmission_length());

	//
	// Send the program to be run on the control pipe.
	//
	do {
		result = putmsg(control_fds[0], NULL, &data, 0);
		err = errno;
	} while ((result < 0) && (err == EINTR || err == EAGAIN));
	if (result < 0) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed putmsg(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		log_error(SC_SYSLOG_MESSAGE("clexecd: Error %d from putmsg"),
		    err);
		delete execit_msg;
		return (err);
	}

	//
	// Send the std{in,out,err} streams on the control pipe.
	//
	if (send_fd(control_fds[0], common_stdin_fd)) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed fcntl(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		log_error(SC_SYSLOG_MESSAGE("clexecd: Error %d from send_fd"),
		    errno);
		delete execit_msg;
		return (1);
	}

	if (send_fd(control_fds[0], stdout_fds[1])) {
		log_error(SC_SYSLOG_MESSAGE("clexecd: Error %d from send_fd"),
		    errno);
		delete execit_msg;
		return (1);
	}
	(void) close(stdout_fds[1]);

	if (send_fd(control_fds[0], stderr_fds[1])) {
		log_error(SC_SYSLOG_MESSAGE("clexecd: Error %d from send_fd"),
		    errno);
		delete execit_msg;
		return (1);
	}
	(void) close(stderr_fds[1]);

	//
	// Set up the pollfd data structure to listen to the open pipes.
	//
	pfds[0].fd = control_fds[0];
	pfds[1].fd = stdout_fds[0];
	pfds[2].fd = stderr_fds[0];
	pfds[0].events = pfds[1].events = pfds[2].events = POLLIN;
	pfds[0].revents = pfds[1].revents = pfds[2].revents = 0;

	bool done = false;

	while (!done) {
		// Do a blocking poll.  Retry if interrupted.
		do {
			result = poll(pfds, 3, INFTIM);
			err = errno;
		} while ((result < 0) && (err == EINTR || err == EAGAIN));
		if (result < 0) {
			// Error - exit.

			//
			// SCMSGS
			// @explanation
			// clexecd program has encountered a failed poll(2)
			// system call. The error message indicates the error
			// number for the failure.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(1, SC_SYSLOG_MESSAGE("clexecd: Error %d "
			    "from poll.  Exiting."), err);
		}

		if (pfds[0].revents != 0) {
			if (pfds[0].revents & (POLLERR | POLLNVAL)) {
				//
				// SCMSGS
				// @explanation
				// clexecd program has encountered an error.
				// @user_action
				// The clexecd program will exit and the node
				// will be halted or rebooted to prevent data
				// corruption. Contact your authorized Sun
				// service provider to determine whether a
				// workaround or patch is available.
				//
				do_exit(1, SC_SYSLOG_MESSAGE("Got back %d in "
				    "revents of the control fd.  Exiting."),
				    pfds[0].revents);
			}
			if (pfds[0].revents & POLLIN) {
				//
				// Get back the exit code of the process on
				// the communication pipe.
				//
				done = true;
				data.maxlen = sizeof (execit_buf_t);
				data.buf = (char *)execit_msg;

				// Retry getmsg() if it gets interrupted.
				do {
					flags = 0;
					err = getmsg(pfds[0].fd, NULL, &data,
					    &flags);
				} while ((err < 0) && (errno == EINTR));
				if (err < 0) {
					//
					// SCMSGS
					// @explanation
					// clexecd program has encountered a
					// failed getmsg(2) system call. The
					// error message indicates the error
					// number for the failure.
					// @user_action
					// The clexecd program will exit and
					// the node will be halted or rebooted
					// to prevent data corruption. Contact
					// your authorized Sun service
					// provider to determine whether a
					// workaround or patch is available.
					//
					do_exit(1, SC_SYSLOG_MESSAGE(
					    "clexecd: getmsg returned %d.  "
					    "Exiting."), errno);
				}
				if (err > 0) {
					clexec_log(CLADM_RED,
					    "getmsg %d flags %x\n", err, flags);
				}
				// XXX why's this here?
				data.buf[data.len] = 0;
			} else if (pfds[0].revents & POLLHUP) {
				done = true;
				data.buf[0] = 0;
				//
				// SCMSGS
				// @explanation
				// clexecd program got an error while
				// executing the program indicated in the
				// error message.
				// @user_action
				// Please check the error message. Contact
				// your authorized Sun service provider to
				// determine whether a workaround or patch is
				// available.
				//
				log_error(SC_SYSLOG_MESSAGE("Unexpected early "
				    "exit while performing: '%s'"
				    " result %d revents %x"),
				    str, result, pfds[0].revents);
			} else {
				//
				// BugId 4292455
				//
				done = true;
				data.buf[0] = 0;
				//
				// SCMSGS
				// @explanation
				// clexecd program has encountered an error.
				// @user_action
				// Contact your authorized Sun service
				// provider to determine whether a workaround
				// or patch is available.
				//
				log_error(SC_SYSLOG_MESSAGE(
				    "Unexpected eventmask"
				    " while performing: '%s'"
				    " result %d revents %x"),
				    str, result, pfds[0].revents);
			}
		}

		// syslog stdout
		if (pfds[1].revents != 0) {
			if (pfds[1].revents & POLLIN) {
				read_fd(msg, "stdout", pfds[1].fd,
				    SC_SYSLOG_NOTICE);
			}
			if (pfds[1].revents & (POLLERR | POLLNVAL | POLLHUP)) {
				// Drop this fd from the list to be 'poll'ed.
				pfds[1].fd = -1;
			}
		}

		// syslog stderr
		if (pfds[2].revents != 0) {
			if (pfds[2].revents & POLLIN) {
				read_fd(msg, "stderr", pfds[2].fd,
				    SC_SYSLOG_ERROR);
			}
			if (pfds[2].revents & (POLLERR | POLLNVAL | POLLHUP)) {
				// Drop this fd from the list to be 'poll'ed.
				pfds[2].fd = -1;
			}
		}
	}

	(void) close(control_fds[0]);
	(void) close(stdout_fds[0]);
	(void) close(stderr_fds[0]);

	ret_val = atoi(execit_msg->get_cmdbuf());

	delete execit_msg;

	return (ret_val);
}

//
// This is the thread that does all the work of exec'ing a process.
//
void *
worker_thread(void *arg)
{
	struct strbuf data;
	int flags;
	int fd = (int)arg;
	int t_stdin, t_stdout, t_stderr;
	struct strrecvfd recvfd;
	pid_t _pid;
	pid_t child_pid;
	int ret_val;
	int err;
	execit_buf_t *execit_msg;

	execit_msg = new execit_buf_t(0, 0, 0, "");
	if (execit_msg == NULL) {
		//
		// SCMSGS
		// @explanation
		// Could not allocate memory. Node is too low on memory.
		// @user_action
		// clexecd program will exit and node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("clexecd: "
		    "Can allocate execit_msg. Exiting."));
	}

	// Get the user program to execute.
	data.maxlen = sizeof (execit_buf_t);
	data.buf = (char *)execit_msg;

	// Retry getmsg() if it gets interrupted.
	do {
		flags = 0;
		err = getmsg(fd, NULL, &data, &flags);
	} while ((err < 0) && (errno == EINTR));
	if (err < 0) {
		do_exit(1, SC_SYSLOG_MESSAGE("clexecd: getmsg returned "
		    "%d.  Exiting."), errno);
	}
	if (err > 0) {
		clexec_log(CLADM_RED, "getmsg %d flags %x\n", err, flags);
	}

	// Get fd's for stdin, stdout, and stderr.
	if (ioctl(fd, I_RECVFD, &recvfd) < 0) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed ioctl(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		log_error(SC_SYSLOG_MESSAGE("clexecd: ioctl(I_RECVFD) returned "
		    "%d.  Returning %d to clexecd."), err, err);
		ret_val = err;
		goto out;
	}
	t_stdin = recvfd.fd;

	if (ioctl(fd, I_RECVFD, &recvfd) < 0) {
		err = errno;
		(void) close(t_stdin);
		log_error(SC_SYSLOG_MESSAGE("clexecd: ioctl(I_RECVFD) returned "
		    "%d.  Returning %d to clexecd."), err, err);
		ret_val = err;
		goto out;
	}
	t_stdout = recvfd.fd;

	if (ioctl(fd, I_RECVFD, &recvfd) < 0) {
		err = errno;
		(void) close(t_stdin);
		(void) close(t_stdout);
		log_error(SC_SYSLOG_MESSAGE("clexecd: ioctl(I_RECVFD) returned "
		    "%d.  Returning %d to clexecd."), err, err);
		ret_val = err;
		goto out;
	}
	t_stderr = recvfd.fd;

	clexec_log(CLADM_GREEN, "worker_thread fork1 <%s> fd %d\n",
	    execit_msg->get_cmdbuf(), fd);
	(void) mutex_lock(&dbg_mutex);
	_pid = fork1();
	err = errno;
	(void) mutex_unlock(&dbg_mutex);

	switch (_pid) {
	case ((pid_t)-1) :
		(void) close(t_stdin);
		(void) close(t_stdout);
		(void) close(t_stderr);
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed fork1(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// If the error number is 12 (ENOMEM), install more memory,
		// increase swap space, or reduce peak memory consumption. If
		// error number is something else, contact your authorized Sun
		// service provider to determine whether a workaround or patch
		// is available.
		//
		log_error(SC_SYSLOG_MESSAGE("clexecd: fork1 returned %d.  "
		    "Returning %d to clexecd."), err, err);
		ret_val = err;
		goto out;

	case 0:
		// Close off unnecessary fd's in the child process.
		if (close(0) || close(1) || close(2) || close(parent_fd)) {
			//
			// SCMSGS
			// @explanation
			// clexecd program has encountered a failed close(2)
			// system call. The error message indicates the error
			// number for the failure.
			// @user_action
			// The clexecd program will exit and the node will be
			// halted or rebooted to prevent data corruption.
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: close "
			    "returned %d while exec'ing (%s).  Exiting."),
			    errno, execit_msg->get_cmdbuf());
		}
		// Graft 'stdin', stdout', and 'stderr' in place.
		if ((dup2(t_stdin, 0)) < 0) {
			//
			// SCMSGS
			// @explanation
			// clexecd program has encountered a failed dup2(2)
			// system call. The error message indicates the error
			// number for the failure.
			// @user_action
			// The clexecd program will exit and the node will be
			// halted or rebooted to prevent data corruption.
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: dup2 of "
			    "stdin returned with errno %d while exec'ing (%s)."
			    "  Exiting."), errno, execit_msg->get_cmdbuf());
		}
		(void) close(t_stdin);
		if ((dup2(t_stdout, 1)) < 0) {
			//
			// SCMSGS
			// @explanation
			// clexecd program has encountered a failed dup2(2)
			// system call. The error message indicates the error
			// number for the failure.
			// @user_action
			// The clexecd program will exit and the node will be
			// halted or rebooted to prevent data corruption.
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: dup2 of "
			    "stdout returned with errno %d while exec'ing (%s)."
			    "  Exiting."), errno, execit_msg->get_cmdbuf());
		}
		(void) close(t_stdout);
		if ((dup2(t_stderr, 2)) < 0) {
			//
			// SCMSGS
			// @explanation
			// clexecd program has encountered a failed dup2(2)
			// system call. The error message indicates the error
			// number for the failure.
			// @user_action
			// The clexecd program will exit and the node will be
			// halted or rebooted to prevent data corruption.
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: dup2 of "
			    "stderr returned with errno %d while exec'ing (%s)."
			    "  Exiting."), errno, execit_msg->get_cmdbuf());
		}
		(void) close(t_stderr);

		//
		// if shell should execute command in TS class
		//
		if (!execit_msg->get_rt_prio()) {
			ts_setprio();
		}

		clexec_log(CLADM_GREEN, "execl <%s>\n",
		    execit_msg->get_cmdbuf());
		(void) execl("/bin/sh", "sh", "-c",
			execit_msg->get_cmdbuf(), (char *)0);
		err = errno;
		clexec_log(CLADM_GREEN, "execl %s exit %d\n",
		    execit_msg->get_cmdbuf(), err);
		exit(err);

	default:
		break;
	}

	// The parent closes off unnecessary fd's.
	(void) close(t_stdin);
	(void) close(t_stdout);
	(void) close(t_stderr);

	//
	// Wait for the child to exit.
	// waitpid() can return with EINTR when a thread forks.
	//
	do {
		child_pid = waitpid(_pid, &ret_val, 0);
	} while ((child_pid < 0) && (errno == EINTR));
	err = errno;

	if (child_pid != _pid) {
	    //
	    // SCMSGS
	    // @explanation
	    // clexecd program has encountered a failed waitpid(2) system
	    // call. The error message indicates the error number for the
	    // failure.
	    // @user_action
	    // clexecd program will exit and node will be halted or rebooted
	    // to prevent data corruption. Contact your authorized Sun service
	    // provider to determine whether a workaround or patch is
	    // available.
	    //
	    log_error(SC_SYSLOG_MESSAGE("clexecd: waitpid returned %d.  "),
		err);
		ret_val = err;
		goto out;
	}

	if (WIFSIGNALED(ret_val)) {
		//
		// Child received a signal.  This is unexpected - return an
		// error to the callerr.
		//
		ret_val = ECANCELED;
	} else {
		ret_val = WEXITSTATUS(ret_val);	/*lint !e702 */
	}

out:
	//
	// We could have added another field to the execit_t struct to
	// pass back the return value, but this is how the code worked
	// before.
	//
	(void) sprintf((char *)execit_msg->get_cmdbuf(), "%d", ret_val);
	data.len = (int)(execit_msg->transmission_length());
	clexec_log(CLADM_GREEN, "<%s> fd %d retval %d data.len %d\n",
	    execit_msg->get_cmdbuf(), fd, ret_val, data.len);
	do {
		ret_val = putmsg(fd, NULL, &data, 0);
		err = errno;
	} while ((ret_val < 0) && (err == EINTR || err == EAGAIN));
	if (ret_val < 0) {
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed putmsg(2) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		log_error(SC_SYSLOG_MESSAGE(
		    "clexecd: putmsg returned %d."), err);
	}
	(void) close(fd);
	delete execit_msg;
	return (NULL);
}

//
// Execute the program specified by 'cmd' in user space.
// This is the new interface to exec_program().  Note that
// we do nothing yet with run_in_foreground and log.
//
void
mounter::exec_program_with_opts(const char *cmd, bool rt_prio,
    bool run_in_foreground,
    bool log, Environment &env)
{
	int ret_val;

	//
	// We check against entering exec_program_with_opts
	// with an uncleared environment.
	//
	ASSERT(!env.exception());

	clexec_log(CLADM_GREEN, "execit<%s>\n", cmd);

	int error = execit(cmd, rt_prio, run_in_foreground, log, ret_val);

	clexec_log(CLADM_GREEN, "execit<%s> error %d\n", cmd, error);

	if (error != 0) {
		ret_val = error;
	}

	if (ret_val != 0) {
		env.exception(new sol::op_e((int32_t)ret_val));
	}
}

//
// Execute the program specified by 'cmd' in user space.  Called by
// pxfs components that want the default arguments to execit.
// (Also a convenient way to not have to change existing call interfaces
// to exec_program by various pxfs components.)
//
void
mounter::exec_program(const char *cmd, Environment &env)
{
	const bool rt_prio = false;
	const bool run_in_foreground = true;
	const bool log = true;
	exec_program_with_opts(cmd, rt_prio, run_in_foreground,
	    log, env);
}

void
mounter::mount(const char *spec, const char *fstype, const char *mntoptions,
    Environment &_environment)
{
	//
	// We need to do a "fsck" on the raw device.
	// Since different file systems require different flags to fsck,
	// we pass the info to a shell script which figures things out.
	//
	char *cmd_string = new char[os::strlen(spec) + os::strlen(fstype) +
	    os::strlen(mntoptions) + 40];
	os::sprintf(cmd_string, "/usr/cluster/lib/sc/dofsck %s %s %s",
	    spec, fstype, mntoptions);

	int ret_val;

	const bool rt_prio = true;
	const bool run_in_foreground = true;
	const bool log = true;

	clexec_log(CLADM_GREEN, "mount <%s>\n", cmd_string);

	int error = execit(cmd_string, rt_prio,
	    run_in_foreground, log, ret_val);

	clexec_log(CLADM_GREEN, "mount <%s> error %d\n", cmd_string, error);

	if (error != 0) {
		ret_val = error;
	}

	delete [] cmd_string;
	if (ret_val != 0) {
		_environment.exception(new
		    repl_pxfs::ha_mounter::MountFailed());
	}
}

//
// This routine returns true if the ha_mounter interface
// is alive and working. It can be used to tell if the
// object reference obtained from the name server is
// valid or not.
//
bool
mounter::is_alive(Environment &)
{
	return (true);
}

//
// Helper function used by do_exit() and log_error().
//
static void
do_log(const char *format, va_list v)
{
	char work[MAXPATHLEN];
	(void) mutex_lock(&dbg_mutex);
	os::sc_syslog_msg msg("", this_prog_name, NULL);
	(void) vsnprintf(work, MAXPATHLEN -1, format, v);
	(void) mutex_unlock(&dbg_mutex);
	clexec_log(CLADM_RED, "do_log %s\n", work);
	(void) mutex_lock(&dbg_mutex);
	(void) msg.log(SC_SYSLOG_ERROR, MESSAGE, NO_SC_SYSLOG_MESSAGE(work));
	(void) mutex_unlock(&dbg_mutex);
}

//
// Helper function to log errors.
//
static void
log_error(const char *format, ...)
{
	va_list v;
	va_start(v, format);	/*lint !e40 */
	do_log(format, v);
	va_end(v);
}

//
// Helper function to log errors and exit.
//
static void
do_exit(int exit_code, const char *format, ...)
{
	va_list v;
	va_start(v, format);	/*lint !e40 */
	do_log(format, v);
	va_end(v);

	clexec_log(CLADM_RED, "do_exit %d\n", exit_code);

	exit(exit_code);
}

//
// Create and arm a failfast object so that the node dies when clexecd dies.
//

static void
create_and_arm_failfast()
{

	ff_admin_ptr = cmm_ns::get_ff_admin();
	Environment 		e;


	//
	// Use FFM_DEFERRED_PANIC to allow enough time for a core
	// dump of this process.
	//

	death_ff_v = ff_admin_ptr->ff_create("clexecd",
	    ff::FFM_DEFERRED_PANIC, e);

	if (e.exception() || CORBA::is_nil(death_ff_v)) {

		//
		// Given the lack of an error code to represent what happened
		// we choose ECOMM to best represent why we failed.
		//

		//
		// SCMSGS
		// @explanation
		// clexecd problem could not enable one of the mechanisms
		// which causes the node to be shutdown to prevent data
		// corruption, when clexecd program dies.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(ECOMM, SC_SYSLOG_MESSAGE("clexecd: unable to create "
		    "failfast object."));
	} else {

		// Generate a failfast only when this process dies.

		death_ff_v->arm(ff::FF_INFINITE, e);
		if (e.exception()) {
			//
			// SCMSGS
			// @explanation
			// clexecd problem could not enable one of the
			// mechanisms which causes the node to be shutdown to
			// prevent data corruption, when clexecd program dies.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is
			// available.
			//
			do_exit(ECOMM, SC_SYSLOG_MESSAGE("clexecd: unable to "
			    "arm failfast."));
		}
		// Trigger the failfast upon receipt of these signals
		ff_register_signal_handler(SIGILL, &death_ff_v);
		ff_register_signal_handler(SIGBUS, &death_ff_v);
		ff_register_signal_handler(SIGSEGV, &death_ff_v);
	}
}

static void *
wait_for_signals(void *arg)
{
	sigset_t	s_mask;
	char *procname = (char *)arg;
	int sig = 0;
	siginfo_t sinfo;

	if (!procname)
		//
		// SCMSGS
		// @explanation
		// clexecd problem encountered an error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("clexecd: wait_for_signals "
		    "got NULL."));

	//
	// Fill the mask with all the signals in the system
	//
	if (sigfillset(&s_mask) == -1)
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed sigfillset(3C)
		// system call. The error message indicates the error number
		// for the failure.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: %s: sigfillset "
		    "returned %d.  Exiting."), procname, errno);

	block_allsignals();

	wait_for_signal_ready++;

	//
	// sigwait can return with EINTR when a thread forks.
	// Also ignore SIGHUP. We could have simply added SIGHUP to the
	// list of signals to be masked but the check is here so that we
	// can add a syslog message if necessary for SIGHUP.
	//
	do {
		sig = sigwaitinfo(&s_mask, &sinfo);
		clexec_log(CLADM_GREEN, "catch signal %d "
			"%d si_code %x si_pid %ld si_uid %ld\n",
			sig,
			sinfo.si_signo,
			sinfo.si_code,
			sinfo.si_pid,
			sinfo.si_uid);
	} while (((sig < 0) && (errno == EINTR))
	      || (sig == SIGHUP) || (sig == SIGCHLD));

	switch (sig) {
	case -1:
		//
		// SCMSGS
		// @explanation
		// clexecd program has encountered a failed sigwait(3C) system
		// call. The error message indicates the error number for the
		// failure.
		// @user_action
		// The clexecd program will exit and the node will be halted
		// or rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		do_exit(errno, SC_SYSLOG_MESSAGE("clexecd: %s: sigwait "
		    "returned %d.  Exiting."), procname, errno);
		/* NOTREACHED */
	case SIGTERM:
		//
		// When we get SIGTERM log the fact so that we will know when
		// we terminate normally versus abnormally
		// Also log information about the signal emitter to debug
		// buffer for debugging purposes.
		//
		log_signal_emitter(sinfo.si_pid);
		//
		// SCMSGS
		// @explanation
		// clexecd program got a signal indicated in the error
		// message.
		// @user_action
		// clexecd program will exit and node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		do_exit(0, SC_SYSLOG_MESSAGE("clexecd: Going down on "
		    "signal %d."), sig);
		/* NOTREACHED */
	default:
		/* Log also the signal emitter */
		log_signal_emitter(sinfo.si_pid);
		//
		// SCMSGS
		// @explanation
		// clexecd program got a signal indicated in the error
		// message.
		// @user_action
		// clexecd program will exit and node will be halted or
		// rebooted to prevent data corruption. Contact your
		// authorized Sun service provider to determine whether a
		// workaround or patch is available.
		//
		do_exit(1, SC_SYSLOG_MESSAGE("clexecd: Got an unexpected"
			    " signal %d in process %s (pid=%d, ppid=%d)"), sig,
			    procname, getpid(), getppid());
	}
	/* NOTREACHED */
	return ((void *)0);
}

//
// Function to log about the signal emitter.
//
static void
log_signal_emitter(pid_t se_pid)
{
	char cmd[32];
	char prbuf[256];
	FILE *ptr;

	(void) sprintf(cmd, "/usr/bin/ptree %u", se_pid);
	if ((ptr = popen(cmd, "r")) != NULL) {
		while (fgets(prbuf, 256, ptr) != NULL) {
			clexec_log(CLADM_GREEN, "ptree %u:%s\n", se_pid, prbuf);
		}
		(void) pclose(ptr);
	}
}

static void
create_end_file()
{
	int fd;
	//
	// This used only to catch unexpected exit(0), see 4470318
	//
	if ((fd = creat(CLEXECD_FILE, 0600)) < 0) {
		do_exit(errno, "unable to create end file"
		    " %s errno %d", CLEXECD_FILE, errno);
	}
	(void) close(fd);
	clexec_log(CLADM_GREEN, "end file created\n");
}


/*
 * Add a printf-style message to whichever debug logs we're currently using.
 */
static void
clexec_log(int level, const char *fmt, ...)
{
	va_list ap;
	cladmin_log_t *tolog;
	size_t len;

	(void) mutex_lock(&dbg_mutex);

	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, fmt);
	/*lint +e40 */

	tolog = (cladmin_log_t *)malloc(sizeof (cladmin_log_t));
	if (tolog == NULL) {
		va_end(ap);
		(void) mutex_unlock(&dbg_mutex);
		return;
	}

	tolog->level = level;
	(void) snprintf(tolog->str, CLADMIN_LOG_SIZE - 1,
	    "clexec%u,%u:", getpid(), thr_self());
	len = strlen(tolog->str);
	(void) vsnprintf(tolog->str + len, CLADMIN_LOG_SIZE - 1 - len, fmt, ap);
	(void) os::cladm(CL_CONFIG, CL_ADMIN_LOG, (void *)tolog);
	free(tolog);
	va_end(ap);
	(void) mutex_unlock(&dbg_mutex);
}
