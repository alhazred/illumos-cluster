/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_QUERY_VM_H
#define	_QUERY_VM_H

#pragma ident	"@(#)query_vm.h	1.3	08/05/20 SMI"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <orb/object/adapter.h>
#include <sys/os.h>
#include <sys/clconf_int.h>
#include <sys/vm_util.h>
#include <orb/infrastructure/orb_conf.h>

#define	MAXSTRING	256
#define	MAXELEM	16

char *subopts[] = {
#define	VP	0
	"vp",
#define	UCC	1
	"ucc",
#define	MAJ_NUM	2
	"maj",
#define	MIN_NUM	3
	"min",
#define	B	4
	"b",
#define	C	5
	"c",
#define	F	6
	"f",
	NULL};

enum q_type {
	Q_NONE = 0,
	Q_LIST = 1,
	Q_RV = 2,
	Q_NPRV = 3,
	Q_DPRINT = 4,
	Q_REGUCC = 5,
	Q_UNREGUCC = 6
};

static char	*last_path_elem(char *path);
static char	*program_name;
int		query_vm(int argc, char *argv[]);

class test_upgrade_callback :
	public McServerof<version_manager::upgrade_callback> {
public:
	test_upgrade_callback();
	~test_upgrade_callback();
	void _unreferenced(unref_t cookie);
	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version, Environment &e);
};

#endif	/* _QUERY_VM_H */
