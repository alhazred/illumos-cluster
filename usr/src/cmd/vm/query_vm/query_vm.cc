//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)query_vm.cc	1.20	08/05/20 SMI"

#include "query_vm.h"

void
usage()
{
	os::printf("usage: %s args\n", program_name);
	os::printf("where args can be one of:\n"
	    "\t-l                   	- List all local versioned protocols\n"
	    "\t-d string            	- Make the debug request associated\n"
	    "\t                     	- with 'string'\n"
	    "\t-r vp_name           	- Get the running version for vp_name\n"
	    "\t-n node_id -p vp_name	- Get the rv for node-pair mode \n"
	    "\t                     	  vp_name with other node node_id \n"
	    "\t-s                   	- Tab delimit output for scripts \n"
	    "\t-N node_id          	- Use -N to query a different node \n"
	    "\t-U ucc_name		- Unregister a ucc\n"
	    "\t			  (Unregister must be invoked on all nodes)\n"
	    "\t-R ucc=ucc_name,        - Register the specified ucc\n"
	    "\t   vp=vp_name,\n"
	    "\t   maj=major_number,\n"
	    "\t   min=minor number,\n"
	    "\t   [b=\"ucc_1 ... ucc_16\"],   	(Up to 16 entries may be \n"
	    "\t   [c=\"ucc_1 ... ucc_16\"],	defined by the double quoted\n"
	    "\t   [f=\"serv_1 ... serv_16\"]	enclosed fields)\n"
	    "\t-w                   	- Wait for ucc to be unreferenced\n"
	    "\t                     	  before exiting (valid only in\n"
	    "\t                     	  conjunction with -R)\n");
}

#ifdef _KERNEL_ORB
int
unode_init()
{
	return (0);
}
#else
int
main(int argc, char *argv[])
{
	int error = clconf_lib_init();
	int res;
	if (error) {
		os::printf("%s: clconf_lib_init() returned error %d\n",
			argv[0], error);
		exit(1);
	}

	res = query_vm(argc, argv);
	clconf_lib_shutdown();
	exit(res);
}
#endif

//
// Mutex, boolean and condition variable used for ucc unreferenced synchro.
//
os::condvar_t			wait_cond;
os::mutex_t			wait_lock;
bool 				go_ahead = false;

// Called from main() or via unode_load.
int
query_vm(int argc, char *argv[])
{
	Environment			e;
	nodeid_t			q_node = orb_conf::local_nodeid();
	nodeid_t			nodepair_node = NODEID_UNKNOWN;
	q_type				qt = Q_NONE;
	char 				*vpname = NULL;
	char 				*ucc = NULL;
	int 				maj = -1;
	int 				minor_num = -1;
	char 				*bstring = NULL;
	char 				*cstring = NULL;
	char 				*fstring = NULL;
	char 				*dstring = NULL;
	CORBA::String_var 		outstring;
	char 				*options = NULL;
	char 				*value = NULL;
	int				c;
	bool				scriptoutput = false;
	bool				waitunref = false;

	program_name = last_path_elem(argv[0]);

	// Reset optind since we can be called multiple times from unode.
	optind = 1;
	while ((c = getopt(argc, argv, "lswd:r:n:p:N:R:U:?")) != EOF) {
		switch (c) {
		case 's':
			scriptoutput = true;
			break;
		case 'w':
			waitunref = true;
			break;
		case 'N':
			q_node = (nodeid_t)atoi(optarg);
			break;
		case 'l':
			if (qt != Q_NONE) {
				usage();
				return (1);
			}
			qt = Q_LIST;
			break;
		case 'd':
			if (qt != Q_NONE) {
				usage();
				return (1);
			}
			qt = Q_DPRINT;
			if (dstring != NULL)
				delete [] dstring;
			dstring = os::strdup(optarg);
			break;
		case 'r':
			if (qt != Q_NONE) {
				usage();
				return (1);
			}
			qt = Q_RV;
			if (vpname != NULL)
				delete [] vpname;
			vpname = os::strdup(optarg);
			break;
		case 'n':
			nodepair_node = (nodeid_t)atoi(optarg);
			break;
		case 'p':
			if (qt != Q_NONE) {
				usage();
				return (1);
			}
			qt = Q_NPRV;
			vpname = os::strdup(optarg);
			break;
		case 'R':
			if (qt != Q_NONE) {
				usage();
				return (1);
			}
			qt = Q_REGUCC;
			// Parse the suboptions.
			options = optarg;
			while (*options != '\0') {
				switch (getsubopt(&options, subopts, &value)) {
				case VP : // vp=
					if (value == NULL) {
						usage();
						return (1);
					}
					if (vpname != NULL)
						delete [] vpname;
					vpname = os::strdup(value);
					break;
				case UCC : // ucc=
					if (value == NULL) {
						usage();
						return (1);
					}
					if (ucc != NULL)
						delete [] ucc;
					ucc = os::strdup(value);
					break;
				case MAJ_NUM : // maj=
					if (value == NULL) {
						usage();
						return (1);
					}
					maj = atoi(value);
					// Major number must be at least 1.
					if (maj <= 0) {
						usage();
						return (1);
					}
					break;
				case MIN_NUM : // min=
					if (value == NULL) {
						usage();
						return (1);
					}
					minor_num = atoi(value);
					// Minor number must be at least 0.
					if (maj < 0) {
						usage();
						return (1);
					}
					break;
				case B : // b=
					if (bstring != NULL)
						delete [] bstring;
					if (value != NULL) {
						bstring = os::strdup(value);
					} else {
						bstring = NULL;
					}
					break;
				case C : // c=
					if (cstring != NULL)
						delete [] cstring;
					if (value != NULL) {
						cstring = os::strdup(value);
					} else {
						cstring = NULL;
					}
					break;
				case F : // f=
					if (fstring != NULL)
						delete [] fstring;
					if (value != NULL) {
						fstring = os::strdup(value);
					} else {
						fstring = NULL;
					}
					break;
				default :
					usage();
					return (1);
				}
			}
			break;
		case 'U':
			if (qt != Q_NONE || optarg == NULL) {
				usage();
				return (1);
			}
			qt = Q_UNREGUCC;
			if (ucc != NULL)
				delete [] ucc;
			ucc = new char [strlen(optarg) + 7];
			ASSERT(ucc);
			if (strcpy(ucc, "unreg ") == NULL) {
				os::printf("Error: Unexpected error - cannot "
				    "invoke a string copy.\n");
				return (1);
			}
			if (strcat(ucc, optarg) == NULL) {
				os::printf("Error: Unexpected error - cannot "
				    "concatenate a string.\n");
				return (1);
			}
			break;
		case '?':
			usage();
			return (1);
		default:
			break;
		}
	}

	if ((waitunref) && (qt != Q_REGUCC)) {
		usage();
		return (1);
	}

	// Check to make sure the node is in the cluster membership.
	version_manager::vm_admin_var	vm_adm;
	if (!orb_conf::node_configured(q_node)) {
		os::printf("Error: node '%ld' not configured in cluster\n",
		    q_node);
		return (1);
	}
	// Get a handle to the local version manager.
	vm_adm = vm_util::get_vm(q_node);
	if (CORBA::is_nil(vm_adm)) {
		os::printf("Error: Could not get vm_admin object for %ld\n",
			q_node);
		return (1);
	}
	// Call into the version manager to carry out the command.
	version_manager::vp_info_seq *vinfo_out;
	version_manager::vp_version_t vpv;
	uint32_t i;

	switch (qt) {
	case Q_NONE:
	default:
		usage();
		return (1);
	case Q_DPRINT:
		ASSERT(dstring != NULL);
		if (strcmp(dstring, "print_uccs") == 0) {
			//
			// print_ucc is a special call - it will pass an out
			// string into debug_request_io().  The string will be
			// constructed in kernel land and then printed by
			// query_vm in userland.  (Printing from kernel land
			// goes to the system console and not stdout.)
			// % query_vm can print its output to stdout and that
			// output can be redirected, captured, and analyzed to
			// support the automated Version Manager tests.
			//
			vm_adm->debug_request_io(dstring, outstring, e);
			if (e.exception()) {
				os::printf("Error: debug_request_io "
				    "returned system exception\n");
				return (1);
			} else {
				// Print the out string to stdout.
				os::printf("%s", (char *)outstring);
			}
		} else {
			vm_adm->debug_request(dstring, e);
			if (e.exception()) {
				os::printf("Error: debug_request "
				    "returned system exception\n");
				return (1);
			}
		}
		break;
	case Q_LIST:
		vm_adm->get_vp_list(vinfo_out, e);
		if (e.exception()) {
			os::printf("Error: get_vp_list returned exception\n");
			return (1);
		}
		{
			version_manager::vp_info_seq &vio = *vinfo_out;
			for (i = 0; i < vio.length(); i++) {
				os::printf((scriptoutput ? "%s\t%s\t%s\n" :
				    "Name: %s\n\tDescription: %s\n\t"
				    "Source: %s\n\n"),
				    (const char *)vio[i].vp_name,
				    (const char *)vio[i].vp_desc,
				    (const char *)vio[i].vp_source);
			}
		}
		delete vinfo_out;
		break;
	case Q_RV:
		if (!vpname) {
			usage();
			return (1);
		}
		vm_adm->get_running_version(vpname, vpv, e);
		if (e.exception()) {
			if (version_manager::not_found::
			    _exnarrow(e.exception())) {
				os::printf("Error: versioned protocol %s not "
				    "found\n", vpname);
				return (1);
			} else if (version_manager::wrong_mode::
			    _exnarrow(e.exception())) {
				os::printf("Error: versioned protocol %s is "
				    "node-pair mode, use other flag\n", vpname);
				return (1);
			} else if (version_manager::too_early::
			    _exnarrow(e.exception())) {
				os::printf("Error: Running version for %s not "
				    "yet set\n", vpname);
				return (1);
			} else {
				os::printf("Error: get_running version for %s "
				    "returned system exception\n", vpname);
				return (1);
			}
		}
		os::printf((scriptoutput ? "%s\t%d\t%d\n" :
		    "Running version for %s is %d.%d\n"), vpname,
		    vpv.major_num, vpv.minor_num);
		break;
	case Q_NPRV:
		if (!vpname || !nodepair_node) {
			usage();
			return (1);
		}
		vm_adm->get_nodepair_running_version(vpname, nodepair_node,
		    INCN_UNKNOWN, vpv, e);
		if (e.exception()) {
			if (version_manager::not_found::
			    _exnarrow(e.exception())) {
				os::printf("Error: versioned protocol %s not "
				    "found\n", vpname);
				return (1);
			} else if (version_manager::wrong_mode::
			    _exnarrow(e.exception())) {
				os::printf("Error: versioned protocol %s is "
				    "not node-pair mode, use other flag\n",
				    vpname);
				return (1);
			} else if (version_manager::too_early::
			    _exnarrow(e.exception())) {
				os::printf("Error: Running version for %s not "
				    "yet set\n", vpname);
				return (1);
			} else {
				os::printf("Error: get_running version for %s "
				    "returned system exception\n", vpname);
				return (1);
			}
		}
		os::printf((scriptoutput ? "%s\t%d\t%d\t%d\n" :
		    "Running version for %s on node %d is %d.%d\n"),
		    vpname, nodepair_node, vpv.major_num, vpv.minor_num);
		break;
	case Q_UNREGUCC:
		//
		// Note it is the responsibility of the user to invoke
		// % query_vm -U <ucc>
		// on every node to ensure proper unregistration.
		//
		vm_adm->debug_request(ucc, e);
		delete [] ucc;
		break;
	case Q_REGUCC:
		char *p = NULL;
		uint_t	bnum = 0;
		uint_t	cnum = 0;
		uint_t	fnum = 0;
		// Ensure that the mandatory fields have been specified.
		if (vpname == NULL) {
			os::printf("Specify a versioned protocol\n");
			return (1);
		}
		if (ucc == NULL) {
			os::printf("Specify a UCC name\n");
			return (1);
		}
		if (maj <= 0) {
			os::printf("Specify a valid major number\n");
			return (1);
		}
		if (minor_num < 0) {
			os::printf("Specify a valid minor number\n");
			return (1);
		}
		// Get a handle to the local test upgrade callback.
		version_manager::upgrade_callback_var cb_v =
			(new test_upgrade_callback)->get_objref();

		// Construct the new ucc to be registered.
		version_manager::ucc_seq_t ucc_seq(1, 1);
		ucc_seq[0].vp_name = os::strdup(vpname);
		ucc_seq[0].ucc_name = os::strdup(ucc);
		// Process the optional fields Before, Concurrent, and Freeze.
		if (bstring != NULL) {
			char **tmp_array = new char *[MAXELEM];
			tmp_array[bnum++] = os::strdup(strtok(bstring, " "));
			while ((p = strtok(NULL, " ")) != NULL &&
			    bnum < MAXELEM) {
				tmp_array[bnum++] = os::strdup(p);
			}
			version_manager::string_seq_t bseq(MAXELEM, bnum,
			    tmp_array, true);
			(ucc_seq)[0].upgrade_before = bseq;
		}
		if (cstring != NULL) {
			char **tmp_array = new char *[MAXELEM];
			tmp_array[cnum++] = os::strdup(strtok(cstring, " "));
			while ((p = strtok(NULL, " ")) != NULL &&
			    cnum < MAXELEM) {
				tmp_array[cnum++] = os::strdup(p);
			}
			version_manager::string_seq_t cseq(MAXELEM, cnum,
			    tmp_array, true);
			(ucc_seq)[0].upgrade_concurrently = cseq;
		}
		if (fstring != NULL) {
			char **tmp_array = new char *[MAXELEM];
			tmp_array[fnum++] = os::strdup(strtok(fstring, " "));
			while ((p = strtok(NULL, " ")) != NULL &&
			    fnum < MAXELEM) {
				tmp_array[fnum++] = os::strdup(p);
			}
			version_manager::string_seq_t fseq(MAXELEM, fnum,
			    tmp_array, true);
			(ucc_seq)[0].freeze = fseq;
		}
		// Get the highest version we support.
		version_manager::vp_version_t highest_version;
		version_manager::vp_version_t running_version;
		highest_version.major_num = (unsigned short)maj;
		highest_version.minor_num = (unsigned short)minor_num;
		// Register the ucc through the version manager interface.
		vm_adm->register_upgrade_callbacks(ucc_seq, cb_v,
		    highest_version, running_version, e);
		if (e.exception()) {
			if (version_manager::ucc_redefined::
			    _exnarrow(e.exception())) {
				os::printf("Error: ucc %s has been previously "
				    "registered.\n", ucc);
			} else if (version_manager::invalid_ucc::
			    _exnarrow(e.exception())) {
				os::printf("Error: ucc %s has invalid "
				    "content.\n", ucc);
			} else if (version_manager::unknown_vp::
			    _exnarrow(e.exception())) {
				os::printf("Error: ucc %s is associated with "
				    "an unknown vp %s.\n", ucc, vpname);
			} else if (version_manager::invalid_ucc_dependencies::
			    _exnarrow(e.exception())) {
				os::printf("Error: ucc %s has invalid "
				    "dependencies to other uccs.\n", ucc);
			}
			e.clear();
		}
		break;
	}
	//
	// Synchronization.
	//
	if (waitunref) {
		// smart pointer cb_v is now out of scope.
		wait_lock.lock();
		os::printf("waiting for unref...\n");
		while (!go_ahead) {
			wait_cond.wait(&wait_lock);
		}
		wait_lock.unlock();
	}
	return (0);
}

// Return a pointer to the last element in a file pathname.
static char *
last_path_elem(char *path)
{
	char    *q;
	for (q = path + os::strlen(path); q >= path; q--)
		if (*q == '/')
			return (q + 1);
	return (path);
}
//
// The test_upgrade_callback class used strictly by query_vm for debugging and
// testing purposes.
//
test_upgrade_callback::test_upgrade_callback()
{
}

test_upgrade_callback::~test_upgrade_callback()
{
}

void
#ifdef DEBUG
test_upgrade_callback::_unreferenced(unref_t cookie)
#else
test_upgrade_callback::_unreferenced(unref_t)
#endif
{
	os::printf("in test_upgrade_callback _unreferenced.\n");
	// This object does not support multiple 0->1 transitions.
	ASSERT(_last_unref(cookie));

	//
	// Synchronization.
	//
	wait_lock.lock();
	go_ahead = true;
	wait_cond.signal();
	wait_lock.unlock();

	// bye
	delete this;
}

void
test_upgrade_callback::do_callback(const char *ucc_name,
    const version_manager::vp_version_t &new_version,
    Environment &)
{
	os::printf("in test_upgrade_callback do_callback ucc is %s,"
	    "new version major num is %d, minor num is %d.\n", ucc_name,
	    new_version.major_num, new_version.minor_num);
}
