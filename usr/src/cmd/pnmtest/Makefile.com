#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.5	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/pnmtest/Makefile.com
#

PROG	= pnmtest

include $(SRC)/cmd/Makefile.cmd

SRCS		= $(OBJS:%.o=../common/%.c)
LINTFILES	= $(SRCS:%.c=%.ln)

OBJS		= pnmtest.o

CLOBBERFILES += $(OBJS) $(LINTFILES)

#
# Compiler flags
#
CPPFLAGS += $(MTFLAG) 
CPPFLAGS += -I$(SRC)/head -DSYSV -DSTRNET -DBSD_COMP

LDLIBS += -lposix4 -lnsl -lkstat -lsocket -ldl -lelf -lpnm -lclconf
AS_CPPFLAGS	+= -D_ASM
ASFLAGS		+= -P

#
# Targets
#

all: $(PROG)

clean :
	$(RM) *.o $(CLOBBERFILES)

$(PROG): $(OBJS)
	$(LINK.cc) -o $@ $(OBJS) $(LDLIBS)
	$(POST_PROCESS)

%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

%.ll:	../common/%.c
	$(WLCC) $(CFLAGS) $(CPPFLAGS) -o $@ $<

include $(SRC)/cmd/Makefile.targ
