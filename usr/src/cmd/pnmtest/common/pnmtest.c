/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmtest.c	1.5	08/05/20 SMI"

#include <sys/os_compat.h>

#include <pnm/pnm_head.h>
#include <rgm/pnm.h>
#include <sys/clconf_int.h>
#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#include <zone.h>
#endif

int silent = 0;
int batch = 0;

static char *cmdname;

FILE *fdi;

static char hostname_default[] = "localhost";

int fct_index = 0;

static char *function_name[] =
{
	"UNKNOWN",
	"pnm_init()",
	"pnm_fini()",
	"pnm_group_list()",
	"pnm_group_status_direct()",
	"pnm_group_status()",
	"pnm_callback_reg()",
	"pnm_callback_unreg()",
	"pnm_callback_list()",
	"pnm_ifconfig_direct()",
	"pnm_adapterinfo_list()",
	"pnm_map_adapter()",
	"pnm_mac_address()",
	"pnm_grp_adp_status()",
	"pnm_ip_check()",
	"pnm_group_create()",
	"pnm_get_instances_direct()",
	"pnm_get_instances()",
#if SOL_VERSION >= __s10
	"pnm_zone_init()",
	"pnm_get_zone_iptype()",
#endif
};

static char *error_msg[] =
{
	"PNM_OK",
	"PNM_EPROG",
	"PNM_ENOBKG",
	"PNM_EBKGEXIST",
	"PNM_ENOADP",
	"PNM_EUSAGE",
	"PNM_ENOOP"
	"PNM_ECALLBACK"
	"PNM_ENONHOMOGENOUS"
	"PNM_ENOBUF"
	"PNM_EHOSTNAMEIP"
};

static char *group_status[] =
{
	"PNM_STAT_OK",
	"PNM_STAT_DOWN",
	"PNM_STAT_STANDBY",
};

static char *adapter_status[] =
{
	"ADP_OK",
	"ADP_FAULTY",
	"ADP_STANDBY",
	"ADP_OFFLINE",
	"ADP_NOTRUN",
};

void
usage() {
	fprintf(stderr, "usage: %s [-h] [-s] [-m] [-i <scenario>]\n",
		cmdname);
	exit(1);
}

void
error(char *err) {
	fprintf(stderr, "(%s:%u) ERROR: %s\n",
		cmdname, pthread_self(), err);
}

void
print_fct(int fct_index) {
	fprintf(stdout, "(%s:%u) FUNCTION: %s\n",
		cmdname, pthread_self(), function_name[fct_index]);
}

void
print_hostname(char *s) {
	fprintf(stdout, "(%s:%u) HOSTNAME: %s\n",
		cmdname, pthread_self(), s);
}

#if SOL_VERSION >= __s10
void
print_zonename(char *s) {
	fprintf(stdout, "(%s:%u) ZONENAME: %s\n",
		cmdname, pthread_self(), s);
}
#endif

void
print_group(pnm_group_t group) {
	fprintf(stdout, "(%s:%u) GROUP: %s\n",
		cmdname, pthread_self(), group);
}

void
print_adapter(pnm_adapter_t adapter) {
	fprintf(stdout, "(%s:%u) GROUP: %s\n",
		cmdname, pthread_self(), adapter);
}

void
retcode(int err) {
	fprintf(stdout, "(%s:%u) RETURN CODE: %s\n",
		cmdname, pthread_self(), error_msg[err]);
}

void
output_group(pnm_group_t group) {
	fprintf(stdout, "(%s:%u) GROUP : %s\n",
		cmdname, pthread_self(), group);
}

void
output_group_list(pnm_group_list_t glist) {
	fprintf(stdout, "(%s:%u) GROUP LIST: %s\n",
		cmdname, pthread_self(), glist);
}

void
output_group_status(pnm_status_t *status) {
	fprintf(stdout, "(%s:%u) GROUP STATUS: %s (%s)\n",
		cmdname, pthread_self(), group_status[status->status],
		status->backups);
}

void
output_callback(pnm_callback_t *callback) {
	fprintf(stdout, "(%s:%u) CALLBACK : tag(%s) cmd(%s)\n",
		cmdname, pthread_self(), callback->id,
		callback->cmd);
}

void
output_callback_list(int count, pnm_callback_t *callback_list) {
	int i;

	for (i = 0; i < count; i++) {
		output_callback(&callback_list[i]);
	}
}

void
output_adapterinfo(pnm_adapterinfo_t *adapter) {
	int i;
	struct in_addr ipmask;
	struct in_addr ipnet;
	struct in_addr ipaddr;

	fprintf(stdout, "(%s:%u) ADAPTER : name = %s\n",
		cmdname, pthread_self(), adapter->adp_name);
	fprintf(stdout, "(%s:%u)         : flags = %llx\n",
		cmdname, pthread_self(), adapter->adp_flags);

	for (i = 0; i < adapter->adp_addrs.num_addrs; i++) {
		IN6_V4MAPPED_TO_INADDR(&(adapter->adp_addrs.adp_ipmask[i]),
			&ipmask);
		IN6_V4MAPPED_TO_INADDR(&(adapter->adp_addrs.adp_ipnet[i]),
			&ipnet);
		IN6_V4MAPPED_TO_INADDR(&(adapter->adp_addrs.adp_ipaddr[i]),
			&ipaddr);
		fprintf(stdout, "(%s:%u)         : ipmask[%d] = %s\n",
			cmdname, pthread_self(), i, inet_ntoa(ipmask));
		fprintf(stdout, "(%s:%u)         : ipnet[%d] = %s\n",
			cmdname, pthread_self(), i, inet_ntoa(ipnet));
		fprintf(stdout, "(%s:%u)         : ipaddr[%d] = %s\n",
			cmdname, pthread_self(), i, inet_ntoa(ipaddr));
	}

	if (adapter->adp_group)
		fprintf(stdout, "(%s:%u)         : group = %s\n",
			cmdname, pthread_self(), adapter->adp_group);
}

void
output_adapterinfo_list(pnm_adapterinfo_t *alist, boolean_t alladdrs) {
	pnm_adapterinfo_t *adapter;

	fprintf(stdout, "\nAdapter Info List with alladdrs = %s\n",
	    alladdrs ? "TRUE" : "FALSE");

	for (adapter = alist; adapter != NULL; adapter = adapter->next) {
		output_adapterinfo(adapter);
	}
}

void
output_mac_address(uint_t mac_addrs_uniq) {
	fprintf(stdout, "(%s:%u) MAC_ADDRS_UNIQ = %d\n",
		cmdname, pthread_self(), mac_addrs_uniq);
}

void
output_adp_status(pnm_adp_status_t *adp_status) {
	fprintf(stdout, "(%s:%u) ADAPTER : name = %s, status = %s\n",
		cmdname, pthread_self(), adp_status->adp_name,
		adapter_status[adp_status->status]);
}

void
output_grp_adp_status(pnm_grp_status_t *grp_status) {
	int i;

	for (i = 0; i < grp_status->num_adps; i++) {
		output_adp_status(&grp_status->pnm_adp_stat[i]);
	}
}

void
output_ip_check_grp(pnm_grp_t *grp) {
	fprintf(stdout, "(%s:%u) GRP = %s\n",
		cmdname, pthread_self(), grp->grp_name);
}

void
output_ip_check_grplist(pnm_ip_check_grplist_t *grp_list) {
	int i;

	for (i = 0; i < grp_list->num_grps; i++) {
		output_ip_check_grp(&grp_list->listp[i]);
	}
}

void
output_get_instances(int result) {
	fprintf(stdout, "(%s:%u) ", cmdname, pthread_self());
	if (result & PNMV4MASK)
		fprintf(stdout, "PNMV4MASK ");
	if (result & PNMV6MASK)
		fprintf(stdout, "PNMV6MASK ");
	fprintf(stdout, "\n");
}

#if SOL_VERSION >= __s10
void
output_zone_iptype(pnm_zone_iptype_t iptype)
{
	fprintf(stdout, "(%s:%u) ", cmdname, pthread_self());
	if (iptype == SHARED_IP) {
		fprintf(stdout, "ZS_SHARED ");
	} else if (iptype == EXCLUSIVE_IP) {
		fprintf(stdout, "ZS_EXCLUSIVE ");
	}
	fprintf(stdout, "\n");
}
#endif

void
print_callback(char *grp) {
	fprintf(stdout, "(%s:%u) CALLBACK for group %s\n",
		cmdname, pthread_self(), grp);
}

void
press_enter() {
	char enter;
	fflush(fdi);
	fscanf(fdi, "%c", &enter);
	fscanf(fdi, "%c", &enter);
}

void
new_line() {
	char cr;
	if (batch) {
		fflush(fdi);
		fscanf(fdi, "%c", &cr);
	}
}

void
test_pnm_init()
{
	char hostname[LINELEN];
	char *hostptr;
	int error;

	clconf_lib_init();
	print_fct(fct_index);

	bzero(hostname, LINELEN);

	fflush(fdi);

	if (!silent)
		fprintf(stdout, "Hostname (localhost) ? ");
	new_line();
	fscanf(fdi, "%s", hostname);
	hostptr = (strlen(hostname) == 0) ? NULL : hostname;

	if (batch)
		print_hostname(hostname);

	error = pnm_init(hostname);
	retcode(error);
}

void
test_pnm_fini()
{
	print_fct(fct_index);

	pnm_fini();
}

void
test_pnm_group_list()
{
	pnm_group_list_t glist = NULL;
	int error;

	print_fct(fct_index);

	error = pnm_group_list(&glist);
	retcode(error);
	if (error == 0) {
		output_group_list(glist);
	}
	if (glist)
		free(glist);
}

void
test_pnm_group_status_direct()
{
	char s[80];
	pnm_group_t group;
	pnm_status_t status;
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Group name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	group = (pnm_group_t)s;

	if (batch)
		print_group(group);
	error = pnm_group_status_direct(group, &status);
	retcode(error);
	if (error == 0) {
		output_group_status(&status);
		pnm_status_free(&status);
	}
}

void
test_pnm_group_status()
{
	char s[80];
	pnm_group_t group;
	pnm_status_t status;
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Group name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	group = (pnm_group_t)s;

	if (batch)
		print_group(group);
	error = pnm_group_status(group, &status);
	retcode(error);
	if (error == 0) {
		output_group_status(&status);
		pnm_status_free(&status);
	}
}

void
test_pnm_callback_reg()
{
	char s[80];
	pnm_group_t group;
	char prefix_cmd[] = "/usr/cluster/bin/pnmtest -c";
	char tag[] = "tag.callback";
	char cmd[256];
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Group name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	group = (pnm_group_t)s;

	sprintf(cmd, "%s %s", prefix_cmd, group);

	if (batch) {
		print_group(group);
	}
	error = pnm_callback_reg(group, tag, cmd);
	retcode(error);
}

void
test_pnm_callback_unreg()
{
	char s[80];
	pnm_group_t group;
	char tag[] = "tag.callback";
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Group name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	group = (pnm_group_t)s;

	if (batch) {
		print_group(group);
	}
	error = pnm_callback_unreg(group, tag);
	retcode(error);
}

void
test_pnm_callback_list()
{
	char s[80];
	pnm_group_t group;
	pnm_callback_t *callback_list = NULL;
	uint_t callback_number;
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Group name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	group = (pnm_group_t)s;

	if (batch) {
		print_group(group);
	}
	error = pnm_callback_list(group, &callback_number, &callback_list);
	retcode(error);

	if (error == 0) {
		output_callback_list(callback_number, callback_list);
	}

	if (callback_list)
		free(callback_list);
}

void
test_pnm_ifconfig_direct()
{
	char s[80];
	char group[80];
	char zone[80];
	pnm_ifconfig_op_t op;
	struct in_addr ip;
	struct in6_addr ip6;
	int error;

	print_fct(fct_index);

	bzero(group, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Group name ? ");
	new_line();
	fscanf(fdi, "%s", group);

	bzero(group, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Zone name ? ");
	new_line();
	fscanf(fdi, "%s", zone);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Operation (0:plumb,1:unplumb,2:up,3:down) ? ");
	new_line();
	fscanf(fdi, "%s", s);
	sscanf(s, "%d", &op);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "IP address ? ");
	new_line();
	fscanf(fdi, "%s", s);
	inet_pton(AF_INET, s, &ip);

	IN6_INADDR_TO_V4MAPPED(&ip, &ip6);

	if (batch) {
		print_group(group);
	}
	error = pnm_ifconfig_direct((pnm_group_t)group, zone, op, 1, &ip6);
	retcode(error);
}

void
test_pnm_adapterinfo_list()
{
	char s[80];
	pnm_adapterinfo_t *alist;
	int error;

	print_fct(fct_index);

	error = pnm_adapterinfo_list(&alist, B_FALSE);
	retcode(error);

	if (error == 0) {
		output_adapterinfo_list(alist, B_FALSE);
		pnm_adapterinfo_free(alist);
	}

	error = pnm_adapterinfo_list(&alist, B_TRUE);
	retcode(error);

	if (error == 0) {
		output_adapterinfo_list(alist, B_TRUE);
		pnm_adapterinfo_free(alist);
	}
}

void
test_pnm_map_adapter()
{
	char s[80];
	pnm_group_t group = NULL;
	pnm_adapter_t adapter;
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Adapter name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	adapter = (pnm_adapter_t)s;

	if (batch) {
		print_adapter(adapter);
	}
	error = pnm_map_adapter(adapter, &group);
	retcode(error);

	if (error == 0) {
		output_group(group);
	}

	if (group)
		free(group);
}

void
test_pnm_mac_address()
{
	uint_t mac_addrs_uniq;
	int error;

	print_fct(fct_index);

	error = pnm_mac_address(&mac_addrs_uniq);
	retcode(error);

	if (error == 0) {
		output_mac_address(mac_addrs_uniq);
	}
}

void
test_pnm_grp_adp_status()
{
	char s[80];
	pnm_group_t group;
	pnm_grp_status_t grp_status;
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Group name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	group = (pnm_group_t)s;

	if (batch)
		print_group(group);
	error = pnm_grp_adp_status(group, &grp_status);
	retcode(error);
	if (error == 0) {
		output_grp_adp_status(&grp_status);
		pnm_grp_status_free(&grp_status);
	}

}

void
test_pnm_ip_check()
{
	char s[80];
	pnm_ip_check_grplist_t grp_list;
	int error;

	print_fct(fct_index);

	error = pnm_ip_check(&grp_list);
	retcode(error);
	if (error == 0) {
		output_ip_check_grplist(&grp_list);
		pnm_ip_check_grplist_free(&grp_list);
	}
}

void
test_pnm_group_create()
{
	char s[80];
	pnm_adapter_t adapter;
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Adapter name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	adapter = (pnm_adapter_t)s;

	if (batch) {
		print_adapter(adapter);
	}
	error = pnm_group_create(adapter);
	retcode(error);
}
void
test_pnm_get_instances_direct()
{
	char s[80];
	pnm_group_t group;
	int result;
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Group name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	group = (pnm_group_t)s;

	if (batch) {
		print_group(group);
	}
	error = pnm_get_instances_direct(group, &result);
	retcode(error);

	if (error == 0) {
		output_get_instances(result);
	}
}

void
test_pnm_get_instances()
{
	char s[80];
	pnm_group_t group;
	int result;
	int error;

	print_fct(fct_index);

	bzero(s, 80);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Group name ? ");
	new_line();
	fscanf(fdi, "%s", s);
	group = (pnm_group_t)s;

	if (batch) {
		print_group(group);
	}
	error = pnm_get_instances(group, &result);
	retcode(error);

	if (error == 0) {
		output_get_instances(result);
	}
}

#if SOL_VERSION >= __s10
//
// Initialize PNM for a specific zone on the given node.
//
void
test_pnm_zone_init()
{
	char hostname[LINELEN];
	char *hostptr;
	char zonename[ZONENAME_MAX];
	int error;
	int result;

	clconf_lib_init();
	print_fct(fct_index);

	bzero(hostname, LINELEN);
	fflush(fdi);
	if (!silent) {
		fprintf(stdout, "Hostname (localhost) ? ");
	}
	new_line();
	fscanf(fdi, "%s", hostname);
	hostptr = (strlen(hostname) == 0) ? NULL : hostname;

	if (batch) {
		print_hostname(hostname);
	}

	bzero(zonename, ZONENAME_MAX);
	fflush(fdi);
	if (!silent) {
		fprintf(stdout, "Zonename (global) ? ");
	}
	new_line();
	fscanf(fdi, "%s", zonename);

	if (batch) {
		print_zonename(zonename);
	}
	if ((strcmp(zonename, "global") == 0) || (zonename[0] == 0)) {
		error = pnm_init(hostname);
	} else {
		error = pnm_zone_init(hostname, zonename);
	}
	retcode(error);
}

//
// Test the functionality to return the IP-type of the given zone
//
void
test_pnm_get_zone_iptype()
{
	char zonename[ZONENAME_MAX];
	int error;
	pnm_zone_iptype_t iptype;

	print_fct(fct_index);
	bzero(zonename, ZONENAME_MAX);
	fflush(fdi);
	if (!silent)
		fprintf(stdout, "Zonename (global) ? ");
	new_line();
	fscanf(fdi, "%s", zonename);

	if (batch) {
		print_zonename(zonename);
	}
	if ((strcmp(zonename, "global") == 0) || (zonename[0] == 0)) {
		error = pnm_get_zone_iptype((pnm_zone_t)"global", &iptype);
	} else {
		error = pnm_get_zone_iptype(zonename, &iptype);
	}
	retcode(error);
	if (error == 0) {
		output_zone_iptype(iptype);
	}
}

#endif

int
main(int argc, char **argv) {
	char s[80];
	int choice;
	char c;

	cmdname = argv[0];
	fdi = stdin;

	while ((c = getopt(argc, argv, "i:c:sh")) != EOF) {
		switch (c) {
		case '?':
		case 'h':
			usage();
		case 'i':
			if (!optarg)
				usage();
			fdi = fopen(optarg, "r");
			if (fdi == NULL) {
				error("Cannot not open scenario file");
				exit(1);
			}
			batch = 1;
			break;
		case 's':
			silent = 1;
			break;
		case 'c':
			print_callback(optarg);
			return (0);
		}
	}

	while (1) {

		if (!silent) {
			system("clear");
			fprintf(stdout, "LIBPNM API functions\n\n");
			fprintf(stdout, "1. pnm_init()\n");
			fprintf(stdout, "2. pnm_fini()\n");
			fprintf(stdout, "3. pnm_group_list()\n");
			fprintf(stdout, "4. pnm_group_status_direct()\n");
			fprintf(stdout, "5. pnm_group_status()\n");
			fprintf(stdout, "6. pnm_callback_reg()\n");
			fprintf(stdout, "7. pnm_callback_unreg()\n");
			fprintf(stdout, "8. pnm_callback_list()\n");
			fprintf(stdout, "9. pnm_ifconfig_direct()\n");
			fprintf(stdout, "10. pnm_adapterinfo_list()\n");
			fprintf(stdout, "11. pnm_map_adapter()\n");
			fprintf(stdout, "12. pnm_mac_address()\n");
			fprintf(stdout, "13. pnm_grp_adp_status()\n");
			fprintf(stdout, "14. pnm_ip_check()\n");
			fprintf(stdout, "15. pnm_group_create()\n");
			fprintf(stdout, "16. pnm_get_instances_direct()\n");
			fprintf(stdout, "17. pnm_get_instances()\n");
#if SOL_VERSION >= __s10
			fprintf(stdout, "18. pnm_zone_init()\n");
			fprintf(stdout, "19. pnm_get_zone_type()\n");
#endif
			fprintf(stdout, "\nChoice (q for quit)? ");
		}
		fscanf(fdi, "%s", s);
		if ((s[0] == 'q') || (s[0] == 'Q'))
			break;
		sscanf(s, "%d", &choice);

		fct_index = choice;

		switch (choice) {
		case 1:
			test_pnm_init();
			break;
		case 2:
			test_pnm_fini();
			break;
		case 3:
			test_pnm_group_list();
			break;
		case 4:
			test_pnm_group_status_direct();
			break;
		case 5:
			test_pnm_group_status();
			break;
		case 6:
			test_pnm_callback_reg();
			break;
		case 7:
			test_pnm_callback_unreg();
			break;
		case 8:
			test_pnm_callback_list();
			break;
		case 9:
			test_pnm_ifconfig_direct();
			break;
		case 10:
			test_pnm_adapterinfo_list();
			break;
		case 11:
			test_pnm_map_adapter();
			break;
		case 12:
			test_pnm_mac_address();
			break;
		case 13:
			test_pnm_grp_adp_status();
			break;
		case 14:
			test_pnm_ip_check();
			break;
		case 15:
			test_pnm_group_create();
			break;
		case 16:
			test_pnm_get_instances_direct();
			break;
		case 17:
			test_pnm_get_instances();
			break;
#if SOL_VERSION >= __s10
		case 18:
			test_pnm_zone_init();
			break;
		case 19:
			test_pnm_get_zone_iptype();
			break;
#endif
		default:
			error("Invalid Choice");
		}

		press_enter();
	}

	return (0);
}
