/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sc_publish_event.c	1.9	08/06/04 SMI"

/*
 * sc_publish_event(1M)
 */

#include <stdio.h>
#include <sys/types.h>
#include <libsysevent.h>
#include <sys/time.h>
#include <string.h>
#include <libintl.h>
#include <stdlib.h>

#include <sys/cl_eventdefs.h>
#include <sys/cladm_int.h>
#include <sys/clconf_int.h>

#if SOL_VERSION >= __s10
#include "sc_event_proxy/sc_event_proxy.h"
#include <sc_event_proxy_impl.h>
#include <netdb.h>
#include <sys/vc_int.h>
#endif

static int parse_vararg_list(int, char **, nvlist_t *);
static int add_attr_triplet(char *, char *, char *, nvlist_t *);
static void usage(void);
static char *prog;

/*
 *  main()
 */
int
main(int argc, char **argv) {

	char *ev_subclass = "";
	char *ev_publisher = "";
	uint32_t ev_severity = 0;
	uint32_t ev_initiator = 0;

	int c;
	int sflg = 0;
	int cflg = 0;
	int pflg = 0;
	int iflg = 0;
	int retval = 0;
#if SOL_VERSION >= __s10
	int vflg = 0;
#endif /* SOL_VERSION >= __s10 */
	struct timeval tod;

	clcluster_name_t clname_args;
	clnode_name_t nodename_args;
	get_ccr_entry_t ccr_entry_args;
	char *table_name = "infrastructure";
	char *entry_name = "cluster.properties.cluster_id";

	nodeid_t nid;

	char clid[256];
	char clname[256];
	size_t publen;

	nvlist_t *attr_list;
	sysevent_id_t eid;
#if SOL_VERSION >= __s10
	char zcname[ZONENAME_MAX] = "";
	char zone_name[ZONENAME_MAX] = "";
	char nodename[MAXHOSTNAMELEN + 1 + ZONENAME_MAX] = "";
	zcnode_name_t zcnode_name;
#else
	char nodename[256];
#endif

	prog = *argv;

	/* Parse mandatory command arguments */
	while ((c = getopt(argc, argv, "Z:c:p:s:i:x:y:z:")) != EOF) {

		switch (c) {
		case 's':
			if (optarg != NULL) {
				ev_severity = (uint32_t)strtoul(optarg,
				    (char **)NULL, 10);
				sflg++;
			}
			break;
		case 'c':
			if (optarg != NULL) {
				ev_subclass =  optarg;
				cflg++;
			}
			break;
		case 'p':
			if (optarg != NULL) {
				ev_publisher = optarg;
				pflg++;
			}
			break;
		case 'i':
			if (optarg != NULL) {
				ev_initiator = (uint32_t)strtoul(optarg,
				    (char **)NULL, 10);
				iflg++;
			}
			break;
		case 'x':
			break;
		case 'y':
			break;
		case 'z':
			break;
#if SOL_VERSION >= __s10
		case 'Z':
			if (optarg != NULL) {
				(void) strcpy(zcname, optarg);
				vflg++;
			}
			break;
#endif /* SOL_VERSION >= __s10 */
		default:
			usage();
			break;
		}
	}

	/* Validate mandatory arguments */
	if (! (sflg & cflg & pflg & iflg)) {
		return (CL_EVENT_EINVAL);
	}

	if (strlen(ev_subclass) > MAX_SUBCLASS_LEN) {
		return (CL_EVENT_EINVAL);
	}

	publen = strlen(ev_publisher) + strlen(SUNW_VENDOR) +
		strlen(SE_USR_PUB) + 1;

	if (publen > MAX_PUB_LEN) {
		return (CL_EVENT_EINVAL);
	}

#if SOL_VERSION >= __s10
	if (vflg) {
		/*
		 * Get cluster id, for a zone cluster, cluster id is same
		 * zone cluster id.
		 */
		cz_id_t czid;
		czid.clid = 0;
		(void) strcpy(czid.name, zcname);

		if (_cladm(CL_CONFIG, CL_GET_ZC_ID, &czid)) {
			return (CL_EVENT_ECLADM);
		}
		/*
		 * Validate that the zone cluster name that is specified and is
		 * configured in the system.
		 */
		if (czid.clid < MIN_CLUSTER_ID) {
			return (CL_EVENT_EINVAL);
		}
		(void) sprintf(clid, "%d", czid.clid);
		/*
		 * Get the cluster name. For a zone cluster, cluster name is
		 * same as the zone name.
		 */
		(void) strcpy(clname, zcname);

		/* Get nodename */
		if (_cladm(CL_CONFIG, CL_NODEID, &nid)) {
			return (CL_EVENT_ECLADM);
		}

		(void) strcpy(zcnode_name.zc_name, zcname);
		zcnode_name.nodeid = nid;
		zcnode_name.namep = nodename;
		zcnode_name.len = sizeof (nodename);

		if (_cladm(CL_CONFIG, CL_GET_ZC_NODE_NAME, &zcnode_name)) {
			return (CL_EVENT_ECLADM);
		}
	} else {

		/* Get cluster id */
		ccr_entry_args.table_name = table_name;
		ccr_entry_args.table_name_len = strlen(table_name) + 1;
		ccr_entry_args.entry_name = entry_name;
		ccr_entry_args.entry_name_len = strlen(entry_name) + 1;
		ccr_entry_args.buf = clid;
		ccr_entry_args.buf_len = sizeof (clid);

		if (_cladm(CL_CONFIG, CL_GET_CCR_ENTRY, &ccr_entry_args)) {
			return (CL_EVENT_ECLADM);
		}

		/* Get cluster name */
		clname_args.name = clname;
		clname_args.len = sizeof (clname);

		if (_cladm(CL_CONFIG, CL_GET_CLUSTER_NAME, &clname_args)) {
			return (CL_EVENT_ECLADM);
		}

		/* Get nodename */
		if (_cladm(CL_CONFIG, CL_NODEID, &nid)) {
			return (CL_EVENT_ECLADM);
		}

		nodename_args.nodeid = nid;
		nodename_args.name = nodename;
		nodename_args.len = sizeof (nodename);

		if (_cladm(CL_CONFIG, CL_GET_NODE_NAME, &nodename_args)) {
			return (CL_EVENT_ECLADM);
		}

		/*
		 * Retrieve the zonename
		 */
		if (getzonenamebyid(getzoneid(), zone_name,
		    sizeof (zone_name)) < 0) {
			return (CL_EVENT_EPOST);
		}

		if (strcmp(zone_name, GLOBAL_ZONENAME) != 0) {
			(void) strlcat(nodename, ":", sizeof (nodename));
			(void) strlcat(nodename, zone_name, sizeof (nodename));
		}
	}
#else
	/* Get cluster id */
	ccr_entry_args.table_name = table_name;
	ccr_entry_args.table_name_len = strlen(table_name) + 1;
	ccr_entry_args.entry_name = entry_name;
	ccr_entry_args.entry_name_len = strlen(entry_name) + 1;
	ccr_entry_args.buf = clid;
	ccr_entry_args.buf_len = sizeof (clid);

	if (_cladm(CL_CONFIG, CL_GET_CCR_ENTRY, &ccr_entry_args)) {
		return (CL_EVENT_ECLADM);
	}

	/* Get cluster name */
	clname_args.name = clname;
	clname_args.len = sizeof (clname);

	if (_cladm(CL_CONFIG, CL_GET_CLUSTER_NAME, &clname_args)) {
		return (CL_EVENT_ECLADM);
	}

	/* Get nodename */
	if (_cladm(CL_CONFIG, CL_NODEID, &nid)) {
		return (CL_EVENT_ECLADM);
	}

	nodename_args.nodeid = nid;
	nodename_args.name = nodename;
	nodename_args.len = sizeof (nodename);

	if (_cladm(CL_CONFIG, CL_GET_NODE_NAME, &nodename_args)) {
		return (CL_EVENT_ECLADM);
	}
#endif /* SOL_VERSION >= __s10 */

	/* Allocate event attribute list */
	if (nvlist_alloc(&attr_list, NV_UNIQUE_NAME_TYPE, 0) != 0) {
		return (CL_EVENT_ENOMEM);
	}

	/*
	 * Add required event attributes:
	 *  - Cluster ID
	 *  - Cluster Name
	 *  - Node Name
	 *  - Time Stamp
	 *  - Event Version
	 *  - Severity
	 *  - Initiator
	 */
	if (nvlist_add_string(attr_list, CL_CLUSTER_ID, clid) != 0) {
		retval = CL_EVENT_EATTR;
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_string(attr_list, CL_CLUSTER_NAME, clname) != 0) {
		retval = CL_EVENT_EATTR;
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_string(attr_list, CL_EVENT_NODE, nodename) != 0) {
		retval = CL_EVENT_EATTR;
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_uint32(attr_list, CL_EVENT_VERSION,
			CL_EVENT_VERSION_NUMBER) != 0) {
		retval = CL_EVENT_EATTR;
		goto event_exit; /*lint !e801 */
	}

	(void) gettimeofday(&tod, (void *) NULL);

	if (nvlist_add_uint32(attr_list, CL_EVENT_TS_SEC,
			(uint32_t)tod.tv_sec) != 0) {
		retval = CL_EVENT_EATTR;
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_uint32(attr_list, CL_EVENT_TS_USEC,
			(uint32_t)tod.tv_usec) != 0) {
		retval = CL_EVENT_EATTR;
		goto event_exit; /*lint !e801 */
	}

	/* Add initiator id and severity values */
	if (nvlist_add_uint32(attr_list, CL_EVENT_SEVERITY,
			ev_severity) != 0) {
		retval = CL_EVENT_EATTR;
		goto event_exit; /*lint !e801 */
	}

	if (nvlist_add_uint32(attr_list, CL_EVENT_INITIATOR,
			ev_initiator) != 0) {
		retval = CL_EVENT_EATTR;
		goto event_exit; /*lint !e801 */
	}

	/* Parse variable arguments list */
	if (parse_vararg_list(argc, argv, attr_list)) {
		goto event_exit; /*lint !e801 */
	}

	/*
	 * Post event...
	 * If we are in the global zone log the event here. If
	 * we are in a non-global zone call the event proxy.
	 */
#if SOL_VERSION >= __s10
	if (strcmp(zone_name, GLOBAL_ZONENAME) == 0) {
		if (sysevent_post_event(EC_CLUSTER, ev_subclass,
		    SUNW_VENDOR, ev_publisher, attr_list, &eid) != 0) {
			retval = CL_EVENT_EPOST;
		}
	} else {
		retval = sc_publish_event_proxy(ev_subclass, ev_publisher,
		    attr_list);
	}
#else
	if (sysevent_post_event(EC_CLUSTER, ev_subclass,
	    SUNW_VENDOR, ev_publisher, attr_list, &eid) != 0) {
		retval = CL_EVENT_EPOST;
	}
#endif
	event_exit:
		if (attr_list) {
			nvlist_free(attr_list);
		}

		return (retval);

}  /* ******    main   ****** */

/*
 * parse_vararg_list
 *
 *      Parses the the variable arguments passed to main().
 */
static
int parse_vararg_list(int argc, char **argv, nvlist_t *attr_list) {

	int xflg = 0;
	int yflg = 0;
	int zflg = 0;
	int c;
	int retval = 0;
	char *value_name = NULL;
	char *value_type = NULL;
	char *value = NULL;

	/* Make sure optind is set */
	optind = 1;

	while ((c = getopt(argc, argv, "Z:x:y:z:c:p:s:i:")) != EOF) {

		switch (c) {
		case 'x':
			if (optarg != NULL) {
				value_name = optarg;
				++xflg;
			}
			break;
		case 'y':
			if (optarg != NULL) {
				value_type = optarg;
				++yflg;
			}
			break;
		case 'z':
			if (optarg != NULL) {
				value = optarg;
				++zflg;
			}
			break;
		case 'Z':
			/*
			 * Ignore as we would have already handled this
			 * option.
			 */
			break;
		default:
			break;
		}

		if ((xflg > 0) && (yflg > 0) && (zflg > 0)) {
			if ((xflg == yflg) && (yflg == zflg)) {

				if ((retval = add_attr_triplet(value_name,
				    value_type,
				    value,
				    attr_list)) != 0) {
					return (retval);
				}
			}
		}
	}

	return (retval);
}

/*
 * add_attr_triplet
 *
 *            Adds the attributes triplet to the internal
 *            attributes list
 */
static
int add_attr_triplet(char *value_name,
    char *value_type,
    char *val,
    nvlist_t *attr_list) {

	int err = 0;
	int retval = 0;

	/*
	 * Each attribute specification is
	 * in the form of a triplet - consisting of a value name, value
	 * type and the actual value.
	 */
	if (strcmp("SE_DATA_TYPE_BYTE", value_type) == 0) {

			uchar_t value;

			/* value =  (uchar_t) atoi(val); */
			value = (uchar_t)(*val);

			if (nvlist_add_byte(attr_list, value_name, value)) {
				retval = CL_EVENT_EATTR;
			}
	} else if (strcmp("SE_DATA_TYPE_INT16", value_type) == 0) {

			int16_t value;
			value = (int16_t)atoi(val);

			if (nvlist_add_int16(attr_list, value_name, value)) {
				retval = CL_EVENT_EATTR;
			}
	} else if (strcmp("SE_DATA_TYPE_UINT16", value_type) == 0) {

			uint16_t  value;
			value = (uint16_t)atoi(val);

			if (nvlist_add_uint16(attr_list, value_name, value)) {
				retval = CL_EVENT_EATTR;
			}
	} else if (strcmp("SE_DATA_TYPE_INT32", value_type) == 0) {

			int32_t value;

			value = (int32_t)atol(val);

			if (nvlist_add_int32(attr_list, value_name, value)) {
				retval = CL_EVENT_EATTR;
			}
	} else if (strcmp("SE_DATA_TYPE_UINT32", value_type) == 0) {

			uint32_t  value;
			value = (uint32_t)atol(val);

			if (nvlist_add_uint32(attr_list, value_name, value)) {
				retval = CL_EVENT_EATTR;
			}
	} else if (strcmp("SE_DATA_TYPE_INT64", value_type) == 0) {

			int64_t value;
			value = (int64_t)atoll(val);

			if (nvlist_add_int64(attr_list, value_name, value)) {
				retval = CL_EVENT_EATTR;
			}
	} else if (strcmp("SE_DATA_TYPE_UINT64", value_type) == 0) {

			uint64_t  value;
			value = (uint64_t)atoll(val);

			if (nvlist_add_uint64(attr_list, value_name, value)) {
				retval = CL_EVENT_EATTR;
			}
	} else if (strcmp("SE_DATA_TYPE_STRING", value_type) == 0) {

			if (nvlist_add_string(attr_list, value_name, val)) {
				retval = CL_EVENT_EATTR;
			}
	} else if (strcmp("SE_DATA_TYPE_BYTES", value_type) == 0) {

			uchar_t *value, *tmp;
			value =
			    (uchar_t *) malloc(sizeof (uchar_t) * strlen(val));
			tmp = value;

			/* Convert char* to uchar_t* */
			while (*val != NULL) {
				*tmp = (uchar_t)*val;
				++tmp; ++val;
			}

			err = nvlist_add_byte_array(attr_list,
				value_name, value, (uint_t)sizeof (value));

			if (err) {
				retval = CL_EVENT_EATTR;
			}

			free(value);

	} else if (strcmp("SE_DATA_TYPE_TIME", value_type) == 0) {

			int64_t value;
			value = (int64_t)atoll(val);

			if (nvlist_add_int64(attr_list, value_name, value)) {
				retval = CL_EVENT_EATTR;
			}
	} else if (strcmp("DATA_TYPE_UNKNOWN", value_type) == 0) {
	} else if (strcmp("DATA_TYPE_BOOLEAN", value_type) == 0) {
	} else if (strcmp("DATA_TYPE_INT16_ARRAY", value_type) == 0) {
	} else if (strcmp("DATA_TYPE_UINT16_ARRAY", value_type) == 0) {
	} else if (strcmp("DATA_TYPE_INT32_ARRAY", value_type) == 0) {
	} else if (strcmp("DATA_TYPE_UINT32_ARRAY", value_type) == 0) {
	} else if (strcmp("DATA_TYPE_INT64_ARRAY", value_type) == 0) {
	} else if (strcmp("DATA_TYPE_UINT64_ARRAY", value_type) == 0) {
	} else if (strcmp("DATA_TYPE_STRING_ARRAY", value_type) == 0) {
	} else {
			retval = CL_EVENT_EINVAL;
	}

	return (retval);
}

static
void usage() {
	(void) fprintf(stderr,
		(const char *) gettext("usage: %s -c subclass -p publisher"
		    " -s severity -i initiator [[ -x attr-name -y attr-type"
		    "-z attr-value], ...]\n"), prog);

	exit(1);
}
