/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scswitch.c	1.81	08/07/14 SMI"

/* Tell lint that real ANSI C and not C++. Lint reports C++ errors */
/* lint -save -fcp -e1746 */

/*
 * Suppress lint message: "Info(793) [c:23]: ANSI limit of 511
 * 'external identifiers' exceeded -- processing is unaffected"
 */
/* lint -e793 */

/*
 * scswitch.c
 *
 *	Parses command line options for the scswitch(1M) command.
 */

/*
 * XXX - General comment for the functions using rgm:
 *	As the rgm function now returns a structure for error status
 *	and scswitch needs to handle different error types from different
 *	modules like dcs, scconf, scswitch, rgm,
 *	the functions using rgm are made to return scha_errmsg_t type
 *	This needs to be handled in this way till a proper error handling
 *	mechanism is provided.
 */

/*
 * New features for FID# 1015
 * ==================================================
 *
 * This version of scswitch introduces new logic for execution of
 * -S (node evacuate) and -Z (enable/manage/start RGs), where most of
 * the work is now done in the rgmd.  Also, we introduce new combinations
 * of command line options:
 *
 * 	-z -g (with no -h) is now permitted.  This brings the specified
 *	resource groups online on their most-preferred masters, taking
 *	RG affinities into account.  The work is done mostly in the rgmd.
 *	All of the specified RGs must already be managed, or an error
 *	is returned.  This command does not manage RGs nor enable resources.
 *
 *	-z (with no -h or -g) is now permitted.  This brings all managed
 *	RGs online on their most preferred masteres, taking RG affinities
 *	into account.  No resources are enabled.
 *
 * A new command line argument may be used only in combination with the
 * node evacuate (-S) flag.  During node evacuation, RGs are prevented
 * from failing over onto the evacuating node.  The -K option extends this
 * restriction for a period of time after the evacuation has completed.
 *
 *	-K <timeout>	"Keep" RGs from failing over onto evacuated node.
 *			(also known as "sticky node evacuation").
 *			'timeout' is a short unsigned int, i.e. it must have
 *			a value between 0 and 65535.
 *			The failovers continue to be prevented for <timeout>
 *			seconds after the evacuation of the node has completed.
 *
 *	The -K option is used by the rgm 'K' init script when a node is
 *	being evacuated prior to shutdown.
 *
 * All of these new features are implemented as part of the putback for
 * Feature ID 1015 (Enhanced Inter-RG Dependencies).
 *
 */

/*
 * FID# 1376
 * =========
 *
 * This feature introduces "-Q [-g] [-k]", "-s [-g] [-k]", and "-r [-g]"
 * command options for suspending/resuming automatic recovery actions on
 * resource groups, and for optionally killing running methods to hasten
 * the quiescing of a resource group.
 */

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include<zone.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <locale.h>
#include <limits.h>

#include <libintl.h>	/* gettext */
#include <dc/libdcs/libdcs.h>

#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_errmsg.h>
#include <rgm/sczones.h>
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>
#include <sys/cladm_int.h>

#define	UPDATEFAILED	"update_failed"
#define	INITFAILED	"init_failed"
#define	FINIFAILED	"fini_failed"
#define	STOPFAILED	"stop_failed"

static char	*progname;	/* global; set to name of this program */
static int exit_code;


static void usage(void);
static void parse_cmd_args(int argc, char **argv, scswitch_errno_t *,
    scha_errmsg_t *);
static scswitch_errno_t print_scswitch_error(
    const scswitch_errno_t scswitch_error, const scha_errmsg_t scha_error);
static scha_errmsg_t all_rgs_online(char **rgnames, char **nodenames,
	boolean_t verbose_flag);
scswitch_errno_t check_device_service_list(char **service_list);

static void generate_event(int, char **);
static char *ZONE = NULL;

int
main(int argc, char **argv)
{
	scswitch_errno_t scswitcherr = SCSWITCH_NOERR;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scha_errmsg_t schaerror = { SCHA_ERR_NOERR, NULL };
	char errbuff[BUFSIZ];
	dc_error_t dcs_error;
	uint_t ismember;
	/* flag to check whether we are inside a CZ or not */
	boolean_t cz_flag = B_FALSE;

	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	/* Initialize and promote to euid=0 */
	cl_auth_init();
	cl_auth_promote();

	/* must be root to proceed */
	if (getuid() != 0) {
		schaerror.err_code = SCHA_ERR_ACCESS;
		(void) sprintf(errbuff, "%s\n",
		    gettext("Not authorized to use this command."));
		schaerror.err_msg = strdup(errbuff);
		goto exit_main;
	}

#if SOL_VERSION >= __s10
	/* Check whether we are inside a CZ */
	scconferr = scconf_zone_cluster_check(&cz_flag);
	if (scconferr != SCCONF_NOERR) {
		scswitcherr = scswitch_convert_scconf_error_code(
		    scconferr);
		goto exit_main;
	}

	if (cz_flag) {
		/* We are inside a CZ. So, disallow */
		scswitcherr = SCSWITCH_ECZONE;
		goto exit_main;
	}
#endif

	/* initialize clconf */
	dcs_error = dcs_initialize();
	if (dcs_error != DCS_SUCCESS) {
		scswitcherr = SCSWITCH_ENOCLUSTER;
		goto exit_main;
	}

	/* make sure that we are active in the cluster */
	ismember = 0;
	scconferr = scconf_ismember(0, &ismember);
	if (scconferr != SCCONF_NOERR || !ismember) {
		switch (scconferr) {
		case SCCONF_EPERM:
			scswitcherr = scswitch_convert_scconf_error_code(
			    scconferr);
			goto exit_main;

		default:
			scswitcherr = SCSWITCH_ENOCLUSTER;
			goto exit_main;
		} /* lint !e788 */
	}

	exit_code = scswitcherr;

	parse_cmd_args(argc, argv, &scswitcherr, &schaerror);

exit_main:

	if (scswitcherr == SCSWITCH_EUSAGE)
		usage();
	else
		scswitcherr = print_scswitch_error(scswitcherr, schaerror);

	exit_code = (int)scswitcherr;
	return ((int)scswitcherr);
}

/*
 * usage
 *
 *	Print a simple usage message to stderr.
 *
 * Possible return values:
 *	NONE
 */
static void
usage(void)
{
	/* Print the common usage */
	(void) fprintf(stderr, "%s:\n", gettext("Usage"));

	/* New usage message from inter_rg features */
	(void) fprintf(stderr,
	    "       scswitch -z\n"
	    "       scswitch -z -g resource_grp[,...] "
	    "[-h node[,...]]\n");
	(void) fprintf(stderr,
	    "       scswitch -z -g resource_grp[,...] -h node[,...]\n");

	(void) fprintf(stderr,
		    "       scswitch -z -D device_group_name[,...] -h node\n");

	/* New usage message from inter_rg features */
	(void) fprintf(stderr,
	    "       scswitch -S -h from_node [-K timeout]\n");

	(void) fprintf(stderr,
		    "       scswitch -R -h node[,...] -g resource_grp[,...]\n"
		    "       scswitch -m -D device_service_name[,...]\n"
		    "       scswitch -e | -n [-M] -j resource[,...]\n"
		    "       scswitch -e | -n [-M] -j resource [-h node[,...]]\n"
		    "       scswitch -u | -o -g resource_grp[,...]\n"
		    "       scswitch -c -h node[,...] -j resource[,...] "
		    "-f flag_name\n"
		    "       scswitch -Z [-g resource_grp[,...]]\n"
		    "       scswitch -F -g resource_grp[,...]"
		    " | -D device_group_name[,...]\n");

	/* Print the usage for suspend_resume feature */
	(void) fprintf(stderr,
	    "       scswitch -Q [-g resource_grp[,...]] [-k]\n");
	(void) fprintf(stderr,
	    "       scswitch -s [-g resource_grp[,...]] [-k]\n"
	    "       scswitch -r [-g resource_grp[,...]]\n");

	(void) fprintf(stderr, "\n");
}

/*
 * parse_cmd_args
 *	Parses command line arguments.
 *
 * Possible return values:
 *	NONE
 */
static void
parse_cmd_args(
	int argc, char **argv,
	scswitch_errno_t *scswitcherr,
	scha_errmsg_t *schaerror)
{
	int	c;
	int	i = 0;
	int zflg = 0, hflg = 0, gflg = 0, Dflg = 0, Rflg = 0, Fflg = 0;
	int mflg = 0, eflg = 0, nflg = 0, Mflg = 0, jflg = 0, Qflg = 0;
	int uflg = 0, oflg = 0, cflg = 0, fflg = 0, Zflg = 0, Sflg = 0;
	int kflg = 0, sflg = 0, rflg = 0, offline_flg = 0;
	int Kflg = 0, k, evac_timeout = -1;
	ushort_t evac_to_ushort = 60;
	char junk[4];
	char *gopts = (char *)0; /* comma-separated list of RG names */
	char *hopts = (char *)0; /* comma-separated list of node names */
	char *jopts = (char *)0; /* comma-separated list of resource names */
	char *Dopts = (char *)0; /* comma-separated list of device names */
	size_t goptlen = 1;
	size_t hoptlen = 1;
	size_t joptlen = 1;
	size_t Doptlen = 1;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_nodeid_t nodeid;
	scswitch_errbuff_t errbuff = { SCSWITCH_NOERR, NULL };
	char **rgnames = NULL;
	char **nodenames = NULL;
	boolean_t verbose_flag = B_FALSE;
	boolean_t is_scshutdown = B_FALSE;
	char *zonename = NULL;

	char *nodelist = NULL;
	uint_t ismix;

	/* Europa (farm) related */
	boolean_t iseuropa;		/* set to B_TRUE if europa */

	/* flag to set the value for switch resource */
	scha_switch_t set_switch_flag = SCHA_SWITCH_DISABLED;

	/* flag to set the value for change state */
	scha_scswitch_obj_state_t change_state_flag = SCSWITCH_UNMANAGED;

	/* flag to clear the error condition */
	scswitch_error_flag_t flag_name = UPDATE_FAILED;

	*scswitcherr = SCSWITCH_NOERR;

	/* Check if europa is enabled */
	scconferr = scconf_iseuropa(&iseuropa);
	if (scconferr != SCCONF_NOERR) {
		*scswitcherr = scswitch_convert_scconf_error_code(scconferr);
		goto cleanup;
	}

	/* dlopen libscxcfg */
	if (iseuropa) {
		scconferr = scconf_libscxcfg_open();
		if (scconferr != SCCONF_NOERR) {
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/* Demote to normal uid */
	cl_auth_demote();

	/* check for options */
	while ((c = getopt(argc, argv, "zh:g:D:RmenMj:uocf:ZFSK:Qksr"))
	    != EOF) {
		switch (c) {
		case 'z':
			zflg ++;
			break;

		case 'h':
			hoptlen += (hflg) ? strlen(optarg) + sizeof (',') :
			    strlen(optarg);
			if (hopts == NULL)
				hopts = (char *)calloc(1, hoptlen);
			else
				hopts = (char *)realloc(hopts, hoptlen);

			if (hopts == NULL) {
				*scswitcherr = SCSWITCH_ENOMEM;
				goto cleanup;
			}
			if (hflg)
				(void) strcat(hopts, ",");
			(void) strcat(hopts, optarg);
			hflg++;
			break;

		case 'g':
			/* Disallow an empty string for RG name */
			if (*optarg == '\0') {
				*scswitcherr = SCSWITCH_EUSAGE;
				goto cleanup;
			}
			goptlen += (gflg) ? strlen(optarg) + sizeof (',') :
			    strlen(optarg);
			if (gopts == NULL)
				gopts = (char *)calloc(1, goptlen);
			else
				gopts = (char *)realloc(gopts, goptlen);

			if (gopts == NULL) {
				*scswitcherr = SCSWITCH_ENOMEM;
				goto cleanup;
			}
			if (gflg)
				(void) strcat(gopts, ",");
			(void) strcat(gopts, optarg);
			gflg++;
			break;

		case 'D':
			Doptlen += (Dflg) ? strlen(optarg) + sizeof (',') :
			    strlen(optarg);
			if (Dopts == NULL)
				Dopts = (char *)calloc(1, Doptlen);
			else
				Dopts = (char *)realloc(Dopts, Doptlen);

			if (Dopts == NULL) {
				*scswitcherr = SCSWITCH_ENOMEM;
				goto cleanup;
			}
			if (Dflg)
				(void) strcat(Dopts, ",");
			(void) strcat(Dopts, optarg);
			if ((*scswitcherr = check_device_service_list(&Dopts))
			    == SCSWITCH_ENAME) {
				(void) fprintf(stderr, gettext(
				    "%s: operation is not supported for this"
				    " device group type\n"), progname);
				goto cleanup;
			}

			/* Cannot switch device in non-global zones */
			if (sc_zonescheck() != 0) {
				goto cleanup;
			}
			Dflg++;
			break;

		case 'R':
			Rflg ++;
			break;

		case 'm':
			mflg ++;
			break;

		case 'e':
			cl_auth_check_command_opt_exit(*argv, "-e",
			    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);

			eflg ++;
			set_switch_flag = SCHA_SWITCH_ENABLED;
			break;

		case 'n':
			cl_auth_check_command_opt_exit(*argv, "-n",
			    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);

			nflg ++;
			set_switch_flag = SCHA_SWITCH_DISABLED;
			break;

		case 'M':
			Mflg ++;
			if (Mflg > 1) {
				*scswitcherr = SCSWITCH_EUSAGE;
				goto cleanup;
			}
			break;

		case 'j':
			joptlen += (jflg) ? strlen(optarg) + sizeof (',') :
				strlen(optarg);
			if (jopts == NULL)
				jopts = (char *)calloc(1, joptlen);
			else
				jopts = (char *)realloc(jopts, joptlen);

			if (jopts == NULL) {
				*scswitcherr = SCSWITCH_ENOMEM;
				goto cleanup;
			}
			if (jflg)
				(void) strcat(jopts, ",");
			(void) strcat(jopts, optarg);
			jflg++;
			break;

		case 'u':
			cl_auth_check_command_opt_exit(*argv, "-u",
			    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);

			uflg ++;
			change_state_flag = SCSWITCH_UNMANAGED;
			break;

		case 'o':
			cl_auth_check_command_opt_exit(*argv, "-o",
			    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);

			oflg ++;
			change_state_flag = SCSWITCH_OFFLINE;
			break;

		case 'c':
			cflg ++;
			break;

		case 'f':
			fflg ++;
			if (strcasecmp(optarg, UPDATEFAILED) == 0) {
				flag_name = UPDATE_FAILED;
			} else if (strcasecmp(optarg, INITFAILED) == 0) {
				flag_name = INIT_FAILED;
			} else if (strcasecmp(optarg, FINIFAILED) == 0) {
				flag_name = FINI_FAILED;
			} else if (strcasecmp(optarg, STOPFAILED) == 0) {
				flag_name = STOP_FAILED;
			} else {
				*scswitcherr = SCSWITCH_EINVAL;
				goto cleanup;
			}
			break;

		case 'Z':
			Zflg ++;
			break;

		case 'F':
			Fflg ++;
			break;

		case 'S':
			Sflg ++;
			break;

		case 'K':
			Kflg++;
			k = sscanf(optarg, "%d%1s", &evac_timeout, junk);
			if (Kflg > 1 || k != 1) {
				*scswitcherr = SCSWITCH_EUSAGE;
				goto cleanup;
			}
			if (evac_timeout < 0 ||
			    evac_timeout > (int)UINT16_MAX) {
				*scswitcherr = SCSWITCH_EINVAL;
				(void) fprintf(stderr, gettext(
				    "%s: -K operand must be an integer between "
				    "0 and %d\n"), progname, (int)UINT16_MAX);
				goto cleanup;
			}
			evac_to_ushort = (ushort_t)evac_timeout;
			break;
		case 'Q':
			/* Quiesce the groups specified from -g */
			Qflg++;
			break;
		case 'k':
			kflg++;
			if (kflg > 1) {
				*scswitcherr = SCSWITCH_EUSAGE;
				goto cleanup;
			}
			break;
		case 's':
			sflg++;
			break;
		case 'r':
			rflg++;
			break;
		case '?':
		default:
			*scswitcherr = SCSWITCH_EUSAGE;
			goto cleanup;
		}
	}
	/* If all the agrguments are not parsed */
	if (argc != optind) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}

	/* sum of basic options */
	i = zflg + Rflg + mflg + eflg + nflg + uflg + oflg + cflg + Zflg
	    + Fflg + Sflg + Qflg + sflg + rflg;

	/* only one form of the command allowed */
	if (i > 1) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}


	/*
	 * Make sure that -f flag appears only once.
	 * Note that -D, -g, -h, -j can appear more than once and args are
	 * concatenated.
	 */
	if (fflg > 1) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}

	/* disallow h flag with certain other flags */
	if (hflg && (Fflg || mflg || uflg || oflg || Zflg ||
		    Qflg || sflg || rflg)) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}

	/* Allow s flag and Q flag only with g flag and/or k flag */
	i = 0;
	i = hflg + Dflg + jflg + fflg + Kflg;
	if ((sflg || Qflg || rflg) && (i > 0)) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}
	if (rflg && kflg) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}

	/* allow k flag only with -Q and -s */
	if (kflg && !Qflg && !sflg) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}

	/* allow K flag only with -S */
	if (Kflg && !Sflg) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}

	/* allow j flag with -e, -n or -c */
	if (jflg && (!eflg && !nflg && !cflg)) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}

	/* allow M flag with -e or -n */
	if (Mflg && (!eflg && !nflg)) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}

	/* allow f flag with -c */
	if (fflg && !cflg) {
		*scswitcherr = SCSWITCH_EUSAGE;
		goto cleanup;
	}

	/* If Europa, check if nodelist has a mix of server and farm nodes */
	if (iseuropa && hflg) {
		if ((zflg && gflg) || cflg) {
			nodelist = strdup(hopts);
			if (nodelist == NULL) {
				*scswitcherr = SCSWITCH_ENOMEM;
				goto cleanup;
			}

			scconferr = scconf_isserverfarm_mix(nodelist, &ismix);
			if (scconferr != SCCONF_NOERR) {
				*scswitcherr =
				    scswitch_convert_scconf_error_code(
				    scconferr);
				goto cleanup;
			} else {
				if (ismix) {
					(void) fprintf(stderr, "%s\n",
					    gettext("You cannot specify "
					    "a mix of server and farm "
					    "nodes in the nodelist."));
					*scswitcherr = SCSWITCH_EINVAL;
					goto cleanup;
				}
			}
		}
	}

	if (Zflg) {

		cl_auth_check_command_opt_exit(*argv, "-Z",
		    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);
		if (gflg) {
			*schaerror = scswitch_dup_list(&rgnames, gopts);
			if (schaerror->err_code != SCHA_ERR_NOERR)
				goto cleanup;
		}
	}

	if (zflg) {

		if (gflg) {
			cl_auth_check_command_opt_exit(*argv, "-z -g",
			    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);
			/* Promote to euid=0 */
			cl_auth_promote();

			/* Generate event */
			generate_event(argc, argv);

			/*
			 * If node list is provided, switch RGs onto
			 * specified masters
			 */
			if (hflg) {
				*schaerror = scswitch_switch_rg(hopts, gopts,
				    verbose_flag, ZONE);

			/*
			 * If node list is missing, rebalance RGs (bring online
			 * on most-preferred masters)
			 */
			} else {
				*schaerror = scswitch_dup_list(&rgnames, gopts);
				if (schaerror->err_code != SCHA_ERR_NOERR) {
					goto cleanup;
				}
				*schaerror = rgm_scswitch_switch_rg(NULL,
				    (const char **)rgnames, RGACTION_REBALANCE,
				    0, verbose_flag, ZONE);
				rgm_free_strarray(rgnames);
			}
		} else if (!hflg && !gflg && !Dflg) {
			/*
			 * No nodelist or rglist; all managed RGs are
			 * to be rebalanced
			 */
			cl_auth_check_command_opt_exit(*argv, "-z",
			    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);
			/* Promote to euid=0 */
			cl_auth_promote();

			/* Generate event */
			generate_event(argc, argv);

			*schaerror = rgm_scswitch_switch_rg(NULL, NULL,
			    RGACTION_REBALANCE, 0, verbose_flag, ZONE);
			if (schaerror->err_code != SCHA_ERR_NOERR) {
				goto cleanup;
			}
		} else if (hflg && Dflg) {
			cl_auth_check_command_opt_exit(*argv, "-z -D",
			    CL_AUTH_DEVICE_ADMIN, SCSWITCH_ESCHA_ERR);
			/* Promote to euid=0 */
			cl_auth_promote();

			/* Generate event */
			generate_event(argc, argv);

			*scswitcherr = scswitch_switch_device_service(hopts,
				Dopts);
		} else {
			*scswitcherr = SCSWITCH_EUSAGE;
		}
	} else if (Rflg) {
		if (hflg && gflg) {
			cl_auth_check_command_opt_exit(*argv, "-R",
			    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);
			/* Promote to euid=0 */
			cl_auth_promote();

			/* Generate event */
			generate_event(argc, argv);

			*schaerror = scswitch_restart_rg(hopts, gopts,
			    verbose_flag, ZONE);
		} else {
			*scswitcherr = SCSWITCH_EUSAGE;
		}
	} else if (mflg) {
		if (Dflg) {
			cl_auth_check_command_opt_exit(*argv, "-m",
			    CL_AUTH_DEVICE_ADMIN, SCSWITCH_ESCHA_ERR);
			offline_flg = 1;

			/* Promote to euid=0 */
			cl_auth_promote();

			/* Generate event */
			generate_event(argc, argv);

			*scswitcherr = scswitch_cli_take_service_offline(Dopts,
			    offline_flg);
		} else {
			*scswitcherr = SCSWITCH_EUSAGE;
		}
	} else if (eflg || nflg) {

		/* Promote to euid=0 */
		cl_auth_promote();

		if (jflg) {
			/* Generate event */
			generate_event(argc, argv);
		}

		/*
		 * Pass the optional arguement hopts containing
		 * hostname to the function scswitch_set_resource_switch
		 * and scswitch_set_monitor_switch .
		 */
		if (jflg && !Mflg) {
			*schaerror = scswitch_set_resource_switch(
			    jopts, set_switch_flag, B_FALSE, hopts,
			    verbose_flag, ZONE);
		} else if (jflg && Mflg) {
			*schaerror = scswitch_set_monitor_switch(jopts,
			    set_switch_flag, hopts, verbose_flag, ZONE);
		} else {
			*scswitcherr = SCSWITCH_EUSAGE;
		}
	} else if (uflg || oflg) {

		/* Promote to euid=0 */
		cl_auth_promote();

		if (gflg) {
			/* Generate event */
			generate_event(argc, argv);

			*schaerror = scswitch_change_rg_state(gopts,
			    change_state_flag, verbose_flag, ZONE);
		} else {
			*scswitcherr = SCSWITCH_EUSAGE;
		}
	} else if (cflg) {
		if (hflg && jflg && fflg) {
			cl_auth_check_command_opt_exit(*argv, "-c",
			    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);

			/* Generate event */
			generate_event(argc, argv);

			/* Promote to euid=0 */
			cl_auth_promote();

			*schaerror = scswitch_clear(
			    hopts, jopts, flag_name, ZONE);
		} else {
			*scswitcherr = SCSWITCH_EUSAGE;
		}
	} else if (Zflg) {

		/* Promote to euid=0 */
		cl_auth_promote();

		if (hflg || Dflg || Mflg || jflg || fflg) {
			*scswitcherr = SCSWITCH_EUSAGE;
		} else {
			/* Generate event */
			generate_event(argc, argv);
			*schaerror = all_rgs_online(
			    rgnames, NULL, verbose_flag);
			/* Free memory */
			rgm_free_strarray(rgnames);
		}
	} else if (Fflg) {
		if (gflg) {
			cl_auth_check_command_opt_exit(*argv, "-F -g",
			    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);

			/* Promote to euid=0 */
			cl_auth_promote();

			/* Generate event */
			generate_event(argc, argv);

			*schaerror = scswitch_take_resourcegrp_offline(
			    gopts, NULL, RGACTION_NONE, verbose_flag, ZONE);

		} else if (Dflg) {
			cl_auth_check_command_opt_exit(*argv, "-F -D",
			    CL_AUTH_DEVICE_ADMIN, SCSWITCH_ESCHA_ERR);
			/* Promote to euid=0 */
			cl_auth_promote();

			/* Generate event */
			generate_event(argc, argv);

			*scswitcherr = scswitch_cli_take_service_offline(Dopts,
			    offline_flg);
		} else {
			*scswitcherr = SCSWITCH_EUSAGE;
		}
	} else if (Qflg) {
		cl_auth_check_command_opt_exit(*argv, "-Q",
		    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);
		/* Promote to euid=0 */
		cl_auth_promote();

		/* Generate event */
		generate_event(argc, argv);

		*schaerror = scswitch_quiesce_resourcegrp(gopts,
		    NULL, (uint_t)kflg, verbose_flag, ZONE);
	} else if (Sflg) {
		cl_auth_check_command_opt_exit(*argv, "-S",
		    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);
		/* Promote to euid=0 */
		cl_auth_promote();

		if (!hflg || hopts == NULL) {
			*scswitcherr = SCSWITCH_EUSAGE;
			goto cleanup;
		}
		/*
		 * Limit -h list to one node.
		 */
		if (strchr(hopts, ',') != NULL) {
			*scswitcherr = SCSWITCH_EUSAGE;
			goto cleanup;
		}

		/*
		 * If we are evacuating this node because of an scshutdown,
		 * there is no need to proceed as the scshutdown command will
		 * halt all nodes and thus we cannot move things to a surviving
		 * node.
		 */
		if (cladm(CL_CONFIG, CL_GET_SCSHUTDOWN, &is_scshutdown) != 0) {
			*scswitcherr = SCSWITCH_EUNEXPECTED;
			goto cleanup;
		}
		if (is_scshutdown) {
			goto cleanup;
		}

		/* Generate event */
		generate_event(argc, argv);

		*schaerror = logicalnode_to_nodeidzone(hopts, &nodeid,
		    &zonename);
		if (schaerror->err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}

		errbuff = scswitch_evacuate(nodeid, zonename, hopts,
		    evac_to_ushort, schaerror, verbose_flag, ZONE);
		if (schaerror->err_code != SCHA_ERR_NOERR) {
			/*
			 * The rgm returns an error message in schaerror
			 * which gets printed by print_scswitch_error() in
			 * main().  So we don't need to print an additional
			 * (redundant) message here.
			 */
		} else if (errbuff.err_code != SCSWITCH_NOERR) {
			*scswitcherr = errbuff.err_code;
			(void) fprintf(stderr, gettext(
			    "%s:  Unable to completely evacuate device "
			    "groups from node %s\n"), progname, hopts);
			if (errbuff.err_msg) {
				(void) fprintf(stderr, "%s:  %s\n",
				    progname, errbuff.err_msg);
				free(errbuff.err_msg);
				errbuff.err_msg = NULL;
			}
		}
	} else if (sflg) {
		cl_auth_check_command_opt_exit(*argv, "-s",
		    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);
		/* Promote to euid=0 */
		cl_auth_promote();

		/* Generate event */
		generate_event(argc, argv);

		/*
		 * We now use either kill all resources or none.
		 * So pass NULL as the 2nd arg. If kflg is set, RGM
		 * will kill running methods of all resources in gopts.
		 */
		*schaerror = scswitch_suspend_resume_resourcegrp(
		    gopts, NULL, CMD_SUSPEND, (uint_t)kflg, ZONE);
	} else if (rflg) {
		cl_auth_check_command_opt_exit(*argv, "-r",
		    CL_AUTH_RESOURCE_ADMIN, SCSWITCH_ESCHA_ERR);
		/* Promote to euid=0 */
		cl_auth_promote();

		/* Generate event */
		generate_event(argc, argv);

		*schaerror = scswitch_suspend_resume_resourcegrp(
		    gopts, NULL, CMD_RESUME, 0, ZONE);
	} else {
		*scswitcherr = SCSWITCH_EUSAGE;
	}

cleanup:

	/* dlclose scxcfg. */
	if (iseuropa) {
		(void) scconf_libscxcfg_close();
	}

	if (gopts)
		free(gopts);
	if (hopts)
		free(hopts);
	if (jopts)
		free(jopts);
	if (Dopts)
		free(Dopts);

	if (nodelist)
		free(nodelist);
	if (zonename)
		free(zonename);
}

/*
 * print_scswitch_error
 * progname is a global variable.
 * Possible return values:
 *	NONE
 *
 * If scha_error contains a message string (returned by the rgmd), print
 * it even if the exit code is 0.
 */
scswitch_errno_t
print_scswitch_error(const scswitch_errno_t scswitch_error,
	const scha_errmsg_t scha_error)
{
	char msg[BUFSIZ];
	scswitch_errno_t error = scswitch_error;

	if (scswitch_error != SCSWITCH_NOERR) {
		scswitch_strerr(NULL, msg, scswitch_error);
		if (msg && *msg)
			(void) fprintf(stderr, "%s: %s\n", progname,  msg);
	} else { /* print SCHA_ERR */
		if (scha_error.err_msg) {
			(void) fprintf(stderr, "%s: %s\n", progname,
			    scha_error.err_msg);
			free(scha_error.err_msg);
		} else if (scha_error.err_code != SCHA_ERR_NOERR) {
			char *scha_msg = NULL;
			/* obtain error message from rgm */
			scha_msg = scswitch_rgmerrmsg(scha_error);
			if (scha_msg) {
				(void) fprintf(stderr, "%s: %s\n", progname,
				    scha_msg);
			}
		}
		if (scha_error.err_code != SCHA_ERR_NOERR) {
			error = SCSWITCH_ESCHA_ERR;
		}
	}
	return (error);
} /* print_scswitch_error */

/*
 * all_rgs_online
 *
 * This function attempts to "turn on" all resource groups found in the
 * given "rgnames", where "rgnames" is NULL-terminated array of resource
 * group names.  If rgnames is NULL then  RGs which are not suspended are
 * turned on.
 *
 * For purposes of this function, "turn on" has the following
 * definition:
 *
 *	- enable all resources in the group not already enabled
 *	- enable monitoring for all resources in the group not already
 *		enabled for monitoring
 *	- set the resource group to the managed state, if it is not already
 *		in the managed state
 *	- bring the resource group online, if it is not already online
 *
 * Return errors from the RGM are passed back to the caller.
 */
static scha_errmsg_t
all_rgs_online(char **rgnames, char **nodenames, boolean_t verbose_flag)
{
	scha_errmsg_t scha_err = {SCHA_ERR_NOERR, NULL};
	char **rsnames = NULL;
	char **all_rsnames = NULL;
	char **all_rgnames = NULL;
	char **rglist;
	char **tmp_rglist = NULL;
	char **tmp_rsnames;
	uint_t i, j, k;
	uint_t tmp_rglist_len = 0;
	uint_t num_rsnames = 0;
	rgm_rg_t *rg = (rgm_rg_t *)0;
	boolean_t only_local_rgs = B_FALSE;
#if SOL_VERSION >= __s10
	char localzone[ZONENAME_MAX];
	getzonenamebyid(getzoneid(), localzone, ZONENAME_MAX);
#else
	char *localzone = NULL;
#endif

	/*
	 * If rgnames is NULL, get a list of all known RGs
	 * which are not suspended.
	 */
	if (rgnames == NULL) {
		if (sc_nv_zonescheck() != 0) {
			only_local_rgs = B_TRUE;
			/*
			 * RGs whose nodelist doesn't contain this zone
			 * will be ignored.
			 */
			(void) fprintf(stderr, gettext(RWM_RG_NGZONE));
			(void) fprintf(stderr, "\n");
		}

		scha_err = scswitch_getrglist(&all_rgnames,
		    only_local_rgs, ZONE);
		if (scha_err.err_code != SCHA_ERR_NOERR) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to obtain list of all resource "
			    "groups.\n"), progname);
			goto bailout;
		}

		/* If all_rgnames is null, there are no RGs -- just return */
		if (all_rgnames == NULL) {
			return (scha_err);
		}

		for (i = 0; all_rgnames[i] != NULL; i++) {
			scha_err = rgm_scrgadm_getrgconf(
			    all_rgnames[i], &rg, localzone);
			if (scha_err.err_code != SCHA_ERR_NOERR) {
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to obtain resource group "
				    "configuration.\n"), progname);
				goto bailout;
			}

			if (rg->rg_suspended) {
				/* Suspended RGs will be ignored. */
				(void) fprintf(stderr, gettext(
				    RWM_RG_NOREBAL_SUSP), rg->rg_name);
				(void) fprintf(stderr, "\n");
				/* Free the RG configuration. */
				rgm_free_rg(rg);
				continue;
			}

			tmp_rglist = (char **)realloc(tmp_rglist,
			    (tmp_rglist_len + 2) * (sizeof (char *)));
			if (tmp_rglist == NULL) {
				scha_err.err_code = SCHA_ERR_NOMEM;
				goto bailout;
			}
			tmp_rglist[tmp_rglist_len++] = strdup(all_rgnames[i]);

			/* Free the RG configuration. */
			rgm_free_rg(rg);
		}

		/* If all RG's are suspended, --just return */
		if (tmp_rglist != NULL) {
			tmp_rglist[tmp_rglist_len]  = NULL;
			rglist = tmp_rglist;
		} else {
			return (scha_err);
		}
	} else {
		rglist = rgnames;
	}

	/*
	 * Build a single NULL-terminated array of the names of all the
	 * resources in all of the RGs in rglist, so that we can enable
	 * them in a single command to the rgm.  The rgm will take care
	 * of dependency ordering.
	 *
	 */
	for (i = 0; rglist[i] != NULL; i++) {
		/* Get the list of resources for this group */
		scha_err = rgm_scrgadm_getrsrclist(
		    rglist[i], &rsnames, localzone);
		if (scha_err.err_code != SCHA_ERR_NOERR) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to obtain resource list "
			    "for resource group \"%s\".\n"), progname,
			    rglist[i]);
			goto bailout;
		}

		/* If there are no resources, go on to the next RG */
		if (rsnames == NULL || *rsnames == NULL)
			continue;

		/* Count number of elements in rsnames */
		for (j = 0; rsnames[j]; j++)
			;
		/* k indexes the current end of the array */
		k = num_rsnames;
		num_rsnames += j;

		/*
		 * If all_rsnames is NULL, set all_rsnames to rsnames;
		 * otherwise, realloc and copy the names in.
		 */
		if (all_rsnames == NULL) {
			all_rsnames = rsnames;
		} else {
			tmp_rsnames = (char **)realloc(all_rsnames,
			    (num_rsnames + 1) * sizeof (char *));
			if (tmp_rsnames == NULL) {
				scha_err.err_code = SCHA_ERR_NOMEM;
				goto bailout;
			}
			all_rsnames = tmp_rsnames;
			/* Concatenate rsnames onto all_rsnames */
			for (j = 0; rsnames[j]; j++, k++) {
				all_rsnames[k] = rsnames[j];
			}
			all_rsnames[num_rsnames] = NULL;
			free(rsnames);
			rsnames = NULL;
		}
	}

	/*
	 * Enable all resource and enable monitoring on all resources.
	 * These actions are idempotent if any resources are already enabled.
	 */
	if (all_rsnames != NULL) {
		/* Enable all resources */
		scha_err = rgm_scswitch_set_resource_switch(
		    (const char **)all_rsnames, SCHA_SWITCH_ENABLED, B_FALSE,
		    B_FALSE, NULL, verbose_flag, ZONE);
		if (scha_err.err_code != SCHA_ERR_NOERR) {
			/*
			 * The rgm returns an error message in scha_err
			 * which gets printed by print_scswitch_error() in
			 * main().  So we don't need to print an additional
			 * (redundant) message here.
			 */
			goto bailout;
		}

		/* Enable monitoring on all resources */
		scha_err = rgm_scswitch_set_resource_switch(
		    (const char **)all_rsnames, SCHA_SWITCH_ENABLED, B_TRUE,
		    B_FALSE, NULL, verbose_flag, ZONE);
		if (scha_err.err_code != SCHA_ERR_NOERR) {
			/*
			 * The rgm returns an error message in scha_err
			 * which gets printed by print_scswitch_error() in
			 * main().  So we don't need to print an additional
			 * (redundant) message here.
			 */
			goto bailout;
		}
	}

	/*
	 * Manage all RGs in the list
	 *
	 * In this context, SCSWITCH_OFFLINE means to switch the RG from
	 * unmanaged to managed (offline) state.  This action is idempotent,
	 * i.e. it does not affect an RG that is already managed.
	 */
	scha_err = rgm_scswitch_change_rg_state((const char **)rglist,
	    SCSWITCH_OFFLINE, verbose_flag, ZONE);
	if (scha_err.err_code != SCHA_ERR_NOERR) {
		/*
		 * The rgm returns an error message in scha_err
		 * which gets printed by print_scswitch_error() in
		 * main().  So we don't need to print an additional
		 * (redundant) message here.
		 */
		goto bailout;
	}

	/*
	 * Bring online (rebalance) all RGs in the list.
	 * If an RG list was entered by the user, pass rglist for the
	 * second argument; otherwise, pass NULL, indicating that the
	 * rgmd should generate the list of RGs.  This allows rgmd to
	 * avoid rebalancing RGs that are currently suspended.
	 */
	if (nodenames == NULL) {
		scha_err = rgm_scswitch_switch_rg(NULL,
		    (const char **)rglist,
		    RGACTION_REBALANCE, 0, verbose_flag, ZONE);

	} else  {
		scha_err = rgm_scswitch_switch_rg((const char **)nodenames,
		    (const char **)rglist,
		    RGACTION_ONLINE, 0, verbose_flag, ZONE);
	}

	/*
	 * If (scha_err.err_code != SCHA_ERR_NOERR), the error message
	 * gets printed by print_scswitch_error() in main().  So we
	 * don't need to print an additional (redundant) message here.
	 */

bailout:
	/*
	 * Suppress lint Warning(644) [c:38]:
	 *	all_rsnames (line 731) may not have been initialized
	 * This may be related to the use of realloc() above.  However,
	 * all_rsnames certainly has been initialized.
	 */
	rgm_free_strarray(all_rsnames);	/* lint !e644 */
	rgm_free_strarray(all_rgnames);

	/* Free the temporary RG list containing non-suspended RG's */
	if (tmp_rglist) {
		for (i = 0; i < tmp_rglist_len && tmp_rglist[i]; i++)
			free(tmp_rglist[i]);
		free(tmp_rglist);
	}
	return (scha_err);
}

/*
 * Checks the specified service list for the existence of any
 * multi-owner device services. If any exist then they are
 * removed and a new list is returned. If only multi-owner
 * device services were specified then an error is returned.
 *
 * Possible return values:
 *	SCSWITCH_ENOMEM
 *	SCSWITCH_ENAME
 * 	SCSWITCH_NOERR
 */
scswitch_errno_t
check_device_service_list(char **service_list)
{

	char *slist = NULL;
	char *service_name = NULL;
	uint_t len = 0;

	/*
	 * Determine the size of the current service_list and add
	 * space for a trailing comma.
	 */
	len = strlen(*service_list) + 2;

	if ((slist = (char *)calloc(len, sizeof (char))) == NULL) {
		return (SCSWITCH_ENOMEM);
	}
	slist[0] = '\0';

	/*
	 * Traverse the current device service list and remove
	 * any multi-owner device services which might appear
	 * in the list.
	 */
	service_name = strtok(*service_list, ",");
	while (service_name != NULL) {
		/*
		 * Skip over any multi-owner device services and
		 * add all others to the new service list.
		 */
		if (!scconf_is_local_device_service(service_name)) {
			(void) strlcat(slist, service_name, len);
			(void) strlcat(slist, ",", len);
		}
		service_name = strtok(NULL, ",");
	}
	len = strlen(slist);

	/*
	 * Remove the trailing comma if it exists.
	 */
	if (slist[len - 1] == ',') {
		slist[len - 1] = '\0';
	}

	/*
	 * Return the new service list if it has any valid
	 * elements in it.
	 */
	if (len > 0) {
		free(*service_list);
		*service_list = slist;
		return (SCSWITCH_NOERR);
	} else {
		/*
		 * The list contained only multi-owner device services
		 * so return an error to indicate this is not a
		 * valid operation.
		 */
		return (SCSWITCH_ENAME);
	}
}

/*
 * generate_event
 *
 * Input:
 *      int    - number of arguments to main()
 *      char** - arguments to main().
 *
 * Possible return values: none
 */
static
void generate_event(int argc, char ** argv) {
	cl_cmd_event_init(argc, argv, &exit_code);
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);
}
