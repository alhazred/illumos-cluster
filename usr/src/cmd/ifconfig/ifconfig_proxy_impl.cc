//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ifconfig_proxy_impl.cc	1.4	08/07/17 SMI"

//
// Ifconfig proxy server object Implementation.
//
#include <libintl.h>
#include <locale.h>
#include <ifconfig_proxy_impl.h>
#include <h/remote_exec.h>
#include <orb/infrastructure/orb_conf.h>
#include <orb/infrastructure/orb.h>
#include <sys/os.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <sys/vc_int.h>

#define	MAX_CMD_LEN	8192
#define	IFCONFIG_PROXY_CMD "/usr/cluster/lib/sc/ifconfig_server_proxy"

//
// This routine returns true if the ifconfig_server interface is alive and
// working. It can be used to tell if the object reference obtained from
// the name server is valid or not.
//
bool
ifconfig_server_impl::is_alive(Environment &)
{
	return (true);
}

//
// Unreference notification.
//
void
ifconfig_server_impl::_unreferenced(unref_t cookie)
{
	(void) _last_unref(cookie);
	exit(0);
}

//
// Interface definition.
//

//
// Execute the ifconfig program specified by 'ifconfig_cmd' in user space.
//
// Arguments:
//	ifconfig_cmd: ifconfig command with its arguments to be executed.
// Output:
//	On failure, returns an non-zero value. On success returns
//	0.
//	retval: exit status of the command.
//	stdout_str: Standard output that the command generates.
//	stderr_str: Standard error that the command generates.
//
// The caller of this interface has to do the necessary cleanup of the
// memory allocated to these strings.
//
int32_t
ifconfig_server_impl::handle_ifconfig_cmd(int32_t &retval,
    CORBA::String_out stdout_str, CORBA::String_out stderr_str,
    const char *cmd_args, Environment &e)
{
	int		exec_ret_val;
	nodeid_t 	nid = orb_conf::local_nodeid();
	char		clexec_name[ZONENAME_MAX + 4];

	os::sprintf(clexec_name, "cl_exec.%d", nid);

	remote_exec::cl_exec_var	cl_exec_v;
	naming::naming_context_var	ctx_v = ns::root_nameserver();
	CORBA::Object_var		obj_v;
	CORBA::Exception		*exp;
	char				cmd[MAX_CMD_LEN];
	char				*cluster_namep;
	char				*stderrstr = NULL;

	uint32_t clid = e.get_cluster_id();
	if (clid < MIN_CLUSTER_ID) {
		//
		// Execution from a global zone or 1334 zone is not
		// supported.
		//
		stderrstr = new char[100];
		ASSERT(stderrstr != NULL);
		(void) sprintf(stderrstr, gettext("cluster id : %d, "
		    "ifconfig proxy can be initiated only from a virtual "
		    "cluster node.\n"), clid);
		stderr_str = stderrstr;
		return (1);
	}
	exec_ret_val = clconf_get_cluster_name(clid, &cluster_namep);

	if (exec_ret_val != 0) {
		stderrstr = new char[100];
		ASSERT(stderrstr != NULL);
		(void) sprintf(stderrstr, gettext("Failed to get the cluster "
		    "name from cluster id %d.\n"), exec_ret_val);
		stderr_str = stderrstr;
		return (1);
	}

	ASSERT((strlen(IFCONFIG_PROXY_CMD) + strlen(cluster_namep) +
	    strlen(cmd_args)) < MAX_CMD_LEN);
	(void) sprintf(cmd, "%s %s %s", IFCONFIG_PROXY_CMD, cluster_namep,
	    cmd_args);

	delete [] cluster_namep;

	obj_v = ctx_v->resolve(clexec_name, e);

	if ((exp = e.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) != NULL) {
			e.clear();
			stderrstr = new char[100];
			ASSERT(stderrstr != NULL);
			(void) sprintf(stderrstr, gettext("Nodeid : %d, "
			    "remote execution service not up.\n"), nid);
			stderr_str = stderrstr;
			return (1);
		}
	} else {
		//
		// Test to see that the object we got from the
		// name server is active.
		//
		cl_exec_v = remote_exec::cl_exec::_narrow(obj_v);
		ASSERT(!CORBA::is_nil(cl_exec_v));
		(void) cl_exec_v->is_alive(e);
		if (e.exception() != NULL) {
			e.clear();
			stderrstr = new char[100];
			ASSERT(stderrstr != NULL);
			(void) sprintf(stderrstr, gettext("The remote "
			    "execution server object is inactive.\n"));
			stderr_str = stderrstr;
			return (1);
		}
	}
	//
	// Here we get the stdout and stderr of the task.
	// We inform the infrastructure that we do require this info to be
	// passed back to us.
	//
	exec_ret_val = cl_exec_v->exec_program_get_output(retval, stdout_str,
	    stderr_str, cmd, remote_exec::DEF, 0, e);

	return (exec_ret_val);
}
