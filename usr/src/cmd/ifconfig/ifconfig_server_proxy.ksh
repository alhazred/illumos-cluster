#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

# 
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)ifconfig_server_proxy.ksh	1.9	09/03/09 SMI"
#
# ifconfig_server_proxy.ksh
#
# ifconfig_server_proxy
#
# This script implements the proxy ifconfig command that is executed
# in the global zone on behalf of a virtual cluster node. The following
# ifconfig commands are executed in the global zone due to the lack of
# privilege in the virtual cluster node:
# ---------------------------------------------------------------
# ifconfig -a [inet | inet6]
# This will list the interfaces that has the following properties:
# 1) The NIC has been explicitly authorized for use in the zone cluster.
# 2) Belongs to the same IPMP group as one of the NIC's that is
#    part of net resource of the virtual cluster node.
# 3) Is a logical interface associated with the virtual cluster
#    zone.
# 4) Is clprivnet0
# ---------------------------------------------------------------
# ifconfig -a[4|6|D][u|d] [inet | inet6]
# This will list the interfaces that is a subset of interfaces
# listed in ifconfig -a output.
# -------------------------------------------------------------
# ifconfig <interface>
# This will display the information about the interface if the
# interface falls into one of the four categories specified above
# for the ifconfig -a option.
# ----------------------------------------------------------------
# ifconfig <interface> [addif|removeif] <address/hostname>
# The physical interfaces that are permitted in this command are
# the interfaces that will be listed in the ifconfig -a output of
# the zone. If a hostname is specified, it is resolved and replaced
# by the corresponding IP address from within the zone. The IP
# address should be one of the IP addresses exclusively assigned
# to the virtual cluster node.
#-----------------------------------------------------------------
# ifconfig <logical_interface> [ plumb | unplumb | up | down | set ]
# The logical interfaces that are permitted in this command are
# the interfaces that belong to the virtual cluster zone.
# If the logical interface does not exist, the ifconfig proxy does
# not support creation of that logical interface.
#-----------------------------------------------------------------
# ifconfig <physical_nic> plumb
# The proxy will support command to plumb a physical interface
# if it is not plumbed already. If the nic is already plumbed
# the proxy will just return success.
#----------------------------------------------------------------
#

#program name and args list. Used for log messages.
if [ -z $PROG ]; then
        typeset -r PROG=${0##*/}
fi
IFCONFIG_CMD=/usr/sbin/ifconfig
ZONEINFO_CMD=/usr/sbin/zonecfg
CZZONEINFO_CMD=/usr/cluster/lib/sc/sczonecfg
SC_IP_CMD=/usr/cluster/lib/scadmin/lib/sc_ip.pl
#
# The list of keywords that can appear after the plumb
# command instead of a hostname/IP address.
#
typeset -r proxy_keywords="up down zone"
#
# The proxy ifconfig command handles commands with these keywords
# in the local zone.
#
typeset -r restrict_keywords="group private -private nud -nud standby -standby subnet tdst tsrc trailers -trailers userrc xmit -xmit encaplimit -encaplimit thoplmit router -router -zone"
#
# The proxy ifconfig command conditionally handles commands with
# these keywords either in the local zone or global zone based on
# other arguments in the ifconfig command.
#
typeset -r conditional_keywords="netmask broadcast"
########################################################################
# is_in_list elem list
# Check if an element elem is part of the list.
# Return Value:
# 	0 if elem is part of the list
#	1 Otherwise
#######################################################################
is_in_list() 
{
	typeset elem
	typeset index
	elem=$1
	shift
	for index in $*
	do
		if [ "$index" = "$elem" ]; then
			# Found.
			return 0
		fi
	done
	return 1
}

#######################################################################
# get_ip host_name | ip_address
# Fetch the IP address, given a hostname. If the argument passed is
# an IP address, this results in the same IP address being returned.
# Return Value:
#       0 If a match is found
#       1 Otherwise
#
#######################################################################
get_ip()
{
        typeset ip_addr

        ip_addr=`$SC_IP_CMD convert_to_ip $1`
        if [ -z $ip_addr ]; then
                return 1
        else
                echo $ip_addr
                return 0
        fi
}

########################################################################
# is_assigned_nic nic zone_name
# Check if the physical nic is assigned to the zone.
# Return Value:
# 	0 nic is assigned to the zone
#	1 otherwise.
#
########################################################################
is_assigned_nic()
{
	typeset zone_nics

	zone_nics=`$ZONEINFO_CMD -z $2 info net | awk '/physical:/ { print $2 }'`
	is_in_list $1 $zone_nics || return 1
	return 0
}

########################################################################
# get_group_list zone_name
# Get the list of IPMP groups associated with the NIC's that are part
# of the net resource of the virtual cluster node.
# Return value:
#	0 Success
#	1 Failure
########################################################################
get_group_list() 
{
	typeset zone_nics
	typeset nic
	typeset grouplist
	typeset groupname

	zone_nics=`$ZONEINFO_CMD -z $1 info net | awk '/physical:/ { print $2 }'`
	for nic in $zone_nics
	do
		groupname=`$IFCONFIG_CMD $nic | awk '/^[\t ]+groupname/ { print $2 }'`
		if [ ! -z "$groupname" ]; then
			if [ -z "$grouplist" ]; then
				grouplist=$groupname
			else
				grouplist="$grouplist $groupname"
			fi
		fi
	done
	echo $grouplist
}


###########################################################################
# get_ip_list zone_name
# Get the list of IP addresses associated with the NIC's that are part
# of the net resource of the virtual cluster node.
# Return value:
#	0 Success
#	1 Failure
###########################################################################
get_ip_list() 
{
	typeset zone_ips
	typeset entry
	typeset ip
	typeset iplist

	zone_ips=`$CZZONEINFO_CMD -z $1 info net | awk '/address:/ { print $2 }'`
	for entry in $zone_ips
	do
		#
		# The entry can be in the following formats: <IP>,
		# <IP>/<netmask-bit-count>, <hostname>, <hostname>/<netmask-bit-count>
		#
		ip=`echo $entry | /usr/bin/cut -f1 -d/`
		#
		# If we are not able to resolve the entry, we just continue
		# with the next one. This will result in the validation failing
		# only for the bad entry.
		#
		newip=`get_ip $ip`
		if [ $? -eq 0 ]; then
			if [ -z "$iplist" ]; then
				iplist=$newip
			else
				iplist="$iplist $newip"
			fi
        	fi
	done
	echo $iplist
}

########################################################################
# is_logical_interface nic
# check if the interface is a logical interface
# Return value:
#	0 if the interface is a logical interface
#	1 Otherwise
#
########################################################################
is_logical_interface()
{
	typeset logical_interface

	logical_interface=`echo $1 | awk '{ FS=":"; print $2 }'`
	if [ ! -z "$logical_interface" ]; then
		return 0
	else
		return 1
	fi
}

#########################################################################
# ifconfig_list zone_name args
# List the logical interfaces that belong to the zone. Apart from
# this, also display the physical interfaces that 
# 1) Are assigned to the virtual cluster node.
# 2) Belongs to the same IPMP group as one of the NIC's that
#    is assigned to the virtual cluster node.
# 3) Is clprivnet0.
# Return Value:
#	0 Success
#	1 Failure
#
########################################################################
ifconfig_list()
{
	typeset zone_name
	typeset nics_in_group
	typeset index
	typeset groupname
	typeset ret
	typeset logical_interface
	typeset physical_interface
	typeset	ipmp_groups
	typeset znname;
	typeset tmpfile=ifconfig_proxy.$$

	znname=$1
	shift

	rm -f /tmp/$tmpfile
	$IFCONFIG_CMD $* > /tmp/$tmpfile
	ret=$?
	if [ $ret -ne 0 ]; then
		return $ret
	fi
	nics=`cat /tmp/$tmpfile | awk '!/^\t/ { print $1 }'`
	#
	# Walk through each NIC and store the ones that have been
	# assigned to the virtual cluster or those that
	# belong to the same IPMP group as one of the NIC's that
	# has been assigned to the virtual cluster node.
	#
	for index in $nics
	do
		logical_interface=`echo $index | awk '{ FS=":"; print $2 }'`
		physical_interface=`echo $index | awk '{ FS=":"; print $1 }'`

		if [ ! -z "$logical_interface" ]; then
			#
			# If this logical interface belongs to the zone,
			# add it to the list.
			#
			zone_name=`$IFCONFIG_CMD $physical_interface:$logical_interface | awk '/^[\t ]+zone/ { print $2 }'`
			if [ "$zone_name" = "$znname" ]; then
				nics_in_group="$nics_in_group $physical_interface:$logical_interface"
			fi
		else
			#
			# If the NIC is assigned to the zone,
			# or it belongs to an IPMP group that has a NIC
			# assigned to the zone, add it to the list.
			#
			is_assigned_nic $physical_interface $znname
			if [ $? -eq 0 ]; then
				nics_in_group="$nics_in_group $physical_interface"
			else
				groupname=`$IFCONFIG_CMD $physical_interface | awk '/^[\t ]+groupname/ { print $2 }'`
				if [ ! -z "$groupname" ]; then
					#
					# check if the nic is assigned to the
					# zone.
					#
					ipmp_groups=`get_group_list $znname`
					is_in_list $groupname $ipmp_groups
					if [ $? -eq 0 ]; then
						nics_in_group="$nics_in_group $physical_interface"
					fi
				fi
			fi
			if [ "$physical_interface" = "clprivnet0" ]; then
				nics_in_group="$nics_in_group $physical_interface"
			fi
		fi
	done
	for index in $nics_in_group
	do
		$IFCONFIG_CMD $index
	done
	rm -f /tmp/$tmpfile
	return 0
}

#######################################################################
# check_physical_nic nic zone_name
# check if the interface is a physical interface. If so, for the nic
# to be accessible to the zone, it has to be clprivnet0 or should
# have a IPMP group associated with it and it should be
# assigned to the zone or the IPMP group should be the same as
# the IPMP group of the nic that is assigned to the zone.
# Return value:
#	0 on Success
#	1 on Failure
########################################################################
check_physical_nic()
{
	typeset groupname
	typeset grouplist

	is_logical_interface $1
	if [ $? -eq 0 ]; then
		#
		# If this is a logical interface, we wanted a
		# physical NIC 
		#
		return 1
	fi
	#
	# If the NIC is clprivnet0, return success.
	#
	if [ "$1" = "clprivnet0" ]; then
		return 0
	fi
	#
	# If the interface does not exist, return failure.
	#
	$IFCONFIG_CMD $1 > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		return 1
	fi
	#
	# If the NIC is assigned to the zone, return success.
	#
	is_assigned_nic $1 $2
	if [ $? -eq 0 ]; then
		return 0
	fi
	#
	# If the NIC interface is part of the IPMP group and is assigned to the
	# zone, return success.
	#
	groupname=`$IFCONFIG_CMD $1 | awk '/^[\t ]+groupname/ { print $2 }'`
	if [ ! -z "$groupname" ]; then
		#
		# check if the nic is assigned to the zone.
		#
		grouplist=`get_group_list $2`
		is_in_list $groupname $grouplist || return 1
		return 0
	else
		return 1
	fi
}

########################################################################
# check_logical_interface nic zone
# check if the interface is a logical interface. If so, for the interface
# to be accessible to the zone, it has to belong to the zone.
# If the logical interface does not exist, then the proxy ifconfig server
# does not support creation of the logical interface with the plumb
# sub-command.
# Return value:
#	0 on Success
#	1 on Failure
########################################################################
check_logical_interface()
{
	typeset zone_name
	typeset physical_nic

	is_logical_interface $1
	if [ $? = 1 ]; then
		#
		# Got a physical NIC instead of a logical interface
		#
		return 1
	fi
	#
	# If the interface does not exist, return failure. 
	#
	$IFCONFIG_CMD $1 > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		return 1
	fi
	#
	# If the interface belongs to the zone, return success.
	#
	zone_name=`$IFCONFIG_CMD $1 | awk '/^[\t ]+zone/ { print $2 }'`

	if [ "$zone_name"  =  "$2" ]; then
		return 0
	else
		return 1
	fi
}

##########################################################################
#
# Main
# Return Value:
#	0 Success
#	1 Failure
#
##########################################################################
main()
{
	typeset zone_name
	typeset index
	typeset iplist
	typeset cmd=""
	typeset cmd_prev=""
	typeset ip
	typeset prefix_len

	integer proxy_cmd=0
	integer zones_cmd=0

	if [ -f /usr/bin/zonename ]; then
        	zone_name=`/usr/bin/zonename`
	else
		# pre_s10 version
		exit 1
	fi
	if [ "$zone_name" != "global" ]; then
		#
		# This command needs to be executed only in the
		# global zone.
		#
		exit 1
	fi
	#
	# The first argument is the name of the virtual cluster zone
	# from which the request originated.
	#
	zone_name=$1
	shift
	#
	# Walk through each argument of ifconfig. If the argument is
	# an IP address, it is validated so that the IP address
	# is one of the IP addresses that is part of net resouce for
	# the zone. If the argument is a sub-command
	# that is -a (list), addif (Add a logical interface),
	# removeif (Remove a logical interface), (un)plumb/set or up/down
	# of a logical interface, the command is executed.
	# Otherwise, the ifconfig server proxy has got a command
	# that it is not supposed to handle, we return an error.
	#
	for index in $*
	do
		#
		# There are a set of commands (specified by the presence
		# of a keyword in the restricted_keywords list) that
		# shoudn't be handled by the proxy in the global zone.
		# For example,
		#       ifconfig bge0:2 [ plumb | up ] [[-]private|[-]router ].
		#
		is_in_list $index $restrict_keywords
		if [ $? -eq 0 ]; then
			proxy_cmd=0
			break
		fi
		case $index in
		-*Z*'a'* |-*'a'*Z*)
			#
			# -Z option should not be handled by the
			# proxy.
			#
			zones_cmd=1
			;;
		-*'a'*)
			#
			# There are a set of commands (specified by the
			# presence of a keyword in the conditional_keywords
			# list) that should be conditionally handled by the
			# proxy in the local zone. For example,
			# ifconfig -a broadcast +
			# ifconfig -a broadcast + netmask
			#
			proxy_cmd=1
			for tmp in $*
			do
				is_in_list $tmp $conditional_keywords
				if [ $? -eq 0 ]; then
					proxy_cmd=0
					break
				fi
			done
			if [ $proxy_cmd -ne 0 ]; then
				ifconfig_list $zone_name $*
				exit $?
			fi
			;;

		'addif')
			#
			# check if the NIC specified is allowed
			# to add a logical interface.
			#
			check_physical_nic $1 $zone_name
			if [ $? -ne 0 ]; then
                		printf \
				    "$(gettext '%s: can not add logical interface to %s.')\n" "${PROG}" $1
				exit 1
			fi
			cmd="addif"
			;;

		'removeif')
			#
			# check if the NIC specified is allowed
			# to add a logical interface.
			#
			check_physical_nic $1 $zone_name
			if [ $? -ne 0 ]; then
                		printf \
				    "$(gettext '%s: can not remove nic %s or nic does not exist.')\n" "${PROG}" $1
				exit 1
			fi
			cmd="removeif"
			;;

		'plumb'| 'unplumb' | 'up' | 'down' | 'set')
			#
			# Up/down can appear as flags for other commands
			# as well.  For ex.
			# ifconfig bge0 addif <ip> netmask 0xffffff00
			# broadcast + depreacated up failover
			# We handle it here.
			#
			if [ $proxy_cmd -eq 1 ] && [ "$index" != "set" ]; then
				# This is already a proxy cmd.
				cmd=$index
			elif [ "$index" = "plumb" ] && [ $# -eq 2 ]; then
				#
				# We support plumb of a physical nic.
				# If the nic is not plumbed, we plumb
				# it, otherwise just return success.
				#
				cmd=$index
			else
				#
				# check if the NIC specified is allowed
				# for the plumb command.
				#
				check_logical_interface $1 $zone_name
				if [ $? -ne 0 ]; then
					printf "$(gettext '%s: can not %s %s.')\n" "${PROG}" $index $1
					exit 1
				fi
				cmd=$index
			fi
			;;

		*)
			#
			# Check what the previous argument was.
			# If the previous argument was "addif" or "removeif"
			# or "set", we can safely assume that the current
			# argument is an IP address.
			# If the previous command was "plumb", then the
			# current argument must be either an IP address
                        # or another sub-command.
			#
			case $cmd_prev in
			'addif'|'removeif'|'set')
				#
				# We got an IP address here. Validate
				# that it is one of the IP addresses
				# that is assigned to the zone.
				# The IP address can be associated with
				# prefix_length. We handle that here.
				#
				ip=`echo $index | awk '{ FS="/"; print $1}'`
				prefix_len=`echo $index | awk '{ FS="/"; print $2}'`
				#
				# Here we make sure that we are handling
        			# commands that set the prefix_length.
				# For example,
				#       ifconfig bge0:2 addif 1.2.3.4/32
				#
				if [ -z "$ip" ] && [ ! -z "$prefix_len" ]; then
        				printf "$(gettext '%s: Not permitted to handle %s %s.')\n" "${PROG}" "ifconfig" "$*"
					exit 1
				fi

				iplist=`get_ip_list $zone_name`
				is_in_list $ip $iplist
				if [ $? -ne 0 ]; then
                			printf "$(gettext '%s: IP %s is not authorized to use for zone %s.')\n" "${PROG}" $ip $zone_name
                			printf "$(gettext 'Update the zone cluster configuration using clzc(1CL) command to include this IP address and rerun the command.')\n"
					exit 1
				fi
				;;

			'plumb')
				#
				# The previous command was 'plumb'.
				# If the current argument is not a keyword,
				# it is safe to assume that it should
				# be an IP address or one of the keywords
				# listed in $proxy_keywords.
				# The IP address can be associated with
				# a prefix_length. We handle that here.
				#
				ip=`echo $index | awk '{ FS="/"; print $1}'`
				prefix_len=`echo $index | awk '{ FS="/"; print $2}'`
				#
				# Here we make sure that we are handling
        			# commands that set the prefix_length.
				# For example,
				#       ifconfig bge0:2 plumb set /32
				#
				if [ -z "$ip" ] && [ ! -z "$prefix_len" ]; then
        				printf "$(gettext '%s: Not permitted to handle %s %s.')\n" "${PROG}" "ifconfig" "$*"
					exit 1
				fi

				is_in_list $ip $proxy_keywords
				if [ $? -ne 0 ]; then
					iplist=`get_ip_list $zone_name`
					is_in_list $ip $iplist
					if [ $? -ne 0 ]; then
                				printf "$(gettext '%s: IP %s is not authorized to use for zone %s.')\n" "${PROG}" $ip $zone_name
                				printf "$(gettext 'Update the zone cluster configuration using clzc(1CL) command to include this IP address and rerun the command.')\n"
						exit 1
					fi
				fi
				;;
			esac
		esac
		#
		# If 'cmd' is set, this means we have a proxy command.
		# Add the sub-command to the new argument list.
		#
		if [ ! -z "$cmd" ]; then
			proxy_cmd=1
		fi
		cmd_prev=$cmd
		cmd=
	done
	if [ $proxy_cmd = 1 ]; then
		#
		# Execute the command.
		#
		is_in_list unplumb $*
		if [ $? -eq 0 ]; then
			$IFCONFIG_CMD $*
			return $?
		fi
		is_in_list removeif $*
		if [ $? -eq 0 ]; then
			$IFCONFIG_CMD $*
			return $?
		fi
		#
		# This supports ifconfig <physical_nic> plumb
		#
		is_in_list plumb $*
		if [ $? -eq 0 ] && [ $# -eq 2 ]; then
			$IFCONFIG_CMD $*
			return $?
		fi
		#
		# The ifconfig command is being executed on behalf of
		# the requesting zone.
		#
		$IFCONFIG_CMD $* zone $zone_name
		return $?
	fi
	#
	# If the number of arguments is 1 and this is not a proxy command,
	# the argument specifies the interface. This is equivalent to the
	# list (-a) command supported by the proxy.
	#
	if [ $# -eq 1 ] && [ $zones_cmd -ne 1 ]; then
		is_logical_interface $1
		if [ $? -eq 0 ]; then
			check_logical_interface $1 $zone_name
		else
			check_physical_nic $1 $zone_name
		fi
		if [ $? -ne 0 ]; then
                	printf "$(gettext '%s: Not permitted to access interface %s or interface does not exist.')\n" "${PROG}" $1
			exit 1
		fi
		$IFCONFIG_CMD $1
		exit $?
	fi
	#
	# The proxy ifconfig in the global zone got a command
	# that it is not supposed to handle.
	#
        printf "$(gettext '%s: Not permitted to handle %s %s.')\n" \
	    "${PROG}" "ifconfig" "$*"
	exit 1
}
main $*
