//
//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ifconfig_proxy_client.cc	1.5	08/07/17 SMI"

//
// The ifconfig proxy client is invoked from a virtual cluster node.
// It forwards some ifconfig commands to the ifconfig proxy daemon
// running in the global zone. The ifconfig command is run in the
// global zone and the output is displayed in the virtual cluster
// node.
//
#include <libintl.h>
#include <locale.h>
#include <nslib/ns.h>
#include <h/ifconfig_proxy.h>

#define	IFCONFIG_PROXY_SERVER "ifconfig_proxy_server"
#define	MAX_CMD_LEN 8192

//
// Main routine.
// Return Value:
//	If the ifconfig command is executed successfully the
//	return value is the return value of the ifconfig command.
//	execution.
//	If the ifconfig command execution fails, return 1.
//	return 0 on success.
//
int
main(int argc, char *const argv[])
{
	char	cmd_str[MAX_CMD_LEN];
	int	indx;
	bool	info_cmd = false;

	//
	// Copy the ifconfig command arguments
	//
	(void) strcpy(cmd_str, argv[1]);
	(void) strcat(cmd_str, " ");
	if (argc > 2) {
		for (indx = 2; indx < argc; ++indx) {
			if (indx == (argc - 1)) {
				(void) strcat(cmd_str, argv[indx]);
			} else {
				(void) strcat(cmd_str, argv[indx]);
				(void) strcat(cmd_str, " ");
			}
			if (argv[indx][0] == '-') {
				//
				// We have an argument that is an
				// ifconfig option flag.
				//
				if (strstr(argv[indx], "a") != NULL) {
					// ifconfig -a (list) command.
					info_cmd = true;
				}
			}
		}
	} else {
		//
		// support command type ifconfig <nic>
		//
		info_cmd = true;
	}
	//
	// We should allow only those ifconfig commands that can be
	// executed by any non-root user.
	//
	if ((getuid() != 0) && !info_cmd) {
		(void) fprintf(stderr, "%s\n", gettext("permission denied"));
		exit(1);
	}

	//
	// Initialize ORB.
	//
	if (ORB::initialize() != 0) {
		(void) fprintf(stderr, "%s\n", gettext("Could not initialize "
		    "the ORB. Exiting."));
		exit(1);
	}
	ifconfig_proxy::ifconfig_server_var	ifconfig_server_v;
	CORBA::Object_var			obj_v;
	Environment				env;
	CORBA::Exception			*exp;
	naming::naming_context_var		ctx_v;

	ctx_v = ns::local_nameserver();

	obj_v = ctx_v->resolve(IFCONFIG_PROXY_SERVER, env);

	if ((exp = env.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) == NULL) {
			(void) fprintf(stderr, "%s\n", gettext("Failed to "
			    "lookup the ifconfig proxy service"));
			exit(1);
		}
		// Wait and then try again.
		env.clear();
	}
	//
	// Test to see that the object we got from the name server is active.
	// If not, wait for the child process to register the new
	// object.
	//
	ifconfig_server_v = ifconfig_proxy::ifconfig_server::_narrow(obj_v);

	ASSERT(!CORBA::is_nil(ifconfig_server_v));

	bool alive = ifconfig_server_v->is_alive(env);
	if (env.exception() != NULL || !alive) {
		env.clear();
		(void) fprintf(stderr, "%s\n", gettext("Failed to access the "
		    "ifconfig proxy service"));
		exit(1);
	}

	int	ret_val;
	char	*stdout_str = NULL;
	char	*stderr_str = NULL;
	int	optval;

	//
	// Invoke the ifconfig server to handle the ifconfig command
	// in the global zone.
	//
	optval = ifconfig_server_v->handle_ifconfig_cmd(ret_val, stdout_str,
	    stderr_str, cmd_str, env);

	if ((exp = env.exception()) != NULL) {
		(void) fprintf(stderr, gettext("Exception occured during "
		    "remote execution, %s\n"), exp->_name());
		env.clear();
		exit(1);
	}
	if (optval != 0) {
		if (stderr_str != NULL) {
			(void) fprintf(stderr, "%s", stderr_str);
		} else {
			(void) fprintf(stderr, gettext("Failed to execute "
			    "the proxy ifconfig command retval = %d\n"),
			    optval);
		}
		exit(1);
	}
	if (stdout_str != NULL) {
		(void) fprintf(stdout, "%s", stdout_str);
	}
	if (stderr_str != NULL) {
		(void) fprintf(stderr, "%s", stderr_str);
	}
	return (ret_val);
}
