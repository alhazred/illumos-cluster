#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)ifconfig_client_proxy.ksh	1.7	08/07/17 SMI"
#
# ifconfig_client_proxy.ksh
#
# ifconfig_client_proxy
#
# This script implements the proxy ifconfig command that is executed
# on a virtual cluster node. The following ifconfig commands are
# executed in the global zone due to the lack of privilege in the
# virtual cluster node:
# ---------------------------------------------------------------
# ifconfig -a [inet | inet6]
# This will list the interfaces that has the following properties:
# 1) The NIC has been explicitly authorized for use in the zone cluster.
# 2) Belongs to the same IPMP group as one of the NIC's that is
#    part of net resource of the virtual cluster node.
# 3) Is a logical interface associated with the virtual cluster
#    zone.
# 4) Is clprivnet0
# ---------------------------------------------------------------
# ifconfig -a[4|6|D][u|d] [inet | inet6]
# This will list the interfaces that is a subset of interfaces
# listed in ifconfig -a output.
# -------------------------------------------------------------
# ifconfig <interface>
# This will display the information about the interface if the
# interface falls into one of the four categories specified above
# for the ifconfig -a option.
# ----------------------------------------------------------------
# ifconfig <interface> [addif|removeif] <address/hostname>
# The physical interfaces that are permitted in this command are
# the interfaces that will be listed in the ifconfig -a output of
# the zone. If a hostname is specified, it is resolved and replaced
# by the corresponding IP address from within the zone. The IP
# address should be one of the IP addresses exclusively assigned
# to the virtual cluster node.
#-----------------------------------------------------------------
# ifconfig <logical_interface> [ plumb | unplumb | up | down | set ]
# The logical interfaces that are permitted in this command are
# the interfaces that belong to the virtual cluster zone. If the
# logical interface does not exist, the ifconfig proxy does not
# support creation of that logical interface.
#-----------------------------------------------------------------
# ifconfig <physical_nic> plumb
# The proxy will support command to plumb a physical interface
# if it is not plumbed already. If the nic is already plumbed
# the proxy will just return success.
#-----------------------------------------------------------------
# Any address that is specified in the form of a hostname is
# resolved within the virtual cluster node to an IP address.
# The command is handed over to the ifconfig proxy daemon in
# the global zone. The command is executed in the global
# zone after verifying that the command is permitted to be run
# on behalf of the virtual cluster node. 
#
#program name and args list. Used for log messages.
if [ -z $PROG ]; then
        typeset -r PROG=${0##*/}
fi
IFCONFIG_CMD=/usr/sbin/ifconfig
IFCONFIG_PROXY_CLIENT=/usr/cluster/lib/sc/ifconfig_proxy_client
IFCONFIG_ORG_CMD=/var/cluster/sbin.org/ifconfig
SC_IP_CMD=/usr/cluster/lib/scadmin/lib/sc_ip.pl

#
# The list of keywords that can appear after the plumb
# command instead of a hostname/IP address.
#
typeset -r proxy_keywords="up down zone"
#
# The proxy ifconfig command handles commands with these keywords
# in the local zone.
#
typeset -r restrict_keywords="group private -private nud -nud standby -standby subnet tdst tsrc trailers -trailers userrc xmit -xmit encaplimit -encaplimit thoplmit router -router -zone"
#
# The proxy ifconfig command conditionally handles commands with
# these keywords either in the local zone or global zone based on
# other arguments in the ifconfig command. 
#
typeset -r conditional_keywords="netmask broadcast"
########################################################################
# is_logical_interface nic
# check if the interface is a logical interface
# Return value:
#	0 if the interface is a logical interface
#	1 Otherwise
#
########################################################################
is_logical_interface()
{
	typeset logical_interface

	logical_interface=`echo $1 | awk '{ FS=":"; print $2 }'`
	if [ ! -z "$logical_interface" ]; then
		return 0
	else
		return 1
	fi
}

#######################################################################
# get_ip host_name | ip_address
# Fetch the IP address, given a hostname. If the argument passed is
# an IP address, this results in the same IP address being returned.
# Return Value:
#	0 If a match is found
#	1 Otherwise
# 
#######################################################################
get_ip()
{
	typeset ip_addr

	ip_addr=`$SC_IP_CMD convert_to_ip $1`
	if [ -z $ip_addr ]; then
		return 1
	else
		echo $ip_addr
		return 0
	fi
}
		
#######################################################################
# replace_hostname_by_ip hostname | ip_address
# Fetch the IP address, given a hostname. If the argument passed is
# an IP address, this results in the same IP address being returned.
# Here we take care so that if a prefix_length is specified, it is handled
# properly.
# Return Value:
#	0 If a match is found
#	1 If only prefix_length is specified.
#	2 Failed to resolve address.
# 
######################################################################
replace_hostname_by_ip()
{
	typeset addr
	typeset prefix_len
	typeset ip

	addr=`echo $1 | awk '{ FS="/"; print $1}'`
	prefix_len=`echo $1 | awk '{ FS="/"; print $2}'`
	#
	# This is to support the set command that only sets
	# the prefix_length. For example,
	#	ifconfig bge0:2 plumb set /32
	#
	if [ -z "$addr" ] && [ ! -z "$prefix_len" ]; then
		echo /$prefix_len
		return 1
	fi
	ip=`get_ip $addr`
	if [ $? -ne 0 ]; then
		return 2
	fi
	if [ ! -z "$prefix_len" ]; then
		echo "$ip/$prefix_len"
	else
		echo "$ip"
	fi
}

######################################################################
#
# Main
# Return Value:
#	0 Success
#	1 Failure
#
######################################################################
main()
{
	typeset zone_name
	typeset index
	typeset	ip
	typeset key
	typeset tmp
	typeset newargs=""
	typeset cmd=""
	typeset cmd_prev=""
	integer proxy_cmd=0
	integer zones_cmd=0
	integer found=0

	if [ -f /usr/bin/zonename ]; then
        	zone_name=`/usr/bin/zonename`
	else
		# pre_s10 version
		exit 1
	fi
	if [ "$zone_name" = "global" ]; then
		#
		# It is not allowed to execute the proxy command in the
		# global zone.
		#
		return 1
	fi
	#
	# Walk through each argument of ifconfig. If the argument is
	# a hostname, it is replaced by the corresponding IP address.
	# If the argument is a sub-command that is -a (list),
	# addif (Add a logical interface), removeif
	# (Remove a logical interface), (un)plumb/set or up/down
	# of a logical interface, the command is handed over to the
	# proxy ifconfig server in the global zone. Otherwise, it is
	# executed in the local zone context.
	#
	for index in $*
	do
		#
		# There are a set of commands (specified by the presence
		# of a keyword in the restricted_keywords list) that
		# should be handled by the proxy in the local zone.
		# For example,
		#	ifconfig bge0:2 [ plumb | up ] [[-]private|[-]router ].
		#
		for key in $restrict_keywords
		do
			if [ "$index" = "$key" ]; then 
				found=1
				break
			fi
		done
		if [ $found = 1 ]; then
			proxy_cmd=0
			break
		fi
		case $index in
		-*Z*'a'* |-*'a'*Z*)
			#
			# -Z option should not be handled by the 
			# proxy.
			#
			zones_cmd=1
			;;
		-*'a'*)
			#
			# There are a set of commands (specified by the
			# presence of a keyword in the conditional_keywords
			# list) that should be conditionally handled by the
			# proxy in the local zone. For example,
			# ifconfig -a broadcast +
			# ifconfig -a broadcast + netmask
			#
			for key in $conditional_keywords
			do
				for tmp in $*
				do
					if [ "$tmp" = "$key" ]; then 
						found=1
						break
					fi
				done
			done
			if [ $found = 1 ]; then
				proxy_cmd=0
				break
			fi
			cmd=$index
			;;

		'addif')
			cmd="addif"
			;;

		'removeif')
			cmd="removeif"
			;;

		'plumb'| 'unplumb' | 'up' | 'down' | 'set')
			#
			# If the ifconfig command is to be handled
			# by the proxy ifconfig server, it should
			# be applicable on a logical interface.
			#
			is_logical_interface $1

			if [ $? -eq 0 ]; then
				cmd=$index;
			fi
			#
			# We support plumb of a physical nic.
			# If the nic is not plumbed, we plumb
			# it, otherwise just return success.
			#
			if [ "$index" = "plumb" ] && [ $# -eq 2 ]; then
				cmd=$index
			fi
			#
			# Up/down can appear as flags for other commands
			# as well.  For ex.
			# ifconfig bge0 addif <ip> netmask 0xffffff00
			# broadcast + depreacated up failover
			# We handle it here.
			#
			if [ $proxy_cmd -eq 1 ] && [ "$index" != "set" ]; then
				# This is already a proxy cmd.
				cmd=$index
			fi
                        ;;

		*)
			#
			# Check what the previous argument was.
			# If the previous argument was "addif" or "removeif"
			# or "set", we can safely assume that the current
			# argument is an IP address or hostname. 
			# If the previous command was "plumb", then the
			# current argument must be either an IP
			# address/hostname or another sub-command.
			#
			case $cmd_prev in
			'addif'|'removeif'|'set')
				#
				# We got an IP address or hostname here.
				# if it is hostname, replace it by IP address.
				#
				ip=`replace_hostname_by_ip $index`

				if [ $? -eq 1 ]; then
					#
					# We have got just the prefix_length
					# here and no IP/hostname.
					# Such commands (for ex.
					# ifconfig bge0:2 set /32)
					# need to executed on the local zone.
					#
					proxy_cmd=0
				fi
				if [ $? -eq 2 ]; then
                			printf "$(gettext '%s: failed to resolve %s.')\n" "${PROG}" $index
					exit 1
				fi
				#
				# Build the new argument list by replacing
				# hostname by IP address.
				#
				newargs="$newargs $ip"
				;;

			'plumb')
				#
				# The previous command was 'plumb'.
				# If the current argument is not a keyword,
				# it is safe to assume that it should
				# be an IP address/hostname or one of the
				# keywords listed in $proxy_keywords.
				#
				for key in $proxy_keywords
				do
					if [ "$index" = "$key" ]; then 
						found=1
						break
					fi
				done
				if [ $found != 1 ]; then
					ip=`replace_hostname_by_ip $index`
					if [ $? -eq 1 ]; then
						proxy_cmd=0
					fi
					if [ $? -eq 2 ]; then
                				printf "$(gettext '%s: failed to resolve %s.')\n" "${PROG}" $index
						exit 1
					fi
					newargs="$newargs $ip"
				else
					newargs="$newargs $index"
				fi
				;;
			*)
				#
				# Add the current argument to the new list of
				# arguments without any change.
				#
				newargs="$newargs $index"
			esac
		esac
		#
		# If 'cmd' is set, this means we have a proxy command.
		# Add the sub-command to the new argument list.
		#
		if [ ! -z "$cmd" ]; then
			newargs="$newargs $cmd"
			proxy_cmd=1
		fi
		cmd_prev=$cmd
		cmd=
	done
	#
	# In a virtual cluster, If we find a command in the argument
	# list that needs to be handled by the ifconfig proxy, we hand
	# it over to the proxy here. Otherwise, the hardlink to the
	# ifconfig command is used to locally execute the command.
	# This is also verified in the global zone.
	#
	if [ $proxy_cmd = 1 ]; then
		$IFCONFIG_PROXY_CLIENT $newargs
		exit $?
	fi
	#
	# If the number of arguments is 1 and this is not a proxy command,
	# the argument specifies the interface. This is equivalent to the
	# list (-a) command supported by the proxy.
	#
	if [ $# -eq 1 ] && [ $zones_cmd -ne 1 ]; then
		$IFCONFIG_PROXY_CLIENT $1
		exit $?
	fi
	#
	# Execute the command in the local zone context.
	#
	$IFCONFIG_ORG_CMD $*
}
main $*
