#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)scprivipadm.ksh	1.15	09/04/22 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#####################################################
#
# Global Constants
#
#####################################################

# Exit codes
integer -r IP_NOERR=0
integer -r IP_EOP=1
integer -r IP_EUSAGE=2
integer -r IP_EINVAL=3
integer -r IP_EINTERNAL=4


# The base directory
typeset -r SC_BASEDIR=/                                 # Base directory

# Check for remote method 
typeset -r RSH=/usr/bin/rsh
typeset -r SSH=/usr/bin/ssh
typeset -r SCRCMD=/usr/cluster/lib/sc/scrcmd
typeset -r mynodename=$(uname -n)

# check for global zone
${SC_BASEDIR}/usr/cluster/lib/sc/sc_zonescheck
if [ $? -ne 0 ]; then
	exit ${IP_EOP}
fi

# The CCR infrastructure file.
typeset SC_CONFIG=${SC_BASEDIR}/etc/cluster/ccr/global/infrastructure
typeset SC_CONFIG_NEW=${SC_BASEDIR}/etc/cluster/ccr/global/infrastructure.new
typeset SC_CONFIG_BAK=${SC_BASEDIR}/etc/cluster/ccr/global/infrastructure.bak

# The CCR directory.
typeset SC_CONFIG_DIR=${SC_CONFIG_DIR:-${SC_BASEDIR}/etc/cluster/ccr}

# The nodes in the cluster
typeset SC_NODES=

# The cluster netaddress set to the default
typeset NETADDR=172.16.0.0

# The cluster netaddress in hex format set to the default
typeset NETADDR_HEX=$((0xac100000))

# The cluster netmask, starts at default 
typeset DEFAULT_NETMASK=255.255.248.0
typeset NETMASK=${DEFAULT_NETMASK}

# The cluster netmask which includes the zone cluster subnets, starts at default 
typeset DEFAULT_CLUSTER_NETMASK=255.255.240.0
typeset CLUSTER_NETMASK=${DEFAULT_CLUSTER_NETMASK}

# The cluster netmask in hex format, starts at default 
typeset NETMASK_HEX=$((0xffff0000))
typeset CLUSTER_NETMASK_HEX=$((0xfffff000))

# The cluster subnetmask, again the default value
typeset DEFAULT_SUBNETMASK=255.255.255.128
typeset SUBNETMASK=${DEFAULT_SUBNETMASK}

# The cluster subnetmask in hexadecimal, starts with the default value
typeset SUBNETMASK_HEX=$((0xffffff70))

# The maximum number of nodes that can be configured, starts at default
typeset MAXNODES=64

# The maximum number of privatenets to be configured in the cluster
# Starts at default
typeset MAXPRIVATENETS=10

# The current cluster netaddress, netmask, maxnodes and maxprivatenets
typeset curr_netaddr=
typeset curr_netmask=
typeset curr_maxnodes=
typeset curr_maxprivatenets=
typeset curr_zc_cnt=

integer -r SC_TRUE=1
integer -r SC_FALSE=0
typeset -r SC_DONE=$(gettext 'done')
typeset -r SC_FAILED=$(gettext 'failed')

typeset SC_SCLIBDIR=/usr/cluster/lib/scadmin/lib
typeset SC_LIB_SC=sc_common
typeset SC_LOADED_COMMON=${SC_FALSE}

typeset -r SC_CLINFO=/usr/sbin/clinfo
typeset -r SC_CCRADM=/usr/cluster/lib/sc/ccradm
typeset -r SC_MODIFY_IP=/usr/cluster/lib/sc/modify_ip
typeset -r CKSUM=/usr/bin/cksum
typeset -r LS=/usr/bin/ls
typeset -r CP=/usr/bin/cp
typeset -r TOUCH=/usr/bin/touch
typeset -r TEST=/usr/bin/test
typeset -r MV=/usr/bin/mv
typeset -r RM=/usr/bin/rm

typeset -r THIS_PROG=${0##*/}
typeset -x PROG=${PROG:-${THIS_PROG}}
PATH=/bin:/usr/bin:/sbin:/usr/sbin:${PATH}; export PATH

#########################################
#
#	Main
#
#########################################
main()
{
	typeset netaddr_given=0
	typeset netmask_given=0
	typeset maxnodes_given=0
	typeset maxprivnets_given=0
	typeset reset_cmd

	typeset min_maxcounts_netmask
	integer min_maxcounts_netmask_hex

	integer min_netmask_hex

	integer result

	if [[ "${PROG}" == "${THIS_PROG}" ]]; then
		reset_cmd="scprivipadm -R"
	else
		reset_cmd="the restore-netprops subcommand"
	fi

	# load common functions
        loadlib ${SC_SCLIBDIR}/${SC_LIB_SC} ${SC_LOADED_COMMON} || return ${IP_EINTERNAL}
        let SC_LOADED_COMMON=${SC_TRUE}

	# Verify root user is running this command
	verify_isroot || return ${IP_EOP}

	# Get current configuration information. This is provided to
	# the user if the -p option is used. 
	get_configuration

	# Get list of nodes in the cluster, so we can check them later
	get_nodes

	# Check options, if "c", make sure to check for netmask, or maxnodes
	# and maxprivatenets

	if [[ $# -lt 1 ]]; then
		print_usage
		return ${IP_EUSAGE}
	fi

	while getopts c:Rp c 2>/dev/null
	do
		case ${c} in
		c)
			typeset given_opts; set -A given_opts $(IFS=, ; echo $OPTARG)
			i=0
			while [[ -n "${given_opts[i]}" ]]
			do	
				opt=$(expr "${given_opts[i]}" : '\(.*\)=.*')
				if [[ -z "${opt}" ]]; then
					opt=${given_opts[i]}
				fi
				if [[ ${opt} == "netaddr" ]]; then
					NETADDR=$(echo ${given_opts[i]} | /usr/bin/cut -f 2 -d "=")
					is_ipv4_dot_notation ${NETADDR}
					if [[ $? -ne 0 ]]; then
						printf "$(gettext '\"%s\" is an invalid network address.')\n" "${NETADDR}" | printerr
						exit ${IP_EINVAL}
					fi
					netaddr_given=1
				fi
				if [[ ${opt} == "netmask" ]]; then
					CLUSTER_NETMASK=$(echo ${given_opts[i]} | /usr/bin/cut -f 2 -d "=")
					netmask_given=1
				fi
				if [[ ${opt} == "maxnodes" ]]; then
					MAXNODES=$(echo ${given_opts[i]} | /usr/bin/cut -f 2 -d "=")
					validate_maxnodes ${MAXNODES}
					maxnodes_given=1
				fi
				if [[ ${opt} == "maxprivatenets" ]]; then
					MAXPRIVATENETS=$(echo ${given_opts[i]} | /usr/bin/cut -f 2 -d "=")
					validate_maxprivatenets ${MAXPRIVATENETS}
					maxprivnets_given=1
				fi
				((i += 1))
			done

			if [[ ${netaddr_given} -eq 0 ]]; then
				print_usage
				exit ${IP_EUSAGE}
			fi

			if [[ "${maxnodes_given}" -ne "${maxprivnets_given}" ]]; then
				print_usage
				exit ${IP_EUSAGE}
			fi

			get_remote_method
			if [[ $? -ne 0 ]]; then
				exit ${IP_EINTERNAL}
			fi
			check_nodes
			;;
		R)			
			repair
			return 0
			;;
		p)	
			get_remote_method
			if [[ $? -ne 0 ]]; then
				exit ${IP_EINTERNAL}
			fi
			check_nodes
			print_current_configuration
			return 0
			;;
		?)
			print_usage
			return ${IP_EUSAGE}
			;;
		esac
	done

	if [[ ${maxnodes_given} -eq 1 ]] && [[ ${maxprivnets_given} -eq 1 ]]; then
		# Maxnodes and the Maxprivatenets are given

		# Generate a private subnet mask based only on MAXNODES
		SUBNETMASK=$(sc_netmask_generate_private ${MAXNODES})
		SUBNETMASK_HEX=$(sc_ipv4_dot_to_hex ${SUBNETMASK})

		# Calculate the actual number of supported nodes
		actualnodes=$(((~SUBNETMASK_HEX + 1 ) - 2))

		#
		# Check whether the privatenetmask is given.
		# If not, generate it using the given values
		#
		if [[ ${netmask_given} -eq 0 ]]; then
			#
			# Generate the clusternetmask which should accommodate
			# the given maxnodes,maxprivatenets and current zone clusters
			#
			CLUSTER_NETMASK=$(sc_netmask_generate_private ${MAXNODES} ${MAXPRIVATENETS} ${curr_zc_cnt})
		fi

		CLUSTER_NETMASK_HEX=$(sc_ipv4_dot_to_hex ${CLUSTER_NETMASK})

		# Calculate the total number of supported privnets
		actualprivnets=$((((3 * (~CLUSTER_NETMASK_HEX + 1)) / (4 * (~SUBNETMASK_HEX + 1))) - 2))

		#
		# The above subnet count includes the subnets needed for
		# zone clusters. Get the actual nets needed for the physical
		# cluster
		#
		actualprivnets=$((actualprivnets - curr_zc_cnt));

		#
		# Calculate the cluster private subnet mask
		#
		NETMASK=$(sc_netmask_generate_private ${actualnodes} ${actualprivnets})
		NETMASK_HEX=$(sc_ipv4_dot_to_hex ${NETMASK})

		if [[ ${actualprivnets} -lt ${MAXPRIVATENETS} ]]; then
			printf "$(gettext 'Netmask \"%s\" cannot accommodate \"%s\" nodes and \"%s\" private networks.')\n" "${CLUSTER_NETMASK}" "${MAXNODES}" "${MAXPRIVATENETS}" | printerr
			return ${IP_EINVAL}
		fi

	else
		#
		# The maxnodes and the maxprivatenets values are not given
		# check whether the netmask is given

		# If netmask not given, use default
		if [[ ${netmask_given} -eq 0 ]]; then
			CLUSTER_NETMASK=${DEFAULT_CLUSTER_NETMASK}
			CLUSTER_NETMASK_HEX=$(sc_ipv4_dot_to_hex ${CLUSTER_NETMASK})

		fi

		# Use default subnetmask
		SUBNETMASK=${DEFAULT_SUBNETMASK}

		#
		# Use defaults for maxnodes and maxprivatenets.
		# These are definitely not given, if we get here.
		#
		actualnodes=${MAXNODES}
		actualprivnets=${MAXPRIVATENETS}
		
		#
		# If the netmask is not the default means it's the
		# user given input, validate it.
		# User can give netmask,maxnodes and maxprivatenets
		# or just the netmask.
		# If only the netmask option is given,
		# the netmask must be >= default. Check for this.
		#
		if [[ "${CLUSTER_NETMASK}" != "${DEFAULT_CLUSTER_NETMASK}" ]]; then
			# User has given the netmask value
			CLUSTER_NETMASK_HEX=$(sc_ipv4_dot_to_hex ${CLUSTER_NETMASK})

			# Calculate the actual number of supported privnets
                        actualprivnets=$((((3 * (~CLUSTER_NETMASK_HEX + 1)) / (4 * (~SUBNETMASK_HEX + 1))) - 2))

			#
			# The above subnet count includes the subnets needed for
			# zone clusters. Get the actual nets needed for the physical
			# cluster
			#
			actualprivnets=$((actualprivnets - curr_zc_cnt));
		fi

		#
		# Calculate the cluster private subnet mask
		#
		NETMASK=$(sc_netmask_generate_private ${MAXNODES} ${actualprivnets})
		NETMASK_HEX=$(sc_ipv4_dot_to_hex ${NETMASK})

	fi

	# Check actual node and private net count
	if [[ ${actualnodes} -gt ${SC_PNET_MAX_MAXNODES} ]]; then
		actualnodes=${SC_PNET_MAX_MAXNODES}
	fi
	if [[ ${actualprivnets} -gt ${SC_PNET_MAX_MAXPRIVATENETS} ]]; then
		actualprivnets=${SC_PNET_MAX_MAXPRIVATENETS}
	fi

	#
	# Make sure that the NETADDR and NETMASK are good
	#
	NETADDR_HEX=$(sc_ipv4_dot_to_hex ${NETADDR})
	CLUSTER_NETMASK_HEX=$(sc_ipv4_dot_to_hex ${CLUSTER_NETMASK})

	# Netmask should have no holes and cover our network address
	min_netmask_hex=$(sc_netmask_min ${NETADDR_HEX})
	sc_netmask_check ${CLUSTER_NETMASK_HEX}
	let result=$?
	if [[ ${CLUSTER_NETMASK_HEX} -lt ${min_netmask_hex} ]] ||
	    [[ ${result} -ne 0 ]]; then
		printf "$(gettext '%s is an invalid netmask for network adddress %s.')\n" "${CLUSTER_NETMASK}" "${NETADDR}" | printerr
		printf "$(gettext 'Choose a netmask which masks at least all of this private network address.')\n" | printerr
	fi

	# Make sure that the netmask is not too restrictive for our counts
	min_maxcounts_netmask=$(sc_netmask_generate_private ${MAXNODES} ${MAXPRIVATENETS} ${curr_zc_cnt})
	min_maxcounts_netmask_hex=$(sc_ipv4_dot_to_hex ${min_maxcounts_netmask})
	if [[ ${CLUSTER_NETMASK_HEX} -gt ${min_maxcounts_netmask_hex} ]]; then
		printf "$(gettext 'This netmask (%s) cannot accommodate the desired configuration.')\n" "${CLUSTER_NETMASK}" | printerr
		printf "$(gettext 'Choose a netmask which masks no more than %s.')\n\n\a" "${min_maxcounts_netmask}" | printerr
	fi

	# Proceed to modify_ccr
	modify_ccr ${NETADDR} ${CLUSTER_NETMASK} ${NETMASK} ${SUBNETMASK} ${actualnodes} ${actualprivnets}
	return 0
}

###################################################
#
# printerr()
#
#	This is to add msgid's to error messages
#	so it looks like it is coming from one of the
#	cl* commands.  This should be removed once
#	this file gets it messages from the properties
#	file instead of the message catalogs
#
#	The function generates a 6 digit msgid from the
#	text from stdin and prints the message to stderr.
#
###################################################
printerr()
{
        # Save the message
        typeset msg=$(cat)

        if [[ -z ${msg} ]]; then
                return
        fi

        if [[ "${PROG}" == "${THIS_PROG}" ]]; then
                echo ${msg} | sed "s/^/${PROG}:  /" >&2
                return
        fi

        # Create the message ID
        msgid=$(echo ${msg} | msgid | awk '{print $1}')
        msgid=$(printf "C%6.6d" ${msgid})

        # Print the message
        echo ${msg} | sed "s/^/${PROG}:  \(${msgid}\) /" >&2
}

#######################################################
#
# get_nodes()
#
#	Print a list of the nodes in the cluster.
#
#######################################################
get_nodes()
{
	SC_NODES="$(nawk 'BEGIN{FS=" "} { if ($0 ~ /cluster.nodes.[0-9]*.name/ ) print $2}' < ${SC_CONFIG})"
	NODE_IDS=`grep '^cluster.nodes.[0-9]*.name	' ${SC_CONFIG} \
	    2>/dev/null | nawk 'BEGIN { FS="." } {print $3}'`
}

#############################################################
#
# get_configuration()
#
#	Sets variables that define the current configuration.
#
#############################################################
get_configuration()
{
	curr_netaddr=$(cat /etc/cluster/ccr/global/infrastructure | grep "private_net_number" | awk '{print $2}')
	curr_netmask=$(cat /etc/cluster/ccr/global/infrastructure | grep "cluster_netmask" | awk '{print $2}')
	curr_maxnodes=$(cat /etc/cluster/ccr/global/infrastructure | grep "maxnodes" | awk '{print $2}')
	if [[ $curr_maxnodes = "" ]]; then
		curr_maxnodes=64
	fi
	curr_maxprivatenets=$(cat /etc/cluster/ccr/global/infrastructure | grep "maxprivnets" | awk '{print $2}')
	if [[ $curr_maxprivatenets = "" ]]; then
		curr_maxprivatenets=10
	fi
	curr_zc_cnt=$(sc_print_ccr_zc_count)
	if [[ $curr_zc_cnt = "" ]]; then
		curr_zc_cnt=12
	fi
}

#######################################################
#
# print_usage()
#
#	Prints the usage message.
#
#######################################################
print_usage()
{
	echo "$(gettext 'Usage:')"
	echo "$(gettext '\t scprivipadm -c netaddr=<netaddr>,[maxnodes=<maxnodes>,maxprivatenets=<maxprivatenets>]')"
	echo "$(gettext '\t scprivipadm -c netaddr=<netaddr>,[netmask=<netmask>,maxnodes=<maxnodes>,maxprivatenets=<maxprivatenets>]')"
	echo "$(gettext '\t scprivipadm -c netaddr=<netaddr>,[netmask=<netmask>]')"
	echo "$(gettext '\t scprivipadm -R')"
	echo "$(gettext '\t scprivipadm -p') "
}

#######################################################
#
# check_nodes()
#
#	Checks to make sure each node in the cluster is reachable and
#	in non-cluster mode, and then for CCR consistency among nodes
#	Also check for the existance of required binaries. If they
#	don't exist, most likely an upgrade is in progress. So, fail.
#
#######################################################
check_nodes()
{
	# Node in cluster mode
	typeset badnode=
	typeset result=

	if [[ -n "${SC_NODES}" ]]; then
		# Check if in non-cluster mode and check to see if
		# infrastructure file exists. Also collect the
		# checksum for the infrastructure file for all
		# nodes.
		i=0
                for node in ${SC_NODES}
                do
			result=$(run_remote_method "${node}" "${SC_CLINFO} >/dev/null 2>&1;" "true")
			if [[ ${result} -eq 0 ]]; then
				badnode=${node}
				printf "$(gettext 'The  node \"%s\" must be in non-cluster mode.')\n" "${node}" | printerr
				exit ${IP_EOP}
			fi
			result=$(run_remote_method "${node}" "${LS} ${SC_MODIFY_IP}" "false")
			if [[ -z $result ]]; then
				badnode=${node}
				printf "$(gettext 'Cannot find required binary on node \"%s\".')\n" "${badnode}" | printerr
				exit ${IP_EINTERNAL}
			fi
			result=$(run_remote_method "${node}" "${LS} ${SC_CONFIG}" "false")
			if [[ -z $result ]]; then
				badnode=${node}
				printf "$(gettext 'Cannot find cluster configuration on node \"%s\".')\n" "${badnode}" | printerr
				exit ${IP_EINTERNAL}
			fi

			nodeccr[i]=$(run_remote_method "${node}" "${CKSUM} ${SC_CONFIG}" "false")
			((i += 1))
		done

		# Check for consistency of infrastructure file.
		i=0
		if [[ -n ${nodeccr[i]} ]]; then
			j=0
			while [[ -n ${nodeccr[j]} ]]
			do
				if [[ "${nodeccr[i]}" != "${nodeccr[j]}" ]]; then
					printf "$(gettext 'The cluster configuration is not consistent among nodes.')\n" | printerr
					exit ${IP_EINVAL}
				fi
				((j += 1))
			done
		fi
        fi
	return 0
}

#####################################################
#
# loadlib() libname [flag]
#
#       If the "flag" is not set to SC_TRUE, load the
#       named include file.
#
#       Return:
#               zero            Success
#               non-zero        Failure
#
#####################################################
loadlib()
{
        typeset libname=$1
        if [[ $# == 2 ]]; then
                integer flag=$2

                # Check flag
                if [[ flag == SC_TRUE ]]; then
                        return 0
                fi
        fi

        # Load the library
        . ${libname}
        if [[ $? != 0 ]]; then
		printf "$(gettext 'Cannot load \"%s\".')\n" "${libname}" | printerr
                return ${IP_EINTERNAL}
        fi
}

#######################################################
#
# modify_ccr() netaddress cluster_netmask netmask subnetmask
#
#	For each node, modify the /etc/ccr/global/infrastructure file	
#	to contain the new netmask and subnetmask.
#
#######################################################
modify_ccr()
{
	# NOTE - this implementation may need to be changed to use rpc.scadmd
	# via a new scrconf call, instead of rsh, or ssh

	NETADDR=$1
	CLUSTER_NETMASK=$2
	NETMASK=$3
	SUBNETMASK=$4
	actualnodes=$5
	actualprivnets=$6

	if [[ -n "${SC_NODES}" ]]; then
                for node in ${SC_NODES}
                do
			# modify_ip is called on each node. It takes as
			# input the new network address, the cluster netmask,
			# subnet netmask and actualnodes and actualprivnets
			# It creates ${SC_CONFIG_NEW}
			result=$(run_remote_method "${node}" "${SC_MODIFY_IP} $NETADDR $CLUSTER_NETMASK $NETMASK $SUBNETMASK $actualnodes $actualprivnets >/dev/null 2>&1;" "true")
			if [[ -z "$result" ]] || [[ "$result" -ne 0 ]]; then
				printf "$(gettext 'Failed to change private network setting on node \"%s\".')\n" "${node}" | printerr
				printf "$(gettext 'Run %s on each node before you try again.')\n" "${reset_cmd}" | printerr
				exit ${IP_EINTERNAL}
			fi 

			# ccradm on ${SC_CONFIG_NEW}. This forces the
			# $(SC_CONFIG_NEW} file to be updated with the 
			# corresponding change to the checksum and the
			# generation number.
			result=$(run_remote_method "${node}" "${SC_CCRADM} -i ${SC_CONFIG_NEW} -o >/dev/null 2>&1;" "true")
			if [[ -z "$result" ]] || [[ "$result" -ne 0 ]]; then
				printf "$(gettext 'Failed to change private network setting on node \"%s\".')\n" "${node}" | printerr
				printf "$(gettext 'Run %s on each node before you try again.')\n" "${reset_cmd}" | printerr
				exit ${IP_EINTERNAL}
			fi

			# Make a copy of the current ${SC_CONFIG}
			result=$(run_remote_method "${node}" "${CP} ${SC_CONFIG} ${SC_CONFIG_BAK} >/dev/null 2>&1;" "true")
			if [[ -z "$result" ]] || [[ "$result" -ne 0 ]]; then
				printf "$(gettext 'Failed to change private network on node \"%s\".')\n" "${node}" | printerr
				printf "$(gettext 'Run %s on each node before you try again.')\n" "${reset_cmd}" | printerr
				exit ${IP_EINTERNAL}
			fi

			# Checkpoint to allow a repair mode in case of
			# node failures or failure to rsh to cluster nodes.
			result=$(run_remote_method "${node}" "${TOUCH} ${SC_CONFIG_DIR}/checkpoint > /dev/null 2>&1;" "true")
			if [[ $result -ne 0 ]]; then
				exit ${IP_EINTERNAL}
			fi
                done

		# At this stage we should have the current config in 
		# ${SC_CONFIG} and ${SC_CONFIG_BAK} and new config 
		# in ${SC_CONFIG_NEW} and the checkpoint file

		# Make sure each node completed to checkpoint
                for node in ${SC_NODES}
                do
			result=$(run_remote_method "${node}" "${TEST} -f ${SC_CONFIG_DIR}/checkpoint >/dev/null 2>&1;" "true")
			if [[ "${result}" != "0" ]]; then
				printf "$(gettext 'Cannot find the checkpoint file on node \"%s\".')\n" "${node}" | printerr
				printf "$(gettext 'Run %s on each node before you try again.')\n" "${reset_cmd}" | printerr
				exit ${IP_EINTERNAL}
			fi

                done
	
		# Here, all nodes completed successfully, so finish
		# by moving the new file to ${SC_CONFIG}.
                for node in ${SC_NODES}
                do
			result=$(run_remote_method "${node}" "${MV} ${SC_CONFIG_NEW} ${SC_CONFIG} > /dev/null 2>&1;" "true")
			if [[ -z "$result" ]] || [[ "$result" -ne 0 ]]; then
				printf "$(gettext 'Cannot update configuration file on node \"%s\".')\n" "${node}" | printerr
				printf "$(gettext 'Run %s on each node before you try again.')\n" "${reset_cmd}" | printerr
				exit ${IP_EINTERNAL}
			fi
                done

		# Here the new configuration is in ${SC_CONFIG} and 
		# old configuration is in ${SC_CONFIG_BAK}.
		# Now we can remove the checkpoint files
                for node in ${SC_NODES}
                do
			result=$(run_remote_method "${node}" "${RM} ${SC_CONFIG_DIR}/checkpoint > /dev/null 2>&1;" "true")
			if [[ $result -ne 0 ]]; then
				exit ${IP_EINTERNAL}
			fi

			# Give verbose messages
			if [[ -n "${CL_VBOSE}" ]]; then
				printf "$(gettext  'The private network is changed on node \"%s\".')\n" ${node}
			fi
		done
        fi

}

	 
#######################################################
#
# validate_maxnodes() maxnodes 
#
#	Validates the given input for the maximum number of nodes.
#
#	This function may exit with an error;  otherwise, it
#	always returns zero.
#
#######################################################
validate_maxnodes()
{
	maxnodes=$1

	integer a
	integer current_node_count

	if [[ "${maxnodes}" != [0-9]* ]]; then
		if [[ -z "${maxnodes}" ]]; then
			if [[ "${PROG}" == "${THIS_PROG}" ]]; then
				printf "$(gettext 'The value of maxnodes must be specified with a number.')\n" | printerr
			else
				printf "$(gettext 'The value of \"max_node\" must be specified with a number.')\n" | printerr
			fi
		else
			if [[ "${PROG}" == "${THIS_PROG}" ]]; then
				printf "$(gettext 'The value of maxnodes \"%s\" is not a number.')\n" "${maxnodes}" | printerr
			else
				printf "$(gettext 'The value of max_nodes \"%s\" is not a number.')\n" "${maxnodes}" | printerr
			fi
		fi
		exit ${IP_EINVAL}
	fi
	let a=${maxnodes}

	if [[ "$a" -lt "$SC_PNET_MIN_MAXNODES" ]] || \
	    [[ "$a" -gt "$SC_PNET_MAX_MAXNODES" ]]; then
		printf "$(gettext 'Maximum nodes \"%s\" is not within \"%s\" and \"%s\".')\n" "${maxnodes}" "${SC_PNET_MIN_MAXNODES}" "${SC_PNET_MAX_MAXNODES}" | printerr
		exit ${IP_EINVAL}
	fi

	# Check against the current node count
	current_node_count=$(sc_print_ccr_node_count)
	if [[ ${a} -lt ${current_node_count} ]]; then
		printf "$(gettext 'This cluster already contains %d nodes.')\n\n\a" "${current_node_count}"
		exit ${IP_EINVAL}
	fi

	# Done
	return 0
}

#######################################################
#
# validate_maxprivatenets() maxprivatenets 
#
#	Validates the given input for the maximum number of privatenets.
#
#	This function may exit with an error;  otherwise, it
#	always returns zero.
#
#######################################################
validate_maxprivatenets()
{
	maxprivatenets=${1}

	integer current_node_count

	if [[ "${maxprivatenets}" != [0-9]* ]]; then
		if [[ "${PROG}" == "${THIS_PROG}" ]]; then
			printf "$(gettext 'The value of maxprivatenets \"%s\" is not a number.')\n" "${maxprivatenets}" | printerr
		else
			printf "$(gettext 'The value of max_privatenets \"%s\" is not a number.')\n" "${maxprivatenets}" | printerr
		fi
	fi

	if [[ -z "${maxprivatenets}" ]]; then
		if [[ "${PROG}" == "${THIS_PROG}" ]]; then
			printf "$(gettext 'The value of maxprivatenets must be specified with a number.')\n" | printerr
		else
			printf "$(gettext 'The value of \"max_privatenets\" must be specified with a number.')\n" | printerr
		fi
		exit ${IP_EINVAL}
	fi

	let a=${maxprivatenets}

	if [[ "$a" -lt "$SC_PNET_MIN_MAXPRIVATENETS" ]] || \
	    [[ "$a" -gt "$SC_PNET_MAX_MAXPRIVATENETS" ]]; then
		printf "$(gettext 'Maximum private networks \"%s\" is not within \"%s\" and \"%s\".')\n" "${a}" "${SC_PNET_MIN_MAXPRIVATENETS}" "${SC_PNET_MAX_MAXPRIVATENETS}" | printerr
		exit ${IP_EINVAL}
	fi

	# Check against the private network count
	current_privnet_count=$(sc_print_ccr_privnet_count)
	if [[ ${a} -lt ${current_privnet_count} ]]; then
		printf "$(gettext 'This cluster already contains %d private networks.')\n\n\a" "${current_privnet_count}"
		exit ${IP_EINVAL}
	fi

	# Done
	return 0
}

#######################################################
#
# repair()
#
#	This function Restores the old infrastructure file.
#	This is used if modification of ccr does not succeed on all nodes.
#
#######################################################
repair()
{
	integer repaired=0
	/usr/sbin/clinfo > /dev/null 2>&1 
	if [[ $? -eq 0 ]]; then
		printf "$(gettext 'You must run this command in noncluster mode.')\n" | printerr
		exit ${IP_EOP}
	fi

	ls ${SC_CONFIG_DIR}/checkpoint > /dev/null 2>&1
	if [[ $? -eq 0 ]];then
		rm -f ${SC_CONFIG_NEW} 2>/dev/null
		if [[ -f ${SC_CONFIG_BAK} ]]; then
			mv ${SC_CONFIG_BAK} {SC_CONFIG} 2>/dev/null
			let repaired=1
		fi

		# Remove checkpoint
		rm -f ${SC_CONFIG_DIR}/checkpoint 2>/dev/null
	fi

	if [[ -n "${CL_VBOSE}" ]]; then
		if [[ ${repaired} -eq 1 ]]; then
			printf "$(gettext  'The private network is restored on this node.')\n"
		else
			printf "$(gettext  'The private network is already restored on this node.')\n"
		fi
	fi

	exit 0
}

################################################################
#
# print_current_configuration()
#
#	Displays current configuration information to the user.
#
################################################################
print_current_configuration()
{
	printf "  $(gettext 'Private network address:    %s')\n" "${curr_netaddr}"
	printf "  $(gettext 'Private netmask:            %s')\n" "${curr_netmask}"
	printf "  $(gettext 'Maximum nodes:              %s')\n" "${curr_maxnodes}"
	printf "  $(gettext 'Maximum private networks:   %s')\n" "${curr_maxprivatenets}"

}

###############################################################################
#
# get_remote_method
#
#	Determine the best remote method for executing remote methods.
#	All nodes must use the same method.
#
#	The order of prefence is ssh, then rsh.
#
#	Upon success, this function sets SC_REMOTE_METHOD to "ssh" or "rsh".
#
#	Return values:
#		ZERO		- SUCCESS
#		NON-ZERO	- FAILURE
#
###################################################################################
get_remote_method()
{
	integer nodecount=0
	integer rsh_count=0
	integer ssh_count=0
	integer scrcmd_count=0

	typeset node
	typeset cmd
	typeset result
	typeset flag

	# Reset remote method
	SC_REMOTE_METHOD=

	# Get the number of nodes.
	nodecount=$(sc_print_ccr_node_count)

	#
	# If this is a single-node cluster, we are done
	#
	if [[ $nodecount == 1 ]]; then
		return 0
	fi

	# Decrement the nodecount by 1.
	((nodecount -= 1))

	# Look at each node
	for node in ${SC_NODES}
	do
		if [[ ${node} == ${mynodename} ]]; then
			continue
		fi
		printf "$(gettext 'Attempting to contact node \"%s\" ...')" "${node}"

		# Try scrcmd
		scrcmd_result=1
		if [[ -x ${SCRCMD} ]]; then 
			cmd="${SCRCMD} -N ${node} test isfullyinstalled"
			scrcmd_result=$(eval ${cmd} 2>/dev/null) 
			ret=$?

			if [[ -z "${scrcmd_result}" ]]; then
				scrcmd_result=107
			fi
			if [[ ${ret} == "1" ]]; then
				((scrcmd_count += 1))
			fi
		fi
	
		# Try rsh
		rsh_result=1
		if [[ -x ${RSH} ]]; then
			cmd="${RSH} ${node} -n \"/bin/sh -c '/bin/true;  /bin/echo SC_COMMAND_STATUS=\\\$?'\""
			rsh_result=$(eval ${cmd} 2>/dev/null | sed -n 's/^SC_COMMAND_STATUS=\(.*\)/\1/p')
			if [[ -z "${rsh_result}" ]]; then
				rsh_result=107
			fi
			if [[ ${rsh_result} == "0" ]]; then
				((rsh_count += 1))
			fi
		fi

		# Try ssh
		ssh_result=1
		if [[ -x ${SSH} ]]; then
			cmd="${SSH} root@${node} -o \"BatchMode yes\" -o \"StrictHostKeyChecking yes\" -n \"/bin/sh -c '/bin/true;  /bin/echo SC_COMMAND_STATUS=\\\$?'\""
			ssh_result=$(eval ${cmd} 2>/dev/null | sed -n 's/^SC_COMMAND_STATUS=\(.*\)/\1/p')
			if [[ -z "${ssh_result}" ]]; then
				ssh_result=107
			fi
			if [[ ${ssh_result} == "0" ]]; then
				((ssh_count += 1))
			fi
		fi

		if [[ ${rsh_result} == "0" ]] || [[ ${ssh_result} == "0" ]] || [[ ${scrcmd_count} -ge 1 ]]; then
			printf "${SC_DONE}\n"
		else
			printf "${SC_FAILED}\n"
		fi

	done

	# Try scrcmd
	if [[ ${scrcmd_count} -eq ${nodecount} ]]; then
		SC_REMOTE_METHOD=scrcmd
	# Try ssh 
	elif [[ ${ssh_count} -eq ${nodecount} ]]; then
		SC_REMOTE_METHOD=ssh
	# Try rsh next
	elif [[ ${rsh_count} -eq ${nodecount} ]]; then
		SC_REMOTE_METHOD=rsh
	else
		printf "$(gettext 'Remote access attempts to one or more of the other nodes failed.')\n" | printerr
		return -1
	fi

	# Return Success.
	return 0
}

##########################################################################################
#
# run_remote_method()
#
#	Executes the given command on the specified node.
#	If the node specified is local node , the command is executed locally,
#	else the command is executed on the remote node using remote methods.
#	The remote method could be either ssh or rsh.
#
#	Input values:
#		node  		Node where the command gets executed.
#		command  	command which to be executed.
#		is_ret_value 	Specifies whether the remote command should return command
#				status or not.
#
#	Returns the return value of the remote command executed. 
#
############################################################################################## 
run_remote_method()
{
	typeset -r node="${1}"
	typeset -r tmp_cmd="${2}"
	typeset -r is_ret_val="${3}"
	typeset cmd=
	typeset command=
	typeset result=

	# Append the return status to the remote command
	if [[ ${is_ret_val} == "true" ]]; then
		cmd="${tmp_cmd}echo \\\$?"
	else 
		cmd="${tmp_cmd}"
	fi

	if [[ ${node} == ${mynodename} ]]; then
		command="/bin/sh -c \"${cmd}\""
		result="$(eval ${command})"

	else
		case ${SC_REMOTE_METHOD} in
		scrcmd)
			keyword=$(validate_command ${cmd})
			ret=$?
			
			if [[ ! -z ${keyword} ]] && [[ ${ret} != ${IP_EINTERNAL} ]] && [[ ${keyword} = "runmodifyip" ]]; then
				result="$(eval ${SCRCMD} -N \"${node}\" modifyip "\"${cmd}\"")"
			elif [[ ! -z ${keyword} ]] && [[ ${ret} != ${IP_EINTERNAL} ]]; then
				result="$(eval ${SCRCMD} -N \"${node}\" scprivipadm "\"${keyword}\"")"
				ret=$?
				if [[ ${is_ret_val} == "true" ]]; then
					echo $ret
				fi
			fi
			;;

		ssh)
			result="$(eval ${SSH} root@${node} -o \"BatchMode yes\" -o \"StrictHostKeyChecking yes\" -n \"/bin/sh -c \'${cmd}\'\")"
			;;
		rsh)
			result="$(eval ${RSH} ${node} -n \"/bin/sh -c \'${cmd}\'\")"
			;;
		*)
			;;
		esac
	fi
	
	echo "${result}"

	# Return Success.
	return 0
}

#
# Compare the command with the list of allowed ones.
# If it matches then allow execution by returning a 0 
# Else return 1
#

validate_command() {
	typeset -r cmd=$1

	ret=$(eval echo ${cmd} | grep -w "${SC_CLINFO}")
	if [[ ! -z ${ret} ]]; then
		echo "isnoncluster"
		return 0
	fi

	ret=$(eval echo ${cmd} | grep -w "${LS}")
	if [[ ! -z ${ret} ]]; then
		cmdtemp=$*

		ret=$(eval echo ${cmdtemp} | grep -w "${LS} ${SC_CONFIG}")
		if [[ ! -z ${ret} ]]; then
			echo "isconfigpresent"
			return 0
		fi

		ret=$(eval echo ${cmdtemp} | grep -w "${LS} ${SC_MODIFY_IP}")
		if [[ ! -z ${ret} ]]; then
			echo "ismodifyippresent"
			return 0
		fi
	fi

	ret=$(eval echo ${cmd} | grep -w "${CKSUM}")
	if [[ ! -z ${ret} ]]; then
		echo "getchksum"
		return 0
	fi

	ret=$(eval echo ${cmd} | grep -w "${SC_MODIFY_IP}")
	if [[ ! -z ${ret} ]]; then
		echo "runmodifyip"
		return 0
	fi

	ret=$(eval echo ${cmd} | grep -w "${SC_CCRADM}")
	if [[ ! -z ${ret} ]]; then
		echo "runccradm"
		return 0
	fi

	ret=$(eval echo ${cmd} | grep -w "${TOUCH}")
	if [[ ! -z ${ret} ]]; then
		echo "runtouchkpt"
		return 0
	fi

	ret=$(eval echo ${cmd} | grep -w "${TEST}")
	if [[ ! -z ${ret} ]]; then
		echo "runtestchkpt"
		return 0
	fi

	ret=$(eval echo ${cmd} | grep -w "${MV}")
	if [[ ! -z ${ret} ]]; then
		echo "runmvconfig"
		return 0
	fi

	ret=$(eval echo ${cmd} | grep -w "${RM}")
	if [[ ! -z ${ret} ]]; then
		echo "runrmchkpt"
		return 0
	fi

	ret=$(eval echo ${cmd} | grep -w "${CP}")
	if [[ ! -z ${ret} ]]; then
		echo "runcpconfig"
		return 0
	fi

	return ${IP_EINTERNAL}
}

main $*

