/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)node_db.c	1.3	08/05/20 SMI"


#include <stdlib.h>
#include <strings.h>

#include <list.h>
#include <libscdpm.h>

#include <node_db_types.h>


int
scdpm_node_stat_db_create(arr_stat_elem_t **arrp)
{
	size_t arr_size;
	arr_stat_elem_t *arr;

	arr_size = NODEID_MAX * sizeof (arr_stat_elem_t);
	arr = (arr_stat_elem_t *)malloc(arr_size);

	(*arrp) = arr;

	if (arr == NULL) {
		return (DPM_ENOMEM);
	}

	bzero(arr, arr_size);
	return (0);
}

int
scdpm_node_stat_db_update(arr_stat_elem_t *arr, uint_t nodeid, char *diskpath)
{
	int error;

	if (strcmp(diskpath, "alls0") == 0) {
		error = scdpm_prepend_dsn(&(arr[nodeid-1].head),
				    &(arr[nodeid-1].tail), diskpath);
	} else {
		error = scdpm_append_dsn(&(arr[nodeid-1].head),
				    &(arr[nodeid-1].tail), diskpath);
	}

	if (error) {
		return (error);
	}

	(arr[nodeid-1].count)++;
	return (0);
}

void
scdpm_node_stat_db_empty(arr_stat_elem_t *arr)
{
	int i;

	for (i = 0; i < NODEID_MAX; i++) {
		scdpm_destroy_dsn_list_framework(arr[i].head);
		arr[i].head = NULL;
		arr[i].tail = NULL;
		arr[i].count = 0;
	}
}

void
scdpm_node_stat_db_destroy(arr_stat_elem_t *arr)
{
	scdpm_node_stat_db_empty(arr);
	free(arr);
}
