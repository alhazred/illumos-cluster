/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident   "@(#)infile.c 1.6     08/05/20 SMI"


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <locale.h>
#include <libintl.h>

#include <clmsg_int.h>
#include <errno.h>

#include <libscdpm.h>
#include <scdpm.h>
#include <monitor.h>

static int send_disk(int, char, char *);

int scdpm_read_file(const char *);

/*
 * Returns DPM_FAILURE if file input related failure is encountered.
 * Returns specific error if a fatal error such as DPM_CCR_ERROR
 * or DPM_ENOMEM is encountered.
 */
int
scdpm_read_file(const char *fname)
{
	FILE *fd;
	char buffer[BUFSIZ];
	char path[_POSIX_PATH_MAX];
	char cmd;
	int err, admin_err, action_err;
	int nb, flag, line;

	fd = fopen(fname, "r");
	if (fd == NULL) {
		clerror("Error opening file \"%s\" - %s.\n", fname,
		    strerror(errno));
		return (DPM_FAILURE);
	}

	flag = line = err = 0;
	admin_err = action_err = DPM_SUCCESS;
	while (fgets(buffer, BUFSIZ, fd) != NULL) {
		line++;
		buffer[strlen(buffer)-1] = (char)0;
		if (strlen(buffer) == 0)
			continue;
		if (buffer[0] == '#')
			continue;
		nb = sscanf(buffer, "%c%s", &cmd, path);
		if (nb != 2) {
			clerror("line %d: syntax error.\n",
			    line);
			err++;
			continue;
		}
		admin_err = send_disk(line, cmd, path);
		if (admin_err == DPM_SUCCESS) {
			flag++;
		} else {
			err++;
			if (scdpm_fatal_error(admin_err)) {
				(void) fclose(fd);
				return (admin_err);
			}
		}
	}

	if (flag) {
		action_err = scdpm_monitor_unmonitor_action();
	}

	if (action_err != DPM_SUCCESS) {
		err++;
		if (scdpm_fatal_error(action_err)) {
			(void) fclose(fd);
			return (action_err);
		}
	}

	(void) fclose(fd);
	return ((err > 0) ? DPM_FAILURE : 0);
}

int
send_disk(int line, char cmd, char *path)
{
	int err;

	err = 0;

	switch (cmd) {
	default:
		clerror("line %d: command \"%c\" is "
		    "not recognized.\n", line, cmd);
		return (DPM_FAILURE);
	case 'm':
	case 'M':
		err = scdpm_monitor_admin(path);
		break;
	case 'u':
	case 'U':
		err = scdpm_unmonitor_admin(path);
		break;
	}

	/*
	 * print the error message
	 */
	if (err != DPM_SUCCESS) {
		clerror("line %d: ", line);
		scdpm_print_error(err, path);
	}

	return (err);
}
