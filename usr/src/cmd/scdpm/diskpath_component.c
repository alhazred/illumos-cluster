/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)diskpath_component.c	1.10	08/05/20 SMI"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libintl.h>	/* gettext */
#include <sys/clconf.h>
#include <clmsg_int.h>
#include <devid.h>
#include <sys/didio.h>
#include <libdid.h>
#include <sys/ddi_impldefs.h>
#include <node_db_types.h>
#include <diskpath_component_types.h>
#include <list.h>
#include <scdpm.h>

extern int scdpm_track_heap(char *string_allocated);

static did_device_list_t *all_disks_p = NULL;

/*
 * This function returns a list of all nodeids present in the cluster.
 */
static int
get_all_nodeids(nodeid_elem_t **headp)
{
	const char 		*nodename;
	clconf_cluster_t	*cl;
	uint_t 			i;
	nodeid_elem_t		*head = NULL, *tail = NULL;
	int			error;

	error = clconf_lib_init();
	if (error) {
		clerror("clconf library init failed.\n");
		(*headp) = NULL;
		return (DPM_UNKNOWN_ERROR);
	}

	cl = clconf_cluster_get_current();

	for (i = 1; i <= NODEID_MAX; i++) {
		nodename = clconf_cluster_get_nodename_by_nodeid(cl,
						    (nodeid_t)i);
		if (nodename != NULL) {
			/* nodeid is a valid nodeid */
			error = scdpm_append_ne(
			    &head, &tail, i, NODEID_ELEM_IS_DISK);
			if (error) {
				scdpm_destroy_ne_list(head);
				return (error);
			}
		}
	}

	clconf_obj_release((clconf_obj_t *)cl);

	(*headp) = head;
	return (0);
}

/*
 * Input:
 *	relative_path	- rdsk/d#
 *	disk		- /dev/did/rdsk/d#s0
 * Oupput:
 *	0  Both paths are same
 *	-1 Paths are different
 *
 * Tricky part is not to just look for the string "rdsk/d#" in
 * "/dev/did/rdsk/d#s0". Eg. rdsk/d1, /dev/did/rdsk/d13/s0 are not the same.
 */
static int
pathcmp(char *relative_path, char *disk)
{
	uint_t relative_path_len, disk_len;
	uint_t rel_disk_len;

	relative_path_len = strlen(relative_path);
	disk_len = strlen(disk);

	/* Assume disk is of form /dev/did/rdsk/d#s0 */

	rel_disk_len = disk_len - strlen("/dev/did/") - strlen("s0");

	if (relative_path_len != rel_disk_len) {
		return (-1);
	} else {
		if (strncmp(relative_path, disk+strlen("/dev/did/"),
						    relative_path_len) == 0) {
			return (0);
		} else {
			return (-1);
		}
	}
}

int scdpm_get_nodeid(char *dispath);

/*
 * Input:
 *	diskname in did format
 * Output:
 *	Returns 0 on success.
 *	Returns non-zero for error.
 *	list of nodeids representing the nodes to which the disk is connected,
 *	can be disks or cdroms.
 *
 * This function gets the list of all devices from the did library,
 * and searches the list for the specified disk. Upon finding the specified list
 * the sub-list of all paths that the disk is on is retrieved, and the nodenames
 * are extracted from the paths and converted to nodeids and returned.
 */
static int
get_nodeids_connected_to_did_disk(char *disk, nodeid_elem_t **headp)
{
	/*
	 * devp is a list of devices.
	 * pathp is a list of device paths.
	 */
	did_device_list_t 	*devp;
	char 			*relative_diskpath = NULL;
	did_subpath_t 		*pathp;
	nodeid_t 		nodeid;
	nodeid_elem_t 		*head = NULL, *tail = NULL;
	int			error;
	impl_devid_t		*devid;
	uint_t			nodeid_type;

	if (all_disks_p == NULL) {
		all_disks_p = list_all_devices("disk");
	}

	for (devp = all_disks_p; devp != NULL; devp = devp->next) {
		relative_diskpath = did_get_did_path(devp);
		if (relative_diskpath == NULL) {
			if (did_geterrorstr() != NULL) {
				(void) fprintf(stderr, "%s", did_geterrorstr());
			}
			continue;
		}

		if (pathcmp(relative_diskpath, disk) == 0) {
			devid = (impl_devid_t *)devp->target_device_id;
			if (devid != NULL) {
				nodeid_type = NODEID_ELEM_IS_DISK;
			} else {
				nodeid_type = NODEID_ELEM_IS_CDROM;
			}

			for (pathp = devp->subpath_listp; pathp != NULL;
						    pathp = pathp->next) {

				error = scdpm_get_nodeid(pathp->device_path);
				if (error < 0) {
					free(relative_diskpath);
					return (error);
				}
				nodeid = (uint_t)error;
				error = scdpm_append_ne(&head, &tail,
				    (uint_t)nodeid, nodeid_type);
				if (error) {
					scdpm_destroy_ne_list(head);
					free(relative_diskpath);
					return (error);
				}
			}
		}
	}

	(*headp) = head;
	if (relative_diskpath != NULL) {
		free(relative_diskpath);
	}
	return (0);
}

/*
 * Input:
 *	diskname in ctd format
 * Output:
 *	Returns 0 on success.
 *	Returns non-zero for error.
 *	list of nodeids representing the nodes to which the disk is connected.
 *
 * This function gets the list of all devices from the did library,
 * and searches the list for the specified disk. Upon finding the specified list
 * the sub-list of all paths that the disk is on is retrieved, and the nodenames
 * are extracted from the paths and converted to nodeids and returned.
 */
static int
get_nodeids_connected_to_ctd_disk(char *disk, nodeid_elem_t **headp)
{
	/*
	 * devp is a list of devices.
	 * pathp is a list of device paths.
	 */
	did_device_list_t 	*devp;
	did_subpath_t 		*pathp;
	nodeid_t 		nodeid;
	nodeid_elem_t 		*head = NULL, *tail = NULL;
	char			*cur_disk;
	int			error;
	impl_devid_t		*devid;
	uint_t			nodeid_type;

	/* paths from libdid do not have the slice */
	disk[strlen(disk) - 2] = '\0';

	if (all_disks_p == NULL) {
		all_disks_p = list_all_devices("disk");
	}

	for (devp = all_disks_p; devp != NULL; devp = devp->next) {

		for (pathp = devp->subpath_listp; pathp != NULL;
					    pathp = pathp->next) {

			cur_disk = strrchr(pathp->device_path, ':') + 1;

			if (strcmp(disk, cur_disk) == 0) {

				devid = (impl_devid_t *)devp->target_device_id;
				if (devid != NULL) {
					nodeid_type = NODEID_ELEM_IS_DISK;
				} else {
					nodeid_type = NODEID_ELEM_IS_CDROM;
				}

				error = scdpm_get_nodeid(pathp->device_path);
				if (error < 0) {
					return (error);
				}
				nodeid = (uint_t)error;
				error = scdpm_append_ne(&head, &tail,
				    (uint_t)nodeid, nodeid_type);
				if (error) {
					/* restore slice info */
					disk[strlen(disk)] = 's';
					scdpm_destroy_ne_list(head);
					return (error);
				}
			}
		}
	}

	/* restore slice info */
	disk[strlen(disk)] = 's';

	(*headp) = head;
	return (0);
}

/*
 * Given a diskslice, returns a list of nodeids of the nodes to which the
 * the disk is connected. The input diskslice can be a did disk or a ctd disk.
 * We expect that this function will only be called with a valid diskname.
 */
static int
get_nodeids_connected_to_disk(char *disk, nodeid_elem_t **headp)
{
	if (strncmp(disk, "/dev/did/rdsk/d", strlen("/dev/did/rdsk/d")) == 0) {

		return (get_nodeids_connected_to_did_disk(disk, headp));

	} else if (strncmp(disk, "/dev/rdsk/c", strlen("/dev/rdsk/c")) == 0) {

		return (get_nodeids_connected_to_ctd_disk(disk, headp));

	} else {

		(*headp) = NULL;
		return (DPM_INVALID_PATH);

	}
}

/*
 * Creates and returns a single element list that contains the given nodeid.
 */
static int
make_list(int nodeid, nodeid_elem_t **headp)
{
	nodeid_elem_t 		*head = NULL, *tail = NULL;
	int			error;

	error = scdpm_append_ne(&head, &tail,
	    (uint_t)nodeid, NODEID_ELEM_IS_DISK);
	(*headp) = head;
	return (error);
}

/*
 * The inputs represent a diskpath.
 * This function returns nodeids corresponding to the diskpath represented
 * by the input.
 *
 * Input:
 *	nodeid:	0 represents all
 *		>0 represents a given node
 *	disk:	alls0 represents all slices
 *		other strings are disknames
 *
 * Output:
 *	List of non-zero nodeids that are part of the diskpath[s] represented
 *	by (nodeid, disk).
 */
int
scdpm_get_nodeid_list(int nodeid, char *disk, nodeid_elem_t **headp)
{
	if ((nodeid == 0) && (strcmp(disk, "alls0") == 0)) { /* all:all */

		return (get_all_nodeids(headp));

	} else if (nodeid == 0) {	/* all keyword for node alone */

		return (get_nodeids_connected_to_disk(disk, headp));

	} else {			/* node specified */

		return (make_list(nodeid, headp));

	}
}

/*
 * Translates the given nodeid to the nodename.
 */
const char *
scdpm_get_nodename(uint_t nodeid)
{
	const char *nodename;
	clconf_cluster_t	*cl;
	int error;

	error = clconf_lib_init();
	if (error) {
		clerror("clconf library init failed.\n");
		return (NULL);
	}
	cl = clconf_cluster_get_current();

	nodename = clconf_cluster_get_nodename_by_nodeid(cl, (nodeid_t)nodeid);

	clconf_obj_release((clconf_obj_t *)cl);

	return (nodename);
}


/*
 *  Given nodename, use clconf_cluster_get_nodename_by_nodeid()
 *  to get the nodeid.
 *  Return DPM_INVALID_NODE if not found
 */
int
scdpm_get_nodeid_by_nodename(char *nodename)
{
	int 			i;
	const char 		*tempname;
	clconf_cluster_t	*cl;
	int			error;

	error = clconf_lib_init();
	if (error) {
		clerror("clconf library init failed.\n");
		return (DPM_INVALID_NODE);
	}

	cl = clconf_cluster_get_current();

	for (i = 0; i < NODEID_MAX + 1; i++) {
		tempname = clconf_cluster_get_nodename_by_nodeid(cl,
		    (nodeid_t)i);

		if (tempname == NULL)
			continue;

		if (strcmp(tempname, nodename) == 0) {
			clconf_obj_release((clconf_obj_t *)cl);
			return (i);
		}
	}

	/* didn't find match */
	clconf_obj_release((clconf_obj_t *)cl);
	return (DPM_INVALID_NODE);
}


/*
 * Extracts nodename from diskpath and
 * gets nodeid corresponding to nodename.
 * nodeid is set to 0 if "all" is specified.
 */
int
scdpm_get_nodeid(char *diskpath)
{
	int nodeid;
	char *colon_ptr;
	char *nodename;

	colon_ptr = strrchr(diskpath, ':');
	if (colon_ptr == NULL) {
		/* wrong diskpath */
		return (DPM_INVALID_PATH);
	}
	(*colon_ptr) = '\0';

	nodename = diskpath;

	if (strcmp(nodename, "all") == 0) {
		(*colon_ptr) = ':';
		return (0);
	} else {
		nodeid = scdpm_get_nodeid_by_nodename(nodename);
	}

	(*colon_ptr) = ':';

	return (nodeid);
}

/*
 * Appends "s0" to the disk component of the diskpath and returns it
 */
int
scdpm_get_diskslice(char *diskpath, char **diskp)
{
	char *disk;
	char *colon;
	int error;

	colon = strrchr(diskpath, ':');

	disk = (char *)malloc(strlen(colon+1) + strlen("s0") +1);
	if (disk == NULL) {
		(*diskp) = NULL;
		return (DPM_ENOMEM);
	}

	error = scdpm_track_heap(disk);
	if (error) {
		return (error);
	}

	(void) strcpy(disk, colon+1);
	(void) strcat(disk, "s0");

	(*diskp) = disk;
	return (0);
}
