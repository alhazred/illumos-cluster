/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DISKPATH_COMPONENT_H
#define	_DISKPATH_COMPONENT_H

#pragma ident	"@(#)diskpath_component.h	1.4	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif


#include <sys/clconf.h>
#include <diskpath_component_types.h>
#include <scdpm.h>


#define	ALL_NODEIDS		0
#define	ALL_DISKPATHS		"alls0"

/*
 * get_nodeid()
 *
 * Returns 	0
 *			if all keyword was specified in diskpath for nodename
 *		nodeid
 *			if nodename in diskpath was a valid nodename,
 *			then returns nodeid as reported from libclconf.
 *		DPM_INVALID_NODE
 *			if nodename is neither a valid nodename nor the keyword
 *			all
 *		DPM_INVALID_PATH
 *			a nodename without : is not allowed, usage is
 *			[node|all]:<[/dev/did/rdsk/]d- |
 *			    [/dev/rdsk/]c-t-d- | all>
 *
 */
extern int scdpm_get_nodeid(char *diskpath);

/*
 * get_diskslice()
 *
 * Fills in (*diskp) with the disk portion of (diskpath) and appends the slice
 * number (s#) as needed by the scdpmd daemon.
 *
 * Returns	0		Success
 *		otherwise	Error
 */
extern int scdpm_get_diskslice(char *diskpath, char **diskp);

/*
 * get_nodeid_list()
 *
 * Input:
 *		nodeid	0 for keyword all, actual nodeid for valid nodename
 * Output:
 *		Fills in the pointer to the output list in (*headp)
 *		The output list is as follows:
 *		all:all		nodeids of all nodes in the cluster
 *		all:disk	nodeids of all nodes connected to disk
 *		node:disk	nodeid of the node
 */
extern int scdpm_get_nodeid_list(uint_t nodeid, char *disk,
	    nodeid_elem_t **headp);

/*
 * get_nodename()
 *			Returns nodename for the nodeid.
 */
extern const char *scdpm_get_nodename(uint_t nodeid);

#ifdef	__cplusplus
}
#endif


#endif /* _DISKPATH_COMPONENT_H */
