/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)util.c	1.3	08/05/20 SMI"


#include <stdlib.h>
#include <libscdpm.h>
#include <list.h>

disk_node_t *heap_head = NULL;
disk_node_t *heap_tail = NULL;

/*
 * This function basically maintains a linked list of
 * addresses of memory malloc'd so far.
 * Whenever memory is malloc'd, the following function
 * is called to note down the newly malloc'd memory.
 */
int
scdpm_track_heap(char *diskpath)
{
	return (scdpm_append_dn(&heap_head, &heap_tail, diskpath));
}

/*
 * This functions frees all the malloc'd memory addresses
 * noted down so far.
 */
void
scdpm_heap_clean_up(void)
{
	disk_node_t *node_processed;

	while (heap_head != NULL) {
		free(heap_head->name);
		node_processed = heap_head;
		heap_head = node_processed->next;
		free(node_processed);
	}
}
