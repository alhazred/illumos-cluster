/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DISKPATH_VALIDATE_H
#define	_DISKPATH_VALIDATE_H

#pragma ident	"@(#)diskpath_validate.h	1.3	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif



/*
 * validate_and_format()
 *
 * Find out if the given diskpath is valid.
 *
 * Returns 0 if path okay, non-zero if invalid.
 *
 * In addition, if the diskpath is valid,
 * format to the standard form,
 * 	<nodename>:/dev/did/rdsk/d# (or)
 * 	<nodename>:/dev/rdsk/c0t0d0s0
 * appropriately.
 *
 * Allowed diskpath variations:
 *	d#
 *	/dev/did/rdsk/d#
 *	c#t#d#
 *	/dev/rdsk/c#t#d#
 *
 */
extern int scdpm_validate_and_format(char **diskpath);

#ifdef	__cplusplus
}
#endif


#endif /* _DISKPATH_VALIDATE_H */
