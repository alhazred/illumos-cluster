/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _LIST_H
#define	_LIST_H

#pragma ident	"@(#)list.h	1.4	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif


#include <libscdpm.h>
#include <diskpath_component_types.h>

/*
 * Set of functions to append to different types of lists.
 * Function name convention: append_<initials of disk type>
 */
extern int scdpm_append_dn(disk_node_t **headp, disk_node_t**lastp,
	    char *diskpath);
extern int scdpm_append_dsn(disk_status_node_t **headp,
	    disk_status_node_t**lastp, char *diskpath);
extern int scdpm_prepend_dsn(disk_status_node_t **headp,
	    disk_status_node_t**lastp, char *diskpath);
extern int scdpm_append_ne(nodeid_elem_t **headp, nodeid_elem_t**lastp,
	    uint_t nodeid, uint_t nodeid_type);

/*
 * Set of functions to destroy list framework.
 * List contents are assumed to have already been destroyed.
 */
extern void scdpm_destroy_dn_list_framework(disk_node_t *head);
extern void scdpm_destroy_dsn_list_framework(disk_status_node_t *head);

extern void scdpm_delete_dsn(disk_status_node_t **headp,
	    disk_status_node_t *prev, disk_status_node_t *cur);
extern void scdpm_destroy_ne_list(nodeid_elem_t *head);

#ifdef	__cplusplus
}
#endif


#endif /* _LIST_H */
