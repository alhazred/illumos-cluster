/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)diskpath_validate.c	1.6	08/05/20 SMI"


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libscdpm.h>	/* error codes */
#include <scdpm.h>	/* error codes */
#include <diskpath_component.h>

extern int scdpm_track_heap(char *diskpath);

/*
 * validate_and_format()
 *
 * Find out if the given diskpath is valid.
 *
 * Returns 0 if path okay, non-zero if invalid.
 *
 * In addition, if the diskpath is valid,
 * format to the standard form,
 * 	<nodename>:/dev/did/rdsk/d#s0 (or)
 * 	<nodename>:/dev/rdsk/c#t#d#s0
 * appropriately.
 *
 * Allowed diskpath variations:
 *	d#
 *	/dev/did/rdsk/d#
 *	c#t#d#
 *	/dev/rdsk/c#t#d#
 *
 * Memory is malloc'd in this function
 * for the newly formatted strings.
 * This memory is tracked, to be freed later
 * by scdpm_heap_clean_up() when scdpm processing
 * has completed.
 */
int
scdpm_validate_and_format(char **diskpath_ptr)
{
	/*lint -e668 */
	char *diskpath;
	char *prefix;
	char *formatted_diskpath = NULL;
	char *colon_ptr, *disk, *diskpath_with_slice;
	int error = 0;
	int nodeid;

	nodeid_elem_t *head, *node_processed;

	diskpath = *diskpath_ptr;

	/*
	 * Find the last occurence of the colon in the string.
	 * 	(last because the nodename may have colons?)
	 * 	(we don't allow <nodename> by itself for diskpath)
	 * Everything after the last colon is a <disk>.
	 * If no colon, then the entire diskpath is the disk.
	 */

	colon_ptr = strrchr(diskpath, ':');


	/* ************* Nodename processing *********************** */

	if (colon_ptr == diskpath) {  /* diskpath begins with a colon */
		return (DPM_INVALID_NODE);
	}

	if (colon_ptr == NULL) { /* these is no colon in the diskpath */
		prefix = "all:";

		/*
		 * Assumes only disk is specified.
		 * Prefix [all:] to diskpath
		 * appropriately.
		 */

		formatted_diskpath = (char *)malloc(strlen(prefix) +
						    strlen(diskpath) + 1);
		if (formatted_diskpath == NULL) {
			return (DPM_ENOMEM);
		}

		error = scdpm_track_heap(formatted_diskpath);
		if (error) {
			return (error);
		}

		(void) strcpy(formatted_diskpath, prefix);
		(void) strcat(formatted_diskpath, diskpath);
		diskpath = formatted_diskpath;
	}

	nodeid = scdpm_get_nodeid(diskpath);
	if (nodeid < 0) {
		return (nodeid);
	}


	/* ************* Disk processing *********************** */

	colon_ptr = strrchr(diskpath, ':');
	disk = (colon_ptr == NULL) ? diskpath : (colon_ptr+1);

	/* assumes '<nodename>:' prefix exists */

	if (strcmp(disk, "") == 0) {

		/*
		 * [<nodename>:] not a valid path
		 */

		return (DPM_INVALID_DISK);

	} else if
	    ((strncmp(disk, "/dev/did/rdsk/d",
	    strlen("/dev/did/rdsk/d")) == 0) ||
	    (strncmp(disk, "/dev/rdsk/c",
	    strlen("/dev/rdsk/c")) == 0)) {

		/*
		 * This potentially could be a good diskname.
		 * More checks later.
		 */

	} else if ((*disk) == 'd') {

		/*
		 * Permits [d#] as valid entry
		 * prefix /dev/did/rdsk/ to the diskname
		 */

		formatted_diskpath = (char *)malloc(strlen(diskpath) + \
				    strlen("/dev/did/rdsk/") + \
				    + 1);
		if (formatted_diskpath == NULL) {
			return (DPM_ENOMEM);
		}

		error = scdpm_track_heap(formatted_diskpath);
		if (error) {
			return (error);
		}

		colon_ptr = strrchr(diskpath, ':');
		*colon_ptr = '\0';
		(void) strcpy(formatted_diskpath, diskpath);
		(void) strcat(formatted_diskpath, ":");
		(void) strcat(formatted_diskpath, "/dev/did/rdsk/");
		(void) strcat(formatted_diskpath, disk);
		*colon_ptr = ':';
	} else if ((*disk) == 'c') {

		/*
		 * Permits [c#t#d#] as valid entry
		 * prefix /dev/rdsk/ to the diskname
		 */

		formatted_diskpath = (char *)malloc(strlen(diskpath) + \
				    strlen("/dev/rdsk/") + \
				    + 1);
		if (formatted_diskpath == NULL) {
			return (DPM_ENOMEM);
		}

		error = scdpm_track_heap(formatted_diskpath);
		if (error) {
			return (error);
		}


		colon_ptr = strrchr(diskpath, ':');
		*colon_ptr = '\0';
		(void) strcpy(formatted_diskpath, diskpath);
		(void) strcat(formatted_diskpath, ":");
		(void) strcat(formatted_diskpath, "/dev/rdsk/");
		(void) strcat(formatted_diskpath, disk);
		*colon_ptr = ':';
	} else if (strcmp(disk, "all") == 0) {
		/* this is okay */
		error = 0;
		goto done;
	} else {
		return (DPM_INVALID_DISK);
	}

	if (formatted_diskpath) {
		disk = strrchr(formatted_diskpath, ':') + 1;
	} else {
		disk = strrchr((*diskpath_ptr), ':') + 1;
	}

	/* disk is specific here. It can't be the all keyword. */

	diskpath_with_slice = (char *)malloc(strlen(disk)+3);
	if (diskpath_with_slice == NULL) {
		return (DPM_ENOMEM);
	}

	(void) strcpy(diskpath_with_slice, disk);
	(void) strcat(diskpath_with_slice, "s0");

	error = scdpm_get_nodeid_list(ALL_NODEIDS, diskpath_with_slice, &head);
	if (error) {
		free(diskpath_with_slice);
		return (error);
	}

	if ((nodeid == ALL_NODEIDS) && (head != NULL)) {
		/* Remove all the cdroms from the head list */
		nodeid_elem_t *prev = NULL;
		node_processed = head;
		while (node_processed != NULL) {
			if (node_processed->nodeid_type ==
			    NODEID_ELEM_IS_CDROM) {
				nodeid_elem_t *tmp = node_processed;
				if (prev != NULL) {
					prev->next = node_processed->next;
				} else {
					head = head->next;
				}
				node_processed = node_processed->next;
				free(tmp);
			} else {
				prev = node_processed;
				node_processed = node_processed->next;
			}
		}
		if (head == NULL) {
			/* Only cdroms were found */
			return (DPM_CDROM);
		}
	}

	if ((nodeid == ALL_NODEIDS) && (head == NULL)) {

		/* disk is not present on any node for all:disk case */
		if (formatted_diskpath) {
			(*diskpath_ptr) = formatted_diskpath;
		}

		return (DPM_INVALID_DISK);

	} else if (nodeid != ALL_NODEIDS) {

		/* find if there indeed exists a disk for given node */
		error = DPM_INVALID_DISK;
		while (head != NULL) {
			if (head->nodeid == (uint_t)nodeid) {
				if (head->nodeid_type == NODEID_ELEM_IS_CDROM) {
					error = DPM_CDROM;
				} else {
					error = 0;
				}
			}
			node_processed = head;
			head = head->next;
			free(node_processed);
		}

		free(diskpath_with_slice);
	}

done:
	if ((error == 0) && (formatted_diskpath)) {
		(*diskpath_ptr) = formatted_diskpath;
	}

	return (error);
	/*lint -e668 */
}
