/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _DISKPATH_COMPONENT_TYPES_H
#define	_DISKPATH_COMPONENT_TYPES_H

#pragma ident	"@(#)diskpath_component_types.h	1.4	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif


#define	NODEID_ELEM_IS_DISK		0
#define	NODEID_ELEM_IS_CDROM		1


typedef struct nodeid_elem {
	uint_t			nodeid;
	uint_t			nodeid_type;
	struct nodeid_elem	*next;
} nodeid_elem_t;


#ifdef	__cplusplus
}
#endif


#endif /* _DISKPATH_COMPONENT_TYPES_H */
