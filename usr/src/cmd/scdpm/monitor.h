/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _MONITOR_H
#define	_MONITOR_H

#pragma ident	"@(#)monitor.h	1.5	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif


extern boolean_t enable_auto_reboot;	/* whether auto reboot feature should */
					/* be enabled or disabled */

/*
 * Initialized data structures for the scdpm command.
 */
extern int scdpm_init(void);

/*
 * admin functions:
 *
 * Input validation is done by these functions.
 *
 * If the diskpath has the keyword 'all' for the disk, then the node is
 * contacted for the option processing.
 *
 * If the disk is a valid diskname (and not 'all'), then the diskpath is
 * stored for later processing. This way we can batch the requests to the nodes
 * and hence avoid many trips to the nodes.
 */
extern int scdpm_monitor_admin(char *diskpath);
extern int scdpm_unmonitor_admin(char *diskpath);
extern int scdpm_print_admin(char *diskpath);
extern int scdpm_print_subopt_admin(char *diskpath);
extern int scdpm_auto_reboot_admin(char *nodename);

/*
 * action functions:
 *
 * These functions generate the batch requests to the nodes based on the
 * diskpaths that have been previously stored. The store is emptied
 * after the requests have been processed.
 */
extern int scdpm_monitor_unmonitor_action(void);
extern int scdpm_print_action(uint_t);
extern int scdpm_print_subopt_action(void);
extern int scdpm_auto_reboot_action(void);

#ifdef	__cplusplus
}
#endif


#endif /* _MONITOR_H */
