/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _SCDPM_H
#define	_SCDPM_H

#pragma ident	"@(#)scdpm.h	1.6	08/05/20 SMI"

#include <didadm.h>

#ifdef	__cplusplus
extern "C" {
#endif


#define	DPM_FAILURE		(-9)
#define	DPM_CCR_ERROR		(-8)
#define	DPM_NOT_ROOT		(-7)
#define	DPM_CDROM		(-6)
#define	DPM_INVALID_NODE	(-5)
#define	DPM_INVALID_DISK	(-4)
#define	DPM_SYNTAX_ERROR	(-3)
#define	DPM_INVALID_PATH	(-2)
#define	DPM_COMPLETE_FAILURE	(-1)

/*
 * The above error codes are in negative, new codes added should have lesser
 * value. libscdpm has positive error codes, this way the local error codes from
 * are different from error codes from the library.
 */

extern int scdpm_fatal_error(int error);
extern void scdpm_print_error(int error, char *diskpath);

#define	DPM_SHOW_UNMONITORED	0x1
#define	DPM_SHOW_OKAY		0x2
#define	DPM_SHOW_FAIL		0x4
#define	DPM_SHOW_UNKNOWN	0x8
#define	DPM_SHOW_NONE		0x0
#define	DPM_SHOW_ALL		(DPM_SHOW_UNMONITORED|DPM_SHOW_OKAY| \
				DPM_SHOW_FAIL|DPM_SHOW_UNKNOWN)

#ifdef	__cplusplus
}
#endif


#endif /* _SCDPM_H */
