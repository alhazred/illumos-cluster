/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)list.c	1.4	08/05/20 SMI"


#include <stdio.h>
#include <stdlib.h>

#include <libscdpm.h>
#include <diskpath_component_types.h>

#define	PREPEND_NODE	cur->next = NULL;			\
			if ((*headp) == NULL) {			\
			(*headp) = (*tailp) = cur;		\
			} else {				\
				cur->next = (*headp);		\
				(*headp) = cur;			\
			}

#define	APPEND_NODE	cur->next = NULL;			\
			if ((*headp) == NULL) {			\
			(*headp) = (*tailp) = cur;		\
			} else {				\
				(*tailp)->next = cur;		\
				(*tailp) = cur;			\
			}

#define	DESTROY_NODES	while (head != NULL) {			\
				node_processed = head;		\
				head = node_processed->next;	\
				free(node_processed);		\
			}
int
scdpm_append_dn(disk_node_t **headp, disk_node_t**tailp, char *diskpath)
{
	disk_node_t *cur  = NULL;

	cur = (disk_node_t *)malloc(sizeof (disk_node_t));
	if (cur == NULL) {
		return (DPM_ENOMEM);
	}
	cur->name = diskpath;

	APPEND_NODE;
	return (0);
}

int
scdpm_append_dsn(disk_status_node_t **headp, disk_status_node_t**tailp,
    char *diskpath)
{
	disk_status_node_t *cur  = NULL;

	cur = (disk_status_node_t *)malloc(sizeof (disk_status_node_t));
	if (cur == NULL) {
		return (DPM_ENOMEM);
	}
	cur->name = diskpath;
	cur->status = DPM_PATH_UNKNOWN;

	APPEND_NODE;
	return (0);
}

int
scdpm_prepend_dsn(disk_status_node_t **headp, disk_status_node_t**tailp,
    char *diskpath)
{
	disk_status_node_t *cur  = NULL;

	cur = (disk_status_node_t *)malloc(sizeof (disk_status_node_t));
	if (cur == NULL) {
		return (DPM_ENOMEM);
	}
	cur->name = diskpath;
	cur->status = DPM_PATH_UNKNOWN;

	PREPEND_NODE;
	return (0);
}

int
scdpm_append_ne(nodeid_elem_t **headp, nodeid_elem_t **tailp,
    uint_t nodeid, uint_t nodeid_type)
{
	nodeid_elem_t *cur  = NULL;

	cur = (nodeid_elem_t *)malloc(sizeof (nodeid_elem_t));
	if (cur == NULL) {
		return (DPM_ENOMEM);
	}
	cur->nodeid = nodeid;
	cur->nodeid_type = nodeid_type;

	APPEND_NODE;
	return (0);
}

void
scdpm_destroy_dn_list_framework(disk_node_t *head)
{
	disk_node_t *node_processed;

	DESTROY_NODES;
}

void
scdpm_destroy_dsn_list_framework(disk_status_node_t *head)
{
	disk_status_node_t *node_processed;

	DESTROY_NODES;
}

void
scdpm_delete_dsn(disk_status_node_t **headp, disk_status_node_t *prev,
    disk_status_node_t *cur)
{
	if ((prev == NULL) || (cur == (*headp))) {
		/* Deleting first element in list */
		(*headp) = cur->next;
	} else {
		prev->next = cur->next;
	}

	if (cur->name != NULL) {
		free(cur->name);
	}
	free(cur);
}

void
scdpm_destroy_ne_list(nodeid_elem_t *head)
{
	nodeid_elem_t *next;

	while (head != NULL) {
		next = head->next;
		free(head);
		head = next;
	}
}
