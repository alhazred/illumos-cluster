/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 *  Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 *  Use is subject to license terms.
 *
 */

#pragma ident	"@(#)monitor.c	1.14	08/05/20 SMI"


#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <libintl.h>

#include <sys/clconf.h>

#include <clmsg_int.h>

#include <libscdpm.h>

#include <node_db.h>
#include <list.h>
#include <diskpath_validate.h>
#include <diskpath_component.h>
#include <scdpm.h>
#include <monitor.h>


#define	STATUS_COLUMN_POSITION	(64)

/*
 * This is for detecting a complete failure of the command.
 * TRUE/FALSE values must be in accordance with description
 * in scdpm.c, where the variable is defined.
 */
extern int complete_failure;
extern int verbose_print;

extern void scdpm_track_heap(char *string);
extern void scdpm_heap_clean_up(void);

/*
 * The following enum is for complete_failure variable.
 * This needs to be in sync with scdpm.c
 */


typedef enum { ALL, FAIL } print_category_t;

static arr_stat_elem_t *m_arr = NULL, *u_arr = NULL;
static arr_stat_elem_t *p_arr = NULL, *f_arr = NULL;


static int reboot_nodeid;		/* node specified by -a or -n */

static int
dpmccr_read_wrapper(disk_status_node_t **a, int *b)
{
	int error;

	error = dpmccr_read_disks_info(a, (uint_t *)b);

	if (error != DPM_SUCCESS) {
		return (DPM_CCR_ERROR);
	} else {
		return (DPM_SUCCESS);
	}
}

static int
dpmccr_write_wrapper(disk_status_node_t *a, int b)
{
	int error;

	error = dpmccr_ready();
	if (error != DPM_SUCCESS) {
		return (DPM_CCR_ERROR);
	}

	error = dpmccr_begin_write_operation();
	if (error != DPM_SUCCESS) {
		return (DPM_CCR_ERROR);
	}

	error = dpmccr_write_disks_info(a, b);
	if (error != DPM_SUCCESS) {
		(void) dpmccr_abort_write_operation();
		return (DPM_CCR_ERROR);
	}

	error = dpmccr_end_write_operation();

	if (error != DPM_SUCCESS) {
		return (DPM_CCR_ERROR);
	} else {
		return (DPM_SUCCESS);
	}
}


/*
 * Creates databases to store diskpaths
 * for monitoring/unmonitoring/status-querying.
 * -- m, u, p options respectively --
 * Initializing the database framework
 * consists of creating an array with NODEID_MAX
 * elements, with each element being the
 * head of the list of diskpaths for a given node.
 */
int
scdpm_init(void)
{
	int error;

	error = scdpm_node_stat_db_create(&m_arr);
	if (error) {
		return (error);
	}
	error = scdpm_node_stat_db_create(&u_arr);
	if (error) {
		return (error);
	}
	error = scdpm_node_stat_db_create(&p_arr);
	if (error) {
		return (error);
	}
	error = scdpm_node_stat_db_create(&f_arr);
	if (error) {
		return (error);
	}
	return (0);
}


/*
 * Administrative procedures for m, u, p options.
 * Validate the diskpath, and format it.
 * Returns DPM_INVALID_PATH if invalid */
static int
mup_common(char **diskpath, uint_t *nodeid, char **disk)
{
	int error = 0;

	error = scdpm_validate_and_format(diskpath);
	if (error) {
		return (error);
	}

	error = scdpm_get_nodeid(*diskpath);
	if (error < 0) {
		return (error);
	}
	(*nodeid) = (uint_t)error;

	error = scdpm_get_diskslice(*diskpath, disk);

	return (error);
}

static int
mu_admin_all_nodes(uint_t nodeid, char *disk, arr_stat_elem_t *arr)
{
	nodeid_elem_t *head, *node_processed;
	int error = 0;

	/*
	 * If nodeid is 0 (all keyword) and disk is alls0 (all keyword),
	 * get all nodeids in the cluster.
	 * If nodeid is 0 (all keyword) alone,
	 * get all nodes that the disk is connected to.
	 */
	error = scdpm_get_nodeid_list(nodeid, disk, &head);
	if (error) {
		return (error);
	}

	while (head != NULL) {
		if (head->nodeid_type == NODEID_ELEM_IS_DISK) {
			error = scdpm_node_stat_db_update(
				arr, head->nodeid, disk);
			if (error != 0) {
				/* Free the list & exit */
				scdpm_destroy_ne_list(head);
				break;
			}
		}
		node_processed = head;
		head = head->next;
		free(node_processed);
	}

	return (error);
}

int
scdpm_monitor_admin(char *diskpath)
{
	int error;
	uint_t nodeid;
	char *disk;


	error = mup_common(&diskpath, &nodeid, &disk);
	if (error) {
		return (error);
	}

	error = mu_admin_all_nodes(nodeid, disk, m_arr);

	return (error);
}

/*
 * Input diskpath has slice information in it.
 * CCR entries are of form nodeid:disk
 */
static int
scdpm_ccr_remove_disk(uint_t nodeid, char *diskpath)
{
	disk_status_node_t *head, *cur, *prev;
	int count, error;
	char invalid_entry[_POSIX_PATH_MAX] = "";

	/*
	 * Construct ccr entry.
	 */
	(void) sprintf(invalid_entry, "%d:%s", nodeid, diskpath);

	/*
	 * Read all the disk entries from ccr.
	 */
	error = dpmccr_read_wrapper(&head, &count);
	if (error != 0) {
		return (error);
	}

	/*
	 * Walk the list, find the specific entry & remove it.
	 */
	prev = head;
	cur = head;
	while (cur != NULL) {
		if (strcmp(cur->name, invalid_entry) == 0) {
			scdpm_delete_dsn(&head, prev, cur);
			break;
		}
		prev = cur;
		cur = cur->next;
	}


	/*
	 * Write out the disk list with force option.
	 */
	error = dpmccr_write_wrapper(head, SCDPM_UPDATE_FORCE);

	return (error);
}

int
scdpm_unmonitor_admin(char *diskpath)
{
	int error, tmp_error;
	uint_t nodeid;
	char *disk;
	disk_status_node_t invalid_disk;

	error = mup_common(&diskpath, &nodeid, &disk);

	/*
	 * Invalid disk has been specified.
	 * Remove this entry if present on the daemon.
	 * Remove this entry from CCR also.
	 */
	if (error == DPM_INVALID_DISK) {
		tmp_error = scdpm_get_nodeid(diskpath);
		if (tmp_error < 0) {
			return (DPM_INVALID_DISK);
		}
		nodeid = (uint_t)tmp_error;
		tmp_error = scdpm_get_diskslice(diskpath, &disk);
		if (tmp_error != 0) {
			/*
			 * The error in this case would be ENOMEM, but
			 * we still return DPM_INVALID_DISK so that users
			 * get a consistent error message.
			 */
			return (DPM_INVALID_DISK);
		}

		invalid_disk.name = disk;
		/*
		 * We would like to return the error as invalid disk
		 * even if the daemon failed to remove it from the list.
		 */
		(void) libdpm_remove_disks(nodeid, &invalid_disk, 1);

		/*
		 * Though the ccr error is more important than invalid disk
		 * error, we still return DPM_INVALID_DISK so that users get
		 * a consistent error message.
		 */
		(void) scdpm_ccr_remove_disk(nodeid, disk);

		return (DPM_INVALID_DISK);
	}

	if (error) {
		return (error);
	}

	error = mu_admin_all_nodes(nodeid, disk, u_arr);

	return (error);
}

/*
 * Given a node name (or "all") returns the node's id, ALL_NODEIDS (0) for "all"
 * or DPM_INVALID_NODE if there is no translation.
 */
int
scdpm_auto_reboot_admin(char *nodename)
{
	extern int scdpm_get_nodeid_by_nodename(char *);

	if (strcasecmp(nodename, "all") == 0) {
		reboot_nodeid = ALL_NODEIDS;
		return (0);
	}
	/* reboot_nodeid will get the node id or DPM_INVALID_NODE */
	reboot_nodeid = scdpm_get_nodeid_by_nodename(nodename);

	if (reboot_nodeid == DPM_INVALID_NODE) {
		return (DPM_INVALID_NODE);
	} else {
		return (DPM_SUCCESS);
	}
}

/*
 * Actually takes the action to set whether the automatic reboot feature
 * should be enabled or disabled on the node(s) specified by
 * reboot_nodeid.
 */
int
scdpm_auto_reboot_action(void)
{
	int error = 0;

	if (reboot_nodeid == DPM_INVALID_NODE) {
		return (DPM_INVALID_NODE);
	}
	error = libdpm_set_auto_reboot_feature_state((nodeid_t)reboot_nodeid,
	    enable_auto_reboot);

	if (error == DPM_SUCCESS) {
		complete_failure = FALSE;
	} else {
		clerror("Failed to set auto reboot state.\n");
		return (error);
	}

	return (0);
}

static void print_all(uint_t nodeid, disk_status_node_t *head, uint_t dopt);

static int
print_admin_common(char *diskpath, print_category_t query)
{
	int error;
	uint_t nodeid;
	char *disk;
	nodeid_elem_t *head, *node_processed;

	error = mup_common(&diskpath, &nodeid, &disk);
	if (error) {
		return (error);
	}

	error = scdpm_get_nodeid_list(nodeid, disk, &head);
	if (error) {
		return (error);
	}

	while (head != NULL) {
		if (head->nodeid_type == NODEID_ELEM_IS_DISK) {
			if (query == ALL) {
				error = scdpm_node_stat_db_update(
				    p_arr, head->nodeid, disk);
			} else {
				error = scdpm_node_stat_db_update(
				    f_arr, head->nodeid, disk);
			}
			if (error != 0) {
				/* Free the list & exit */
				scdpm_destroy_ne_list(head);
				break;
			}
		}
		node_processed = head;
		head = head->next;
		free(node_processed);
	}

	return (error);
}

int
scdpm_print_admin(char *diskpath)
{
	return (print_admin_common(diskpath, ALL));
}

int
scdpm_print_subopt_admin(char *diskpath)
{
	return (print_admin_common(diskpath, FAIL));
}


static void
position_cursor_to_second_column(uint_t curpos)
{
	if (curpos < STATUS_COLUMN_POSITION) {
		while (curpos < STATUS_COLUMN_POSITION) {
			(void) printf(" ");
			curpos ++;
		}
	} else {
		/*
		 * Add at least one space so that
		 * status is separated from disk name.
		 */
		(void) printf(" ");
	}
}

static void
print_path_to_stderr(const char *nodename, char *diskslice)
{
	uint_t len;
	/* slice number is not printed */

	len = strlen(diskslice);
	diskslice[len-2] = '\0';
	(void) fprintf(stderr, "%s:%s", nodename, diskslice);
	diskslice[len-2] = 's';
}

/*
 * This function prints the path to screen.
 * The diskpath argument contains the slice number in it as well.
 * The slice number is not printed to the screen.
 */
void
print_path_to_screen(const char *nodename, char *diskpath)
{
	uint_t len;
	/* slice number is not printed */

	len = strlen(diskpath);
	diskpath[len-2] = '\0';
	(void) printf("%s:%s", nodename, diskpath);
	diskpath[len-2] = 's';
}

static int
include_status(const int status, const uint_t d_mask)
{
	uint_t check;
	if (!DPM_PATH_IS_MONITORED(status))
		check = DPM_SHOW_UNMONITORED;
	else if (DPM_PATH_IS_OK(status))
		check = DPM_SHOW_OKAY;
	else if (DPM_PATH_IS_FAIL(status))
		check = DPM_SHOW_FAIL;
	else
		check = DPM_SHOW_UNKNOWN;
	return (d_mask & check ? TRUE : FALSE);
}

static void
print_status_to_screen(int status)
{
	if (!DPM_PATH_IS_MONITORED(status)) {
		(void) printf(gettext("Unmonitored\n"));
		return;
	}

	if (DPM_PATH_IS_OK(status)) {
		(void) printf(gettext("Ok\n"));
	} else if (DPM_PATH_IS_FAIL(status)) {
		(void) printf(gettext("Fail\n"));
	} else {
		(void) printf(gettext("Unknown\n"));
	}
}	

static void
print_all(uint_t nodeid, disk_status_node_t *head, uint_t dopt)
{
	const char *nodename;
	disk_status_node_t *cur;

	nodename = scdpm_get_nodename(nodeid);

	cur = head;
	while (cur) {
		if (include_status(cur->status, dopt)) {
			print_path_to_screen(nodename, cur->name);
			position_cursor_to_second_column(strlen(nodename) + 1
			    + strlen(cur->name) + 1);
			print_status_to_screen(cur->status);
		}
		cur = cur->next;
	}
}

/*
 * The DPM daemon might have encountered errors for specific disks.
 * This functions processes errors for individual disk errors.
 * If the verbose print option had been set earlier, then this
 * function prints further information on the processing.
 */
static void
process_mu_error(disk_status_node_t *head, uint_t i, char *success_string)
{
	disk_status_node_t *cur;

	cur = head;

	while (cur != NULL) {
		if (!DPM_PATH_IS_REGISTERED(cur->status)) {
			/*
			 * Daemon did not register this request.
			 * This implies the request made had an invalid path.
			 * Currently, all input validations are done in the
			 * command processing.  However, we retain this
			 * code inorder to catch any validations that might
			 * have been missed somehow.
			 */
			(void) fprintf(stderr, gettext("scdpm: invalid path "));
			print_path_to_stderr(scdpm_get_nodename(i+1),
						    cur->name);
			(void) fprintf(stderr, "\n");
		} else {
			/*
			 * Atleast one path was successfully registered.
			 * So, the command is not a complete failure.
			 */
			complete_failure = FALSE;
			if (verbose_print) {
				print_path_to_screen(scdpm_get_nodename(i+1),
				    cur->name);
				position_cursor_to_second_column(
					    strlen(scdpm_get_nodename(i+1)) + 1
					    + strlen(cur->name) + 1);
				(void) printf("%s\n", success_string);
			}
		}
		cur = cur->next;
	}
}

static int
mu_action_all_ccr(uint_t nodeid, int command)
{
	disk_status_node_t *head, *cur, *prev;
	int count;
	int error;

	/*
	 * Since, we are processing all disks for the given node,
	 * we read all disks on all nodes from CCR,
	 * filter out disks not on this node,
	 * set the new status as per the change request
	 * in the disk data structure.
	 * Write the list of disks & new disk status for given node
	 * to the ccr.
	 */

	error = dpmccr_read_wrapper(&head, &count);
	if (error != DPM_SUCCESS) {
		return (error);
	}

	prev = NULL;
	cur = head;
	while (cur != NULL) {
		if ((uint_t)atoi(cur->name) != nodeid) {
			/* Does not belong to given node, delete. */
			scdpm_delete_dsn(&head, prev, cur);
			cur = cur->next;
			continue;
		}

		/* Set status: monitor or unmonitor */
		cur->status = command;
		if (command == DPM_PATH_MONITORED) {
			/*
			 * Initialize for monitoring,
			 * current disk state is unknown
			 */
			cur->status |= DPM_PATH_UNKNOWN;
		}
		prev = cur;
		cur = cur->next;
	}

	/*
	 * Now the list contains disk information for all disks
	 * pertaining to the given node. Write this to the ccr.
	 */

	error = dpmccr_write_wrapper(head, 0);
	if (error != DPM_SUCCESS) {
		return (error);
	} else {
		/*
		 * We have successfully made changes to the CCR.
		 * Hence, this command is not a complete failure.
		 */
		complete_failure = FALSE;
	}

	return (0);
}

static int
mu_action_all(uint_t nodeid, int command)
{
	int error;

	error = mu_action_all_ccr(nodeid, command);

	if (error) {
		return (error);
	}

	error = libdpm_change_monitoring_status_all((nodeid_t)nodeid, command);

	if (error == DPM_SUCCESS) {
		complete_failure = FALSE;

		if (verbose_print) {
			print_path_to_screen(scdpm_get_nodename(nodeid),
			    "alls0");
			position_cursor_to_second_column(
			    strlen(scdpm_get_nodename(nodeid)) + 1
			    + strlen("alls0") + 1);
			if (command == DPM_PATH_MONITORED) {
				(void) printf(gettext("Monitored\n"));
			} else {
				(void) printf(gettext("Unmonitored\n"));
			}
		}
	}

	if (error == DPM_NO_DAEMON) {
		clerror("No response from daemon on node "
		    "\"%s\".\n", scdpm_get_nodename(nodeid));
	}

	return (error);
}

/*
 * This function notes down monitor & unmonitor actions in the ccr.
 * Input:
 *	List of diskpaths for a given nodeid.
 */
static int
dpmccr_write_adding_nodeid(uint_t nodeid, disk_status_node_t *head, int command)
{
	disk_status_node_t *new_head = NULL, *new_tail = NULL;
	disk_status_node_t *cur;
	char *nodeid_diskslicep;
	int error = 0;

	/* create list with nodeid prefix added */
	cur = head;
	while (cur != NULL) {
		nodeid_diskslicep =
		    (char *)malloc(sizeof (char) * _POSIX_PATH_MAX);

		if (nodeid_diskslicep == NULL) {
			error = ENOMEM;
			goto clean_up;
		}

		(void) sprintf(nodeid_diskslicep, "%u:%s", nodeid, cur->name);

		error =
		    scdpm_append_dsn(&new_head, &new_tail, nodeid_diskslicep);

		if (error != 0) {
			goto clean_up;
		}

		cur = cur->next;
	}

	cur = new_head;
	while (cur != NULL) {
		cur->status |= command;
		cur = cur->next;
	}

	error = dpmccr_write_wrapper(new_head, 0);

clean_up:
	/* clean-up the new list */
	cur = new_head;
	while (cur != NULL) {
		if (cur->name != NULL) {
			free(cur->name);
		}
		cur = cur->next;
	}
	scdpm_destroy_dsn_list_framework(new_head);

	return (error);
}

int
scdpm_monitor_unmonitor_action(void)
{
	uint_t i;
	uint_t m_count, u_count;
	int error = 0;
	disk_status_node_t *head;

	for (i = 0; i < NODEID_MAX; i++) {
		m_count = m_arr[i].count;
		u_count = u_arr[i].count;

		if (m_count) {

			head = m_arr[i].head;

			while ((head) &&
			    (strcmp(head->name, "alls0") == 0)) {
				error = mu_action_all(i+1, DPM_PATH_MONITORED);
				head = head->next;
				m_count--;
				/* print error? */
			}

			if (head) {
				/*
				 * Record in CCR first,
				 * before calling daemon
				 * to update in-core list.
				 */
				/* dpmccr expects nodeid:diskslice */
				error = dpmccr_write_adding_nodeid(i+1, head,
						    DPM_PATH_MONITORED);
				/*
				 * Problem writing to CCR, abort.
				 */
				if (error != DPM_SUCCESS) {
					goto scdpm_mu_action_cleanup;
				}

				complete_failure = FALSE;

				/*
				 * Send request to scdpmd daemon,
				 * via libscdpm library.
				 */
				error = libdpm_change_monitoring_status(
				    i+1, head, m_count, NULL, 0);

				/*
				 * Daemon could not be reached.
				 * However, we have already made the
				 * change in the CCR, so we just
				 * print a message, and record this error,
				 * and go ahead.
				 */
				if (error == DPM_NO_DAEMON) {
					clerror("No response from daemon on "
					    "node \"%s\".\n",
					    scdpm_get_nodename(i+1));
				}

				/*
				 * Daemon was present and processed the request.
				 * But the daemon found an invalid path.
				 * The following statement would never be
				 * reached today, as cli does are invalidations.
				 * Cli also calls the libdpm_remove_disks
				 * interfaces in case of an invalid disk
				 * being found, hence this would be caught
				 * and processed much earlier today.
				 * The code below is only a safety net.
				 */
				if ((error != DPM_SUCCESS) &&
				    (error != DPM_NO_DAEMON)) {
					process_mu_error
					    (head, i, gettext("Monitored"));
				}

			}
		}

		if (u_count) {

			head = u_arr[i].head;

			while ((head) &&
			    (strcmp(head->name, "alls0") == 0)) {
				error = mu_action_all(i+1,
				    DPM_PATH_UNMONITORED);
				head = head->next;
				u_count--;
				/* print error? */
			}

			if (head) {
				/*
				 * Record in CCR first,
				 * before calling daemon
				 * to update in-core list.
				 */
				/* dpmccr expects nodeid:diskslice */
				error = dpmccr_write_adding_nodeid(i+1, head,
						    DPM_PATH_UNMONITORED);
				if (error != DPM_SUCCESS) {
					goto scdpm_mu_action_cleanup;
				}

				complete_failure = FALSE;

				/*
				 * Send request to scdpmd daemon,
				 * via libscdpm library.
				 */
				error = libdpm_change_monitoring_status(i+1,
				    NULL, 0, head, u_count);

				/*
				 * Daemon could not be reached.
				 * However, we have already made the
				 * change in the CCR, so we just
				 * print a message, and record this error,
				 * and go ahead.
				 */
				if (error == DPM_NO_DAEMON) {
					clerror("No response from daemon on "
					    "node \"%s\".\n",
					    scdpm_get_nodename(i+1));
				}

				/*
				 * Daemon was present and processed the request.
				 * But the daemon found an invalid path.
				 * The following statement would never be
				 * reached today, as cli does are invalidations.
				 * Cli also calls the libdpm_remove_disks
				 * interfaces in case of an invalid disk
				 * being found, hence this would be caught
				 * and processed much earlier today.
				 * The code below is only a safety net.
				 */
				if ((error != DPM_SUCCESS) &&
				    (error != DPM_NO_DAEMON)) {
					process_mu_error(head, i,
					    gettext("Unmonitored"));
				}
			}
		}
	}

scdpm_mu_action_cleanup:

	scdpm_node_stat_db_empty(m_arr);
	scdpm_node_stat_db_empty(u_arr);

	return (error);
}

/*
 * Throws away non-nodeid entries.
 * Retains only information that pertains to the given node.
 */
static void
chew_list(disk_status_node_t **headp, uint_t nodeid)
{
	disk_status_node_t *prev, *cur;

	prev = NULL;
	cur = *headp;
	while (cur != NULL) {
		if ((uint_t)atoi(cur->name) != nodeid) {
			/* Does not belong to given node, delete. */
			free(cur->name);
			scdpm_delete_dsn(headp, prev, cur);
			cur = cur->next;
			continue;
		} else {
			scdpm_track_heap(cur->name);
			cur->name = strchr(cur->name, ':') + 1;
		}

		prev = cur;
		cur = cur->next;
	}
}

/*
 * get_query_answer()
 *
 * The function fills in the status information for the given list
 * of disks for status queries.
 *
 * This function will be invoked only if the status information
 * could not be obtained from the daemon, and we need to fall back
 * on the status information stored in the CCR.
 *
 * Input:
 *	List of disks for which the status needs to be found.
 *	List of all disks in the system, and their current status as in CCR.
 *
 * Note: In case the status was last recorded as DPM_PATH_OK,
 * the answer should always be DPM_UNKNOWN in this case.
 *
 */
static void
get_query_answer(disk_status_node_t *query_headp,
    disk_status_node_t *answer_headp)
{
	disk_status_node_t *query_cur, *answer_cur;
	char *answer_diskp;

	query_cur = query_headp;

	while (query_cur != NULL) {

		/* assume Unmonitored to start with */
		query_cur->status = DPM_PATH_UNMONITORED;

		answer_cur = answer_headp;

		while (answer_cur != NULL) {

			answer_diskp = strchr(answer_cur->name, ':')+1;

			if (strcmp(query_cur->name, answer_diskp) == 0) {

				if (DPM_PATH_IS_MONITORED(answer_cur->status)) {
					query_cur->status = DPM_PATH_MONITORED
							    | DPM_PATH_UNKNOWN;
				}
			}
			answer_cur = answer_cur->next;
		}

		query_cur = query_cur->next;
	}
}


/*
 * print_action_common()
 *
 * This function processes all -p and -F requests accumulated so far,
 * We get the auto reboot status from the CCR.
 * We get the disk path monitoring status information from the daemon.
 * If the daemon cannot be reached, then we fallback on the information
 * stored in the CCR.
 *
 * Input:
 *		arr   - pointer to the accumulated requests
 *		query - ALL or FAIL (whether -p or -F)
 */
static int
print_action_common(arr_stat_elem_t *arr, print_category_t query, uint_t d_opt)
{
	uint_t i;
	disk_status_node_t *head, *cur;
	uint_t p_count;
	int error = 0, autoreboot_error = 0;
	disk_status_node_t *all_head;
	int all_count;
	boolean_t autorebootlist[NODEID_MAX];

	/* Get auto reboot state from the CCR */
	autoreboot_error = libdpm_get_all_auto_reboot_state(autorebootlist);
	if (autoreboot_error) {
		/* Print warning message */
		clwarning("Failed to read auto-reboot states from the cluster "
		    "configuration.\n");
	}

	/* process for each node */
	for (i = 0; i < NODEID_MAX; i++) {
		p_count = arr[i].count;

		if (p_count) {
			/* If this node is mentioned, print it's reboot state */
			if (!autoreboot_error) {
				(void) printf("%s:reboot_on_path_failure %s",
				    scdpm_get_nodename(i+1),
				    (autorebootlist[i] ?
				    gettext("enabled\n") :
				    gettext("disabled\n")));
			}
			/* Skip it if we failed to retrieve auto reboot state */

			head = arr[i].head;

			/* process if "all" disks were requested */
			while ((head) &&
			    (strcmp(head->name, "alls0") == 0)) {
				if (query == ALL) {
					error
					    = libdpm_get_monitoring_status_all(
					    (uint_t)i+1, &all_head);
				} else {
					error = libdpm_get_failed_disk_status(
					    (uint_t)i+1, &all_head);
				}
				if (error == DPM_SUCCESS) {
					complete_failure = FALSE;
					print_all(i+1, all_head, d_opt);
				}
				if (error == DPM_NO_DAEMON) {
					clerror("No response from daemon on "
					    "node \"%s\".\n",
					    scdpm_get_nodename(i+1));

					error = dpmccr_read_wrapper(&all_head,
								    &all_count);
					chew_list(&all_head, i+1);
					print_all(i+1, all_head, d_opt);
				}
				head = head->next;
				p_count--;
				/* print error? */
			}


			/* process for specific disk */
			if (head) {
				error = libdpm_get_monitoring_status(
				    i+1, head, p_count, FALSE);
			}

			if (error == DPM_SUCCESS) {
				complete_failure = FALSE;
			}

			if (error == DPM_NO_DAEMON) {
				clerror("No response from daemon on node "
				    "\"%s\".\n", scdpm_get_nodename(i+1));
			}

			if (error != DPM_SUCCESS) {

				/* Call to daemon failed. Fallback on CCR. */

				error =
				    dpmccr_read_wrapper(&all_head, &all_count);
				if (error != DPM_SUCCESS) {
					goto scdpm_print_action_common_cleanup;
				} else {
					complete_failure = FALSE;
				}

				/*
				 * head : list of queries
				 * all_head: list of all answers
				 */
				get_query_answer(head, all_head);
			}


			/* print results */
			cur = head;
			while (cur) {
				if ((query == FAIL) &&
				    (DPM_PATH_IS_FAIL(cur->status))) {
					cur = cur->next;
					continue;
				}
				print_path_to_screen(scdpm_get_nodename(i+1),
						    cur->name);
				position_cursor_to_second_column(
					    strlen(scdpm_get_nodename(i+1)) + 1
					    + strlen(cur->name) + 1);
				print_status_to_screen(cur->status);
				cur = cur->next;
			}
		}
	}

scdpm_print_action_common_cleanup:
	/* Finished processing the requests. Clean-up. */
	if (query == FAIL) {
		scdpm_node_stat_db_empty(f_arr);
	} else {
		scdpm_node_stat_db_empty(p_arr);
	}

	return (error);
}

int
scdpm_print_action(uint_t d_opt)
{
	return (print_action_common(p_arr, ALL, d_opt));
}

int
scdpm_print_subopt_action(void)
{
	return (print_action_common(f_arr, FAIL, DPM_SHOW_ALL));
}

void
scdpm_clean_up(void)
{
	scdpm_node_stat_db_destroy(m_arr);
	scdpm_node_stat_db_destroy(u_arr);
	scdpm_node_stat_db_destroy(p_arr);
	scdpm_node_stat_db_destroy(f_arr);
	scdpm_heap_clean_up();
}
