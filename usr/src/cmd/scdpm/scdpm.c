/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)scdpm.c	1.10	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <locale.h>	/* setlocale */
#include <libintl.h>	/* gettext */

#include <libscdpm.h>

#include <clmsg_int.h>

#include <monitor.h>
#include <scdpm.h>
#include <rgm/sczones.h>

#include <scadmin/scconf.h>
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>

extern void scdpm_clean_up(void);
extern int scdpm_read_file(const char *);

/*
 * Assume the command is a complete failure,
 * until atleast one option succeeds.
 */
int complete_failure = TRUE;
int verbose_print = FALSE;
char option_processed = '\0';
static int exit_code;
static int CLDEVICE_CMD = 0;
boolean_t enable_auto_reboot;

char *print_subopts[] = {
#define	FAULTY_PATHS	0
	"-F",
#define	SELECT_STATES	1
	"-s",
	NULL};


static void
usage(void)
{
	(void) fprintf(stderr, gettext(
	    "Usage:\n"
	    "\tscdpm -m [node|all]:<[/dev/did/rdsk/]d- | [/dev/rdsk/]c-t-d- |"
	    " all>\n"
	    "\tscdpm -u [node|all]:<[/dev/did/rdsk/]d- | [/dev/rdsk/]c-t-d- |"
	    " all>\n"
	    "\tscdpm -p [-F] [node|all]:<[/dev/did/rdsk/]d- | [/dev/rdsk/]c-t"
	    "-d- | all>\n"
	    "\tscdpm -a <node|all>\n"
	    "\tscdpm -n <node|all>\n"
	    "\tscdpm -f filename\n"));
}

/*
 * Maps the errors from libscdpm, and local errors
 * to the return error code as described in scdpm manpage.
 */
static int
map_error(int error)
{
	switch (error) {
	case DPM_SUCCESS:
				return (0);
	case DPM_SYNTAX_ERROR:
	case DPM_COMPLETE_FAILURE:
				if (!CLDEVICE_CMD)
					return (1);
				else
					return (CL_EINTERNAL);
	case DPM_NO_DAEMON:
	case DPM_ENOENT:
				if (CLDEVICE_CMD)
					return (CL_ENOENT);
	case DPM_ENOMEM:
				if (CLDEVICE_CMD)
					return (CL_ENOMEM);
	case DPM_EEXIST:
				if (CLDEVICE_CMD)
					return (CL_EEXIST);
	case DPM_EACCES:
				if (CLDEVICE_CMD)
					return (CL_EACCESS);
	case DPM_INVALID_DISK:
				if (CLDEVICE_CMD)
					return (CL_ENOENT);
	case DPM_INVALID_NODE:
	case DPM_INVALID_PATH:
				if (CLDEVICE_CMD)
					return (CL_EINVAL);
	case DPM_UNKNOWN_ERROR:
	case DPM_ERR_COMM:
	case DPM_CCR_ERROR:
	case DPM_FAILURE:
	default:
				if (CLDEVICE_CMD)
					return (CL_EINTERNAL);
				else
					return (2);
	}
}

/*
 * This function takes in a error code, and
 * returns whether this error is considered fatal
 * or not.  In case of a fatal error, the
 * scdpm command processing aborts with
 * an error message.
 *
 * Return values: 1 - if error is fatal
 *		  0 - if error is not fatal
 *
 * Currently out of memory and ccr processing errors
 * are considered to be fatal errors.
 */
int
scdpm_fatal_error(int error)
{
	switch (error) {
	case DPM_ENOMEM:
	case DPM_CCR_ERROR:
			return (1);
	default:
			return (0);
	}
}

void
scdpm_print_error(int error, char *diskpath)
{
	switch (error) {
	case DPM_NO_DAEMON:
				/* printed when lib call is made */
				break;
	case DPM_ENOENT:
				break;
	case DPM_ENOMEM:
				clerror("Not enough memory. Aborting.\n");
				break;
	case DPM_EEXIST:
				/*
				 * Should be printed
				 * in monitor_unmonitor_action()
				 */
				break;
	case DPM_EACCES:
				clerror("Permission denied.\n");
				break;
	case DPM_ERR_COMM:
				clerror("Communication error.\n");
				break;
	case DPM_INVALID_PATH:
				clerror("Invalid path - \"%s\".\n",
				    diskpath);
				break;
	case DPM_CDROM:
				clerror("Cannot monitor CD-ROM \"%s\".\n",
				    diskpath);
				break;
	case DPM_INVALID_DISK:
				clerror("Invalid disk in \"%s\".\n",
				    diskpath);
				break;
	case DPM_INVALID_NODE:
				clerror("Invalid node in \"%s\".\n",
				    diskpath);
				break;
	case DPM_SYNTAX_ERROR:
				/*
				 * Detailed syntax error messages
				 * are printed during the parse phase.
				 */
				break;
	case DPM_UNKNOWN_ERROR:
				clerror("Internal error while processing "
				    "\"%s\".\n", diskpath);
				break;
	case DPM_FAILURE:
				/*
				 * This generic failure code is returned
				 * by the file input processing code,
				 * while handling the '-f' option.
				 * Specific error messages are printed
				 * by that code.
				 */
				break;
	case DPM_CCR_ERROR:
				clerror("Error processing CCR. Aborting.\n");
				break;
	default:
				break;
	}
}

/*
 * This function processes the -F suboption
 * to the -p option.
 * The input 'options' is the pointer returned
 * by getsubopt() after detecting the -F option.
 */
static int
parse_arg_F(int argc, char **argv, char *options)
{
	int error;

	if (*options == '\0') {
		/*
		 * subopt processing over.
		 * i.e, -p<white-space>-F<white-space>
		 * was specified on the command line.
		 */

		/*
		 * -F is the last arg?
		 * If its is the last arg,
		 * this means that the -F option did not have
		 * an argument specified, in which case we default to
		 * all:all
		 */
		if (optind == argc) {
			error = scdpm_print_subopt_admin("all:all");
			return (error);
		}

		/*
		 * -F has an argument?
		 * To answer this question, we need to determine if
		 * the argv[i] string that follows -F is a option
		 * to scdpm.  If not, we treat argv[i] as the argument
		 * to -F.
		 */
		if (argv[optind][0] != '-') {
			error = scdpm_print_subopt_admin(argv[optind]);
			optind++;
		} else {
			error = scdpm_print_subopt_admin("all:all");
		}
	} else {
		/*
		 * comma and more suboptions detected.
		 * i.e., -p<white-space>-F,xxxx...
		 * was detected.
		 */
		(void) fprintf(stderr, gettext("scdpm: incorrect suboption for "
					    "option p\n"));
		error = DPM_SYNTAX_ERROR;
	}
	return (error);
}

/*
 * This function processes the -s suboption to the -p option.
 *
 * The '-s' flag is a private interface used to support the "cldevice"
 * command.  The flag and it's required argument may be repeated to select
 * multiple device states:
 * 	scdpm -p [-s state]... <remainder of -p options>
 */
static int
parse_arg_s(int argc, char **argv, uint_t *s_select)
{
	int error = 0;

	if (optind == argc) {
		/* not enough arguments passed */
		error = DPM_SYNTAX_ERROR;
		return (error);
	}

	*s_select = DPM_SHOW_NONE;

	/*
	 * XXX - The following hardcoded states must be the same as those
	 *	 accepted by the "cldevice status -s" command and should
	 *	 correspond to the non-localized output of the
	 *	 print_status_to_screen().   Ideally, these strings should be
	 *	 coded as labels/defines and shared between the two programs.
	 */
	while (optind < argc) {
		if (strcmp(argv[optind], "unmonitored") == 0) {
			*s_select |= DPM_SHOW_UNMONITORED;
		} else if (strcmp(argv[optind], "ok") == 0) {
			*s_select |= DPM_SHOW_OKAY;
		} else if (strcmp(argv[optind], "fail") == 0) {
			*s_select |= DPM_SHOW_FAIL;
		} else if (strcmp(argv[optind], "unknown") == 0) {
			*s_select |= DPM_SHOW_UNKNOWN;
		} else {
			error = DPM_SYNTAX_ERROR;
			break;
		}
		if (argv[++optind]) {
			if (strcmp(argv[optind], "-s") == 0) {
				optind++;
			} else {
				/* pass next argument to print command */
				break;
			}
		}
	}

	if (!error) {
		if (optind == argc) {
			/*
			 * If no arguments other than "-s" were supplied
			 * to command, then default to "all:all" as is
			 * done for "-F"
			 */
			error = scdpm_print_admin("all:all");
		} else {
			error = scdpm_print_admin(argv[optind++]);
		}
	}
	return (error);
}

static int
parse_args(int argc, char **argv, uint_t *sel)
{
	int c;
	extern char *optarg;
	extern int optind;
	extern int opterr;
	extern int optopt;
	char *value;
	int vflg = 0;
	int mflg = 0;
	int uflg = 0;
	int pflg = 0;
	int Fflg = 0;
	int fflg = 0;
	int aflg = 0;
	int nflg = 0;
	int error = 0;
	int done = FALSE;
	int cur_option;

	char *options;

	opterr = 0;
	done = FALSE;

	if (argc == 1) {
		usage();
		return (0);
	}

	cl_auth_demote();

	while (!done) {
		c = getopt(argc, argv, "vm:u:p:f:a:n:");
		if (c == EOF) {
			if (optind < argc) {
				(void) fprintf(stderr, gettext(
					    "scdpm: syntax error at "
					    "%s. Aborting.\n"), argv[optind]);
				return (DPM_SYNTAX_ERROR);
			} else {
				done = TRUE;
			}
			continue;
		}

		if (c == '?') {
			/*
			 * option letter not included in the optstring found.
			 * or no argument after an option that expects one.
			 */
			cur_option = optopt;
		} else {
			cur_option = c;
		}

		/*
		 * Check if incompatible options have been specified
		 * in the command.
		 */
		switch (cur_option) {
		case 'm':
		case 'u':
		case 'p':
		case 'f':
		case 'a':
		case 'n':
			if ((option_processed) && (c != option_processed)) {
				(void) fprintf(stderr, gettext(
				    "scdpm: incompatible options "
				    "-%c -%c\n"), option_processed, cur_option);
				return (DPM_SYNTAX_ERROR);
			}
			break;
		case 'h':
			done = TRUE;
			complete_failure = FALSE;
			continue;
		default:
			break;
		}

		if (c == '?') {
			/*
			 * option letter not included in the optstring found.
			 * or no argument after an option that expects one.
			 */
			c = optopt;

			switch (c) {
			case 'm':
			case 'u':
			case 'p':
			case 'f':
			case 'a':
			case 'n':
				(void) fprintf(stderr, gettext(
				    "scdpm: missing argument for option "
				    "-%c\n"), c);
				return (DPM_SYNTAX_ERROR);
			default:
				(void) fprintf(stderr, gettext(
				    "scdpm: unrecognized option "
				    "-%c\n"), c);
				return (DPM_SYNTAX_ERROR);
			}
		}


		switch (c) {

		case 'v':
			vflg++;
			break;

		case 'a':
			aflg++;
			cl_auth_check_command_opt_exit(*argv, "-a",
			    CL_AUTH_DEVICE_ADMIN, DPM_COMPLETE_FAILURE);

			/* Promote to euid=0 */
			cl_auth_promote();

			error = scdpm_auto_reboot_admin(optarg);
			enable_auto_reboot = B_TRUE;
			break;

		case 'n':
			nflg++;
			cl_auth_check_command_opt_exit(*argv, "-n",
			    CL_AUTH_DEVICE_ADMIN, DPM_COMPLETE_FAILURE);

			/* Promote to euid=0 */
			cl_auth_promote();

			error = scdpm_auto_reboot_admin(optarg);
			enable_auto_reboot = B_FALSE;
			break;

		case 'm':
			mflg++;
			cl_auth_check_command_opt_exit(*argv, "-m",
			    CL_AUTH_DEVICE_ADMIN, DPM_COMPLETE_FAILURE);

			/* Promote to euid=0 */
			cl_auth_promote();

			error = scdpm_monitor_admin(optarg);
			break;

		case 'u':
			uflg++;
			cl_auth_check_command_opt_exit(*argv, "-u",
			    CL_AUTH_DEVICE_ADMIN, DPM_COMPLETE_FAILURE);

			/* Promote to euid=0 */
			cl_auth_promote();

			error = scdpm_unmonitor_admin(optarg);
			break;

		case 'p':
			pflg++;
			*sel = DPM_SHOW_ALL;

			/* Only read permissions needed for print options. */
			cl_auth_check_command_opt_exit(*argv, "-p",
			    CL_AUTH_DEVICE_READ, DPM_COMPLETE_FAILURE);

			/* Promote to euid=0 */
			cl_auth_promote();

			options = optarg;
			if (*options != '\0') {
				switch (getsubopt(&options, print_subopts,
				    &value)) {
				case FAULTY_PATHS:
					Fflg++;
					pflg--;
					error = parse_arg_F(argc, argv,
							    options);
					break;

				case SELECT_STATES:
					error = parse_arg_s(argc, argv, sel);
					break;

				default:
					error = scdpm_print_admin(optarg);
				}
			}
			break;
		case 'f':
			fflg++;
			cl_auth_check_command_opt_exit(*argv, "-f",
			    CL_AUTH_DEVICE_ADMIN, DPM_COMPLETE_FAILURE);

			/* Promote to euid=0 */
			cl_auth_promote();

			error = scdpm_read_file(optarg);
			/*
			 * All error messages are printed by
			 * by the file option processing code.
			 */
			break;
		default:
			/* execution would never reach the following line */
			break;
		}

		scdpm_print_error(error, optarg);

		if (scdpm_fatal_error(error)) {
			return (error);
		}

		/*
		 * If m/u/p/f/a/n flags have been processed. Record that fact.
		 */
		switch (c) {
		case 'm':
		case 'u':
		case 'p':
		case 'f':
		case 'a':
		case 'n':
			option_processed = (char)c;
			break;
		case 'F':
		case 's':
			option_processed = 'p';
			break;
		default:
			break;
		}

	}

	if (vflg) {
		verbose_print = TRUE;
	}

	if (!mflg && !pflg && !uflg && !Fflg && !fflg && !aflg && !nflg) {
		usage();
	}

	return (error);
}

int
main(int argc, char **argv)
{
	int error;
	scconf_errno_t scconferr = SCCONF_NOERR;
	uint_t ismember;
	char *cmd_name = NULL;
	uint_t sel_option;		/* states to select, for -p */

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	/* See if this is from the newcli */
	cmd_name = getenv(CLCMD_ENV);
	if (cmd_name) {
		CLDEVICE_CMD++;
	} else {
		/* Set an evnironment variable to turn off msg ID's */
		(void) putenv(CLCOMMANDS_VAR_NO_MESSAGE_IDS_SET);
		cmd_name = argv[0];
	}

	/* Initialize and promote to euid=0 */
	if (!CLDEVICE_CMD) {
		cl_auth_init();
		cl_auth_promote();
	}

	if (sc_zonescheck() != 0) {
		if (CLDEVICE_CMD)
			return (CL_EINTERNAL);
		else
			return (1);
	}

	/* Initialize the clmsg library */
	if (init_clmsg(cmd_name)) {
		fprintf(stderr, gettext("Could not initialize message "
		    "printing library.\n"));
		exit(CL_ENOMEM);
	}

	/* make sure that we are active in the cluster */
	ismember = 0;
	scconferr = scconf_ismember(0, &ismember);
	if (scconferr != SCCONF_NOERR || !ismember) {
		/*lint -e788 */

		/*
		 * All other scconf error values translate
		 * to complete failure.
		 */
		switch (scconferr) {

		case SCCONF_EPERM:
			/* This should not be returned really */
			scdpm_print_error(DPM_EACCES, NULL);
			return (map_error(DPM_COMPLETE_FAILURE));

		case SCCONF_ENOMEM:
			scdpm_print_error(DPM_ENOMEM, NULL);
			return (map_error(DPM_COMPLETE_FAILURE));

		default:
			clerror("Not in cluster mode.\n");
			return (map_error(DPM_COMPLETE_FAILURE));
		}
		/*lint +e788 */
	}

	error = dpm_initialize();
	if (error) {
		clerror("Could not initialize DPM infrastructure.\n");
		return (map_error(DPM_COMPLETE_FAILURE));
	}

	error = dpmccr_initialize(NULL);
	if (error) {
		clerror("Could not initialize CCR.\n");
		return (map_error(DPM_COMPLETE_FAILURE));
	}

	error = scdpm_init();
	if (error) {
		return (map_error(error));
	}

	error = parse_args(argc, argv, &sel_option);

	if (error == DPM_NOT_ROOT) {
		/* must be root to proceed */
		clerror("Must be superuser.\n");
		if (CLDEVICE_CMD)
			return (CL_EACCESS);
		else
			map_error(DPM_COMPLETE_FAILURE);
	}

	if (error == DPM_SYNTAX_ERROR) {
		usage();
		if (CLDEVICE_CMD)
			return (CL_EINVAL);
		else
			return (map_error(DPM_COMPLETE_FAILURE));
	}

	if (error == DPM_ENOMEM) {
		if (CLDEVICE_CMD)
			return (CL_ENOMEM);
		else
			return (map_error(DPM_COMPLETE_FAILURE));
	}

	/* Save the error as the return value */
	if (error && CLDEVICE_CMD)
		exit_code = map_error(error);
	else
		exit_code = 0;

	/* Generate event */
	if (!CLDEVICE_CMD && (
	    option_processed == 'm' || option_processed == 'u' ||
	    option_processed == 'f' || option_processed == 'a' ||
	    option_processed == 'n')) {
		cl_cmd_event_init(argc, argv, &exit_code);
		(void) cl_cmd_event_gen_start_event();
		(void) atexit(cl_cmd_event_gen_end_event);
	}

	switch (option_processed) {
	case 'm':
	case 'u':
		error = scdpm_monitor_unmonitor_action();
		break;
	case 'p':
		error = scdpm_print_action(sel_option);
		break;
	case 'F':
		error = scdpm_print_subopt_action();
		break;
	case 'a':
	case 'n':
		error = scdpm_auto_reboot_action();
		break;
	default:
		break;
	}

	scdpm_clean_up();

	if (complete_failure == TRUE && !CLDEVICE_CMD) {
		exit_code = map_error(DPM_COMPLETE_FAILURE);
		return (map_error(DPM_COMPLETE_FAILURE));
	} else {
		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = map_error(error);
		} else {
			exit_code = map_error(error);
		}
		return (exit_code);
	}
}
