/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCZONECFG_H
#define	_SCZONECFG_H

#pragma ident	"@(#)sczonecfg.h	1.7	08/07/17 SMI"

/*
 * header file for sczonecfg command
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <unistd.h>
#include <strings.h>
#include <stdbool.h>
#include <zonecluster_prop/zonecluster_prop.h>

#define	ZC_ERR		1
#define	ZC_USAGE	2
#define	ZC_REPEAT	3

#define	CMD_ADD		0
#define	CMD_CANCEL	1
#define	CMD_COMMIT	2
#define	CMD_CREATE	3
#define	CMD_DELETE	4
#define	CMD_END		5
#define	CMD_EXIT	6
#define	CMD_EXPORT	7
#define	CMD_HELP	8
#define	CMD_INFO	9
#define	CMD_REMOVE	10
#define	CMD_REVERT	11
#define	CMD_SELECT	12
#define	CMD_SET		13
#define	CMD_VERIFY	14

#define	CMD_MIN		CMD_ADD
#define	CMD_MAX		CMD_VERIFY

#define	MAX_EQ_PROP_PAIRS	3

#define	PROP_VAL_SIMPLE		0
#define	PROP_VAL_COMPLEX	1
#define	PROP_VAL_LIST		2

#define	PROP_VAL_MIN		PROP_VAL_SIMPLE
#define	PROP_VAL_MAX		PROP_VAL_LIST

/*
 * If any subcommand is ever modified to take more than three arguments,
 * this will need to be incremented.
 */
#define	MAX_SUBCMD_ARGS		3

typedef struct complex_property {
	int	cp_type;	/* from the PT_* list above */
	char	*cp_value;
	struct complex_property *cp_next;
} complex_property_t, *complex_property_ptr_t;

typedef struct list_property {
	char	*lp_simple;
	complex_property_ptr_t	lp_complex;
	struct list_property	*lp_next;
} list_property_t, *list_property_ptr_t;

typedef struct property_value {
	int	pv_type;	/* from the PROP_VAL_* list above */
	char	*pv_simple;
	complex_property_ptr_t	pv_complex;
	list_property_ptr_t	pv_list;
} property_value_t, *property_value_ptr_t;

typedef struct cmd {
	char	*cmd_name;
	void	(*cmd_handler)(struct cmd *);
	int	cmd_res_type;
	int	cmd_prop_nv_pairs;
	int	cmd_prop_name[MAX_EQ_PROP_PAIRS];
	property_value_ptr_t	cmd_property_ptr[MAX_EQ_PROP_PAIRS];
	int	cmd_argc;
	char	*cmd_argv[MAX_SUBCMD_ARGS + 1];
} cmd_t;

#define	HELP_USAGE	0x01
#define	HELP_SUBCMDS	0x02
#define	HELP_SYNTAX	0x04
#define	HELP_RESOURCES	0x08
#define	HELP_PROPS	0x10
#define	HELP_META	0x20
#define	HELP_NETADDR	0x40
#define	HELP_RES_SCOPE	0x80
#define	HELP_NODE_SCOPE	0x100

#define	HELP_RES_PROPS	(HELP_RESOURCES | HELP_PROPS)

extern void add_func(cmd_t *);
extern void cancel_func(cmd_t *);
extern void commit_func(cmd_t *);
extern void create_func(cmd_t *);
extern void delete_func(cmd_t *);
extern void end_func(cmd_t *);
extern void exit_func(cmd_t *);
extern void export_func(cmd_t *);
extern void help_func(cmd_t *);
extern void info_func(cmd_t *);
extern void remove_func(cmd_t *);
extern void revert_func(cmd_t *);
extern void select_func(cmd_t *);
extern void set_func(cmd_t *);
extern void verify_func(cmd_t *);

extern cmd_t *alloc_cmd(void);
extern complex_property_ptr_t alloc_complex(void);
extern list_property_ptr_t alloc_list(void);
extern void free_cmd(cmd_t *cmd);
extern void free_complex(complex_property_ptr_t complex);
extern void free_list(list_property_ptr_t list);
extern void free_outer_list(list_property_ptr_t list);

extern void usage(bool verbose, uint_t flags);
extern void short_usage(int command);

extern FILE *yyin;

#ifdef __cplusplus
}
#endif

#endif	/* _SCZONECFG_H */
