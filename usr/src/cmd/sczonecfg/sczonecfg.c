/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sczonecfg.c	1.35	09/03/31 SMI"

/*
 * sczonecfg is the distributed version of zonecfg(1M). It is used for
 * configuring a "zone cluster" on one or more Sun Cluster nodes.
 *
 * sczonecfg is a private Sun Cluster command and should not be used directly
 * by the user. clzonecluster(1CL) should be used instead.
 *
 * sczonecfg is modeled after zonecfg(1M) in Solaris and borrows code and
 * algorithms from zonecfg. sczonecfg supports extra zone configuration
 * parameters over and above those available in the regular zonecfg command.
 * These are the sysidcfg parameters, and the ability to configure zone
 * resources on a per-node basis. The description below is taken from
 * usr/src/cmd/zonecfg/zonecfg.c in Solaris, with zonecfg substituted with
 * sczonecfg.
 *
 * sczonecfg is a lex/yacc based command interpreter used to manage zone
 * cluster configurations.  The lexer (see sczonecfg_lex.l) builds up tokens,
 * which the grammar (see sczonecfg_grammar.y) builds up into commands, some of
 * which takes resources and/or properties as arguments.  See the block
 * comments near the end of sczonecfg_grammar.y for how the data structures
 * which keep track of these resources and properties are built up.
 *
 * The resource/property data structures are inserted into a command
 * structure (see sczonecfg.h), which also keeps track of command names,
 * miscellaneous arguments, and function handlers.  The grammar selects
 * the appropriate function handler, each of which takes a pointer to a
 * command structure as its sole argument, and invokes it.  The grammar
 * itself is "entered" (a la the Matrix) by yyparse(), which is called
 * from read_input(), our main driving function.  That in turn is called
 * by one of do_interactive(), cmd_file() or one_command_at_a_time(), each
 * of which is called from main() depending on how the program was invoked.
 *
 * The rest of this module consists of the various function handlers and
 * their helper functions.  Some of these functions, particularly the
 * X_to_str() functions, which maps command, resource and property numbers
 * to strings, are used quite liberally, as doing so results in a better
 * program w/rt I18N, reducing the need for translation notes.
 */

#include <sys/mntent.h>
#include <sys/varargs.h>
#include <sys/sysmacros.h>

#include <errno.h>
#include <fcntl.h>
#include <strings.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h>
#include <zone.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <locale.h>
#include <libintl.h>
#include <alloca.h>
#include <signal.h>
#include <wait.h>
#include <libtecla.h>
#include <auth_attr.h>
#include <pwd.h>

#include "sczonecfg.h"
#include <sys/clconf_int.h>
#include <sys/sc_syslog_msg.h>
#include <sys/cladm_int.h>
#include <libzccfg/libzccfg.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <scha.h>

#if !defined(TEXT_DOMAIN)		/* should be defined by cc -D */
#define	TEXT_DOMAIN	"SYS_TEST"	/* Use this only if it wasn't */
#endif

#define	PAGER	"/usr/bin/more"

struct help {
	uint_t	cmd_num;
	char	*cmd_name;
	uint_t	flags;
	char	*short_usage;
};

extern int yyparse(void);
extern int lex_lineno;

#define	MAX_LINE_LEN	1024
#define	MAX_CMD_HIST	1024

#define	ONE_MB		1048576

/*
 * Each SHELP_ should be a simple string.
 */

#define	SHELP_ADD	"add <resource-type>\n\t(global scope)\n" \
	"add <property-name> <property-value>\n\t(resource scope)"
#define	SHELP_CANCEL	"cancel"
#define	SHELP_COMMIT	"commit"
#define	SHELP_CREATE	"create [ -b | -t <template> ]"
#define	SHELP_DELETE	"delete [-F]"
#define	SHELP_END	"end"
#define	SHELP_EXIT	"exit [-F]"
#define	SHELP_EXPORT	"export [-f output-file]"
#define	SHELP_HELP	"help [commands] [syntax] [usage] [<command-name>]"
#define	SHELP_INFO	"info [<resource-type> [property-name=property-value]*]"
#define	SHELP_REMOVE	"remove <resource-type> { <property-name>=<property-" \
	"value> }\n\t(global scope)\nremove <property-name> <property-value>" \
	"\n\t(resource scope)"
#define	SHELP_REVERT	"revert [-F]"
#define	SHELP_SELECT	"select <resource-type> { <property-name>=" \
	"<property-value> }"
#define	SHELP_SET	"set <property-name>=<property-value>"
#define	SHELP_VERIFY	"verify"

/*lint -e786 */
static struct help helptab[] = {
	{ CMD_ADD,	"add",		HELP_RES_PROPS,	SHELP_ADD, },
	{ CMD_CANCEL,	"cancel",	0,		SHELP_CANCEL, },
	{ CMD_COMMIT,	"commit",	0,		SHELP_COMMIT, },
	{ CMD_CREATE,	"create",	0,		SHELP_CREATE, },
	{ CMD_DELETE,	"delete",	0,		SHELP_DELETE, },
	{ CMD_END,	"end",		0,		SHELP_END, },
	{ CMD_EXIT,	"exit",		0,		SHELP_EXIT, },
	{ CMD_EXPORT,	"export",	0,		SHELP_EXPORT, },
	{ CMD_HELP,	"help",		0,		SHELP_HELP },
	{ CMD_INFO,	"info",		HELP_RES_PROPS,	SHELP_INFO, },
	{ CMD_REMOVE,	"remove",	HELP_RES_PROPS,	SHELP_REMOVE, },
	{ CMD_REVERT,	"revert",	0,		SHELP_REVERT, },
	{ CMD_SELECT,	"select",	HELP_RES_PROPS,	SHELP_SELECT, },
	{ CMD_SET,	"set",		HELP_PROPS,	SHELP_SET, },
	{ CMD_VERIFY,	"verify",	0,		SHELP_VERIFY, },
	{ 0 },
};
/*lint +e786 */

#define	MAX_RT_STRLEN	16


/* These *must* match the order of the PROP_VAL_ define's from sczonecfg.h */
static char *prop_val_types[] = {
	"simple",
	"complex",
	"list",
};

/*
 * The various _cmds[] lists below are for command tab-completion. The order of
 * commands in the tables is not important.
 */

/*
 * remove has a space afterwards because it has qualifiers; the other commands
 * that have qualifiers (add, select and set) don't need a space here because
 * they have their own _cmds[] lists below.
 */
static const char *global_scope_cmds[] = {
	"add",
	"commit",
	"create",
	"delete",
	"exit",
	"export",
	"help",
	"info",
	"remove ",
	"revert",
	"select",
	"set",
	"verify",
	NULL
};

static const char *add_cmds[] = {
	"add fs",
	"add inherit-pkg-dir",
	"add net",
	"add device",
	"add rctl",
	"add attr",
	"add dataset",
	"add node",
	"add sysid",
	"add dedicated-cpu",
	"add capped-cpu",
	"add capped-memory",
	NULL
};

static const char *remove_cmds[] = {
	"remove fs ",
	"remove inherit-pkg-dir ",
	"remove net ",
	"remove device ",
	"remove rctl ",
	"remove attr ",
	"remove dataset ",
	"remove node ",
	"remove sysid",
	"remove dedicated-cpu",
	"remove capped-memory",
	"remove capped-cpu",
	NULL
};

static const char *select_cmds[] = {
	"select fs ",
	"select inherit-pkg-dir ",
	"select net ",
	"select device ",
	"select rctl ",
	"select attr ",
	"select dataset ",
	"select node ",
	"select sysid",
	"select dedicated-cpu",
	"select capped-cpu",
	"select capped-memory",
	NULL
};

/*lint -e786 */
static const char *set_cmds[] = {
	"set zonename=",
	"set zonepath=",
	"set brand=",
	"set autoboot=",
	"set pool=",
	"set limitpriv=",
	"set bootargs=",
	"set scheduling-class=",
	"set enable_priv_net=",
	"set ip-type=",
	"set " ALIAS_MAXLWPS "=",
	"set " ALIAS_MAXSHMMEM "=",
	"set " ALIAS_MAXSHMIDS "=",
	"set " ALIAS_MAXMSGIDS "=",
	"set " ALIAS_MAXSEMIDS "=",
	"set " ALIAS_SHARES "=",
	NULL
};
/*lint +e786 */

static const char *info_cmds[] = {
	"info fs ",
	"info inherit-pkg-dir ",
	"info net ",
	"info device ",
	"info rctl ",
	"info attr ",
	"info dataset ",
	"info node ",
	"info zonename",
	"info zonepath",
	"info autoboot",
	"info pool",
	"info limitpriv",
	"info bootargs",
	"info sysid",
	"info brand",
	"info scheduling-class",
	"info max-lwps",
	"info max-shm-memory",
	"info max-shm-ids",
	"info max-msg-ids",
	"info max-sem-ids",
	"info cpu-shares",
	"info enable_priv_net",
	NULL
};

static const char *fs_res_scope_cmds[] = {
	"add options ",
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"remove options ",
	"set dir=",
	"set raw=",
	"set special=",
	"set type=",
	NULL
};

static const char *net_res_scope_cmds[] = {
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"set address=",
	"set physical=",
	"set defrouter=",
	NULL
};

static const char *ipd_res_scope_cmds[] = {
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"set dir=",
	NULL
};

static const char *device_res_scope_cmds[] = {
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"set match=",
	NULL
};

static const char *attr_res_scope_cmds[] = {
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"set name=",
	"set type=",
	"set value=",
	NULL
};

static const char *rctl_res_scope_cmds[] = {
	"add value ",
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"remove value ",
	"set name=",
	NULL
};

static const char *dataset_res_scope_cmds[] = {
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"set name=",
	NULL
};

static const char *pset_res_scope_cmds[] = {
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"set ncpus=",
	"set importance=",
	/* "clear importance", */
	NULL
};

static const char *pcap_res_scope_cmds[] = {
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"set ncpus=",
	NULL
};

static const char *mcap_res_scope_cmds[] = {
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"set physical=",
	"set swap=",
	"set locked=",
	/*
	 * "clear physical",
	 * "clear swap",
	 * "clear locked",
	 */
	NULL
};


static const char *node_res_scope_cmds[] = {
	"add net",
	"add fs",
	"add rctl",
	"add dataset",
	"add device",
	"cancel",
	"end",
	"exit",
	"help",
	"info",
	"select net ",
	"select fs ",
	"select device ",
	"select dataset ",
	"select rctl ",
	"set physical-host=",
	"set hostname=",
	NULL
};

static const char *sysid_res_scope_cmds[] = {
	"set root_password=",
	"set name_service=",
	"set nfs4_domain=",
	"set security_policy=",
	"set system_locale=",
	"set terminal=",
	"set timezone=",
	NULL,
};

/* Global variables */

/*
 * The name of the command. Set early in main(), never modified thereafter.
 * Used all over the place.
 */
static char *execnamep;

/*
 * The top level prompt string. We want to hide 'sczonecfg' from the user, so
 * this is a different string - the abreviated version of 'clzonecluster(1CL)'.
 */
static const char *main_prompt = "clzc";

/* The XML tree pointer, set in main(), used all over the place */
static zc_dochandle_t handle;

/* The name of the zone cluster, used all over the place */
static char zone[ZONENAME_MAX];

/*
 * Flags whether or not the configuration has changed.
 * Set in modifying functions, checked in read_input().
 */
static bool need_to_commit = false;

/*
 * Flags whether or not this zone cluster is new.
 */
static bool new_zone_cluster = false;

/*
 * Flags whether or not there was a configuration error.
 */
bool saw_error;

/*
 * Set in yacc parser, checked in read_input().
 * If true, it means that the input line is complete, and we need to process
 * the next input line and/or display the interactive prompt (if in
 * interactive mode).
 */
bool newline_terminated;

/*
 * Set to true if we're in command file mode.
 * Set in main(), checked in lex error handler.
 */
bool cmd_file_mode;

/*
 * Set in exit_func(), checked in read_input().
 * If true, we exit the interactive read_input() loop.
 */
static bool time_to_exit = false;

/*
 * Set to true if the configuration has changed but the user does not
 * want to commit the changes.
 */
static bool force_exit = false;

/* used in short_usage() and zerr() */
static char *cmd_file_namep = NULL;

/*
 * Set to true if we're in interactive mode.
 * Checked in read_input() and other places
 */
static bool ok_to_prompt = false;

/*
 * If true, it means that we successfully initialized/obtained the XML handle
 * for the specified zone cluster. Set and checked in initialize().
 */
static bool got_handle = false;

/*
 * Set to true if we're in interactive mode.
 * initialized in do_interactive(), checked in initialize()
 */
static bool interactive_mode;

/*
 * Set to true if the user does not have permission to change the
 * configuration. Set in main(), checked in multiple places.
 */
static bool read_only_mode;

/*
 * Set to true if we're in the global scope.
 */
static bool global_scope = true;

/*
 * Indicates which resource scope we're in. The interactive prompt changes
 * based on its value. Should be in the RT_ list from sczonecfg.h.
 */
static int resource_scope;

/*
 * node_scope is true in between "add/select node", and "end" in the node
 * scope.
 */
static bool node_scope = false;
static int end_op = -1;		/* operation on end is either add or modify */

/*
 * saved_end_op is used in the node scope to track which operation (add or
 * modify) to perform when the user enters the end command there.
 */
static int saved_end_op = -1;

/*
 * defrouter_support is set to true if the underlying Solaris zones
 * supports this feature.
 */
bool defrouter_support = false;

/*
 * Number of property n/v pairs.
 */
int num_prop_vals;

static	int	initialize(bool);
static	void	bytes_to_units(char *, char *, int);

/*
 * These are for keeping track of resources as they are specified as part of
 * the multi-step process.  They should be initialized by add_resource() or
 * select_func() and filled in by add_property() or set_func().
 */
static struct zc_fstab		old_fstab;
static struct zc_fstab		in_prog_fstab;
static struct zc_fstab		old_ipdtab;
static struct zc_fstab		in_prog_ipdtab;
static struct zc_nwiftab	old_nwiftab;
static struct zc_nwiftab	in_prog_nwiftab;
static struct zc_devtab		old_devtab;
static struct zc_devtab		in_prog_devtab;
static struct zc_rctltab	old_rctltab;
static struct zc_rctltab	in_prog_rctltab;
static struct zc_attrtab	in_prog_attrtab;
static struct zc_dstab		old_dstab;
static struct zc_dstab		in_prog_dstab;
static struct zc_nodetab	old_nodetab;
static struct zc_nodetab	in_prog_nodetab;
static struct zc_sysidtab	old_sysidtab;
static struct zc_sysidtab	in_prog_sysidtab;
static struct zc_psettab	old_psettab;
static struct zc_psettab	in_prog_psettab;
static struct zc_mcaptab	old_mcaptab;
static struct zc_mcaptab	in_prog_mcaptab;

static GetLine *get_linep;	/* The gl_get_line() resource object */

/* Functions begin here */

static bool
initial_match(const char *line1, const char *line2, int word_end)
{
	if (word_end <= 0) {
		return (true);
	}
	return (strncmp(line1, line2, (unsigned int)word_end) == 0);
}

/*
 * Helper function for tab word completion.
 */
static int
add_stuff(WordCompletion *cpl, const char *line1, const char **list,
    int word_end)
{
	int i, err;

	for (i = 0; list[i] != NULL; i++) {
		if (initial_match(line1, list[i], word_end)) {
			err = cpl_add_completion(cpl, line1, 0, word_end,
			    list[i] + word_end, "", "");
			if (err != 0) {
				return (err);
			}
		}
	}
	return (0);
}

/*
 * Word completion function. When the user types in a few characters in the
 * interactive shell and hits the 'Tab' key, this function looks at the
 * possible commands in the current scope and either completes the command, or
 * suggest choices if the match is not unique.
 *
 * See libtecla documentation for more details on how the library works.
 */
static
/* ARGSUSED */
CPL_MATCH_FN(cmd_cpl_fn)
{
	if (global_scope) {
		/*
		 * The MAX/MIN tests below are to make sure we have at least
		 * enough characters to distinguish from other prefixes (MAX)
		 * but only check MIN(what we have, what we're checking).
		 */
		if (strncmp(line, "add ", MAX(MIN(word_end, 4), 1)) == 0) {
			return (add_stuff(cpl, line, add_cmds, word_end));
		}
		if (strncmp(line, "select ", MAX(MIN(word_end, 7), 3)) == 0) {
			return (add_stuff(cpl, line, select_cmds, word_end));
		}
		if (strncmp(line, "set ", MAX(MIN(word_end, 4), 3)) == 0) {
			return (add_stuff(cpl, line, set_cmds, word_end));
		}
		if (strncmp(line, "remove ", MAX(MIN(word_end, 7), 1)) == 0) {
			return (add_stuff(cpl, line, remove_cmds, word_end));
		}
		if (strncmp(line, "info ", MAX(MIN(word_end, 5), 1)) == 0) {
			return (add_stuff(cpl, line, info_cmds, word_end));
		}
		return (add_stuff(cpl, line, global_scope_cmds, word_end));
	}
	switch (resource_scope) {
	case RT_FS:
		return (add_stuff(cpl, line, fs_res_scope_cmds, word_end));
	case RT_IPD:
		return (add_stuff(cpl, line, ipd_res_scope_cmds, word_end));
	case RT_NET:
		return (add_stuff(cpl, line, net_res_scope_cmds, word_end));
	case RT_DEVICE:
		return (add_stuff(cpl, line, device_res_scope_cmds, word_end));
	case RT_RCTL:
		return (add_stuff(cpl, line, rctl_res_scope_cmds, word_end));
	case RT_ATTR:
		return (add_stuff(cpl, line, attr_res_scope_cmds, word_end));
	case RT_DATASET:
		return (add_stuff(cpl, line, dataset_res_scope_cmds, word_end));
	case RT_NODE:
		return (add_stuff(cpl, line, node_res_scope_cmds, word_end));
	case RT_SYSID:
		return (add_stuff(cpl, line, sysid_res_scope_cmds, word_end));
	case RT_DCPU:
		return (add_stuff(cpl, line, pset_res_scope_cmds, word_end));
	case RT_PCAP:
		return (add_stuff(cpl, line, pcap_res_scope_cmds, word_end));
	case RT_MCAP:
		return (add_stuff(cpl, line, mcap_res_scope_cmds, word_end));
	default:
		break;
	}
	return (0);
}

/*
 * For the main CMD_func() functions below, several of them call getopt()
 * then check optind against argc to make sure an extra parameter was not
 * passed in.  The reason this is not caught in the grammar is that the
 * grammar just checks for a miscellaneous TOKEN, which is *expected* to
 * be "-F" (for example), but could be anything.  So (for example) this
 * check will prevent "create bogus".
 */
cmd_t *
alloc_cmd(void)
{
	return (calloc(1, sizeof (cmd_t)));
}

void
free_cmd(cmd_t *cmdp)
{
	int i;

	for (i = 0; i < MAX_EQ_PROP_PAIRS; i++)
		if (cmdp->cmd_property_ptr[i] != NULL) {
			property_value_ptr_t pp = cmdp->cmd_property_ptr[i];

			switch (pp->pv_type) {
			case PROP_VAL_SIMPLE:
				free(pp->pv_simple);
				break;
			case PROP_VAL_COMPLEX:
				free_complex(pp->pv_complex);
				break;
			case PROP_VAL_LIST:
				free_list(pp->pv_list);
				break;
			default:
				break;
			}
		}
	for (i = 0; i < cmdp->cmd_argc; i++) {
		free(cmdp->cmd_argv[i]);
	}
	free(cmdp);
}

complex_property_ptr_t
alloc_complex(void)
{
	return (calloc(1, sizeof (complex_property_t)));
}

void
free_complex(complex_property_ptr_t complex)
{
	if (complex == NULL) {
		return;
	}
	free_complex(complex->cp_next);
	if (complex->cp_value != NULL) {
		free(complex->cp_value);
	}
	free(complex);
}

list_property_ptr_t
alloc_list(void)
{
	return (calloc(1, sizeof (list_property_t)));
}

void
free_list(list_property_ptr_t list)
{
	if (list == NULL) {
		return;
	}
	if (list->lp_simple != NULL) {
		free(list->lp_simple);
	}
	free_complex(list->lp_complex);
	free_list(list->lp_next);
	free(list);
}

void
free_outer_list(list_property_ptr_t list)
{
	if (list == NULL) {
		return;
	}
	free_outer_list(list->lp_next);
	free(list);
}

static struct zc_rctlvaltab *
alloc_rctlvaltab(void)
{
	return (calloc(1, sizeof (struct zc_rctlvaltab)));
}

/*
 * Convert from RT_* number to resource-type name string.
 */
static char *
rt_to_str(int res_type)
{
	assert(res_type >= RT_MIN && res_type <= RT_MAX);
	return (res_types[res_type]);
}

/*
 * Convert from PT_* number to property-type name string.
 */
static char *
pt_to_str(int prop_type)
{
	assert(prop_type >= PT_MIN && prop_type <= PT_MAX);
	return (prop_types[prop_type]);
}

/*
 * Convert from property value type to string.
 */
static char *
pvt_to_str(int pv_type)
{
	assert(pv_type >= PROP_VAL_MIN && pv_type <= PROP_VAL_MAX);
	return (prop_val_types[pv_type]); /*lint !e661 */
}

/*
 * Convert from command number to command string.
 */
static char *
cmd_to_str(uint_t cmd_num)
{
	assert(cmd_num <= CMD_MAX);
	return (helptab[cmd_num].cmd_name);
}

/*
 * This is a separate function rather than a set of define's because of the
 * gettext() wrapping.
 */

/*
 * TRANSLATION_NOTE
 * Each string below should have \t follow \n whenever needed; the
 * initial \t and the terminal \n will be provided by the calling function.
 */
static char *
long_help(uint_t cmd_num)
{
	static char line[1024];	/* arbitrary large amount */

	assert(cmd_num <= CMD_MAX);
	switch (cmd_num) {
		case CMD_HELP:
			return (gettext("Prints help message."));
		case CMD_CREATE:
			(void) snprintf(line, sizeof (line),
			    gettext("Creates a configuration for the "
			    "specified zone cluster. %s\n\tshould be used to "
			    "begin configuring a new zone cluster. "
			    "\n\tIf -t template is given, creates a "
			    "configuration\n\tidentical to the specified "
			    "template, except that the zone name is\n\tchanged"
			    " from template to zonename. '%s -b' results in a"
			    "\n\tblank configuration. '%s' with no arguments "
			    "applies the Sun Cluster\n\tdefault settings."),
			    cmd_to_str(CMD_CREATE), cmd_to_str(CMD_CREATE),
			    cmd_to_str(CMD_CREATE), cmd_to_str(CMD_CREATE));
			return (line);
		case CMD_EXIT:
			return (gettext("Exits the program.  The -F flag can "
			    "be used to force the action."));
		case CMD_EXPORT:
			return (gettext("Prints configuration to standard "
			    "output, or to output-file if\n\tspecified, in "
			    "a form suitable for use in a command-file."));
		case CMD_ADD:
			return (gettext("Add specified resource to "
			    "configuration."));
		case CMD_DELETE:
			return (gettext("Deletes the specified zone cluster. "
			    "The -F flag can be used to force \n\tthe "
			    "action."));
		case CMD_REMOVE:
			return (gettext("Remove specified resource from "
			    "configuration.  Note that the curly\n\tbraces "
			    "('{', '}') mean one or more of whatever "
			    "is between them."));
		case CMD_SELECT:
			(void) snprintf(line, sizeof (line),
			    gettext("Selects a resource to modify.  "
			    "Resource modification is completed\n\twith the "
			    "command \"%s\".  The property name/value pairs "
			    "must uniquely\n\tidentify a resource.  Note that "
			    "the curly braces ('{', '}') mean one\n\tor more "
			    "of whatever is between them."),
			    cmd_to_str(CMD_END));
			return (line);
		case CMD_SET:
			return (gettext("Sets property values."));
		case CMD_INFO:
			return (gettext("Displays information about the "
			    "current configuration.  If resource\n\ttype is "
			    "specified, displays only information about "
			    "resources of\n\tthe relevant type.  If resource "
			    "id is specified, displays only\n\tinformation "
			    "about that resource."));
		case CMD_VERIFY:
			return (gettext("Verifies current configuration "
			    "for correctness (some resource types\n\thave "
			    "required properties)."));
		case CMD_COMMIT:
			(void) snprintf(line, sizeof (line),
			    gettext("Commits current configuration.  "
			    "Configuration must be committed to\n\tbe used by "
			    "%s.  Until the configuration is committed, "
			    "changes \n\tcan be removed with the %s "
			    "command.  This operation is\n\tattempted "
			    "automatically upon completion of a %s\n\t"
			    "session."), "zoneadm", cmd_to_str(CMD_REVERT),
			    "clzonecluster configure");
			return (line);
		case CMD_REVERT:
			return (gettext("Reverts configuration back to the "
			    "last committed state.  The -F flag\n\tcan be "
			    "used to force the action."));
		case CMD_CANCEL:
			return (gettext("Cancels resource/property "
			    "specification."));
		case CMD_END:
			return (gettext("Ends resource/property "
			    "specification."));
		default:
			break;
	}
	/* NOTREACHED */
	return (NULL);
}

/*
 * Called with verbose true when help is explicitly requested, false for
 * unexpected errors.
 */
void
usage(bool verbose, uint_t flags)
{
	FILE *fp = verbose ? stdout : stderr;
	FILE *newfp;
	bool need_to_close = false;
	char *pagerp;
	uint_t i;

	/* don't page error output */
	if (verbose && interactive_mode) {
		if ((pagerp = getenv("PAGER")) == NULL) {
			pagerp = PAGER;
		}
		if ((newfp = popen(pagerp, "w")) != NULL) {
			need_to_close = true;
			fp = newfp;
		}
	}
	if (flags & HELP_META) {
		(void) fprintf(fp, gettext("More help is available for the "
		    "following:\n"));
		(void) fprintf(fp, "\n\tcommands ('%s commands')\n",
		    cmd_to_str(CMD_HELP));
		(void) fprintf(fp, "\tsyntax ('%s syntax')\n",
		    cmd_to_str(CMD_HELP));
		(void) fprintf(fp, "\tusage ('%s usage')\n\n",
		    cmd_to_str(CMD_HELP));
		(void) fprintf(fp, gettext("You can also obtain help on any "
		    "command by typing '%s <command-name>.'\n"),
		    cmd_to_str(CMD_HELP));
	}
	if (flags & HELP_RES_SCOPE) {
		switch (resource_scope) {
		case RT_FS:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to configure a file system.\n"),
			    rt_to_str(resource_scope));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_DIR), gettext("<path>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_SPECIAL), gettext("<path>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_RAW), gettext("<raw-device>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_TYPE), gettext("<file-system type>"));
			(void) fprintf(fp, "\t%s %s %s\n", cmd_to_str(CMD_ADD),
			    pt_to_str(PT_OPTIONS),
			    gettext("<file-system options>"));
			(void) fprintf(fp, "\t%s %s %s\n",
			    cmd_to_str(CMD_REMOVE), pt_to_str(PT_OPTIONS),
			    gettext("<file-system options>"));
			(void) fprintf(fp, gettext("Consult the file system "
			    "manual page, such as mount_ufs(1M), "
			    "for\ndetails about file system options.  Note "
			    "that any file system option with an\nembedded "
			    "'=' character must be enclosed in double quotes, "
			    /*CSTYLED*/
			    "such as \"%s=5\".\n"), MNTOPT_RETRY);
			break;
		case RT_IPD:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to configure a directory\ninherited from the "
			    "global zone into a non-global zone in read-only "
			    "mode.\n"), rt_to_str(resource_scope));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_DIR), gettext("<path>"));
			break;
		case RT_NET:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to configure a network interface.\n"),
			    rt_to_str(resource_scope));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_ADDRESS), gettext("<IP-address>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_PHYSICAL), gettext("<interface>"));
			(void) fprintf(fp, gettext("See ifconfig(1M) for "
			    "details of the <interface> string.\n"));

			if (defrouter_support) {
				(void) fprintf(fp, "\t%s %s=%s\n",
				    cmd_to_str(CMD_SET),
				    pt_to_str(PT_DEFROUTER),
				    gettext("<IP-address>"));
				(void) fprintf(fp, gettext("The %s "
				    "property cannot be set if %s is a "
				    "resource in 'global' scope.\nAlso, %s %s "
				    "is valid if the %s property is set to %s,"
				    "\notherwise it must not be set.\n"),
				    pt_to_str(PT_DEFROUTER),
				    rt_to_str(resource_scope),
				    cmd_to_str(CMD_SET),
				    pt_to_str(PT_DEFROUTER),
				    pt_to_str(PT_IPTYPE), "shared");
			}
			(void) fprintf(fp, gettext("If the %s "
			    "resource is a 'global' resource, you can only "
			    "specify the '%s' property.\nThe '%s' property is "
			    "automatically set\nto 'auto', telling the system "
			    "to dynamically choose its value at runtime.\n"),
			    rt_to_str(resource_scope),
			    pt_to_str(PT_ADDRESS), pt_to_str(PT_PHYSICAL));
			break;
		case RT_DEVICE:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to configure a device node.\n"),
			    rt_to_str(resource_scope));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_MATCH), gettext("<device-path>"));
			break;
		case RT_RCTL:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to configure a resource control.\n"),
			    rt_to_str(resource_scope));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_NAME), gettext("<string>"));
			(void) fprintf(fp, "\t%s %s (%s=%s,%s=%s,%s=%s)\n",
			    cmd_to_str(CMD_ADD), pt_to_str(PT_VALUE),
			    pt_to_str(PT_PRIV), gettext("<priv-value>"),
			    pt_to_str(PT_LIMIT), gettext("<number>"),
			    pt_to_str(PT_ACTION), gettext("<action-value>"));
			(void) fprintf(fp, "\t%s %s (%s=%s,%s=%s,%s=%s)\n",
			    cmd_to_str(CMD_REMOVE), pt_to_str(PT_VALUE),
			    pt_to_str(PT_PRIV), gettext("<priv-value>"),
			    pt_to_str(PT_LIMIT), gettext("<number>"),
			    pt_to_str(PT_ACTION), gettext("<action-value>"));
			(void) fprintf(fp, "%s\n\t%s := privileged\n"
			    "\t%s := none | deny\n", gettext("Where"),
			    gettext("<priv-value>"), gettext("<action-value>"));
			break;
		case RT_ATTR:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to configure a generic attribute.\n"),
			    rt_to_str(resource_scope));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_NAME), gettext("<name>"));
			(void) fprintf(fp, "\t%s %s=boolean\n",
			    cmd_to_str(CMD_SET), pt_to_str(PT_TYPE));
			(void) fprintf(fp, "\t%s %s=true | false\n",
			    cmd_to_str(CMD_SET), pt_to_str(PT_VALUE));
			(void) fprintf(fp, gettext("or\n"));
			(void) fprintf(fp, "\t%s %s=int\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_TYPE));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_VALUE), gettext("<integer>"));
			(void) fprintf(fp, gettext("or\n"));
			(void) fprintf(fp, "\t%s %s=string\n",
			    cmd_to_str(CMD_SET), pt_to_str(PT_TYPE));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_VALUE), gettext("<string>"));
			(void) fprintf(fp, gettext("or\n"));
			(void) fprintf(fp, "\t%s %s=uint\n",
			    cmd_to_str(CMD_SET), pt_to_str(PT_TYPE));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_VALUE), gettext("<unsigned integer>"));
			break;
		case RT_DATASET:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to export ZFS data sets.\n"),
			    rt_to_str(resource_scope));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_NAME), gettext("<name>"));
			break;
		case RT_NODE:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to configure those properties of a\n"
			    "zone cluster that are specific to a given "
			    "cluster node.\n"), rt_to_str(RT_NODE));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_PHYSICALHOST), gettext("<string>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_HOSTNAME), gettext("<string>"));
			(void) fprintf(fp, "\t%s %s\n", cmd_to_str(CMD_ADD),
			    rt_to_str(RT_NET));
			break;
		case RT_SYSID:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to configure the sysidcfg(4) parameters of\n"
			    "a zone cluster that are common across "
			    "the cluster.\n"), rt_to_str(RT_SYSID));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_ROOTPASSWD), gettext("<string>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_NAMESERVICE), gettext("<string>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_NFS4DOMAIN), gettext("<string>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_SECPOLICY), gettext("<string>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_SYSLOCALE), gettext("<string>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_TERMINAL), gettext("<string>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_TIMEZONE), gettext("<string>"));
			break;
		case RT_DCPU:
			(void) fprintf(fp, gettext("The '%s' resource scope "
			    "configures the 'pools' facility to dedicate\na "
			    "subset of the system's processors to this zone "
			    "cluster while it is running.\n"),
			    rt_to_str(resource_scope));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_NCPUS),
			    gettext("<unsigned integer | range>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_IMPORTANCE),
			    gettext("<unsigned integer>"));
			break;
		case RT_PCAP:
			(void) fprintf(fp, gettext("The '%s' resource scope is"
			    " used to set an upper limit (a cap) on the\n"
			    "percentage of CPU that can be used by this zone. "
			    " A '%s' value of 1\ncorresponds to one cpu.  The "
			    "value can be set higher than 1, up to the total\n"
			    "number of CPUs on the system.  The value can "
			    "also be less than 1,\nrepresenting a fraction of "
			    "a CPU.\n"),
			    rt_to_str(resource_scope), pt_to_str(PT_NCPUS));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_NCPUS), gettext("<unsigned decimal>"));
			    break;
		case RT_MCAP:
			(void) fprintf(fp, gettext("The '%s' resource scope is "
			    "used to set an upper limit (a cap) on the\n"
			    "amount of physical memory, swap space, and locked "
			    "memory that can be used by\nthis zone cluster.\n"),
			    rt_to_str(resource_scope));
			(void) fprintf(fp, gettext("Valid commands:\n"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_PHYSICAL),
			    gettext("<qualified unsigned decimal>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_SWAP),
			    gettext("<qualified unsigned decimal>"));
			(void) fprintf(fp, "\t%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_LOCKED),
			    gettext("<qualified unsigned decimal>"));
			break;
		default:
			break;
		}
		(void) fprintf(fp, gettext("From any resource scope, you "
		    "can:\n"));
		(void) fprintf(fp, "\t%s\t%s\n", cmd_to_str(CMD_END),
		    gettext("(to conclude this operation)"));
		(void) fprintf(fp, "\t%s\t%s\n", cmd_to_str(CMD_CANCEL),
		    gettext("(to cancel this operation)"));
		(void) fprintf(fp, "\t%s\t%s\n", cmd_to_str(CMD_EXIT),
		    gettext("(to exit the interactive shell)"));
	}
	if (flags & HELP_USAGE) {
		(void) fprintf(fp, "%s:\t%s %s\n", gettext("usage"),
		    execnamep, cmd_to_str(CMD_HELP));
		(void) fprintf(fp, "\t%s -z <zone-cluster>\t\t\t(%s)\n",
		    execnamep, gettext("interactive"));
		(void) fprintf(fp, "\t%s -z <zone-cluster> <command>\n",
		    execnamep);
		(void) fprintf(fp, "\t%s -z <zone-cluster> -f "
		    " <command-file>\n",
		    execnamep);
	}
	if (flags & HELP_SUBCMDS) {
		(void) fprintf(fp, "%s:\n\n", gettext("Commands"));
		for (i = 0; i <= CMD_MAX; i++) {
			(void) fprintf(fp, "%s\n", helptab[i].short_usage);
			if (verbose) {
				(void) fprintf(fp, "\t%s\n\n", long_help(i));
			}
		}
	}
	if (flags & HELP_SYNTAX) {
		if (!verbose) {
			(void) fprintf(fp, "\n");
		}
		(void) fprintf(fp, "<zone> := [A-Za-z0-9][A-Za-z0-9_.-]*\n");
		(void) fprintf(fp, gettext("\t(except the reserved words "
		    "'%s', anything starting with '%s',"), "global",
		    "SUNW");
		(void) fprintf(fp, gettext("\n\tthe base cluster name and "
		    "any base cluster nodename.)\n"));
		(void) fprintf(fp,
		    gettext("\tName must be less than %d characters.\n"),
		    ZONENAME_MAX);
		if (verbose) {
			(void) fprintf(fp, "\n");
		}
	}
	if (flags & HELP_NETADDR) {
		(void) fprintf(fp, gettext("\n<net-addr> :="));
		(void) fprintf(fp,
		    gettext("\t<IPv4-address>[/<IPv4-prefix-length>] |\n"));
		(void) fprintf(fp,
		    gettext("\t\t<IPv6-address>/<IPv6-prefix-length> |\n"));
		(void) fprintf(fp,
		    gettext("\t\t<hostname>[/<IPv4-prefix-length>]\n"));
		(void) fprintf(fp, gettext("See inet(3SOCKET) for IPv4 and "
		    "IPv6 address syntax.\n"));
		(void) fprintf(fp, gettext("<IPv4-prefix-length> := [0-32]\n"));
		(void) fprintf(fp,
		    gettext("<IPv6-prefix-length> := [0-128]\n"));
		(void) fprintf(fp,
		    gettext("<hostname> := [A-Za-z0-9][A-Za-z0-9-.]*\n"));
	}
	if (flags & HELP_RESOURCES) {
		(void) fprintf(fp, "<%s> := %s | %s | %s | %s | %s | %s |\n\t"
		    "%s | %s | %s | %s | %s\n\n",
		    gettext("resource type"), rt_to_str(RT_FS),
		    rt_to_str(RT_IPD), rt_to_str(RT_NET), rt_to_str(RT_DEVICE),
		    rt_to_str(RT_RCTL), rt_to_str(RT_ATTR),
		    rt_to_str(RT_DATASET), rt_to_str(RT_DCPU),
		    rt_to_str(RT_MCAP), rt_to_str(RT_SYSID),
		    rt_to_str(RT_NODE));
	}
	if (flags & HELP_PROPS) {
		(void) fprintf(fp, gettext("For resource type ... there are "
		    "property types ...:\n"));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_ZONENAME));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_ZONEPATH));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_BRAND));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_AUTOBOOT));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_BOOTARGS));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_POOL));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_LIMITPRIV));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_SCHED));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_IPTYPE));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_MAXLWPS));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_MAXSHMMEM));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_MAXSHMIDS));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_MAXMSGIDS));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_MAXSEMIDS));
		(void) fprintf(fp, "\t%s\t%s\n", gettext("(global)"),
		    pt_to_str(PT_SHARES));
		(void) fprintf(fp, "\t%s\t\t%s, %s, %s, %s, %s\n",
		    rt_to_str(RT_FS), pt_to_str(PT_DIR), pt_to_str(PT_SPECIAL),
		    pt_to_str(PT_RAW), pt_to_str(PT_TYPE),
		    pt_to_str(PT_OPTIONS));
		(void) fprintf(fp, "\t%s\t%s\n", rt_to_str(RT_IPD),
		    pt_to_str(PT_DIR));
		//
		// We include defrouter here, but defrouter is valid only
		// for node scope, not global scope. We have to do this
		// only when Base solaris version supports defrouter.
		//
		if (defrouter_support) {
			(void) fprintf(fp, "\t%s\t\t%s, %s\n",
			    rt_to_str(RT_NET), pt_to_str(PT_ADDRESS),
			    pt_to_str(PT_PHYSICAL), pt_to_str(PT_DEFROUTER));
			(void) fprintf(fp, "\t%s\t\t%s\n",
			    rt_to_str(RT_DEVICE), pt_to_str(PT_MATCH));
			(void) fprintf(fp, "\t%s\t\t%s, %s\n",
			    rt_to_str(RT_RCTL), pt_to_str(PT_NAME),
			    pt_to_str(PT_VALUE));
		}
		(void) fprintf(fp, "\t%s\t\t%s, %s, %s\n", rt_to_str(RT_ATTR),
		    pt_to_str(PT_NAME), pt_to_str(PT_TYPE),
		    pt_to_str(PT_VALUE));
		(void) fprintf(fp, "\t%s\t\t%s\n", rt_to_str(RT_DATASET),
		    pt_to_str(PT_NAME));
		(void) fprintf(fp, "\t%s\t%s, %s\n", rt_to_str(RT_DCPU),
		    pt_to_str(PT_NCPUS), pt_to_str(PT_IMPORTANCE));
		(void) fprintf(fp, "\t%s\t%s\n", rt_to_str(RT_PCAP),
		    pt_to_str(PT_NCPUS));
		(void) fprintf(fp, "\t%s\t%s, %s, %s\n", rt_to_str(RT_MCAP),
		    pt_to_str(PT_PHYSICAL), pt_to_str(PT_SWAP),
		    pt_to_str(PT_LOCKED));
		(void) fprintf(fp, "\t%s\t\t%s, %s, %s, \n\t\t\t%s, %s, %s,"
		    " %s\n", rt_to_str(RT_SYSID), pt_to_str(PT_ROOTPASSWD),
		    pt_to_str(PT_NAMESERVICE), pt_to_str(PT_NFS4DOMAIN),
		    pt_to_str(PT_SECPOLICY), pt_to_str(PT_SYSLOCALE),
		    pt_to_str(PT_TERMINAL), pt_to_str(PT_TIMEZONE));
		(void) fprintf(fp, "\t%s\t\t%s, %s\n", rt_to_str(RT_NODE),
		    pt_to_str(PT_PHYSICALHOST), pt_to_str(PT_HOSTNAME));
	}
	if (need_to_close) {
		(void) pclose(fp);
	}
}

/* PRINTFLIKE1 */
static void
zerr(const char *fmt, ...)
{
	va_list alist;
	static int last_lineno = 0;

	/* lex_lineno has already been incremented in the lexer; compensate */
	if (cmd_file_mode && lex_lineno > last_lineno) {
		if (strcmp(cmd_file_namep, "-") == 0) {
			(void) fprintf(stderr, gettext("On line %d:\n"),
			    lex_lineno - 1);
		} else {
			(void) fprintf(stderr, gettext("On line %d of %s:\n"),
			    lex_lineno - 1, cmd_file_namep);
		}
		last_lineno = lex_lineno;
	}
	va_start(alist, fmt);
	(void) vfprintf(stderr, fmt, alist);
	(void) fprintf(stderr, "\n");
	va_end(alist);
}

static void
zone_perror(char *prefix, int err, bool set_saw)
{
	zerr("%s: %s", prefix, zccfg_strerror(err));
	if (set_saw) {
		saw_error = true;
	}
}

/*
 * zone_perror() expects a single string, but for remove and select
 * we have both the command and the resource type, so this wrapper
 * function serves the same purpose in a slightly different way.
 */
static void
z_cmd_rt_perror(uint_t cmd_num, int res_num, int err, bool set_saw)
{
	zerr("%s %s: %s", cmd_to_str(cmd_num), rt_to_str(res_num),
	    zccfg_strerror(err));
	if (set_saw) {
		saw_error = true;
	}
}

/*
 * Ensure that the zone cluster name is legit. The check is the same as that
 * used in Solaris, with three exceptions/additions:
 *
 * - "global" is invalid
 * - zone cluster name cannot be the same as that of the base cluster
 * - zone cluster name cannot be the same as a base cluster node name
 *
 * So if "phys-cluster-1" is a base cluster node name, then we can't have a
 * zone cluster called "phys-cluster-1".
 */
static int
validate_zc_name(const char *zonename)
{
	char *cluster_name;
	scstat_node_t *nodesp, *tmp;
	scstat_errno_t err;

	if (strcmp(zonename, GLOBAL_ZONENAME) == 0) {
		return (ZC_BOGUS_ZONE_NAME);
	}

	if (strlen(zonename) >= ZONENAME_MAX) {
		return (ZC_BOGUS_ZONE_NAME);
	}

	cluster_name = clconf_get_clustername();
	assert(cluster_name != NULL);
	if (cluster_name == NULL) {
		zerr(gettext("Unexpected error while determining cluster "
		    "name."));
		return (ZC_ERR);
	}
	if (strcmp(zonename, cluster_name) == 0) {
		free(cluster_name);
		return (ZC_BOGUS_ZONE_NAME);
	}
	free(cluster_name);

	err = scstat_get_nodes(&nodesp);
	if (err != SCSTAT_ENOERR) {
		zerr(gettext("Unexpected error while validating zone cluster"
		    " name."));
		return (ZC_ERR);
	}

	for (tmp = nodesp; tmp; tmp = tmp->scstat_node_next) {
		if (strcmp(zonename, tmp->scstat_node_name) == 0) {
			scstat_free_nodes(nodesp);
			return (ZC_BOGUS_ZONE_NAME);
		}
	}

	tmp = NULL;
	if (nodesp) {
		scstat_free_nodes(nodesp);
	}

	return (zccfg_validate_zonename(zonename));
}

/*
 * Convenience function. Returns "true" if state of zone cluster is at least
 * equal to "state".
 */
static bool
state_atleast(zone_cluster_state_t state)
{
	zone_cluster_state_t max_state = ZC_STATE_UNKNOWN;
	int err;

	/* A small optimization: a new zone cluster has no state */
	if (new_zone_cluster) {
		return (false);
	}

	/*
	 * Existing zone cluster. Get state of zone cluster on each node.
	 * The state of the zone cluster is the highest state of a zone member
	 * on any node.
	 */
	if ((err = get_zone_cluster_state(zone, &max_state)) != ZC_OK) {
		zerr(gettext("Unexpectedly failed to determine state "
		    "of zone cluster %s: %s"), zone, zccfg_strerror(err));
		exit(ZC_ERR);
	}
	return (max_state >= state);
}

/*
 * short_usage() is for bad syntax: getopt() issues, too many arguments, etc.
 */
void
short_usage(int command)
{
	/* lex_lineno has already been incremented in the lexer; compensate */
	if (cmd_file_mode) {
		if (strcmp(cmd_file_namep, "-") == 0) {
			(void) fprintf(stderr,
			    gettext("syntax error on line %d\n"),
			    lex_lineno - 1);
		} else {
			(void) fprintf(stderr,
			    gettext("syntax error on line %d of %s\n"),
			    lex_lineno - 1, cmd_file_namep);
		}
	}
	(void) fprintf(stderr, "%s:\n%s\n", gettext("usage"),
	    helptab[command].short_usage);
	saw_error = true;
}

/*
 * long_usage() is for bad semantics: e.g., wrong property type for a given
 * resource type.  It is also used by longer_usage() below.
 */
static void
long_usage(uint_t cmd_num, bool set_saw)
{
	(void) fprintf(set_saw ? stderr : stdout, "%s:\n%s\n", gettext("usage"),
	    helptab[cmd_num].short_usage);
	(void) fprintf(set_saw ? stderr : stdout, "\t%s\n", long_help(cmd_num));
	if (set_saw) {
		saw_error = true;
	}
}

/*
 * longer_usage() is for 'help foo' and 'foo -?': call long_usage() and also
 * any extra usage() flags as appropriate for whatever command.
 */
static void
longer_usage(uint_t cmd_num)
{
	/* lint thinks "true/false" is int */
	long_usage(cmd_num, false); /*lint !e747 */
	if (helptab[cmd_num].flags != 0) {
		(void) printf("\n");
		usage(true, helptab[cmd_num].flags); /*lint !e747 */
	}
}

/*
 * scope_usage() is simply used when a command is called from the wrong scope.
 */
static void
scope_usage(uint_t cmd_num)
{
	zerr(gettext("The %s command only makes sense in the %s scope."),
	    cmd_to_str(cmd_num),
	    global_scope ?  gettext("resource") : gettext("global"));
	saw_error = true;
}

/*
 * A utility function to prompt for "yes/no" answer from the user. If
 * default_answer is true, "yes" is the default answer; else "no" is the
 * default answer.
 * On return, true => 1, false => no, could not ask => -1.
 */
static int
ask_yesno(bool default_answer, const char *question)
{
	char line[64];	/* should be enough to answer yes or no */

	if (!ok_to_prompt) {
		saw_error = true;
		return (-1);
	}
	for (;;) {
		if (printf("%s (%s)? ", question,
		    default_answer ? "[y]/n" : "y/[n]") < 0) {
			return (-1);
		}
		if (fgets(line, sizeof (line), stdin) == NULL) {
			return (-1);
		}
		if (line[0] == '\n') {
			return (default_answer ? 1 : 0);
		}
		if (tolower(line[0]) == 'y') {
			return (1);
		}
		if (tolower(line[0]) == 'n') {
			return (0);
		}
	}
}

/*
 * Searches for 'zonename' in a string of the form 'nodename:zonename'.
 * Returns true if the zonename is found, else returns false.
 */
static bool
parse_nodelist(const scha_str_array_t *nodelist)
{
	char **lnip;

	if (!nodelist) {
		return (false);
	}

	lnip = nodelist->str_array;
	while (lnip && *lnip) {
		if (strstr((const char *)*lnip, zone)) {
			return (true);
		}
		lnip++;
	}
	return (false);
}

/*
 * This function checks whether or not the zone cluster name is already
 * being used in the nodelist of RG's in the global zone.
 *
 * Returns true if name is already used, false otherwise.
 */
static bool
zone_is_in_rg_nodelist(void)
{
	scha_errmsg_t		rgm_status = {SCHA_ERR_NOERR, NULL};
	scha_err_t		err;
	char			**managed_rg_names = 0;
	char			**unmanaged_rg_names = 0;
	char			**rg_namep;
	scha_resourcegroup_t	rg_handle = NULL;
	scha_str_array_t	*nodelist = NULL;
	bool			retval = false;

	/* Get all RGs. */
	rgm_status = rgm_scrgadm_getrglist(&managed_rg_names,
	    &unmanaged_rg_names, (boolean_t)false, NULL);
	assert(rgm_status.err_code == SCHA_ERR_NOERR);
	if (rgm_status.err_code != SCHA_ERR_NOERR) {
		zerr(gettext("Unexpected error from rgm_scrgadm_getrglist"));
		exit(ZC_ERR);
	}

	/* Check managed RG's. */
	for (rg_namep = managed_rg_names; rg_namep && *rg_namep;
	    ++rg_namep) {
		err = scha_resourcegroup_open(*rg_namep, &rg_handle);
		assert(err == SCHA_ERR_NOERR);
		if (err != SCHA_ERR_NOERR) {
			zerr(gettext("Unexpected error from "
			    "scha_resourcegroup_open"));
			exit(ZC_ERR);
		}
		err = scha_resourcegroup_get(rg_handle, SCHA_NODELIST,
		    &nodelist);
		assert(err == SCHA_ERR_NOERR);
		if (err != SCHA_ERR_NOERR) {
			zerr(gettext("Unexpected error from "
			    "scha_resourcegroup_get"));
			exit(ZC_ERR);
		}
		if (parse_nodelist(nodelist)) {
			zerr(gettext("Warning: %s is already in the nodelist "
			    "of %s."), zone, *rg_namep);
			retval = true;
		}
	}

	/* Check unmanaged RG's. */
	for (rg_namep = unmanaged_rg_names; rg_namep && *rg_namep;
	    ++rg_namep) {
		err = scha_resourcegroup_open(*rg_namep, &rg_handle);
		assert(err == SCHA_ERR_NOERR);
		if (err != SCHA_ERR_NOERR) {
			zerr(gettext("Unexpected error from "
			    "scha_resourcegroup_open"));
			exit(ZC_ERR);
		}
		err = scha_resourcegroup_get(rg_handle, SCHA_NODELIST,
		    &nodelist);
		assert(err == SCHA_ERR_NOERR);
		if (err != SCHA_ERR_NOERR) {
			zerr(gettext("Unexpected error from "
			    "scha_resourcegroup_get"));
			exit(ZC_ERR);
		}
		if (parse_nodelist(nodelist)) {
			zerr(gettext("Warning: %s is already in the nodelist "
			    "of %s."), zone, *rg_namep);
			retval = true;
		}
	}

	if (managed_rg_names) {
		rgm_free_strarray(managed_rg_names);
	}
	if (unmanaged_rg_names) {
		rgm_free_strarray(unmanaged_rg_names);
	}
	(void) scha_resourcegroup_close(rg_handle);
	return (retval);
}

/*
 * Prints warning if Solaris zone already exists.
 *
 * There are a few things to consider:
 *
 * 1. A zone cluster of the same name exists.
 * 2. There is a Solaris zone of the same name in the cluster.
 * 3. No zone of the same name exists, but there is a resource group that has
 *    this zone name in its nodelist.
 *
 * We will exit with ZC_ERR in all the three cases.
 */
static int
check_if_zone_already_exists(void)
{
	scstat_node_t 	*nodesp, *tmp;
	scstat_errno_t 	err;
	char		cmd_buf[MAXPATHLEN];
	nodeid_t	nodeid;
	int		retval = ZC_OK;
	int		remote_val; /* Remote command status. */
	zc_dochandle_t	tmphandle;
	bool		zone_exists = false;

	if ((tmphandle = zccfg_init_handle()) == NULL) {
		zone_perror(execnamep, ZC_NOMEM, true); /*lint !e747 */
		exit(ZC_ERR);
	}

	retval = zccfg_get_handle(zone, tmphandle);
	zccfg_fini_handle(tmphandle);
	if (retval == ZC_OK) {
		zerr(gettext("Zone cluster %s already exists. Ignoring %s "
		    "command."), zone, cmd_to_str(CMD_CREATE));
		exit(ZC_ERR);
	}

	/*
	 * Now check whether or not a Solaris zone of the same name exists
	 * some where in the cluster.
	 */
	err = scstat_get_nodes(&nodesp);
	if (err != SCSTAT_ENOERR) {
		zerr(gettext("Unexpected error while checking if zone cluster"
		    " already exists."));
		exit(ZC_ERR);
	}

	(void) snprintf(cmd_buf, sizeof (cmd_buf), "%s %s exists",
	    ZC_HELPER, zone);
	for (tmp = nodesp; tmp; tmp = tmp->scstat_node_next) {
		/* Skip node if not in the cluster. */
		if (tmp->scstat_node_status != SCSTAT_ONLINE) {
			continue;
		}

		nodeid = clconf_cluster_get_nodeid_by_nodename(
		    tmp->scstat_node_name);
		remote_val = zccfg_execute_cmd(nodeid, cmd_buf);
		if (remote_val == ZC_OK) {
			zerr(gettext("Solaris zone %s already exists "
			    "on node %s."), zone,
			    tmp->scstat_node_name);
			zone_exists = true;
		}
	}

	tmp = NULL;
	if (nodesp) {
		free(nodesp);
	}
	if (zone_exists) {
		zerr(gettext("%s command not allowed."),
		    cmd_to_str(CMD_CREATE));
		exit(ZC_ERR);
	}

	/*
	 * The global zone RGM allows creation of an RG in which the
	 * nodelist may contain a zone that is yet to be configured. Make
	 * sure the zone cluster name is not already used in such a nodelist.
	 */
	zone_exists = zone_is_in_rg_nodelist();

	if (zone_exists) {
		zerr(gettext("%s command not allowed."),
		    cmd_to_str(CMD_CREATE));
		exit(ZC_ERR);
	}
	return (ZC_OK);
}

static bool
zc_is_read_only(uint_t cmd_num)
{
	if (strncmp(zone, "SUNW", 4) == 0) {
		zerr(gettext("%s: zone clusters beginning with SUNW are "
		    "read-only."), zone);
		saw_error = true;
		return (true);
	}
	if (read_only_mode) {
		zerr(gettext("%s: cannot %s in read-only mode."), zone,
		    cmd_to_str(cmd_num));
		saw_error = true;
		return (true);
	}
	return (false);
}

/*
 * Create a new zone cluster configuration. It's an error to "create" an
 * existing zone cluster, so we guard against this.
 */
void
create_func(cmd_t *cmdp)
{
	int err, arg;
	char zone_template[ZONENAME_MAX];
	zc_dochandle_t tmphandle;

	assert(cmdp != NULL);

	/* This is the default if no arguments are given. */
	(void) strlcpy(zone_template, "SUNWcldefault", sizeof (zone_template));

	optind = 0;
	/*
	 * We support the -b and -t arguments: -b to create a 'blank'
	 * configuration, and -t to create from an existing zone cluster.
	 * Note that we don't support the -a and -F options supported by
	 * zonecfg(1M).
	 */
	while ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?bt:"))
	    != EOF) {
		switch (arg) {
		case '?':
			if (optopt == '?')
				longer_usage(CMD_CREATE);
			else
				short_usage(CMD_CREATE);
			return;
		case 'b':
			(void) strlcpy(zone_template, "SUNWclblank",
			    sizeof (zone_template));
			break;
		case 't':
			(void) strlcpy(zone_template, optarg,
			    sizeof (zone_template));
			break;
		default:
			short_usage(CMD_CREATE);
			return;
		}
	}
	if (optind != cmdp->cmd_argc) {
		short_usage(CMD_CREATE);
		return;
	}

	if (zc_is_read_only(CMD_CREATE)) {
		return;
	}
	if (check_if_zone_already_exists() != ZC_OK) {
		return;
	}

	/*
	 * Get a temporary handle first.  If that fails, the old handle
	 * will not be lost.  Then finish whichever one we don't need,
	 * to avoid leaks.  Then get the handle for zone_template, and
	 * set the name to zone: this "copy, rename" method is how
	 * create -[b|t] works.
	 */
	if ((tmphandle = zccfg_init_handle()) == NULL) {
		zone_perror(execnamep, ZC_NOMEM, true); /*lint !e747 */
		exit(ZC_ERR);
	}

	err = zccfg_get_template_handle(zone_template, zone, tmphandle);

	if (err != ZC_OK) {
		zccfg_fini_handle(tmphandle);
		zone_perror(zone_template, err, true); /*lint !e747 */
		return;
	}

	need_to_commit = true;
	zccfg_fini_handle(handle);
	handle = tmphandle;
	got_handle = true;
	new_zone_cluster = true;
}

/*
 * This malloc()'s memory, which must be freed by the caller.
 */
static char *
quoteit(char *instrp)
{
	char *outstrp;
	size_t outstrsize = strlen(instrp) + 3;	/* 2 quotes + '\0' */

	if ((outstrp = malloc(outstrsize)) == NULL) {
		zone_perror(zone, ZC_NOMEM, false); /*lint !e747 */
		exit(ZC_ERR);
	}
	if (strchr(instrp, ' ') == NULL) {
		(void) strlcpy(outstrp, instrp, outstrsize);
		return (outstrp);
	}
	(void) snprintf(outstrp, outstrsize, "\"%s\"", instrp);
	return (outstrp);
}

static void
export_prop(FILE *ofp, int prop_num, char *prop_idp)
{
	char *quote_strp;

	if (strlen(prop_idp) == 0) {
		return;
	}
	quote_strp = quoteit(prop_idp);
	(void) fprintf(ofp, "%s %s=%s\n", cmd_to_str(CMD_SET),
	    pt_to_str(prop_num), quote_strp);
	free(quote_strp);
}

void
export_func(cmd_t *cmdp)
{
	struct zc_nwiftab nwiftab;
	struct zc_fstab fstab;
	struct zc_devtab devtab;
	struct zc_attrtab attrtab;
	struct zc_rctltab rctltab;
	struct zc_dstab dstab;
	struct zc_psettab psettab;
	struct zc_mcaptab mcaptab;
	struct zc_rctlvaltab *valptr;
	struct zc_nodetab nodetab;
	struct zc_sysidtab sysid_lookup;
	zc_nwifelem_t *nwif_listp;
	int err, arg;
	char zonepath[MAXPATHLEN], outfile[MAXPATHLEN], pool[MAXNAMELEN];
	char bootargs[BOOTARGS_MAX];
	char sched[MAXNAMELEN];
	char brand[MAXNAMELEN];
	char *limitpriv;
	FILE *of;
	bool autoboot;
	bool privateip;
	zc_iptype_t iptype;
	boolean_t need_to_close = false;
	boolean_t arg_err = false;

	assert(cmdp != NULL);

	outfile[0] = '\0';
	optind = 0;
	while ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?f:")) != EOF) {
		switch (arg) {
		case '?':
			if (optopt == '?') {
				longer_usage(CMD_EXPORT);
			} else {
				short_usage(CMD_EXPORT);
			}
			arg_err = true;
			break;
		case 'f':
			(void) strlcpy(outfile, optarg, sizeof (outfile));
			break;
		default:
			short_usage(CMD_EXPORT);
			arg_err = true;
			break;
		}
	}
	if (arg_err) {
		return;
	}
	if (optind != cmdp->cmd_argc) {
		short_usage(CMD_EXPORT);
		return;
	}
	if (strlen(outfile) == 0) {
		of = stdout;
	} else {
		if ((of = fopen(outfile, "w")) == NULL) {
			zerr(gettext("opening file %s: %s"),
			    outfile, strerror(errno));
			goto done;
		}
		setbuf(of, NULL);
		need_to_close = true;
	}

	if ((err = initialize(true)) != ZC_OK) {
		goto done;
	}
	(void) fprintf(of, "%s -b\n", cmd_to_str(CMD_CREATE));

	if (zccfg_get_zonepath(handle, zonepath, sizeof (zonepath)) == ZC_OK &&
	    strlen(zonepath) > 0) {
		(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
		    pt_to_str(PT_ZONEPATH), zonepath);
	}

	if ((zccfg_get_brand(handle, brand, sizeof (brand)) == ZC_OK))
		(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
		    pt_to_str(PT_BRAND), brand);

	if (zccfg_get_autoboot(handle, &autoboot) == ZC_OK) {
		(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
		    pt_to_str(PT_AUTOBOOT), autoboot ? "true" : "false");
	}

	if (zccfg_get_bootargs(handle, bootargs, sizeof (bootargs)) == ZC_OK &&
	    strlen(bootargs) > 0) {
		(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
		    pt_to_str(PT_BOOTARGS), bootargs);
	}

	if (zccfg_get_pool(handle, pool, sizeof (pool)) == ZC_OK &&
	    strlen(pool) > 0) {
		(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
		    pt_to_str(PT_POOL), pool);
	}

	if (zccfg_get_limitpriv(handle, &limitpriv) == ZC_OK &&
	    strlen(limitpriv) > 0) {
		(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
		    pt_to_str(PT_LIMITPRIV), limitpriv);
		free(limitpriv);
	}

	if (zccfg_get_sched_class(handle, sched, sizeof (sched)) == ZC_OK &&
	    strlen(sched) > 0) {
		(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
		    pt_to_str(PT_SCHED), sched);
	}

	/* Get clprivnet */
	if ((err = zccfg_get_clprivnet(handle, &privateip)) == ZC_OK) {
		if (privateip) {
			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
				pt_to_str(PT_CLPRIVNET), "true");
		} else {
			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
				pt_to_str(PT_CLPRIVNET), "false");
		}
	}

	if (zccfg_get_iptype(handle, &iptype) == ZC_OK) {
		switch (iptype) {
		case CZS_SHARED:
			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_IPTYPE), "shared");
			break;
		case CZS_EXCLUSIVE:
			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_IPTYPE), "exclusive");
			break;
		}
	}

	if ((err = zccfg_setipdent(handle)) != ZC_OK) {
		zone_perror(zone, err, false);
		goto done;
	}
	while (zccfg_getipdent(handle, &fstab) == ZC_OK) {
		(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
		    rt_to_str(RT_IPD));
		export_prop(of, PT_DIR, fstab.zc_fs_dir);
		(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
	}
	(void) zccfg_endipdent(handle);

	if ((err = zccfg_setfsent(handle)) != ZC_OK) {
		zone_perror(zone, err, false);
		goto done;
	}
	while (zccfg_getfsent(handle, &fstab) == ZC_OK) {
		zc_fsopt_t *optptr;

		(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
		    rt_to_str(RT_FS));
		export_prop(of, PT_DIR, fstab.zc_fs_dir);
		export_prop(of, PT_SPECIAL, fstab.zc_fs_special);
		export_prop(of, PT_RAW, fstab.zc_fs_raw);
		export_prop(of, PT_TYPE, fstab.zc_fs_type);
		for (optptr = fstab.zc_fs_options; optptr != NULL;
		    optptr = optptr->zc_fsopt_next) {
			/*
			 * Simple property values with embedded equal signs
			 * need to be quoted to prevent the lexer from
			 * mis-parsing them as complex name=value pairs.
			 */
			if (strchr(optptr->zc_fsopt_opt, '='))
				(void) fprintf(of, "%s %s \"%s\"\n",
				    cmd_to_str(CMD_ADD),
				    pt_to_str(PT_OPTIONS),
				    optptr->zc_fsopt_opt);
			else
				(void) fprintf(of, "%s %s %s\n",
				    cmd_to_str(CMD_ADD),
				    pt_to_str(PT_OPTIONS),
				    optptr->zc_fsopt_opt);
		}
		(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
		zccfg_free_fs_option_list(fstab.zc_fs_options);
	}
	(void) zccfg_endfsent(handle);

	if ((err = zccfg_setnwifent(handle)) != ZC_OK) {
		zone_perror(zone, err, false);
		goto done;
	}
	while (zccfg_getnwifent(handle, &nwiftab) == ZC_OK) {
		(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
		    rt_to_str(RT_NET));
		export_prop(of, PT_ADDRESS, nwiftab.zc_nwif_address);
		export_prop(of, PT_PHYSICAL, nwiftab.zc_nwif_physical);
		export_prop(of, PT_DEFROUTER, nwiftab.zc_nwif_defrouter);
		(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
	}
	(void) zccfg_endnwifent(handle);

	if ((err = zccfg_setdevent(handle)) != ZC_OK) {
		zone_perror(zone, err, false);
		goto done;
	}
	while (zccfg_getdevent(handle, &devtab) == ZC_OK) {
		(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
		    rt_to_str(RT_DEVICE));
		export_prop(of, PT_MATCH, devtab.zc_dev_match);
		(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
	}
	(void) zccfg_enddevent(handle);

	if ((err = zccfg_setdsent(handle)) != ZC_OK) {
		zone_perror(zone, err, false);
		goto done;
	}
	while (zccfg_getdsent(handle, &dstab) == ZC_OK) {
		(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
		    rt_to_str(RT_DATASET));
		export_prop(of, PT_NAME, dstab.zc_dataset_name);
		(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
	}
	(void) zccfg_enddsent(handle);

	if (zccfg_getpsetent(handle, &psettab) == ZC_OK) {
		(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
		    rt_to_str(RT_DCPU));
		if (strcmp(psettab.zc_ncpu_min, psettab.zc_ncpu_max) == 0) {
			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_NCPUS), psettab.zc_ncpu_max);
		} else {
			(void) fprintf(of, "%s %s=%s-%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_NCPUS), psettab.zc_ncpu_min,
			    psettab.zc_ncpu_max);
		}
		if (psettab.zc_importance[0] != '\0') {
			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
			    pt_to_str(PT_IMPORTANCE), psettab.zc_importance);
		}
		(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
	}

	if (zccfg_getmcapent(handle, &mcaptab) == ZC_OK) {
		char buf[128];
		(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
		    rt_to_str(RT_MCAP));
		/* set physical */
		bytes_to_units(mcaptab.zc_physmem_cap, buf, sizeof (buf));
		(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
		    pt_to_str(PT_PHYSICAL), buf);
		(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
	}

	/* Get rctl */
	if ((err = zccfg_setrctlent(handle)) != ZC_OK) {
		zone_perror(zone, err, false);
		goto done;
	}
	while (zccfg_getrctlent(handle, &rctltab) == ZC_OK) {
		(void) fprintf(of, "%s rctl\n", cmd_to_str(CMD_ADD));
		export_prop(of, PT_NAME, rctltab.zc_rctl_name);
		for (valptr = rctltab.zc_rctl_valp; valptr != NULL;
		    valptr = valptr->zc_rctlval_next) {
			(void) fprintf(of, "%s %s (%s=%s,%s=%s,%s=%s)\n",
			    cmd_to_str(CMD_ADD), pt_to_str(PT_VALUE),
			    pt_to_str(PT_PRIV), valptr->zc_rctlval_priv,
			    pt_to_str(PT_LIMIT), valptr->zc_rctlval_limit,
			    pt_to_str(PT_ACTION), valptr->zc_rctlval_action);
		}
		(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
		zccfg_free_rctl_value_list(rctltab.zc_rctl_valp);
	}
	(void) zccfg_endrctlent(handle);

	if ((err = zccfg_setattrent(handle)) != ZC_OK) {
		zone_perror(zone, err, false);
		goto done;
	}
	while (zccfg_getattrent(handle, &attrtab) == ZC_OK) {
		(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
		    rt_to_str(RT_ATTR));
		export_prop(of, PT_NAME, attrtab.zc_attr_name);
		export_prop(of, PT_TYPE, attrtab.zc_attr_type);
		export_prop(of, PT_VALUE, attrtab.zc_attr_value);
		(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
	}
	(void) zccfg_endattrent(handle);

	/*
	 * There is nothing to export for pcap since this resource is just
	 * a container for an rctl alias.
	 */

	/* Add the per-node based properties to be output */

	if (zccfg_setnodeent(handle) == ZC_OK) {
		while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
			/*
			 * For each node add the following
			 * 1. node  (physical, hostname)
			 * 2. net (address, physical)
			 * add node
			 */
			(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
				rt_to_str(RT_NODE));

			/* set physical host */
			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
				pt_to_str(PT_PHYSICALHOST),
				nodetab.zc_nodename);
			/* set hostname */
			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
				pt_to_str(PT_HOSTNAME),
				nodetab.zc_hostname);

			nwif_listp = nodetab.zc_nwif_listp;
			while (nwif_listp != NULL) {
				(void) fprintf(of, "%s %s\n",
				    cmd_to_str(CMD_ADD), rt_to_str(RT_NET));

				(void) fprintf(of, "%s %s=%s\n",
					cmd_to_str(CMD_SET),
					pt_to_str(PT_ADDRESS),
					nwif_listp->elem.zc_nwif_address);

				(void) fprintf(of, "%s %s=%s\n",
					cmd_to_str(CMD_SET),
					pt_to_str(PT_PHYSICAL),
					nwif_listp->elem.zc_nwif_physical);

				if (strlen(nwif_listp->elem.zc_nwif_defrouter)
				    > 0) {
					(void) fprintf(of, "%s %s=%s\n",
					    cmd_to_str(CMD_SET),
					    pt_to_str(PT_DEFROUTER),
					    nwif_listp->elem.zc_nwif_defrouter);
				}

				nwif_listp = nwif_listp->next;
				(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
			}
			/*
			 * TODO: Add fs, dev, dataset
			 * No implementation available
			 */
			(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));

		}

		(void) zccfg_endnodeent(handle);
	}

	if (zccfg_setsysident(handle) == ZC_OK) {
		while (zccfg_getsysident(handle, &sysid_lookup) == ZC_OK) {
			(void) fprintf(of, "%s %s\n", cmd_to_str(CMD_ADD),
				rt_to_str(RT_SYSID));

			struct passwd *pwp = getpwuid(getuid());
			assert(pwp);

			/*
			 * Export the encrypted root password only if the user
			 * has the solaris.cluster.admin RBAC profile.
			 */

			if (chkauthattr("solaris.cluster.admin",
				(const char *)pwp->pw_name) == 1) {
				(void) fprintf(of, "%s %s=%s\n",
					cmd_to_str(CMD_SET),
					pt_to_str(PT_ROOTPASSWD),
					sysid_lookup.zc_root_password);
			}

			(void) fprintf(of, "%s %s=\"%s\"\n",
				cmd_to_str(CMD_SET),
				pt_to_str(PT_NAMESERVICE),
				sysid_lookup.zc_name_service);

			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
				pt_to_str(PT_NFS4DOMAIN),
				sysid_lookup.zc_nfs4_domain);

			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
				pt_to_str(PT_SECPOLICY),
				sysid_lookup.zc_sec_policy);

			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
				pt_to_str(PT_SYSLOCALE),
				sysid_lookup.zc_sys_locale);

			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
				pt_to_str(PT_TERMINAL),
				sysid_lookup.zc_terminal);

			(void) fprintf(of, "%s %s=%s\n", cmd_to_str(CMD_SET),
				pt_to_str(PT_TIMEZONE),
				sysid_lookup.zc_timezone);

			(void) fprintf(of, "%s\n", cmd_to_str(CMD_END));
		}

		(void) zccfg_endsysident(handle);
	}

done:
	if (need_to_close && (of != NULL)) {
		(void) fclose(of);
	}
}

void
exit_func(cmd_t *cmdp)
{
	int arg;
	int answer;

	optind = 0;
	while ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?F")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_EXIT);
			return;
		case 'F':
			force_exit = true;
			break;
		default:
			short_usage(CMD_EXIT);
			return;
		}
	}
	if (optind < cmdp->cmd_argc) {
		short_usage(CMD_EXIT);
		return;
	}

	if (global_scope || force_exit) {
		time_to_exit = true;
		return;
	}

	answer = ask_yesno(false, /*lint !e747 */
	    "Resource incomplete; really quit");
	if (answer == -1) {
		zerr(gettext("Resource incomplete, input "
		    "not from terminal and -F not specified:\n%s command "
		    "ignored, but exiting anyway."), cmd_to_str(CMD_EXIT));
		exit(ZC_ERR);
	} else if (answer == 1) {
		time_to_exit = true;
	}
	/* (answer == 0) => just return */
}

static int
validate_zonepath_syntax(char *path)
{
	if (path[0] != '/') {
		zerr(gettext("%s is not an absolute path."), path);
		return (ZC_ERR);
	}
	if (strcmp(path, "/") == 0) {
		zerr(gettext("/ is not allowed as a %s."),
		    pt_to_str(PT_ZONEPATH));
		return (ZC_ERR);
	}
	return (ZC_OK);
}

static void
add_resource(cmd_t *cmdp)
{
	int type;
	struct zc_mcaptab tmp_mcaptab;
	struct zc_psettab tmp_psettab;
	uint64_t tmp;
	uint64_t tmp_mcap;
	char pool[MAXNAMELEN];

	if ((type = cmdp->cmd_res_type) == RT_UNKNOWN) {
		long_usage(CMD_ADD, true); /*lint !e747 */
		goto bad;
	}

	switch (type) {
	case RT_FS:
		/* Can't add fs in node scope yet */
		if (node_scope == true) {
			zerr(gettext("Adding %s resource in the %s scope is "
			    "not supported."),
			    rt_to_str(RT_FS), rt_to_str(RT_NODE));
			break;
		}
		bzero(&in_prog_fstab, sizeof (in_prog_fstab));
		return;
	case RT_IPD:
		if (node_scope == true) {
			long_usage(CMD_ADD, true); /*lint !e747 */
			goto bad;
		}

		/* Can't add IPD to an installed zone cluster. */
		if (state_atleast(ZC_STATE_INSTALLED)) {
			zerr(gettext("Zone cluster %s already installed; "
			    "%s %s not allowed."), zone, cmd_to_str(CMD_ADD),
			    rt_to_str(RT_IPD));
			goto bad;
		}
		bzero(&in_prog_ipdtab, sizeof (in_prog_ipdtab));
		return;
	case RT_NET:
		bzero(&in_prog_nwiftab, sizeof (in_prog_nwiftab));

		/*
		 * If we're adding a 'global' net resource that has an IP
		 * which can move from node to node in the zone cluster,
		 * the IP address is managed by runtimes other that the zones
		 * runtime. Such IP addresses include Sun Cluster logical
		 * host and shared addresses, and also Oracle RAC virtual IPs.
		 * We want the user to 'export' such addresses to the zone
		 * cluster, even though we don't configure them into the
		 * individual Solaris zones themselves. The reason is that we
		 * want to make sure the zones can't use IP addresses that
		 * have not been explicitly granted to them by the global zone
		 * admin.
		 *
		 * For 'global' net resources, we don't care about the physical
		 * NIC that will host the IP address. In fact, we don't allow
		 * setting of the physical property in this case. The physical
		 * property of all global net resources are hard-wired to the
		 * value "auto".
		 */
		if (!node_scope) {
			(void) strlcpy(in_prog_nwiftab.zc_nwif_physical,
			    "auto",
			    sizeof (in_prog_nwiftab.zc_nwif_physical));
		}
		return;
	case RT_DEVICE:
		/* Can't add device in node scope yet */
		if (node_scope == true) {
			zerr(gettext("Adding %s resource in the %s scope is "
			    "not supported."),
			    rt_to_str(RT_DEVICE), rt_to_str(RT_NODE));
			break;
		}
		bzero(&in_prog_devtab, sizeof (in_prog_devtab));
		return;
	case RT_RCTL:
		/* Can't add rctl in node scope yet */
		if (node_scope == true) {
			zerr(gettext("Adding %s resource in the %s scope is "
			    "not supported."),
			    rt_to_str(RT_RCTL), rt_to_str(RT_NODE));
			break;
		}
		bzero(&in_prog_rctltab, sizeof (in_prog_rctltab));
		return;
	case RT_ATTR:
		zerr(gettext("Zone cluster does not support the %s resource "
		    "type at this time."), rt_to_str(RT_ATTR));
		break;
	case RT_DATASET:
		/* Can't add dataset in node scope yet */
		if (node_scope == true) {
			zerr(gettext("Adding %s resource in the %s scope is "
			    "not supported."),
			    rt_to_str(RT_DATASET), rt_to_str(RT_NODE));
			break;
		}
		bzero(&in_prog_dstab, sizeof (in_prog_dstab));
		return;
	case RT_SYSID:
		/* Can't add sysid reource in the node scope. */
		if (node_scope == true) {
			long_usage(CMD_ADD, true); /*lint !e747 */
			goto bad;
		}


		/* only one sysid resource allowed */
		assert(handle != NULL);
		if (zccfg_num_resources(handle, "sysid") > 0) {
			zerr(gettext("Only one %s resource can be specified "
			" for a zone cluster."),
			    rt_to_str(RT_SYSID));
			goto bad;
		}

		/* Can't add sysid if zone cluster is already installed. */
		if (state_atleast(ZC_STATE_INSTALLED)) {
			zerr(gettext("Zone cluster %s already installed; "
			    "%s %s not allowed."), zone, cmd_to_str(CMD_ADD),
			    rt_to_str(RT_SYSID));
			goto bad;
		}
		bzero(&in_prog_sysidtab, sizeof (in_prog_sysidtab));
		return;
	case RT_NODE:
		/* Can't add a node resource if already in node scope. */
		if (node_scope == true) {
			long_usage(CMD_ADD, true); /*lint !e747 */
			goto bad;
		}
		bzero(&in_prog_nodetab, sizeof (in_prog_nodetab));
		return;
	case RT_DCPU:
		/* Can't add a dedicated CPU resource in node scope. */
		if (node_scope == true) {
			long_usage(CMD_ADD, true); /*lint !e747 */
			goto bad;
		}

		/* Make sure there isn't already a cpu-set entry. */
		if (zccfg_lookup_pset(handle, &tmp_psettab) == ZC_OK) {
			zerr(gettext("The %s resource already exists."),
			    rt_to_str(RT_DCPU));
			goto bad;
		}

		/* Make sure there isn't already a cpu-cap entry. */
		if (zccfg_get_aliased_rctl(handle, ALIAS_CPUCAP, &tmp) ==
		    ZC_OK) {
			zerr(gettext("The %s resource already exists."),
			    rt_to_str(RT_PCAP));
			goto bad;
		}

		/* Make sure the pool property isn't set. */
		if (zccfg_get_pool(handle, pool, sizeof (pool)) == ZC_OK &&
		    strlen(pool) > 0) {
			zerr(gettext("The %s property is already set.  "
			    "A persistent pool is incompatible with\nthe %s "
			    "resource."),
			    pt_to_str(PT_POOL), rt_to_str(RT_DCPU));
			goto bad;
		}

		bzero(&in_prog_psettab, sizeof (in_prog_psettab));
		return;
	case RT_PCAP:
		/* Can't add a dedicated CPU resource in node scope. */
		if (node_scope == true) {
			long_usage(CMD_ADD, true); /*lint !e747 */
			goto bad;
		}

		/*
		 * Make sure there isn't already a cpu-set or incompatible
		 * cpu-cap rctls.
		 */
		if (zccfg_lookup_pset(handle, &tmp_psettab) == ZC_OK) {
			zerr(gettext("The %s resource already exists."),
			    rt_to_str(RT_DCPU));
			goto bad;
		}

		switch (zccfg_get_aliased_rctl(handle, ALIAS_CPUCAP,
		    &tmp)) {
		case ZC_ALIAS_DISALLOW:
			zone_perror(rt_to_str(RT_PCAP), ZC_ALIAS_DISALLOW,
			    false); /*lint !e747 */
			goto bad;
		case ZC_OK:
			zerr(gettext("The %s resource already exists."),
			    rt_to_str(RT_PCAP));
			goto bad;
		default:
			break;
		}
		return;
	case RT_MCAP:
		/* Can't add a capped memory resource in node scope. */
		if (node_scope == true) {
			long_usage(CMD_ADD, true); /*lint !e747 */
			usage(false, HELP_RESOURCES); /*lint !e747 */
			goto bad;
		}

		/*
		 * Make sure there isn't already a mem-cap entry or max-swap
		 * or max-locked rctl.
		 */
		if (zccfg_lookup_mcap(handle, &tmp_mcaptab) == ZC_OK ||
		    zccfg_get_aliased_rctl(handle, ALIAS_MAXSWAP,
		    &tmp_mcap) == ZC_OK || zccfg_get_aliased_rctl(handle,
		    ALIAS_MAXLOCKEDMEM, &tmp_mcap) == ZC_OK) {
			zerr(gettext("The %s resource or a related resource "
			    "control already exists."), rt_to_str(RT_MCAP));
			goto bad;
		}
		bzero(&in_prog_mcaptab, sizeof (in_prog_mcaptab));
		return;
	default:
		zone_perror(rt_to_str(type), ZC_NO_RESOURCE_TYPE,
		    true); /*lint !e747 */
		long_usage(CMD_ADD, true); /*lint !e747 */
		usage(false, HELP_RESOURCES); /*lint !e747 */
	}
bad:
	if (node_scope) {
		resource_scope = RT_NODE;
	} else {
		global_scope = true;
	}
	end_op = -1;
}

static void
do_complex_rctl_val(complex_property_ptr_t cp)
{
	struct zc_rctlvaltab *valtabp;
	complex_property_ptr_t cx;
	bool seen_priv = false;
	bool seen_limit = false;
	bool seen_action = false;
	rctlblk_t *rctlblkp;
	int err;

	if ((valtabp = alloc_rctlvaltab()) == NULL) {
		zone_perror(zone, ZC_NOMEM, true); /*lint !e747 */
		exit(ZC_ERR);
	}
	for (cx = cp; cx != NULL; cx = cx->cp_next) {
		switch (cx->cp_type) {
		case PT_PRIV:
			if (seen_priv) {
				zerr(gettext("%s already specified"),
				    pt_to_str(PT_PRIV));
				goto bad;
			}
			(void) strlcpy(valtabp->zc_rctlval_priv,
			    cx->cp_value,
			    sizeof (valtabp->zc_rctlval_priv));
			seen_priv = true;
			break;
		case PT_LIMIT:
			if (seen_limit) {
				zerr(gettext("%s already specified"),
				    pt_to_str(PT_LIMIT));
				goto bad;
			}
			(void) strlcpy(valtabp->zc_rctlval_limit,
			    cx->cp_value,
			    sizeof (valtabp->zc_rctlval_limit));
			seen_limit = true;
			break;
		case PT_ACTION:
			if (seen_action) {
				zerr(gettext("%s already specified"),
				    pt_to_str(PT_ACTION));
				goto bad;
			}
			(void) strlcpy(valtabp->zc_rctlval_action,
			    cx->cp_value,
			    sizeof (valtabp->zc_rctlval_action));
			seen_action = true;
			break;
		default:
			/*lint -e747 */
			zone_perror(pt_to_str(PT_VALUE),
			    ZC_NO_PROPERTY_TYPE, true);
			long_usage(CMD_ADD, true);
			usage(false, HELP_PROPS);
			/*lint +e747 */
			zccfg_free_rctl_value_list(valtabp);
			return;
		}
	}
	if (!seen_priv) {
		zerr(gettext("%s not specified"), pt_to_str(PT_PRIV));
	}
	if (!seen_limit) {
		zerr(gettext("%s not specified"), pt_to_str(PT_LIMIT));
	}
	if (!seen_action) {
		zerr(gettext("%s not specified"), pt_to_str(PT_ACTION));
	}
	if (!seen_priv || !seen_limit || !seen_action) {
		goto bad;
	}
	valtabp->zc_rctlval_next = NULL;
	rctlblkp = (rctlblk_t *)alloca(rctlblk_size());
	/*
	 * Make sure the rctl value looks roughly correct; we won't know if
	 * it's truly OK until we verify the configuration on the target
	 * system.
	 */
	if (zccfg_construct_rctlblk(valtabp, rctlblkp) != ZC_OK ||
	    !zccfg_valid_rctlblk(rctlblkp)) {
		zerr(gettext("Invalid %s %s specification"), rt_to_str(RT_RCTL),
		    pt_to_str(PT_VALUE));
		goto bad;
	}
	err = zccfg_add_rctl_value(&in_prog_rctltab, valtabp);
	if (err != ZC_OK) {
		zone_perror(pt_to_str(PT_VALUE), err, true); /*lint !e747 */
	}
	return;
bad:
	zccfg_free_rctl_value_list(valtabp);
}

//
// The fs and rctl resource types have some properties that must be "added"
// (and not set). add_property() supports the add property operation in their
// respective resource scopes.
//
static void
add_property(cmd_t *cmdp)
{
	char *prop_idp;
	int err;
	int res_type;
	int prop_type;
	property_value_ptr_t pp;
	list_property_ptr_t l;

	res_type = resource_scope;
	prop_type = cmdp->cmd_prop_name[0];
	if (res_type == RT_UNKNOWN || prop_type == PT_UNKNOWN) {
		long_usage(CMD_ADD, true); /*lint !e747 */
		return;
	}

	if (cmdp->cmd_prop_nv_pairs != 1) {
		long_usage(CMD_ADD, true); /*lint !e747 */
		return;
	}

	if (initialize(true) != ZC_OK) {
		return;
	}

	switch (res_type) {
	case RT_FS:
		if (prop_type != PT_OPTIONS) {
			zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE,
			    true); /*lint !e747 */
			long_usage(CMD_ADD, true); /*lint !e747 */
			usage(false, HELP_PROPS); /*lint !e747 */
			return;
		}
		pp = cmdp->cmd_property_ptr[0];
		if (pp->pv_type != PROP_VAL_SIMPLE &&
		    pp->pv_type != PROP_VAL_LIST) {
			zerr(gettext("A %s or %s value was expected here."),
			    pvt_to_str(PROP_VAL_SIMPLE),
			    pvt_to_str(PROP_VAL_LIST));
			saw_error = true;
			return;
		}
		if (pp->pv_type == PROP_VAL_SIMPLE) {
			if (pp->pv_simple == NULL) {
				long_usage(CMD_ADD, true); /*lint !e747 */
				return;
			}
			prop_idp = pp->pv_simple;
			err = zccfg_add_fs_option(&in_prog_fstab,
			    prop_idp);
			if (err != ZC_OK) {
				zone_perror(pt_to_str(prop_type), err,
				    true); /*lint !e747 */
			}
		} else {
			list_property_ptr_t list;

			for (list = pp->pv_list; list != NULL;
			    list = list->lp_next) {
				prop_idp = list->lp_simple;
				if (prop_idp == NULL) {
					break;
				}
				err = zccfg_add_fs_option(
				    &in_prog_fstab, prop_idp);
				if (err != ZC_OK) {
					zone_perror(pt_to_str(prop_type), err,
					    true); /*lint !e747 */
				}
			}
		}
		return;
	case RT_RCTL:
		if (prop_type != PT_VALUE) {
			/*lint -e747 */
			zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE,
			    true);
			long_usage(CMD_ADD, true);
			usage(false, HELP_PROPS);
			/*lint +e747 */
			return;
		}
		pp = cmdp->cmd_property_ptr[0];
		if (pp->pv_type != PROP_VAL_COMPLEX &&
		    pp->pv_type != PROP_VAL_LIST) {
			zerr(gettext("A %s or %s value was expected here."),
			    pvt_to_str(PROP_VAL_COMPLEX),
			    pvt_to_str(PROP_VAL_LIST));
			saw_error = true;
			return;
		}
		if (pp->pv_type == PROP_VAL_COMPLEX) {
			do_complex_rctl_val(pp->pv_complex);
			return;
		}
		for (l = pp->pv_list; l != NULL; l = l->lp_next) {
			do_complex_rctl_val(l->lp_complex);
		}
		return;
	default:
		/*lint -e747 */
		zone_perror(rt_to_str(res_type), ZC_NO_RESOURCE_TYPE, true);
		long_usage(CMD_ADD, true);
		usage(false, HELP_RESOURCES);
		/*lint +e747 */
		return;
	}
}

/*
 * Add resource to the zone cluster configuration.
 */
void
add_func(cmd_t *cmdp)
{
	int arg;

	assert(cmdp != NULL);

	optind = 0;
	if ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_ADD);
			return;
		default:
			short_usage(CMD_ADD);
			return;
		}
	}
	if (optind != cmdp->cmd_argc) {
		short_usage(CMD_ADD);
		return;
	}

	if (zc_is_read_only(CMD_ADD)) {
		return;
	}

	/* do initialization stuff */
	if (initialize(true) != ZC_OK) {
		return;
	}


	if (global_scope) {
		global_scope = false;
		resource_scope = cmdp->cmd_res_type;
		end_op = CMD_ADD;
		add_resource(cmdp);
	} else if (resource_scope == RT_NODE) {
		node_scope = true;
		resource_scope = cmdp->cmd_res_type;

		/* Record the fact that we entered node scope via 'add' cmd. */
		if (saved_end_op == -1) {
			saved_end_op = CMD_ADD;
		}
		end_op = CMD_ADD;
		add_resource(cmdp);
	} else {
		add_property(cmdp);
	}
}

/*
 * Deletes the zone cluster. This operation deletes the component Solaris
 * zone from each node, then removes the zone cluster entry from the CCR.
 */
void
delete_func(cmd_t *cmdp)
{
	int err;
	int arg;
	int answer;
	int i; /* to iterate over nodelistp array */
	char line[ZONENAME_MAX + 128]; /* enough to ask a question */
	char cmd_buf[MAXPATHLEN];
	struct zc_nodetab nodetab = {"", "", NULL};
	char **nodelistp = NULL;
	nodeid_t nodeid;
	bool force = false;
	bool privateip;
	bool partial_failure = false;
	zone_cluster_state_t state = ZC_STATE_UNKNOWN;
	int node_count = 0; /* number of nodes in this ZC */

	optind = 0;
	while ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?F")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_DELETE);
			return;
		case 'F':
			force = true;
			break;
		default:
			short_usage(CMD_DELETE);
			return;
		}
	}

	if (optind != cmdp->cmd_argc) {
		short_usage(CMD_DELETE);
		return;
	}

	if (zc_is_read_only(CMD_DELETE)) {
		return;
	}
	/*
	 * Initialize sets up the global called "handle" and warns the
	 * user if the ZC is not configured.
	 */
	err = initialize(true);
	if (err == ZC_MISSING_CONFIG) {
		/* Partially created ZC. Force destroy it */
		zerr(gettext("Zone cluster is partially configured.\n"
		    "Destroying it."));
		if ((err = zccfg_destroy(zone, true)) != ZC_OK) {
			zone_perror(zone, err, true);
		}
		return;
	} else if (err != ZC_OK) {
		return;
	}

	if (!force) {

		(void) snprintf(line, sizeof (line),
		    gettext("Are you sure you want to delete %s"), zone);
		if ((answer = ask_yesno(false, line)) == -1) {
			zerr(gettext("Input not from terminal and -F not "
			    "specified:\n%s command ignored, exiting."),
			    cmd_to_str(CMD_DELETE));
			exit(ZC_ERR);
		}
		if (answer != 1) {
			return;
		}
	}

	/* Skip a bunch of steps if configuration has not been committed */
	if (new_zone_cluster) {
		goto cleanup;
	}

	if (state_atleast(ZC_STATE_INCOMPLETE)) {
		zerr(gettext("Zone cluster %s not in %s state; "
		    "%s not allowed."), zone,
		    zone_cluster_state_str(ZC_STATE_CONFIGURED),
		    cmd_to_str(CMD_DELETE));
		return;
	}

	node_count = zccfg_num_resources(handle, "node");

	nodelistp = (char **)calloc((size_t)node_count, sizeof (char *));
	if (nodelistp == NULL) {
		zone_perror(zone, ZC_NOMEM, true);
		exit(ZC_ERR);
	}

	if ((err = zccfg_setnodeent(handle)) != ZC_OK) {
		zone_perror(zone, err, true);
		return;
	}

	i = 0;
	while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
		nodelistp[i] = strdup(nodetab.zc_nodename);
		if (nodelistp[i] == NULL) {
			zone_perror(zone, ZC_NOMEM, true);
			exit(ZC_ERR);
		}
		i++;
	}
	(void) zccfg_endnodeent(handle);

	for (i = 0; i < node_count; i++) {
		nodeid = clconf_cluster_get_nodeid_by_nodename(nodelistp[i]);
		(void) get_zone_state_on_node(zone, nodeid, &state);
		if (state < ZC_STATE_CONFIGURED ||
		    state > ZC_STATE_INCOMPLETE) {
			saw_error = true;
			zerr(gettext("Zone cluster state on node %s is %s.\n"
			    "State must be %s or %s to delete.\n"),
			    nodelistp[i],
			    zone_cluster_state_str(state),
			    zone_cluster_state_str(ZC_STATE_CONFIGURED),
			    zone_cluster_state_str(ZC_STATE_INCOMPLETE));
		}
	}

	if (saw_error) {
		/*
		 * We must be absolutely sure about zone cluster state
		 * before it can be deleted.
		 */
		return;
	}

	/*
	 * "zc_helper <zonename> delete" invokes
	 * "zonecfg -z <zonename> delete -F".
	 */
	(void) sprintf(cmd_buf, "%s %s delete", ZC_HELPER, zone);

	/*
	 * Delete the zone from each node in the nodelist.
	 */
	for (i = 0; i < node_count; i++) {
		nodeid = clconf_cluster_get_nodeid_by_nodename(
		    nodelistp[i]);
		err = zccfg_execute_cmd(nodeid, cmd_buf);
		if (err != ZC_OK) {
			partial_failure = true;
			zone_perror(zone, err, true);
			(void) printf(gettext("Failed to delete %s on %s.\n"),
			    zone, nodelistp[i]);
			/*
			 * Add entry in the command log so that the
			 * delete command can be replayed later on this node.
			 */
			err = zccfg_add_cmd_log_entry(zone,
			    nodelistp[i], cmd_to_str(CMD_DELETE));
			if (err != ZC_OK) {
				zone_perror(zone, err, true);
			}
			continue;
		} else {
			/*
			 * We have deleted the zone cluster member on
			 * nodelistp[i]. We must now remove this node
			 * from the configuration.
			 */
			(void) strcpy(nodetab.zc_nodename, nodelistp[i]);
			if ((err = zccfg_delete_node(handle, &nodetab))
			    != ZC_OK) {
				zone_perror(zone, err, true);
			}

			need_to_commit = true;
			continue;
		}
	}

	/*
	 * Commit the new configuration.
	 */
	if (need_to_commit) {
		if ((err = zccfg_save(handle)) != ZC_OK) {
			zone_perror(zone, err, true);
			exit(ZC_ERR);
		}
	}

	if (((err = zccfg_get_clprivnet(handle, &privateip)) == ZC_OK) &&
	    (privateip == true) && !partial_failure) {
		err = zccfg_remove_netccr_entry(zone);
	}

	/*
	 * Don't remove configuration from CCR if there were failures
	 * in the while loop above. zccfg_destroy must only be called
	 * after all zone members of the ZC are deleted.
	 *
	 * In case of partial deletion, the nodes on which the delete did not
	 * succeed will replay the delete command on node start-up, and the
	 * ZC itself will be deleted when the last node is deleted.
	 */
	if (!partial_failure) {
		if ((err = zccfg_destroy(zone, true)) != ZC_OK) {
			zone_perror(zone, err, true);
		}
	} else {
		(void) printf(gettext("The %s command resulted in partial "
		    "failure(s).\nZone cluster configuration not "
		    "destroyed.\n"), cmd_to_str(CMD_DELETE));
	}

cleanup:
	need_to_commit = false;

	/* Finish off the old handle and get a new one to avoid leaks */
	if (got_handle) {
		zccfg_fini_handle(handle);
		if ((handle = zccfg_init_handle()) == NULL) {
			zone_perror(execnamep, ZC_NOMEM, true);
			exit(ZC_ERR);
		}
		if ((err = zccfg_get_handle(zone, handle)) != ZC_OK) {
			/* If there was no zone cluster before, that's OK. */
			if (err != ZC_NO_ZONE) {
				zone_perror(zone, err, true);
			}
			got_handle = false;
		}
	}
	if (nodelistp) {
		free(nodelistp);
	}
}

static int
fill_in_fstab(cmd_t *cmdp, struct zc_fstab *fstab, bool fill_in_only)
{
	int err;
	int i;
	property_value_ptr_t pp;

	if ((err = initialize(true)) != ZC_OK) {
		return (err);
	}

	bzero(fstab, sizeof (*fstab));
	for (i = 0; i < cmdp->cmd_prop_nv_pairs; i++) {
		pp = cmdp->cmd_property_ptr[i];
		if (pp->pv_type != PROP_VAL_SIMPLE || pp->pv_simple == NULL) {
			zerr(gettext("A simple value was expected here."));
			saw_error = true;
			return (ZC_INSUFFICIENT_SPEC);
		}
		switch (cmdp->cmd_prop_name[i]) {
		case PT_DIR:
			(void) strlcpy(fstab->zc_fs_dir, pp->pv_simple,
			    sizeof (fstab->zc_fs_dir));
			break;
		case PT_SPECIAL:
			(void) strlcpy(fstab->zc_fs_special, pp->pv_simple,
			    sizeof (fstab->zc_fs_special));
			break;
		case PT_RAW:
			(void) strlcpy(fstab->zc_fs_raw, pp->pv_simple,
			    sizeof (fstab->zc_fs_raw));
			break;
		case PT_TYPE:
			(void) strlcpy(fstab->zc_fs_type, pp->pv_simple,
			    sizeof (fstab->zc_fs_type));
			break;
		default:
			zone_perror(pt_to_str(cmdp->cmd_prop_name[i]),
			    ZC_NO_PROPERTY_TYPE, true);
			return (ZC_INSUFFICIENT_SPEC);
		}
	}
	if (fill_in_only) {
		return (ZC_OK);
	}
	return (zccfg_lookup_filesystem(handle, fstab));
}

static int
fill_in_ipdtab(cmd_t *cmdp, struct zc_fstab *ipdtab, bool fill_in_only)
{
	int err;
	int i;
	property_value_ptr_t pp;

	if ((err = initialize(true)) != ZC_OK)
		return (err);

	bzero(ipdtab, sizeof (*ipdtab));
	for (i = 0; i < cmdp->cmd_prop_nv_pairs; i++) {
		pp = cmdp->cmd_property_ptr[i];
		if (pp->pv_type != PROP_VAL_SIMPLE || pp->pv_simple == NULL) {
			zerr(gettext("A simple value was expected here."));
			saw_error = true;
			return (ZC_INSUFFICIENT_SPEC);
		}
		switch (cmdp->cmd_prop_name[i]) {
		case PT_DIR:
			(void) strlcpy(ipdtab->zc_fs_dir, pp->pv_simple,
			    sizeof (ipdtab->zc_fs_dir));
			break;
		default:
			zone_perror(pt_to_str(cmdp->cmd_prop_name[i]),
			    ZC_NO_PROPERTY_TYPE, true);
			return (ZC_INSUFFICIENT_SPEC);
		}
	}
	if (fill_in_only)
		return (ZC_OK);
	return (zccfg_lookup_ipd(handle, ipdtab));
}

/*
 * Given a node resource entry, check whether or not the net resource
 * specified by the second argument exists in the node resource.
 * Returns ZC_OK if there is exactly one matching net resource,
 * ZC_NO_RESOURCE_ID if there is no match, and ZC_INSUFFICIENT_SPEC if
 * there is more than one match.
 */
static int
lookup_nwif_node(struct zc_nodetab *nodep, struct zc_nwiftab *tabp)
{
	zc_nwifelem_t *listp;
	zc_nwifelem_t *curp = NULL;
	int addr_match = 0;
	int phys_match = 0;

	listp = nodep->zc_nwif_listp;
	while (listp) {
		if ((tabp->zc_nwif_address[0] != '\0') &&
		    (strcmp(listp->elem.zc_nwif_address,
		    tabp->zc_nwif_address) == 0)) {
			addr_match++;
			curp = listp;

			/*
			 * Address is always unique, so we can break if
			 * we match on the address.
			 */
			break;
		}
		if ((tabp->zc_nwif_physical[0] != '\0') &&
		    (strcmp(listp->elem.zc_nwif_physical,
		    tabp->zc_nwif_physical) == 0)) {
			phys_match++;
			curp = listp;
		}

		listp = listp->next;
	}

	if (phys_match > 1) {
		return (ZC_INSUFFICIENT_SPEC);
	} else if (curp == NULL && phys_match == 0 && addr_match == 0) {
		return (ZC_NO_RESOURCE_ID);
	} else {
		(void) strcpy(tabp->zc_nwif_address,
		    curp->elem.zc_nwif_address);
		(void) strcpy(tabp->zc_nwif_physical,
		    curp->elem.zc_nwif_physical);
		(void) strcpy(tabp->zc_nwif_defrouter,
		    curp->elem.zc_nwif_defrouter);
		return (ZC_OK);
	}
}

/*
 * Populate the net resource pointed to by the second argument with values
 * contained in the command pointer. If the third argument is true, we just
 * fill and return; else we lookup the specified net resource from the
 * configuration.
 */
static int
fill_in_nwiftab(cmd_t *cmdp, struct zc_nwiftab *nwiftab, bool fill_in_only)
{
	int err;
	int i;
	property_value_ptr_t pp;

	if ((err = initialize(true)) != ZC_OK) {
		return (err);
	}

	bzero(nwiftab, sizeof (*nwiftab));
	for (i = 0; i < cmdp->cmd_prop_nv_pairs; i++) {
		pp = cmdp->cmd_property_ptr[i];
		if (pp->pv_type != PROP_VAL_SIMPLE || pp->pv_simple == NULL) {
			zerr(gettext("A simple value was expected here."));
			saw_error = true;
			return (ZC_INSUFFICIENT_SPEC);
		}
		switch (cmdp->cmd_prop_name[i]) {
		case PT_ADDRESS:
			(void) strlcpy(nwiftab->zc_nwif_address,
			    pp->pv_simple,
			    sizeof (nwiftab->zc_nwif_address));
			break;
		case PT_PHYSICAL:
			(void) strlcpy(nwiftab->zc_nwif_physical,
			    pp->pv_simple,
			    sizeof (nwiftab->zc_nwif_physical));
			break;
		case PT_DEFROUTER:
			(void) strlcpy(nwiftab->zc_nwif_defrouter,
			    pp->pv_simple,
			    sizeof (nwiftab->zc_nwif_defrouter));
			break;
		default:
			zone_perror(pt_to_str(cmdp->cmd_prop_name[i]),
			    ZC_NO_PROPERTY_TYPE, true);
			return (ZC_INSUFFICIENT_SPEC);
		}
	}
	if (fill_in_only) {
		return (ZC_OK);
	}

	if (!node_scope) {
		err = zccfg_lookup_nwif(handle, nwiftab);
	} else {
		err = lookup_nwif_node(&in_prog_nodetab, nwiftab);
	}
	return (err);
}

static int
fill_in_devtab(cmd_t *cmdp, struct zc_devtab *devtab, bool fill_in_only)
{
	int err;
	int i;
	property_value_ptr_t pp;

	if ((err = initialize(true)) != ZC_OK) {
		return (err);
	}

	bzero(devtab, sizeof (*devtab));
	for (i = 0; i < cmdp->cmd_prop_nv_pairs; i++) {
		pp = cmdp->cmd_property_ptr[i];
		if (pp->pv_type != PROP_VAL_SIMPLE || pp->pv_simple == NULL) {
			zerr(gettext("A simple value was expected here."));
			saw_error = true;
			return (ZC_INSUFFICIENT_SPEC);
		}
		switch (cmdp->cmd_prop_name[i]) {
		case PT_MATCH:
			(void) strlcpy(devtab->zc_dev_match, pp->pv_simple,
			    sizeof (devtab->zc_dev_match));
			break;
		default:
			zone_perror(pt_to_str(cmdp->cmd_prop_name[i]),
			    ZC_NO_PROPERTY_TYPE, true);
			return (ZC_INSUFFICIENT_SPEC);
		}
	}
	if (fill_in_only) {
		return (ZC_OK);
	}
	err = zccfg_lookup_dev(handle, devtab);
	return (err);
}

static int
fill_in_rctltab(cmd_t *cmdp, struct zc_rctltab *rctltab, bool fill_in_only)
{
	int err;
	int i;
	property_value_ptr_t pp;

	if ((err = initialize(true)) != ZC_OK) {
		return (err);
	}

	bzero(rctltab, sizeof (*rctltab));
	for (i = 0; i < cmdp->cmd_prop_nv_pairs; i++) {
		pp = cmdp->cmd_property_ptr[i];
		if (pp->pv_type != PROP_VAL_SIMPLE || pp->pv_simple == NULL) {
			zerr(gettext("A simple value was expected here."));
			saw_error = true;
			return (ZC_INSUFFICIENT_SPEC);
		}
		switch (cmdp->cmd_prop_name[i]) {
		case PT_NAME:
			(void) strlcpy(rctltab->zc_rctl_name, pp->pv_simple,
			    sizeof (rctltab->zc_rctl_name));
			break;
		default:
			zone_perror(pt_to_str(cmdp->cmd_prop_name[i]),
			    ZC_NO_PROPERTY_TYPE, true);
			return (ZC_INSUFFICIENT_SPEC);
		}
	}
	if (fill_in_only) {
		return (ZC_OK);
	}
	err = zccfg_lookup_rctl(handle, rctltab);
	return (err);
}

static int
fill_in_dstab(cmd_t *cmdp, struct zc_dstab *dstab, bool fill_in_only)
{
	int err;
	int i;
	property_value_ptr_t pp;

	if ((err = initialize(true)) != ZC_OK) {
		return (err);
	}

	dstab->zc_dataset_name[0] = '\0';
	for (i = 0; i < cmdp->cmd_prop_nv_pairs; i++) {
		pp = cmdp->cmd_property_ptr[i];
		if (pp->pv_type != PROP_VAL_SIMPLE || pp->pv_simple == NULL) {
			zerr(gettext("A simple value was expected here."));
			saw_error = true;
			return (ZC_INSUFFICIENT_SPEC);
		}
		switch (cmdp->cmd_prop_name[i]) {
		case PT_NAME:
			(void) strlcpy(dstab->zc_dataset_name,
			    pp->pv_simple,
			    sizeof (dstab->zc_dataset_name));
			break;
		default:
			zone_perror(pt_to_str(cmdp->cmd_prop_name[i]),
			    ZC_NO_PROPERTY_TYPE, true);
			return (ZC_INSUFFICIENT_SPEC);
		}
	}
	if (fill_in_only) {
		return (ZC_OK);
	}
	return (zccfg_lookup_ds(handle, dstab));
}

static int
fill_in_nodetab(cmd_t *cmdp, struct zc_nodetab *nodetab, bool fill_in_only)
{
	int err;
	int i;
	property_value_ptr_t pp;

	if ((err = initialize(true)) != ZC_OK) {
		return (err);
	}

	bzero(nodetab, sizeof (*nodetab));
	for (i = 0; i < cmdp->cmd_prop_nv_pairs; i++) {
		pp = cmdp->cmd_property_ptr[i];
		if (pp->pv_type != PROP_VAL_SIMPLE || pp->pv_simple == NULL) {
			zerr(gettext("A simple value was expected here."));
			saw_error = true;
			return (ZC_INSUFFICIENT_SPEC);
		}
		switch (cmdp->cmd_prop_name[i]) {
		case PT_PHYSICALHOST:
			(void) strlcpy(nodetab->zc_nodename, pp->pv_simple,
			    sizeof (nodetab->zc_nodename));
			break;
		case PT_HOSTNAME:
			(void) strlcpy(nodetab->zc_hostname, pp->pv_simple,
			    sizeof (nodetab->zc_hostname));
			break;
		default:
			zone_perror(pt_to_str(cmdp->cmd_prop_name[i]),
			    ZC_NO_PROPERTY_TYPE, true);
			return (ZC_INSUFFICIENT_SPEC);
		}
	}
	if (fill_in_only) {
		return (ZC_OK);
	}
	return (zccfg_lookup_node(handle, nodetab));
}

/*
 * Print property value pointed to by propvalp to the file specified by fp.
 * Swap and locked memory get special formatting; if print_notspec is true,
 * we say "not specified" if the property has a null value.
 */
static void
output_prop(FILE *fp, int pnum, char *propvalp, bool print_notspec)
{
	char *qstr;

	if ((propvalp != NULL) && (*propvalp != '\0')) {
		qstr = quoteit(propvalp);
		if (pnum == PT_SWAP || pnum == PT_LOCKED) {
			(void) fprintf(fp, "\t[%s: %s]\n", pt_to_str(pnum),
			    qstr);
		} else {
			(void) fprintf(fp, "\t%s: %s\n", pt_to_str(pnum), qstr);
		}
		free(qstr);
	} else if (print_notspec) {
		(void) fprintf(fp, gettext("\t%s not specified\n"),
		    pt_to_str(pnum));
	}
}

static void
info_zonename(zc_dochandle_t hndl, FILE *fp)
{
	char zonename[ZONENAME_MAX];

	if (zccfg_get_name(hndl, zonename, sizeof (zonename)) == ZC_OK) {
		(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_ZONENAME),
		    zonename);
	} else {
		(void) fprintf(fp, gettext("%s not specified\n"),
		    pt_to_str(PT_ZONENAME));
	}
}

static void
info_zonepath(zc_dochandle_t hndl, FILE *fp)
{
	char zonepath[MAXPATHLEN];

	if (zccfg_get_zonepath(hndl, zonepath, sizeof (zonepath))
	    == ZC_OK) {
		(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_ZONEPATH),
		    zonepath);
	} else {
		(void) fprintf(fp, gettext("%s not specified\n"),
		    pt_to_str(PT_ZONEPATH));
	}
}

static void
info_autoboot(zc_dochandle_t hndl, FILE *fp)
{
	bool autoboot;
	int err;

	if ((err = zccfg_get_autoboot(hndl, &autoboot)) == ZC_OK) {
		(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_AUTOBOOT),
		    autoboot ? "true" : "false");
	} else {
		zone_perror(zone, err, true);
	}
}

static void
info_clprivnet(zc_dochandle_t hndl, FILE *fp)
{
	bool privateip;
	int err;

	if ((err = zccfg_get_clprivnet(hndl, &privateip)) == ZC_OK) {
		(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_CLPRIVNET),
		    privateip ? "true" : "false");
	} else {
		zone_perror(zone, err, true);
	}
}

static void
info_brand(zc_dochandle_t hndl, FILE *fp)
{
	char brand[MAXNAMELEN];

	if (zccfg_get_brand(hndl, brand, sizeof (brand)) == ZC_OK) {
		(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_BRAND),
		    brand);
	} else {
		(void) fprintf(fp, "%s %s\n", pt_to_str(PT_BRAND),
		    gettext("not specified"));
	}
}

static void
info_pool(zc_dochandle_t hndl, FILE *fp)
{
	char pool[MAXNAMELEN];
	int err;

	if ((err = zccfg_get_pool(hndl, pool, sizeof (pool))) == ZC_OK) {
		(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_POOL), pool);
	} else {
		zone_perror(zone, err, true);
	}
}

static void
info_limitpriv(zc_dochandle_t hndl, FILE *fp)
{
	char *limitpriv;
	int err;

	if ((err = zccfg_get_limitpriv(hndl, &limitpriv)) == ZC_OK) {
		(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_LIMITPRIV),
		    limitpriv);
	} else {
		zone_perror(zone, err, true);
	}
}

static void
info_bootargs(zc_dochandle_t hndl, FILE *fp)
{
	char bootargs[BOOTARGS_MAX];
	int err;

	if ((err = zccfg_get_bootargs(hndl, bootargs,
	    sizeof (bootargs))) == ZC_OK) {
		(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_BOOTARGS),
		    bootargs);
	} else {
		zone_perror(zone, err, true);
	}
}

static void
info_sched(zc_dochandle_t hndl, FILE *fp)
{
	char sched[MAXNAMELEN];
	int err;

	if ((err = zccfg_get_sched_class(hndl, sched, sizeof (sched)))
	    == ZC_OK) {
		(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_SCHED), sched);
	} else {
		zone_perror(zone, err, true);
	}
}

static void
info_iptype(zc_dochandle_t hndl, FILE *fp)
{
	zc_iptype_t iptype;
	int err;

	if ((err = zccfg_get_iptype(hndl, &iptype)) == ZC_OK) {
		switch (iptype) {
		case CZS_SHARED:
			(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_IPTYPE),
			    "shared");
			break;
		case CZS_EXCLUSIVE:
			(void) fprintf(fp, "%s: %s\n", pt_to_str(PT_IPTYPE),
			    "exclusive");
			break;
		}
	} else {
		zone_perror(zone, err, true);
	}
}

/*
 * Helper function to convert from bytes to the SI units. str has the input
 * byte string and buf contains the conversion result.
 */
void
bytes_to_units(char *str, char *buf, int bufsize)
{
	unsigned long long num;
	unsigned long long save = 0;
	char *units = "BKMGT";
	char *up = units;

	num = strtoll(str, NULL, 10);

	/* less than 1K, so no prefix */
	if (num < 1024) {
		(void) snprintf(buf, bufsize, "%llu%c", num, *up);
		return;
	}

	/* From 'Kilo' to 'Tera' */
	while ((num >= 1024) && (*up != 'T')) {
		up++; /* next unit of measurement */
		save = num;
		num = (num + 512) >> 10;
	}

	/* check if we should output a fraction. snprintf will round for us */
	if (save % 1024 != 0 && ((save >> 10) < 10)) {
		(void) snprintf(buf, bufsize, "%2.1f%c", ((float)save / 1024),
		    *up);
	} else {
		(void) snprintf(buf, bufsize, "%llu%c", num, *up);
	}
}

/*
 * Print properties of a specific file system resource to fp.
 */
static void
output_fs(FILE *fp, struct zc_fstab *fstab)
{
	zc_fsopt_t *this;

	(void) fprintf(fp, "%s:\n", rt_to_str(RT_FS));
	output_prop(fp, PT_DIR, fstab->zc_fs_dir, true);
	output_prop(fp, PT_SPECIAL, fstab->zc_fs_special, true);
	output_prop(fp, PT_RAW, fstab->zc_fs_raw, true);
	output_prop(fp, PT_TYPE, fstab->zc_fs_type, true);
	(void) fprintf(fp, "\t%s: [", pt_to_str(PT_OPTIONS));

	/*
	 * A file system can have multiple mount options. Walk through the
	 * list of mount options and print each one in turn.
	 */
	for (this = fstab->zc_fs_options; this != NULL;
	    this = this->zc_fsopt_next) {
		/* Handle embedded equal sign. */
		if (strchr(this->zc_fsopt_opt, '=')) {
			(void) fprintf(fp, "\"%s\"", this->zc_fsopt_opt);
		} else {
			(void) fprintf(fp, "%s", this->zc_fsopt_opt);
		}
		if (this->zc_fsopt_next != NULL) {
			(void) fprintf(fp, ",");
		}
	}
	(void) fprintf(fp, "]\n");
}

static void
info_fs(zc_dochandle_t hndl, FILE *fp, cmd_t *cmdp)
{
	struct zc_fstab lookup;
	struct zc_fstab user;
	bool output = false;
	bool dir_match = false;
	bool spec_match = false;
	bool raw_match = false;
	bool type_match = false;

	if (zccfg_setfsent(hndl) != ZC_OK) {
		return;
	}
	/*
	 * If the user did not specify a particular fs resource to display, we
	 * display all fs resources. Else if the user specified a particular fs
	 * to display (cmd_prop_nv_pairs > 0) we stuff the specified fs into
	 * "user" and display the matching fs stored in "lookup", if any.
	 */
	while (zccfg_getfsent(hndl, &lookup) == ZC_OK) {
		if (cmdp->cmd_prop_nv_pairs == 0) {
			output_fs(fp, &lookup);
		} else	if (fill_in_fstab(cmdp, &user, true) == ZC_OK) {
			if (strlen(user.zc_fs_dir) == 0 ||
			    strcmp(user.zc_fs_dir, lookup.zc_fs_dir)
			    == 0) {
				dir_match = true;
			}
			if (strlen(user.zc_fs_type) == 0 ||
			    strcmp(user.zc_fs_type, lookup.zc_fs_type)
			    == 0) {
				type_match = true;
			}
			if (strlen(user.zc_fs_special) == 0 ||
			    strcmp(user.zc_fs_special,
			    lookup.zc_fs_special)
			    == 0) {
				spec_match = true;
			}
			if (strlen(user.zc_fs_raw) == 0 ||
			    strcmp(user.zc_fs_raw, lookup.zc_fs_raw)
			    == 0) {
				raw_match = true;
			}

			/* Output fs if any property matched. */
			if (dir_match || type_match || spec_match ||
			    raw_match) {
				output_fs(fp, &lookup);
				output = true;
			}
		}
		zccfg_free_fs_option_list(lookup.zc_fs_options);
	}
	(void) zccfg_endfsent(hndl);
	/*
	 * If a property n/v pair was specified, warn the user if there was
	 * nothing to output.
	 */
	if (!output && cmdp->cmd_prop_nv_pairs > 0) {
		(void) printf(gettext("No such %s resource.\n"),
		    rt_to_str(RT_FS));
	}
}

static void
output_net(FILE *fp, struct zc_nwiftab *nwiftab)
{
	(void) fprintf(fp, "%s:\n", rt_to_str(RT_NET));
	output_prop(fp, PT_ADDRESS, nwiftab->zc_nwif_address, true);
	output_prop(fp, PT_PHYSICAL, nwiftab->zc_nwif_physical, true);
	if (!global_scope && defrouter_support) {
		output_prop(fp, PT_DEFROUTER, nwiftab->zc_nwif_defrouter, true);
	} else {
		output_prop(fp, PT_DEFROUTER, nwiftab->zc_nwif_defrouter,
		    false);
	}
}

static void
info_net(zc_dochandle_t hndl, FILE *fp, cmd_t *cmdp)
{
	struct zc_nwiftab lookup;
	struct zc_nwiftab user;
	bool output = false;

	if (zccfg_setnwifent(hndl) != ZC_OK) {
		return;
	}

	/*
	 * If the user did not specify a particular net resource to display, we
	 * display all net resources. Else if the user specified a particular
	 * net to display (cmd_prop_nv_pairs > 0) we stuff the specified net
	 * into "user" and display the matching net stored in "lookup", if any.
	 */
	while (zccfg_getnwifent(hndl, &lookup) == ZC_OK) {
		if (cmdp->cmd_prop_nv_pairs == 0) {
			output_net(fp, &lookup);
			continue;
		}
		if (fill_in_nwiftab(cmdp, &user, true) != ZC_OK) {
			continue;
		}
		if (strlen(user.zc_nwif_physical) > 0 &&
		    strcmp(user.zc_nwif_physical,
		    lookup.zc_nwif_physical) != 0) {
			continue;	/* no match */
		}
		if (strlen(user.zc_nwif_address) > 0 &&
		    !zccfg_same_net_address(user.zc_nwif_address,
		    lookup.zc_nwif_address)) {
			continue;	/* no match */
		}
		output_net(fp, &lookup);
		output = true;
	}
	(void) zccfg_endnwifent(hndl);
	/*
	 * If a property n/v pair was specified, warn the user if there was
	 * nothing to output.
	 */
	if (!output && cmdp->cmd_prop_nv_pairs > 0) {
		(void) printf(gettext("No such %s resource.\n"),
		    rt_to_str(RT_NET));
	}
}

static void
output_ipd(FILE *fp, struct zc_fstab *ipdtab)
{
	(void) fprintf(fp, "%s:\n", rt_to_str(RT_IPD));
	output_prop(fp, PT_DIR, ipdtab->zc_fs_dir, true);
}

static void
info_ipd(zc_dochandle_t hndl, FILE *fp, cmd_t *cmdp)
{
	struct zc_fstab lookup;
	struct zc_fstab user;
	bool output = false;

	if (zccfg_setipdent(hndl) != ZC_OK) {
		return;
	}

	/*
	 * If the user did not specify a particular IPD resource to display, we
	 * display all IPD resources. Else if the user specified a particular
	 * IPD to display (cmd_prop_nv_pairs > 0) we stuff the specified IPD
	 * into "user" and display the matching IPD stored in "lookup", if any.
	 */
	while (zccfg_getipdent(hndl, &lookup) == ZC_OK) {
		if (cmdp->cmd_prop_nv_pairs == 0) {
			output_ipd(fp, &lookup);
			continue;
		}
		if (fill_in_ipdtab(cmdp, &user, true) != ZC_OK) {
			continue;
		}
		if (strlen(user.zc_fs_dir) > 0 &&
		    strcmp(user.zc_fs_dir, lookup.zc_fs_dir) != 0) {
			continue;	/* no match */
		}
		output_ipd(fp, &lookup);
		output = true;
	}
	(void) zccfg_endipdent(hndl);
	/*
	 * If a property n/v pair was specified, warn the user if there was
	 * nothing to output.
	 */
	if (!output && cmdp->cmd_prop_nv_pairs > 0) {
		(void) printf(gettext("No such %s resource.\n"),
		    rt_to_str(RT_IPD));
	}
}

static void
output_dev(FILE *fp, struct zc_devtab *devtab)
{
	(void) fprintf(fp, "%s:\n", rt_to_str(RT_DEVICE));
	output_prop(fp, PT_MATCH, devtab->zc_dev_match, true);
}

static void
info_dev(zc_dochandle_t hndl, FILE *fp, cmd_t *cmdp)
{
	struct zc_devtab lookup;
	struct zc_devtab user;
	bool output = false;

	if (zccfg_setdevent(hndl) != ZC_OK) {
		return;
	}

	/*
	 * If the user did not specify a particular dev resource to display, we
	 * display all dev resources. Else if the user specified a particular
	 * dev to display (cmd_prop_nv_pairs > 0) we stuff the specified dev
	 * into "user" and display the matching dev stored in "lookup", if any.
	 */
	while (zccfg_getdevent(hndl, &lookup) == ZC_OK) {
		if (cmdp->cmd_prop_nv_pairs == 0) {
			output_dev(fp, &lookup);
			continue;
		}
		if (fill_in_devtab(cmdp, &user, true) != ZC_OK) {
			continue;
		}
		if (strlen(user.zc_dev_match) > 0 &&
		    strcmp(user.zc_dev_match, lookup.zc_dev_match)
		    != 0) {
			continue;	/* no match */
		}
		output_dev(fp, &lookup);
		output = true;
	}
	(void) zccfg_enddevent(handle);
	/*
	 * If a property n/v pair was specified, warn the user if there was
	 * nothing to output.
	 */
	if (!output && cmdp->cmd_prop_nv_pairs > 0) {
		(void) printf(gettext("No such %s resource.\n"),
		    rt_to_str(RT_DEVICE));
	}
}

static void
output_ds(FILE *fp, struct zc_dstab *dstab)
{
	(void) fprintf(fp, "%s:\n", rt_to_str(RT_DATASET));
	output_prop(fp, PT_NAME, dstab->zc_dataset_name, true);
}

static void
info_ds(zc_dochandle_t hndl, FILE *fp, cmd_t *cmdp)
{
	struct zc_dstab lookup;
	struct zc_dstab user;
	bool output = false;

	if (zccfg_setdsent(hndl) != ZC_OK) {
		return;
	}

	/*
	 * If the user did not specify a particular DS resource to display, we
	 * display all DS resources. Else if the user specified a particular
	 * DS to display (cmd_prop_nv_pairs > 0) we stuff the specified DS
	 * into "user" and display the matching DS stored in "lookup", if any.
	 */
	while (zccfg_getdsent(hndl, &lookup) == ZC_OK) {
		if (cmdp->cmd_prop_nv_pairs == 0) {
			output_ds(fp, &lookup);
			continue;
		}
		if (fill_in_dstab(cmdp, &user, true) != ZC_OK) {
			continue;
		}
		if (strlen(user.zc_dataset_name) > 0 &&
		    strcmp(user.zc_dataset_name,
		    lookup.zc_dataset_name) != 0) {
			continue;	/* no match */
		}
		output_ds(fp, &lookup);
		output = true;
	}
	(void) zccfg_enddsent(hndl);
	/*
	 * If a property n/v pair was specified, warn the user if there was
	 * nothing to output.
	 */
	if (!output && cmdp->cmd_prop_nv_pairs > 0) {
		(void) printf(gettext("No such %s resource.\n"),
		    rt_to_str(RT_DATASET));
	}
}

/*
 * This function displays properties of node-local resources (like net, fs,
 * etc.) and formats the output in such a way that it is clear that the
 * displayed properties belong to resources, which in turn belong to a node
 * resource.
 *
 * Indent output by two tabs relative to node attribute output. The idea is to
 * nest the node resource output further to the right for readability. What we
 * want is something like
 *
 * node:
 * 		physical-host: physicalhost
 * 		hostname: zonehostname
 *		net:
 *			address: 1.2.3.4
 *			physical: bge0
 *		fs:
 *			dir: /mnt/foo
 *			special: /dev/dsk/c0t0d0s0
 *			type: ufs
 *
 */
static void
output_node_local_prop(FILE *fp, int pnum, char *propvalp, bool print_notspec)
{
	char *qstr;

	if (*propvalp != '\0') {
		qstr = quoteit(propvalp);
		if (pnum == PT_SWAP || pnum == PT_LOCKED) {
			(void) fprintf(fp, "\t\t[%s: %s]\n", pt_to_str(pnum),
			    qstr);
		} else {
			(void) fprintf(fp, "\t\t%s: %s\n", pt_to_str(pnum),
			    qstr);
		}
		free(qstr);
	} else if (print_notspec) {
		(void) fprintf(fp, gettext("\t\t%s not specified\n"),
		    pt_to_str(pnum));
	}
}

/*
 * Indent output by one tab relative to node attribute output.
 */
static void
output_local_net(FILE *fp, struct zc_nwiftab *nwiftab)
{
	(void) fprintf(fp, "\t%s:\n", rt_to_str(RT_NET));
	output_node_local_prop(fp, PT_ADDRESS, nwiftab->zc_nwif_address,
	    true);
	output_node_local_prop(fp, PT_PHYSICAL, nwiftab->zc_nwif_physical,
	    true);
	if (defrouter_support) {
		output_node_local_prop(fp, PT_DEFROUTER,
		    nwiftab->zc_nwif_defrouter, true);
	}
}

/*
 * Print settings of a node resource to fp. Note that when we support other
 * node-local resource types (fs, rctl, etc.), a function like output_local_net
 * must be written for each such resource type.
 */
static void
output_node(FILE *fp, struct zc_nodetab *nodetab)
{
	zc_nwifelem_t *nwif_listp;

	(void) fprintf(fp, "%s:\n", rt_to_str(RT_NODE));
	output_prop(fp, PT_PHYSICALHOST, nodetab->zc_nodename, true);
	output_prop(fp, PT_HOSTNAME, nodetab->zc_hostname, true);

	nwif_listp = nodetab->zc_nwif_listp;
	while (nwif_listp != NULL) {
		output_local_net(fp, &(nwif_listp->elem));
		nwif_listp = nwif_listp->next;
	}
}

static void
info_node(zc_dochandle_t hndl, FILE *fp, cmd_t *cmdp)
{
	struct zc_nodetab lookup;
	struct zc_nodetab user;
	bool output = false;

	if (zccfg_setnodeent(hndl) != ZC_OK) {
		return;
	}

	/*
	 * If the user did not specify a particular node resource to display, we
	 * display all node resources. Else if the user specified a particular
	 * node to display (cmd_prop_nv_pairs > 0) we stuff the specified node
	 * into "user" and display the matching node stored in "lookup", if any.
	 */
	while (zccfg_getnodeent(hndl, &lookup) == ZC_OK) {
		if (cmdp->cmd_prop_nv_pairs == 0) {
			output_node(fp, &lookup);
			continue;
		}
		if (fill_in_nodetab(cmdp, &user, true) != ZC_OK) {
			continue;
		}
		if (strlen(user.zc_nodename) > 0 &&
		    strcmp(user.zc_nodename,
		    lookup.zc_nodename) != 0) {
			continue; /* no match */
		}
		if (strlen(user.zc_hostname) > 0 &&
		    strcmp(user.zc_hostname,
		    lookup.zc_hostname) != 0) {
			continue; /* no match */
		}
		output_node(fp, &lookup);
		output = true;
	}
	(void) zccfg_endnodeent(hndl);
	/*
	 * If a property n/v pair was specified, warn the user if there was
	 * nothing to output.
	 */
	if (!output && cmdp->cmd_prop_nv_pairs > 0) {
		(void) printf(gettext("No such %s resource.\n"),
		    rt_to_str(RT_NODE));
	}
}

static void
output_sysid(FILE *fp, struct zc_sysidtab *sysidtab)
{
	struct passwd *pwp = getpwuid(getuid());
	assert(pwp);

	(void) fprintf(fp, "%s:\n", rt_to_str(RT_SYSID));

	/*
	 * Show the encrypted root password only if the user has the
	 * solaris.cluster.admin RBAC profile.
	 */
	if (chkauthattr("solaris.cluster.admin",
	    (const char *)pwp->pw_name) == 1) {
		output_prop(fp, PT_ROOTPASSWD, sysidtab->zc_root_password,
		    true);
	}
	output_prop(fp, PT_NAMESERVICE, sysidtab->zc_name_service, true);
	output_prop(fp, PT_NFS4DOMAIN, sysidtab->zc_nfs4_domain, true);
	output_prop(fp, PT_SECPOLICY, sysidtab->zc_sec_policy, true);
	output_prop(fp, PT_SYSLOCALE, sysidtab->zc_sys_locale, true);
	output_prop(fp, PT_TERMINAL, sysidtab->zc_terminal, true);
	output_prop(fp, PT_TIMEZONE, sysidtab->zc_timezone, true);
}

static void
info_sysid(zc_dochandle_t hndl, FILE *fp)
{
	struct zc_sysidtab lookup;

	bzero(&lookup, sizeof (lookup));
	if (zccfg_setsysident(hndl) != ZC_OK) {
		return;
	}

	if (zccfg_getsysident(hndl, &lookup) == ZC_OK) {
		output_sysid(fp, &lookup);
	}
	(void) zccfg_endsysident(hndl);
}

static void
output_pset(FILE *fp, struct zc_psettab *tabp)
{
	(void) fprintf(fp, "%s:\n", rt_to_str(RT_DCPU));
	if (strcmp(tabp->zc_ncpu_min, tabp->zc_ncpu_max) == 0) {
		(void) fprintf(fp, "\t%s: %s\n", pt_to_str(PT_NCPUS),
		    tabp->zc_ncpu_max);
	} else {
		(void) fprintf(fp, "\t%s: %s-%s\n", pt_to_str(PT_NCPUS),
		    tabp->zc_ncpu_min, tabp->zc_ncpu_max);
	}
	if (tabp->zc_importance[0] != '\0') {
		(void) fprintf(fp, "\t%s: %s\n", pt_to_str(PT_IMPORTANCE),
		    tabp->zc_importance);
	}
}

static void
info_pset(zc_dochandle_t hndl, FILE *fp)
{
	struct zc_psettab lookup;

	if (zccfg_getpsetent(hndl, &lookup) == ZC_OK) {
		output_pset(fp, &lookup);
	}
}

static void
output_pcap(FILE *fp)
{
	uint64_t cap;

	if (zccfg_get_aliased_rctl(handle, ALIAS_CPUCAP, &cap) == ZC_OK) {
		float scaled = (float)cap / 100;
		(void) fprintf(fp, "%s:\n", rt_to_str(RT_PCAP));
		(void) fprintf(fp, "\t[%s: %.2f]\n", pt_to_str(PT_NCPUS),
		    scaled);
	}
}

static void
info_pcap(FILE *fp)
{
	output_pcap(fp);
}

static void
info_aliased_rctl(zc_dochandle_t hndl, FILE *fp, char *alias)
{
	uint64_t limit;

	if (zccfg_get_aliased_rctl(hndl, alias, &limit) == ZC_OK) {
		/* convert memory based properties */
		if (strcmp(alias, ALIAS_MAXSHMMEM) == 0) {
			char buf[128];

			(void) snprintf(buf, sizeof (buf), "%llu", limit);
			bytes_to_units(buf, buf, sizeof (buf));
			(void) fprintf(fp, "[%s: %s]\n", alias, buf);
			return;
		}

		(void) fprintf(fp, "[%s: %llu]\n", alias, limit);
	}
}

static void
output_rctl(FILE *fp, struct zc_rctltab *tabp)
{
	struct zc_rctlvaltab *valp;

	(void) fprintf(fp, "%s:\n", rt_to_str(RT_RCTL));
	output_prop(fp, PT_NAME, tabp->zc_rctl_name, true);
	for (valp = tabp->zc_rctl_valp; valp != NULL;
	    valp = valp->zc_rctlval_next) {
		(void) fprintf(fp, "\t%s: (%s=%s,%s=%s,%s=%s)\n",
		    pt_to_str(PT_VALUE),
		    pt_to_str(PT_PRIV), valp->zc_rctlval_priv,
		    pt_to_str(PT_LIMIT), valp->zc_rctlval_limit,
		    pt_to_str(PT_ACTION), valp->zc_rctlval_action);
	}
}

static void
info_rctl(zc_dochandle_t hndl, FILE *fp, cmd_t *cmdp)
{
	struct zc_rctltab lookup;
	struct zc_rctltab user;
	bool output = false;

	if (zccfg_setrctlent(hndl) != ZC_OK) {
		return;
	}

	/*
	 * If the user did not specify a particular rctl resource to display, we
	 * display all rctl resources. Else if the user specified a particular
	 * rctl to display (cmd_prop_nv_pairs > 0) we stuff the specified rctl
	 * into "user" and display the matching rctl stored in "lookup", if any.
	 */
	while (zccfg_getrctlent(hndl, &lookup) == ZC_OK) {
		if (cmdp->cmd_prop_nv_pairs == 0) {
			output_rctl(fp, &lookup);
			continue;
		}
		if (fill_in_rctltab(cmdp, &user, true) != ZC_OK) {
			continue;
		}
		if ((strlen(user.zc_rctl_name) > 0 &&
		    strcmp(user.zc_rctl_name, lookup.zc_rctl_name)
		    != 0)) {
			continue; /* no match */
		}

		output_rctl(fp, &lookup);
		output = true;
		zccfg_free_rctl_value_list(lookup.zc_rctl_valp);
	}
	(void) zccfg_endrctlent(hndl);
	/*
	 * If a property n/v pair was specified, warn the user if there was
	 * nothing to output.
	 */
	if (!output && cmdp->cmd_prop_nv_pairs > 0) {
		(void) printf(gettext("No such %s resource.\n"),
		    rt_to_str(RT_RCTL));
	}
}

static void
output_mcap(FILE *fp, struct zc_mcaptab *tabp, int showswap,
    uint64_t maxswap, int showlocked, uint64_t maxlocked)
{
	char buf[128];

	(void) fprintf(fp, "%s:\n", rt_to_str(RT_MCAP));
	if (tabp->zc_physmem_cap[0] != '\0') {
		bytes_to_units(tabp->zc_physmem_cap, buf, sizeof (buf));
		output_prop(fp, PT_PHYSICAL, buf, true);
	}

	if (showswap == ZC_OK) {
		(void) snprintf(buf, sizeof (buf), "%llu", maxswap);
		bytes_to_units(buf, buf, sizeof (buf));
		output_prop(fp, PT_SWAP, buf, true);
	}

	if (showlocked == ZC_OK) {
		(void) snprintf(buf, sizeof (buf), "%llu", maxlocked);
		bytes_to_units(buf, buf, sizeof (buf));
		output_prop(fp, PT_LOCKED, buf, true);
	}
}

static void
info_mcap(zc_dochandle_t hndl, FILE *fp)
{
	int res1;
	int res2;
	int res3;
	uint64_t swap_limit;
	uint64_t locked_limit;
	struct zc_mcaptab lookup;

	bzero(&lookup, sizeof (lookup));
	res1 = zccfg_getmcapent(hndl, &lookup);
	res2 = zccfg_get_aliased_rctl(hndl, ALIAS_MAXSWAP, &swap_limit);
	res3 = zccfg_get_aliased_rctl(hndl, ALIAS_MAXLOCKEDMEM,
	    &locked_limit);

	if (res1 == ZC_OK || res2 == ZC_OK || res3 == ZC_OK) {
		output_mcap(fp, &lookup, res2, swap_limit, res3, locked_limit);
	}
}

/*
 * Displays the configuration of the zone if in the global scope. Else displays
 * the resource setting. If the output is too large to fit in a terminal window
 * and we're in interactive mode, 'PAGER' is used to page the display.
 */
void
info_func(cmd_t *cmdp)
{
	FILE *fp = stdout;
	bool need_to_close = false;
	char *pagerp;
	int res1;
	int res2;
	uint64_t swap_limit;
	uint64_t locked_limit;

	assert(cmdp != NULL);

	if (initialize(true) != ZC_OK) {
		return;
	}

	if (interactive_mode) {
		if ((pagerp = getenv("PAGER")) == NULL) {
			pagerp = PAGER;
		}
		if ((fp = popen(pagerp, "w")) != NULL) {
			need_to_close = true;
		} else {
			fp = stdout;
		}
		setbuf(fp, NULL);
	}

	if (!global_scope) {
		/* do resource scope output */
		switch (resource_scope) {
		case RT_FS:
			output_fs(fp, &in_prog_fstab);
			break;
		case RT_IPD:
			output_ipd(fp, &in_prog_ipdtab);
			break;
		case RT_NET:
			output_net(fp, &in_prog_nwiftab);
			break;
		case RT_DATASET:
			output_ds(fp, &in_prog_dstab);
			break;
		case RT_NODE:
			output_node(fp, &in_prog_nodetab);
			break;
		case RT_SYSID:
			output_sysid(fp, &in_prog_sysidtab);
			break;
		case RT_DEVICE:
			output_dev(fp, &in_prog_devtab);
			break;
		case RT_DCPU:
			output_pset(fp, &in_prog_psettab);
			break;
		case RT_PCAP:
			output_pcap(fp);
			break;
		case RT_RCTL:
			output_rctl(fp, &in_prog_rctltab);
			break;
		case RT_MCAP:
			res1 = zccfg_get_aliased_rctl(handle,
			    ALIAS_MAXSWAP, &swap_limit);
			res2 = zccfg_get_aliased_rctl(handle,
			    ALIAS_MAXLOCKEDMEM, &locked_limit);
			output_mcap(fp, &in_prog_mcaptab, res1, swap_limit,
			    res2, locked_limit);
			break;
		}
		goto cleanup;
	}

	switch (cmdp->cmd_res_type) {
	case RT_UNKNOWN:
		/* output everything */
		info_zonename(handle, fp);
		info_zonepath(handle, fp);
		info_autoboot(handle, fp);
		info_brand(handle, fp);
		info_bootargs(handle, fp);
		info_pool(handle, fp);
		info_limitpriv(handle, fp);
		info_sched(handle, fp);
		info_iptype(handle, fp);
		info_aliased_rctl(handle, fp, ALIAS_MAXLWPS);
		info_aliased_rctl(handle, fp, ALIAS_MAXSHMMEM);
		info_aliased_rctl(handle, fp, ALIAS_MAXSHMIDS);
		info_aliased_rctl(handle, fp, ALIAS_MAXMSGIDS);
		info_aliased_rctl(handle, fp, ALIAS_MAXSEMIDS);
		info_aliased_rctl(handle, fp, ALIAS_SHARES);
		info_clprivnet(handle, fp);
		info_ipd(handle, fp, cmdp);
		info_fs(handle, fp, cmdp);
		info_net(handle, fp, cmdp);
		info_dev(handle, fp, cmdp);
		info_ds(handle, fp, cmdp);
		info_pset(handle, fp);
		info_pcap(fp);
		info_mcap(handle, fp);
		info_rctl(handle, fp, cmdp);
		info_sysid(handle, fp);
		info_node(handle, fp, cmdp);
		break;
	case RT_ZONENAME:
		info_zonename(handle, fp);
		break;
	case RT_ZONEPATH:
		info_zonepath(handle, fp);
		break;
	case RT_AUTOBOOT:
		info_autoboot(handle, fp);
		break;
	case RT_CLPRIVNET:
		info_clprivnet(handle, fp);
		break;
	case RT_BRAND:
		info_brand(handle, fp);
		break;
	case RT_POOL:
		info_pool(handle, fp);
		break;
	case RT_LIMITPRIV:
		info_limitpriv(handle, fp);
		break;
	case RT_BOOTARGS:
		info_bootargs(handle, fp);
		break;
	case RT_SCHED:
		info_sched(handle, fp);
		break;
	case RT_IPTYPE:
		info_iptype(handle, fp);
		break;
	case RT_MAXLWPS:
		info_aliased_rctl(handle, fp, ALIAS_MAXLWPS);
		break;
	case RT_MAXSHMMEM:
		info_aliased_rctl(handle, fp, ALIAS_MAXSHMMEM);
		break;
	case RT_MAXSHMIDS:
		info_aliased_rctl(handle, fp, ALIAS_MAXSHMIDS);
		break;
	case RT_MAXMSGIDS:
		info_aliased_rctl(handle, fp, ALIAS_MAXMSGIDS);
		break;
	case RT_MAXSEMIDS:
		info_aliased_rctl(handle, fp, ALIAS_MAXSEMIDS);
		break;
	case RT_SHARES:
		info_aliased_rctl(handle, fp, ALIAS_SHARES);
		break;
	case RT_FS:
		info_fs(handle, fp, cmdp);
		break;
	case RT_IPD:
		info_ipd(handle, fp, cmdp);
		break;
	case RT_NET:
		info_net(handle, fp, cmdp);
		break;
	case RT_DEVICE:
		info_dev(handle, fp, cmdp);
		break;
	case RT_RCTL:
		info_rctl(handle, fp, cmdp);
		break;
	case RT_DATASET:
		info_ds(handle, fp, cmdp);
		break;
	case RT_DCPU:
		info_pset(handle, fp);
		break;
	case RT_PCAP:
		info_pcap(fp);
		break;
	case RT_MCAP:
		info_mcap(handle, fp);
		break;
	case RT_NODE:
		info_node(handle, fp, cmdp);
		break;
	case RT_SYSID:
		info_sysid(handle, fp);
		break;
	default:
		zone_perror(rt_to_str(cmdp->cmd_res_type), ZC_NO_RESOURCE_TYPE,
		    true);
		break;
	}
cleanup:
	if (need_to_close) {
		(void) pclose(fp);
	}
}

/*
 * Helper function to insert a net resource at the end of the list of net
 * resources of the current node resource (in_prog_nodetab).
 */
static int
add_net_to_node(struct zc_nwiftab *tabp)
{
	zc_nwifelem_t *elemp;
	zc_nwifelem_t *startp;

	elemp = calloc(1, sizeof (zc_nwifelem_t));
	if (elemp == NULL) {
		return (ZC_NOMEM);
	}

	(void) strlcpy(elemp->elem.zc_nwif_address,
	    tabp->zc_nwif_address,
	    sizeof (tabp->zc_nwif_address));
	(void) strlcpy(elemp->elem.zc_nwif_physical,
	    tabp->zc_nwif_physical,
	    sizeof (tabp->zc_nwif_physical));
	elemp->elem.zc_nwif_defrouter[0] = '\0';
	(void) strlcpy(elemp->elem.zc_nwif_defrouter,
	    tabp->zc_nwif_defrouter,
	    sizeof (tabp->zc_nwif_defrouter));
	elemp->next = NULL;

	if (in_prog_nodetab.zc_nwif_listp == NULL) {
		in_prog_nodetab.zc_nwif_listp = elemp;
	} else {
		startp = in_prog_nodetab.zc_nwif_listp;
		while (startp->next != NULL) {
			startp = startp->next;
		}
		startp->next = elemp;
	}
	return (ZC_OK);
}

/*
 * Helper function to replace an existing net entry (oldtabp) in the
 * current node resource (in_prog_nodetab), with the new net resource
 * entry (newtabp).
 */
static int
modify_local_net(struct zc_nwiftab *oldtabp,
    struct zc_nwiftab *newtabp)
{
	zc_nwifelem_t *matchp;

	matchp = in_prog_nodetab.zc_nwif_listp;
	assert(matchp != NULL);

	while (matchp) {
		if (strcmp(matchp->elem.zc_nwif_address,
		    oldtabp->zc_nwif_address) == 0 &&
		    strcmp(matchp->elem.zc_nwif_physical,
		    oldtabp->zc_nwif_physical) == 0 &&
		    strcmp(matchp->elem.zc_nwif_defrouter,
		    oldtabp->zc_nwif_defrouter) == 0) {
			break;
		}
		matchp = matchp->next;
	}

	assert(matchp != NULL);
	(void) strlcpy(matchp->elem.zc_nwif_address,
	    newtabp->zc_nwif_address,
	    sizeof (matchp->elem.zc_nwif_address));
	(void) strlcpy(matchp->elem.zc_nwif_physical,
	    newtabp->zc_nwif_physical,
	    sizeof (matchp->elem.zc_nwif_physical));
	(void) strlcpy(matchp->elem.zc_nwif_defrouter,
	    newtabp->zc_nwif_defrouter,
	    sizeof (matchp->elem.zc_nwif_defrouter));
	return (ZC_OK);
}

/*
 * Helper function to delete a net resource (tabp) from a node resource
 * (nodep).
 */
static int
delete_local_net(struct zc_nodetab *nodep, struct zc_nwiftab *tabp)
{
	zc_nwifelem_t *listp;
	zc_nwifelem_t *temp;
	zc_nwifelem_t *next;

	listp = nodep->zc_nwif_listp;
	for (temp = listp; temp != NULL; temp = temp->next) {
		if (strcmp(temp->elem.zc_nwif_address,
		    tabp->zc_nwif_address) == 0) {
			next = temp->next;
			if (temp == nodep->zc_nwif_listp) {
				nodep->zc_nwif_listp = next;
			} else {
				listp->next = next;
			}
			free(temp);
			return (ZC_OK);
		} else {
			listp = temp;
		}
	}
	return (ZC_NO_PROPERTY_ID);
}

/*
 * Helper function for node_scope_end_func() and end_func().
 * Checks the existence of a given property and emits an error message
 * if the property is not specified.
 */
static int
end_check_reqd(char *attr, int pt, bool *validation_failed)
{
	if (attr == NULL || strlen(attr) == 0) {
		*validation_failed = true;
		zerr(gettext("%s not specified"), pt_to_str(pt));
		return (ZC_ERR);
	}
	return (ZC_OK);
}

/*
 * end_func() for the 'node scope'. Called from end_func() if we're in a
 * resource scope 'within' the node scope. Its purpose is identical to
 * that of end_func(), but the change is applied to the particular node
 * resource we're modifying.
 */
static void
node_scope_end_func(void)
{
	int err = ZC_OK;
	bool validation_failed = false;
	struct zc_nwiftab tmp_nwiftab;

	assert(node_scope == true);
	assert(end_op == CMD_ADD || end_op == CMD_SELECT);

	switch (resource_scope) {
	case RT_NET:
		/*
		 * Make sure the resource specification is complete.
		 * We need to remove the check for address property in the
		 * future when we support exclusive IP type.
		 */
		(void) end_check_reqd(in_prog_nwiftab.zc_nwif_address,
		    PT_ADDRESS, &validation_failed);
		(void) end_check_reqd(in_prog_nwiftab.zc_nwif_physical,
		    PT_PHYSICAL, &validation_failed);
		if (validation_failed) {
			saw_error = true;
			return;
		}

		if (end_op == CMD_ADD) {
			/* Check to make sure this isn't a duplicate. */
			bzero(&tmp_nwiftab, sizeof (tmp_nwiftab));
			(void) strlcpy(tmp_nwiftab.zc_nwif_address,
			    in_prog_nwiftab.zc_nwif_address,
			    sizeof (tmp_nwiftab.zc_nwif_address));
			err = lookup_nwif_node(&in_prog_nodetab,
			    &tmp_nwiftab);
			if (err == ZC_OK) {
				zerr(gettext("A %s resource with the "
				    "%s '%s' already exists."),
				    rt_to_str(RT_NET),
				    pt_to_str(PT_ADDRESS),
				    in_prog_nwiftab.zc_nwif_address);
				saw_error = true;
				return;
			}
			err = add_net_to_node(&in_prog_nwiftab);
		} else {
			err = modify_local_net(&old_nwiftab, &in_prog_nwiftab);
		}
		break;
	default:
		zerr(gettext("Zone custer does not support the %s "
		    "resource type in the %s scope."),
		    rt_to_str(resource_scope), rt_to_str(RT_NODE));
		break;
	}

	if (err != ZC_OK) {
		zone_perror(zone, err, true);
	} else {
		need_to_commit = true;
		resource_scope = RT_NODE;
	}
}

/*
 * Helper function for end_func() to fill in default sysidcfg parameters.
 * We provide/fetch default values for:
 *
 * system_locale, terminal, timezone, nfs4_domain, and security_policy,
 * and name_service.
 *
 * Note that we set default values only if the user didn't specify them.
 */
static void
fill_in_default_sysid_params(struct zc_sysidtab *tabp)
{
	FILE *fp;
	char line[MAXNAMELEN]; /* to hold a line from /etc/sysidcfg */
	char *str = NULL;

	/* Use global zone's locale setting by default */
	if (strlen(tabp->zc_sys_locale) == 0) {
		(void) strcpy(tabp->zc_sys_locale, setlocale(LC_ALL, ""));
	}

	/* Set security policy to NONE by default. */
	if (strlen(tabp->zc_sec_policy) == 0) {
		(void) strcpy(tabp->zc_sec_policy, "NONE");
	}

	/* Set nfs4_domain to dynamic by default. */
	if (strlen(tabp->zc_nfs4_domain) == 0) {
		(void) strcpy(tabp->zc_nfs4_domain, "dynamic");
	}

	/* Set value of terminal if none is specified. */
	if (strlen(tabp->zc_terminal) == 0) {
		/* Get it from environment if it's set */
		str = getenv("TERM");
		if (str && (strlen(str) > 0)) {
			(void) strcpy(tabp->zc_terminal, str);
		} else {
			/* TERM not set in environment. Just use xterm. */
			(void) strcpy(tabp->zc_terminal, "xterm");
		}
	}

	/* Set timezone if none is specified. */
	if (strlen(tabp->zc_timezone) == 0) {
		str = getenv("TZ");
		if (str) {
			(void) strcpy(tabp->zc_timezone, str);
		}
	}

	/*
	 * If name_service is not specified, try to get it from the global
	 * zone's /etc/sysidcfg file.
	 */
	if (strlen(tabp->zc_name_service) == 0) {
		if ((fp = fopen("/etc/sysidcfg", "r")) == NULL) {
			/*
			 * Can't determine name_service from global zone.
			 * Set it to NONE.
			 */
			(void) strcpy(tabp->zc_name_service, "NONE");
			return;
		}

		str = NULL;
		while (fgets(line, sizeof (line), fp)) {
			if ((str = strstr(line, "name_service="))) {
				/* We want the string following '=' sign. */
				(void) strlcpy(tabp->zc_name_service,
				    str + strlen("name_service="),
				    sizeof (tabp->zc_name_service));
				break;
			}
		}
		(void) fclose(fp);

		/* Global zone doesn't specify name service. Set to NONE */
		if (str == NULL) {
			(void) strcpy(tabp->zc_name_service, "NONE");
		}
	}
}

/*
 * Resource specification is completed with the 'end' command. The end_func
 * validates the resource specification; if that checks out, the resource is
 * inserted to the XML tree pointed to by the 'handle' global variable.
 */
void
end_func(cmd_t *cmdp)
{
	int arg;
	int err = ZC_OK;
	bool validation_failed = false;
	struct zc_fstab tmp_fstab;
	struct zc_nwiftab tmp_nwiftab;
	struct zc_devtab tmp_devtab;
	struct zc_rctltab tmp_rctltab;
	struct zc_dstab tmp_dstab;
	struct zc_nodetab tmp_nodetab;
	int res1;
	int res2;
	int res3;
	uint64_t swap_limit;
	uint64_t locked_limit;
	uint64_t proc_cap;

	assert(cmdp != NULL);

	optind = 0;
	if ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_END);
			return;
		default:
			short_usage(CMD_END);
			return;
		}
	}
	if (optind != cmdp->cmd_argc) {
		short_usage(CMD_END);
		return;
	}

	if (global_scope) {
		scope_usage(CMD_END);
		return;
	} else if (node_scope && resource_scope != RT_NODE) {
		node_scope_end_func();
		return;
	}

	/*
	 * If we entered the node scope via select or add commands, and then
	 * went into a resource scope within the node scope, restore the
	 * initial command that got us into the node scope in the first place.
	 */
	if (node_scope && resource_scope == RT_NODE) {
		end_op = saved_end_op;
	}

	assert(end_op == CMD_ADD || end_op == CMD_SELECT);

	switch (resource_scope) {
	case RT_FS:
		/* First make sure everything was filled in. */
		if (end_check_reqd(in_prog_fstab.zc_fs_dir,
		    PT_DIR, &validation_failed) == ZC_OK) {
			if (in_prog_fstab.zc_fs_dir[0] != '/') {
				zerr(gettext("%s %s is not an absolute path."),
				    pt_to_str(PT_DIR),
				    in_prog_fstab.zc_fs_dir);
				validation_failed = true;
			}
		}

		(void) end_check_reqd(in_prog_fstab.zc_fs_special,
		    PT_SPECIAL, &validation_failed);

		if (in_prog_fstab.zc_fs_raw[0] != '\0' &&
		    in_prog_fstab.zc_fs_raw[0] != '/') {
			zerr(gettext("%s %s is not an absolute path."),
			    pt_to_str(PT_RAW),
			    in_prog_fstab.zc_fs_raw);
			validation_failed = true;
		}

		(void) end_check_reqd(in_prog_fstab.zc_fs_type, PT_TYPE,
		    &validation_failed);

		if (validation_failed) {
			saw_error = true;
			return;
		}

		if (end_op == CMD_ADD) {
			/* Make sure there isn't already one like this. */
			bzero(&tmp_fstab, sizeof (tmp_fstab));
			(void) strlcpy(tmp_fstab.zc_fs_dir,
			    in_prog_fstab.zc_fs_dir,
			    sizeof (tmp_fstab.zc_fs_dir));
			err = zccfg_lookup_filesystem(handle, &tmp_fstab);
			zccfg_free_fs_option_list(
			    tmp_fstab.zc_fs_options);
			if (err == ZC_OK) {
				zerr(gettext("A %s resource "
				    "with the %s '%s' already exists."),
				    rt_to_str(RT_FS), pt_to_str(PT_DIR),
				    in_prog_fstab.zc_fs_dir);
				saw_error = true;
				return;
			}
			err = zccfg_add_filesystem(handle,
			    &in_prog_fstab);
		} else {
			err = zccfg_modify_filesystem(handle, &old_fstab,
			    &in_prog_fstab);
		}
		zccfg_free_fs_option_list(in_prog_fstab.zc_fs_options);
		in_prog_fstab.zc_fs_options = NULL;
		break;
	case RT_IPD:
		/* First make sure everything was filled in. */
		if (end_check_reqd(in_prog_ipdtab.zc_fs_dir, PT_DIR,
		    &validation_failed) == ZC_OK) {
			if (in_prog_ipdtab.zc_fs_dir[0] != '/') {
				zerr(gettext("%s %s is not an absolute path."),
				    pt_to_str(PT_DIR),
				    in_prog_ipdtab.zc_fs_dir);
				validation_failed = true;
			}
		}
		if (validation_failed) {
			saw_error = true;
			return;
		}

		if (end_op == CMD_ADD) {
			/* Make sure there isn't already one like this. */
			bzero(&tmp_fstab, sizeof (tmp_fstab));
			(void) strlcpy(tmp_fstab.zc_fs_dir,
			    in_prog_ipdtab.zc_fs_dir,
			    sizeof (tmp_fstab.zc_fs_dir));
			err = zccfg_lookup_ipd(handle, &tmp_fstab);

			if (err == ZC_OK) {
				zerr(gettext("An %s resource "
				    "with the %s '%s' already exists."),
				    rt_to_str(RT_IPD), pt_to_str(PT_DIR),
				    in_prog_ipdtab.zc_fs_dir);
				saw_error = true;
				return;
			} else {
				err = zccfg_add_ipd(handle, &in_prog_ipdtab);
			}
		} else {
			err = zccfg_modify_ipd(handle, &old_ipdtab,
			    &in_prog_ipdtab);
		}
		break;
	case RT_NET:
		/*
		 * First make sure everything was filled in.
		 * Since we don't know whether IP will be shared
		 * or exclusive here, some checks are deferred until
		 * the verify command.
		 */
		(void) end_check_reqd(in_prog_nwiftab.zc_nwif_physical,
		    PT_PHYSICAL, &validation_failed);

		if (validation_failed) {
			saw_error = true;
			return;
		}
		if (end_op == CMD_ADD) {
			/* Make sure there isn't already one like this. */
			bzero(&tmp_nwiftab, sizeof (tmp_nwiftab));
			(void) strlcpy(tmp_nwiftab.zc_nwif_physical,
			    in_prog_nwiftab.zc_nwif_physical,
			    sizeof (tmp_nwiftab.zc_nwif_physical));
			(void) strlcpy(tmp_nwiftab.zc_nwif_address,
			    in_prog_nwiftab.zc_nwif_address,
			    sizeof (tmp_nwiftab.zc_nwif_address));
			if (zccfg_lookup_nwif(handle, &tmp_nwiftab)
			    == ZC_OK) {
				zerr(gettext("A %s resource with the %s '%s', "
				    "and %s '%s' already exists."),
				    rt_to_str(RT_NET),
				    pt_to_str(PT_PHYSICAL),
				    in_prog_nwiftab.zc_nwif_physical,
				    pt_to_str(PT_ADDRESS),
				    in_prog_nwiftab.zc_nwif_address);
				saw_error = true;
				return;
			}
			err = zccfg_add_nwif(handle, &in_prog_nwiftab);
		} else {
			err = zccfg_modify_nwif(handle, &old_nwiftab,
			    &in_prog_nwiftab);
		}
		break;
	case RT_DEVICE:
		/* First make sure everything was filled in. */
		(void) end_check_reqd(in_prog_devtab.zc_dev_match,
		    PT_MATCH, &validation_failed);

		if (validation_failed) {
			saw_error = true;
			return;
		}

		if (end_op == CMD_ADD) {
			/* Make sure there isn't already one like this. */
			(void) strlcpy(tmp_devtab.zc_dev_match,
			    in_prog_devtab.zc_dev_match,
			    sizeof (tmp_devtab.zc_dev_match));
			if (zccfg_lookup_dev(handle, &tmp_devtab)
			    == ZC_OK) {
				zerr(gettext("A %s resource with the %s '%s' "
				    "already exists."), rt_to_str(RT_DEVICE),
				    pt_to_str(PT_MATCH),
				    in_prog_devtab.zc_dev_match);
				saw_error = true;
				return;
			}
			err = zccfg_add_dev(handle, &in_prog_devtab);
		} else {
			err = zccfg_modify_dev(handle, &old_devtab,
			    &in_prog_devtab);
		}
		break;
	case RT_RCTL:
		/* First make sure everything was filled in. */
		(void) end_check_reqd(in_prog_rctltab.zc_rctl_name,
		    PT_NAME, &validation_failed);

		if (in_prog_rctltab.zc_rctl_valp == NULL) {
			zerr(gettext("no %s specified"), pt_to_str(PT_VALUE));
			validation_failed = true;
		}

		if (validation_failed) {
			saw_error = true;
			return;
		}

		if (end_op == CMD_ADD) {
			/* Make sure there isn't already one like this. */
			(void) strlcpy(tmp_rctltab.zc_rctl_name,
			    in_prog_rctltab.zc_rctl_name,
			    sizeof (tmp_rctltab.zc_rctl_name));
			tmp_rctltab.zc_rctl_valp = NULL;
			err = zccfg_lookup_rctl(handle, &tmp_rctltab);
			zccfg_free_rctl_value_list(
			    tmp_rctltab.zc_rctl_valp);
			if (err == ZC_OK) {
				zerr(gettext("A %s resource "
				    "with the %s '%s' already exists."),
				    rt_to_str(RT_RCTL), pt_to_str(PT_NAME),
				    in_prog_rctltab.zc_rctl_name);
				saw_error = true;
				return;
			}
			err = zccfg_add_rctl(handle, &in_prog_rctltab);
		} else {
			err = zccfg_modify_rctl(handle, &old_rctltab,
			    &in_prog_rctltab);
		}
		if (err == ZC_OK) {
			zccfg_free_rctl_value_list(
			    in_prog_rctltab.zc_rctl_valp);
			    in_prog_rctltab.zc_rctl_valp = NULL;
		}
		break;
	case RT_ATTR:
		/* We do not support the attr resource at this time. */
		break;
	case RT_DATASET:
		/* First make sure everything was filled in. */
		if (strlen(in_prog_dstab.zc_dataset_name) == 0) {
			zerr("%s %s", pt_to_str(PT_NAME),
			    gettext("not specified"));
			saw_error = true;
			validation_failed = true;
		}
		if (validation_failed) {
			return;
		}

		if (end_op == CMD_ADD) {
			/* Make sure there isn't already one like this. */
			bzero(&tmp_dstab, sizeof (tmp_dstab));
			(void) strlcpy(tmp_dstab.zc_dataset_name,
			    in_prog_dstab.zc_dataset_name,
			    sizeof (tmp_dstab.zc_dataset_name));
			err = zccfg_lookup_ds(handle, &tmp_dstab);
			if (err == ZC_OK) {
				zerr(gettext("A %s resource "
				    "with the %s '%s' already exists."),
				    rt_to_str(RT_DATASET), pt_to_str(PT_NAME),
				    in_prog_dstab.zc_dataset_name);
				saw_error = true;
				return;
			}
			err = zccfg_add_ds(handle, &in_prog_dstab);
		} else {
			err = zccfg_modify_ds(handle, &tmp_dstab,
			    &in_prog_dstab);
		}
		break;
	case RT_SYSID:
		/*
		 * Root password MUST be specified.
		 */
		(void) end_check_reqd(in_prog_sysidtab.zc_root_password,
		    PT_ROOTPASSWD, &validation_failed);
		if (validation_failed) {
			saw_error = true;
			return;
		}

		/*
		 * If necessary, fill in default values for other
		 * properties..
		 */
		fill_in_default_sysid_params(&in_prog_sysidtab);

		if (end_op == CMD_ADD) {
			err = zccfg_add_sysid(handle, &in_prog_sysidtab);
		} else {
			err = zccfg_modify_sysid(handle, &old_sysidtab,
			    &in_prog_sysidtab);
		}
		free(in_prog_sysidtab.zc_root_password);
		in_prog_sysidtab.zc_root_password = NULL;
		break;
	case RT_NODE:
		/*
		 * Make sure all node properties are specified.
		 */
		(void) end_check_reqd(in_prog_nodetab.zc_nodename,
		    PT_PHYSICALHOST, &validation_failed);
		(void) end_check_reqd(in_prog_nodetab.zc_hostname,
		    PT_HOSTNAME, &validation_failed);

		/*
		 * Make sure the node resource contains at least one net
		 * resource.
		 */
		if (in_prog_nodetab.zc_nwif_listp == NULL) {
			zerr(gettext("%s must have at least one %s resource"),
			    rt_to_str(RT_NODE), rt_to_str(RT_NET));
			validation_failed = true;
		}

		if (validation_failed) {
			saw_error = true;
			return;
		}

		if (end_op == CMD_ADD) {
			/* Make sure this is not a duplicate entry. */
			bzero(&tmp_nodetab, sizeof (tmp_nodetab));
			(void) strlcpy(tmp_nodetab.zc_nodename,
			    in_prog_nodetab.zc_nodename,
			    sizeof (tmp_nodetab.zc_nodename));
			(void) strlcpy(tmp_nodetab.zc_hostname,
			    in_prog_nodetab.zc_hostname,
			    sizeof (tmp_nodetab.zc_hostname));
			if (zccfg_lookup_node(handle,
			    &tmp_nodetab) == ZC_OK) {
				zerr(gettext("A %s resource with the %s '%s', "
				    "and %s '%s' already exists."),
				    rt_to_str(RT_NODE),
				    pt_to_str(PT_NAME),
				    in_prog_nodetab.zc_nodename,
				    pt_to_str(PT_HOSTNAME),
				    in_prog_nodetab.zc_hostname);
				saw_error = true;
				return;
			}
			err = zccfg_add_node(handle, &in_prog_nodetab);
		} else {
			err = zccfg_modify_node(handle, &old_nodetab,
			    &in_prog_nodetab);
		}
		zccfg_free_nwif_list(in_prog_nodetab.zc_nwif_listp);
		in_prog_nodetab.zc_nwif_listp = NULL;
		break;
	case RT_DCPU:
		/* Make sure everything was filled in. */
		if (end_check_reqd(in_prog_psettab.zc_ncpu_min,
		    PT_NCPUS, &validation_failed) != ZC_OK) {
			saw_error = true;
			return;
		}

		if (end_op == CMD_ADD) {
			err = zccfg_add_pset(handle, &in_prog_psettab);
		} else {
			err = zccfg_modify_pset(handle, &in_prog_psettab);
		}
		break;
	case RT_PCAP:
		/* Make sure everything was filled in. */
		if (zccfg_get_aliased_rctl(handle, ALIAS_CPUCAP, &proc_cap)
		    != ZC_OK) {
			zerr(gettext("%s not specified"), pt_to_str(PT_NCPUS));
			saw_error = true;
			validation_failed = true;
			return;
		}
		err = ZC_OK;
		break;
	case RT_MCAP:
		/* Make sure everything was filled in. */
		res1 = strlen(in_prog_mcaptab.zc_physmem_cap) == 0 ?
		    ZC_ERR : ZC_OK;
		res2 = zccfg_get_aliased_rctl(handle, ALIAS_MAXSWAP,
		    &swap_limit);
		res3 = zccfg_get_aliased_rctl(handle, ALIAS_MAXLOCKEDMEM,
		    &locked_limit);

		if (res1 != ZC_OK && res2 != ZC_OK && res3 != ZC_OK) {
			zerr(gettext("No property was specified.  One of %s, "
			    "%s or %s is required."), pt_to_str(PT_PHYSICAL),
			    pt_to_str(PT_SWAP), pt_to_str(PT_LOCKED));
			saw_error = true;
			return;
		}

		/* if phys & locked are both set, verify locked <= phys */
		if (res1 == ZC_OK && res3 == ZC_OK) {
			uint64_t phys_limit;
			char *endp;

			phys_limit = strtoull(
			    in_prog_mcaptab.zc_physmem_cap, &endp, 10);
			if (phys_limit < locked_limit) {
				zerr(gettext("The %s cap must be less than or "
				    "equal to the %s cap."),
				    pt_to_str(PT_LOCKED),
				    pt_to_str(PT_PHYSICAL));
				saw_error = true;
				return;
			}
		}

		err = ZC_OK;
		if (res1 == ZC_OK) {
			/*
			 * We could be ending from either an add operation
			 * or a select operation.  Since all of the properties
			 * within this resource are optional, we always use
			 * modify on the mcap entry.  zccfg_modify_mcap()
			 * will handle both adding and modifying a memory cap.
			 */
			err = zccfg_modify_mcap(handle, &in_prog_mcaptab);
		} else if (end_op == CMD_SELECT) {
			/*
			 * If we're ending from a select and the physical
			 * memory cap is empty then the user could have cleared
			 * the physical cap value, so try to delete the entry.
			 */
			(void) zccfg_delete_mcap(handle);
		}
		break;
	default:
		zone_perror(rt_to_str(resource_scope), ZC_NO_RESOURCE_TYPE,
		    true);
		return;
	}

	if (err != ZC_OK) {
		zone_perror(zone, err, true);
	} else {
		need_to_commit = true;
		node_scope = false;
		global_scope = true;

		/* Reset end_op and saved_end_op to invalid command numbers */
		end_op = saved_end_op = -1;
	}
}

/*
 * commits the configuration to persistent storage.
 *
 * We first commit the configuration to the CCR. If that succeeds, we then
 * invoke zc_helper on each node in the nodelist of the zone cluster to
 * commit the Solaris zone configuration.
 */
void
commit_func(cmd_t *cmdp)
{
	int arg;

	assert(cmdp != NULL);
	optind = 0;
	if ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_COMMIT);
			return;
		default:
			short_usage(CMD_COMMIT);
			return;
		}
	}
	if (optind != cmdp->cmd_argc) {
		short_usage(CMD_COMMIT);
		return;
	}

	if (zc_is_read_only(CMD_COMMIT)) {
		return;
	}

	/*
	 * Disallow commit command if not in global scope.
	 */
	if (!global_scope) {
		short_usage(CMD_COMMIT);
		return;
	}

	cmdp->cmd_argc = 1;
	/*
	 * cmd_arg normally comes from a strdup() in the lexer, and the
	 * whole cmd structure and its (char *) attributes are freed at
	 * the completion of each command, so the strdup() below is needed
	 * to match this and prevent a core dump from trying to free()
	 * something that can't be.
	 */
	if ((cmdp->cmd_argv[0] = strdup("save")) == NULL) {
		zone_perror(zone, ZC_NOMEM, true);
		exit(ZC_ERR);
	}
	cmdp->cmd_argv[1] = NULL;
	verify_func(cmdp);
}

/*
 * Reverts settings to the last saved configuration.
 * TODO: Implement this function.
 */
void
revert_func(cmd_t *cmdp)
{
}

/*
 * Handler of the select command. Select command takes us into the scope of
 * the specified resource matching the value of some property of the resource.
 * The select command is used to edit the properties of a resource.
 */
void
select_func(cmd_t *cmdp)
{
	int type;
	int err;
	int res;
	uint64_t limit; /* for swap or max locked memory */
	uint64_t tmp;

	if (zc_is_read_only(CMD_SELECT)) {
		return;
	}

	if (global_scope || node_scope) {
		global_scope = false;
		resource_scope = cmdp->cmd_res_type;

		end_op = CMD_SELECT;
	} else {
		scope_usage(CMD_SELECT);
		return;
	}

	if ((type = cmdp->cmd_res_type) == RT_UNKNOWN) {
		long_usage(CMD_SELECT, true);
		return;
	}

	if (initialize(true) != ZC_OK) {
		return;
	}

	switch (type) {
	case RT_FS:
		if ((err = fill_in_fstab(cmdp, &old_fstab, false))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_FS, err, true);
			if (node_scope) {
				resource_scope = RT_NODE;
			} else {
				global_scope = true;
			}
		}
		bcopy(&old_fstab, &in_prog_fstab,
		    sizeof (struct zc_fstab));
		return;
	case RT_IPD:
		/* Invalid command in the node scope. */
		if (node_scope == true) {
			scope_usage(CMD_SELECT);
			end_op = -1;
			return;
		}

		if (state_atleast(ZC_STATE_INCOMPLETE)) {
			zerr(gettext("Zone cluster %s not in %s state; "
			    "%s %s not allowed."), zone,
			    zone_cluster_state_str(ZC_STATE_CONFIGURED),
			    cmd_to_str(CMD_SELECT), rt_to_str(RT_IPD));
			global_scope = true;
			end_op = -1;
			return;
		}
		if ((err = fill_in_ipdtab(cmdp, &old_ipdtab, false))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_IPD, err, true);
			global_scope = true;
		}
		bcopy(&old_ipdtab, &in_prog_ipdtab,
		    sizeof (struct zc_fstab));
		return;
	case RT_NODE:
		if (node_scope == true) {
			scope_usage(CMD_SELECT);
			end_op = -1;
			return;
		}
		node_scope = true;

		/*
		 * Record the fact that we entered node scope via
		 * 'select' command.
		 */
		if (saved_end_op == -1) {
			saved_end_op = CMD_SELECT;
		}

		if ((err = fill_in_nodetab(cmdp, &old_nodetab, false))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_NODE, err, false);
			global_scope = true;
			node_scope = false;
			end_op = saved_end_op = -1;
			return;
		}
		bcopy(&old_nodetab, &in_prog_nodetab,
		    sizeof (struct zc_nodetab));
		return;
	case RT_SYSID:
		/* Invalid command in the node scope. */
		if (node_scope == true) {
			scope_usage(CMD_SELECT);
			end_op = -1;
			return;
		}

		if (state_atleast(ZC_STATE_INCOMPLETE)) {
			zerr(gettext("Zone cluster %s not in %s state; "
			    "%s %s not allowed."), zone,
			    zone_cluster_state_str(ZC_STATE_CONFIGURED),
			    cmd_to_str(CMD_SELECT), rt_to_str(RT_SYSID));
			global_scope = true;
			end_op = -1;
			return;
		}
		if ((err = zccfg_lookup_sysid(handle, &old_sysidtab))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_SYSID, err, true);
			global_scope = true;
		}
		bcopy(&old_sysidtab, &in_prog_sysidtab,
		    sizeof (struct zc_sysidtab));
		return;
	case RT_DEVICE:
		if ((err = fill_in_devtab(cmdp, &old_devtab, false))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_DEVICE, err, true);
			if (node_scope) {
				resource_scope = RT_NODE;
			} else {
				global_scope = true;
			}
		}
		bcopy(&old_devtab, &in_prog_devtab,
		    sizeof (struct zc_devtab));
		return;
	case RT_NET:
		if ((err = fill_in_nwiftab(cmdp, &old_nwiftab, false))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_NET, err, true);
			if (node_scope) {
				resource_scope = RT_NODE;
			} else {
				global_scope = true;
			}
		}
		bcopy(&old_nwiftab, &in_prog_nwiftab,
		    sizeof (struct zc_nwiftab));
		return;
	case RT_RCTL:
		if ((err = fill_in_rctltab(cmdp, &old_rctltab, false))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_RCTL, err, true);
			if (node_scope) {
				resource_scope = RT_NODE;
			} else {
				global_scope = true;
			}
		}
		bcopy(&old_rctltab, &in_prog_rctltab,
		    sizeof (struct zc_rctltab));
		return;
	case RT_DATASET:
		if ((err = fill_in_dstab(cmdp, &old_dstab, false))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_DATASET, err, true);
			if (node_scope) {
				resource_scope = RT_NODE;
			} else {
				global_scope = true;
			}
		}
		bcopy(&old_dstab, &in_prog_dstab,
		    sizeof (struct zc_dstab));
		return;
	case RT_DCPU:
		/* Invalid command in the node scope. */
		if (node_scope == true) {
			scope_usage(CMD_SELECT);
			end_op = -1;
			return;
		}

		if ((err = zccfg_lookup_pset(handle, &old_psettab))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_DCPU, err, true);
			global_scope = true;
		}
		bcopy(&old_psettab, &in_prog_psettab,
		    sizeof (struct zc_psettab));
		return;
	case RT_PCAP:
		/* Invalid command in the node scope. */
		if (node_scope == true) {
			scope_usage(CMD_SELECT);
			end_op = -1;
			return;
		}

		if ((err = zccfg_get_aliased_rctl(handle, ALIAS_CPUCAP,
		    &tmp)) != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_PCAP, err, true);
			global_scope = true;
		}
		return;
	case RT_MCAP:
		/* Invalid command in the node scope. */
		if (node_scope == true) {
			scope_usage(CMD_SELECT);
			end_op = -1;
			return;
		}

		/* If none of these exist, there is no resource to select. */
		if ((res = zccfg_lookup_mcap(handle,
		    &old_mcaptab)) != ZC_OK &&
		    zccfg_get_aliased_rctl(handle, ALIAS_MAXSWAP,
		    &limit) != ZC_OK &&
		    zccfg_get_aliased_rctl(handle, ALIAS_MAXLOCKEDMEM,
		    &limit) != ZC_OK) {
			z_cmd_rt_perror(CMD_SELECT, RT_MCAP,
			ZC_NO_RESOURCE_TYPE, true);
			global_scope = true;
		}
		if (res == ZC_OK) {
			bcopy(&old_mcaptab, &in_prog_mcaptab,
			    sizeof (struct zc_mcaptab));
		} else {
			bzero(&in_prog_mcaptab, sizeof (in_prog_mcaptab));
		}
		return;
	default:
		zone_perror(rt_to_str(type), ZC_NO_RESOURCE_TYPE, true);
		long_usage(CMD_SELECT, true);
		usage(false, HELP_RESOURCES);
		return;
	}
}

/*
 * validate_net_address_syntax from Solaris usr/src/cmd/zonecfg/zonecfg.c
 *
 * Network "addresses" can be one of the following forms:
 *	<IPv4 address>
 *	<IPv4 address>/<prefix length>
 *	<IPv6 address>/<prefix length>
 *	<host name>
 *	<host name>/<prefix length>
 * In other words, the "/" followed by a prefix length is allowed but not
 * required for IPv4 addresses and host names, and required for IPv6 addresses.
 * If a prefix length is given, it must be in the allowable range: 0 to 32 for
 * IPv4 addresses and host names, 0 to 128 for IPv6 addresses.
 * Host names must start with an alpha-numeric character, and all subsequent
 * characters must be either alpha-numeric or "-".
 */
static int
validate_net_address_syntax(char *address)
{
	char *slashp;
	char part1[MAXHOSTNAMELEN];
	struct in6_addr in6;
	struct in_addr in4;
	int i;
	int prefixlen = 0;

	/*
	 * Copy the part before any '/' into part1 or copy the whole
	 * thing if there is no '/'.
	 */
	if ((slashp = strchr(address, '/')) != NULL) {
		*slashp = '\0';
		(void) strlcpy(part1, address, sizeof (part1));
		*slashp = '/';
		prefixlen = atoi(++slashp);
	} else {
		(void) strlcpy(part1, address, sizeof (part1));
	}

	if (inet_pton(AF_INET6, part1, &in6) == 1) {
		if (slashp == NULL) {
			zerr(gettext("%s: IPv6 addresses "
			    "require /prefix-length suffix."), address);
			return (ZC_ERR);
		}
		if (prefixlen < 0 || prefixlen > 128) {
			zerr(gettext("%s: IPv6 address "
			    "prefix lengths must be 0 - 128."), address);
			return (ZC_ERR);
		}
		return (ZC_OK);
	}

	/* At this point, any /prefix must be for IPv4. */
	if (slashp != NULL) {
		if (prefixlen < 0 || prefixlen > 32) {
			zerr(gettext("%s: IPv4 address "
			    "prefix lengths must be 0 - 32."), address);
			return (ZC_ERR);
		}
	}
	if (inet_pton(AF_INET, part1, &in4) == 1) {
		return (ZC_OK);
	}

	/* address may also be a host name */
	if (!isalnum(part1[0])) {
		zerr(gettext("%s: bogus host name or network address syntax"),
		    part1);
		saw_error = true;
		usage(false, HELP_NETADDR);
		return (ZC_ERR);
	}
	for (i = 1; part1[i]; i++) {
		if (!isalnum(part1[i]) && part1[i] != '-' && part1[i] != '.') {
			zerr(gettext("%s: bogus host name or "
			    "network address syntax"), part1);
			saw_error = true;
			usage(false, HELP_NETADDR);
			return (ZC_ERR);
		}
	}
	return (ZC_OK);
}

/*
 * Make sure the user specified a physical network interface.
 */
static int
validate_net_physical_syntax(char *ifnamep)
{
	if (strchr(ifnamep, ':') == NULL) {
		return (ZC_OK);
	}
	zerr(gettext("%s: physical interface name required; "
	    "logical interface name not allowed"), ifnamep);
	return (ZC_ERR);
}

/*
 * Verifies that a given file system type is "sane". It does not actually
 * go and verify that the specified type is supported by the system. That is
 * done by zoneadm(1M).
 */
static bool
valid_fs_type(const char *type)
{
	/*
	 * Is this a valid path component?
	 */
	if (strlen(type) + 1 > MAXNAMELEN) {
		return (false);
	}
	/*
	 * Make sure a bad value for "type" doesn't make
	 * /usr/lib/fs/<type>/mount turn into something else.
	 */
	if (strchr(type, '/') != NULL || type[0] == '\0' ||
	    strcmp(type, ".") == 0 || strcmp(type, "..") == 0) {
		return (false);
	}
	/*
	 * More detailed verification happens later by zoneadm(1m).
	 */
	return (true);
}

/*
 * Cancel an operation. Is only valid in a resource scope. The cancel command
 * brings us back to the next upper level scope.
 */
void
cancel_func(cmd_t *cmdp)
{
	int arg;

	assert(cmdp != NULL);

	optind = 0;
	if ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_CANCEL);
			return;
		default:
			short_usage(CMD_CANCEL);
			return;
		}
	}
	if (optind != cmdp->cmd_argc) {
		short_usage(CMD_CANCEL);
		return;
	}

	if (global_scope) {
		short_usage(CMD_CANCEL);
	}

	if (node_scope && resource_scope != RT_NODE) {
		/*
		 * We're in an inner-most scope. Go back up to the node
		 * scope.
		 */
		resource_scope = RT_NODE;
	} else if (node_scope && resource_scope == RT_NODE) {
		/*
		 * We're in the node resource scope. Go back to the global
		 * scope.
		 */
		global_scope = true;
		node_scope = false;
	} else {
		/*
		 * We're in a resource scope other than the node scope.
		 */
		global_scope = true;
	}

	if (global_scope == true) {
		zccfg_free_fs_option_list(in_prog_fstab.zc_fs_options);
		bzero(&in_prog_fstab, sizeof (in_prog_fstab));
		bzero(&in_prog_nwiftab, sizeof (in_prog_nwiftab));
		bzero(&in_prog_ipdtab, sizeof (in_prog_ipdtab));
		bzero(&in_prog_devtab, sizeof (in_prog_devtab));
		zccfg_free_rctl_value_list(in_prog_rctltab.zc_rctl_valp);
		bzero(&in_prog_rctltab, sizeof (in_prog_rctltab));
		bzero(&in_prog_attrtab, sizeof (in_prog_attrtab));
		bzero(&in_prog_dstab, sizeof (in_prog_dstab));
		bzero(&in_prog_psettab, sizeof (in_prog_psettab));
		bzero(&in_prog_mcaptab, sizeof (in_prog_mcaptab));
	}
}

/*
 * Convenience function to set aliased rctl values. Does a bunch of
 * validations as well. Note that this function is not identical to its
 * zonecfg.c counterpart, since here we do not have to deal with setting
 * the global zone's resource controls.
 */
static void
set_aliased_rctl(char *alias, int prop_type, char *value)
{
	int err;
	uint64_t limit;
	char tmp[128];

	/* convert memory based properties */
	if (prop_type == PT_MAXSHMMEM) {
		if (!zccfg_valid_memlimit(value, &limit)) {
			zerr(gettext("A non-negative number with a required "
			    "scale suffix (K, M, G or T) was expected\n"
			    "here."));
			saw_error = true;
			return;
		}
		(void) snprintf(tmp, sizeof (tmp), "%llu", limit);
		value = tmp;
	}

	if (!zccfg_aliased_rctl_ok(handle, alias)) {
		zone_perror(pt_to_str(prop_type), ZC_ALIAS_DISALLOW, false);
		saw_error = true;
	} else if (!zccfg_valid_alias_limit(alias, value, &limit)) {
		zerr(gettext("%s property is out of range."),
		    pt_to_str(prop_type));
		saw_error = true;
	} else if ((err = zccfg_set_aliased_rctl(handle, alias, limit))
	    != ZC_OK) {
		zone_perror(zone, err, true);
		saw_error = true;
	} else {
		need_to_commit = true;
	}
}

/*
 * Handler of the "set" command. Despite its length the function is quite
 * simple : depending on which scope we're currently in the set command works
 * with different property types. In the global scope, we can set the zone
 * cluster properties; in the same way, in any given resource scope we can set
 * that resource's properties. If we are in a resource scope, we check the
 * specified property type to take action.
 *
 * We retain the code structuring used in the function of the same name in
 * Solaris usr/src/cmd/zonecfg.c, to aid in ease of porting new code from
 * there.
 */
void
set_func(cmd_t *cmdp)
{
	char			*prop_idp;
	int			arg;
	int			err;
	int			res_type;
	int			prop_type;
	property_value_ptr_t	pp;
	bool			autoboot;
	bool			privateip;
	char			brand[MAXNAMELEN];
	nodeid_t		nodeid;
	uint64_t		mem_cap;
	uint64_t		mem_limit;
	float			cap;
	char			*unitp;
	size_t physmem_size = sizeof (in_prog_mcaptab.zc_physmem_cap);
	struct zc_psettab	tmp_psettab;
	zc_iptype_t		iptype;

	if (zc_is_read_only(CMD_SET)) {
		return;
	}

	assert(cmdp != NULL);

	optind = opterr = 0;
	while ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_SET);
			return;
		default:
			short_usage(CMD_SET);
			return;
		}
	}

	prop_type = cmdp->cmd_prop_name[0];
	if (global_scope) {
		if (prop_type == PT_ZONENAME) {
			/* don't allow zone rename for zone cluster */
			/* may be supported in the future */
			zerr(gettext("%s of a zone cluster cannot be "
			    "changed."), pt_to_str(PT_ZONENAME));
			saw_error = true;
			return;
		} else if (prop_type == PT_ZONEPATH) {
			res_type = RT_ZONEPATH;
		} else if (prop_type == PT_AUTOBOOT) {
			res_type = RT_AUTOBOOT;
		} else if (prop_type == PT_IPTYPE) {
			res_type = RT_IPTYPE;
		} else if (prop_type == PT_CLPRIVNET) {
			res_type = RT_CLPRIVNET;
		} else if (prop_type == PT_BRAND) {
			res_type = RT_BRAND;
		} else if (prop_type == PT_POOL) {
			res_type = RT_POOL;
		} else if (prop_type == PT_LIMITPRIV) {
			res_type = RT_LIMITPRIV;
		} else if (prop_type == PT_BOOTARGS) {
			res_type = RT_BOOTARGS;
		} else if (prop_type == PT_SCHED) {
			res_type = RT_SCHED;
		} else if (prop_type == PT_MAXLWPS) {
			res_type = RT_MAXLWPS;
		} else if (prop_type == PT_MAXSHMMEM) {
			res_type = RT_MAXSHMMEM;
		} else if (prop_type == PT_MAXSHMIDS) {
			res_type = RT_MAXSHMIDS;
		} else if (prop_type == PT_MAXMSGIDS) {
			res_type = RT_MAXMSGIDS;
		} else if (prop_type == PT_MAXSEMIDS) {
			res_type = RT_MAXSEMIDS;
		} else if (prop_type == PT_SHARES) {
			res_type = RT_SHARES;
		} else {
			zerr(gettext("Cannot set a resource-specific "
			    "property from the global scope."));
			saw_error = true;
			return;
		}
	} else {
		res_type = resource_scope;
	}

	pp = cmdp->cmd_property_ptr[0];
	prop_idp = pp->pv_simple;
	/*
	 * 1. fs options are simple or list (tested below)
	 * 2. rctl values are complex or list (tested below)
	 * 3. Anything else should be simple.
	 */
	if (!(res_type == RT_FS && prop_type == PT_OPTIONS) &&
	    !(res_type == RT_RCTL && prop_type == PT_VALUE) &&
	    (pp->pv_type != PROP_VAL_SIMPLE || prop_idp == NULL)) {
		zerr(gettext("A %s value was expected here."),
		    pvt_to_str(PROP_VAL_SIMPLE));
		saw_error = true;
		return;
	}
	if (prop_type == PT_UNKNOWN) {
		long_usage(CMD_SET, true);
		return;
	}

	/*
	 * Special case: the user can change the zone name (or zone cluster
	 * name) prior to 'create'; if the zone or zone cluster already
	 * exists, we fall through and let initialize() and the rest of the
	 * logic run.
	 */
	if (res_type == RT_ZONENAME && got_handle == false &&
	    !state_atleast(ZC_STATE_CONFIGURED)) {
		if ((err = validate_zc_name(prop_idp)) != ZC_OK) {
			zone_perror(prop_idp, err, true);
			usage(false, HELP_SYNTAX);
			return;
		}
		(void) strlcpy(zone, prop_idp, sizeof (zone));
		return;
	}

	if (initialize(true) != ZC_OK) {
		return;
	}

	switch (res_type) {
	case RT_ZONEPATH:
		/* zonepath value can't be changed if already installed. */
		if (state_atleast(ZC_STATE_INSTALLED)) {
			zerr(gettext("Zone cluster %s already installed. "
			    "%s %s not allowed."), zone,
			    cmd_to_str(CMD_SET), rt_to_str(RT_ZONEPATH));
			return;
		}
		if (validate_zonepath_syntax(prop_idp) != ZC_OK) {
			saw_error = true;
			return;
		}
		if ((err = zccfg_set_zonepath(handle, prop_idp)) != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_AUTOBOOT:
		if (strcmp(prop_idp, "true") == 0) {
			autoboot = true;
		} else if (strcmp(prop_idp, "false") == 0) {
			autoboot = false;
		} else {
			zerr(gettext("%s value must be '%s' or '%s'."),
			    pt_to_str(PT_AUTOBOOT), "true", "false");
			saw_error = true;
			return;
		}
		if ((err = zccfg_set_autoboot(handle, autoboot)) != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_CLPRIVNET:
		if (strcmp(prop_idp, "true") == 0) {
			privateip = true;
		} else if (strcmp(prop_idp, "false") == 0) {
			privateip = false;
		} else {
			zerr(gettext("%s value must be '%s' or '%s'."),
			    pt_to_str(PT_CLPRIVNET), "true", "false");
			saw_error = true;
			return;
		}
		if ((err = zccfg_set_clprivnet(handle,
		    privateip)) != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_POOL:
		/* don't allow use of the reserved temporary pool names */
		if (strncmp("SUNW", prop_idp, 4) == 0) {
			zerr(gettext("pool names starting with SUNW are "
			    "reserved."));
			saw_error = true;
			return;
		}

		/* can't set pool if dedicated-cpu exists */
		if (zccfg_lookup_pset(handle, &tmp_psettab) == ZC_OK) {
			zerr(gettext("The %s resource already exists.  "
			    "A persistent pool is incompatible\nwith the %s "
			    "resource."), rt_to_str(RT_DCPU),
			    rt_to_str(RT_DCPU));
			saw_error = true;
			return;
		}

		if ((err = zccfg_set_pool(handle, prop_idp)) != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_LIMITPRIV:
		if ((err = zccfg_set_limitpriv(handle, prop_idp))
		    != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_BOOTARGS:
		if ((err = zccfg_set_bootargs(handle, prop_idp)) != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_SCHED:
		if ((err = zccfg_set_sched(handle, prop_idp)) != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_IPTYPE:
		if (strcmp(prop_idp, "shared") == 0) {
			iptype = CZS_SHARED;
		} else {
			zerr(gettext("The current release of zone cluster "
			    "software only supports zones\nwith 'shared' IP "
			    "stacks."));
			zerr(gettext("%s value must be '%s'."),
			    pt_to_str(PT_IPTYPE), "shared");
			saw_error = true;
			return;
		}
		if ((err = zccfg_set_iptype(handle, iptype)) != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_BRAND:
		/*
		 * The only value allowed is "cluster"
		 */
		if (strcmp(prop_idp, "cluster") == 0) {
			(void) strcpy(brand, "cluster");
		} else {
			zerr(gettext("%s value must be 'cluster'."),
			    pt_to_str(PT_BRAND));
			saw_error = true;
			return;
		}
		if ((err = zccfg_set_brand(handle, brand)) != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_MAXLWPS:
		set_aliased_rctl(ALIAS_MAXLWPS, prop_type, prop_idp);
		return;
	case RT_MAXSHMMEM:
		set_aliased_rctl(ALIAS_MAXSHMMEM, prop_type, prop_idp);
		return;
	case RT_MAXSHMIDS:
		set_aliased_rctl(ALIAS_MAXSHMIDS, prop_type, prop_idp);
		return;
	case RT_MAXMSGIDS:
		set_aliased_rctl(ALIAS_MAXMSGIDS, prop_type, prop_idp);
		return;
	case RT_MAXSEMIDS:
		set_aliased_rctl(ALIAS_MAXSEMIDS, prop_type, prop_idp);
		return;
	case RT_SHARES:
		set_aliased_rctl(ALIAS_SHARES, prop_type, prop_idp);
		return;
	case RT_NODE:
		switch (prop_type) {
		case PT_PHYSICALHOST:
			/*
			 * Verify that the specified node is
			 * configured as a cluster member.
			 */
			nodeid = clconf_cluster_get_nodeid_by_nodename(
			    prop_idp);
			if (nodeid == NODEID_UNKNOWN) {
				zone_perror(prop_idp, ZC_NO_CLUSTER, true);
				return;
			}
			(void) strlcpy(in_prog_nodetab.zc_nodename,
			    prop_idp,
			    sizeof (in_prog_nodetab.zc_nodename));
			return;
		case PT_HOSTNAME:
			(void) strlcpy(in_prog_nodetab.zc_hostname,
			    prop_idp,
			    sizeof (in_prog_nodetab.zc_hostname));
			return;
		default:
			break;
		}
		zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE, true);
		long_usage(CMD_SET, true);
		usage(false, HELP_PROPS);
		return;
	case RT_SYSID:
		switch (prop_type) {
		case PT_ROOTPASSWD:
			in_prog_sysidtab.zc_root_password
			    = strdup(prop_idp);
			if (in_prog_sysidtab.zc_root_password == NULL) {
				zone_perror(pt_to_str(PT_ROOTPASSWD), ZC_NOMEM,
				    true);
				return;
			}
			return;
		case PT_NAMESERVICE:
			(void) strlcpy(in_prog_sysidtab.zc_name_service,
			    prop_idp,
			    sizeof (in_prog_sysidtab.zc_name_service));
			return;
		case PT_NFS4DOMAIN:
			(void) strlcpy(in_prog_sysidtab.zc_nfs4_domain,
			    prop_idp,
			    sizeof (in_prog_sysidtab.zc_nfs4_domain));
			return;
		case PT_SECPOLICY:
			(void) strlcpy(in_prog_sysidtab.zc_sec_policy,
			    prop_idp,
			    sizeof (in_prog_sysidtab.zc_sec_policy));
			return;
		case PT_SYSLOCALE:
			(void) strlcpy(in_prog_sysidtab.zc_sys_locale,
			    prop_idp,
			    sizeof (in_prog_sysidtab.zc_sys_locale));
			return;
		case PT_TERMINAL:
			(void) strlcpy(in_prog_sysidtab.zc_terminal,
			    prop_idp,
			    sizeof (in_prog_sysidtab.zc_terminal));
			return;
		case PT_TIMEZONE:
			(void) strlcpy(in_prog_sysidtab.zc_timezone,
			    prop_idp,
			    sizeof (in_prog_sysidtab.zc_timezone));
			return;
		default:
			break;
		}
		zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE, true);
		long_usage(CMD_SET, true);
		usage(false, HELP_PROPS);
		return;
	case RT_FS:
		switch (prop_type) {
		case PT_DIR:
			(void) strlcpy(in_prog_fstab.zc_fs_dir, prop_idp,
			    sizeof (in_prog_fstab.zc_fs_dir));
			return;
		case PT_SPECIAL:
			(void) strlcpy(in_prog_fstab.zc_fs_special,
			    prop_idp,
			    sizeof (in_prog_fstab.zc_fs_special));
			return;
		case PT_RAW:
			(void) strlcpy(in_prog_fstab.zc_fs_raw,
			    prop_idp, sizeof (in_prog_fstab.zc_fs_raw));
			return;
		case PT_TYPE:
			if (!valid_fs_type(prop_idp)) {
				zerr(gettext("\"%s\" is not a valid %s."),
				    prop_idp, pt_to_str(PT_TYPE));
				saw_error = true;
				return;
			}
			(void) strlcpy(in_prog_fstab.zc_fs_type, prop_idp,
			    sizeof (in_prog_fstab.zc_fs_type));
			return;
		case PT_OPTIONS:
			if (pp->pv_type != PROP_VAL_SIMPLE &&
			    pp->pv_type != PROP_VAL_LIST) {
				zerr(gettext("A %s or %s value was expected "
				    "here."), pvt_to_str(PROP_VAL_SIMPLE),
				    pvt_to_str(PROP_VAL_LIST));
				saw_error = true;
				return;
			}
			zccfg_free_fs_option_list(
			    in_prog_fstab.zc_fs_options);
			    in_prog_fstab.zc_fs_options = NULL;
			if (!(pp->pv_type == PROP_VAL_LIST &&
			    pp->pv_list == NULL)) {
				add_property(cmdp);
			}
			return;
		default:
			break;
		}
		zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE, true);
		long_usage(CMD_SET, true);
		usage(false, HELP_PROPS);
		return;
	case RT_NET:
		switch (prop_type) {
		case PT_ADDRESS:
			if (validate_net_address_syntax(prop_idp) != ZC_OK) {
				saw_error = true;
				return;
			}
			(void) strlcpy(in_prog_nwiftab.zc_nwif_address,
			    prop_idp,
			    sizeof (in_prog_nwiftab.zc_nwif_address));
			return;
		case PT_PHYSICAL:
			if (!node_scope && (strcmp(prop_idp, "auto") != 0)) {
				(void) printf(gettext("This is a global net "
				    "resource. It is not controlled by the "
				    "zones runtime.\nThe famework controlling "
				    "it will determine which physical "
				    "interface to use.\nThe only value allowed"
				    " for '%s' in this scope is 'auto'.\n"),
				    pt_to_str(PT_PHYSICAL));
				return;
			}
			if (validate_net_physical_syntax(prop_idp) != ZC_OK) {
				saw_error = true;
				return;
			}
			(void) strlcpy(in_prog_nwiftab.zc_nwif_physical,
			    prop_idp,
			    sizeof (in_prog_nwiftab.zc_nwif_physical));
			return;
		case PT_DEFROUTER:
			if (!defrouter_support && strlen(prop_idp) > 0) {
				(void) printf(gettext("The property %s is not "
				    "supported in the installed version of "
				    "Solaris.\n This feature is supported "
				    "starting in Solaris 10 10/08\n."),
				    pt_to_str(PT_DEFROUTER));
			}
			if (!node_scope && (strlen(prop_idp) > 0)) {
				(void) printf(gettext("The property %s is not "
				    "valid for a global net resource. This "
				    "property can be set only in the node "
				    "scope.\n"), pt_to_str(PT_DEFROUTER));
				return;
			}
			if (validate_net_address_syntax(prop_idp) != ZC_OK) {
				saw_error = true;
				return;
			}
			(void) strlcpy(in_prog_nwiftab.zc_nwif_defrouter,
			    prop_idp,
			    sizeof (in_prog_nwiftab.zc_nwif_defrouter));
			return;

		default:
			break;
		}
		zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE, true);
		long_usage(CMD_SET, true);
		usage(false, HELP_PROPS);
		return;
	case RT_IPD:
		switch (prop_type) {
		case PT_DIR:
			(void) strlcpy(in_prog_ipdtab.zc_fs_dir, prop_idp,
			    sizeof (in_prog_ipdtab.zc_fs_dir));
			return;
		default:
			break;
		}
		zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE, true);
		long_usage(CMD_SET, true);
		usage(false, HELP_PROPS);
		return;
	case RT_DEVICE:
		switch (prop_type) {
		case PT_MATCH:
			(void) strlcpy(in_prog_devtab.zc_dev_match,
			    prop_idp,
			    sizeof (in_prog_devtab.zc_dev_match));
			return;
		default:
			break;
		}
		zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE, true);
		long_usage(CMD_SET, true);
		usage(false, HELP_PROPS);
		return;
	case RT_RCTL:
		switch (prop_type) {
		case PT_NAME:
			if (!zccfg_valid_rctlname(prop_idp)) {
				zerr(gettext("'%s' is not a valid zone %s "
				    "name."), prop_idp, rt_to_str(RT_RCTL));
				return;
			}
			(void) strlcpy(in_prog_rctltab.zc_rctl_name,
			    prop_idp,
			    sizeof (in_prog_rctltab.zc_rctl_name));
			return;
		case PT_VALUE:
			if (pp->pv_type != PROP_VAL_COMPLEX &&
			    pp->pv_type != PROP_VAL_LIST) {
				zerr(gettext("A %s or %s value was expected "
				    "here."), pvt_to_str(PROP_VAL_COMPLEX),
				    pvt_to_str(PROP_VAL_LIST));
				saw_error = true;
				return;
			}
			zccfg_free_rctl_value_list(
			    in_prog_rctltab.zc_rctl_valp);
			in_prog_rctltab.zc_rctl_valp = NULL;
			if (!(pp->pv_type == PROP_VAL_LIST &&
			    pp->pv_list == NULL)) {
				add_property(cmdp);
			}
			return;
		default:
			break;
		}
		zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE, true);
		long_usage(CMD_SET, true);
		usage(false, HELP_PROPS);
		return;
	case RT_DATASET:
		switch (prop_type) {
		case PT_NAME:
			(void) strlcpy(in_prog_dstab.zc_dataset_name,
			    prop_idp,
			    sizeof (in_prog_dstab.zc_dataset_name));
			return;
		default:
			break;
		}
		zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE, true);
		long_usage(CMD_SET, true);
		usage(false, HELP_PROPS);
		return;
	case RT_DCPU:
		switch (prop_type) {
		char *lowp, *highp;

		case PT_NCPUS:
			lowp = prop_idp;
			if ((highp = strchr(prop_idp, '-')) != NULL) {
				*highp++ = '\0';
			} else {
				highp = lowp;
			}

			/* Make sure the input makes sense. */
			if (!zccfg_valid_ncpus(lowp, highp)) {
				zerr(gettext("%s property is out of range."),
				    pt_to_str(PT_NCPUS));
				saw_error = true;
				return;
			}

			(void) strlcpy(in_prog_psettab.zc_ncpu_min, lowp,
			    sizeof (in_prog_psettab.zc_ncpu_min));
			(void) strlcpy(in_prog_psettab.zc_ncpu_max, highp,
			    sizeof (in_prog_psettab.zc_ncpu_max));
			return;
		case PT_IMPORTANCE:
			/* Make sure the value makes sense. */
			if (!zccfg_valid_importance(prop_idp)) {
				zerr(gettext("%s property is out of range."),
				    pt_to_str(PT_IMPORTANCE));
				saw_error = true;
				return;
			}
			(void) strlcpy(in_prog_psettab.zc_importance,
			    prop_idp,
			    sizeof (in_prog_psettab.zc_importance));
			return;
		default:
			break;
		}
		zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE, true);
		long_usage(CMD_SET, true);
		usage(false, HELP_PROPS);
		return;
	case RT_PCAP:
		if (prop_type != PT_NCPUS) {
			zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE,
			    true);
			long_usage(CMD_SET, true);
			usage(false, HELP_PROPS);
			return;
		}

		/*
		 * We already checked that an rctl alias is allowed in
		 * the add_resource() function.
		 */
		if ((cap = strtof(prop_idp, &unitp)) <= 0 || *unitp != '\0' ||
		    (int)(cap * 100) < 1) {
			zerr(gettext("%s property is out of range."),
			    pt_to_str(PT_NCPUS));
			saw_error = true;
			return;
		}

		if ((err = zccfg_set_aliased_rctl(handle, ALIAS_CPUCAP,
		    (int)(cap * 100))) != ZC_OK) {
			zone_perror(zone, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	case RT_MCAP:
		switch (prop_type) {
		case PT_PHYSICAL:
			if (!zccfg_valid_memlimit(prop_idp, &mem_cap)) {
				zerr(gettext("A positive number with a "
				    "required scale suffix (K, M, G or T) was "
				    "expected here."));
				saw_error = true;
			} else if (mem_cap < ONE_MB) {
				zerr(gettext("%s value is too small.  It must "
				    "be at least 1M."),
				    pt_to_str(PT_PHYSICAL));
				saw_error = true;
			} else {
				(void) snprintf(
				    in_prog_mcaptab.zc_physmem_cap,
				    physmem_size, "%llu", mem_cap);
			}
			return;
		case PT_SWAP:
			/*
			 * We have to check if an rctl is allowed here since
			 * there might already be a rctl defined that blocks
			 * the alias.
			 */
			if (!zccfg_aliased_rctl_ok(handle,
			    ALIAS_MAXSWAP)) {
				zone_perror(pt_to_str(PT_MAXSWAP),
				    ZC_ALIAS_DISALLOW, false);
				saw_error = true;
				return;
			}

			/*
			 * Note that in Solaris zonecfg.c, there's a check for
			 * whether or not this is a global zone. Since a zone
			 * cluster is NEVER composed of global zones, that
			 * check doesn't apply here.
			 */
			mem_limit = ONE_MB * 50;
			if (!zccfg_valid_memlimit(prop_idp, &mem_cap)) {
				zerr(gettext("A positive number with a "
				    "required scale suffix (K, M, G or T) was "
				    "expected here."));
				saw_error = true;
			} else if (mem_cap < mem_limit) {
				char buf[128];

				(void) snprintf(buf, sizeof (buf), "%llu",
				    mem_limit);
				bytes_to_units(buf, buf, sizeof (buf));
				zerr(gettext("%s value is too small.  It must "
				    "be at least %s."), pt_to_str(PT_SWAP),
				    buf);
				saw_error = true;
			} else {
				if ((err = zccfg_set_aliased_rctl(handle,
				    ALIAS_MAXSWAP, mem_cap)) != ZC_OK) {
					zone_perror(zone, err, true);
				} else {
					need_to_commit = true;
				}
			}
			return;
		case PT_LOCKED:
			/*
			 * We have to check if an rctl is allowed here since
			 * there might already be a rctl defined that blocks
			 * the alias.
			 */
			if (!zccfg_aliased_rctl_ok(handle,
			    ALIAS_MAXLOCKEDMEM)) {
				zone_perror(pt_to_str(PT_LOCKED),
				    ZC_ALIAS_DISALLOW, false);
				saw_error = true;
				return;
			}

			if (!zccfg_valid_memlimit(prop_idp, &mem_cap)) {
				zerr(gettext("A non-negative number with a "
				    "required scale suffix (K, M, G or T) was "
				    "expected\nhere."));
				saw_error = true;
			} else {
				if ((err = zccfg_set_aliased_rctl(handle,
				    ALIAS_MAXLOCKEDMEM, mem_cap)) != ZC_OK) {
					zone_perror(zone, err, true);
				} else {
					need_to_commit = true;
				}
			}
			return;
		default:
			zone_perror(pt_to_str(prop_type), ZC_NO_PROPERTY_TYPE,
			    true);
			long_usage(CMD_SET, true);
			usage(false, HELP_PROPS);
			return;
		}
	default:
		return;
	}
}

/*
 * The fs and rctl resources support removal of 'properties' within their
 * scopes.
 */
static void
remove_property(cmd_t *cmdp)
{
	/* TODO Implement function. */
}

/*
 * Called in the context of unqualified resource removal (see below). Asks the
 * user to confirm if she really wants to remove all instances of a resource
 * type.
 */
static bool
prompt_remove_resource(cmd_t *cmdp, const char *rsrc)
{
	int num;
	int arg;
	int answer;
	bool force = false;
	char prompt[128];

	optind = 0;
	while ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "F")) != EOF) {
		switch (arg) {
		case 'F':
			force = true;
			break;
		default:
			return (false);
		}
	}

	num = zccfg_num_resources(handle, rsrc);
	if (num == 0) {
		z_cmd_rt_perror(CMD_REMOVE, cmdp->cmd_res_type, ZC_NO_ENTRY,
		    true);
		return (false);
	} else if (num > 1 && !force) {
		if (!interactive_mode) {
			zerr(gettext("There are multiple instances of this "
			    "resource.  Either qualify the resource to\n"
			    "remove a single instance or use the -F option to "
			    "remove all instances."));
			saw_error = true;
			return (false);
		}
		(void) snprintf(prompt, sizeof (prompt), gettext(
		    "Are you sure you want to remove ALL '%s' resources"),
		    rsrc);
		answer = ask_yesno(false, prompt);
		if (answer == -1) {
			zerr(gettext("Resource incomplete."));
			return (false);
		}
		if (answer != 1) {
			return (false);
		}
	}
	return (true);
}

/*
 * General comment for remove_<resource_type> functions: There is a "qualified
 * removal" and an "unqualified removal". The former has the resource to be
 * removed specified as an argument to the function, whereas the latter means
 * removal of all instances of the specified resource type.
 *
 * "remove fs dir=/foo" is an example of qualified removal.
 * "remove fs" is an example of unqualified removal.
 *
 * remove_fs() removes file system resource(s).
 */
static void
remove_fs(cmd_t *cmdp)
{
	int err;

	/* qualified fs removal */
	if (cmdp->cmd_prop_nv_pairs > 0) {
		struct zc_fstab fstab;

		if ((err = fill_in_fstab(cmdp, &fstab, false)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_FS, err, true);
			return;
		}

		if ((err = zccfg_delete_filesystem(handle,
		    &fstab)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_FS, err, true);
		} else {
			need_to_commit = true;
		}

		zccfg_free_fs_option_list(fstab.zc_fs_options);
		return;
	}

	/*
	 * Unqualified fs removal. Remove all fs's but prompt if there are
	 * more than one instances.
	 */
	if (!prompt_remove_resource(cmdp, "fs")) {
		return;
	}

	if ((err = zccfg_del_all_resources(handle, "fs")) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_FS, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Remove IPD resource(s). See block comment above remove_fs for the
 * general algorithm.
 */
static void
remove_ipd(cmd_t *cmdp)
{
	int err;

	/*
	 * Make sure that none of the zone instances of the zone cluster
	 * is in INSTALLED state or above. IPD can only be changed if a zone is
	 * in the CONFIGURED state.
	 */
	if (state_atleast(ZC_STATE_INSTALLED)) {
		zerr(gettext("Zone cluster %s already installed; %s %s not "
		    "allowed."), zone, cmd_to_str(CMD_REMOVE),
		    rt_to_str(RT_IPD));
		return;
	}

	/* Qualified ipd removal */
	if (cmdp->cmd_prop_nv_pairs > 0) {
		struct zc_fstab fstab;

		if ((err = fill_in_ipdtab(cmdp, &fstab, false)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_IPD, err, true);
			return;
		}

		if ((err = zccfg_delete_ipd(handle, &fstab)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_IPD, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	}

	/*
	 * Unqualified ipd removal. Remove all ipd's but prompt if there are
	 * more than one instances.
	 */
	if (!prompt_remove_resource(cmdp, "inherit-pkg-dir")) {
		return;
	}

	if ((err = zccfg_del_all_resources(handle, "inherit-pkg-dir"))
	    != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_IPD, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Remove net resources. See block comment above remove_fs for the general
 * algorithm.
 */
static void
remove_net(cmd_t *cmdp)
{
	int err;

	/* qualified net removal */
	if (cmdp->cmd_prop_nv_pairs > 0) {
		struct zc_nwiftab nwiftab;

		if ((err = fill_in_nwiftab(cmdp, &nwiftab, false)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_NET, err, true);
			return;
		}

		if ((err = zccfg_delete_nwif(handle,
		    &nwiftab)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_NET, err, true);
		} else {
			need_to_commit = true;
		}

		return;
	}

	/*
	 * Unqualified net removal. Remove all net's but prompt if there are
	 * more than one instances.
	 */
	if (!prompt_remove_resource(cmdp, "net")) {
		return;
	}

	if ((err = zccfg_del_all_resources(handle, "net")) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_NET, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Removes sysid resource from the configuration.
 *
 * Following convention with other resource types, we'll not prompt the user
 * for confirmation since there can only be one sysid resource.
 */
static void
remove_sysid()
{
	int err;
	struct zc_sysidtab sysidtab;

	/*
	 * Can't remove sysid if zone cluster is already installed.
	 */
	if (state_atleast(ZC_STATE_INSTALLED)) {
		zerr(gettext("Zone cluster %s already installed; %s %s not "
		    "allowed."), zone, cmd_to_str(CMD_REMOVE),
		    rt_to_str(RT_SYSID));
		return;
	}

	if ((err = zccfg_lookup_sysid(handle, &sysidtab)) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_SYSID, err, true);
		return;
	}
	if ((err = zccfg_delete_sysid(handle, &sysidtab)) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_SYSID, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Helper function for remove_node(). This function removes a single node
 * from the configuration after performing sanity checks.
 */
static int
remove_a_node(struct zc_nodetab *tabp)
{
	int err;
	nodeid_t nodeid;
	zone_cluster_state_t state = ZC_STATE_UNKNOWN;
	char cmdbuf[MAXPATHLEN];


	/*
	 * If zone cluster is new (i.e., the configuration hasn't been
	 * committed to the CCR), just remove the specified node from the
	 * configuration.
	 */
	if (new_zone_cluster == true) {
		return (zccfg_delete_node(handle, tabp));
	}

	/*
	 * Existing zone cluster. Make sure the node being removed is in the
	 * cluster, as otherwise we can't determine the state of the zone
	 * cluster member on that node.
	 */
	nodeid = clconf_cluster_get_nodeid_by_nodename(
	    tabp->zc_nodename);
	if (!conf_is_cluster_member(nodeid)) {
		(void) printf(gettext("%s not in the cluster. Can't remove "
		    "node.\n"), tabp->zc_nodename);
		return (ZC_NO_CLUSTER);
	}

	/*
	 * Now ensure that the state of the zone on the node being removed is
	 * less than INSTALLED. We must not remove the node if the zone state
	 * is INSTALLED and above, since it may be in some RG's nodelist.
	 */
	if ((err = get_zone_state_on_node(zone, nodeid, &state)) != ZC_OK) {
		/* Can't get state. So can't remove... */
		return (err);
	}

	if (state == ZC_STATE_CONFIGURED) {
		(void) sprintf(cmdbuf, "%s %s delete", ZC_HELPER, zone);
		err = zccfg_execute_cmd(nodeid, cmdbuf);
		if (err != ZC_OK) {
			return (err);
		} else {
			return (zccfg_delete_node(handle, tabp));
		}
	} else {
		(void) printf(gettext("Uninstall %s on node %s first and try "
		    "again.\n"), zone, tabp->zc_nodename);
		return (ZC_BAD_ZONE_STATE);
	}
}

static int
remove_all_nodes(void)
{
	int err;
	bool removed = false;
	struct zc_nodetab nodetab;

	/* If new zone cluster, just remove all the nodes from the handle. */
	if (new_zone_cluster) {
		return (zccfg_del_all_resources(handle, "node"));
	}

	/* Existing zone cluster. Do each node in turn. */
	if ((err = zccfg_setnodeent(handle)) != ZC_OK) {
		return (err);
	}

	while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
		if ((err = remove_a_node(&nodetab)) != ZC_OK) {
			continue;
		} else {
			removed = true;

			/*
			 * Re-initialize the XMl handle to point to the
			 * node entries.
			 */
			err = zccfg_setnodeent(handle);
			if (err != ZC_OK) {
				break;
			}
		}
	}
	(void) zccfg_endnodeent(handle);

	if (removed) {
		/*
		 * At least one node was removed. Return ZC_OK, so that the
		 * caller will set the need_to_commit flag.
		 */
		return (ZC_OK);
	} else {
		return (err);
	}
}

/*
 * Remove node resource(s). See block comment above remove_fs for the
 * general algorithm.
 *
 * Removing a node resource is more involved than other resource types because
 * we also need to remove the zone component on the removed node. That is, if
 * node foo is removed from the node list of a zone cluster, the Solaris zone
 * component on foo must be deleted (zonecfg delete). This means that the zone
 * component on node foo must already be in the CONFIGURED state.
 *
 * To delete a node resource, we must be able to determine the state of the
 * zone component on that node. This means that the node being removed must be
 * in the physical cluster. Otherwise, we could end up in a situation where
 * we have removed the node from the zone cluster's configuration, but the zone
 * component on that node is still in INSTALLED state.
 */
static void
remove_node(cmd_t *cmdp)
{
	int err;

	/* property name and value are specified */
	if (cmdp->cmd_prop_nv_pairs > 0) {
		struct zc_nodetab nodetab;
		if ((err = fill_in_nodetab(cmdp, &nodetab, false))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_NODE, err, true);
			return;
		}
		if ((err = remove_a_node(&nodetab)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_NODE, err, true);
			return;
		} else {
			need_to_commit = true;
		}
		return;
	}

	/*
	 * Unqualified node removal. Remove all nodes but prompt if there
	 * are more than one instances.
	 */
	if (!prompt_remove_resource(cmdp, "node")) {
		return;
	}

	if ((err = remove_all_nodes()) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_NODE, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Remove device resource(s). See block comment above remove_fs for the
 * general algorithm.
 */
static void
remove_device(cmd_t *cmdp)
{
	int err;

	/* property name and value are specified */
	if (cmdp->cmd_prop_nv_pairs > 0) {
		struct zc_devtab devtab;

		if ((err = fill_in_devtab(cmdp, &devtab, false)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_DEVICE, err, true);
			return;
		}
		if ((err = zccfg_delete_dev(handle, &devtab))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_DEVICE, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	}

	/*
	 * Unqualified device removal. Remove all devices but prompt if there
	 * are more than one instances.
	 */
	if (!prompt_remove_resource(cmdp, "device")) {
		return;
	}

	if ((err = zccfg_del_all_resources(handle, "device")) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_DEVICE, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Remove dataset resource(s). See block comment above remove_fs for the
 * general algorithm.
 */
static void
remove_dataset(cmd_t *cmdp)
{
	int err;

	/* property name and value are specified */
	if (cmdp->cmd_prop_nv_pairs > 0) {
		struct zc_dstab dstab;

		if ((err = fill_in_dstab(cmdp, &dstab, false)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_DATASET, err, true);
			return;
		}
		if ((err = zccfg_delete_ds(handle, &dstab)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_DATASET, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	}

	/*
	 * Unqualified dataset removal. Remove all datasets but prompt if there
	 * are more than one instances.
	 */
	if (!prompt_remove_resource(cmdp, "dataset")) {
		return;
	}

	if ((err = zccfg_del_all_resources(handle, "dataset")) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_DATASET, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Remove the capped-memory resource. Note that there can only be one
 * capped-memory resource configured.
 */
static void
remove_mcap()
{
	int err;
	int res1; /* lookup result for the physical memory cap */
	int res2; /* lookup result for the max swap rctl */
	int res3; /* lookup result for the max locked memory rctl */
	uint64_t tmp;
	struct zc_mcaptab mcaptab;
	bool revert = false; /* will restore configuration if true */

	res1 = zccfg_lookup_mcap(handle, &mcaptab);
	res2 = zccfg_get_aliased_rctl(handle, ALIAS_MAXSWAP, &tmp);
	res3 = zccfg_get_aliased_rctl(handle, ALIAS_MAXLOCKEDMEM, &tmp);

	/* if none of these exists, there's no mcap resource to remove */
	if (res1 != ZC_OK && res2 != ZC_OK && res3 != ZC_OK) {
		zerr("%s %s: %s", cmd_to_str(CMD_REMOVE), rt_to_str(RT_MCAP),
		    zccfg_strerror(ZC_NO_RESOURCE_TYPE));
		saw_error = true;
		return;
	}
	if (res1 == ZC_OK) {
		if ((err = zccfg_delete_mcap(handle)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_MCAP, err, true);
			revert = true;
		} else {
			need_to_commit = true;
		}
	}
	if (res2 == ZC_OK) {
		if ((err = zccfg_rm_aliased_rctl(handle, ALIAS_MAXSWAP))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_MCAP, err, true);
			revert = true;
		} else {
			need_to_commit = true;
		}
	}
	if (res3 == ZC_OK) {
		if ((err = zccfg_rm_aliased_rctl(handle,
		    ALIAS_MAXLOCKEDMEM)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_MCAP, err, true);
			revert = true;
		} else {
			need_to_commit = true;
		}
	}

	/* don't commit if there was an error */
	if (revert) {
		need_to_commit = false;
	}
}

/*
 * Remove dedicated-cpu resource from the configuration. Note that there can
 * only be one dedicated-cpu resource.
 */
static void
remove_pset()
{
	int err;
	struct zc_psettab psettab;

	if ((err = zccfg_lookup_pset(handle, &psettab)) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_DCPU, err, true);
		return;
	}
	if ((err = zccfg_delete_pset(handle)) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_DCPU, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Remove a capped-cpu resource from the configuration. Note that there can
 * only be one capped-cpu resource.
 */
static void
remove_pcap()
{
	int err;
	uint64_t tmp;

	if (zccfg_get_aliased_rctl(handle, ALIAS_CPUCAP, &tmp) != ZC_OK) {
		zerr("%s %s: %s", cmd_to_str(CMD_REMOVE), rt_to_str(RT_PCAP),
		    zccfg_strerror(ZC_NO_RESOURCE_TYPE));
		    saw_error = true;
		    return;
	}

	if ((err = zccfg_rm_aliased_rctl(handle, ALIAS_CPUCAP)) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_PCAP, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Remove rctl resource from the configuration. See block comment above
 * remove_fs for the general algorithm.
 */
static void
remove_rctl(cmd_t *cmdp)
{
	int err;

	/* unqualified removal */
	if (cmdp->cmd_prop_nv_pairs > 0) {
		struct zc_rctltab rctltab;

		if ((err = fill_in_rctltab(cmdp, &rctltab, false)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_RCTL, err, true);
			return;
		}
		if ((err = zccfg_delete_rctl(handle, &rctltab)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_RCTL, err, true);
		} else {
			need_to_commit = true;
		}
		zccfg_free_rctl_value_list(rctltab.zc_rctl_valp);
		return;
	}

	/*
	 * unqualified rctl removal. remove all rctls but prompt if more
	 * than one.
	 */
	if (!prompt_remove_resource(cmdp, "rctl")) {
		return;
	}

	if ((err = zccfg_del_all_resources(handle, "rctl")) != ZC_OK) {
		z_cmd_rt_perror(CMD_REMOVE, RT_RCTL, err, true);
	} else {
		need_to_commit = true;
	}
}

/*
 * Remove the specified resource from the current configuration. Fairly
 * straightforward as we just call the remove_<resource_type> function.
 */
static void
remove_resource(cmd_t *cmdp)
{
	int type;
	int arg;

	if ((type = cmdp->cmd_res_type) == RT_UNKNOWN) {
		long_usage(CMD_REMOVE, true);
		return;
	}

	optind = 0;
	while ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?F")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_REMOVE);
			return;
		case 'F':
			break;
		default:
			short_usage(CMD_REMOVE);
			return;
		}
	}

	if (initialize(true) != ZC_OK) {
		return;
	}

	switch (type) {
	case RT_FS:
		remove_fs(cmdp);
		return;
	case RT_IPD:
		remove_ipd(cmdp);
		return;
	case RT_SYSID:
		remove_sysid();
		return;
	case RT_NET:
		remove_net(cmdp);
		return;
	case RT_NODE:
		remove_node(cmdp);
		return;
	case RT_DEVICE:
		remove_device(cmdp);
		return;
	case RT_DATASET:
		remove_dataset(cmdp);
		return;
	case RT_RCTL:
		remove_rctl(cmdp);
		return;
	case RT_ATTR:
		zerr(gettext("The %s resource type is not supported by this "
		    "version of the zone cluster software."),
		    rt_to_str(RT_ATTR));
		/* remove_attr(cmdp); */
		return;
	case RT_MCAP:
		remove_mcap();
		return;
	case RT_DCPU:
		remove_pset();
		return;
	case RT_PCAP:
		remove_pcap();
		return;
	default:
		zone_perror(rt_to_str(type), ZC_NO_RESOURCE_TYPE, true);
		long_usage(CMD_REMOVE, true);
		usage(false, HELP_RESOURCES);
		return;
	}
}

/*
 * Remove net resource(s) from a node scope.
 */
static void
remove_net_from_node(cmd_t *cmdp)
{
	int err;

	/*
	 * Qualified removal. Remove matching entry.
	 */
	if (cmdp->cmd_prop_nv_pairs > 0) {
		struct zc_nwiftab nwiftab;

		if ((err = fill_in_nwiftab(cmdp, &nwiftab, false)) != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_NET, err, true);
			return;
		}

		if ((err = delete_local_net(&in_prog_nodetab, &nwiftab))
		    != ZC_OK) {
			z_cmd_rt_perror(CMD_REMOVE, RT_NET, err, true);
		} else {
			need_to_commit = true;
		}
		return;
	}

	/*
	 * Unqualified removal. This will remove all net resources in the
	 * current node scope. Prompt for confirmation if in interactive mode.
	 */
	if (interactive_mode == true) {
		if (ask_yesno(false, "Are you sure you want to remove ALL "
		    "network resources in this scope") != 1) {
			/* User doesn't want to remove all. Return. */
			return;
		}
	}
	zccfg_free_nwif_list(in_prog_nodetab.zc_nwif_listp);
	in_prog_nodetab.zc_nwif_listp = NULL;
}

/*
 * This function removes a resource contained within a node resource. We
 * support only the nested 'net' resource type at present. Handle other
 * nested resource types as support is added.
 */
static void
node_scope_remove_func(cmd_t *cmdp)
{
	int type;
	int arg;

	if ((type = cmdp->cmd_res_type) == RT_UNKNOWN) {
		long_usage(CMD_REMOVE, true);
		return;
	}

	optind = 0;
	while ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?F")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_REMOVE);
			return;
		case 'F':
			break;
		default:
			short_usage(CMD_REMOVE);
			return;
		}
	}

	if (initialize(true) != ZC_OK) {
		return;
	}

	switch (type) {
	case RT_NET:
		remove_net_from_node(cmdp);
		return;
	default:
		zerr(gettext("%s %s not supported in this scope."),
		    cmd_to_str(CMD_REMOVE), rt_to_str(type));
		return;
	}
}

/*
 * Remove resource(s) or properties from the configuration. The actual
 * work is done in either remove_resource() or remove_property().
 */
void
remove_func(cmd_t *cmdp)
{
	if (zc_is_read_only(CMD_REMOVE)) {
		return;
	}

	assert(cmdp != NULL);

	if (global_scope) {
		remove_resource(cmdp);
	} else if (node_scope && resource_scope == RT_NODE) {
		node_scope_remove_func(cmdp);
	} else {
		remove_property(cmdp);
	}
}

/*
 * This function can be called from two places: from parse_and_run(), and
 * from commit_func(). The former call is just to check that the configuration
 * is valid. When called from commit_func(), we need to save the configuration
 * to persistent storage, in addition to validating the configuration. We will
 * perform validation checks that make sense in the clustering world here.
 * Validation checks that can be done at the individual zone level will be
 * handed down to zonecfg(1M) - we do not want to duplicate all the logic
 * there.
 *
 * When called from commit_func(), we commit the configuration to the CCR and
 * then create the zone component on each node in the zone cluster's node list.
 */
void
verify_func(cmd_t *cmdp)
{
	bool			save = false;
	bool			privateip;
	int			err;
	int			ret_val = ZC_OK;
	int			arg;
	int			node_count;
	char			cmd_buf[MAXPATHLEN];
	char			zonepath[MAXPATHLEN];
	struct zc_nodetab	nodetab;
	struct zc_sysidtab	sysidtab;
	nodeid_t		nodeid;
	char			*cz_subnet = NULL;

	assert(cmdp != NULL);

	optind = 0;
	if ((arg = getopt(cmdp->cmd_argc, cmdp->cmd_argv, "?")) != EOF) {
		switch (arg) {
		case '?':
			longer_usage(CMD_VERIFY);
			return;
		default:
			short_usage(CMD_VERIFY);
			return;
		}
	}
	if (optind > cmdp->cmd_argc) {
		short_usage(CMD_VERIFY);
		return;
	}

	if (zc_is_read_only(CMD_VERIFY)) {
		return;
	}

	if (cmdp->cmd_argc > 0 && (strcmp(cmdp->cmd_argv[0], "save") == 0)) {
		save = true;
	}
	if (initialize(true) != ZC_OK) {
		return;
	}

	/*
	 * Make sure this version of Solaris and zone cluster software are
	 * compatible. Exit on error as there's nothing more we can do until
	 * the problem is rectified by the user.
	 */
	if ((err = zccfg_validate_brand_definition()) != ZC_OK) {
		zone_perror(zone, err, true);
		zerr(gettext("Detected mismatch between cluster brand "
		    "definition and Solaris brand DTD.\nMake sure "
		    "you are running the correct Solaris version to use\nthe "
		    "zone cluster feature."));
		zccfg_fini_handle(handle);
		exit(ZC_ERR);
	}

	/*
	 * Make sure that the zonepath is sane.
	 */
	if (zccfg_get_zonepath(handle, zonepath,
	    sizeof (zonepath)) != ZC_OK) {
		zerr(gettext("%s not specified"), pt_to_str(PT_ZONEPATH));
		ret_val = ZC_REQD_RESOURCE_MISSING;
		saw_error = true;
	}
	if (strlen(zonepath) == 0) {
		zerr(gettext("%s cannot be empty"), pt_to_str(PT_ZONEPATH));
		ret_val = ZC_REQD_RESOURCE_MISSING;
		saw_error = true;
	}

	/*
	 * Check whether the enable_priv_net is TRUE, if yes, then assign
	 * a subnet for this virtual cluster and return that value.
	 * This subnet will be stored in the CZNET_CCR_TABLE file later after
	 * the directory ccr file is created. This is because, the updates
	 * to the CZC_NET_CCR registers for the CZMM callbacks for this CZ
	 * which will fail if the directory is not created.
	 *
	 * If there are not enough subnets, then return error.
	 */
	if (((err = zccfg_get_clprivnet(handle, &privateip)) == ZC_OK) &&
	    (privateip == true)) {
			ret_val = zccfg_process_clprivnet(zone, &cz_subnet);
			if (ret_val != ZC_OK) {
				zone_perror(zone, ret_val, true);
				(void) printf(gettext("Failed to assign a "
				    "subnet for zone %s.\n"), zone);
				saw_error = true;
			}
	}

	/*
	 * Sysid resource MUST be specified.
	 */
	if ((err = zccfg_setsysident(handle)) != ZC_OK) {
		zone_perror(zone, err, true);
		return;
	}

	sysidtab.zc_root_password = NULL;
	if ((err = zccfg_getsysident(handle, &sysidtab)) != ZC_OK) {
		zerr(gettext("%s not specified"), rt_to_str(RT_SYSID));
		ret_val = ZC_REQD_RESOURCE_MISSING;
		saw_error = true;
	}
	(void) zccfg_endsysident(handle);

	if ((err = zccfg_setnodeent(handle)) != ZC_OK) {
		zone_perror(zone, err, true);
		return;
	}

	/*
	 * There must be at least one physical node in the ZC's nodelist.
	 * Each node must be currently a cluster member.
	 * Each node must have at least one network resource specified.
	 */
	node_count = 0;
	while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
		node_count++;

		if (!conf_is_cluster_member(
		    clconf_cluster_get_nodeid_by_nodename(
		    nodetab.zc_nodename))) {
			(void) printf(gettext("%s is either down or not in the"
			    " cluster.\nUnable to complete verification.\n"),
			    nodetab.zc_nodename);
			saw_error = true;
			ret_val = ZC_NO_CLUSTER;
		}
		if (nodetab.zc_nwif_listp == NULL) {
			zerr(gettext("%s must have at least one %s resource"),
			    rt_to_str(RT_NODE), rt_to_str(RT_NET));
			ret_val = ZC_REQD_RESOURCE_MISSING;
			saw_error = true;
		}
	}

	if (node_count < 1) {
		zerr(gettext("%s must have at least one %s resource."),
		    zone, rt_to_str(RT_NODE));
		ret_val = ZC_REQD_RESOURCE_MISSING;
		saw_error = true;
	}
	(void) zccfg_endnodeent(handle);

	/*
	 * Commit the configuration to CCR, then invoke zc_helper on each
	 * node in the nodelist to configure/update the Solaris zone.
	 */
	if (save) {
		if (ret_val != ZC_OK) {
			zerr(gettext("%s: failed to verify"), zone);
			return;
		}

		ret_val = zccfg_save(handle);
		if (ret_val == ZC_OK) {
			need_to_commit = false;
			new_zone_cluster = false;
		} else {
			zone_perror(zone, ret_val, true);
			return;
		}

		/*
		 * Add an entry (ZC name and subnet) to the CZNET_CCR_TABLE
		 * for this ZC.
		 */
		ret_val = zccfg_write_subnet_to_netccr(zone, cz_subnet);
		if (ret_val != ZC_OK) {
			zone_perror(zone, ret_val, true);
			(void) printf(gettext("Failed to write %s subnet to "
			    "%s.\n"), zone, CZNET_CCR_TABLE);
		}

		(void) sprintf(cmd_buf, "%s %s create", ZC_HELPER, zone);
		if ((ret_val = zccfg_setnodeent(handle)) != ZC_OK) {
			zone_perror(zone, ret_val, true);
			return;
		}
		while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
			nodeid = clconf_cluster_get_nodeid_by_nodename(
			    nodetab.zc_nodename);
			ret_val = zccfg_execute_cmd(nodeid, cmd_buf);
			if (ret_val != ZC_OK) {
				zone_perror(zone, ret_val, true);
				(void) printf(gettext("Failed to create %s on "
				    "%s.\n"), zone, nodetab.zc_nodename);
				/*
				 * Add entry in the command log so that the
				 * commit (which is really create for us) can
				 * be replayed later on this node.
				 */
				ret_val = zccfg_add_cmd_log_entry(zone,
				    nodetab.zc_nodename,
				    cmd_to_str(CMD_CREATE));
				if (ret_val != ZC_OK) {
					zone_perror(zone, ret_val, true);
				}
			}
		}
		(void) zccfg_endnodeent(handle);
	}
}

void
help_func(cmd_t *cmdp)
{
	int i;

	assert(cmdp != NULL);

	if (cmdp->cmd_argc == 0) {
		if (node_scope) {
			usage(true, HELP_RES_SCOPE);
			return;
		} else {
			usage(true, global_scope ? HELP_SUBCMDS :\
			    HELP_RES_SCOPE);
			return;
		}
	}
	if (strcmp(cmdp->cmd_argv[0], "usage") == 0) {
		usage(true, HELP_USAGE);
		return;
	}
	if (strcmp(cmdp->cmd_argv[0], "commands") == 0) {
		usage(true, HELP_SUBCMDS);
		return;
	}
	if (strcmp(cmdp->cmd_argv[0], "syntax") == 0) {
		usage(true, HELP_SYNTAX | HELP_RES_PROPS);
		return;
	}
	if (strcmp(cmdp->cmd_argv[0], "-?") == 0) {
		longer_usage(CMD_HELP);
		return;
	}

	for (i = 0; i <= CMD_MAX; i++) {
		if (strcmp(cmdp->cmd_argv[0], cmd_to_str(i)) == 0) {
			longer_usage(i);
			return;
		}
	}
	/* We do not use zerr() here because we do not want its extra \n. */
	(void) fprintf(stderr, gettext("Unknown help subject %s.  "),
	    cmdp->cmd_argv[0]);
	usage(false, HELP_META);
}

static int
string_to_yyin(char *string)
{
	if ((yyin = tmpfile()) == NULL) {
		zone_perror(execnamep, ZC_TEMP_FILE, true);
		return (ZC_ERR);
	}
	if (fwrite(string, strlen(string), 1, yyin) != 1) {
		zone_perror(execnamep, ZC_TEMP_FILE, true);
		return (ZC_ERR);
	}
	if (fseek(yyin, 0, SEEK_SET) != 0) {
		zone_perror(execnamep, ZC_TEMP_FILE, true);
		return (ZC_ERR);
	}
	return (ZC_OK);
}

/* This is the back-end helper function for read_input() below. */
static int
cleanup()
{
	int answer;
	cmd_t *cmdp;

	if (!interactive_mode && !cmd_file_mode) {
		/*
		 * If we're not in interactive mode, and we're not in command
		 * file mode, then we must be in commands-from-the-command-line
		 * mode.  As such, we can't loop back and ask for more input.
		 * It was OK to prompt for such things as whether or not to
		 * really delete a zone cluster in the command handler called
		 * from yyparse() above, but "really quit?" makes no sense in
		 * this context.  So disable prompting.
		 */
		ok_to_prompt = false;
	}
	if (!global_scope) {
		if (!time_to_exit) {
			/*
			 * Just print a simple error message in the -1 case,
			 * since exit_func() already handles that case, and
			 * EOF means we are finished anyway.
			 */
			answer = ask_yesno(false,
			    gettext("Resource incomplete; really quit"));
			if (answer == -1) {
				zerr(gettext("Resource incomplete."));
				return (ZC_ERR);
			}
			if (answer != 1) {
				yyin = stdin;
				return (ZC_REPEAT);
			}
		} else {
			saw_error = true;
		}
	}
	/*
	 * Make sure we tried something and that the handle checks
	 * out, or we would get a false error trying to commit.
	 */
	if (need_to_commit && zccfg_check_handle(handle) == ZC_OK) {
		if ((cmdp = alloc_cmd()) == NULL) {
			zone_perror(zone, ZC_NOMEM, true);
			return (ZC_ERR);
		}
		cmdp->cmd_argc = 0;
		cmdp->cmd_argv[0] = NULL;
		commit_func(cmdp);
		free_cmd(cmdp);
		/*
		 * need_to_commit will get set back to false if the
		 * configuration is saved successfully.
		 */
		if (need_to_commit) {
			if (force_exit) {
				zerr(gettext("Configuration not saved."));
				return (ZC_ERR);
			}
			answer = ask_yesno(false,
			    gettext("Configuration not saved; really quit"));
			if (answer == -1) {
				zerr(gettext("Configuration not saved."));
				return (ZC_ERR);
			}
			if (answer != 1) {
				time_to_exit = false;
				yyin = stdin;
				return (ZC_REPEAT);
			}
		}
	}
	return ((need_to_commit || saw_error) ? ZC_ERR : ZC_OK);
}

/*
 * read_input() is the driver of this program.  It is a wrapper around
 * yyparse(), printing appropriate prompts when needed, checking for
 * exit conditions and reacting appropriately [the latter in its cleanup()
 * helper function].
 *
 * Like most functions here, it returns ZC_OK or ZC_ERR, *or* ZC_REPEAT
 * so do_interactive() knows that we are not really done (i.e, we asked
 * the user if we should really quit and the user said no).
 */
static int
read_input()
{
	bool yyin_is_a_tty = isatty(fileno(yyin));
	/*
	 * The prompt is "e:z> ", "e:z:r> " or "e:z:r:r> " where e is execnamep,
	 * z is zone and r is resource_scope: 6 is for the three ":"s + "> " +
	 * terminator.
	 */
	char prompt[MAXPATHLEN + ZONENAME_MAX + MAX_RT_STRLEN + 6] = "";
	char *line;

	/* yyin should have been set to the appropriate (FILE *) if not stdin */
	newline_terminated = true;
	for (;;) {
		if (yyin_is_a_tty) {
			if (newline_terminated) {
				if (global_scope) {
					(void) snprintf(prompt, sizeof (prompt),
					    "%s:%s> ", main_prompt, zone);
				} else if (resource_scope && !node_scope ||
				    resource_scope == RT_NODE) {
					(void) snprintf(prompt, sizeof (prompt),
					    "%s:%s:%s> ", main_prompt, zone,
					    rt_to_str(resource_scope));
				} else if (node_scope && resource_scope &&
				    resource_scope != RT_NODE) {
					(void) snprintf(prompt, sizeof (prompt),
					    "%s:%s:%s:%s> ", main_prompt, zone,
					    rt_to_str(RT_NODE),
					    rt_to_str(resource_scope));
				}
			}
			/*
			 * If the user hits ^C then we want to catch it and
			 * start over.  If the user hits EOF then we want to
			 * bail out.
			 */
			line = gl_get_line(get_linep, prompt, NULL, -1);
			if (gl_return_status(get_linep) == GLR_SIGNAL) {
				gl_abandon_line(get_linep);
				continue;
			}
			if (line == NULL) {
				break;
			}
			(void) string_to_yyin(line);
			while (!feof(yyin)) {
				(void) yyparse();
			}
		} else {
			(void) yyparse();
		}
		/* Bail out on an error in command file mode. */
		if (saw_error && cmd_file_mode && !interactive_mode) {
			time_to_exit = true;
		}
		if (time_to_exit || (!yyin_is_a_tty && feof(yyin))) {
			break;
		}
	}
	return (cleanup());
}

/*
 * Called in multiple places.
 * Initializes 'handle' for the specified zone cluster.
 * If zccfg_get_handle returned ZC_NO_ZONE, it means that there is no
 * zone cluster of the specified name, so we ask the user to start creating
 * it.
 *
 * initialize() is also used to ensure that we have the latest copy of the
 * configuration.
 */
static int
initialize(bool handle_expected)
{
	int err;

	if (zccfg_check_handle(handle) != ZC_OK) {
		if ((err = zccfg_get_handle(zone, handle)) == ZC_OK) {
			got_handle = true;
		} else {
			zone_perror(zone, err, handle_expected || got_handle);
			if (err == ZC_NO_ZONE && !got_handle &&
			    interactive_mode && !read_only_mode) {
				(void) printf(gettext("Use '%s' to begin "
				    "configuring a new zone cluster.\n"),
				    cmd_to_str(CMD_CREATE));
			}
			return (err);
		}
	}

	return (ZC_OK);
}

/*
 * This function is used in the interactive-mode scenario: it just
 * calls read_input() until we are done.
 */
static int
do_interactive(void)
{
	int err;

	interactive_mode = true;
	if (!read_only_mode) {
		/*
		 * Try to set things up proactively in interactive mode, so
		 * that if the zone in question does not exist yet, we can
		 * provide the user with a clue.
		 */
		(void) initialize(false);
	}
	do {
		err = read_input();
	} while (err == ZC_REPEAT);
	return (err);
}

/*
 * cmd_file() wraps on read_input() if the user specified a command file to
 * read the commands from (using the -f option). If a command file is
 * specified, we make sure that we can open the file for reading and that the
 * file is a regular file. We then assign yyin to the command file pointer.
 * A file name argument of "-" is equivalent to reading inputs from the stdin,
 * so we just set interactive_mode to true if that's the case.
 */
static int
cmd_file(char *file)
{
	FILE *infile = NULL;
	int err;
	struct stat statbuf;

	/* True if the input file is not stdin. */
	bool using_real_file = (strcmp(file, "-") != 0);

	if (using_real_file) {
		/*
		 * zerr() prints a line number in cmd_file_mode, which we do
		 * not want here, so temporarily unset it.
		 */
		cmd_file_mode = false;
		if ((infile = fopen(file, "r")) == NULL) {
			zerr(gettext("could not open file %s: %s"),
			    file, strerror(errno));
			return (ZC_ERR);
		}
		if ((err = fstat(fileno(infile), &statbuf)) != 0) {
			zerr(gettext("could not stat file %s: %s"),
			    file, strerror(errno));
			err = ZC_ERR;
			goto done;
		}
		if (!S_ISREG(statbuf.st_mode)) {
			zerr(gettext("%s is not a regular file."), file);
			err = ZC_ERR;
			goto done;
		}
		yyin = infile;
		cmd_file_mode = true;
		ok_to_prompt = false;
	} else {
		/*
		 * "-f -" is essentially the same as interactive mode,
		 * so treat it that way.
		 */
		interactive_mode = true;
	}
	/* ZC_REPEAT is for interactive mode; treat it like ZC_ERR here. */
	if ((err = read_input()) == ZC_REPEAT) {
		err = ZC_ERR;
	}
done:
	if (using_real_file && (infile != NULL)) {
		(void) fclose(infile);
	}
	return (err);
}

/*
 * Since yacc is based on reading from a (FILE *) whereas what we get from
 * the command line is in argv format, we need to convert when the user
 * gives us commands directly from the command line.  That is done here by
 * concatenating the argv list into a space-separated string, writing it
 * to a temp file, and rewinding the file so yyin can be set to it.  Then
 * we call read_input(), and only once, since prompting about whether to
 * continue or quit would make no sense in this context.
 */
static int
one_command_at_a_time(int argc, char *argv[])
{
	char *command;
	size_t len = 2; /* terminal \n\0 */
	int i;
	int err;

	for (i = 0; i < argc; i++) {
		len += strlen(argv[i]) + 1;
	}
	if ((command = malloc(len)) == NULL) {
		zone_perror(execnamep, ZC_NOMEM, true);
		return (ZC_ERR);
	}
	(void) strlcpy(command, argv[0], len);
	for (i = 1; i < argc; i++) {
		(void) strlcat(command, " ", len);
		(void) strlcat(command, argv[i], len);
	}
	(void) strlcat(command, "\n", len);
	err = string_to_yyin(command);
	free(command);
	if (err != ZC_OK) {
		return (err);
	}
	while (!feof(yyin)) {
		(void) yyparse();
	}
	return (cleanup());
}

/*
 * Convenience function to get the base executable name, without all the
 * leading and trailing slashes. So, for example if one ran
 * "/usr/bin/ls/, this function would return just the "ls" part.
 */
static char *
get_execbasename(char *execfullname)
{
	char *last_slash;
	char *execbasename;

	/* guard against '/' at end o	f command invocation */
	for (;;) {
		last_slash = strrchr(execfullname, '/');
		if (last_slash == NULL) {
			execbasename = execfullname;
			break;
		} else {
			execbasename = last_slash + 1;
			if (*execbasename == '\0') {
				*last_slash = '\0';
				continue;
			}
			break;
		}
	}
	return (execbasename);
}

int
main(int argc, char *argv[])
{
	int err;
	int arg;
	int bootflags = 0; /* For cladm(2) call below. */
	struct passwd *pwp = NULL;

	/* This must be before anything goes to stdout. */
	setbuf(stdout, NULL);

	saw_error = false;
	cmd_file_mode = false;

	execnamep = get_execbasename(argv[0]);
	assert(execnamep);

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);

	if (getzoneid() != GLOBAL_ZONEID) {
		zerr(gettext("%s can only be run from the global zone."),
		    execnamep);
		exit(ZC_ERR);
	}

	/* Bail out early if we're not in a cluster */
	if ((err = cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags)) != 0) {
		zerr(gettext("%s: cladm(2) returned %d. Unable to proceed."),
		    execnamep, err);
		exit(ZC_ERR);
	}
	if (!(bootflags & CLUSTER_BOOTED)) {
		zerr(gettext("%s can only be run on a cluster node."),
		    execnamep);
		exit(ZC_ERR);
	}

	/* We need to use libclconf. */
	if ((err = clconf_lib_init()) != 0) {
		zerr(gettext("%s cannot initialize libclconf. Aborting."),
		    execnamep);
		exit(ZC_ERR);
	}

	if (argc < 2) {
		usage(false, HELP_USAGE | HELP_SUBCMDS);
		exit(ZC_USAGE);
	}
	if (strcmp(argv[1], cmd_to_str(CMD_HELP)) == 0) {
		(void) one_command_at_a_time(argc - 1, &(argv[1]));
		exit(ZC_OK);
	}

	while ((arg = getopt(argc, argv, "?f:z:")) != EOF) {
		switch (arg) {
		case '?':
			if (optopt == '?') {
				usage(true, HELP_USAGE | HELP_SUBCMDS);
			} else {
				usage(false, HELP_USAGE);
			}
			exit(ZC_USAGE);
			/* NOTREACHED */
		case 'f':
			cmd_file_namep = optarg;
			cmd_file_mode = true;
			break;
		case 'z':
			if (validate_zc_name(optarg) != ZC_OK) {
				zone_perror(optarg, ZC_BOGUS_ZONE_NAME, true);
				usage(false, HELP_SYNTAX);
				exit(ZC_USAGE);
			}
			(void) strlcpy(zone, optarg, sizeof (zone));
			break;
		default:
			usage(false, HELP_USAGE);
			exit(ZC_USAGE);
		}
	}

	if (optind > argc || strcmp(zone, "") == 0) {
		usage(false, HELP_USAGE);
		exit(ZC_USAGE);
	}

	/*
	 * Check the user's access rights here. If user is a non-root
	 * user, read_only_mode flag must be set to true.
	 */
	if ((pwp = getpwuid(getuid())) == NULL) {
		zerr(gettext("getpwuid failure"));
		exit(ZC_ERR);
	}

	if (chkauthattr("solaris.cluster.modify", (const char *)pwp->pw_name)
	    == 0) {
		read_only_mode = true;
	}

	if ((handle = zccfg_init_handle()) == NULL) {
		zone_perror(execnamep, ZC_NOMEM, true);
		exit(ZC_ERR);
	}
	/*
	 * Determine the underlying Solaris version. If the attribute
	 * 'defrouter' is supported, we record the information here.
	 */
	defrouter_support = zccfg_attr_exists_in_zones_dtd("defrouter");
	/*
	 * This may get set back to false again in cmd_file() if cmd_file_namep
	 * is a "real" file as opposed to "-" (i.e. meaning use stdin).
	 */
	if (isatty(STDIN_FILENO)) {
		ok_to_prompt = true;
	}
	if ((get_linep = new_GetLine(MAX_LINE_LEN, MAX_CMD_HIST)) == NULL) {
		exit(ZC_ERR);
	}
	if (gl_customize_completion(get_linep, NULL, cmd_cpl_fn) != 0) {
		exit(ZC_ERR);
	}
	(void) sigset(SIGINT, SIG_IGN);
	if (optind == argc) {
		if (!cmd_file_mode) {
			err = do_interactive();
		} else {
			err = cmd_file(cmd_file_namep);
		}
	} else {
		err = one_command_at_a_time(argc - optind, &(argv[optind]));
	}
	zccfg_fini_handle(handle);
	(void) del_GetLine(get_linep);
	return (err);
}
