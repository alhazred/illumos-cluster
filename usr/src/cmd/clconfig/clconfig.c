/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ident	"@(#)clconfig.c	1.25	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <sys/cladm_int.h>
#include <rgm/sczones.h>

static void usage(void);
static char *prog;

int
main(int argc, char **argv)
{
	int c;
	int errflg;
	int ret = 0;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	if (sc_zonescheck() != 0)
		return (1);

	prog = argv[0];
	if (argc == 1)
		usage();

	errflg = 0;
	while ((c = getopt(argc, argv, "csgdl:u:")) != EOF) {
		switch (c) {
		case 'c':	/* start cluster mode (join cluster) */
			c = 1;
			if (cladm(CL_CONFIG, CL_CLUSTER_ENABLE, &c) != 0) {
				perror(gettext("_cladm: CL_CLUSTER_ENABLE"));
				ret |= 1;
			}
			break;

		case 'g':	/* enable global mounts */
			c = 1;
			if (cladm(CL_CONFIG, CL_GBLMNT_ENABLE, &c) != 0) {
				perror(gettext("_cladm: CL_GBLMNT_ENABLE"));
				ret |= 1;
			}
			break;

		case 's':	/* start switchbacks */
			c = 1;
			if (cladm(CL_CONFIG, CL_SWITCHBACK_ENABLE, &c) != 0) {
				perror(gettext("_cladm: enabling switchback"));
				ret |= 1;
			}
			break;

		case 'l':	/* lock a device (for fsck/mount during boot) */
			if (cladm(CL_CONFIG, CL_GBLMNT_LOCK, optarg) != 0) {
				perror(gettext("_cladm: CL_GBLMNT_LOCK"));
				ret |= 1;
			}
			break;

		case 'u':	/* unlock a device */
			if (cladm(CL_CONFIG, CL_GBLMNT_UNLOCK, optarg) != 0) {
				perror(gettext("_cladm: CL_GBLMNT_UNLOCK"));
				ret |= 1;
			}
			break;

		case 'd':
			c = 1;
			if (cladm(CL_CONFIG, CL_UNMOUNT_DISABLE, &c) != 0) {
				perror(gettext("_cladm: CL_UNMOUNT_DISABLE"));
				ret |= 1;
			}
			break;

		default:
			errflg = 1;
			break;
		}
	}

	if (errflg)
		usage();

	return (ret);
}

static void
usage()
{
	(void) fprintf(stderr,
		gettext("usage: %s [-cg] [-l device] [-u device]\n"),
		prog);
	exit(1);
}
