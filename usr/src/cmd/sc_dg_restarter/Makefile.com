#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)Makefile.com	1.7	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/sc_delegated_restarter/Makefile.com
# The basic concept was taken from cmd/pmf/Makefile.com
#

DAEMON	= $(SERVER)

SERVER	= sc_delegated_restarter

include ../../Makefile.cmd

SERVER_SOURCES_CC = $(SERVER_OBJECTS_CC:%.o=../common/%.cc)
SERVER_SOURCES = $(SERVER_SOURCES_CC)

LINTFILES	= $(SERVER_OBJECTS:%.o=%.ln) $(CLIENT_OBJECTS:%.o=%.ln) $(REIM_OBJECTS_CC:%.o=%.ln)
PIFILES		= $(SERVER_SOURCES_CC:%.cc=%.pi)

POFILE		= sc_restarter.po

CLOBBERFILES	+= $(DAEMON)

$(POST_S9_BUILD)SERVER_OBJECTS	= $(SERVER_OBJECTS_CC)


$(POST_S9_BUILD)SERVER_OBJECTS_CC = resource_mgmt.o service_mgmt.o
$(POST_S9_BUILD)SERVER_OBJECTS_CC += res_svc_basic_info.o
$(POST_S9_BUILD)SERVER_OBJECTS_CC += sc_delegated_restarter.o
$(POST_S9_BUILD)SERVER_OBJECTS_CC += sc_restarter_contract.o svc_timeout_wait.o
$(POST_S9_BUILD)SERVER_OBJECTS_CC += repository_val.o sc_dg_error.o
OBJECTS = $(SERVER_OBJECTS)

$(POST_S9_BUILD)REIM_OBJECTS_CC = reim_server.o 
$(POST_S9_BUILD)XDR_OBJECTS = reim_xdr.o

$(POST_S9_BUILD)CHECKHDRS = resource_mgmt.h service_mgmt.h res_svc_basic_info.h
$(POST_S9_BUILD)CHECKHDRS += sc_delegated_restarter.h sc_restarter_contract.h
$(POST_S9_BUILD)CHECKHDRS += svc_timeout_wait.h repository_val.h
$(POST_S9_BUILD)CHECKHDRS += sc_dg_error.h

LIBRESTART      = $(REF_PROTO)/lib/librestart.so.1

.KEEP_STATE:

#
# Compiler flags
#
CPPFLAGS	+= $(CL_CPPFLAGS) -I. -I$(SRC)/common/cl
CPPFLAGS	+= -I$(REF_PROTO)/usr/src/head
CPPFLAGS	+= -I$(REF_PROTO)/usr/src/lib/librestart/common
LINTFLAGS	+= -I. -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS
LDLIBS		+= -lc -lnsl -lsecurity -lclos -lclevent  -ldoor -ldsdev -lscha
LDLIBS		+= -lumem
LDLIBS		+= -lproject
$(POST_S9_BUILD)LDLIBS += -lscf $(LIBRESTART) -lcontract -lclcomm


MTFLAG	= -mt

$(SERVER):= LDLIBS += -lscutils -lclst

#
# The 64 bit compilation uses the POSIX semantics for readdir_r, even if
# we do not define _POSIX_PTHREAD_SEMANTICS.  Thus, we must define
# _POSIX_PTHREAD_SEMANTICS for 32 bit so it uses the POSIX semantics for
# readdir_r as well.  We must also define the flag for 64-bit, so that
# other functions (like getpwuid_r and ctime_r) use POSIX semantics
# in both 32 and 64-bit.
#
$(MACH)_CFLAGS += -D_POSIX_PTHREAD_SEMANTICS
$(MACH64)_CFLAGS += -D_POSIX_PTHREAD_SEMANTICS

CPPFLAGS += -D_POSIX_PTHREAD_SEMANTICS

#
# Targets
#
all: $(DAEMON)

clean :
	$(RM) *.o $(CLOBBERFILES)

$(POST_S9_BUILD)SERVERLINK = $(LINK.cc)

$(SERVER): $(SERVER_OBJECTS) $(XDR_OBJECTS) $(REIM_OBJECTS_CC)
	$(SERVERLINK) -o $@ $(SERVER_OBJECTS) $(REIM_OBJECTS_CC) $(XDR_OBJECTS) $(LDLIBS)
	$(POST_PROCESS)

#
# build rules
#
%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

%.ll:	../common/%.cc
	$(WLCC) $(CFLAGS) $(CPPFLAGS) -o $@ $<

include ../../Makefile.targ
