/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
//
// This file contains information about the service FMRI management
//

#ifndef	_SERVICE_MGMT_H
#define	_SERVICE_MGMT_H

#pragma ident	"@(#)service_mgmt.h	1.5	08/05/20 SMI"

#include <librestart.h>
#include <sys/threadpool.h>
#include <libscf.h>
#include <map>
#include <string>
#include <sstream>

#include "resource_mgmt.h"
#include "sc_delegated_restarter.h"
#include "sc_restarter_contract.h"
#include "repository_val.h"

using namespace std;


class resource_info;
class rep_val_handling;

const int DESIRED_SVC_FMRI_THREADS = 1;

//
// Instance states used internal to sc_delegated_restarter
//
enum dr_intrnl_inst_state_type {
	DR_UNINITIALIZED,
	DR_ONLINE,
	DR_IN_ONLINE_METHOD,
	DR_OFFLINE,
	DR_IN_OFFLINE_METHOD,
	DR_SC_FORCED_OFFLINE,
	DR_DISABLED,
	DR_IN_DISABLE_METHOD,
	DR_IN_REFRESH_METHOD,
	DR_MAINTENANCE,
	DR_DEGRADED,
	DR_NONE
};
typedef enum dr_intrnl_inst_state_type dr_intrnl_inst_state_type;

//
// instance methods.
//
enum inst_method_type {
	IM_START,
	IM_ONLINE,
	IM_OFFLINE,
	IM_DISABLE,
	IM_REFRESH,
	IM_RESTART,
	IM_NONE
};
typedef enum inst_method_type inst_method_type;
//
// Type of exec method to be called.As of now its just start and stop.
// In future others might be added
enum svc_exec_mthd_type {
	SVC_EXEC_STOP = 0,
	SVC_EXEC_START,
	SVC_TIMEOUT_START,
	SVC_TIMEOUT_STOP
};
typedef enum svc_exec_mthd_type svc_exec_mthd_type;

struct exec_mthd_struct {
	svc_exec_mthd_type	mthd_type;
	char			*pg_name;
	char			*prop_name;
};
typedef struct exec_mthd_struct exec_mthd_struct;

/*
 * Collection of information for exec method.
 * NOTE:  This table is indexed into using the dr_intrnl_inst_state_type,
 * so the ordering needs to be kept in sync.
 */
static exec_mthd_struct exec_mthd_info[] = {
	{SVC_EXEC_STOP, "stop", "exec"},
	{SVC_EXEC_START, "start", "exec"},
	{SVC_TIMEOUT_START, "start", "timeout_seconds"},
	{SVC_TIMEOUT_STOP, "stop", "timeout_seconds"}
};


struct inst_state_info {
	dr_intrnl_inst_state_type	dr_istate;
	char				*name;
	restarter_instance_state_t	smf_state;
	inst_method_type		method_running;
};
typedef struct inst_state_info inst_state_info;

/*
 * Collection of information for each state.
 * NOTE:  This table is indexed into using the dr_intrnl_inst_state_type,
 * so the ordering needs to be kept in sync.
 */
static inst_state_info inst_states[] = {
	{DR_UNINITIALIZED, "uninitialized", RESTARTER_STATE_UNINIT,
	    IM_NONE},
	{DR_ONLINE, "online", RESTARTER_STATE_ONLINE, IM_START},
	{DR_IN_ONLINE_METHOD, "online_method", RESTARTER_STATE_OFFLINE,
	    IM_ONLINE},
	{DR_OFFLINE, "offline", RESTARTER_STATE_OFFLINE, IM_NONE},
	{DR_IN_OFFLINE_METHOD, "offline_method", RESTARTER_STATE_OFFLINE,
	    IM_OFFLINE},
	{DR_SC_FORCED_OFFLINE, "scoffline", RESTARTER_STATE_OFFLINE,
	    IM_NONE},
	{DR_DISABLED, "disabled", RESTARTER_STATE_DISABLED, IM_NONE},
	{DR_IN_DISABLE_METHOD, "disabled_method", RESTARTER_STATE_OFFLINE,
	    IM_DISABLE},
	{DR_IN_REFRESH_METHOD, "refresh_method", RESTARTER_STATE_ONLINE,
	    IM_REFRESH},
	{DR_MAINTENANCE, "maintenance", RESTARTER_STATE_MAINT, IM_NONE},
	{DR_DEGRADED, "degraded", RESTARTER_STATE_DEGRADED, IM_NONE},
	{DR_NONE, "none", RESTARTER_STATE_NONE, IM_NONE}
};

//
// Type of event the svc is working on, like
// smf  event based or contract event based,
// or if any direct updates to be done from res.....
//
enum svc_event_operation_type {
	SVC_ET_OP_SMF = 0,
	SVC_ET_OP_CONTRACT,
	SVC_ET_OP_RES
};
typedef enum svc_event_operation_type svc_event_operation_type;


//
// Manages a single service FMRI
//
class service_task {
	private:
		//
		// Current and next repository states of
		// this service
		//
		restarter_instance_state_t cur_inst_state;
		restarter_instance_state_t next_inst_state;

		//
		// Locking and signalling the completion
		//
		void reduce_svc_count();

		bool is_retrying_expired();

		void check_and_set_transient();

	public:
		// Name of the service fmri
		char	*svc_fmri;

		// Pointers of the doubly linked list
		service_task	*next_svc_ptr;
		service_task	*prev_svc_ptr;

		// Pointer to the respective resource object
		resource_info	*res_info;

		// Pointer to the respective contract info object
		contract_info	*svc_contract_info;

		//
		// If the current operation is resource based, then to
		// identify if its a start or stop res method
		//
		res_call_type res_method_type;

		//
		// A marker to keep track during updation, if this service
		// is removed or not
		//
		bool is_update_marked;

		// The current restarter type that is arrived on this
		// service
		restarter_event_type_t	cur_res_event_type;

		// Current event type service is working on
		svc_event_operation_type svc_et_op_type;

		//
		// Svc exec method  process id
		//
		int exec_mthd_pid;

		//
		// Flag to differentiate if the svc is being restarted or is it
		// being started for the first time through res start
		//
		bool is_svc_restart;

		//
		// Flag is set if its a transient process
		//
		bool svc_is_transient;

		// Constructor
		service_task(char *service_fmri, int *err);
		~service_task();

		dr_intrnl_inst_state_type conv_cur_smf_to_internal_state();

		//
		// Perfrom the operation on the service FMRI.
		//
		void execute_service();

		void
			update_state(char *name,
				restarter_instance_state_t new_cur,
				char *aux_state,
				restarter_error_t restarter_e);

		int
			run_method(char *inst, svc_exec_mthd_type mthd_type,
				int *method_exit);


		void
			svc_start_event(char *name);

		void
			svc_stop_event(char *name);
		//
		// Hash table to keep service fmris as the key and its
		// corresponding object address as the data
		//
		static std::map<string, service_task *> svc_fmri_hash_tbl;

		//
		// Threaspool to manage and perform various operations on
		// service instances
		//
		threadpool svc_fmri_thread_pool;
		pthread_mutex_t svc_fmri_thread_pool_lock;

		//
		// Restarter event handle and lock to protect it
		//
		static	restarter_event_handle_t *res_evt_handle;
		static	pthread_mutex_t	res_evt_hdl_lock;

		restarter_instance_state_t get_svc_cur_state();

		//
		// Failover thread lock
		//
		static pthread_mutex_t res_failover_lock;

		//
		// Flag to determine if this svc has been started to be
		// managed by the delegated restarter.Set in the restarter
		// ADD_INSTANCE event
		//
		bool is_svc_managed;
};


//
// Just set the required fields that is used by the service_task to work
// on the service fmri.
//

class svc_event_task : public defer_task {
public:
	// The current restarter smf event type that is arrived on this
	// service
	restarter_event_type_t	cur_smf_event_type;

	// Current operation (smf based, contract based or resource based)
	// this service is working on
	svc_event_operation_type cur_et_op_type;

	//
	// If the current operation is resource based, then to identify if its
	// a start or stop res method
	//
	res_call_type set_res_method_type;

	//
	// Pointer to service task that it should pass the control to
	//
	service_task *svc_ptr;

	//
	// The thread pool execute function
	//
	void execute();

	svc_event_task();
};

#endif /* _SERVICE_MGMT_H */
