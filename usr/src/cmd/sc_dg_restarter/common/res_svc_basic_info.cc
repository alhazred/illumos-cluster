/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)res_svc_basic_info.cc	1.9	09/01/26 SMI"

#include "res_svc_basic_info.h"
#include "sc_dg_error.h"
#include <rgm/libdsdev.h>

res_svc_basic_info::res_svc_basic_info()
{
}
//
// Returns 0 on success, 1 on failure
//
int
res_svc_basic_info::process_method(char *res_name_or_fmri,
				char *res_grp_name,
				res_call_type mthd_type,
				char *svc_fmri_file,
				int retry_cnt,
				int retry_interval)
{
	int err;
	resource_info *res_ptr;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	switch (mthd_type) {
	case RCT_INIT:
	case RCT_BOOT:
	case RCT_START_REG:
		res_ptr = get_resource_info(res_name_or_fmri);
		if (res_ptr != NULL) {
			debug_print(NOGET("\n The res %s is "
			"already present in the res map table %d\n\n"),
				res_name_or_fmri, res_ptr->cur_res_state);
			//
			// The resource already exists in the resource map
			// table. Whenever the restarter is disabled and
			// and enabled or it is restarted after its death
			// this function is called with mthd_type
			// RCT_START_REG. Return error only if mthd_type
			// is RCT_START_REG. In the case of BOOT or INIT
			// there are chances that the resource already
			// exists because RCT_START_REG mthd_type gets
			// executed much before INIT or BOOT.
			if (mthd_type ==  RCT_START_REG)
				return (1);
			else if (mthd_type == RCT_BOOT) {
				err = res_ptr->process_res(RCT_BOOT);
				if (err != 0) {
					//
					// SCMSGS
					// @explanation
					// Not able to process the
					// resource boot method.
					// @user_action
					// Contact your Sun vendor
					// for more help.
					//
					logger.log(LOG_ERR, MESSAGE,
					    "\n Boot method fo smf "
					    "proxy resource %s exited "
					    "with error %d",
					    res_name_or_fmri, err);
					return (1);
				}
			}
			return (0);
		}
		res_ptr = new resource_info(res_name_or_fmri, res_grp_name,
					    err);

		if (err == 0) {
			return (0);
		} else {
			delete (res_ptr);
			return (1);
		}
		break;
	case RCT_START:
		res_ptr = get_resource_info(res_name_or_fmri);
		if (res_ptr == NULL) {
			debug_print(NOGET("\n\n Res %s is not "
				"present in the internal res table \n\n"),
				res_name_or_fmri);
			return (1);
		}
		PTHREAD_MTX_LOCK(res_ptr->res_mgmt_lock);
		err = res_ptr->process_res(RCT_START);
		if (err != 0) {
			//
			// SCMSGS
			// @explanation
			// Not able to process the resource to start all the
			// services.Previous messages to this should give the
			// reason.
			// @user_action
			// Stop the resource and try starting again. If it
			// fails after couple of retries,the resource can be
			// manually failed over to another node or delegated
			// restarter does it after the expiration of retry
			// counts. Contact SUN vendor for more help
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in res %s "
				"execution Start ", res_name_or_fmri);
			PTHREAD_MTX_UNLOCK(res_ptr->res_mgmt_lock);
			return (1);
		} else {
			debug_print(NOGET("\n Success in res %s "
				"execution Start"), res_name_or_fmri);
			PTHREAD_MTX_UNLOCK(res_ptr->res_mgmt_lock);
			return (0);
		}
		break;
	case RCT_STOP:
		res_ptr = get_resource_info(res_name_or_fmri);
		if (res_ptr == NULL) {
			debug_print(NOGET("\n Res %s is not "
				"present in the internal res table"),
				res_name_or_fmri);
			return (1);
		}
		//
		// Set the flag so that if there are any pending services to
		// start will not start, since the stop will anyways follow,
		// therby incresing the effeciency
		//
		res_ptr->is_res_stop_called = true;
		PTHREAD_MTX_LOCK(res_ptr->res_mgmt_lock);
		err = res_ptr->process_res(RCT_STOP);
		if (err != 0) {
			//
			// SCMSGS
			// @explanation
			// Not able to process the resource to stop all the
			// services.Previous messages to this should give the
			// reason.
			// @user_action
			// If it fails after couple of retries, contact SUN
			// vendor for more help
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in res %s "
				"execution Stop ", res_name_or_fmri);
			PTHREAD_MTX_UNLOCK(res_ptr->res_mgmt_lock);
			return (1);
		} else {
			debug_print(NOGET("\n Success in res %s "
				"execution Stop"), res_name_or_fmri);
			PTHREAD_MTX_UNLOCK(res_ptr->res_mgmt_lock);
			return (0);
		}
		break;
	case RCT_IS_RES_REG:
		res_ptr = get_resource_info(res_name_or_fmri);
		if (res_ptr != NULL) {
			debug_print(NOGET("\n RCT_IS_RES_REG: "
			"The res %s is already present in the res map table"),
				res_name_or_fmri);
			return (0);
		} else {
			//
			// Res not registered
			//
			debug_print(NOGET("\n RCT_IS_RES_REG:"\
			"The res %s is not present in the res map table"),
				res_name_or_fmri);

			return (1);
		}
		break;
	case RCT_IS_SVC_OFFLINE:
		if (is_res_svcs_offline(res_name_or_fmri)) {
			return (0);
		} else {
		}
			return (1);
		break;
	case RCT_IS_THIS_SVC_MANAGED:
		if (is_this_svc_managed(res_name_or_fmri)) {
			// Managed
			return (0);
		} else {
			// Not Managed
			return (1);
		}
		break;
	case RCT_IS_SVC_MANAGED:
		// Return 0 if any one service under the res is managed
		if (is_res_svcs_managed(svc_fmri_file))
			return (0);
		else
			return (1);
		break;
	case RCT_VALIDATE:
		break;
	case RCT_VALIDATE_UPDATE:
		res_ptr = get_resource_info(res_name_or_fmri);
		if (res_ptr == NULL) {
			debug_print(NOGET("\n The res %s is not been"
				"registered with the restarter, hence update"
				" cannot be performed"), res_name_or_fmri);
			return (1);
		}
		if (res_ptr->resource_update(res_name_or_fmri, res_grp_name,
				svc_fmri_file, retry_cnt, retry_interval)
		    != 0) {
			return (1);
		}
		return (0);
		break;
	case RCT_FINI:
		//
		// Free the res
		//
		res_ptr = get_resource_info(res_name_or_fmri);
		if (res_ptr)
			delete(res_ptr);
		return (1);
		break;
	}
}


//
// Return a pointer to the resource_info of res_name else return NULL
//
resource_info*
res_svc_basic_info::get_resource_info(char *res_name)
{
	string	res_str;
	resource_info *res_obg;
	std::map<string, resource_info *>::const_iterator it_res_info;

	//
	// Check if the resource is already registered
	//
	// Since the key value of hash table is string, convert
	// char* r_name to string
	//
	res_str = res_name;
	it_res_info = resource_info::res_name_hash_tbl.find(res_str);

	//
	// Confirm that the returned key from the res hash table is a valid one
	//
	if (it_res_info == resource_info::res_name_hash_tbl.end()) {
		return (NULL);
	} else {
		return (it_res_info->second);
	}
}

//
// Return true if any one of the svcs in the res is managed.
//
bool
res_svc_basic_info::is_res_svcs_managed(char *svc_fmri_file)
{
	FILE *fp;
	char stream_read[200] = {0};
	char file_name[100] = {0};
	char fmri_val[100] = {0};
	char *ptr1 = NULL, *ptr2 = NULL;
	char outstr[100] = {0};
	bool result = false;

	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In the is_res_svcs_managed"));

	if (!svc_fmri_file) {
		debug_print(NOGET("\n svc_fmri_file is NULL"));
		return (result);
	}

	if ((fp = fopen(svc_fmri_file, "r")) == NULL) {
		//
		// SCMSGS
		// @explanation
		// Cannot open the file provided the extended resource
		// property
		// @user_action
		// Chack for the permision and path validity of the file
		//
		logger.log(LOG_ERR, MESSAGE, "\nCan't open %s\n",
			svc_fmri_file);
		return (result);
	}

	while (!feof(fp)) {
		fscanf(fp, "%s", stream_read);
		if (strcmp(stream_read, "") == 0)
			break;
		debug_print(NOGET("\n input_stream %s"), stream_read);
		ptr1 = strrchr(stream_read, '>');
		ptr2 = strrchr(stream_read, '<');
		strncpy(file_name, ptr2+1, ptr1-ptr2-1);
		debug_print(NOGET("\nfilename : %s"), file_name);
		ptr1 = strchr(stream_read, '<');
		ptr2 = strchr(stream_read, '>');
		strncpy(fmri_val, ptr1+1, ptr2-ptr1-1);
		debug_print(NOGET("\nfmri_value : %s"), fmri_val);

		// Check if the svc frmi is already present
		if (get_svc_res(fmri_val)) {
			// Svc already present.
			result = true;
			goto cleanup;
		}
		memset(stream_read, 0, 199);
		memset(fmri_val, 0, 99);
		memset(file_name, 0, 99);
	}
cleanup:
	fclose(fp);
	return (result);
}
//
// Return true if all the services under the res else false
//
bool
res_svc_basic_info::is_res_svcs_offline(char *res_name)
{
	service_task *svc_task;
	resource_info *res_ptr;
	restarter_instance_state_t state;
	res_ptr = get_resource_info(res_name);
	if (res_ptr == NULL) {
		return (false);
	}
	svc_task = res_ptr->svc_fmri_head;
	while (svc_task != NULL) {
		state = svc_task->get_svc_cur_state();
		// If the restarter is disabled and enabled or
		// comes after death the state could also be
		// RESTARTER_STATE_NONE as that is the default
		// state which service gets initialised to.
		if ((state != RESTARTER_STATE_OFFLINE) &&
		    (state != RESTARTER_STATE_NONE))
			return (false);
		svc_task = svc_task->next_svc_ptr;
	}

	return (true);
}

//
// Given a svc fmri, returns the res name it belongs to else returns NULL
//
char*
res_svc_basic_info::get_svc_res(char *fmri)
{
	string	fmri_str;
	std::map<string, service_task *>::const_iterator it_svc_info;

	//
	// Check if the svc is present
	//
	// Since the key value of hash table is string, convert
	// char* fmri to string
	//
	fmri_str = fmri;
	it_svc_info = service_task::svc_fmri_hash_tbl.find(fmri_str);

	//
	// Confirm that the returned key from the svc hash table is a valid one
	//
	if (it_svc_info == service_task::svc_fmri_hash_tbl.end()) {
		return (NULL);
	} else {
		//
		// Return the res name of the svc
		//
		return (it_svc_info->second->res_info->get_res_name());
	}
}

//
// Given a svc fmri, returns true if the svc fmri is managed by the delegated
// restarter else false
//
bool
res_svc_basic_info::is_this_svc_managed(char *fmri)
{
	string	fmri_str;
	std::map<string, service_task *>::const_iterator it_svc_info;

	//
	// Check if the svc is present
	//
	// Since the key value of hash table is string, convert
	// char* fmri to string
	//
	fmri_str = fmri;
	it_svc_info = service_task::svc_fmri_hash_tbl.find(fmri_str);

	//
	// Confirm that the returned key from the svc hash table is a valid one
	//
	if (it_svc_info == service_task::svc_fmri_hash_tbl.end()) {
		return (false);
	} else {
		return (it_svc_info->second->is_svc_managed);
	}
}
