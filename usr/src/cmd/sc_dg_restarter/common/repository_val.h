/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_REPOSITORY_VAL_H
#define	_REPOSITORY_VAL_H

#pragma ident	"@(#)repository_val.h	1.5	08/05/20 SMI"

//
// This file interacts with the respository in getting and setting properties
// and property groups. The setting procedure is followed as done in inetd
//

#include <libscf.h>
#include <umem.h>

#define	REP_HDL_BIND_RETRIES 10

#define	SC_DELEGATED_PG (const char*) "sc_delegated_pg"
#define	SVC_CONTRACT_ID_PROP (const char*) "svc_contract_id_prop"
#define	SVC_DG_INIT_FLAG_PROP (const char*) "svc_dg_init_flag_prop"
#define	SVC_CUR_RES_STATE_PROP (const char*) "svc_cur_res_state_prop"
#define	SVC_RES_RETRY_COUNT_PROP (const char*) "svc_res_retry_count_prop"

class rep_val_handling {
private:
	//
	// Pointer to chage the property values
	//
	// Handle to the repository
	//
	scf_handle_t	*rep_handle;
	scf_propertygroup_t	*pg;
	scf_instance_t		*inst;
	scf_transaction_t	*trans;
	scf_transaction_entry_t	*entry;
	scf_property_t		*prop;
	int			init_status;
	void close_rep_interaction();
public:
	rep_val_handling();
	~rep_val_handling();
	int put_rep_vals(const char *svc_inst, int val, const char *pg_name,
			    const char *prop_name);

	char *get_method(const char *svc_inst, const char *pg_name,
				const char *prop_name,
				int64_t *int_val, int *err);

	void rep_hdl_init();
};

#endif /* _REPOSITORY_VAL_H */
