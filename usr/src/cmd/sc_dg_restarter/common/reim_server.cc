//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
#pragma ident	"@(#)reim_server.cc	1.10	08/05/20 SMI"

// This file contains the implementation of door server for ReIM module.

// Include headers.
#include <rgm/reim_common.h>
#include "sc_delegated_restarter.h"
#include "res_svc_basic_info.h"


// Debug Switch.
#ifdef DEBUG
static int debug = 1;
#else
static int debug = 0;
#endif

// Class declaration for calling methods of SC_delegated_restarter.
class res_svc_basic_info;

// Functions used in reim_server.
static int attach_door(int door_id);
static void syslognomem(void);
// xdr decoding function.
static bool_t xdr_smf_proxy_resource_input_args(
	register XDR *xdrs, smf_proxy_rs_input_args *res_args);
// door server function.
static void reim_door_server(void *cookie,
    char *dataptr, size_t data_size,
    door_desc_t *desc_ptr, size_t ndesc);
// door service creation function.
static int reim_svc_door_create(door_server *proc);


// ring buffer for debug print.
dbg_print_buf sc_reim_dbg_buf(SC_DG_DBG_BUFSIZE);


//
// Routine for tracing/logging debug info.
// Tracing is always on. Logging (into syslog and the console) is turned
// only if Debugflag is turned on.
//
void
reim_debug_print(char *fmt, ...)
{
	/*lint -e534 */
	if (debug) {
		va_list args;
		char str1[SC_DG_BUFSIZ];
		va_start(args, fmt);	//lint !e40
		if (vsnprintf(str1, SC_DG_BUFSIZ, fmt, args) >= SC_DG_BUFSIZ) {
			str1[SC_DG_BUFSIZ - 1] = '\0';
		}
		va_end(args);
		syslog(LOG_NOTICE, str1);
	}
}

//
// Syslog message that we cannot allocate memory.
//
static void
syslognomem(void) {
	sc_syslog_msg_handle_t handle;
	(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_SMF_DR_TAG, "");
	//
	// SCMSGS
	// @explanation
	// Need explanation of this message!
	// @user_action
	// Need a user action for this message.
	//
	(void) sc_syslog_msg_log(handle, LOG_ERR,
	    MESSAGE,
	    "Failed to allocate memory In ReIM module");
	sc_syslog_msg_done(&handle);
}

//
// This function is used to decode the "XDR"ed data arguments
//
static bool_t
xdr_smf_proxy_resource_input_args(register XDR *xdrs,
    smf_proxy_rs_input_args *res_args) {
#if defined(_LP64) || defined(_KERNEL)
	register int *buf;
#else
	register long *buf;
#endif
sc_syslog_msg_handle_t handle;
	// Check if method type is of Int.
	if (!xdr_int(xdrs, (int*)&res_args->method)) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_SMF_DR_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE,
		    "ReIM Module : XDR error in decoding Proxy method type");
		sc_syslog_msg_done(&handle);
		return (FALSE);
	}

	// Decode data based on Method.
	switch (res_args->method) {
		case METHOD_BOOT:
		case METHOD_START:
		case METHOD_INIT:
		case METHOD_VALIDATE:
		case METHOD_VALIDATE_IS_SVC_MANAGED:
		case METHOD_VALIDATE_U:
			// Allocate Memory.:
			res_args->data = (smf_proxy_rs_boot_start_args *)
			    calloc(1, sizeof (smf_proxy_rs_boot_start_args));
			if (res_args->data == NULL) {
				return (FALSE);
			}
			// Decode the args.
			if (!xdr_smf_proxy_rs_boot_start_args(xdrs,
			    (smf_proxy_rs_boot_start_args *)res_args->data)) {
				(void) sc_syslog_msg_initialize(&handle,
					SC_SYSLOG_SMF_DR_TAG, "");
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "ReIM Module: XDR error decoding Proxy"
				    " method args");
				sc_syslog_msg_done(&handle);

				return (FALSE);
			}
			break;

		case METHOD_STOP:
		case METHOD_FINI:
		case METHOD_START_IS_RES_REG:
		case METHOD_START_IS_SVC_OFFLINE:

			// Allocate Memory.
			res_args->data = (smf_proxy_rs_common_args *)
			    calloc(1, sizeof (smf_proxy_rs_common_args));

			// Decode the argument data.
			if (!xdr_smf_proxy_rs_common_args(xdrs,
			    (smf_proxy_rs_common_args *)res_args->data)) {
				(void) sc_syslog_msg_initialize(&handle,
					SC_SYSLOG_SMF_DR_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "ReIM Module: XDR error decoding Proxy"
				    " method args");
				sc_syslog_msg_done(&handle);
				return (FALSE);
			}
			break;
		case METHOD_IS_THIS_SVC_MANAGED:

			// Allocate Memory.
			res_args->data = (smf_proxy_rs_boot_extra_args *)
			    calloc(1, sizeof (smf_proxy_rs_boot_extra_args));

			// Decode the argument data.
			if (!xdr_smf_proxy_rs_boot_extra_args(xdrs,
			    (smf_proxy_rs_boot_extra_args *)res_args->data)) {
				(void) sc_syslog_msg_initialize(&handle,
					SC_SYSLOG_SMF_DR_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "ReIM Module: XDR error decoding Proxy"
				    " method args");
				sc_syslog_msg_done(&handle);
				return (FALSE);
			}
			break;
	}
	return (TRUE);
}

// This function is used to get the corresponding enum values w.r.t
// SMF Proxy method enum values for method types.If both proxy methods
// and SC delegated restarter uses the same enum his function can be deleted.

static res_call_type
method_type_to_restarter_type(method_type_t method) {
	switch (method) {
		case METHOD_BOOT:
			return (RCT_BOOT);
			break;
		case METHOD_START:
			return (RCT_START);
			break;
		case METHOD_VALIDATE:
			return (RCT_VALIDATE);
			break;
		case METHOD_VALIDATE_U:
			return (RCT_VALIDATE_UPDATE);
			break;
		case METHOD_INIT:
			return (RCT_INIT);
			break;
		case METHOD_FINI:
			return (RCT_FINI);
			break;
		case METHOD_STOP:
			return (RCT_STOP);
			break;
		case METHOD_START_IS_RES_REG:
			return (RCT_IS_RES_REG);
			break;
		case METHOD_START_IS_SVC_OFFLINE:
			return (RCT_IS_SVC_OFFLINE);
			break;
		case METHOD_VALIDATE_IS_SVC_MANAGED:
			return (RCT_IS_SVC_MANAGED);
			break;
		case METHOD_IS_THIS_SVC_MANAGED:
			return (RCT_IS_THIS_SVC_MANAGED);
			break;
		default:
			break;
	}
}

//
// This function is called by the door server function to handle the client
// data based on the method type.
//
static int
handle_client_data(method_type_t method, void *data) {
	int return_code = -1;
	smf_proxy_rs_boot_start_args *rs_args = NULL;
	smf_proxy_rs_common_args *rs_common_args = NULL;
	char *svc_fmri = NULL;
	res_svc_basic_info res_svc_info;
	res_call_type restarter_meth;
	sc_syslog_msg_handle_t handle;

	reim_debug_print(NOGET("In Handle Client Data"));
	switch (method) {
		case METHOD_BOOT:
		case METHOD_START:
		case METHOD_VALIDATE:
		case METHOD_VALIDATE_U:
		case METHOD_VALIDATE_IS_SVC_MANAGED:
		case METHOD_INIT: {

			// Get the method type for SC Restarter.
			restarter_meth = method_type_to_restarter_type(method);
			// Allocate Memory.
			rs_args = (smf_proxy_rs_boot_start_args *)calloc(1,
			    sizeof (smf_proxy_rs_boot_start_args));
			if (rs_args == NULL) {
				syslognomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;
			}
			rs_args->common_args = (smf_proxy_rs_common_args *)
			    calloc(1, sizeof (smf_proxy_rs_common_args));
			if (rs_args->common_args == NULL) {
				syslognomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;
			}
			// Get the R Name.
			rs_args->common_args->rs_name =
			    strdup((((smf_proxy_rs_common_args*)
			    ((smf_proxy_rs_boot_start_args*)(data))
			    ->common_args)->rs_name));
			if (rs_args->common_args->rs_name == NULL) {
				syslognomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;
			}

			// Get the RG Name.
			rs_args->common_args->rg_name =
			    strdup((((smf_proxy_rs_common_args*)
			    ((smf_proxy_rs_boot_start_args*)(data))
			    ->common_args)->rg_name));
			if (rs_args->common_args->rg_name == NULL) {
				syslognomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;
			}

			//
			// If Data Field is NULL then the method is not called
			// for registeration.
			// If its non-null then we need to get the other
			// data like fmri,retry count
			// and retry interval and pass it on the
			// the SC restarter.
			//
			if (((smf_proxy_rs_boot_extra_args*)
			    ((smf_proxy_rs_boot_start_args*)(data))
			    ->boot_args) != NULL) {
				// Allocate Memory.
				rs_args->boot_args=
				    (smf_proxy_rs_boot_extra_args *)calloc(1,
				    sizeof (smf_proxy_rs_boot_extra_args));

				if (rs_args->boot_args == NULL) {
					syslognomem();
					return_code = MEMORY_ALLOCATION_FAILED;
					goto finished;
				}

				// Get the FMRI.
				rs_args->boot_args->fmri =
				    strdup((((smf_proxy_rs_boot_extra_args*)
				    ((smf_proxy_rs_boot_start_args*)(data))
				    ->boot_args)->fmri));

				if (rs_args->boot_args->fmri == NULL) {
					syslognomem();
					return_code = MEMORY_ALLOCATION_FAILED;
					goto finished;
				}

				// Get the Retry Count.
				rs_args->boot_args->retry_cnt =
				    (((smf_proxy_rs_boot_extra_args*)
				    ((smf_proxy_rs_boot_start_args*)(data))
				    ->boot_args)->retry_cnt);

				// Get the Retry Interval.
				rs_args->boot_args->retry_interval =
				    (((smf_proxy_rs_boot_extra_args*)
				    ((smf_proxy_rs_boot_start_args*)(data))
				    ->boot_args)->retry_interval);

				// Call the SC_delegated_restarter interface to
				// register the data.
				return_code = res_svc_info.process_method
				    (rs_args->common_args->rs_name,
				    rs_args->common_args->rg_name,
				    restarter_meth,
				    rs_args->boot_args->fmri,
				    rs_args->boot_args->retry_cnt,
				    rs_args->boot_args->retry_interval);
			}
			else
			{
				//
				// Call the SC_delegated_restarter interface
				// to start the svc.
				//
				return_code = res_svc_info.process_method
				    (rs_args->common_args->rs_name,
				    rs_args->common_args->rg_name, RCT_START);
			}
			break;
		}
		case METHOD_FINI:
		case METHOD_STOP:
		case METHOD_START_IS_RES_REG:
		case METHOD_START_IS_SVC_OFFLINE: {

			// Get the method type for SC restarter.
			restarter_meth = method_type_to_restarter_type(method);
			// Allocate Memory.
			rs_common_args = (smf_proxy_rs_common_args *)
			    calloc(1, sizeof (smf_proxy_rs_common_args));
			if (rs_common_args == NULL) {
				syslognomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;
			}
			// Get the R Name.
			rs_common_args->rs_name =
			    strdup(((smf_proxy_rs_common_args*)
			    (data))->rs_name);
			if (rs_common_args->rs_name == NULL) {
				syslognomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;
			}
			// Get the RG name.
			rs_common_args->rg_name =
			    strdup(((smf_proxy_rs_common_args*)
			    (data))->rg_name);
			if (rs_common_args->rg_name == NULL) {
				syslognomem();
				return_code = MEMORY_ALLOCATION_FAILED;
				goto finished;
			}
			// Call the SC_delegated_restarter interface.
			return_code = res_svc_info.process_method
			    (rs_common_args->rs_name,
			    rs_common_args->rg_name, restarter_meth);
			break;
		}
		case METHOD_IS_THIS_SVC_MANAGED: {
			// Get the method type for SC restarter.
			restarter_meth = method_type_to_restarter_type(method);
			// Allocate Memory.
			char *svc_fmri;
			(void) sc_syslog_msg_initialize(&handle,
					SC_SYSLOG_SMF_DR_TAG, "");

			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
					"In Reim Server Is Svc Managed");

			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
				"Data Frmi: %s",
			(((smf_proxy_rs_boot_extra_args*)(data))->fmri));
			// Get the fmri val
			svc_fmri = strdup(((smf_proxy_rs_boot_extra_args*)
					(data))->fmri);

			if (!svc_fmri) {
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				MESSAGE, "Expecting an svc fmri value instead"
					"of NULL");
				goto finished;
			}
			// Call the SC_delegated_restarter interface.
			// Check if the svc fmri is managed or not
			return_code = res_svc_info.process_method
			    (svc_fmri, NULL, restarter_meth);
			break;
		}
		default:
			break;
	}
finished:
	if (rs_args) {
		if (rs_args->common_args) {
			if (rs_args->common_args->rs_name)
				free(rs_args->common_args->rs_name);
			if (rs_args->common_args->rg_name)
				free(rs_args->common_args->rg_name);
			free(rs_args->common_args);
		}
		if (rs_args->boot_args) {
			if (rs_args->boot_args->fmri)
				free(rs_args->boot_args->fmri);
			free(rs_args->boot_args);
		}
		free(rs_args);
	}
	if (rs_common_args) {
		if (rs_common_args->rs_name)
			free(rs_common_args->rs_name);
		if (rs_common_args->rg_name)
			free(rs_common_args->rg_name);
		free(rs_common_args);
	}
	if (svc_fmri)
		free(svc_fmri);
	return (return_code);

}



/*
 * reim_door_server :
 *
 * This is the process which acts as the server for the
 * ReIM. All the proxy resource calls which are made
 * will come to this server which will determine the API
 * that has been called and then appropriately call the
 * corresponding function. And after the function returns
 * it will send back the results.
 * All the data that comes from the proxy services is marshalled
 * using the xdr library and so the server needs to unmarshall
 * it before it can call the api functions.
 * Similarly, when the server wants to return the results
 * back to the calling function it marshall's the results
 * which are unmarshalled at the other end to get the
 * results.
*/
static void
reim_door_server(void *cookie,
    char *dataptr, size_t data_size,
    door_desc_t *desc_ptr, size_t ndesc) {
	smf_proxy_rs_input_args_t *res_args;
	// Used to store the input args after unmarshalling
	XDR xdrs;
	// Initialise return code.
	int return_code = -1;
	sc_syslog_msg_handle_t handle;

	// Create Memory for the arguments.
	res_args = (smf_proxy_rs_input_args_t*)calloc(1,
	    sizeof (smf_proxy_rs_input_args_t));

	if (res_args == NULL) {
		syslognomem();
		return_code = MEMORY_ALLOCATION_FAILED;
		goto finished;
	}

	// Decode the args.
	xdrmem_create(&xdrs, dataptr, data_size, XDR_DECODE);

	// XDR the args.
	if (!xdr_smf_proxy_resource_input_args(&xdrs, res_args)) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_SMF_DR_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE,
		    "ReIM Module : XDR Error in decoding proxy resource args.");
		sc_syslog_msg_done(&handle);
		return_code = XDR_ERROR_AT_SERVER;
		goto finished;
	}

	// Based on methods do the appropriate action.
	return_code = handle_client_data(res_args->method, res_args->data);
finished:
	reim_debug_print(NOGET("BEFORE Door Return"));
	xdr_destroy(&xdrs);
	if (res_args) {
		if (res_args->data)
			free(res_args->data);
		free(res_args);
	}
	// Return from door call.
	door_return((char *)&return_code, sizeof (return_code), NULL, 0);
}

//
// reim_svc_door_create
// This function will start the door server specified by
// the calling function and return the door id to the
// calling function
//
static int
reim_svc_door_create(door_server *proc) {
	int did, err;
	sc_syslog_msg_handle_t handle;
	// sigset_t sig_set = {0}, orig_set = {0};
	int i;


	// Call door_create to create door server
	if ((did = door_create(proc, NULL, 0)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_SMF_DR_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE,
		    "ReIM Module : Unable to create door : %s", strerror(err));
		sc_syslog_msg_done(&handle);
		return (REIM_DOOR_CREATE_FAILED);
	}

	// Return the door id
	return (did);
}

//
//  Interface to initialize and register service; performed at server startup
//
int
reim_svc_reg(door_server *proc)
{
	int did = -1, res, err;
	sc_syslog_msg_handle_t handle;
	/* Create door server */
	if ((did = reim_svc_door_create(proc)) == -1)  {
		goto REIM_SVC_REG_END;
	}

	/* Attach the door file to the door server */
	if ((res = attach_door(did)) == -1) {
		goto REIM_SVC_REG_END;
	}

	return (0);

REIM_SVC_REG_END:
	(void) sc_syslog_msg_initialize(&handle,
		SC_SYSLOG_SMF_DR_TAG, "");
	//
	// SCMSGS
	// @explanation
	// Need explanation of this message!
	// @user_action
	// Need a user action for this message.
	//
	(void) sc_syslog_msg_log(handle, LOG_ERR,
	    MESSAGE,
	    "ReIM Module : Unable to register service %s", strerror(err));
	sc_syslog_msg_done(&handle);
	return (REIM_DOOR_SVC_REG_FAILED);
}


//
// attach_door
// This function will be called once by the reim_svc_reg
// to attach the door file to the door_server. However, if we
// are running on or above Sol 10 then this function will be
// called when we want to register another zone and allow
// that zone also to be able to communicate via a door.
// We will attach the door file in the zone to the door_server
// in this function
//
int
attach_door(int door_id) {
	int err, fd;
	sc_syslog_msg_handle_t handle;
	/* Remove the door file if it already exists */
	unlink(REIM_DOOR_PATH);

	/* Create the door file */
	if ((fd = open(REIM_DOOR_PATH, O_CREAT | O_RDWR,
	    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) <= 0) {
		int err = errno;
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_SMF_DR_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE,
		    "ReIM Module : Creation of door file %s failed : %s",
		    REIM_DOOR_PATH,
		    strerror(err));
		sc_syslog_msg_done(&handle);

		goto error_return;
	}
	/* Close the file */
	close(fd);

	/* Attach the file to the door desc */
	if (fattach(door_id, REIM_DOOR_PATH) != 0) {
		int err = errno;
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_SMF_DR_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE,
		    "ReIM Module : fattach of door file %s failed : %s",
		    REIM_DOOR_PATH,
		    strerror(err));
		sc_syslog_msg_done(&handle);
		goto error_return;
	}

	return (0);

error_return:
	return (REIM_CREATE_ATTACH_DOOR_FAILED);
}


// Interface for the restarter daemon to register/start reim server.
void *
register_reim(void *) {
	sc_syslog_msg_handle_t handle;
	if ((reim_svc_reg(reim_door_server)) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_SMF_DR_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE,
		    "fatal:unable to register ReIM service; : aborting node");
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}
	while (1) {
		pause();
	}
	return (NULL);
}
