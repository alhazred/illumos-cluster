/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)service_mgmt.cc	1.17	09/04/07 SMI"

#include "service_mgmt.h"
#include "svc_timeout_wait.h"
#include <iostream>
#include <fstream.h>
#include <stdlib.h>
#include <project.h>
#include <sys/task.h>
#include "sc_dg_error.h"
#include <pwd.h>
#define	PWD_BUF_SIZE 1024
std::map<string, service_task *> service_task::svc_fmri_hash_tbl;

//
// Intialize the restarter event handle and lock to protect it
//
restarter_event_handle_t *service_task::res_evt_handle = NULL;
pthread_mutex_t	service_task::res_evt_hdl_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t	service_task::res_failover_lock = PTHREAD_MUTEX_INITIALIZER;
//
// Constructor for the service task
//
service_task::service_task(char *service_fmri, int *err):svc_fmri(NULL),
				is_svc_restart(false),
				next_svc_ptr(NULL),
				prev_svc_ptr(NULL),
				is_svc_managed(false)
{
	typedef pair<string, service_task *> fmri_addr_pair;
	pair<std::map<string, service_task *>::iterator, bool> pr_res;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
	string svc_str;

	*err = 0;
	debug_print(NOGET("\n In service_task::service_task"));

	//
	// Insert the svc_fmri into the service hash table
	// Since the service fmri is the key in the hash table and it is a
	// string, convert char* to a string before inserting
	//
	svc_fmri = new char[strlen(service_fmri)+1];
	if (svc_fmri) {
		sprintf(svc_fmri, "%s", service_fmri);
	} else {
		//
		// SCMSGS
		// @explanation
		// Problem in creating memory space during service management
		// of SMF delegated restarter
		// @user_action
		// Check the system memory usage
		//
		logger.log(LOG_ERR, MESSAGE, "\n Unable to allocate memory "
			"in sc delegated service_mgmt");
		*err = -1;
		return;
	}

	svc_str = svc_fmri;
	pr_res = svc_fmri_hash_tbl.insert(fmri_addr_pair(svc_str, this));

	if (pr_res.second == true) {
		debug_print(NOGET("\n FMRI map insert success %s"), svc_fmri);
	} else {
		debug_print(NOGET("\n FMRI map insert exist :%s"), svc_fmri);
	}

	//
	// Set only during updation
	//
	is_update_marked = false;

	//
	// Check if svc is transient or not
	//
	svc_is_transient = false;
	//
	// Create a contract info for this service instance, if its not a
	// transient svc
	//
	svc_contract_info = NULL;
	svc_contract_info = new contract_info(this, err);
	if (svc_contract_info == NULL) {
		logger.log(LOG_ERR, MESSAGE, "\n Unable to allocate memory "
			"in sc delegated service_mgmt");
		*err = -1;
		return;
	}
	if (*err == -1) {
		//
		// SCMSGS
		// @explanation
		// Problem in initializing the contract for the service
		// specified
		// @user_action
		// Depending on the error, check the SMF man page.Contact SUN
		// vendor for help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Could not initialize "
			"the contract info for svc :%s", svc_fmri);
		return;
	}

	//
	// Specify that the threadpool does not grow dynamically and
	// the name of the thread pool is the service_fmri
	//
	svc_fmri_thread_pool.modify(false,  DESIRED_SVC_FMRI_THREADS,
				svc_fmri);
	pthread_mutex_init(&svc_fmri_thread_pool_lock, NULL);

	//
	// Initialize current and next repository states
	//
	if (resource_info::res_dg_init_flag) {
		//
		// Set the current state if the restarter had come up after
		// death
		//
		dr_intrnl_inst_state_type state =
			conv_cur_smf_to_internal_state();
		cur_inst_state = inst_states[state].smf_state;
	} else {
		cur_inst_state = RESTARTER_STATE_NONE;
	}
	next_inst_state = RESTARTER_STATE_NONE;
}

service_task::~service_task()
{
	debug_print(NOGET("\n Destructing service: %s"), svc_fmri);
	//
	// Remove the entry from hash table
	//
	string svc_str;
	svc_str = svc_fmri;
	svc_fmri_hash_tbl.erase(svc_str);
	//
	// Take care of freeing allocated memories
	//
	if (svc_fmri)
		delete(svc_fmri);
	if (svc_contract_info)
		delete(svc_contract_info);
	pthread_mutex_destroy(&res_failover_lock);
	pthread_mutex_destroy(&res_evt_hdl_lock);
}

//
// Locking and signalling the completion of the res, after reducing the
// svc count
//
void
service_task::reduce_svc_count()
{
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
	int err;
	//
	// Reduce the svc_count and signal waiting res if 0
	//
	//
	// Signal if this is the last service.DO this only during res starting
	// or stopping
	//
	if ((res_info->cur_res_state == RS_STARTING) ||
		(res_info->cur_res_state == RS_STOPPING)) {
		PTHREAD_MTX_LOCK(res_info->res_svc_lock.res_exec_lock);
		debug_print(NOGET("\n In res start signal lock"));
		//
		// reduce svc completion count by 1
		//
		if (res_info->res_svc_lock.svc_count > 0)
			res_info->res_svc_lock.svc_count--;
		debug_print(NOGET("\n**To Signal "
			"count: %d"), res_info->res_svc_lock.svc_count);

		if (res_info->res_svc_lock.svc_count == 0) {
			err = pthread_cond_signal
			(&res_info->res_svc_lock.res_exec_cond);
			debug_print(NOGET("\n**Signalled: %d"), err);

			if (err == EINVAL) {
				debug_print(NOGET(
					"\nCondition var not initilized:%s"),
				strerror(err));
				PTHREAD_MTX_UNLOCK(res_info->res_svc_lock.
						res_exec_lock);
				abort();
			}
		}

		PTHREAD_MTX_UNLOCK(res_info->res_svc_lock.res_exec_lock);
		return;
	}
}


dr_intrnl_inst_state_type
service_task::conv_cur_smf_to_internal_state()
{

	char	*state;
	state = smf_get_state(svc_fmri);

	if (state == NULL)
		return (DR_NONE);

	if (strcmp(state, SCF_STATE_STRING_UNINIT) == 0)
		return (DR_UNINITIALIZED);
	else if (strcmp(state, SCF_STATE_STRING_MAINT) == 0)
		return (DR_MAINTENANCE);
	else if (strcmp(state, SCF_STATE_STRING_OFFLINE) == 0)
		return (DR_OFFLINE);
	else if (strcmp(state, SCF_STATE_STRING_DISABLED) == 0)
		return (DR_DISABLED);
	else if (strcmp(state, SCF_STATE_STRING_ONLINE) == 0)
		return (DR_ONLINE);
	else if (strcmp(state, SCF_STATE_STRING_DEGRADED) == 0)
		return (DR_DEGRADED);
	else if (strcmp(state, SCF_STATE_STRING_NONE) == 0)
		return (DR_NONE);
}

//
// Update the state of the service fmri in the repository to the new_cur state
//
void
service_task::update_state(char *name, restarter_instance_state_t new_cur,
			char *aux_state, restarter_error_t restarter_e)
{
	int	err;
	restarter_instance_state_t 	cur_state_t;
	restarter_instance_state_t 	next_state_t;
	/* XXX We need to add cluster related states to librestarter
	   XXX Until then, we just ignore aux_state */
	restarter_str_t                 new_aux_state = restarter_str_none;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In service_task:update_state"));

	//
	// Update the state of the instance in the repository
	//
	(void) pthread_mutex_lock(&service_task::res_evt_hdl_lock);
	if ((err = restarter_set_states(service_task::res_evt_handle, name,
					cur_inst_state, new_cur,
					next_inst_state,
					restarter_string_to_state("none"),
					restarter_e, new_aux_state)) != 0) {
		//
		// SCMSGS
		// @explanation
		// Have problems in starting a communication with the SMF
		// repository
		// @user_action
		// Depending on the error, check the SMF man page.Contact SUN
		// vendor for help.
		//
		logger.log(LOG_ERR, MESSAGE, "Error setting states in"
			"repository: %s: %d\n", strerror(err), err);
		(void) pthread_mutex_unlock(&service_task::res_evt_hdl_lock);
		return;
	}
	cur_inst_state = new_cur;
	(void) pthread_mutex_unlock(&service_task::res_evt_hdl_lock);
}

//
// Executes the particular method specified by method_name, performing the
// required operation specified by method_oper.Like executing start exec
// method, stop exec method....
// Returns 0 on successful method execution, -1 on error.
// When -1 is returned, also sets method_exit to the method exit_code whenever
// avialable
//
// service_task::run_method(char *inst, char *pg_name, char *prop_name,
//
int
service_task::run_method(char *inst, svc_exec_mthd_type mthd_type,
			int *method_exit)
{
	char 	*method = NULL;
	char	*tmp_str = NULL;
	char	*user = NULL;
	pid_t 	pid;
	int 	ret_status;
	int	status_fd;
	int 	sig;
	int	ret;
	int64_t prop_int_val;
	time_t	exec_timeout;
	ct_stathdl_t	status_hdl;
	typedef pair<ctid_t, contract_info *> contract_addr_pair;
	pair<std::map<ctid_t, contract_info *>::iterator, bool> insert_res;

	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In service_task::run_method"));

	rep_val_handling rep_method_hdl;
	method = rep_method_hdl.get_method(svc_fmri,
	    exec_mthd_info[mthd_type].pg_name,
	    exec_mthd_info[mthd_type].prop_name, &prop_int_val, &ret);

	if (method == NULL) {
		debug_print(NOGET("\n Got a NULL method to run"));
		return (-1);
	}

	//
	// Replace %m if it exists, with the respective method names
	//
	tmp_str = strstr(method, "%m");
	if (tmp_str) {
		int len = strlen(tmp_str + 2) +
			strlen(exec_mthd_info[mthd_type].pg_name) + 1;
		char *tstrptr = new char[len];
		// Assuming len does not exceed MAXLEN of method because
		// there is no reason for something to be after %m,else
		// have to re create a new buffer
		strcpy(tstrptr, exec_mthd_info[mthd_type].pg_name);
		// Copy anything leftover after %m, and copy the tmp str
		// to the original
		strcat(tstrptr, tmp_str + 2);
		strcpy(tmp_str, tstrptr);
		delete(tstrptr);
	}

	debug_print(NOGET("\n Method String to Run: %s\n"), method);
	//
	// Handle the :true case
	// Just return success without doing anything
	//
	if (restarter_is_null_method(method)) {
		return (0);
	}

	// Get the user name from the  method credentials.
	// The pgname is "start" and the prop name is "user"
	user = rep_method_hdl.get_method(svc_fmri, "start", "user",
	    &prop_int_val, &ret);

	//
	// Handle the :kill case
	// Do only if its a stop method
	//
	if (mthd_type == SVC_EXEC_STOP) {
		sig = restarter_is_kill_method(method);
		if (sig > 0) {
			//
			// Send the signal to the processes under the contract
			// of this service instance
			//
			if (svc_contract_info) {
				if ((svc_contract_info->contract_send_sig(sig))
				    == 0) {
					return (0);
				} else {
					*method_exit = errno;
					return (-1);
				}
			}
		}
	}
	//
	// Activate the contract template
	//
	sc_restarter_contract::instance().contract_prefork();

	pid = fork1();

	sc_restarter_contract::instance().contract_postfork();

	if (pid == -1) {
		//
		// SCMSGS
		// @explanation
		// Have problems in starting a communication with the SMF
		// repository
		// @user_action
		// Depending on the error, check the SMF man page.Contact SUN
		// vendor for help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n%s: Couldn't fork to execute"
			" method %s", inst, method);
		delete(method);
		*method_exit = errno;
		return (-1);
	}

	//
	// Execute the method
	//
	if (pid == 0) {
		struct passwd pass, *passp = NULL;
		char pass_buff[PWD_BUF_SIZE];
		boolean_t user_not_root = B_FALSE;
		uid_t uid;
		gid_t gid;

		// Check for user name.
		if ((user != NULL) && (strcmp(user, "root") != 0)) {
			if (getpwnam_r(user, &pass, pass_buff,
			    PWD_BUF_SIZE, &passp) != 0) {
				//
				// SCMSGS
				// @explanation
				// Invalid username
				// @user_action
				// Please check the user specified under
				// method_credentials in the smf manifest file
				// for this fmri.
				//
				logger.log(LOG_ERR, MESSAGE, "invalid user name"
				    " %s for method %s fmri %s", user, method,
				    svc_fmri);
				return (-1);
			}
			uid = pass.pw_uid;
			gid = pass.pw_gid;
			user_not_root = B_TRUE;
		} else {
			// root
			uid = getuid();
			gid = getgid();
		}

		//
		// Set the project, before executing.This piece of code was
		// taken from pmf_contract.cc
		//
		if (res_info->project_name != NULL &&
			*res_info->project_name != '\0') {
			struct passwd pwd, *pwdp;
			char pwd_buff[PWD_BUF_SIZE];

			if (getpwuid_r(uid, &pwd, pwd_buff,
				PWD_BUF_SIZE, &pwdp) == 0) {
				if (setproject(res_info->project_name,
					pwd.pw_name, TASK_NORMAL) == -1) {
					debug_print(NOGET("\n"
					"setproject: %s;attempting to continue"
					"the process with the system default"
						" project."), strerror(errno));
					(void) setproject("default",
						pwd.pw_name, TASK_NORMAL);
				} else {
					debug_print(NOGET("\n Change project "
					"name to%s "), res_info->project_name);
				}
			}
		} else {
			debug_print(NOGET("\n Project name is null "
				"before exec"));
		}

		if (user_not_root) {
			setregid(gid, gid);
			setreuid(uid, uid);
		}

		//
		// Fill in the SMF_FMRI environment variable.
		// This is used by some of the smf services.
		// The rest of the SMF_ env variables like
		// SMF_METHOD, SMF_RESTARTER and SMF_ZONENAME
		// are not used.
		//

		char *env[2];
		env[0] = (char *)calloc(strlen("SMF_FMRI=") + strlen(
		    svc_fmri) + 1, 1);
		if (env[0] == NULL) {
			logger.log(LOG_ERR, MESSAGE, "\n Unable to allocate "
			    "memory in sc delegated service_mgmt");
			return (-1);
		}

		(void) sprintf(env[0], "SMF_FMRI=%s", svc_fmri);
		env[1] = (char*)0;
		(void) execle("/bin/sh", "/bin/sh", "-c", method, NULL,
		    env);
		//
		// SCMSGS
		// @explanation
		// Cannot execute the specified method of the service fmri
		// @user_action
		// The exec call has errored out not being able to
		// execute.Test and see if the method is able to be executed
		// without sc_delegated_restarter.
		//
		logger.log(LOG_ERR, MESSAGE, "\n %s: exec failed for method "
			"%s: %s \n", inst, method, strerror(errno));
		return (-1);
	}

	if (mthd_type == SVC_EXEC_START) {

		//
		// Check if the svc is transient.If its transient dont update
		// the contract info, because it will not be managed
		//
		if (svc_is_transient) {
			goto transient_cont;
		}

		// Update the contract service instance relationship with
		// the new contract created if it is a start method. Also
		// put the contract info in the repository and contract hash
		// table
		//

		if ((status_fd = open64(CONTRACT_LATEST_PATH, O_RDONLY)) ==
		    -1) {
				//
				// SCMSGS
				// @explanation
				// Cannot open the specified contract file
				// @user_action
				// Man contract(4), should help in providing
				// the reason.
				//
				logger.log(LOG_ERR, MESSAGE, "\nFailed to "
					"open contract file %s: %s",
					CONTRACT_LATEST_PATH, strerror(errno));
				return (-1);
			}
		if (ct_status_read(status_fd, CTD_COMMON, &status_hdl) != 0) {
			//
			// SCMSGS
			// @explanation
			// Cannot read the contract status file descriptor.
			// @user_action
			// Depending on the error, check the SMF man
			// page.Contact SUN vendor for help.
			//
			logger.log(LOG_ERR, MESSAGE, "\nFailed to read the "
				"status_fd");
			return (-1);
		}
		if (svc_contract_info) {
			svc_contract_info->contract_id =
				ct_status_get_id(status_hdl);
		}
		//
		// Free the status hdl
		//
		ct_status_free(status_hdl);

		//
		// Store the contract id in the repository
		//
		if (svc_contract_info) {
			rep_val_handling rep_ctr_hdl;
			ret = rep_ctr_hdl.put_rep_vals(svc_fmri,
					svc_contract_info->contract_id,
					SC_DELEGATED_PG,
					SVC_CONTRACT_ID_PROP);
			if (ret != 0) {
				debug_print(NOGET("\n Could not "
				"put the prop of svc %s,in the repository:%s"),
				svc_fmri, scf_strerror((scf_error_t)ret));
				return (-1);
			}
			//
			// Insert the contract info into the contract hash
			// table
			//
			insert_res =
				contract_info::contract_info_hash_tbl.insert(
			contract_addr_pair(svc_contract_info->contract_id,
					svc_contract_info));
		}
	}

	//
	// Start the timer on the pid. If the timer expires kill the pid
	// and the signal will be caught below and returns an error and the
	// clean up of all the processes under this contract will be done in
	// the stop method
	//
transient_cont:
	rep_val_handling rep_exec_hdl;
	exec_mthd_pid = pid;
	switch (mthd_type) {
	case SVC_EXEC_START:
		rep_exec_hdl.get_method(svc_fmri,
				    exec_mthd_info[SVC_TIMEOUT_START].pg_name,
			exec_mthd_info[SVC_TIMEOUT_START].prop_name,
			&prop_int_val, &ret);
		break;
	case SVC_EXEC_STOP:
		rep_exec_hdl.get_method(svc_fmri,
				    exec_mthd_info[SVC_TIMEOUT_STOP].pg_name,
			exec_mthd_info[SVC_TIMEOUT_STOP].prop_name,
			&prop_int_val, &ret);
		break;
	}

	if (ret == -1) {
		debug_print(NOGET("\n Error in reading the method "
			"timeout prop value from the repository"));
		return (-1);
	}
	exec_timeout = (time_t)prop_int_val;

	if (exec_timeout >= 0) {
		svc_timeout_wait::instance().wait(this, exec_timeout);
	}

	//
	// Wait for the exec to return
	//
	while (waitpid(pid, &ret_status, NULL) != pid)
		continue;

	if (WIFEXITED(ret_status)) {
		if (WEXITSTATUS(ret_status) != 0) {
			//
			// SCMSGS
			// @explanation
			// Problem in the execution of the smf method.
			// @user_action
			// The waitpid which was waiting on the completion of
			// the process exited because of some problem.The
			// error will describe the reason.Try contact SUN
			// vendor for help.
			//
			logger.log(LOG_ERR, MESSAGE, "\n%s:error in method "
			"%s: %d", inst, method, WEXITSTATUS(ret_status));
			*method_exit = WEXITSTATUS(ret_status);
			if (method)
				delete(method);
			return (-1);
		}
	} else {
		if (WIFSIGNALED(ret_status))
			//
			// SCMSGS
			// @explanation
			// The process that was trying to execute the method
			// was terminated by a signal.
			// @user_action
			// Try to rexecute the method and see if the signal is
			// terminating the process.Try contact SUN vendor for
			// help.
			//
			logger.log(LOG_ERR, MESSAGE, "%s: method %s "
				"terminated by signal\n", inst, method);
		if (method)
			delete(method);
		*method_exit = WIFSIGNALED(ret_status);
		return (-1);
	}

	debug_print(NOGET("\n %s:Successfully ran method %s: %d"),
		inst, method, pthread_self());
	delete(method);
	return (0);
}

//
// Check if the service retries has reached its max
// Return true if max retries have been reached
//	  false if still retries possible
//
bool
service_task::is_retrying_expired()
{
	time_t	cur_time;
	std::list<time_t>::iterator time_iter;

	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	PTHREAD_MTX_LOCK(res_info->retry_cnt_mgmt.retry_cnt_lock);

	if (res_info->retry_cnt_mgmt.retry_counter >=
	res_info->svc_retry_count) {
		//
		// Retry count is expired.No more retries
		//
		PTHREAD_MTX_UNLOCK(res_info->retry_cnt_mgmt.retry_cnt_lock);
		return (true);
	}

	cur_time = time(NULL);
	//
	// Push the time into the queue
	//
	res_info->retry_cnt_mgmt.retry_timer_queue.push_back(cur_time);
	res_info->retry_cnt_mgmt.retry_counter++;

	//
	// Remove all the stale timers in the queue
	//
	time_iter = res_info->retry_cnt_mgmt.retry_timer_queue.begin();
	while ((cur_time - *time_iter) > res_info->svc_retry_intvl) {
		//
		// Remove the front entry in the queue
		//
		res_info->retry_cnt_mgmt.retry_timer_queue.remove(*time_iter);
		res_info->retry_cnt_mgmt.retry_counter--;
		time_iter = res_info->retry_cnt_mgmt.retry_timer_queue.begin();
	}
	PTHREAD_MTX_UNLOCK(res_info->retry_cnt_mgmt.retry_cnt_lock);
	return (false);
}

//
// Start the svc method
//
void
service_task::svc_start_event(char *name)
{
	int method_exit;
	int err;
	scha_err_t scha_err;
	char *res_name;
	char *rg_name;
	pthread_t res_failover_thr;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In service_task:svc_start_event"));
	//
	// Do only if current state is offline, else neglect
	//
	if (cur_inst_state == inst_states[DR_OFFLINE].smf_state) {
		//
		// Check if the service is been restarted
		//
		if (!is_svc_restart) {
			is_svc_restart = true;
		} else {
			//
			// Check if the services is been restarted till the
			// max available retries
			//
			res_name = res_info->get_res_name();
			rg_name = res_info->get_rg_name();
			if (is_retrying_expired()) {
				//
				// Signal the waiting Res start before
				// failing over
				//

				//
				// SCMSGS
				// @explanation
				// The number of restarts of services under
				// the resource has reached the max. So the
				// resource will be failing over
				// @user_action
				// No action neede.Just a notice meesage
				//
				logger.log(LOG_ERR, MESSAGE, "\n\n\nResource%s"
				" has reached the max retries of its services "
				"and will be failingover now", res_name);
				//
				// Failover
				//
				PTHREAD_MTX_LOCK(res_failover_lock);
				pthread_create(&res_failover_thr, NULL,
					res_failover, (void *)res_info);
				PTHREAD_MTX_UNLOCK(res_failover_lock);

				debug_print(NOGET("\n After res Giveover"));
				return;
			}

			//
			// Inform the RGM about the restart.
			//
			scha_err = scha_control(SCHA_RESOURCE_IS_RESTARTED,
						rg_name,
						res_name);
			if (scha_err != 0) {
				//
				// SCMSGS
				// @explanation
				// When a service is restarted, the sc
				// delegate restarter informs RGM about the
				// restart of the resource using
				// SCHA_RESOURCE_IS_RESTARTED scha control
				// API.This message is displayed when this
				// call cannot be made.
				// @user_action
				// Check for the validity and the states of
				// resource and services.Contact SUN vendor
				// for more help.But this should not be a
				// problem in normal operation of the
				// delegated restarter
				//
				logger.log(LOG_ERR, MESSAGE, "\nError "
				"in informing the res group about the "
					"svc restart:%s", rg_name);
				}
		}
		//
		// Execute the start method of the service
		//
		//
		// If resource is in starting state grab the lock and set the
		// failure success flag along with reducing the svc fmri start
		// completion count under this resource
		//
		if (run_method(name, SVC_EXEC_START, &method_exit) == 0) {
			update_state(name, inst_states[DR_ONLINE].smf_state,
				"none", RERR_NONE);
		} else {
			//
			// No need of grabbing lock becoz it does not make a
			// diff who sets it
			//

			if (res_info->cur_res_state == RS_STARTING) {
				res_info->is_svc_exec_failed = true;
			}

			update_state(name, inst_states[DR_MAINTENANCE].
				smf_state, "none", RERR_RESTART);
		}
	}
	//
	// reduce the svc completion count if the res is
	// starting
	//
	reduce_svc_count();

}

void
service_task::svc_stop_event(char *name)
{
	int method_exit;
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In service_task:svc_stop_event"));
	//
	// Get the latest state if the restarter had come up after
	// death. The restarter might have been disabled when the
	// service is online and re-enabled.
	//
	dr_intrnl_inst_state_type state =
		conv_cur_smf_to_internal_state();
	cur_inst_state = inst_states[state].smf_state;

	//
	// Do only if current inst state is online, else neglect
	//
	if (cur_inst_state == inst_states[DR_ONLINE].smf_state) {

		//
		// Execute the stop method of the service
		//
		if (run_method(name, SVC_EXEC_STOP, &method_exit) == 0) {
			//
			// If it is a SMF event update the state to Disable
			// it else if its contract update to offline
			//
			update_state(name,
			    inst_states[DR_OFFLINE].smf_state,
			    inst_states[DR_SC_FORCED_OFFLINE].name,
			    RERR_RESTART);
			//
			// reduce the svc completion count
			//
			// reduce_svc_count();
		} else {
			//
			// No need of grabbing lock becoz it does not make a
			// diff who sets it
			//
			if (res_info->cur_res_state == RS_STOPPING) {
				res_info->is_svc_exec_failed = true;
			}

			update_state(name, inst_states[DR_MAINTENANCE].
				smf_state, "none", RERR_RESTART);
		}
		//
		// After running the stop menthods empty the contract if
		// there are any hanging processes
		//
		if (svc_contract_info) {
			if (svc_contract_info->empty_contract() != 0) {
				debug_print(NOGET("\n Could not"\
				"empty the contract while stopping"));
				res_info->is_svc_exec_failed = true;
			}
		}
	}
	//
	// reduce the svc completion count
	//
	reduce_svc_count();
}

void
service_task::check_and_set_transient()
{
	int64_t prop_int_val;
	char *trans_val;
	int err = 0;
	rep_val_handling rep_trans_hdl;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	trans_val = rep_trans_hdl.get_method(svc_fmri, "startd", "duration",
					&prop_int_val, &err);
	if (err == 0) {
		if (trans_val != NULL) {
			if (strcmp(trans_val, "transient") == 0) {
				debug_print(NOGET("\nThe svc %s "
						"is transient"), svc_fmri);
				svc_is_transient = true;
			}
		}
	} else {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\n\n Transient: %d, %s",
			scf_error(), scf_strerror(scf_error()));
		if (scf_error() == SCF_ERROR_NOT_FOUND) {
			//
			// Transient prop is not defined,so reset the err
			//
		} else {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in reading the "
				"transient prop of svc: %s", svc_fmri);
		}
	}
}

void
service_task::execute_service()
{
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In service_task:execute"));


	//
	// Depending upon was the evnt from SMF or Contract
	//
	switch (svc_et_op_type) {
	case (SVC_ET_OP_SMF):
		debug_print(NOGET("\n Execute SMF Event"));

	switch (cur_res_event_type) {
	case (RESTARTER_EVENT_TYPE_ADD_INSTANCE):
		debug_print(NOGET("\n RESTARTER_EVENT_TYPE_ADD_INSTANCE"));
		if (cur_inst_state == RESTARTER_STATE_NONE) {
			update_state(svc_fmri,
				inst_states[DR_UNINITIALIZED].smf_state,
				"none", RERR_NONE);
			is_svc_managed = true;
		}
		break;
	case (RESTARTER_EVENT_TYPE_REMOVE_INSTANCE):
		debug_print(NOGET(
			"\n In RESTARTER_EVENT_TYPE_REMOVE_INSTANCE"));
		break;
	case (RESTARTER_EVENT_TYPE_ENABLE):
		//
		// If the enbale event is not from within this restarter then
		// its a no-op
		//
		debug_print(NOGET("\n In RESTARTER_EVENT_TYPE_ENABLE"));
		switch (cur_inst_state) {
		case RESTARTER_STATE_DISABLED:
		case RESTARTER_STATE_UNINIT:
			update_state(svc_fmri,
			    inst_states[DR_OFFLINE].smf_state,
			    inst_states[DR_SC_FORCED_OFFLINE].name,
			    RERR_RESTART);
				// inst_states[DR_SC_FORCED_OFFLINE].name);
				break;
		}
		break;
	case (RESTARTER_EVENT_TYPE_DISABLE):
		debug_print(NOGET("\n In RESTARTER_EVENT_TYPE_DISABLE"));
		switch (cur_inst_state) {
		case RESTARTER_STATE_UNINIT:
			update_state(svc_fmri,
			    inst_states[DR_DISABLED].smf_state,
			    "none", RERR_RESTART);
			break;
		case RESTARTER_STATE_OFFLINE:
			//
			// Dont call stop, because already in offline
			// state
			reduce_svc_count();
			break;
		default:
			//
			// For any other states, if disabled comes, then
			// it will be from manual interruption and outcome
			// is unknown
			//
			debug_print(NOGET("\n Disable is called.Will go ahead "
					"and call stop method"));
			svc_stop_event(svc_fmri);
			break;
		}
		break;
	case (RESTARTER_EVENT_TYPE_ADMIN_DISABLE):
		debug_print(NOGET("\n RESTARTER_EVENT_TYPE_ADMIN_DISABLE"));

		//
		// Admin disable is called. The outcome is unknown
		//

		//
		// SCMSGS
		// @explanation
		// This message comes when user does "svcadm disable". Since
		// manual "disabling" of a service is not encouraged once the
		// service
		// @user_action
		// Check for the validity and the states of resource and
		// services.Contact SUN vendor for more help.But this should
		// not be a problem in normal operation of the delegated
		// restarter
		//
		logger.log(LOG_NOTICE, MESSAGE, "\n Admin Disable is been "
			"called. The stop method of the service will be "
			"called and outcome "
			"is unknown.For proper functioning of the res, "
			"continue by disable the resource first");

		update_state(svc_fmri,
			inst_states[DR_DISABLED].smf_state, "none",
			RERR_RESTART);
		break;
	case (RESTARTER_EVENT_TYPE_ADMIN_DEGRADED):
		debug_print(NOGET("\n RESTARTER_EVENT_TYPE_ADMIN_DEGRADED"));
		break;
	case (RESTARTER_EVENT_TYPE_ADMIN_REFRESH):
		debug_print(NOGET("\n RESTARTER_EVENT_TYPE_ADMIN_REFRESH"));
		break;
	case (RESTARTER_EVENT_TYPE_ADMIN_RESTART):
		debug_print(NOGET("\n RESTARTER_EVENT_TYPE_ADMIN_RESTART"));
		break;
	case (RESTARTER_EVENT_TYPE_ADMIN_MAINT_OFF):
		debug_print(NOGET("\n RESTARTER_EVENT_TYPE_ADMIN_MAINT_OFF"));
		switch (cur_inst_state) {
		case RESTARTER_STATE_MAINT:
			update_state(svc_fmri,
			    inst_states[DR_OFFLINE].smf_state,
			    inst_states[DR_SC_FORCED_OFFLINE].name,
			    RERR_RESTART);
			break;
		}
		break;
	case (RESTARTER_EVENT_TYPE_ADMIN_MAINT_ON):
		debug_print(NOGET("RESTARTER_EVENT_TYPE_ADMIN_MAINT_ON"));

		switch (cur_inst_state) {
		case RESTARTER_STATE_OFFLINE:
		case RESTARTER_STATE_ONLINE:
		case RESTARTER_STATE_DISABLED:
			update_state(svc_fmri,
			inst_states[DR_MAINTENANCE].smf_state, "none",
				RERR_RESTART);
			break;
		}
		break;
	case (RESTARTER_EVENT_TYPE_ADMIN_MAINT_ON_IMMEDIATE):
		debug_print(NOGET(
			"\n RESTARTER_EVENT_TYPE_ADMIN_MAINT_ON_IMMEDIATE"));
		break;
	case (RESTARTER_EVENT_TYPE_INVALID_DEPENDENCY):
		debug_print(NOGET(
			"\n RESTARTER_EVENT_TYPE_INVALID_DEPENDENCY"));

		switch (cur_inst_state) {
		case RESTARTER_STATE_OFFLINE:
		case RESTARTER_STATE_ONLINE:
		case RESTARTER_STATE_DISABLED:
			update_state(svc_fmri,
			inst_states[DR_MAINTENANCE].smf_state, "none",
				RERR_RESTART);
			break;
		}
		break;
	case (RESTARTER_EVENT_TYPE_DEPENDENCY_CYCLE):
		debug_print(NOGET(
			"\n RESTARTER_EVENT_TYPE_INVALID_DEPENDENCY"));

		switch (cur_inst_state) {
		case RESTARTER_STATE_OFFLINE:
		case RESTARTER_STATE_ONLINE:
		case RESTARTER_STATE_DISABLED:
			update_state(svc_fmri,
			inst_states[DR_MAINTENANCE].smf_state, "none",
				RERR_RESTART);
			break;
		}
		break;
	case (RESTARTER_EVENT_TYPE_STOP):
		debug_print(NOGET("\n In RESTARTER_EVENT_TYPE_STOP"));
		//
		// Call only if the resource state is online, else the stop
		// method would already have been called by the RCT_STOP
		//
		if (res_info->cur_res_state == RS_ONLINE) {
			svc_stop_event(svc_fmri);
		}
		break;
	case (RESTARTER_EVENT_TYPE_START):
		debug_print(NOGET("\n In RESTARTER_EVENT_TYPE_START"));
		//
		// Start the service only if it was from internal request.
		// and only during the time frame when the res is either
		// starting or already online
		//
		if (((res_info->cur_res_state == RS_ONLINE) ||
			(res_info->cur_res_state == RS_STARTING)) &&
			!(res_info->is_res_stop_called)) {
			svc_start_event(svc_fmri);
		}
		break;
	case (RESTARTER_EVENT_TYPE_INVALID):
		debug_print(NOGET("\n RESTARTER_EVENT_TYPE_INVALID"));
	default:
		debug_print(NOGET("\n Unknown Event Type"));
		break;
	}
	break;
	//
	// Contract specific work if any
	//
	case (SVC_ET_OP_CONTRACT):
		debug_print(NOGET("\nExecute Contract specific:%d"),
			pthread_self());
		//
		// Call stop which in turn sends the start event only if the
		// resource state is online
		//
		if (res_info->cur_res_state == RS_ONLINE) {
			svc_stop_event(svc_fmri);
		}
		break;
	//
	// Resource specific work if any
	//
	case (SVC_ET_OP_RES):
		debug_print(NOGET("\nExecute Res specific:%d"),
			res_method_type);
		if (res_method_type == RCT_START) {
			//
			// If res stop is called, just dont execute it since
			// anyway stop follows
			//
			if (res_info->is_res_stop_called) {
				//
				// reduce the svc completion count
				//
				reduce_svc_count();
			} else {
				//
				// If it is a transient service, cet the
				// svc_is_transient flag
				//
				check_and_set_transient();
				//
				// Since already enable is called but start
				// is been blocked waiting for res start,
				// now setting it to offline will call start
				// event again
				//
				update_state(svc_fmri,
				    inst_states[DR_OFFLINE].smf_state,
				    inst_states[DR_SC_FORCED_OFFLINE].name,
				    RERR_RESTART);
			}
		} else if (res_method_type == RCT_STOP) {
			svc_stop_event(svc_fmri);
		}
		break;
	}
}


restarter_instance_state_t
service_task::get_svc_cur_state()
{
	return (cur_inst_state);
}


svc_event_task::svc_event_task()
{
}

//
// Set the parameters passed from outside the threadpool required for the
// execution of the service task and execute the service_task
//
void
svc_event_task::execute()
{

	svc_ptr->svc_et_op_type = cur_et_op_type;

	switch (cur_et_op_type) {
	case SVC_ET_OP_SMF:
		//
		// Set the current smf event type
		//
		svc_ptr->cur_res_event_type = cur_smf_event_type;
		break;

	case SVC_ET_OP_RES:
		//
		// Set the res method call type
		//
		svc_ptr->res_method_type = set_res_method_type;
		break;

	case SVC_ET_OP_CONTRACT:
		break;
	}

	svc_ptr->execute_service();
}
