/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RES_SVC_BASIC_INFO_H
#define	_RES_SVC_BASIC_INFO_H

#pragma ident	"@(#)res_svc_basic_info.h	1.8	08/05/20 SMI"

#include "sc_delegated_restarter.h"
#include "resource_mgmt.h"
#include "service_mgmt.h"

using namespace std;
class resource_info;


//
// Basic resource management on any given resource
//

class res_svc_basic_info {
private:
	bool is_res_svcs_offline(char *res_name);
	bool is_res_svcs_managed(char *svc_fmri_file);
	bool is_this_svc_managed(char *fmri);
public:
	res_svc_basic_info();
	int process_method(char *res_name, char *res_grp_name,
			res_call_type mthd_type, char *svc_fmri_file = NULL,
			int retry_cnt = 0, int retry_interval = 0);
//	int process_method(char * fmri);
	char *get_svc_res(char *fmri);
	resource_info* get_resource_info(char *res_name);
};

#endif /* _RES_SVC_BASIC_INFO_H */
