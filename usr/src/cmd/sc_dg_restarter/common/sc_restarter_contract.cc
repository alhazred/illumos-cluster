/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sc_restarter_contract.cc	1.8	08/05/20 SMI"

#include "sc_delegated_restarter.h"
#include "sc_restarter_contract.h"
#include "repository_val.h"
#include "resource_mgmt.h"
#include <fcntl.h>
#include <fstream.h>
#include "sc_dg_error.h"

sc_restarter_contract *sc_restarter_contract::manage_contract = NULL;
pthread_mutex_t
sc_restarter_contract::contract_create_lock = PTHREAD_MUTEX_INITIALIZER;
int sc_restarter_contract::active_tmpl_fd = -1;

std::map<ctid_t, contract_info *> contract_info::contract_info_hash_tbl;

sc_restarter_contract&
sc_restarter_contract::instance()
{
	if (manage_contract == NULL) {
		create();
	}
	return (*manage_contract);
}

void
sc_restarter_contract::create()
{
	//
	// To make sure the first thread will create and no race
	// conditions
	//
	pthread_mutex_lock(&contract_create_lock);
	if (manage_contract == NULL) {
		manage_contract = new sc_restarter_contract();
	}
	pthread_mutex_unlock(&contract_create_lock);
}

sc_restarter_contract::sc_restarter_contract()
{
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	if (contract_init() < 0) {
		//
		// SCMSGS
		// @explanation
		// Not able to initialize the contract for contract event
		// delivery
		// @user_action
		// The previous messages should explain the reason.Make sure
		// the basic contract functionality is working fine?Contact
		// SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Contract Initialization "
			"Failed");
		exit(0);
	}
}

// Note: Some of the contract code below is been taken from inted, pmf
// and modified

//
// Creates and configures the contract template used for
// sc_delegated_restarter methods.
// Returns -1 on error, else the fd of the created template.
//
static int
sc_restarter_contract::create_contract_template(void)
{
	int		fd;
	int		err;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	if ((fd = open64(CONTRACT_TEMPLATE_PATH, O_RDWR)) == -1) {
	//
	// SCMSGS
	// @explanation
	// Need explanation of this message!
	// @user_action
	// Need a user action for this message.
	//
	logger.log(LOG_ERR, MESSAGE, "\n Failed to open contract file %s: "
		"%s", CONTRACT_TEMPLATE_PATH, strerror(errno));
		return (-1);
	}


	//
	// Make contract inheritable and make hardware errors fatal.
	// Also limit the scope of fatal events to the process
	// group. Services run in a single process group.
	//
	if (((err = ct_pr_tmpl_set_param(fd,
	    CT_PR_INHERIT|CT_PR_PGRPONLY)) != 0) ||
	    ((err = ct_pr_tmpl_set_fatal(fd, CT_PR_EV_HWERR)) != 0) ||
	    ((err = ct_tmpl_set_critical(fd, CT_PR_EV_EMPTY)) != 0) ||
	    ((err = ct_tmpl_set_informative(fd, 0)) != 0)) {
		//
		// SCMSGS
		// @explanation
		// Cannot set the various parameters for the contract
		// @user_action
		// Make sure the basic contract functionality is working
		// fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Failed to set parameter "
			"for contract template: %s", strerror(err));
		(void) close(fd);
		return (-1);
	}

	return (fd);
}

//
// Initialize the contract by creating a prcoess contract
// Returns -1 on error, else 0.
//
static int
sc_restarter_contract::contract_init(void)
{
	int res;
	pthread_t	event_listen_thr;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	if ((active_tmpl_fd = create_contract_template()) == -1) {
		//
		// SCMSGS
		// @explanation
		// The previous messages will tell the reason for the failure.
		// @user_action
		// Make sure the basic contract functionality is working
		// fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Failed to create contract "
			"template");
		return (-1);
	}
	if ((res = pthread_create(&event_listen_thr, NULL,
				contract_event_listener, NULL)) != 0) {
		//
		// SCMSGS
		// @explanation
		// Not able to start the thread to listen for events
		// @user_action
		// The error code will tell the reason for not being able to
		// start the thread. Check the pthread man pages for more
		// info. Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Failed to create the thread"
			" event_listen_thr: %d", res);
	}
	return (0);
}

static void
sc_restarter_contract::contract_fini(void)
{
	if (active_tmpl_fd != -1) {
		(void) close(active_tmpl_fd);
		active_tmpl_fd = -1;
	}
}

//
// To be called directly before a service method is forked, this function
// results in the method process being in a new contract based on the active
// contract template.
//
static void
sc_restarter_contract::contract_prefork(void)
{
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	if ((err = ct_tmpl_activate(active_tmpl_fd)) != 0) {
		//
		// SCMSGS
		// @explanation
		// Not able to activate the contract template.
		// @user_action
		// Check the contract manpage to know more about the
		// error.Also make sure the basic contract functionalities are
		// working fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Failed to activate contract"
			" template: %s", strerror(err));
		exit(0);
	}
}

//
// To be called in both processes directly after a service method is forked,
// this function results in switching off contract creation for any
// forks done by either process, unless contract_prefork() is called beforehand
//
static void
sc_restarter_contract::contract_postfork(void)
{
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	if ((err = ct_tmpl_clear(active_tmpl_fd)) != 0)
		//
		// SCMSGS
		// @explanation
		// Not able to clear the active contract template.
		// @user_action
		// Check the contract manpage to know more about the
		// error.Also make sure the basic contract functionalities are
		// working fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Failed to clear active " \
		"contract template: %s", strerror(err));
}

static void *
sc_restarter_contract::contract_event_listener(void *)
{
	int err;
	ct_evthdl_t evt_hdl;
	ctevid_t ev_id;
	int method_exit;
	char ct_ctl_file_path[CT_PATH_MAX];
	int fd;
	std::map<ctid_t, contract_info *>::const_iterator it_contract_info;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	//
	// Get the fd for event endpoint to listen to the events
	//
	int event_fd = open64(CONTRACT_PBUNDLE_PATH, O_RDONLY);
	if (event_fd == -1) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// Cannot open the contract file for event listening
		// @user_action
		// Check the contract manpage to know more about the
		// error.Also make sure the basic contract functionalities are
		// working fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Failed to open the pbundle "
			"event listener FD: %s", strerror(err));
		exit(err);
	}

	//
	// Reset the pbundle queue so that the previous un ackowledged events
	// will be sent
	//
	if ((ct_event_reset(event_fd)) != 0) {
		debug_print(NOGET("\n Contract Event reset error"));
	}

	//
	// Listen for events.Ack the events after reading
	//
	while (true) {
		if ((err = ct_event_read(event_fd, &evt_hdl)) != 0) {
			//
			// SCMSGS
			// @explanation
			// Not able to read the event from the file descriptor
			// obtained from the pbundle.
			// @user_action
			// Check the contract manpage to know more about the
			// error.Also make sure the basic contract
			// functionalities are working fine.Contact SUN vendor
			// for more help.
			//
			logger.log(LOG_ERR, MESSAGE, "\n ct_event_read "
				"error'd out: %d", err);
		} else {
			ctid_t ct_id = ct_event_get_ctid(evt_hdl);

			debug_print(NOGET("\n Got the contract event %d"),
				    ct_id);
			//
			// Acknowledge the contract
			//
			sprintf(ct_ctl_file_path,
				"/system/contract/process/%d/ctl", ct_id);

			if ((fd = open64(ct_ctl_file_path, O_WRONLY)) == -1) {
				//
				// SCMSGS
				// @explanation
				// Not able to open the contract control file
				// to acknowledge the event
				// @user_action
				// Check the contract manpage to know more
				// about the error.Also make sure the basic
				// contract functionalities are working
				// fine.Contact SUN vendor for more help.This
				// should not seriously effect the working of
				// delegated restarter.
				//
				logger.log(LOG_ERR, MESSAGE, "\n Failed to"
				" open contract ctl file to ack%s: %s",
					ct_ctl_file_path, strerror(errno));
			}
			ev_id = ct_event_get_evid(evt_hdl);
			if (ct_ctl_ack(fd, ev_id) != 0) {
				//
				// SCMSGS
				// @explanation
				// Not able to clear the active contract
				// template.
				// @user_action
				// Check the contract manpage to know more
				// about the error.Also make sure the basic
				// contract functionalities are working
				// fine.Contact SUN vendor for more help.
				// Previous messages will give the reason
				//
				logger.log(LOG_ERR, MESSAGE, "\n Error in"
					"contract event acknowledging");
			}



			//
			// Get the service instance corresponding to this
			// contract id.For that happen, first get this
			// contract id from the contract hash table and then
			// get the pointer of the corresponding service_task
			// from that contract object
			//
			it_contract_info = contract_info::
				contract_info_hash_tbl.find(ct_id);

			//
			// Confirm that the returned key from the contract
			// hash table is a valid one
			//
			if (it_contract_info == contract_info::
			    contract_info_hash_tbl.end()) {
				debug_print(NOGET("\nThe contract"\
					"%d is not present in the contract"
					" map table"), ct_id);

				//
				// Abandon the contract.This contract is either
				// created by the stop method or some other
				// which dg restarter which is not interested
				// in
				//
				sprintf(ct_ctl_file_path,
				"/system/contract/process/%d/ctl", ct_id);

				if ((fd = open64(ct_ctl_file_path, O_WRONLY))
				    == -1) {
					//
					// SCMSGS
					// @explanation
					// Not able to open the contract
					// control file.
					// @user_action
					// Check the contract manpage to know
					// more about the error.Also make sure
					// the basic contract functionalities
					// are working fine.Contact SUN vendor
					// for more help.
					//
					logger.log(LOG_ERR, MESSAGE, "\n "
					"Failed to open contract ctl"
					"file %s: %s", ct_ctl_file_path,
						strerror(errno));
				}
				if (ct_ctl_abandon(fd) != 0) {
					//
					// SCMSGS
					// @explanation
					// Cannot abandon the displayed
					// contract.
					// @user_action
					// Check the contract manpage to know
					// more about the error.Also make sure
					// the basic contract functionalities
					// are working fine.Contact SUN vendor
					// for more help.This will not trouble
					// the functioning of the delegated
					// restarter, can continue working.
					//
					logger.log(LOG_ERR, MESSAGE, "\nError"
					" in abandoning Contract:%d", ct_id);
				}
				continue;
			}
			//
			// If we got the contract id info from Hash Table
			// do following
			//
			service_task *ptr_svc_task;
			ptr_svc_task = it_contract_info->second->svc_inst;

			//
			// Create a new service event task object and put it
			// into the threadpool queue of ptr_svc_task for
			// further execution
			//
			svc_event_task *svc_ev_task = new svc_event_task();

			//
			// Set the svc event operation type to contract
			//
			svc_ev_task->cur_et_op_type = SVC_ET_OP_CONTRACT;


			//
			// Set the service task
			//
			svc_ev_task->svc_ptr = ptr_svc_task;

			//
			// Put the service fmri object into the thread queue
			// which takes the necessary action on that svc fmri
			// in the excute() method of that service object
			//
			PTHREAD_MTX_LOCK(
				ptr_svc_task->svc_fmri_thread_pool_lock);
			ptr_svc_task->svc_fmri_thread_pool.
				defer_processing(svc_ev_task);
			PTHREAD_MTX_UNLOCK(
				ptr_svc_task->svc_fmri_thread_pool_lock);

		}
		//
		// Free the handle
		//
		ct_event_free(evt_hdl);
	}
	return (NULL);
}

//
// Retrun through err 0 on success and -1 on error
//
contract_info::contract_info(service_task *svc_ptr, int *err)
{
	svc_inst = svc_ptr;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
	if (resource_info::res_dg_init_flag) {
		//
		// Set the contract id from repository.
		//
		int64_t contr_id;
		rep_val_handling rep_contract_hdl;
		rep_contract_hdl.get_method(svc_inst->svc_fmri,
					SC_DELEGATED_PG,
					SVC_CONTRACT_ID_PROP,
					&contr_id, err);
		if (*err != 0) {
			//
			// SCMSGS
			// @explanation
			// Cannot read the contract id from SMF repository
			// @user_action
			// The previous messages should give more details for
			// the reason.Make sure the basic SMF functionalities
			// are working fine.Contact SUN vendor for more help.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in getting the"
				" contract id from repository for svc:%s",
				svc_inst->svc_fmri);
			return;
		}
		contract_id = contr_id;
		//
		// Insert the contract info into the contract hash table
		//
		typedef pair<ctid_t, contract_info *> contract_addr_pair;
		contract_info::contract_info_hash_tbl.insert(
			contract_addr_pair(contract_id, this));

		//
		// Adopt it
		//
		*err = adopt_contract();
		if (*err != 0) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Could not adopt the "
				" contract id: %d of inst %s", contract_id,
				svc_inst->svc_fmri);
		}
	}
}

//
// Send signal sig all the processes with contract id ctid
//
int
contract_info::contract_send_sig(int sig)
{
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
	if (sigsend(P_CTID, contract_id, sig) == -1 && errno != ESRCH) {
		//
		// SCMSGS
		// @explanation
		// During the stopping of the resource, could not clear all
		// the contracts that belong to the services under that
		// resource.
		// @user_action
		// This should not cause much problem.Contact SUN vendor for
		// more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n %s: Could not signal all"
			" contract members: %s\n", svc_inst->svc_fmri,
			strerror(errno));
		return (-1);
	}

	return (0);
}

//
// Return 0 on Success or -1 on error
//
int
contract_info::adopt_contract()
{
	char	cid_buf[20];
	char ct_ctl_file_path[CT_PATH_MAX];
	int fd;
	int cid;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	if (contract_id <= 0)
		return (-1);

	sprintf(ct_ctl_file_path, "/system/contract/process/%d/ctl",
		contract_id);

	if ((fd = open64(ct_ctl_file_path, O_WRONLY)) == -1) {
		//
		// SCMSGS
		// @explanation
		// Cannot open the contract control file.
		// @user_action
		// Check the contract manpage to know more about the
		// error.Also make sure the basic contract functionalities are
		// working fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Failed to open contract ctl"
			" file %s: %s", ct_ctl_file_path, strerror(errno));
		return (-1);
	}
	if (ct_ctl_adopt(fd) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in adopting contract");
		return (-1);
	} else {
		debug_print(NOGET("\n Success in adopting contract"));
	}

	debug_print(NOGET("\n COntract Adopt Done"));
	return (0);
}

static int
close_on_exec(int fd)
{
	int flags = fcntl(fd, F_GETFD, 0);
	if ((flags != -1) && (fcntl(fd, F_SETFD, flags | FD_CLOEXEC) != -1))
		return (0);
	return (-1);
}

int
contract_open(ctid_t ctid, const char *type, const char *file, int oflag)
{
	char path[CT_PATH_MAX];
	int n, fd;

	assert((oflag & O_CREAT) == 0);

	if (type == NULL)
		type = "all";

	n = snprintf(path, CT_PATH_MAX, CTFS_ROOT "/%s/%ld/%s", type, ctid,
		file);
	if (n >= CT_PATH_MAX) {
		return (-1);
	}

	fd = open64(path, oflag);
	if (fd != -1) {
		if (close_on_exec(fd) == -1) {
			int err = errno;
			(void) close(fd);
			errno = err;
			return (-1);
		}
	}
	return (fd);
}


//
// Kill all the process in the contract.Abandon the contract and make other
// clean ups
//

int
contract_info::empty_contract()
{
	ct_stathdl_t	status_hdl;
	char		ct_str[10];
	int 		status_fd;
	pid_t		*pid_arr_ptr;
	int		num_pids;
	int		sig_ret;
	char 		ct_ctl_file_path[CT_PATH_MAX];
	int 		fd;
	char ct_status_file_path[100];
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	//
	// Send signal to all processes under the contract
	//
	for (; ; ) {
		sig_ret = sigsend(P_CTID, contract_id, SIGKILL);
		if (sig_ret != 0) {
			if (errno == ESRCH) {
				//
				// No more processes so break
				//
				break;
			} else {
				//
				// SCMSGS
				// @explanation
				// Could not send the signal to the specified
				// contract
				// @user_action
				// Man pages for sigsend will give more info
				// about it.Contact SUN vendor for more help.
				//
				logger.log(LOG_ERR, MESSAGE, "\nSigsend failed"
					" to send sig to contract %d: %s",
					contract_id, strerror(errno));
				return (-1);
			}
		}
	}

	//
	// Abandon the contract
	//
	sprintf(ct_ctl_file_path, "/system/contract/process/%d/ctl",
		contract_id);

	if ((fd = open64(ct_ctl_file_path, O_WRONLY)) == -1) {
		logger.log(LOG_ERR, MESSAGE, "\n Failed to open contract ctl "
			"file %s: %s", ct_ctl_file_path, strerror(errno));
		// The contract is already been deleted and it does not
		// exist anymore
		goto cleanup;
	}
	if (ct_ctl_abandon(fd) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in abandoning "
		"contract: %d", contract_id);
	}

cleanup:
	//
	// Set the contract id to 0 and update it in the respository
	//
	contract_id = 0;
	rep_val_handling rep_ctr_put_hdl;
	if ((rep_ctr_put_hdl.put_rep_vals(
		svc_inst->svc_fmri, contract_id,
		SC_DELEGATED_PG, SVC_CONTRACT_ID_PROP)) != 0) {
		//
		// SCMSGS
		// @explanation
		// Not able to set the desired values in the SMF repository.
		// @user_action
		// Check the SMF manpage to know more about the error.Also
		// make sure the basic SMF functionalities are working
		// fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Could not set the contract id"
			" to 0 in the repository");
	}

	//
	// Remove the contract id from the hash table
	//
	contract_info_hash_tbl.erase(contract_id);

	return (0);
}
