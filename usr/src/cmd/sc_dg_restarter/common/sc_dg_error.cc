//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
//
// This code is been copied from pmf
//

#pragma ident	"@(#)sc_dg_error.cc	1.5	08/05/20 SMI"

#include "sc_dg_error.h"

#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <syslog.h>
#include <pthread.h>
#include <sys/syslog.h>
#include <sys/sc_syslog_msg.h>

#ifdef DEBUG
static int debug = 1;
#else
static int debug = 0;
// static int debug = 0;
#endif


#define	SC_DG_DBG_BUFSIZE (64 * 1024) // 64KB ring buffer for tracing

dbg_print_buf sc_dg_dbg_buf(SC_DG_DBG_BUFSIZE); // ring buffer for tracing

#define	SC_DG_BUFSIZ	256

//
// Routine for tracing/logging debug info.
// Tracing is always on. Logging (into syslog and the console) is turned on
// only if Debugflag is turned on.
//
void
debug_print(char *fmt, ...)
{
	/*lint -e534 */
	if (debug) {
		va_list args;
		char str1[SC_DG_BUFSIZ];
		va_start(args, fmt);	//lint !e40
		if (vsnprintf(str1, SC_DG_BUFSIZ, fmt, args) >= SC_DG_BUFSIZ) {
			str1[SC_DG_BUFSIZ - 1] = '\0';
		}
		va_end(args);
		os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
		logger.log(LOG_NOTICE, MESSAGE, str1);
	}
}
