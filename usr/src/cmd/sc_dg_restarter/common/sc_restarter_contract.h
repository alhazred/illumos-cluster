/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SC_RESTARTER_CONTRACT_H
#define	_SC_RESTARTER_CONTRACT_H

#pragma ident	"@(#)sc_restarter_contract.h	1.4	08/05/20 SMI"

#include <sys/ctfs.h>
#include <sys/contract/process.h>
#include <libcontract.h>
#include <fcntl.h>
#include <map>
#include <string>
#include <sstream>

#include "service_mgmt.h"

/* paths/filenames of contract related files */
#define	CONTRACT_ROOT_PATH	CTFS_ROOT "/process/"
#define	CONTRACT_TEMPLATE_PATH  CONTRACT_ROOT_PATH "template"
#define	CONTRACT_LATEST_PATH CONTRACT_ROOT_PATH "latest"
#define	CONTRACT_PBUNDLE_PATH "/system/contract/process/pbundle"

#define	CT_PATH_MAX 100

class service_task;
//
// Manage the contracts of the sc_delegated_restarter
// This follows the singleton pattern
// Idea was taken and understood from the pmf code and other documents
// on singleton
//
class sc_restarter_contract {
public:
	static 	sc_restarter_contract &instance();
	static 	void create();
	static	int create_contract_template(void);
	static 	int contract_init(void);
	static  void contract_fini(void);
	static 	void contract_prefork(void);
	static 	void contract_postfork(void);
	static void *contract_event_listener(void *);
	static int active_tmpl_fd;

	static sc_restarter_contract	*manage_contract;
	static pthread_mutex_t		contract_create_lock;

	sc_restarter_contract();


};

//
// Detailed information of an individual contract
//
class contract_info {
private:

public:
	ctid_t contract_id;

	//
	// Pointer to the service instance this contract belongs to
	//
	service_task *svc_inst;

	// Hash table to keep contract_id as the key and its corresponding
	// object address as the data
	static std::map<ctid_t, contract_info *> contract_info_hash_tbl;

	//
	// Send signal sig to all the processes under this contract
	//
	int contract_send_sig(int sig);

	contract_info(service_task *svc_ptr, int *err);

	//
	// Read any pending contracts of previous sc_delegated restarter
	// and adopt them
	//
	int adopt_contract();

	//
	// Clear the contract
	//
	int empty_contract();
};

#endif /* _SC_RESTARTER_CONTRACT_H */
