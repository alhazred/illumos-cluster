/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)repository_val.cc	1.6	08/05/20 SMI"

#include "repository_val.h"
#include "sc_delegated_restarter.h"
#include "sc_dg_error.h"

#define	MAX_METHOD_LEN 512

rep_val_handling::rep_val_handling()
{
}

void
rep_val_handling::rep_hdl_init()
{
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
	int retry_count = 0;

	init_status = 0;
	rep_handle = NULL;
	pg = NULL;
	inst = NULL;
	trans = NULL;
	entry = NULL;
	prop = NULL;

	//
	// Create and bind the handle for the repository
	//
	if ((rep_handle = scf_handle_create(SCF_VERSION)) == NULL) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Could not create the "
		"repository handle: %s", scf_strerror(scf_error()));
		init_status = -1;
		return;
	}

	for (retry_count = 0; retry_count <= REP_HDL_BIND_RETRIES;
		retry_count++) {
			if ((scf_handle_bind(rep_handle) == 0) ||
			    (scf_error() == SCF_ERROR_IN_USE)) {
				break;
			}
		}

	if (retry_count >= REP_HDL_BIND_RETRIES) {
		//
		// SCMSGS
		// @explanation
		// Error in creating the handle to bind with the SMF
		// repository.
		// @user_action
		// Check the SMF manpage to know more about the error.Also
		// make sure the basic SMF functionalities are working
		// fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in binding the "
		"handle:%s", scf_strerror(scf_error()));
		scf_handle_destroy(rep_handle);
		rep_handle = NULL;
		init_status = -1;
		return;
	}

	if (((pg = scf_pg_create(rep_handle)) == NULL) ||
	    ((inst = scf_instance_create(rep_handle)) == NULL)||
	    ((trans = scf_transaction_create(rep_handle)) == NULL) ||
	    ((entry = scf_entry_create(rep_handle)) == NULL) ||
	    ((prop = scf_property_create(rep_handle)) == NULL)) {
		//
		// SCMSGS
		// @explanation
		// Error in creating various handles to interact with the SMF
		// repository.
		// @user_action
		// Check the SMF manpage to know more about the error.Also
		// make sure the basic SMF functionalities are working
		// fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Failed to create repository"
			" object %s", scf_strerror(scf_error()));
		close_rep_interaction();
		init_status = -1;
	}

}

rep_val_handling::~rep_val_handling()
{
}

void
rep_val_handling::close_rep_interaction()
{
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
	debug_print(NOGET("\n Destructing rep_hdl of svc"));

	if (rep_handle != NULL) {

		scf_handle_unbind(rep_handle);

		scf_pg_destroy(pg);
		pg = NULL;
		scf_instance_destroy(inst);
		inst = NULL;
		scf_transaction_destroy(trans);
		trans = NULL;
		scf_entry_destroy(entry);
		entry = NULL;
		scf_property_destroy(prop);
		prop = NULL;

		scf_handle_destroy(rep_handle);
		rep_handle = NULL;
	}

}

//
// Returns 0 on success else a scf error is returned
// SCF_ERROR_NO_MEMORY if memory allocation failed.
// SCF_ERROR_NO_RESOURCES if the server doesn't have required resources.
// SCF_ERROR_VERSION_MISMATCH if program compiled against a newer libscf
// than on system.
// SCF_ERROR_PERMISSION_DENIED if insufficient privileges to modify pg.
// SCF_ERROR_BACKEND_ACCESS if the repository back-end refused the pg modify.
// SCF_ERROR_CONNECTION_BROKEN if the connection to the repository was broken.
//
int
rep_val_handling::put_rep_vals(const char *svcfmri, int val,
			const char *pg_name,
			const char *prop_name)
{

	scf_value_t	*scf_val = NULL;
	int		ret;
	int		cret;
	int retry_count;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	rep_hdl_init();
	if (init_status != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Repository handles is "
			"not being initialized");
	}

	if (scf_handle_decode_fmri(rep_handle, svcfmri, NULL, NULL, inst,
				NULL, NULL, SCF_DECODE_FMRI_EXACT) == -1) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\nError in fmri handle decode:%s"
			", FMRI: %s", svcfmri, scf_strerror(scf_error()));
		close_rep_interaction();
		return (scf_error());
	}
	//
	// Get the instance state pg, and if it doesn't exist try and
	// create it.
	//
	if (scf_instance_get_pg(inst, pg_name, pg) < 0) {
		if (scf_error() != SCF_ERROR_NOT_FOUND) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\nError scf_instance_get_pg:%s",
				scf_strerror(scf_error()));
		close_rep_interaction();
			return (scf_error());
		}
		if (scf_instance_add_pg(inst, pg_name,
				SCF_GROUP_METHOD, 0, pg) < 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\nError scf_instance_add_pg:%s",
				scf_strerror(scf_error()));
		close_rep_interaction();
			return (scf_error());
		}
	}

	//
	// Perform a transaction to write the values to the requested property.
	// If someone is already using it, loop and retry.
	//
	scf_transaction_reset(trans);
	scf_entry_reset(entry);
	do {
		if (scf_transaction_start(trans, pg) < 0) {
			ret = scf_error();
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in Transaction"
			"start :%d, %s", ret, scf_strerror(scf_error()));

			goto cleanup;
		}

		if ((scf_transaction_property_new(trans, entry,
		    prop_name, SCF_TYPE_INTEGER) < 0) &&
		    (scf_transaction_property_change_type(trans, entry,
		    prop_name, SCF_TYPE_INTEGER) < 0)) {
			ret = scf_error();
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in Transaction"
			"property:%d, %s", ret, scf_strerror(scf_error()));
			goto cleanup;
		}

		//
		// Add value to the transaction
		//
		if ((scf_val = scf_value_create(rep_handle)) == NULL) {
			ret = scf_error();
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in Value Create"
				":%d, %s", ret, scf_strerror(scf_error()));

			goto cleanup;
		}
		scf_value_set_integer(scf_val, val);

		if (scf_entry_add_value(entry, scf_val) < 0) {
			ret = scf_error();
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in Value add"
				":%d, %s", ret, scf_strerror(scf_error()));

			goto cleanup;
		}

		if ((cret = scf_transaction_commit(trans)) < 0) {
			ret = scf_error();
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in Transcation"
			" Commit:%d, %s", ret, scf_strerror(scf_error()));

			goto cleanup;
		} else if (cret == 0) {
			scf_transaction_reset(trans);
			scf_entry_reset(entry);
			scf_value_destroy(scf_val);
			if (scf_pg_update(pg) < 0) {
				ret = scf_error();
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in PG Update"
				" :%d, %s", ret, scf_strerror(scf_error()));

			close_rep_interaction();
			return (ret);
			}
		}
	} while (cret == 0);

	ret = 0;
cleanup:
	scf_transaction_reset(trans);
	scf_entry_reset(entry);
	if (scf_val) {
		scf_value_destroy(scf_val);
	}
	close_rep_interaction();
	return (ret);
}


//
// When time permits implement this function with a template
//
// If the property type is astring, returns the string value, else NULL
// If the prop val is count or integer, return the value through int_val
// and error through err which is 0 on success else -1
//
char *
rep_val_handling::get_method(const char *svcfmri, const char *pg_name,
			const char *prop_name,
			int64_t *int_val, int *err)
{
	scf_simple_prop_t	*sprop;
	char			*scf_str;
	char			*method_str;
	int64_t			*i_val;
	int ret;

	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	*err = 0;
	debug_print(NOGET("\n rep_val_handling::get_method"));

	//
	// Get the method from the property of the instance
	//
	sprop = scf_simple_prop_get(NULL, svcfmri, pg_name, prop_name);

	if (sprop == NULL) {
		//
		// SCMSGS
		// @explanation
		// There is no property in the service or there was an error
		// in reading the property.
		// @user_action
		// Check the SMF manpage to know more about the error.Also
		// make sure the basic SMF functionalities are working
		// fine.Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n %s->In scf_simple_prop_get:"
		"%s, method: %s\n", svcfmri, scf_strerror(scf_error()),
			pg_name);
		*err = -1;
		return (NULL);
	} else {
		switch (scf_simple_prop_type(sprop)) {
		case SCF_TYPE_ASTRING:

			method_str = new char[MAX_METHOD_LEN];

			if (method_str == NULL) {
				//
				// SCMSGS
				// @explanation
				// Could not allocate memory
				// @user_action
				// Check the availabilty of the memory.Contact
				// SUN vendor for more help.
				//
				logger.log(LOG_ERR, MESSAGE, "\n Could not "
					"allocate memory in get_method");
				*err = -1;
				return (NULL);
			}

			while ((scf_str = scf_simple_prop_next_astring(sprop))
				!= NULL) {
					sprintf(method_str, "%s", scf_str);
				}
			if (scf_error() != SCF_ERROR_NONE) {
				//
				// An error occurred during reading the
				// property
				//

				//
				// SCMSGS
				// @explanation
				// There was an error in reading the property
				// of the SMF service instance from the
				// repository
				// @user_action
				// Check the SMF manpage to know more about
				// the error.Also make sure the basic SMF
				// functionalities are working fine.Contact
				// SUN vendor for more help.
				//
				logger.log(LOG_ERR, MESSAGE, "\n Error during "
					"reading the property from "
					"repository %s",
					scf_strerror(scf_error()));
				scf_simple_prop_free(sprop);
				*err = -1;
				return (NULL);
			}
			scf_simple_prop_free(sprop);
			return (method_str);
			break;
		case SCF_TYPE_COUNT:
			//
			// Here have to incorporate the logic if more than one
			// values exist in the property to concat all values.
			// As of now assume one val
			//
			while ((i_val = (int64_t *)
				scf_simple_prop_next_count(sprop)) != NULL) {
					*int_val = *i_val;
				}
			if (scf_error() != SCF_ERROR_NONE) {
				//
				// An error occurred during reading the
				// property
				//
				logger.log(LOG_ERR, MESSAGE, "\n Error during "
					"reading the property from "
					"repository %s",
					scf_strerror(scf_error()));
				*err = -1;
			}
			scf_simple_prop_free(sprop);
			return (NULL);
			break;

		case SCF_TYPE_INTEGER:
			//
			// Here have to incorporate the logic if more than one
			// values exist in the property to concat all values.
			// As of now assume one val
			//
			while ((i_val = scf_simple_prop_next_integer(sprop))
				!= NULL) {
					*int_val = *i_val;
				}
			if (scf_error() != SCF_ERROR_NONE) {
				//
				// An error occurred during reading the
				// property
				//
				logger.log(LOG_ERR, MESSAGE, "\n Error during "
					"reading the property from "
					"repository %s",
					scf_strerror(scf_error()));
				*err = -1;
			}
			scf_simple_prop_free(sprop);
			return (NULL);
			break;
		}
	}
}
