/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sc_delegated_restarter.cc	1.10	08/05/20 SMI"

#include "sc_delegated_restarter.h"
#include "repository_val.h"
#include <scha.h>
#include <string>
#include "res_svc_basic_info.h"
#include "sc_dg_error.h"
#include "svc_timeout_wait.h"

class res_svc_basic_info;

void *register_reim(void *);


// A flag to indicate that the restarter is no more waiting and exiting
static int finished = 0;
std::list <res_rg_names> res_list;

//
// Call back function that is called by the master restarter to pass the events
//
static int
callback(restarter_event_t *e)
{
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	int			ret_flag = 0;
	char			name[1000];
	service_task 		*svc_task;
	string			svc_str;
	std::map<string, service_task *>::const_iterator it_svc_task;


	(void) restarter_event_get_instance(e, name, 1000 - 1);

	debug_print(NOGET("\n In The Callback event:%s"), name);

	//
	// Get a pointer to the service fmri object whose service
	// fmri is "name". Since the key value of hash table is string, convert
	// char* to string
	// No need of having a lock to access has hash table because nothing
	// is been changed and doesnt matter if smf grabs the service first or
	// the contract
	//
	svc_str = name;
	it_svc_task = service_task::svc_fmri_hash_tbl.find(svc_str);

	//
	// Confirm that the returned key from the svc hash table is a valid one
	//
	if (it_svc_task == service_task::svc_fmri_hash_tbl.end()) {
		debug_print(NOGET("\n The inst %s is " \
			    "not present in the service map table"), name);
		return (ret_flag);
	}
	svc_task = it_svc_task->second;

	//
	// Create a new service event task object and put it into the thread
	// pool queue of svc_task for further execution
	//
	svc_event_task *svc_ev_task = new svc_event_task();

	//
	// Set the svc event operation type to SMF
	//
	svc_ev_task->cur_et_op_type = SVC_ET_OP_SMF;

	//
	// Get the event type
	//
	svc_ev_task->cur_smf_event_type = restarter_event_get_type(e);

	//
	// Set the service task
	//
	svc_ev_task->svc_ptr = svc_task;

	//
	// Put the service fmri object into the threadpool queue of its own,
	// which inturn takes the necessary action on that svc
	// fmri in the excute_service method of that particular  service object
	//
	PTHREAD_MTX_LOCK(svc_task->svc_fmri_thread_pool_lock);
	svc_task->svc_fmri_thread_pool.defer_processing(svc_ev_task);
	PTHREAD_MTX_UNLOCK(svc_task->svc_fmri_thread_pool_lock);

	return (ret_flag);
}

//
// Failover the resource
//
void *
res_failover(void *r_info)
{
	scha_err_t scha_err;
	char *res_name;
	char *rg_name;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In the resource failover thread: %d"),
			pthread_self());

	res_name = ((resource_info *)r_info)->get_res_name();
	rg_name = ((resource_info *)r_info)->get_rg_name();

	debug_print(NOGET("\n In the resource failover res_name: %s, "
			"rg_name: %s"), res_name, rg_name);

	scha_err = scha_control(SCHA_CHECK_GIVEOVER, rg_name, res_name);
	if (scha_err != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\nError "
			"in chaecking the failing over the res group:%s",
			rg_name);
		return (NULL);
	}
	scha_err = scha_control(SCHA_GIVEOVER, rg_name, res_name);
	if (scha_err != 0) {
		//
		// SCMSGS
		// @explanation
		// The number of restarts of services under the resource has
		// reached the max. So the resource will be failing over
		// @user_action
		// No action needed.Just a notice meesage
		//
		logger.log(LOG_ERR, MESSAGE, "\nError "
			"in failing over the res group:%s",
			rg_name);
	}
	debug_print(NOGET("\n Done with Res Failover :%s"), rg_name);
}

bool
is_res_dg_type(char *res, char *rg, int *err)
{
	char	*res_type;
	scha_resource_t	res_handle;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	*err = 0;
	*err = scha_resource_open(res, rg, &res_handle);

	*err = scha_resource_get(res_handle,
				SCHA_TYPE,
				&res_type);
	if (*err == SCHA_ERR_NOERR) {
		//
		// Assume that *res_type has only one value
		//
		if (((strcasecmp(res_type, PROXY_SMF_FAILOVER)) == 0) ||
		    ((strcasecmp(res_type, PROXY_SMF_LOAD_BALANCED)) == 0) ||
		    ((strcasecmp(res_type, PROXY_SMF_MULTI_MASTER)) == 0)) {
			*err = scha_resource_close(res_handle);
			return (true);
		} else {
			debug_print(NOGET("\n The res type "
			    " is not that of dg restarter %s"), res_type);
			*err = scha_resource_close(res_handle);
			return (false);
		}
	} else {
		//
		// SCMSGS
		// @explanation
		// Error in reading the resource type of the resource
		// @user_action
		// Check if the RGM functionalities are working fine and if
		// the resources
		//  are present. Contact SUN vendor for more help.
		//
		logger.log(LOG_NOTICE, MESSAGE, "\nError in "
			"scha_resource_get: %s", scha_strerror(*err));
		*err = scha_resource_close(res_handle);
		return (false);
	}
}


//
// Get the list of resources that are under the control of delegated restarter
// If success return 0 a pointer that points to the head of the sigly linked
// list else returns -1 and NULL list
//
int
get_res_list()
{
	//
	// Get all the rg's in the node
	//
	scha_cluster_t  	handle;
	scha_resourcegroup_t	rg_handle;
	scha_str_array_t	*all_rgs = NULL;
	scha_str_array_t	*all_res = NULL;
	int		err = 0;
	int		rg_count;
	int		res_count;
	char		*rg;
	char		*res;
	string 		res_str;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
	err = scha_cluster_open(&handle);
	//
	// Get all the rg's under the cluster
	//
	if ((err = scha_cluster_get(handle, SCHA_ALL_RESOURCEGROUPS,
				    &all_rgs)) == 0) {
		for (rg_count = 0; rg_count < (int)all_rgs->array_cnt;
			rg_count++) {
			rg = all_rgs->str_array[rg_count];
			//
			// Get all the resources under this rg
			//
			err = scha_resourcegroup_open(rg, &rg_handle);

			err = scha_resourcegroup_get(rg_handle,
						SCHA_RESOURCE_LIST,
						&all_res);
			if (err == SCHA_ERR_NOERR) {
				for (res_count = 0; res_count <
					(int)all_res->array_cnt; res_count++) {
					res = all_res->str_array[res_count];
					if (is_res_dg_type(res, rg, &err)) {
						//
						// Add the res to the link list
						//
						res_rg_names r_rg_info;
						r_rg_info.res_name = res;
						r_rg_info.rg_name = rg;
						res_list.push_back(r_rg_info);
					} else {
						//
						// Either res is not of the
						// desired type or err
						//
						if (err != 0) {
					scha_resourcegroup_close(rg_handle);
					scha_cluster_close(handle);
					return (-1);
						}
					}
				}

			} else {
				//
				// SCMSGS
				// @explanation
				// Error in reading the resource group
				// @user_action
				// Check if the RGM functionalities are
				// working fine and if the resource group is
				// present. Contact SUN vendor for more help.
				//
				logger.log(LOG_ERR, MESSAGE, "\nError in "
					"scha_resourcegroup_get");
				err = scha_resourcegroup_close(rg_handle);
				err =  scha_cluster_close(handle);
				return (-1);
			}
			err = scha_resourcegroup_close(rg_handle);
		}
	} else {
		//
		// SCMSGS
		// @explanation
		// Error in getting all the resource groups in the cluster
		// @user_action
		// Check if the RGM functionalities are working fine and if
		// the resource group is present. Contact SUN vendor for more
		// help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in scha_cluster_get");
	}

	err =  scha_cluster_close(handle);

	return (err);
}

//
// If the restarter dies and comes back, then have to get back to the previous
// working state by populating the data structure and re initialize all the
// values. Return 0 on success and -1 on error
//
int
re_init_restarter()
{
	int err;
	pthread_t res_failover_thr;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
	std::list<res_rg_names>::const_iterator iter;

	if (get_res_list() != 0) {
		debug_print(NOGET("\n Error in getting the "\
			"resource list"));
		return (-1);
	}
	//
	// Start constructing the data base
	//
	for (iter = res_list.begin(); iter != res_list.end(); iter++) {
		res_svc_basic_info rg_info;
		resource_info *rs_info;
		res_rg_names r_rg_name;
		r_rg_name = *iter;
		if ((rg_info.process_method((char *)r_rg_name.res_name.c_str(),
				(char *)r_rg_name.rg_name.c_str(),
				RCT_START_REG)) != 0) {
			debug_print(NOGET("\n Error while "
				"registering the res  %s"),
				(char *)r_rg_name.res_name.c_str());

			//
			// Failover the resource
			//
			rs_info = rg_info.get_resource_info(
					(char *)r_rg_name.res_name.c_str());
			if (rs_info) {
				PTHREAD_MTX_LOCK(service_task::
					res_failover_lock);
				pthread_create(&res_failover_thr, NULL,
					res_failover, (void *)rs_info);
				PTHREAD_MTX_UNLOCK(service_task::
					res_failover_lock);
			}
		}
	}
	return (0);
}

//
// Sets the flag when INT or TERM signal arrives
//
static void
die_handler(int sig, siginfo_t *info, void *data)
{
	finished = 1;
}

int
main(int argc, char **argv)
{
	pid_t pid;
	struct sigaction act;
	sigset_t nullset;
	int err;
	pthread_t reim_reg_thread;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	if ((pid = fork1()) < 0)
		return (-1);

	if (pid != 0)
		exit(0);

	err = pthread_create(&reim_reg_thread, NULL, register_reim, NULL);
	if (err != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_NOTICE, MESSAGE, "\n Error in register_reim()"
			" thread creation");
		abort();
	}

	//
	// Initialize the sc_syslog tag
	//
	(void) sc_syslog_msg_set_syslog_tag(SC_SYSLOG_SMF_DR_TAG);

	//
	// To delegated restarter exits only if Interrupt or Termination signal
	// is delivered
	//
	act.sa_sigaction = &die_handler;
	(void) sigfillset(&act.sa_mask);
	act.sa_flags = SA_SIGINFO;
	(void) sigaction(SIGINT, &act, NULL);
	(void) sigaction(SIGTERM, &act, NULL);

	//
	// Assign a handle for the restarter and register it
	//
	if ((restarter_bind_handle(RESTARTER_EVENT_VERSION,
				SC_DR_INSTANCE_FMRI,
			callback, RESTARTER_FLAG_DEBUG,
				&service_task::res_evt_handle)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The sc delegated restarter is not able to bind the handle
		// with the SMF.
		// @user_action
		// Check if the SMF default restarter
		// "svc:/system/svc/restarter:default"
		//  is online. Check if other SMF services are functioning
		//  properly.
		// Contact SUN vendor for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Failed to bind delegated "
			"restarter handle: %s\n", strerror(errno));
	} else {
		debug_print(NOGET("\n Restarter successfully"
			" registered"));
	}

	//
	// Create and initialize the contract
	//
	sc_restarter_contract::create();

	//
	// Check if the restarter is comming through RGM boot or after death
	//
	int64_t init_flag_val = 0;
	//
	// Update the delegated restarter repository property
	//
	rep_val_handling rep_sc_hdl;

	rep_sc_hdl.get_method(SC_DR_INSTANCE_FMRI,
			SC_DELEGATED_PG, SVC_DG_INIT_FLAG_PROP,
			&init_flag_val, &err);
	if (err != 0) {
		debug_print(NOGET("\n Could not get the prop val "
			"%s from inst %s"), SVC_DG_INIT_FLAG_PROP,
			SC_DR_INSTANCE_FMRI);
		return (-1);
	}
	if (init_flag_val == 0) {
		//
		// Update the respository till it gets set by next rgm boot
		// method
		//
		debug_print(NOGET("\n Restarter started for first time"));
		err = rep_sc_hdl.put_rep_vals(
					SC_DR_INSTANCE_FMRI,
					1, SC_DELEGATED_PG,
					SVC_DG_INIT_FLAG_PROP);
		if (err != 0) {
			debug_print(NOGET("\n Could not put the"
				"prop of svc %s, in the repository: %s"),
				SC_DR_INSTANCE_FMRI,
				scf_strerror((scf_error_t)err));
			return (-1);
		}
	} else {
		//
		// Restarter comming after death
		//
		debug_print(NOGET("\n Restarter started after death"));
		err = re_init_restarter();
		if (err != 0) {
			debug_print(NOGET("\n Re-Initialization"
				" Failure"));
			return (-1);
		} else {
			debug_print(NOGET("\n Re-Initialization"
				" Success"));
		}
	}

	(void) sigemptyset(&nullset);
	while (!finished) {
		debug_print(NOGET("Restarter Main thread paused\n"));
		(void) sigsuspend(&nullset);
	}
	//
	// Clear the Service hash table
	//
	service_task::svc_fmri_hash_tbl.clear();
	//
	// Clear the resource hash table
	//
	resource_info::res_name_hash_tbl.clear();
	//
	// Clear the resource hash table
	//
	contract_info::contract_info_hash_tbl.clear();
	//
	// Free the contract handling
	//
	if (sc_restarter_contract::manage_contract)
		delete(sc_restarter_contract::manage_contract);

	//
	// Free the svc timeout
	//
	if (svc_timeout_wait::the_svc_timeout)
		delete(svc_timeout_wait::the_svc_timeout);

	debug_print(NOGET("Restarter Main thread exiting\n"));

	return (0);

}
