/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SC_DELEGATED_RESTARTER_H
#define	_SC_DELEGATED_RESTARTER_H

#pragma ident	"@(#)sc_delegated_restarter.h	1.5	08/05/20 SMI"

#include <librestart.h>
#include <libscf.h>
#include <sys/sc_syslog_msg.h>
#include <sys/os.h>
#include <sys/rsrc_tag.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <assert.h>
#include <sys/threadpool.h>
#include <sys/ctfs.h>
#include <sys/contract/process.h>
#include <libcontract.h>
#include <string>
#include <list>

using namespace std;

enum res_call_type {
	RCT_INIT = 0,
	RCT_BOOT,
	RCT_START_REG,
	RCT_START,
	RCT_STOP,
	RCT_FINI,
	RCT_VALIDATE,
	RCT_VALIDATE_UPDATE,
	RCT_IS_RES_REG,
	RCT_IS_SVC_OFFLINE,
	RCT_IS_SVC_MANAGED,	// For all the svc's under a res
	RCT_IS_THIS_SVC_MANAGED // For a single svc
};
typedef enum res_call_type res_call_type;

enum res_state {
	RS_OFFLINE = 0,
	RS_ONLINE,
	RS_START_FAILED,
	RS_STOP_FAILED,
	RS_ONLINE_NOT_MONITORED,
	RS_MON_FAILED,
	RS_STARTING,
	RS_STOPPING
}res_state;
typedef enum res_state res_state;

#include "resource_mgmt.h"
#include "service_mgmt.h"
#include "sc_restarter_contract.h"

#define	SC_DR_INSTANCE_FMRI "svc:/system/cluster/sc_restarter:default"
#ifdef DEBUG
//
// Note: Just add os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);
// before calling this.It was not added inside the body because the compiler
// might give warning for double declaration if logger is already defined in
// the function
//
#define	PTHREAD_MTX_LOCK(h) {						   \
	int err;							   \
	    if ((err = pthread_mutex_lock(&h)) != 0) {			   \
	    logger.log(LOG_ERR, MESSAGE, "\n Lock Error: %s: %d: %s",      \
			    __FILE__, __LINE__, strerror(err)); 	   \
		abort();					   	   \
	} else {       							   \
	    logger.log(LOG_ERR, MESSAGE, "\n Lock Success: %s: %d",        \
			    __FILE__, __LINE__);	 		   \
	}								   \
}


#define	PTHREAD_MTX_UNLOCK(h) {						   \
	int err;							   \
	    if ((err = pthread_mutex_unlock(&h)) != 0) {		   \
	    logger.log(LOG_ERR, MESSAGE, "\nUnLock Error: %s: %d: %s",     \
			    __FILE__, __LINE__, strerror(err)); 	   \
		abort();						   \
	} else {       							   \
	    logger.log(LOG_ERR, MESSAGE, "\n UnLock Success: %s: %d",      \
			    __FILE__, __LINE__);	 		   \
	}								   \
}

#else

#define	PTHREAD_MTX_LOCK(h)	(void)pthread_mutex_lock(&h)
#define	PTHREAD_MTX_UNLOCK(h)	(void)pthread_mutex_unlock(&h)

#endif


struct res_rg_names {
	string res_name;
	string rg_name;
};
typedef struct res_rg_names res_rg_names;

static int
callback(restarter_event_t *e);

static void
die_handler(int sig, siginfo_t *info, void *data);

void *
res_failover(void *r_info);

int
get_res_list();

int
re_init_restarter();

#endif /* _SC_DELEGATED_RESTARTER_H */
