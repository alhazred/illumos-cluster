/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RESOURCE_MGMT_H
#define	_RESOURCE_MGMT_H

#pragma ident	"@(#)resource_mgmt.h	1.7	08/05/20 SMI"

//
// This file contains information about the resource management
//

#include <sys/threadpool.h>
#include <scha.h>
#include <pthread.h>
#include <string>
#include <map>
#include <sstream>
#include "repository_val.h"
#include "service_mgmt.h"
#include "sc_delegated_restarter.h"

#define	PROXY_SMF_FAILOVER	"SUNW.PROXY_SMF_FAILOVER"
#define	PROXY_SMF_LOAD_BALANCED	"SUNW.PROXY_SMF_LOAD_BALANCED"
#define	PROXY_SMF_MULTI_MASTER	"SUNW.PROXY_SMF_MULTI_MASTER"

class service_task;

typedef enum {
	RES_EXEC_SUCCESS = 0,
	RES_EXEC_FAILED,
	RES_NOT_ONLINE,
	RES_ALREADY_ONLINE,
	RES_REGISTERED,
	RES_NOT_REGISTERED,
	RES_REG_SUCCESS,
	RES_REG_FAILED,
	RES_REPOSITORY_ERR
}res_reg_process_status;

struct res_svc_coordinate {

	//
	// Conditional variable to synchronise between  resource and
	// the service management.Ones all the work on the services
	// under that
	// particulae res is done, the waiting res thread is woken up
	//
	pthread_cond_t	res_exec_cond;

	//
	// Lock to work on the res_exec_cond
	//
	pthread_mutex_t	res_exec_lock;

	//
	// To keep track of the number of services that has finished
	// execution.Ones svc_count reaches the num_fmri's count,
	// will signal the condition varibale to wake the
	// sleeping thread
	//
	int svc_count;
};

typedef struct res_svc_coordinate res_svc_coordinate;

struct retry_cnt_struct {
	//
	// Count, to keep track the number of times any of the
	// service  is restarted.Initially this is 0, and when it
	// reaches svc_retry_count,failover the resource
	//
	int retry_counter;

	//
	// Queue, to keep track of the retry timer
	//
	std::list<time_t> retry_timer_queue;

	//
	// Since all services under this resource will be updating the
	// information, lock has to be grabbed to serialize
	//
	pthread_mutex_t	retry_cnt_lock;

};

typedef struct retry_cnt_struct retry_cnt_struct;
//
// Manages a single resource
//
class resource_info {
	private:
		//
		// resource name
		//
		char	*res_name;

		//
		// resource group name
		//
		char	*rg_name;

		// Filename from the resource property,
		// Proxied_service_instances
		char	*res_prop_file_name;


		// number of svc fmri's
		int	num_fmris;

		// To create a svc_fmri obj and fill it up
		int create_svc_fmri(char *new_svc_fmri);

		//
		// set current res state
		//
		int  set_cur_res_state(res_state cur_state);

		int get_res_prop(char *r_name, char *res_grp_name);
		int update_fmris(char *input_file);
		int process_fmri(char *input_file);
	public:
		//
		// Flag to distinguish between resource level initialization
		// and dg getting restarted after death
		// If true, then the dg restarter came up after its death
		// If false, dg restarter is being started for the first
		// time from RGM boot
		//
		static bool res_dg_init_flag;

		// Head pointer to a linked list of svc fmris under this
		// resource

		// Move later into private
		service_task	*svc_fmri_head;
		service_task	*svc_fmri_tail;

		//
		// Set this flag when a res stop is called so that any furure
		// starts of the services can be cancelled there by avoiding
		// pending starts as much as possible
		//
		bool is_res_stop_called;

		//
		// Current State of the resource
		//
		res_state	cur_res_state;

		//
		//  During starting or stopping the res, if any one service
		// fails set this flag
		// Since only one value 'true' will be set
		bool is_svc_exec_failed;

		//
		// Coordinate between the working of res and svc. Wake the
		// resource when all the sermutexvices under it have finished
		// its jobs
		//
		res_svc_coordinate res_svc_lock;

		//
		// To serialize the operation on a particular resource,
		// have to grab
		// this lock
		//
		pthread_mutex_t	res_mgmt_lock;

		// Hash table to keep resource names as the key and its
		// corresponding
		// storage address as the data
		static std::map<string, resource_info *> res_name_hash_tbl;

		//
		// Project Name
		//
		char *project_name;

		//
		// Max service retries before failover.This is read as a
		// property (RETRY_COUNT) from the resource
		//
		int svc_retry_count;

		//
		// Service retry interval.This is read as a resource property
		// (RETRY_INTERVAL)
		//
		time_t svc_retry_intvl;

		retry_cnt_struct retry_cnt_mgmt;

		resource_info(char *r_name, char *res_grp_name, int &err);
		~resource_info();

		int resource_update(char *r_name, char *rg_name,
			char *svc_fmri_file, int retry_cnt,
			int retry_interval);

		void add_svc_fmri(char *r_name, service_task *svc_fmri);
		char *get_res_name();
		char *get_rg_name();
		//
		// If succeeds, returns the number of svc fmri's else
		// returns an error
		//
		int get_svc_fmris(service_task *svc_fmris[]);

		int set_svc_fmri(service_task svc_fmri);
		int get_num_fmris();
		//
		// Takes care of execution of threads,
		// which internally calls service_task:execute
		//  for each svc fmri
		//
		int process_res(res_call_type rcall_type);
};

#endif /* _RESOURCE_MGMT_H */
