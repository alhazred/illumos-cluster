/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)resource_mgmt.cc	1.9	09/01/26 SMI"

#include "resource_mgmt.h"

#include <errno.h>
#include <sys/types.h>
#include <fstream.h>
#include "sc_dg_error.h"

using namespace std;

std::map<string, resource_info *> resource_info::res_name_hash_tbl;
bool resource_info::res_dg_init_flag = false;
//
// Create an svc_fmri object and link it to the list
// Returns 0 on success or the error code
//
int
resource_info::create_svc_fmri(char *new_svc_fmri) {

	service_task	*tmp_svc_ptr;
	int	err = 0;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	tmp_svc_ptr = new service_task(new_svc_fmri, &err);

	if (! tmp_svc_ptr) {
		//
		// SCMSGS
		// @explanation
		// Problem in creating memory space during resource management
		// of SMF delegated restarter
		// @user_action
		// Check the system memory usage
		//
		logger.log(LOG_ERR, MESSAGE, "\n Unable to allocate memory "
			"in sc delegated resource mgmt");
		return (ENOMEM);
	}
	if (err != 0) {
		debug_print(NOGET("\n Could not construct the "\
			"service task: %s,in res: %s"), new_svc_fmri,
			res_name);
		delete(tmp_svc_ptr);
		tmp_svc_ptr = NULL;
		return (-1);
	}

	tmp_svc_ptr->res_info = this;
	tmp_svc_ptr->prev_svc_ptr = NULL;
	tmp_svc_ptr->next_svc_ptr = NULL;
	//
	// Link the tmp_svc to the svc_fmri linked list
	//
	if (svc_fmri_head == NULL) {
		svc_fmri_head = tmp_svc_ptr;
		svc_fmri_tail = tmp_svc_ptr;
	} else {
		svc_fmri_tail->next_svc_ptr = tmp_svc_ptr;
		tmp_svc_ptr->prev_svc_ptr = svc_fmri_tail;

		svc_fmri_tail = tmp_svc_ptr;
	}
	tmp_svc_ptr = NULL;
	return (0);
}

int
resource_info::process_fmri(char *input_file) {
	FILE *fp;
	char stream_read[200] = {0};
	char file_name[100] = {0};
	char fmri_val[100] = {0};
	char *ptr1 = NULL, *ptr2 = NULL;
	char outstr[100] = {0};
	int err;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	if ((fp = fopen(input_file, "r")) == NULL) {
		logger.log(LOG_ERR, MESSAGE, "\nCan't open %s\n", input_file);
		return (-1);
	}

	debug_print(NOGET("\nSuccess open %s\n"), input_file);
	while (!feof(fp)) {
		fscanf(fp, "%s", stream_read);
		if (strcmp(stream_read, "") == 0)
			break;
		debug_print(NOGET("\n input_stream %s"), stream_read);
		ptr1 = strrchr(stream_read, '>');
		ptr2 = strrchr(stream_read, '<');
		strncpy(file_name, ptr2+1, ptr1-ptr2-1);
		debug_print(NOGET("\nfilename : %s"), file_name);
		ptr1 = strchr(stream_read, '<');
		ptr2 = strchr(stream_read, '>');
		strncpy(fmri_val, ptr1+1, ptr2-ptr1-1);
		debug_print(NOGET("\nfmri_value : %s"), fmri_val);

		err = create_svc_fmri(fmri_val);
		if (err != 0) {
			//
			// SCMSGS
			// @explanation
			// Could not create the named smf service fmri to mage
			// under SC delegated restarter
			// @user_action
			// Check the previous messsages for the reason
			//
			logger.log(LOG_NOTICE, MESSAGE, "\n Error in creating"
				" svc_fmri %s", fmri_val);
			fclose(fp);
			return (-1);
		}
		num_fmris++;

		debug_print(NOGET("\n FMRI: %s, \n Manifest: %s"),
			fmri_val, file_name);
		memset(stream_read, 0, 199);
		memset(fmri_val, 0, 99);
		memset(file_name, 0, 99);
	}
	fclose(fp);
	return (0);
}

//
// Get the resource properties like the extended prop, retry_count,
// retry_interval.
// Retrun 0 on success
//	-1 on failure
//
int
resource_info::get_res_prop(char *r_name, char *res_grp_name)
{
	scha_err_t 		scha_err;
	scha_extprop_value_t 	*extprop_svc_insts;
	int			r_retry_count;
	time_t			r_retry_intvl;
	scha_resource_t 	res_handle;
	scha_resourcegroup_t	rg_handle;
	char			*res_proj_name = NULL;
	char			*rg_proj_name = NULL;


	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In the resource_info::get_res_prop"));

	// Get the svc fmri's from the file name obtained from the
	// extended resource property "Proxied_service_instances"
	// from the RTR file

	scha_err = scha_resource_open(res_name, rg_name, &res_handle);
	if (scha_err != 0) {
		//
		// SCMSGS
		// @explanation
		// Cannot create a handle for the scha resource
		// @user_action
		// Check out the man pages for help depending on the error
		// type
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in scha_res_open of "
			"res:%s, rg:%s, Err: %s,", res_name, rg_name,
			scha_strerror(scha_err));
		return (-1);
	}

	scha_err = scha_resource_get(res_handle, SCHA_EXTENSION,
				"Proxied_service_instances",
				&extprop_svc_insts);
	if (scha_err != 0) {
		//
		// SCMSGS
		// @explanation
		// Cannot read the extension property
		// @user_action
		// Take action dependng on the error.Can contact your SUN
		// vendor for updates or patches if any
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in scha_res_get of "
			"extn prop,res:%s, Err: %s", res_name,
			scha_strerror(scha_err));
		scha_strerror(scha_err);
		scha_resource_close(res_handle);
		return (-1);
	}

	scha_err = scha_resource_get(res_handle, SCHA_RETRY_COUNT,
				&r_retry_count);
	if (scha_err != 0) {
		//
		// SCMSGS
		// @explanation
		// Cannot read the resource retry count in sc delegated
		// restarter
		// @user_action
		// Take action depending on the error and contact sun vendor
		// for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in scha_res_get of "
			"retry cnt prop,res:%s, Err: %s", res_name,
			scha_strerror(scha_err));

		scha_strerror(scha_err);
		scha_resource_close(res_handle);
		return (-1);
	}
	svc_retry_count = r_retry_count;

	scha_err = scha_resource_get(res_handle, SCHA_RETRY_INTERVAL,
				&r_retry_intvl);
	if (scha_err != 0) {
		//
		// SCMSGS
		// @explanation
		// Cannot read the resource retry interval property in sc
		// delegated restarter
		// @user_action
		// Take action depending on the error and contact sun vendor
		// for more help.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in scha_res_get of "
			"retry intvl prop,res:%s, Err: %s", res_name,
			scha_strerror(scha_err));
		scha_strerror(scha_err);
		scha_resource_close(res_handle);
		return (-1);
	}
	svc_retry_intvl = r_retry_intvl;

	// Copy the file name from the resource property
	res_prop_file_name = new char[strlen(
			extprop_svc_insts->val.val_str)+1];
	if (res_prop_file_name == NULL) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Could not allocate memory to"
			"store the res property file name %s",
			res_prop_file_name);
		return (-1);
	}

	debug_print(NOGET("\n Extn Prop File Name:"
			"extn prop,res:%s, File: %s"), res_name,
			extprop_svc_insts->val.val_str);

	strcpy(res_prop_file_name, extprop_svc_insts->val.val_str);

	//
	// Get the Res Project Name
	//
	scha_err = scha_resource_get(res_handle, SCHA_RESOURCE_PROJECT_NAME,
				&res_proj_name);
	if (scha_err != 0) {
		//
		// SCMSGS
		// @explanation
		// Cannot read the project name property of the resource
		// @user_action
		// Take action depending on the error and contact SUN vendor
		// for more help
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in scha_res_get of "
			"project name prop,res:%s, Err: %s", res_name,
			scha_strerror(scha_err));
		scha_strerror(scha_err);
		scha_resource_close(res_handle);
		return (-1);
	}

	//
	// If res project name was null, get from rg project name.If that is
	// also null, set to default
	//
	if ((res_proj_name == NULL) || (*res_proj_name == '\0')) {
		scha_err = scha_resourcegroup_open(rg_name, &rg_handle);
		if (scha_err != 0) {
			//
			// SCMSGS
			// @explanation
			// Cannot open the resource group
			// @user_action
			// Take action depending on the error depending on the
			// error code
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in "
			"scha_rg_open of rg:%s, Err: %s", rg_name,
			scha_strerror(scha_err));
			return (-1);
		}

		scha_err = scha_resourcegroup_get(rg_handle,
						SCHA_RG_PROJECT_NAME,
						&rg_proj_name);
		if (scha_err != 0) {
			//
			// SCMSGS
			// @explanation
			// Cannot get the requesting rg
			// @user_action
			// Depending on the error take action and the deatils
			// are provided in the man page scha_rg_get(3HA)
			//
			logger.log(LOG_ERR, MESSAGE, "\n Error in "
			"scha_rg_get of rg:%s, Err: %s", rg_name,
				scha_strerror(scha_err));
			scha_strerror(scha_err);
			scha_resourcegroup_close(rg_handle);
			return (-1);
		}
		scha_resourcegroup_close(rg_handle);
		if ((rg_proj_name == NULL) || (*rg_proj_name == '\0')) {
			//
			// Project name not present in both the res and rg
			// properties, so set to default
			//
			project_name = strdup("default");
			if (project_name == NULL) {
				//
				// SCMSGS
				// @explanation
				// Problem in creating memory space during
				// resource management of SMF delegated
				// restarter
				// @user_action
				// Check the system memory usage
				//
				logger.log(LOG_ERR, MESSAGE, "\n Unable to"
				"allocate memory in sc delegated resource "
				"mgmt");
				return (-1);
			}
		} else {
			project_name = strdup(rg_proj_name);
			if (project_name == NULL) {
				logger.log(LOG_ERR, MESSAGE, "\n Unable to "
				"allocate memory in sc delegated resource "
				"mgmt");
				return (-1);
			}
		}
	} else {
		project_name = strdup(res_proj_name);
		if (project_name == NULL) {
			logger.log(LOG_ERR, MESSAGE, "\n Unable to allocate "
			"memory in sc delegated resource mgmt");
			return (-1);
		}
	}

	debug_print(NOGET("\n Project Name:%s"), project_name);

	scha_resource_close(res_handle);
	return (0);
}

//
// Constructor of resource_info
// Build the resource, service database
//
resource_info::resource_info(char *r_name, char *res_grp_name, int &err):
				res_name(NULL), rg_name(NULL),
				res_prop_file_name(NULL),
				is_svc_exec_failed(false),
				svc_fmri_tail(NULL), project_name(NULL),
				is_res_stop_called(false),
				svc_fmri_head(NULL) {

	scha_err_t 		scha_err;
	scha_extprop_value_t 	*extprop_svc_insts;
	int			r_retry_count;
	time_t			r_retry_intvl;
	scha_resource_t 	res_handle;
	scha_resourcegroup_t	rg_handle;
	int			svc_fd;
	char			*svc_fmri_buf;
	ifstream		res_file_stream;
	string			res_name_str;
	char			*res_proj_name = NULL;
	char			*rg_proj_name = NULL;
	typedef pair<string, resource_info *> res_addr_pair;
	pair<std::map<string, resource_info *>::iterator, bool> pr_res;

	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In the resource_info::resource_info"));

	//
	// Initalise all the res level condition variables and locks
	//
	pthread_cond_init(&res_svc_lock.res_exec_cond, NULL);
	pthread_mutex_init(&res_svc_lock.res_exec_lock, NULL);
	pthread_mutex_init(&res_mgmt_lock, NULL);
	pthread_mutex_init(&retry_cnt_mgmt.retry_cnt_lock, NULL);

	res_svc_lock.svc_count = 0;
	err = 0;
	num_fmris = 0;
	retry_cnt_mgmt.retry_counter = 0;

	if (r_name && res_grp_name) {

		res_name = new char[strlen(r_name)+1];
		rg_name = new char[strlen(res_grp_name)+1];

		if (res_name && rg_name) {
			sprintf(res_name, "%s", r_name);
			sprintf(rg_name, "%s", res_grp_name);
		} else {
		logger.log(LOG_ERR, MESSAGE, "\n Unable to allocate memory "
			"in sc delegated resource mgmt");
			err = -1;
			return;
		}
	} else {
		//
		// Invalid resource or resource group name
		//

		//
		// SCMSGS
		// @explanation
		// Specified resource group or resource name that was asked to
		// be managed by delegated restarter is not valid
		// @user_action
		// check for the validy of names
		//
		logger.log(LOG_ERR, MESSAGE, "\n Invalid resource or resource"
			" group name");
		err = EINVAL;
		return;
	}

	//
	// Insert this resource into the hash table
	//
	// convert char* to a string before inserting
	//
	res_name_str = res_name;
	pr_res = res_name_hash_tbl.insert(res_addr_pair(res_name_str, this));
	if (pr_res.second == false) {
		debug_print(NOGET("\n Res map insert exist:%s"), res_name);
	}

	//
	// Get all the res and rg properties
	//
	if (get_res_prop(res_name, rg_name) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "Error in retrieveing the prop "
		"from res %s", res_name);
		err = -1;
		return;
	}

	if (process_fmri(res_prop_file_name) != 0) {
		//
		// SCMSGS
		// @explanation
		// Cannot process the smf fmri under the resource by sc
		// delegated restarter
		// @user_action
		// The previous messages that gets displayed should
		// help.Contact the SUN vendor for more info
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in processing and "
			" creating fmri");
		err = -1;
		return;
	}
	//
	// Set the current state if the restarter had come up after death
	// Since all the services under thsi res would have stored the state,
	// just the state from the first svc will be fine
	//
	if (res_dg_init_flag) {
		int64_t state_val;
		int err;
		rep_val_handling rep_val_hdl;
		rep_val_hdl.get_method(svc_fmri_head->svc_fmri,
						SC_DELEGATED_PG,
						SVC_CUR_RES_STATE_PROP,
						&state_val, &err);
		cur_res_state = (res_state)state_val;
	}
}

resource_info::~resource_info()
{
	debug_print(NOGET("\n Destructing resource: %s"), res_name);
	//
	// Destroy all the res level condition variables and locks
	//
	(void) pthread_cond_destroy(&res_svc_lock.res_exec_cond);
	(void) pthread_mutex_destroy(&res_svc_lock.res_exec_lock);
	(void) pthread_mutex_destroy(&res_mgmt_lock);
	(void) pthread_mutex_destroy(&retry_cnt_mgmt.retry_cnt_lock);
	//
	// Remove the entry from res hash table
	//
	string res_name_str;
	res_name_str = res_name;
	res_name_hash_tbl.erase(res_name_str);

	if (res_name)
		delete(res_name);
	if (rg_name)
		delete(rg_name);
	if (project_name)
		free(project_name);
	//
	// Clear the service tasks
	//
	service_task	*tmp_svc_ptr;
	while (svc_fmri_head != svc_fmri_tail) {
		tmp_svc_ptr = svc_fmri_head;
		svc_fmri_head = svc_fmri_head->next_svc_ptr;
		if (tmp_svc_ptr)
			delete(tmp_svc_ptr);
	}
	//
	// Free the last service_task
	//
	if (svc_fmri_head)
		delete(svc_fmri_head);

}

//
// Update the service fmris if there have been any changes
// Return 0 on success
//	 -1 on failure
//
int
resource_info::update_fmris(char *input_file)
{
	FILE *fp;
	char stream_read[200] = {0};
	char fmri_val[100] = {0};
	char *ptr1 = NULL, *ptr2 = NULL;
	char outstr[100] = {0};
	service_task	*tmp_svc_ptr;
	service_task	*tmp_svc_ptr_tail;
	int err;
	int result = -1;

	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In the resource_info::update_fmris"));

	if ((fp = fopen(input_file, "r")) == NULL) {
		logger.log(LOG_ERR, MESSAGE, "\nCan't open %s\n", input_file);
		return (result);
	}

	debug_print(NOGET("\nSuccess open %s\n"), input_file);
	//
	// To keep track of the current tail
	//
	tmp_svc_ptr_tail = svc_fmri_tail;

	//
	// Add if there are new svc fmris
	//
	while (!feof(fp)) {
		fscanf(fp, "%s", stream_read);
		if (strcmp(stream_read, "") == 0)
			break;
		debug_print(NOGET("\n input_stream %s"), stream_read);
		ptr1 = strchr(stream_read, '<');
		ptr2 = strchr(stream_read, '>');
		strncpy(fmri_val, ptr1+1, ptr2-ptr1-1);
		debug_print(NOGET("\nfmri_value : %s"), fmri_val);

		tmp_svc_ptr = svc_fmri_head;

		if (tmp_svc_ptr) {
			//
			// Check if the fmri is new or already present in
			// the table
			//
			while (tmp_svc_ptr != tmp_svc_ptr_tail->next_svc_ptr) {
				//
				// If the above fmri is already present in the
				// internal fmri list, mark the fmri in the
				// internal list
				//
				if (!tmp_svc_ptr->is_update_marked) {
					if (strcmp(tmp_svc_ptr->svc_fmri,
						fmri_val) == 0) {
						tmp_svc_ptr->is_update_marked =
							true;
						break;
					}
				}
				tmp_svc_ptr = tmp_svc_ptr->next_svc_ptr;
			}
		}

		if ((tmp_svc_ptr == NULL) ||
		    (!tmp_svc_ptr->is_update_marked)) {
			//
			// Its a new svc.Create the new svc
			//
			err = create_svc_fmri(fmri_val);
			if (err != 0) {
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				logger.log(LOG_NOTICE, MESSAGE, "\n Error in"
				"creating svc_fmri %s", fmri_val);
				result = -1;
				goto clean_up;
			}
			num_fmris++;
		}

		debug_print(NOGET("\n FMRI: %s"), fmri_val);
		memset(stream_read, 0, 199);
		memset(fmri_val, 0, 99);
	}

	//
	// Delete if there are any removed svc fmris
	//
	// If there are any unmarked fmris between svc_fmri_head and
	// tmp_svc_ptr_tail, they are to be deleted
	//
	tmp_svc_ptr = svc_fmri_head;

	if (tmp_svc_ptr) {
		//
		// Remove the unmarked fmris, by re-arranging the linked list
		// pointer and deleting the pointer
		//
		while (tmp_svc_ptr != tmp_svc_ptr_tail->next_svc_ptr) {
			if (!tmp_svc_ptr->is_update_marked) {
				//
				// Do not delete if the svc is in online state
				//
				restarter_instance_state_t svc_state;
				svc_state = tmp_svc_ptr->get_svc_cur_state();
				if (svc_state == RESTARTER_STATE_ONLINE) {
					//
					// SCMSGS
					// @explanation
					// Need explanation of this message!
					// @user_action
					// Need a user action for this
					// message.
					//
					logger.log(LOG_NOTICE, MESSAGE, "\nThe"
					"svc frmi %s is in online state."
					"To remove this fmri,take the resource"
					"to offline state and then update the"
					"fmri list", tmp_svc_ptr->svc_fmri);
				} else {
					if (tmp_svc_ptr->prev_svc_ptr != NULL)
						tmp_svc_ptr->prev_svc_ptr->
						    next_svc_ptr =
						    tmp_svc_ptr->next_svc_ptr;
					if (tmp_svc_ptr->next_svc_ptr != NULL)
						tmp_svc_ptr->next_svc_ptr->
						    prev_svc_ptr =
						    tmp_svc_ptr->prev_svc_ptr;
					delete(tmp_svc_ptr);
				}
			}
			tmp_svc_ptr = tmp_svc_ptr->next_svc_ptr;
		}
	}

	result = 0;
clean_up:
	// Reset the is_update_marked field in the existing svc's and keep the
	// svc list clean
	tmp_svc_ptr = svc_fmri_head;
	while (tmp_svc_ptr != tmp_svc_ptr_tail->next_svc_ptr) {
		tmp_svc_ptr->is_update_marked = false;
		tmp_svc_ptr = tmp_svc_ptr->next_svc_ptr;
	}
	return (result);
}

//
// Update the resource properties
// Return  0 on success
// Return -1 on failure
//
int
resource_info::resource_update(char *r_name, char *res_gr_name,
			char *svc_fmri_file, int retry_cnt, int retry_interval)
{
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	//
	// Set the property values, if they are different
	//
	if (strcmp(res_prop_file_name, svc_fmri_file) != 0) {
		// Free prev allocated space
		if (res_prop_file_name)
			free(res_prop_file_name);
		res_prop_file_name = new char[strlen(svc_fmri_file)+1];
		if (res_prop_file_name == NULL) {
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			logger.log(LOG_ERR, MESSAGE, "\n Could not "
				"allocate memory to store the res property "
				"file name %s", res_prop_file_name);
			return (-1);
		}
		strcpy(res_prop_file_name, svc_fmri_file);
			debug_print(NOGET("\n Update Extn Prop File Name:"
			"extn prop,res:%s, File: %s"), r_name,
			res_prop_file_name);
	}

	svc_retry_count = retry_cnt;
	svc_retry_intvl = retry_interval;

	if (update_fmris(res_prop_file_name) != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error in processing and "
			" updating fmri");
		return (-1);
	}

	return (0);
}
//
// Return 0 on success
//	 -1 on Error
//
int
resource_info::set_cur_res_state(res_state cur_state)
{
	service_task *svc_task;
	int ret;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	cur_res_state = cur_state;

	svc_task = svc_fmri_head;

	while (svc_task != NULL) {
		//
		// Write into the respository of all the service
		//
		rep_val_handling rep_val_hdl;
		ret = rep_val_hdl.put_rep_vals(svc_task->svc_fmri,
						cur_res_state,
				    SC_DELEGATED_PG, SVC_CUR_RES_STATE_PROP);

		if (ret != 0) {
			//
			// SCMSGS
			// @explanation
			// Problem in creating memory space during resource
			// management of SMF delegated restarter
			// @user_action
			// Check the system memory usage
			//
			logger.log(LOG_ERR, MESSAGE, "\n Could not put the "
				" prop %s of svc %s, in the repository: %s",
				SVC_CUR_RES_STATE_PROP,
				svc_task->svc_fmri,
				scf_strerror((scf_error_t)ret));
			return (-1);
		}

		svc_task = svc_task->next_svc_ptr;
	}

	return (0);
}

//
// Process the request from the resource
//
// Returns res_reg_process_status code
//	RES_EXEC_SUCCESS	If execution of all services succeed
//	RES_SVC_FAILED		If execution of any one service failed
//	RES_NOT_ONLINE		If resource is not online
//	RES_ALREADY_ONLINE	If res already online
//	RES_ALREADY_OFFLINE	If resource already offline
//	RES_REPOSITORY_ERR	If error seeting prop state in repository
//
//
//

int
resource_info::process_res(res_call_type rcall_type)
{
	service_task	*svc_task;

	int64_t state_val;
	int err = 0;
	rep_val_handling rep_val_hdl;
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	debug_print(NOGET("\n In resource_info::process_res"));
	res_svc_lock.svc_count = num_fmris;
	// Get the current state of the resource.
	(void) rep_val_hdl.get_method(svc_fmri_head->svc_fmri,
	    SC_DELEGATED_PG,  SVC_CUR_RES_STATE_PROP, &state_val,
	    &err);
	cur_res_state = (res_state)state_val;
	//
	// Set some variables depending on the type of the method
	//

	//
	// If this is the boot method, ensure that cur_res_state
	// is set to RS_OFFLINE. This is required because a node
	// can go down while hosting this service and the stop
	// method was not run on the way down (panic/reboot/halt
	// vs. init 6).
	//
	if (rcall_type == RCT_BOOT) {
		set_cur_res_state(RS_OFFLINE);
		return (RES_EXEC_SUCCESS);
	}

	if (rcall_type == RCT_START) {
		//
		// To make start idempotent
		//
		if ((cur_res_state == RS_ONLINE) ||
		    (cur_res_state == RS_STARTING)) {
			return (RES_ALREADY_ONLINE);
		}
		if (set_cur_res_state(RS_STARTING) != 0) {
			return (RES_REPOSITORY_ERR);
		}

		//
		// Reset the retry_count and the retry_count_timer queue
		//
		retry_cnt_mgmt.retry_counter = 0;
		retry_cnt_mgmt.retry_timer_queue.clear();

	} else 	if (rcall_type == RCT_STOP) {
		//
		// Resource need to be in online to stop it and also idempotent
		//
		if (cur_res_state != RS_ONLINE) {
			return (RES_NOT_ONLINE);
		}
		//
		// Reset the flag, since stop is going to start now
		//
		is_res_stop_called = false;
		if (set_cur_res_state(RS_STOPPING) != 0) {
			return (RES_REPOSITORY_ERR);
		}
	}

	//
	// Reset the exec flag before starting the res execution
	//
	is_svc_exec_failed = false;

	// Put all the service fmri's from the service list of the
	// resource into the thread pool queue so that they get
	// executed as threads becomes available
	svc_task = svc_fmri_head;

	while (svc_task != NULL) {
		//
		// Create a new service event task object and put it
		// into the thread pool queue of svc_task for further
		// execution
		//
		svc_event_task *svc_ev_task = new svc_event_task();

		//
		// Set the type of resource method call and the required
		// variable settings
		//
		svc_ev_task->set_res_method_type = rcall_type;

		//
		// Set the service task
		//
		svc_ev_task->svc_ptr = svc_task;

		//
		// Set the svc event operation type to RES
		//
		svc_ev_task->cur_et_op_type = SVC_ET_OP_RES;

		if (rcall_type == RCT_START) {
			svc_ev_task->svc_ptr->is_svc_restart = false;
		}


		//
		// Put the service fmri object into the threadpool queue of
		// its own,
		// which inturn takes the necessary action on that svc
		// fmri in the excute_service method of that particular
		// service object
		//
		PTHREAD_MTX_LOCK(svc_task->svc_fmri_thread_pool_lock);
		svc_task->svc_fmri_thread_pool.
			defer_processing(svc_ev_task);
		PTHREAD_MTX_UNLOCK(svc_task->svc_fmri_thread_pool_lock);

		svc_task = svc_task->next_svc_ptr;
	}
	//
	// Wait till all the services under this resource finishes it work
	//
	PTHREAD_MTX_LOCK(res_svc_lock.res_exec_lock);
	//
	// Sometimes cond wait wakes up for spurious wakeups with
	// returning 0.
	//
	err = 0;
	while ((res_svc_lock.svc_count != 0) && (err == 0)) {
		debug_print(NOGET("\n**** Cond Waiting ****"));
		err = pthread_cond_wait(&res_svc_lock.res_exec_cond,
				&res_svc_lock.res_exec_lock);
	}
	if (err != 0) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		logger.log(LOG_ERR, MESSAGE, "\n Error while waiting on"
			"res_exec_cond:%s", strerror(err));
		PTHREAD_MTX_UNLOCK(res_svc_lock.res_exec_lock);
		abort();
	}
	PTHREAD_MTX_UNLOCK(res_svc_lock.res_exec_lock);

	//
	// Check if any services failed, if yes, return error 1
	//
	if (is_svc_exec_failed) {
		debug_print(NOGET("\n Resource %s failed in "\
		"executing one or more services into online/offline state"),
		res_name);
		//
		// Failed execution in one or all the services
		//
		switch (cur_res_state) {
		case RS_STARTING:
			if (set_cur_res_state(RS_START_FAILED) != 0) {
				return (RES_REPOSITORY_ERR);
			}
			break;
		case RS_STOPPING:
			if (set_cur_res_state(RS_STOP_FAILED) != 0) {
				return (RES_REPOSITORY_ERR);
			}
			break;
		default:
			break;
		}

		return (RES_EXEC_FAILED);
	} else {
		//
		// Suuccessfull execution of all the services
		//
		switch (cur_res_state) {
		case RS_STARTING:
			if (set_cur_res_state(RS_ONLINE) != 0) {
				return (RES_REPOSITORY_ERR);
			}
			break;
		case RS_STOPPING:
			if (set_cur_res_state(RS_OFFLINE) != 0) {
				return (RES_REPOSITORY_ERR);
			}
			break;
		default:
			break;
		}
		return (RES_EXEC_SUCCESS);
	}
}

char*
resource_info::get_res_name()
{
	return (res_name);
}

char*
resource_info::get_rg_name()
{
	return (rg_name);
}
