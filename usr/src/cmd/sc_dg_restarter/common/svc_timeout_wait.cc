//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)svc_timeout_wait.cc	1.6	09/02/26 SMI"

#include "svc_timeout_wait.h"

#include <sys/cl_assert.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <signal.h>
#include <memory>
#include "sc_dg_error.h"

svc_timeout_wait* svc_timeout_wait::the_svc_timeout = NULL;
pthread_mutex_t svc_timeout_wait::create_lock = PTHREAD_MUTEX_INITIALIZER;

svc_timeout_wait&
svc_timeout_wait::instance()
{
	if (the_svc_timeout == NULL) {
		create();
	}

	return (*the_svc_timeout);
}

void
svc_timeout_wait::create()
{
	// allow only one thread at a time to attempt to create
	// the throttler in order to avoid creating multiple throttlers
	// in a race condition.
	CL_PANIC(pthread_mutex_lock(&create_lock) == 0);
	if (the_svc_timeout == NULL) {
		the_svc_timeout = new svc_timeout_wait();
	}
	CL_PANIC(pthread_mutex_unlock(&create_lock) == 0);
}


//
// Wrapper function for thread creation.
//
// Defined with "C" linkage because it's called by a C function
// (pthread_create).
//
extern "C" void *svc_thread_wrapper(void *)
{
	svc_timeout_wait::instance().timeout_waits();
	return (NULL);
}

svc_timeout_wait::svc_timeout_wait()
{
	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	(void) pthread_mutex_init(&lock, NULL);
	(void) pthread_cond_init(&cond, NULL);

	//
	// Create the timeout thread
	//
	int ret;
	if ((ret = pthread_create(&wait_thread, NULL,
	    svc_thread_wrapper, NULL)) != 0) {
		char *err_str = strerror(ret);

		debug_print(NOGET("svc_timeout pthread_create: %s"),
			    err_str);
		exit(ret);
	}
}

//
// timeout_waits()
//
// This method performs the timeout logic. It keeps a priority_queue
// of timeout_entry objects, ordered by their restart times, with the
// next one to restart first in the queue. The thread sleeps on a condition
// variable until a new timeout entry is added to the queue, or until
// it's time to finish the timeout of the first entry on the queue.
// A timeout is completed by calling start() on the pmf_contract pointer.
//
void
svc_timeout_wait::timeout_waits()
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	while (true) {
		timestruc_t timeout;
		int err;

		// If the queue is empty, do a normal cond_wait.
		if (pq.empty()) {
			err = pthread_cond_wait(&cond, &lock);
		} else {
			// The thread is not empty, so wait until it's time
			// to finish the timeout for the top entry.
			timeout.tv_sec = pq.top().restart_time;
			timeout.tv_nsec = 0;
			err = pthread_cond_timedwait(&cond, &lock, &timeout);
		}

		if (err != 0 && err != ETIMEDOUT) {
			debug_print(NOGET("svc_timeout pthread_cond_wait:%s"),
				strerror(err));
		}

		//
		// Here we're holding the lock again (after the return from
		// pthread_cond_wait or pthread_cond_timedwait)
		//
		// Either we timed out and it's time to check the top
		// entry, or a new entry was added to the queue. We don't
		// bother distinguishing the two cases.
		//

		// Restart all processes that are done waiting
		// We can't handle a failure from the time() call here.
		time_t cur_time = time(NULL);
		CL_PANIC(cur_time != -1);

		while (!pq.empty() && pq.top().restart_time <= cur_time) {
			//
			// If it timesout, kill the process
			//
			if ((kill(pq.top().exec_meth_pid, SIGKILL))
			    != 0) {
				debug_print(NOGET("Could not kill "
				"the timedout process: %s"), strerror(errno));
			} else {
				debug_print(NOGET("Timeout kill "
					"the timedout process:%d"),
					pq.top().exec_meth_pid);
			}
			pq.pop();
		}
	}
}

int
svc_timeout_wait::wait(service_task *svc_ptr, time_t time_in)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	os::sc_syslog_msg logger(SC_SYSLOG_SMF_DR_TAG, "", NULL);

	// Add this timeout entry to the priority queue
	try {
		time_t cur_time = time(NULL);
		if (cur_time == -1) {
			int err = errno;
			debug_print(NOGET("\n time in svc wait: "
				"%s"), strerror(err));
			return (err);
		}
		pq.push(timeout_entry(svc_ptr, time(NULL) + time_in,
		    svc_ptr->exec_mthd_pid));
	// CSTYLED
	} catch (std::bad_alloc&) {
		return (ENOMEM);
	}

	// signal the timeout thread in case this new entry should wait
	// less time than any of the current entries
	(void) pthread_cond_broadcast(&cond);
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (0);
}

bool
// CSTYLED
operator>(const timeout_entry& lhs, const timeout_entry& rhs)
{
	return (lhs.restart_time > rhs.restart_time);
}


timeout_entry::timeout_entry(service_task *svc_ptr, time_t time, int pid)
	: svc_task(svc_ptr), restart_time(time), exec_meth_pid(pid)
{
}
