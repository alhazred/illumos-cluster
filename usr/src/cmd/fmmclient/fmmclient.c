/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)fmmclient.c	1.2	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <poll.h>
#include <errno.h>

#include <saf_clm.h>


void
clusterNodeGet(SaInvocationT invocation,
	SaClmClusterNodeT *clusterNode,
	SaErrorT error)
{
	(void) printf("ClusterNodeGet callback\n");
	(void) printf("\terror %d\n", error);
	if ((error == SA_OK) && (clusterNode != NULL)) {
		(void) printf("\tinvocation %llu\n", invocation);
		(void) printf("\tnodeId %d\n", clusterNode->nodeId);
		(void) printf("\tnodeAddress %s\n",
		    clusterNode->nodeAddress.value);
		(void) printf("\tclusterName %s\n",
		    clusterNode->clusterName.value);
		(void) printf("\tmember %s\n",
		    (clusterNode->member)?"YES":"NO");
		(void) printf("\tbootTimestamp %lld\n",
		    clusterNode->bootTimestamp);
		(void) printf("\tinitialViewNumber %llu\n",
		    clusterNode->initialViewNumber);
	}
}


void
clusterTrackStart(SaClmClusterNotificationT *notificationBuffer,
	SaUint32T numberOfItems,
	SaUint32T numberOfMembers,
	SaUint64T viewNumber,
	SaErrorT error)
{
	unsigned int i = 0;

	(void) printf("clusterTrackStart callback\n");
	(void) printf("\terror %d\n", error);
	if (error != SA_OK)
		return;


	(void) printf("\tnumberOfItems %d\n", numberOfItems);
	(void) printf("\tnumberOfMembers %d\n", numberOfMembers);
	(void) printf("\tviewNumber %llu\n", viewNumber);
	for (i = 0; i < numberOfItems; i++) {
		(void) printf("\tchange %d\n",
		    notificationBuffer[i].clusterChanges);
		(void) printf("\t\tnodeId %d\n",
		    notificationBuffer[i].clusterNode.nodeId);
		(void) printf("\t\tnodeAddress %s\n",
		    notificationBuffer[i].clusterNode.nodeAddress.value);
		(void) printf("\t\tclusterName %s\n",
		    notificationBuffer[i].clusterNode.clusterName.value);
		(void) printf("\t\tmember %s\n",
		    (notificationBuffer[i].clusterNode.member)?"YES":"NO");
		(void) printf("\t\tbootTimestamp %lld\n",
		    notificationBuffer[i].clusterNode.bootTimestamp);
		(void) printf("\t\tinitialViewNumber %llu\n",
		    notificationBuffer[i].clusterNode.initialViewNumber);
	}
}


void
main(void)
{
	SaErrorT result = SA_OK;
	SaVersionT version;
	SaClmCallbacksT callbacks;
	SaClmHandleT handle;
	SaSelectionObjectT selectionObject;
	SaClmClusterNodeT clusterNode;
	SaTimeT timeout = 1000000000; /* 1 s */
	SaClmClusterNotificationT *notificationBuffer;
	SaUint32T numberOfItems = 3;
	boolean_t go_on = B_TRUE;

	struct pollfd poll_fd;

	callbacks.saClmClusterNodeGetCallback = clusterNodeGet;
	callbacks.saClmClusterTrackCallback = clusterTrackStart;
	version.releaseCode = 'A';
	version.major = 0x01;
	version.minor = 0xff;

	result = saClmInitialize(&handle, &callbacks, &version);
	(void) printf("saClmInitialize returned %d\n", result);

	result = saClmClusterNodeGet(handle, 3, timeout, &clusterNode);
	(void) printf("saClmClusterNodeGet returned %d\n", result);

	result = saClmClusterNodeGet(handle, 4, timeout, &clusterNode);
	(void) printf("saClmClusterNodeGet returned %d\n", result);

	notificationBuffer = malloc(numberOfItems
	    * sizeof (SaClmClusterNotificationT));
	result = saClmClusterTrackStart(handle, SA_TRACK_CURRENT,
	    notificationBuffer, numberOfItems);
	(void) printf("saClmClusterTrackStart returned %d\n", result);

	result = saClmClusterTrackStart(handle, SA_TRACK_CHANGES,
	    notificationBuffer, numberOfItems);
	(void) printf("saClmClusterTrackStart returned %d\n", result);

	result = saClmSelectionObjectGet(handle, &selectionObject);
	(void) printf("saClmSelectionObject returned %d\n", result);

	while (go_on) {
		poll_fd.fd = (int)selectionObject;
		poll_fd.events = POLLIN;


		switch (poll(&poll_fd, 1, -1)) {
		case -1:
			(void) printf("polling error %s\n",
			    strerror(errno)); /*lint !e746 */
			go_on = B_FALSE;
			break;
		case 0:
			break;
		case 1:
			if ((!(poll_fd.revents & POLLIN)) ||
			    (poll_fd.revents & POLLHUP) ||
			    (poll_fd.revents & POLLERR) ||
			    (poll_fd.revents & POLLNVAL)) {
				(void) printf("polling error\n");
				go_on = B_FALSE;
				break;
			}

			result = saClmDispatch(handle, SA_DISPATCH_ALL);
			(void) printf("saClmDispatch returned %d\n", result);
			break;
		default:
			break;
		}

	}

	result = saClmClusterTrackStop(handle);
	(void) printf("saClusterTrackStop returned %d\n", result);
	result = saClmFinalize(handle);
	(void) printf("saClmFinalize returned %d\n", result);

	result = saClmFinalize(handle);
	(void) printf("saClmFinalize returned %d\n", result);

	exit(0);
}
