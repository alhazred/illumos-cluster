/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DAM_H
#define	_DAM_H

#pragma ident	"@(#)dam.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#define	DAM_SOCKET_NAME		"/var/cluster/run/dam_fifo"

typedef enum dam_cmd {
	DAM_MONITOR = 1,
	DAM_UNMONITOR = 2,
	DAM_STATUS = 3
} dam_cmd;

typedef enum dam_error {
	DAM_OK = 0,
	DAM_ENOTMONITORED = 1,
	DAM_ESYSERR = 2,
	DAM_ENOMEM = 3,
	DAM_ENODAEMON = 4,
	DAM_ETRUNC = 5,
	DAM_EPATHINVAL = 6,
	DAM_EINIT = 7,
	DAM_ENOINIT = 8,
	DAM_EFENCED = 9,
	DAM_ETIMEOUT = 10,
	DAM_EINVAL = 11
} dam_error;

typedef	struct dam_cmd_msg
{
	int size;
	int command;
	char *path;
} dam_cmd_msg;

typedef	struct dam_reply_msg
{
	int size;
	int result;
	int command;
	char *msg;
} dam_reply_msg;


#ifdef __cplusplus
}
#endif

#endif /* _DAM_H */
