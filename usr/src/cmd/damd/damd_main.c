/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)damd_main.c	1.4	08/05/20 SMI"

#include <unistd.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>
#include <errno.h>
#include <stdarg.h>
#include <syslog.h>
#include <string.h>
#include <pthread.h>

#include <sys/st_failfast.h>
#include <scxcfg/scxcfg.h>
#include "dam.h"
#include "damd.h"

#include <rgm/sczones.h>

void dam_log(int const pri, char const * const fmt, ...);


#define	DEFAULT_LOG_TAG	"damd"
static char *logTag = DEFAULT_LOG_TAG;
static int logLevel = LOG_NOTICE;

#define	MAX_MSG_SIZE 1024
static char message[MAX_MSG_SIZE];

scxcfg Cfg;

pthread_t cmdThread;


void
dam_log_set_level(int level)
{
	logLevel = level;
}

void
dam_log_set_tag(char *tag)
{
	if (!tag)
		return;
	/* only first call initialize tag */
	if (strcmp(logTag, DEFAULT_LOG_TAG) == 0)
		logTag = strdup(tag);
}

void
dam_log(int const pri, char const * const fmt, ...)
{
	va_list ap;
	if (pri > logLevel)
		return;
	va_start(ap, fmt);
	openlog(logTag, LOG_CONS | LOG_PID, LOG_DAEMON);
	vsyslog(pri, fmt, ap);
	closelog();
	va_end(ap);
}

void
dam_monitor_thread(void)
{
	while (1) {
		sleep(10);
		dam_log(LOG_DEBUG, "dam_monitor_thread");
	}
}


void
dam_cmd_thread(void)
{
	int rc;
	dam_log(LOG_DEBUG, "dam_cmd_thread, calling dam_svc_wait");
	rc = dam_svc_wait();
}

/*
 * daemonize myself
 */
static void
daemonize(void)
{
	pid_t pid;
	extern int errno;

	pid = fork();
	if (pid < 0) {
		(void) fprintf(stderr, "fork: %s", strerror(errno));
		exit(1);
	}
	if (pid > 0) {
		exit(0);
	}
	/*
	 * set process group
	 */
	pid = setsid();
	if (pid == (pid_t)-1) {
		(void) fprintf(stderr, "setsid: %s", strerror(errno));
		exit(1);
	}

	(void) signal(SIGCHLD, SIG_IGN);
	(void) signal(SIGHUP, SIG_IGN);

	pid = fork();
	if (pid < 0) {
		(void) fprintf(stderr, "fork: %s", strerror(errno));
	}
	if (pid > 0) {
		exit(0);
	}

	(void) chdir("/");

	(void) close(STDIN_FILENO);
	(void) close(STDOUT_FILENO);
	(void) close(STDERR_FILENO);
}

void
sigint_handler(int sig)
{
	st_ff_disarm();
	scxcfg_close(&Cfg);
	exit(1);
}




static int
start_threads()
{
	int rc;
#if 0
	if ((rc = pthread_create(&cmdThread, (pthread_attr_t *)0,
	    (void *(*)(void *)) dam_cmd_thread, 0)) != 0) {
		dam_log(LOG_ERR, "Cannot create  thread (cmd): %s",
		    strerror(rc));
		return (DAM_ESYSERR);
	}

	if ((rc = pthread_create(&monitorThread, (pthread_attr_t *)0,
	    (void *(*)(void *)) dam_monitor_thread, 0)) != 0) {
		dam_log(LOG_ERR, "Cannot create thread (monitor): %s",
		    strrcor(rc));
	}
#endif
	return (DAM_OK);
}



int
main(int argc, char *argv[])
{
	int rc = FCFG_OK;
	int debug_mode = 0;
	int num = 1;
	scxcfg_error error;

#ifndef linux
	if (sc_zonescheck() != 0)
		return (1);
#endif

	dam_log_set_tag("Cluster.damd.daemon");
	/*
	 * hidden option
	 */
	if (argc > 1) {
		if (strcmp(argv[1], "-d") == 0) {
			dam_log_set_level(LOG_DEBUG);
			debug_mode = 1;
		}
	}
	if (!debug_mode) {
		daemonize();
	}
	signal(SIGINT, sigint_handler);
	signal(SIGTERM, sigint_handler);
	rc = scxcfg_open(&Cfg, &error);
	if (rc != FCFG_OK) {
		dam_log(LOG_ERR, "Cannot open farm config: %s",
		    scxcfg_error2string(&error));
		exit(1);
	}
	/*
	 * Ignore the SIGPIPE signal
	 */
	(void) sigset(SIGPIPE, SIG_IGN);

	if (st_ff_arm("damd") != 0) {
		dam_log(LOG_ERR, "fatal: could not arm failfast");
		exit(1);
	}

	/*
	 * Start other service threads XXX
	 */
	start_threads();

	/*
	 * Create the damMonitor
	 */
	rc = create_dam_monitor(&Cfg);
	if (rc != DAM_OK) {
		dam_log(LOG_ERR, "start create DAM monitor object: %d",
		    rc);
		exit(1);
	}
	/*
	 * Read the config
	 */
	rc = readConfig(&Cfg);
	if (rc != DAM_OK) {
		dam_log(LOG_ERR, "DAMD cannot read config: %d",
		    rc);
		exit(1);
	}

	/*
	 * Start command thread in the main thread
	 */
	dam_cmd_thread();

	scxcfg_close(&Cfg);

	exit(0);
}
