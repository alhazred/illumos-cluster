/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)damd_fs_sunos.cc	1.3	08/05/20 SMI"


#include <stdio.h>
#include <errno.h>
#include <sys/mnttab.h>

#include "dam.h"
#include "damd.h"
#include "damd_cfg.h"

//
// SunOS specific definition.
//
#define	FSTAB "/etc/vfstab"
#define	MTAB "/etc/mnttab"


static int
remote2local(Fdevice &fdev)
{
	FILE *mt;
	struct mnttab entry;
	int rstatus = DAM_OK;
	int rc;
	mt = fopen(MTAB, "r");
	if (mt == NULL) {
		dam_log(LOG_ERR, "remote2local fopen: %s",
		    strerror(errno));
		return (DAM_EINVAL);
	}
	char *cmp_str = (char *)malloc(strlen(fdev.server.c_str())+
	    strlen(fdev.path.c_str()) + 5);
	sprintf(cmp_str, "%s:%s", fdev.server.c_str(), fdev.path.c_str());
	bool found = false;
	while (true) {
		rc = getmntent(mt, &entry);
		if (rc == -1)
			break;
		if (rc > 0)
			continue;
		if (strcmp(entry.mnt_special, cmp_str) == 0) {
			fdev.mntPt = string(entry.mnt_mountp);
			found = true;
			printf("found: mntpt = %s \n", fdev.mntPt.c_str());
		}
	}
	fclose(mt);
	return (rstatus);
}


int
findMountPoint(std::list<Fdevice*>Flist)
{
	std::list<Fdevice*>::const_iterator it;
	int rc;
	int rstatus = DAM_OK;

	for (it = FdeviceList.begin(); it != FdeviceList.end(); it++) {
		rc = remote2local(*(*it));
		if (rc != DAM_OK) {
			dam_log(LOG_ERR, "cannot find mount point for: %s:%s",
			    (*it)->server.c_str(), (*it)->path.c_str());
			// XXX remove from list ?
			continue;
		}
	}
	return (rstatus);
}
