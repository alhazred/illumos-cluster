/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)damd_monitor.cc	1.4	08/05/20 SMI"

#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <strings.h>
#define	PORTMAP
#include <rpc/rpc.h>
#include <rpc/pmap_clnt.h>

#include <scxcfg/rpc/scxcfg_rpc.h>
#include <scxcfg_int.h>
#include "damd_monitor.h"

extern "C"
{
	int create_dam_monitor(scxcfg_t cfg);
	int dam_do_monitor(const char *);
	int dam_do_unmonitor(const char *);
}

//
// "The" DAM monitor object pointer.
//
damMonitor *theDamMonitor;

//
// TIMEOUT_TO_KILL: in seconds, time to wait when the access to
// fencing device is blocked and when no answer about the fencing state
// can be obtained from server nodes.
//
const int TIMEOUT_TO_KILL = 300;

//
// MAIN_ACCESS_INTERVAL: time in seconds between each access for a
// file system.
//
const int MAIN_ACCESS_INTERVAL = 5;

//
// Name format of the file accessed by this DAM daemon
//
const char *ACCESSFILE = ".cluster.%s.%d";

//
// Start monitoring after this time.
//
int BootDelay = 50;
//
// create_dam_monitor:
// Extern "C" function to allocate the singleton
// class damMonitor.
//
int
create_dam_monitor(scxcfg_t cfg)
{
	int rc;
	if (theDamMonitor)
		return (DAM_EINIT);

	theDamMonitor = new damMonitor();
	if (theDamMonitor == NULL) {
		return (DAM_ENOMEM);
	}
	rc = theDamMonitor->init(cfg);
	if (rc != DAM_OK) {
		delete theDamMonitor;
		theDamMonitor = NULL;
	}
	return (rc);
}

//
// extern "C" function for starting monitoring a file system.
//
int
dam_do_monitor(const char *path)
{
	int rc;
	if (!theDamMonitor)
		return (DAM_ENOINIT);
	rc = theDamMonitor->startMonitor(path);
	return (rc);
}

//
// extern "C" function for un-monitoring a file system.
//
int
dam_do_unmonitor(const char *path)
{
	int rc;
	if (!theDamMonitor)
		return (DAM_ENOINIT);
	rc = theDamMonitor->stopMonitor(path);
	return (rc);
}

//
// Helper function. Used for timeouts.
//
static uint64_t
getTime()
{
	struct timeval tp;
	(void) gettimeofday(&tp, NULL);
	return ((uint64_t)tp.tv_sec);
}

//
// monitoredFS constructor.
//
monitoredFS::monitoredFS(const char *p, const char *f)
{
	Path = string(p);
	fileName = Path + "/" + string(f);
	pthread_mutex_init(&mutex, NULL);
	writeFileError = DAM_AERROR_OK;
	writeDirError = DAM_AERROR_OK;
	readFileError = DAM_AERROR_OK;
	mainAccessInterval = MAIN_ACCESS_INTERVAL;
	timeoutToKill = TIMEOUT_TO_KILL;
	State = DAM_STATE_UNMONITORED;
}

//
// monitoredFS destructor.
//
monitoredFS::~monitoredFS()
{
}

//
// monitoredFS::init().
// Create thread that will do the actal accesses to the file system.
//
int
monitoredFS::init()
{
	dam_log(LOG_DEBUG, "monitoredFS::init: %s", Path.c_str());
	int rc = DAM_OK;
	if ((rc = pthread_create(&monitorThread, (pthread_attr_t *)0,
	    (void *(*)(void *))run, (void *)this)) != 0) {
		dam_log(LOG_ERR, "monitoredFS: create (monitorThread): %s",
		    strerror(rc));
		return (DAM_ESYSERR);
	}
	return (rc);
}

//
// monitoredFS::stop().
// Tells the monitorThread to stop monitoring.
//
int
monitoredFS::stop()
{
	dam_log(LOG_DEBUG, "monitoredFS::stop: %s", Path.c_str());
	int rc = DAM_OK;

	setState(DAM_STATE_STOPPING);
	if ((rc = pthread_cancel(monitorThread)) != 0) {
		dam_log(LOG_ERR, "monitoredFS: cancel (monitorThread): %s",
		    strerror(rc));
		return (DAM_ESYSERR);
	}
	if ((rc =  pthread_join(monitorThread, NULL)) != 0) {
		dam_log(LOG_ERR, "monitoredFS: join (monitorThread): %s",
		    strerror(rc));
		return (DAM_ESYSERR);
	}
	return (rc);
}

//
// monitoredFS::run().
// Main routine of the monitor thread.
//
void
monitoredFS::run(monitoredFS *m)
{
	int rc;
	dam_log(LOG_DEBUG, "monitoredFS thread start: %s",
	    m->Path.c_str());
	while (m->State != DAM_STATE_STOPPING) {
		rc = m->tryAccess();
		if (rc != DAM_OK) {
			dam_log(LOG_ERR, "monitoredFS tryAccess() failed:"
			    "aborting thread for: %s", m->Path.c_str());
			return;
		}
		sleep(m->mainAccessInterval);
	}
}

//
// errno2dam(), helper function
// Categorize errno system errors to internal DAM access errors.
//
static int
errno2dam(int err)
{
	int rc;
	switch (err) {
	case 0:
		rc = DAM_AERROR_OK;
		break;
	case EINTR:
	case EAGAIN:
		rc = DAM_AERROR_RETRY;
		break;
	case ENOSPC:
		rc = DAM_AERROR_RETRY;
		break;
	case EACCES:
		rc = DAM_AERROR_PERM;
		break;
	case EIO:
		rc = DAM_AERROR_STALE;
		break;
	case EROFS:
		rc = DAM_AERROR_ROFS;
		break;
	default:
		rc = DAM_AERROR_OTHER;
		break;
	}
	return (rc);
}

//
// monitoredFS::tryAccess() does the actual accesses to the file
// system. Setting the  internal state according to the
// operation results.
//
int
monitoredFS::tryAccess()
{
	int rc, fd;
	int fileMode = O_RDWR;
	monState_t state;

	dam_log(LOG_DEBUG, "monitoredFS::tryAccess: %s",
	    Path.c_str());
	setState(DAM_STATE_TRYACCESS);

	fd = open(fileName.c_str(), fileMode);
	if (fd < 0) {
		if (errno == ENOENT) {
			int mode = fileMode | O_CREAT;
			fd = open(fileName.c_str(), mode, 0600);
			if (fd < 0) {
				writeDirError = errno2dam(errno);
				dam_log(LOG_DEBUG, "DAM:creat err %d, %s %d",
					errno, strerror(errno), writeDirError);
			} else {
				writeDirError = DAM_AERROR_OK;
			}
		} else {
			readFileError = errno2dam(errno);
			dam_log(LOG_ERR, "DAM open failed: %s.",
			    strerror(errno));
		}
	} else {
		readFileError = DAM_AERROR_OK;
	}

	if (fd > 0) {
		char buf [4096];
		rc = read(fd, buf, sizeof (buf));
		if (rc < 0) {
			readFileError = errno2dam(errno);
			dam_log(LOG_ERR, "DAM read failed: %s.",
				strerror(errno));
		} else {
			readFileError = DAM_AERROR_OK;
		}
		rc = write(fd, buf, 100);
		if (rc < 0) {
			writeFileError = errno2dam(errno);
			dam_log(LOG_ERR, "DAM read failed: %s.",
				strerror(errno));
		} else {
			writeFileError = DAM_AERROR_OK;
		}

		(void) close(fd);

		rc = unlink(fileName.c_str());
		if (rc < 0) {
			writeDirError = errno2dam(errno);
			dam_log(LOG_ERR, "DAM unlink failed: %d %s.",
			    errno, strerror(errno));
		} else {
			writeDirError = DAM_AERROR_OK;
		}
	}
	if (writeDirError != DAM_AERROR_OK) {
#ifdef linux
		if (writeDirError == DAM_AERROR_PERM)
			state = DAM_STATE_FENCED;
#else
		if (writeDirError == DAM_AERROR_ROFS)
			state = DAM_STATE_FENCED;
#endif
		else
			state = DAM_STATE_TRANSIENT_ERROR;
	} else if (readFileError != DAM_AERROR_OK)
		state = DAM_STATE_TRANSIENT_ERROR;
	else if (writeFileError != DAM_AERROR_OK)
		state = DAM_STATE_TRANSIENT_ERROR;
	else
		state = DAM_STATE_MONITORED_OK;

	setState(state);
	return (DAM_OK);
}


monState_t
monitoredFS::getState()
{
	return (State);
}

void
monitoredFS::setState(monState_t s)
{
	lock();
	State = s;
	unlock();
}

const char *
monitoredFS::getPath()
{
	return (Path.c_str());
}

void
monitoredFS::lock()
{
	pthread_mutex_lock(&mutex);
}

void
monitoredFS::unlock()
{
	pthread_mutex_unlock(&mutex);
}


//
// damMonitor implementation
//


//
// damMonitor constructor.
//
damMonitor::damMonitor()
{
	pthread_mutex_init(&mutex, NULL);
	mainTimeout = 0;
}

//
// damMonitor destructor.
//
damMonitor::~damMonitor()
{
}

int
damMonitor::buildServerList(scxcfg_t cfg)
{
	f_property_t *list = NULL;
	f_property_t *v = NULL;
	int rc;
	scxcfg_nodeid_t nodeid;
	scxcfg_error error;
	char ipAddr[FCFG_MAX_LEN];
	char nodeName[FCFG_MAX_LEN];
	char prop[32];
	int rstatus = DAM_OK;
	int num = 0;

	rc = scxcfg_getlistproperty_value(cfg, NULL, "farm.servers", &list,
	    &num, &error);
	if (rc != FCFG_OK || num == 0) {
		//
		// Fatal error or no server nodes in config.
		//
		return (DAM_ESYSERR);
	}
	for (v = list; v != NULL; v = v->next) {
		nodeid = atoi(v->value);
		rc = scxcfg_get_ipaddress(cfg, nodeid, ipAddr, &error);
		if (rc != FCFG_OK) {
			dam_log(LOG_NOTICE, "get ip addr for node: %d, err %d",
				nodeid, rc);
			continue;
		}
		nodeName[0] = 0;
		Server *serv = new Server(nodeName, ipAddr);
		if (!serv) {
			rstatus = DAM_ESYSERR;
			continue;
		}
		serversList.push_back(serv);
	}
	if (list)
		scxcfg_freelist(list);
	return (rstatus);
}

static void
run_monitorAll(damMonitor *dm)
{
	dm->monitorAll(dm);
}

int
damMonitor::init(scxcfg_t cfg)
{
	int rc;
	scxcfg_error error;
	char value[FCFG_MAX_LEN];

	if ((rc = scxcfg_get_local_nodeid(&localNodeid, &error)) != FCFG_OK) {
		dam_log(LOG_ERR, "Cannot get local nodeid: %d",
		    rc);
		return (DAM_ESYSERR);
	}
	if ((rc = scxcfg_get_local_nodename(cfg, value, &error)) != FCFG_OK) {
		dam_log(LOG_ERR, "Cannot get local nodename: %d",
		    rc);
		return (DAM_ESYSERR);
	}
	localNodeName = string(value);

	//
	// Construct the acces file name.
	//
	sprintf(value, ACCESSFILE, localNodeName.c_str(), localNodeid);
	accessFileName = string(value);

	//
	// Get the servers address list
	//
	if (rc = buildServerList(cfg) != DAM_OK) {
		dam_log(LOG_ERR, "Cannot get build server list: %d",
		    rc);
		return (DAM_ESYSERR);
	}
	//
	// Create the thread that will monitor all FS
	// monitoring threads.
	//
	if ((rc = pthread_create(&monitorAllThread, (pthread_attr_t *)0,
	    (void *(*)(void *))run_monitorAll, this)) != 0) {
		dam_log(LOG_ERR, "Cannot create thread (monitorAll): %s",
		    strerror(rc));
		return (DAM_ESYSERR);
	}
	return (DAM_OK);
}

static void
abortNode()
{
	dam_log(LOG_ERR, "DAMD: ABORTING NODE");
	sleep(1);
	//
	// Exit without stopping failfast.
	//
	exit(1);
}

bool
damMonitor::hasTimeoutFired()
{
	if (!mainTimeout)
		return (false);
	if (getTime() > (TIMEOUT_TO_KILL + mainTimeout))
		return (true);
	else
		return (false);
}

void
damMonitor::setMainTimeout()
{
	mainTimeout = getTime();
}


void
damMonitor::monitorAll(void *)
{
	dam_log(LOG_DEBUG, "Starting  damMonitor::monitorAll thread");
	if (!theDamMonitor) {
		dam_log(LOG_ERR, "damMonitor::monitorAll notc created");
		abort();
	}
	std::set<registeredFS>::const_iterator it;
	//
	// Do not start monitoring too early.
	//
	sleep(BootDelay);
	dam_log(LOG_DEBUG, "monitorAll, end BootDelay");
	while (1) {
		if (mainTimeout) {
			if (hasTimeoutFired()) {
				dam_log(LOG_ERR, "DAM ABORTING isolated from "
				    "fencing device and servers");
				abortNode();
			}
		}
		int count = 0;
		int problems = 0;
		it = theDamMonitor->FSList.begin();
		if (it == theDamMonitor->FSList.end()) {
			dam_log(LOG_DEBUG, "monitorAll: empty list of FS");
			goto sleep;
		}
		while (it != theDamMonitor->FSList.end()) {
			monState_t state;
			state =  (*it).FS->getState();
			dam_log(LOG_DEBUG, "monitorAll: %s : state %d",
				(*it).Path.c_str(), state);
			if (state == DAM_STATE_TRYACCESS) {
				dam_log(LOG_NOTICE, "DAM access: %s BLOCKED",
					(*it).Path.c_str());
				if (!mainTimeout) {
					setMainTimeout();
				}
				problems++;
			} else if (state == DAM_STATE_FENCED) {
				dam_log(LOG_DEBUG, "monitorAll: %s FENCED !!",
					(*it).Path.c_str());
				dam_log(LOG_ERR, "damd: node FENCED from %s",
					(*it).Path.c_str());
				abortNode();
			}
			count++;
			it++;
		}
		dam_log(LOG_DEBUG, "monitorAll: %d FS registered", count);
		if (problems) {
			(void) updateManagerStatus();
		} else {
			//
			// If access has resumed, reset timer.
			//
			if (mainTimeout) {
				dam_log(LOG_DEBUG, "monitorAll:"
				    "end of problem, rst timeout");
				mainTimeout = 0;
			}
		}
sleep:
		sleep(MAIN_ACCESS_INTERVAL);
	}
}

int
damMonitor::startMonitor(const char *path)
{
	int rc = DAM_OK;
	//
	// Check path XXX
	//

	// Is it already registered
	std::set<registeredFS>::const_iterator it;
	it = FSList.find(registeredFS(path));
	if (it != FSList.end()) {
		dam_log(LOG_DEBUG, "startMonitor: %s already registered",
		    path);
		return (DAM_EPATHINVAL);
	}
	monitoredFS *newFS = new monitoredFS(path, accessFileName.c_str());
	rc = newFS->init();
	if (rc != DAM_OK) {
		dam_log(LOG_ERR, "startMonitor: failed init new FS %s %d",
		    path, rc);
		delete newFS;
		return (rc);
	}
	registeredFS *newRegFS = new registeredFS(path);
	newRegFS->FS = newFS;
	FSList.insert(*newRegFS);
	return (rc);
}

int
damMonitor::stopMonitor(const char *path)
{
	int rc = DAM_OK;

	std::set<registeredFS>::const_iterator it;
	it = FSList.find(registeredFS(path));
	if (it == FSList.end()) {
		dam_log(LOG_DEBUG, "stopMonitor: %s not registered",
		    path);
		return (DAM_ENOTMONITORED);
	}
	rc = ((*it).FS)->stop();
	if (rc != DAM_OK) {
		dam_log(LOG_ERR, "stoptMonitor: failed end FS %s: %d",
		    (*it).Path.c_str(), rc);
		return (rc);
	}
	delete (*it).FS;
	FSList.erase(it);
	return (rc);
}


int
damMonitor::status(const char *path)
{
	int rc = DAM_OK;
	return (rc);
}


int
damMonitor::testThisNodeAlive(const char *ipAddr)
{
	struct sockaddr_in  addr;
	int rc;
	int sockfd;
	enum clnt_stat rpc_stat;
	scxcfg_input_t input;
	scxcfg_output_t output;
	int rstatus = DAM_OK;
	CLIENT *clntHandle;
	struct timeval timev;


	dam_log(LOG_DEBUG, "testThisNodeAlive: Server %s", ipAddr);
	/*
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	*/
	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(SCXCFG_PORT);
	addr.sin_addr.s_addr = inet_addr(ipAddr);
	/*
	rc = connect(sockfd, (struct sockaddr*)&addr, sizeof (addr));
	if (rc) {
		dam_log(LOG_ERR, "connect failed");
		::close(sockfd);
		return (DAM_ETIMEOUT);
	}
	*/
	timev.tv_sec = 2;
	timev.tv_usec = 0;

	clntHandle = clntudp_create(&addr, SCXCFG_SERVER,
	    SCXCFG_VERS, timev, &sockfd);
	/*
	clntHandle = clnttcp_create(&addr, SCXCFG_SERVER,
		SCXCFG_VERS, &sockfd, 0, 0);
	*/
	if (clntHandle == (CLIENT *) NULL) {
		dam_log(LOG_ERR, "tcp client rpc: cannot "
			    " create tcp rpc client with %s", ipAddr);
		::close(sockfd);
		return (DAM_ETIMEOUT);
	}

	bzero(&input, sizeof (input));
	bzero(&output, sizeof (output));
	input.name = (char *)localNodeName.c_str();
	input.node_id = (int)localNodeid;

	clnt_control(clntHandle, CLSET_TIMEOUT, (char*)&timev);

	rpc_stat = scxcfg_alive_nodeid_1(&input, &output, clntHandle);

	if (rpc_stat != RPC_SUCCESS) {
		//   clnt_perror(clntHandle, "call failed");
		perror("rpc error");

		dam_log(LOG_DEBUG, "Call alive_nodeid: Failed on %s",
			ipAddr);
		clnt_perrno(rpc_stat);
		rstatus = DAM_ETIMEOUT;
	} else {
		if (output.error == SCXCFG_OK)
			rstatus = DAM_OK;
		else
			rstatus = DAM_EFENCED;
	}
	clnt_destroy(clntHandle);
	::close(sockfd);

	return (rstatus);
}


void
damMonitor::updateManagerStatus()
{
	int rc = DAM_OK;
	dam_log(LOG_DEBUG, "updateManagerStatus");
	const char *ipaddr;

	std::list<Server*>::const_iterator it;
	for (it = serversList.begin(); it != serversList.end(); it++) {
		ipaddr = (*it)->ipAddr.c_str();
		rc = testThisNodeAlive(ipaddr);
		if (rc == DAM_ETIMEOUT) {
			//
			// Cannot reach this server node.
			// It's still unknown what needs to be
			// done on this node.
			// Remain in his state unless the timeout
			// fires.
			//
			dam_log(LOG_NOTICE, "FS blocked, server %s "
				"unreachable", ipaddr);
		} else if (rc == DAM_OK) {
			//
			// This node is still in the farm membership.
			// Don't kill it.
			// Re-arm the Shared-access timeout to its
			// initial value.
			//
			setMainTimeout();
			break;
		} else if (rc == DAM_EFENCED) {
			dam_log(LOG_ERR, "Server %s declare us fenced",
			    ipaddr);
			abortNode();
		}
	}
}

void
damMonitor::lock()
{
	pthread_mutex_lock(&mutex);
}

void
damMonitor::unlock()
{
	pthread_mutex_unlock(&mutex);
}
