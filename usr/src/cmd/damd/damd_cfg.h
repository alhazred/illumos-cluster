/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DAMD_CFG_H
#define	_DAMD_CFG_H

#pragma ident	"@(#)damd_cfg.h	1.3	08/05/20 SMI"

#include <string>
#include <list>

using std::string;

struct Fdevice
{
	Fdevice(const char *t, const char *s, const char *p) :
		type(t), server(s), path(p) {};
	string type;
	string server;
	string path;
	string mntPt;
};

//
// Global routines.
//
extern int findMountPoint(std::list<Fdevice*>Flist);

//
// Global variables.
//
extern std::list<Fdevice*>FdeviceList;

#endif /* _DAMD_CFG_H */
