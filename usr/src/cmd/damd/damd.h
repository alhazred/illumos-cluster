/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DAMD_H
#define	_DAMD_H

#pragma ident	"@(#)damd.h	1.3	08/05/20 SMI"

#include <syslog.h>
#include <scxcfg/scxcfg.h>

#ifdef __cplusplus
extern "C" {
#endif

void dam_log(int const pri, char const * const fmt, ...);
int dam_svc_wait();
int create_dam_monitor(scxcfg_t cfg);
int dam_do_monitor(const char *);
int dam_do_unmonitor(const char *);
int readConfig(scxcfg_t cfg);

#ifdef __cplusplus
}
#endif

#endif /* _DAMD_H */
