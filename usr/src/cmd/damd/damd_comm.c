/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)damd_comm.c	1.4	08/05/20 SMI"


#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

#include "dam.h"
#include "damd.h"


/*
 * Control socket related utilities. This facility provides a way to
 * communicate with the damd monitor and get response from it.
 * From the client side, all that is needed is a dam_sendmsg() call which
 * sends a message to the monitor and gets an integer response from it.
 * On the server (damd) side, it is supposed to do a
 * dam_waitsock(), which returns 0 if there is a message pending, it
 * then needs to do a dam_getmsg() to get the messages and then do
 * a dam_sendreply() to send an integer to the client.
 */


static int new_socket;		/* Client connection socket */

/*
 * dam_opensock(): Opens a UNIX domain socket on which the
 * damd waits for UNIX domain connections. The entities
 * which could potentially communicate with it are, damadm cmd
 * returns a UNIX domain socket descriptor.
 */
int
dam_opensock(char *sockname)
{
	int sd;
	int n;
	struct sockaddr_un target;

	sd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sd < 0) {
		dam_log(LOG_ERR, "Failed to create socket: %s.",
		    strerror(errno));			/*lint !e746 */
		return (-1);
	}
	bzero((char *)&target, sizeof (target));
	target.sun_family = AF_UNIX;
	(void) strncpy(target.sun_path, sockname, sizeof (target.sun_path));
	(void) unlink(target.sun_path);

	n = 1;
	(void) setsockopt(sd, SOL_SOCKET, SO_REUSEADDR,
	    (char *)&n, (int)sizeof (int));
	if (bind(sd, (struct sockaddr *)&target, (int)sizeof (target)) < 0) {
		dam_log(LOG_ERR, "Failed to bind socket to %s: %s.",
		    sockname, strerror(errno));
		(void) close(sd);
		return (-1);
	}
	(void) listen(sd, 5);
	return (sd);
}

/*
 * dam_closesock(): Closes the fault monitor control socket and
 * removes the socket file from the file system.
 * Called by the fault monitor before exiting cleanly.
 */

int
dam_closesock(int sock, char *sockname)
{
	(void) close(sock);
	(void) unlink(sockname);
	return (0);
}

/*
 * dam_waitsock(): Wait for the specified period of time on the UNIX
 * domain socket for connections to arrive. If a connection does arrive,
 * 0 is returned to the caller. If the timeout expires, -1 is returned.
 */

int
dam_waitsock(int sock, int timeout)
{
	fd_set fdset;
	struct timeval t;

	if (new_socket > 0) {		/* Something is already pending */
		return (0);
	}
	/*
	 * Suppress the lint info message. On s8u6, the usage of FD_ZERO
	 * below is correct, but lint flags it in s9.
	 * The lint error Info(792) is described as "void cast
	 * of void expression". This lint error shows up because of
	 * a change to the definition of the FD_ZERO macro in s9 to
	 * the following.
	 * #define FD_ZERO(__p) (void) memset((__p), 0, sizeof (*(__p)))
	 */
	FD_ZERO(&fdset); /*lint !e792 */
	FD_SET((uint_t)sock, &fdset);		/*lint !e737 !e713 */

	t.tv_sec = timeout;
	t.tv_usec = 0;
	dam_log(LOG_DEBUG, "dam_waitsock: select ...");
	if (select(sock+1, &fdset, NULL, NULL, &t) > 0)
{		dam_log(LOG_DEBUG, "dam_waitsock: select OK");
		return (0);
	}
	dam_log(LOG_DEBUG, "dam_waitsock: select Failed"
	    ": %s", strerror(errno));
	return (-1);
}

/*
 * dam_getmsg(): Get a message from the control socket. The caller is
 * supposed to have already done a waitsock() which returned 0 because there
 * was a connection pending on the socket. The message is returned in a
 * static buffer (thus we can handle only one active message at a time).
 */

int
dam_getmsg(int sock, char *message, int size, int *rsize)
{
	struct sockaddr_un from;
	ssize_t n;
	int fromlen;

	dam_log(LOG_DEBUG, "dam_getmsg");
	fromlen = sizeof (from);
	new_socket = accept(sock, (struct sockaddr *)&from, &fromlen);
	if (new_socket < 0) {
		dam_log(LOG_ERR,
		    "Failed to accept connection on socket: %s.",
		    strerror(errno));
		return (-1);
	}
	if ((n = recv(new_socket, message, size - 1, 0)) < 1) {
		dam_log(LOG_ERR,
		    "Failed to communicate: %s.",
		    strerror(errno));
		(void) close(new_socket);
		new_socket = -1;
		return (-1);
	}
	message[n] = 0;
	*rsize = n;
	return (DAM_OK);
}

/*
 * dam_sendreply(): Send an integer value to the client
 * which sent us the message.
 */

int
dam_sendreply(int sock, int response)
{
	ssize_t n;
	char reply_buf[256];
	dam_reply_msg msg;

	dam_log(LOG_DEBUG, "dam_sendreply");
	if (new_socket < 0) {
		return (-1);
	}
	msg.size = sizeof (dam_reply_msg);
	msg.result = response;
	msg.command = 0;
	msg.msg = NULL;

	*((dam_reply_msg*)reply_buf) = msg;

	if (n = send(new_socket, reply_buf, sizeof (dam_reply_msg), 0) < 0) {
		dam_log(LOG_ERR, "dam_sendreply send: %s.", strerror(errno));
	}
	(void) close(new_socket);
	new_socket = -1;
	return ((int)n);
}


int
handle_cmd(const char *msg, int rsize)
{
	const char *p = msg;
	const char *path;
	dam_cmd_msg *cmd;
	int rc = DAM_OK;

	cmd = (dam_cmd_msg *)msg;
	if (rsize < sizeof (dam_cmd_msg) + 2)
		return (DAM_EPATHINVAL);
	path = (const char*)(cmd + 1);
	switch (cmd->command) {
	case DAM_MONITOR:
		rc = dam_do_monitor(path);
		break;
	case DAM_UNMONITOR:
		rc = dam_do_unmonitor(path);
		break;
	}
	return (rc);
}

/*
 * dam_svc_wait()
 * Wait for a dam command.
 */
int
dam_svc_wait()
{
	fd_set	fdset;
	char msg[2048];
	int unix_sock;
	int rc = DAM_OK;
	int response;
	int rsize;

	dam_log(LOG_DEBUG, "dam_svc_wait");
	unix_sock = dam_opensock(DAM_SOCKET_NAME);
	if (unix_sock < 0) {
		dam_log(LOG_ERR, "Socket creation failed: %s.",
		    strerror(errno));
		rc = DAM_ESYSERR;
		goto finish;
	}
wait_again:
	FD_ZERO(&fdset);	/*lint !e792 */
	FD_SET((uint32_t)unix_sock, &fdset); /*lint !e737 !e713 */

	if (select(unix_sock + 1, &fdset, NULL, NULL, NULL) > 0) {
		dam_log(LOG_DEBUG, "dam_svc_wait: select OK");
		rc = dam_getmsg(unix_sock, msg, (int)sizeof (msg),
		    &rsize);
		if (rc != DAM_OK) {
			dam_log(LOG_ERR, "dam_getmsg error: rc %d",
			    rc);
			goto finish;
		}
		if (rsize < sizeof (dam_cmd_msg)) {
			dam_log(LOG_NOTICE, "dam_svc_wait, command too small");
			goto wait_again;
		}
		response = handle_cmd(msg, rsize);

		(void) dam_sendreply(unix_sock, response);

		goto wait_again;
	} else if (errno == EINTR) {
		dam_log(LOG_DEBUG, "select interrupted: %s.");
		goto wait_again;
	} else {
		dam_log(LOG_ERR, "select failed: %s.",
		    strerror(errno));
		rc = DAM_ESYSERR;
	}
finish:
	if (rc != DAM_OK) {
		dam_log(LOG_NOTICE, "dam_svc_wait exiting with error: rc %d",
		    rc);
	}
	return (rc);
}
