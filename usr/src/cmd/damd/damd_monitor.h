/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DAM_MONITOR_H
#define	_DAM_MONITOR_H

#pragma ident	"@(#)damd_monitor.h	1.3	08/05/20 SMI"

#include <string>
#include <list>
#include <set>
#include <pthread.h>
#include <scxcfg/scxcfg.h>

#include "damd.h"
#include "dam.h"

using std::string;

//
// Monitored File system  internal state.
//
typedef enum mon_state {
	DAM_STATE_UNMONITORED = 1,
	DAM_STATE_TRYACCESS = 2,
	DAM_STATE_UNKOWM_ERROR = 3,
	DAM_STATE_TRANSIENT_ERROR = 4,
	DAM_STATE_ACCESSTIMEOUT = 5,
	DAM_STATE_MONITORED_OK = 6,
	DAM_STATE_FENCED = 7,
	DAM_STATE_MANAGER_UNREACHABLE = 8,
	DAM_STATE_DATA_SERVER_UNREACHABLE = 10,
	DAM_STATE_CONFIG_ERROR = 11,
	DAM_STATE_STOPPING = 12
} monState_t;

//
// Internal error values for access errors.
//
typedef enum accessError {
	DAM_AERROR_OK = 0,
	DAM_AERROR_STALE = 1,
	DAM_AERROR_PERM = 2,
	DAM_AERROR_ROFS = 4,
	DAM_AERROR_RETRY = 5,
	DAM_AERROR_OTHER = 6
} accessError_t;


extern const int TIMEOUT_TO_KILL;
extern const int MAIN_ACCESS_INTERVAL;

class monitoredFS
{
public:
	monitoredFS(const char *, const char *);
	virtual ~monitoredFS();
	int init();
	int stop();
	static void run(monitoredFS*);
	void lock();
	void unlock();
	monState_t getState();
	void setState(monState_t s);
	const char *getPath();
protected:
	int tryAccess();
	string Path;
	string fileName;
	monState_t State;
	pthread_t monitorThread;
	int mainAccessInterval;
	int timeoutToKill;
private:
	pthread_mutex_t mutex;
	int writeFileError;
	int writeDirError;
	int readFileError;
};

struct registeredFS
{
	registeredFS(const char *path) {
		Path = string(path);
	}
	string Path;
	monitoredFS *FS;
	friend bool operator > (const registeredFS& left,
	    const registeredFS& right)
	{
		return (left.Path > right.Path);
	}
	friend bool operator == (const registeredFS& left,
	    const registeredFS& right) {
		return (left.Path == right.Path);
	}
	friend int operator < (const registeredFS& left,
	    const registeredFS& right)
	{
		return (left.Path < right.Path);
	}
};


class damMonitor
{
public:
	damMonitor();
	virtual ~damMonitor();
	int init(scxcfg_t cfg);
	int startMonitor(const char *path);
	int stopMonitor(const char *path);
	int status(const char *path);
	void monitorAll(void *);
private:
	struct Server
	{
		Server(const char *n, const char *i) :
		nodeName(n), ipAddr(i) {};
		Server(string n, string i) {
			nodeName = n;
			ipAddr = i;
		}
		string nodeName;
		string ipAddr;
	};
	int buildServerList(scxcfg_t cfg);
	void updateManagerStatus();
	void setMainTimeout();
	bool hasTimeoutFired();
	int testThisNodeAlive(const char *);
	void lock();
	void unlock();
	pthread_t monitorAllThread;
	pthread_mutex_t mutex;
	std::set<registeredFS> FSList;
	std::list<Server*> serversList;
	scxcfg_nodeid_t localNodeid;
	string localNodeName;
	string accessFileName;
	uint64_t mainTimeout;
};

#endif /* _DAM_MONITOR_H */
