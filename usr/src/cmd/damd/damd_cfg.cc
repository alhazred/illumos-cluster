/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)damd_cfg.cc	1.3	08/05/20 SMI"

#include <fstream>
#include <iostream>
#include <string>
#include <list>

#include <syslog.h>
#include <scxcfg/scxcfg.h>

#include "dam.h"
#include "damd.h"
#include "damd_cfg.h"

//
// Global variable.
//
std::list<Fdevice*>FdeviceList;


int
readConfig(scxcfg_t cfg)
{
	int rc;
	char prop[FCFG_MAX_LEN];
	char type[FCFG_MAX_LEN];
	char server[FCFG_MAX_LEN];
	char path[FCFG_MAX_LEN];
	f_property_t *list = NULL, *v;
	int num = 0;
	scxcfg_error err;
	int rstatus = DAM_OK;
	std::list<Fdevice*>::const_iterator it;

	dam_log(LOG_DEBUG, "damd readConfig");
	// read the fencing devices in scxcfg config
	// verify the NAS devices
	// farm.properties.fencing_fs.1.type
	// farm.properties.fencing_fs.1.server
	// farm.properties.fencing_fs.1.path

	sprintf(prop, "farm.properties.fencing_fs");
	rc = scxcfg_getlistproperty_value(cfg, NULL, prop, &list, &num, &err);

	if (rc != FCFG_OK) {
		dam_log(LOG_ERR, "readConfig: err reading cfg: %d",
		    rc);
		return (DAM_EINVAL);
	}
	if (num == 0) {
		// no fencing FS
		return (DAM_OK);
	}
	for (v = list; v != NULL; v = v->next) {
		sprintf(prop, "farm.properties.fencing_fs.%s.type", v->value);
		rc =  scxcfg_getproperty_value(cfg, NULL, prop, type, &err);
		if (rc != FCFG_OK) {
			dam_log(LOG_ERR, "readConfig: err read: %s", prop);
			rstatus  = DAM_EINVAL;
			goto cleanup;
		}
		sprintf(prop, "farm.properties.fencing_fs.%s.server", v->value);
		rc =  scxcfg_getproperty_value(cfg, NULL, prop, server, &err);
		if (rc != FCFG_OK) {
			dam_log(LOG_ERR, "readConfig: err read: %s", prop);
			rstatus  = DAM_EINVAL;
			goto cleanup;
		}
		sprintf(prop, "farm.properties.fencing_fs.%s.path", v->value);
		rc =  scxcfg_getproperty_value(cfg, NULL, prop, path, &err);
		if (rc != FCFG_OK) {
			dam_log(LOG_ERR, "readConfig: err read: %s", prop);
			rstatus  = DAM_EINVAL;
			goto cleanup;
		}
		Fdevice *dev = new Fdevice(type, server, path);
		if (dev == NULL) {
			rstatus  = DAM_ENOMEM;
			goto cleanup;
		}
		FdeviceList.push_back(dev);
	}

	rc = findMountPoint(FdeviceList);
	if (rc != DAM_OK) {
		dam_log(LOG_ERR, "readConfig: findMountPoint : %s",
			rc);
		rstatus  = DAM_EINVAL;
		goto cleanup;
	}

	//
	// Monitor the configured fencing FS.
	//
	for (it = FdeviceList.begin(); it != FdeviceList.end(); it++) {
		//
		// If the mount point has not been found
		// skip it.
		//
		if ((*it)->mntPt == "") {
			dam_log(LOG_ERR, "skipping %s:%s  no mount point",
				(*it)->server.c_str(), (*it)->path.c_str());
			continue;
		}
		rc = dam_do_monitor((*it)->mntPt.c_str());
		if (rc != DAM_OK) {
			dam_log(LOG_ERR, "failed to start monitor : %s",
				(*it)->mntPt.c_str());
		}
	}
cleanup:
	if (list)
		scxcfg_freelist(list);

	return (rstatus);
}
