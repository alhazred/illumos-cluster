/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pmmd_impl.h
 */

#ifndef	_PMMD_IMPL_H
#define	_PMMD_IMPL_H

#pragma ident	"@(#)pmmd_impl.h	1.2	08/07/26 SMI"

#include <sys/os.h>
#include <sys/contract/process.h>

#define	PROG_NAME		"pmmd"
#define	PMMD_DOOR_FILE_NAME	"/var/cluster/.pmmd"

class pmmd_impl {
public:
	void initialize();
	void door_service(char *argp, size_t arglen) const;
	void launch_process(
	    char *argp, size_t arglen, int &error, pid_t &pid) const;
};
#endif /* _PMMD_IMPL_H */
