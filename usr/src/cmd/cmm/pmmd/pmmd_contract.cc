//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pmmd_contract.cc	1.9	08/10/21 SMI"

#include <sys/os.h>
#include <sys/wait.h>
#include <sys/contract/process.h>
#include <pmmd_contract.h>
#include <libclcontract/cl_contract_table.h>
#include <cmm/cmm_ns.h>
#include <orb/infrastructure/orb_conf.h>
#include <fcntl.h>
#include <door.h>
#include <h/membership.h>
#include <cmm/membership_engine_impl.h>
#include <sys/vm_util.h>

#define	FATAL_EVENTS		((uint_t)(CT_PR_EV_HWERR))
#define	CRITICAL_EVENTS		((uint_t)(CT_PR_EV_EMPTY|CT_PR_EV_FORK \
				|CT_PR_EV_CORE|CT_PR_EV_EXIT|CT_PR_EV_SIGNAL))
#define	INFORMATIVE_EVENTS	((uint_t)0)

#define	PROG_NAME	"pmmd"

extern void debug_print(char *, ...);

extern os::sc_syslog_msg logger;

// Constructor
// lint tool does not understand the typedef'ed function pointer prototypes
//lint -e1025 -e1703 -e64
pmmd_contract::pmmd_contract(
	const sigset_t &blocked_set, const sigset_t &ignored_set,
	const char *zone_namep, char *identifier_in,
	char **cmd_in, int &err, char **env_in) :
	cl_contract(identifier_in, cmd_in,
	FATAL_EVENTS, CRITICAL_EVENTS, INFORMATIVE_EVENTS, err,
	(char *)PROG_NAME, &debug_print, env_in)
//lint +e1025 +e1703 +e64
{
	zonenamep = os::strdup(zone_namep);
	ASSERT(zonenamep);
	blocked_sig_set = blocked_set;
	ignored_sig_set = ignored_set;
	debug_print("pmmd_contract ctor for %s\n",
	    identifier_in);
}

// Destructor
// lint tool complains that certain functions might throw exceptions
// in destructor.
//lint -e1551
pmmd_contract::~pmmd_contract()
{
	debug_print("pmmd_contract dtor for %s\n",
	    nametag.c_str());
	delete [] zonenamep;
}
//lint +e1551

//
// setup_child()
//
// After fork but before exec,
// sets up certain parameters in the forked process.
// - The client of pmmd could have specified a set of signals
// to be blocked by the forked child.
// Here we set the signal mask of the child process
// to that specified blocked signal set.
// - The client of pmmd could have specified a set of signals
// to be ignored by the forked child.
// Here we set the signal disposition of the child
// for those signals to SIG_IGN.
//
void
pmmd_contract::setup_child()
{
	int err = 0;
	sigset_t current_sig_set;

	// Invoke base class setup_child() first
	cl_contract::setup_child();

	//
	// Set the signal mask as specified by
	// the client of pmmd which asked pmmd to fork this child.
	//
	if (sigprocmask(SIG_SETMASK, &blocked_sig_set, &current_sig_set) != 0) {
		err = errno;
		debug_print("pmmd_contract: sigprocmask() failed to set "
		    "signal mask in setup_child(), error %s\n", strerror(err));
		//
		// SCMSGS
		// @explanation
		// The child that was forked by the pmmd daemon was unable
		// to set its signal mask because the sigprocmask(2) function
		// failed.
		// The message contains the system error.
		// The child that was forked by the pmmd daemon will not
		// set up its signal handling settings as specified.
		// @user_action
		// Save a copy of the /var/adm/messages files on this node,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "sigprocmask(2) failed for child (pid %d) forked by pmmd: "
		    "error %s", getpid(), strerror(err));
		return;
	}

	//
	// The client of pmmd, which asked pmmd to fork this child,
	// could have specified that this forked child should ignore
	// some signals.
	// For all such signals, set the signal disposition to SIG_IGN.
	//
	for (int sig = 1; sig <= SIGRTMAX; sig++) {
		if (sigismember(&ignored_sig_set, sig) == 0) {
			// Not ignored
			continue;
		}

		//
		// Convert the signal to its name.
		// This is also a check that we are operating on a valid signal.
		// Ideally, we should not find any invalid signal num
		// in the ignored signal set.
		// Of course, the assumption here is that sig2str() does not
		// fail for a valid signal number.
		//
		char sig_name[SIG2STR_MAX];
		if (sig2str(sig, (char *)sig_name) != 0) {
			debug_print("pmmd_contract: sig2str() failed "
			    "for signal %d in setup_child()\n", sig);
			//
			// SCMSGS
			// @explanation
			// The child that was forked by the pmmd daemon was
			// unable to get the signal name for a signal number
			// because the sig2str(3C) function failed.
			// @user_action
			// Save a copy of the /var/adm/messages files
			// on this node, and report the problem to
			// your authorized Sun service provider.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "child (pid %d) forked by pmmd could not "
			    "get the signal name for signal number %d "
			    "using sig2str(3C)", getpid(), sig);
			return;
		}

		if (sigignore(sig) != 0) {
			err = errno;
			debug_print("pmmd_contract: sigignore() failed "
			    "for signal SIG%s in setup_child(), error %s\n",
			    sig_name, strerror(err));
			//
			// SCMSGS
			// @explanation
			// The child that was forked by the pmmd daemon was
			// unable to set the signal disposition for a signal to
			// SIG_IGN, because the sigignore(3C) function failed.
			// The message contains the system error.
			// The child that was forked by the pmmd daemon
			// will not set up its signal handling settings
			// as specified.
			// @user_action
			// Save a copy of the /var/adm/messages files on this
			// node, and report the problem to your authorized
			// Sun service provider.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "sigignore(3C) of signal SIG%s failed for child "
			    "(pid %d) forked by pmmd: error %s",
			    sig_name, getpid(), strerror(err));
			return;
		}
	}
}

//
// event_dispatch()
//
// Switches on the event type,
// calls the appropriate method based on event type,
// acks critical events.
//
void
pmmd_contract::event_dispatch(ct_evthdl_t ev)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	// assert that this event is for us
	ctid_t ev_ctid = ct_event_get_ctid(ev);
	CL_PANIC(ev_ctid == contract_id);

	switch (ct_event_get_type(ev)) {
	case CT_PR_EV_EMPTY:
		empty_event(ev);
		ack_event(ev);
		break;
	case CT_PR_EV_FORK:
		fork_event(ev);
		ack_event(ev);
		break;
	case CT_PR_EV_EXIT:
		exit_event(ev);
		ack_event(ev);
		break;
	case CT_PR_EV_CORE:
	case CT_PR_EV_SIGNAL:
		core_event(ev);
		ack_event(ev);
		break;
	case CT_PR_EV_HWERR:
		// No need to ack these because they aren't critical
		debug_print("%s received non-critical event %d\n",
		    nametag.c_str(), ct_event_get_type(ev));
		break;
	default:
		debug_print("%s received unrecognized event %d\n",
		    nametag.c_str(), ct_event_get_type(ev));
		break;
	}

	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
}

void
pmmd_contract::fork_event(ct_evthdl_t ev)
{
	pid_t curr_pid;
	pid_t pid, ppid;
	int err;

	curr_pid = getpid();

	if ((err = ct_pr_event_get_ppid(ev, &ppid)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The pmmd was unable to retrieve the parent pid
		// from a contract event.
		// The pmmd will continue to monitor the process,
		// but the pmmd may have missed an event of interest.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_INFO, MESSAGE,
		    "%s: ct_pr_event_get_ppid: %s", PROG_NAME, strerror(err));

		//
		// Nothing we can do to recover the missing info, but
		// we might as well continue monitoring.
		//
		return;
	}

	// Get the id of the new process and its parent
	if ((err = ct_pr_event_get_pid(ev, &pid)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The pmmd was unable to retrieve the pid from a contract
		// event. The pmmd will continue to monitor the process,
		// but the pmmd may have missed an event of interest.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_INFO, MESSAGE,
		    "%s: fork_event: ct_pr_event_get_pid: %s",
		    PROG_NAME, strerror(err));

		//
		// Nothing we can do to recover the missing info, but
		// we might as well continue monitoring.
		//
		return;
	}

	debug_print("fork_event() for %s: pid=%d, ppid=%d\n",
	    nametag.c_str(), pid, ppid);
}

//
// We obtain the core file paths from Solaris Contracts APIs,
// and issue fsync on them, so that the core gets written to the disk.
// If we cannot get any core file paths, then we return failure.
// If we cannot sync a core file to which a path was obtained,
// then we return failure.
//
// Returns true on success, false on failure
//
bool
pmmd_contract::fsync_core_files(ct_evthdl_t ev)
{
	int pcore_err, gcore_err, zcore_err;
	int fd;
	const char *pcore_filep;
	const char *gcore_filep;
	const char *zcore_filep;

	if ((pcore_err = ct_pr_event_get_pcorefile(ev, &pcore_filep)) == 0) {
		debug_print("%s: process dumped core, pcorefile <%s>\n",
		    nametag.c_str(), pcore_filep);
	} else {
		debug_print("%s: could not get pcorefile, error %s\n",
		    nametag.c_str(), strerror(pcore_err));
	}

	if ((gcore_err = ct_pr_event_get_gcorefile(ev, &gcore_filep)) == 0) {
		debug_print("%s: process dumped core, gcorefile <%s>\n",
		    nametag.c_str(), gcore_filep);
	} else {
		debug_print("%s: could not get gcorefile, error %s\n",
		    nametag.c_str(), strerror(gcore_err));
	}

	if ((zcore_err = ct_pr_event_get_zcorefile(ev, &zcore_filep)) == 0) {
		debug_print("%s: process dumped core, zcorefile <%s>\n",
		    nametag.c_str(), zcore_filep);
	} else {
		debug_print("%s: could not get zcorefile, error %s\n",
		    nametag.c_str(), strerror(zcore_err));
	}

	// If we could not any get any core files, then we return failure
	if (pcore_err && gcore_err && zcore_err) {
		//
		// SCMSGS
		// @explanation
		// The pmmd was unable to retrieve any core file paths
		// from a contract event. The pmmd will continue to
		// monitor the process, but the pmmd has missed
		// information of interest.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(SC_SYSLOG_PANIC, MESSAGE,
		    "%s: fsync_core_files: could not get any core file paths: "
		    "pcorefile error %s, gcorefile error %s, "
		    "zcorefile error %s", PROG_NAME, strerror(pcore_err),
		    strerror(gcore_err), strerror(zcore_err));
		return (false);
	}


	// If path to the core file has been obtained, sync the core file
	if (!pcore_err) {
		fd = open(pcore_filep, O_RDONLY);
		if (fd < 0) {
			//
			// SCMSGS
			// @explanation
			// The pmmd was unable to open the core file dumped
			// by a monitored process. The pmmd will continue to
			// monitor the process, but the pmmd has missed
			// information of interest.
			// @user_action
			// Search for other syslog error messages on
			// the same node. Save a copy of the /var/adm/messages
			// files on all nodes, and report the problem to
			// your authorized Sun service provider.
			//
			(void) logger.log(SC_SYSLOG_PANIC, MESSAGE,
			    "%s: fsync_core_files: %s open <%s> failed, "
			    "error %s", PROG_NAME, "pcorefile",
			    pcore_filep, strerror(errno));
			return (false);
		}

		if (fsync(fd)) {
			//
			// SCMSGS
			// @explanation
			// The pmmd was unable to sync the core file dumped
			// by a monitored process. The pmmd will continue to
			// monitor the process, but the pmmd has missed
			// information of interest.
			// @user_action
			// Search for other syslog error messages on
			// the same node. Save a copy of the /var/adm/messages
			// files on all nodes, and report the problem to
			// your authorized Sun service provider.
			//
			(void) logger.log(SC_SYSLOG_PANIC, MESSAGE,
			    "%s: fsync_core_files: %s fsync <%s> failed, "
			    "error %s", PROG_NAME, "pcorefile",
			    pcore_filep, strerror(errno));
			(void) close(fd);
			return (false);
		}
		(void) close(fd);
		debug_print("%s: fsync for pcorefile<%s> succeeded",
		    nametag.c_str(), pcore_filep);
	}

	if (!gcore_err) {
		fd = open(gcore_filep, O_RDONLY);
		if (fd < 0) {
			(void) logger.log(SC_SYSLOG_PANIC, MESSAGE,
			    "%s: fsync_core_files: %s open <%s> failed, "
			    "error %s", PROG_NAME, "gcorefile",
			    gcore_filep, strerror(errno));
			return (false);
		}

		if (fsync(fd)) {
			(void) logger.log(SC_SYSLOG_PANIC, MESSAGE,
			    "%s: fsync_core_files: %s fsync <%s> failed, "
			    "error %s", PROG_NAME, "gcorefile",
			    gcore_filep, strerror(errno));
			(void) close(fd);
			return (false);
		}
		(void) close(fd);
		debug_print("%s: fsync for gcorefile<%s> succeeded",
		    nametag.c_str(), gcore_filep);
	}

	if (!zcore_err) {
		fd = open(zcore_filep, O_RDONLY);
		if (fd < 0) {
			(void) logger.log(SC_SYSLOG_PANIC, MESSAGE,
			    "%s: fsync_core_files: %s open <%s> failed, "
			    "error %s", PROG_NAME, "zcorefile",
			    zcore_filep, strerror(errno));
			return (false);
		}

		if (fsync(fd)) {
			(void) logger.log(SC_SYSLOG_PANIC, MESSAGE,
			    "%s: fsync_core_files: %s fsync <%s> failed, "
			    "error %s", PROG_NAME, "zcorefile",
			    zcore_filep, strerror(errno));
			(void) close(fd);
			return (false);
		}
		(void) close(fd);
		debug_print("%s: fsync for zcorefile<%s> succeeded",
		    nametag.c_str(), zcore_filep);
	}

	return (true);
}

void
pmmd_contract::core_event(ct_evthdl_t ev)
{
	debug_print("%s received core event\n", nametag.c_str());

	pid_t pid, ppid;
	int err;

	// Get the id of the process that dumped core
	if ((err = ct_pr_event_get_pid(ev, &pid)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The pmmd was unable to retrieve the pid from a contract
		// event. The pmmd will continue to monitor the process,
		// but the pmmd may have missed an event of interest.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_INFO, MESSAGE,
		    "%s: core_event: ct_pr_event_get_pid: %s",
		    PROG_NAME, strerror(err));

		//
		// Nothing we can do to recover the missing info, but
		// we might as well continue monitoring. We should receive
		// an exit event eventually.
		//
		return;
	}

	debug_print("%s: process %d dumped core\n", nametag.c_str(), pid);

	//
	// We obtain the core file paths from Solaris Contracts APIs,
	// and issue fsync on them, so that the core gets written to the disk,
	// before we do immediate failfast of the zone.
	// If there is any error, we do not do immediate failfast; so the
	// deferred panic of the zone would happen after the failfast timeout.
	//
	bool immediate_failfast = fsync_core_files(ev);
	if (!immediate_failfast) {
		// Sync did not happen. Do not do immediate failfast
		return;
	}

	// Failfast immediately the zone for which the process was running
	ff::failfast_admin_var ff_admin_v =
	    cmm_ns::get_ff_admin(orb_conf::local_nodeid());
	if (CORBA::is_nil(ff_admin_v)) {
		debug_print("%s: could not get failfast admin obj\n",
		    nametag.c_str());
		return;
	}

	Environment e;
	zoneid_t zone_id = ff_admin_v->zonename_to_zoneid(zonenamep, e);
	if (e.exception()) {
		e.clear();
		debug_print("%s: exception while trying to convert "
		    "zonename %s to zoneid\n", nametag.c_str(), zonenamep);
		return;
	}
	bool result = ff_admin_v->failfast_now(zone_id, e);
	if (e.exception()) {
		e.clear();
		debug_print("%s: exception while trying to failfast zone %s\n",
		    nametag.c_str(), zonenamep);
		return;
	}
}

void
pmmd_contract::exit_event(ct_evthdl_t ev)
{
	debug_print("%s received exit event\n", nametag.c_str());

	pid_t pid, ppid;
	int err;

	// Get the id of the process that exited
	if ((err = ct_pr_event_get_pid(ev, &pid)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The pmmd was unable to retrieve the pid from a contract
		// event. The pmmd will continue to monitor the process,
		// but the pmmd may have missed an event of interest.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_INFO, MESSAGE,
		    "%s: exit_event: ct_pr_event_get_pid: %s",
		    PROG_NAME, strerror(err));

		//
		// Nothing we can do to recover the missing info, but
		// we might as well continue monitoring. We should receive
		// an empty event eventually.
		//
		return;
	}

	//
	// If the process that just exited is the parent root process,
	// get its exit status
	//
	if (pid == parent_pid) {
		if ((err = ct_pr_event_get_exitstatus(ev, &parent_return_code))
		    != 0) {
			//
			// SCMSGS
			// @explanation
			// The pmmd was unable to determine the exit
			// status of a process under its control that exited.
			// It will assume failure.
			// @user_action
			// Search for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) logger.log(LOG_INFO, MESSAGE,
			    "%s: ct_pr_event_get_exitstatus: %s",
			    PROG_NAME, strerror(err));

			//
			// Nothing we can do to recover the missing info, but
			// we might as well continue monitoring. We should
			// receive an empty event eventually.
			//
			parent_return_code = 1;
			return;
		}
		// clean up the zombie process
		(void) waitpid(pid, &err, 0);
	}

	debug_print("%s: process %d exited\n", nametag.c_str(), pid);

	// Remove this entry from the process table
	procs.erase(pid);

	version_manager::vm_admin_ptr vm_adm_p = vm_util::get_vm();
	if (CORBA::is_nil(vm_adm_p)) {
		debug_print("%s: could not get VM admin object ref\n",
		    nametag.c_str());
		    return;
	}
	version_manager::vp_version_t vp_vers;
	Environment environ;
	vm_adm_p->get_running_version(MEMBERSHIP_VP_NAME, vp_vers, environ);
	if (environ.exception() != NULL) {
		debug_print("%s: could not get membership subsystem version\n",
		    nametag.c_str());
		    return;
	}
	if ((vp_vers.major_num < MEMBERSHIP_API_AVAILABLE_VP_MAJOR_NUM) ||
	    ((vp_vers.major_num == MEMBERSHIP_API_AVAILABLE_VP_MAJOR_NUM) &&
	    (vp_vers.minor_num < MEMBERSHIP_API_AVAILABLE_VP_MINOR_NUM))) {
		// Membership API support not available
		debug_print("%s: membership API support not available\n",
		    nametag.c_str());
		return;
	}

	// Deliver membership change event to membership engine
	uint32_t clid = 0;
	if (clconf_get_cluster_id(zonenamep, &clid)) {
		// Must be able to convert cluster name to cluster id
		// temporary fix for now. It is possible that we come
		// here after a zone cluster has been deleted.
		return;
	}
	membership::engine_var engine_v = cmm_ns::get_membership_engine_ref();
	ASSERT(!CORBA::is_nil(engine_v));
	membership::engine_event_info event_info;
	event_info.mem_manager_info.mem_type =
	    membership::PROCESS_UP_PROCESS_DOWN;
	event_info.mem_manager_info.cluster_id = clid;
	//
	// The nametag string of the contract structure follows this format :
	// "<zone_name> <process_name>". The membership subsystem keeps
	// only the <process_name> as an identifier for the process.
	// So, we strip the <zone_name> part of the nametag here.
	//
	size_t zonename_len = os::strlen(zonenamep);
	const char *nametag_copyp = nametag.c_str();
	// Skip over the <zone_name> and the space char
	const char *process_namep = nametag_copyp + zonename_len + 1;

	event_info.mem_manager_info.process_namep = os::strdup(process_namep);
	event_info.event = membership::PROCESS_DOWN;
	Environment e;
	engine_v->deliver_event_to_engine(
	    membership::DELIVER_MEMBERSHIP_CHANGE_EVENT, event_info, e);
	ASSERT(e.exception() == NULL);
}

void
pmmd_contract::empty_event(ct_evthdl_t ev)
{
	int err;

	debug_print("%s: empty event -- is_running=%s\n",
	    nametag.c_str(), is_running ?
	    "true" : "false");

	// abandon the contract so it will go away
	if ((err = ct_ctl_abandon(contract_fd)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The pmmd was unable to abandon an empty contract. The
		// contract will continue to exist even though it has no
		// processes in it.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. If there are many instances of the message, there is
		// a problem with the contract file system. Search for other
		// syslog error messages on the same node. Save a copy of the
		// /var/adm/messages files on all nodes, and report the
		// problem to your authorized Sun service provider.
		//
		(void) logger.log(LOG_INFO, MESSAGE,
		    "%s: ct_ctl_abandon: %s", PROG_NAME, strerror(err));
	}
	(void) close(contract_fd);
	contract_fd = 0;

	// Remove the contract / nametag mapping from the contract table
	cl_contract_table::instance().remove_mapping(contract_id);
	contract_id = 0;

	is_running = false;

	end_monitoring();
}
