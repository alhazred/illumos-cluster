/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pmmd_contract.h
 */

#ifndef	_PMMD_CONTRACT_H
#define	_PMMD_CONTRACT_H

#pragma ident	"@(#)pmmd_contract.h	1.4	08/10/21 SMI"

#include <signal.h>
#include <libclcontract/cl_contract.h>

// lint complains default constructor not defined.
// Defining default constructor would require default constructor
// for parent class 'cl_contract'.
//lint -e1712
class pmmd_contract : public cl_contract {
public:
	pmmd_contract(
	    const sigset_t &blocked_set, const sigset_t &ignored_set,
	    const char *zone_namep, char *identifier_in,
	    char **cmd_in, int &err, char **env_in = NULL);
	virtual ~pmmd_contract();

	/* Process a contract event */
	void event_dispatch(ct_evthdl_t ev);

protected:
	virtual void fork_event(ct_evthdl_t ev);
	virtual void core_event(ct_evthdl_t ev);
	virtual void exit_event(ct_evthdl_t ev);
	virtual void empty_event(ct_evthdl_t ev);

	void setup_child();

private:
	bool fsync_core_files(ct_evthdl_t ev);

	/* prevent assignment and pass-by-value */
	pmmd_contract(const pmmd_contract &src);
	pmmd_contract &operator = (const pmmd_contract &rhs);

	char *zonenamep;
	sigset_t blocked_sig_set;
	sigset_t ignored_sig_set;
};
//lint +e1712
#endif /* _PMMD_CONTRACT_H */
