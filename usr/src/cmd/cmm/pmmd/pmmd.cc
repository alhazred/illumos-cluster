//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pmmd.cc	1.4	08/07/28 SMI"

#include <zone.h>
#include <orb/infrastructure/orb.h>
#include <sys/os.h>
#include <nslib/ns.h>
#include <cmm/cmm_ns.h>
#include <sys/sol_version.h>
#include <cmm/ff_signal.h>
#include <signal.h>
#include <fcntl.h>
#include "pmmd_impl.h"
#include <libclcontract/cl_contract_main.h>
#include <sys/clconf_int.h>

static ff::failfast_var	death_ff_v;
static int pipe_fds[2];

#define	DBG_BUFSIZE (64 * 1024) // 64KB ring buffer for tracing
dbg_print_buf pmmd_dbg_buf(DBG_BUFSIZE); // ring buffer for tracing

#define	PMM_PROC_BUFSIZ	256

pmmd_impl *implp = NULL;

os::sc_syslog_msg logger(PROG_NAME, "", NULL);

// Routine for tracing debug info.
void
debug_print(char *fmt, ...)
{
	uint_t	len = 0;
	char	str1[PMM_PROC_BUFSIZ], fmt1[PMM_PROC_BUFSIZ];
	va_list args;

	(void) snprintf(fmt1, PMM_PROC_BUFSIZ, "%s", fmt);

	// Add newline if there isn't in 'fmt'.
	len = (uint_t)strlen(fmt1);
	if (fmt1[len - 1] != '\n' && len < PMM_PROC_BUFSIZ - 1) {
		fmt1[len] = '\n';
		fmt1[len + 1] = '\0';
	}

	va_start(args, fmt);	//lint !e40
	if (vsnprintf(str1, PMM_PROC_BUFSIZ, fmt1, args) >= PMM_PROC_BUFSIZ) {
		str1[PMM_PROC_BUFSIZ - 1] = '\0';
	}
	va_end(args);

	pmmd_dbg_buf.dbprintf(str1);
}

// Create and arm a failfast object so that the node dies when pmmd dies.
void
create_and_arm_failfast(const char *ff_namep)
{
	ASSERT(ff_namep != NULL);

	Environment e;
	ff::failfast_admin_var ff_admin_v = cmm_ns::get_ff_admin();

	//
	// Use FFM_DEFERRED_PANIC to allow enough time for a core
	// dump of this process.
	//

	death_ff_v = ff_admin_v->ff_create(ff_namep, ff::FFM_DEFERRED_PANIC, e);

	if (e.exception() || CORBA::is_nil(death_ff_v)) {

		//
		// Given the lack of an error code to represent what happened
		// we choose ECOMM to best represent why we failed.
		//

		//
		// SCMSGS
		// @explanation
		// The pmmd program was not able to register with failfast.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: unable to create failfast object.", PROG_NAME);
		exit(ECOMM);
	} else {
		// Generate a failfast only when this process dies.
		death_ff_v->arm(ff::FF_INFINITE, e);
		if (e.exception()) {
			//
			// SCMSGS
			// @explanation
			// The pmmd program was not able to arm the
			// failfast unit.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "%s: unable to arm failfast.", PROG_NAME);
			exit(ECOMM);
		}
		// Trigger the failfast upon receipt of these signals
		ff_register_signal_handler(SIGILL, &death_ff_v);
		ff_register_signal_handler(SIGBUS, &death_ff_v);
		ff_register_signal_handler(SIGSEGV, &death_ff_v);
	}
}

// Utility function to mask all the maskable signals.
void
block_allsignals(void)
{
	sigset_t s_mask;

	//
	// Ignore SIGHUP and SIGTERM so the transition from rcS to rc2 doesn't
	// kill us.  Block the signals so that sending signals to a
	// user program that calls 'pmmd' does not lead to
	// our handling the signal in unexpected ways.
	//
	if (sigfillset(&s_mask) == -1) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: sigfillset returned %d. Exiting.", PROG_NAME, errno);
		exit(errno);
	}
	if (sigdelset(&s_mask, SIGILL) == -1) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: sigdelset returned %d. Exiting.", PROG_NAME, errno);
		exit(errno);
	}
	if (sigdelset(&s_mask, SIGBUS) == -1) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: sigdelset returned %d. Exiting.", PROG_NAME, errno);
		exit(errno);
	}
	if (sigdelset(&s_mask, SIGSEGV) == -1) {
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: sigdelset returned %d. Exiting.", PROG_NAME, errno);
		exit(errno);
	}
	if (thr_sigsetmask(SIG_BLOCK, &s_mask, NULL) != 0) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: thr_sigsetmask returned %d. Exiting.",
		    PROG_NAME, errno);
		exit(errno);
	}
}

// The daemon sets up stuff
static void
pmmd()
{
	implp = new pmmd_impl;
	ASSERT(implp);
	implp->initialize();
}

static void
start_pmmd(void)
{
	ssize_t res;
	char c = 0;

	block_allsignals();

	// Daemonize.
	(void) close(0);
	(void) close(1);
	(void) close(2);

	(void) setsid();

	// Initialize clconf and ORB.
	if (clconf_lib_init() != 0) {
		//
		// SCMSGS
		// @explanation
		// The pmmd program was unable to initialize ORB.
		// @user_action
		// Make sure the nodes are booted in cluster mode. If so,
		// contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: Could not initialize the ORB. Exiting.", PROG_NAME);
		exit(1);
	}

	libclcontracts_initialize();

	pmmd();

	//
	// Create and arm a failfast object so that if failfastd dies, the
	// node will be forced to panic.
	//
	create_and_arm_failfast(PROG_NAME);

	debug_print("%s(%d):ready\n", PROG_NAME, getpid());

	// Now unblock the parent process, waiting for the daemon to be ready
	res = write(pipe_fds[1], (void *)&c, 1);
	if (res != 1) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: Unblock parent, write errno %d", PROG_NAME, errno);
		exit(1);
	}
	(void) close(pipe_fds[1]);
	debug_print("%s(%d):write pipe\n", PROG_NAME, getpid());

	thr_exit(0);
	// not reached
}

// Wait for the deamon to be ready
static void
wait_for_daemon(void)
{
	ssize_t res;
	char c = (char)-1;

	// Block until the daemon has written to the pipe
	res = read(pipe_fds[0], (void *)&c, 1);
	if (res != 1) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: problem waiting for the daemon, read errno %d",
		    PROG_NAME, errno);
		exit(1);
	} else {
		ASSERT(c == 0);
	}
	(void) close(pipe_fds[0]);
	debug_print("%s(%d):read pipe\n", PROG_NAME, getpid());
}

// Create the daemon process
static void
daemonize()
{
	int ret;
	int err;
	debug_print("%s(%d):fork1\n", PROG_NAME, getpid());
	ret = fork1();
	err = errno;
	debug_print("%s(%d):fork1\n", PROG_NAME, getpid());
	switch (ret) {
	case ((pid_t)-1) :
		//
		// SCMSGS
		// @explanation
		// The pmmd program was unable to fork and daemonize
		// because fork1() system call failed.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: fork1 returned %d. Exiting.", PROG_NAME, err);
		exit(err);
		break;
	case 0:
		/* In the child */
		(void) close(pipe_fds[0]);
		start_pmmd();
		/* NOTREACHED */
		break;
	default:
		/* In the father */
		(void) close(pipe_fds[1]);
		return;
	}
}

//
// Main routine.
// Return value:
//	0 on success.
//	non-zero on failure.
//
int
main(int, char * const[])
{
	block_allsignals();

	// Create a communication pipe with the daemon process
	if (pipe(pipe_fds) != 0) {
		//
		// SCMSGS
		// @explanation
		// Internal error.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: pipe returned %d. Exiting.", PROG_NAME, errno);
		exit(errno);
	}

	// Create the daemon
	daemonize();
	debug_print("%s(%d): done\n", PROG_NAME, getpid());

	// Wait until the daemon is ready
	wait_for_daemon();
	debug_print("%s(%d): exit\n", PROG_NAME, getpid());

	return (0);
}
