//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pmmd_impl.cc	1.5	08/10/21 SMI"

#include "pmmd_impl.h"
#include <sys/sol_version.h>
#include <libclcontract/smart_ptr.h>
#include <libclcontract/cl_contract.h>
#include <libclcontract/cl_contract_table.h>
#include "pmmd_contract.h"
#include <door.h>
#include <fcntl.h>
#include <cmm/cmm_ns.h>

extern void debug_print(char *, ...);

extern os::sc_syslog_msg logger;

static int
unlink_file(char *fnamep)
{
	int err;
	while (unlink(fnamep) == -1) {
		err = errno;
		if (err == ENOENT) {
			return (0);
		}

		debug_print("pmmd_impl: unlink %s errno %d\n", fnamep, err);
		if (err != EINTR && err != EAGAIN) {
			return (err);
		}
	}
	return (0);
}

// Door server
static void
door_server_func(void *cookie, char *arg, size_t arglen, door_desc_t *, uint_t)
{
	pmmd_impl *objp = (pmmd_impl *)cookie;
	objp->door_service(arg, arglen);
	ASSERT(0);	// should not reach here
}

void
pmmd_impl::initialize()
{
	int door_fd = -1;
	int door_file_fd = -1;
	int err;

	if (unlink_file((char *)PMMD_DOOR_FILE_NAME) != 0) {
		return;
	}

	if ((door_file_fd =
	    open(PMMD_DOOR_FILE_NAME, O_RDWR | O_CREAT, 0600)) < 0) {
		err = errno;
		debug_print("pmmd_impl: open %s errno %d\n",
		    PMMD_DOOR_FILE_NAME, err);
		return;
	}
	(void) close(door_file_fd);

	if ((door_fd = door_create(door_server_func, (void *)this, 0)) < 0) {
		err = errno;
		debug_print(
		    "pmmd_impl: could not create door, errno = %d\n", err);
		(void) unlink_file((char *)PMMD_DOOR_FILE_NAME);
		return;
	}

	if (fattach(door_fd, PMMD_DOOR_FILE_NAME) < 0) {
		err = errno;
		debug_print("pmmd_impl: fattach %s errno %d\n",
		    PMMD_DOOR_FILE_NAME, err);
		(void) unlink_file((char *)PMMD_DOOR_FILE_NAME);
		return;
	}
}

// The entry point in the object for the door_call.
void
pmmd_impl::door_service(char *argp, size_t arglen) const
{
	struct ret_data_t {
		int err;
		pid_t pid;
	};
	struct ret_data_t ret_data;

	launch_process(argp, arglen, ret_data.err, ret_data.pid);
	(void) door_return(
	    (char *)&ret_data, sizeof (struct ret_data_t), NULL, 0);
}

void
pmmd_impl::launch_process(
    char *argp, size_t arglen, int &error, pid_t &pid_to_return) const
{
	int err = 0;
	uint_t len = 0;
	sigset_t blocked_sig_set;
	sigset_t ignored_sig_set;
	char *zonenamep = NULL;
	char *process_namep = NULL;
	char *cmdp = NULL;
	char **env_varsp = NULL;
	uint_t num_env_vars = 0;

	smart_ptr<cl_contract> sp(NULL);

	//
	// The string passed in is of the form :
	// "<blocked_sig_set><ignored_sig_set>
	// <process_name>'\0'<zonename>'\0'
	// <cmd_string with args>'\0'<env_var1>'\0'<env_var2>..."
	//
	ASSERT(argp[arglen-1] == '\0');

	//
	// The set of signals to be blocked
	//
	if (sigemptyset(&blocked_sig_set) != 0) {
		// Pass the error to door client
		error = errno;
		debug_print("pmmd_impl: sigemptyset() failed in "
		    "launch_process(), error %s\n", strerror(error));
		//
		// SCMSGS
		// @explanation
		// The pmmd daemon was unable to initialize a signal set
		// because the sigemptyset(3C) function failed.
		// The message contains the system error.
		// The pmmd daemon will not do the requested action.
		// @user_action
		// Save a copy of the /var/adm/messages files on this node,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: sigemptyset(3C): %s", PROG_NAME, strerror(error));
		pid_to_return = 0;
		return;
	}
	len = sizeof (sigset_t);
	(void) memcpy((void *)&blocked_sig_set,
	    (const void *)argp, (size_t)len);
	argp += len;

	//
	// The set of signals to be ignored
	//
	if (sigemptyset(&ignored_sig_set) != 0) {
		// Pass the error to door client
		error = errno;
		debug_print("pmmd_impl: sigemptyset() failed in "
		    "launch_process(), error %s\n", strerror(error));
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s: sigemptyset(3C): %s", PROG_NAME, strerror(error));
		pid_to_return = 0;
		return;
	}
	len = sizeof (sigset_t);
	(void) memcpy((void *)&ignored_sig_set,
	    (const void *)argp, (size_t)len);
	argp += len;

	// The process name part of the string
	len = os::strlen(argp);
	process_namep = argp;
	argp += (len + 1);

	// The zonename part of the string
	len = os::strlen(argp);
	zonenamep = argp;
	argp += (len + 1);

	// The command in the string
	len = os::strlen(argp);
	cmdp = argp;
	argp += (len + 1);

	// There might be some env vars
	uint_t rem_len = arglen -
	    ((sizeof (sigset_t) * 2) + os::strlen(process_namep) + 1 +
	    os::strlen(zonenamep) + 1 + os::strlen(cmdp) + 1);
	if (rem_len > 0) {
		// Env vars present
		char *tmpp = argp;
		for (uint_t ind = 0; ind < rem_len; ind++, tmpp++) {
			if (*tmpp == '\0') {
				num_env_vars++;
			}
		}
		ASSERT(num_env_vars > 0);
		env_varsp = new char*[num_env_vars+1];
		ASSERT(env_varsp);
		for (uint_t ind = 0; ind < num_env_vars; ind++) {
			len = os::strlen(argp);
			env_varsp[ind] = os::strdup(argp);
			argp += (len + 1);
		}
		env_varsp[num_env_vars] = NULL;
	} else {
		num_env_vars = 0;
		env_varsp = new char*[1];
		env_varsp[0] = NULL;
	}
	ASSERT(*(argp-1) == '\0');

	// Trace into debug buffer all the info received from door client
	debug_print("pmmd_impl: To execute task <%s>, "
	    "process name %s, zone %s\n", cmdp, process_namep, zonenamep);

	for (int sig = 1; sig <= SIGRTMAX; sig++) {
		if (sigismember(&blocked_sig_set, sig)) {
			char sig_name[SIG2STR_MAX];
			if (sig2str(sig, (char *)sig_name) != 0) {
				//
				// The clients of pmmd are cluster processes
				// which should be sending signal sets
				// that have no invalid signal numbers.
				// So we should be able to get the signal name
				// for a signal number in the set.
				//
				ASSERT(0);

				//
				// sig2str(3C) returns either 0 (success)
				// or -1 (failure). It does not set errno.
				// We simply return -1 as error to door client.
				//
				error = -1;
				debug_print("pmmd_impl:: sig2str(3C) failed "
				    "for signal %d in blocked_sig_set in "
				    "launch_process()\n", sig);

				//
				// SCMSGS
				// @explanation
				// The pmmd daemon was unable to get
				// the signal name for a signal number
				// because the sig2str(3C) function failed.
				// The pmmd daemon will not do
				// the requested action.
				// @user_action
				// Save a copy of the /var/adm/messages files
				// on this node, and report the problem to
				// your authorized Sun service provider.
				//
				(void) logger.log(LOG_ERR, MESSAGE,
				    "%s: sig2str(3C) failed for signal %d",
				    PROG_NAME, sig);
				pid_to_return = 0;
				return;
			}

			debug_print("pmmd_impl: signal SIG%s to be blocked\n",
			    sig_name);
		}
	}

	for (int sig = 1; sig <= SIGRTMAX; sig++) {
		if (sigismember(&ignored_sig_set, sig)) {
			char sig_name[SIG2STR_MAX];
			if (sig2str(sig, (char *)sig_name) != 0) {
				//
				// The clients of pmmd are cluster processes
				// which should be sending signal sets
				// that have no invalid signal numbers.
				// So we should be able to get the signal name
				// for a signal number in the set.
				//
				ASSERT(0);

				//
				// sig2str(3C) returns either 0 (success)
				// or -1 (failure). It does not set errno.
				// We simply return -1 as error to door client.
				//
				error = -1;
				debug_print("pmmd_impl:: sig2str(3C) failed "
				    "for signal %d in ignored_sig_set in "
				    "launch_process()\n", sig);

				(void) logger.log(LOG_ERR, MESSAGE,
				    "%s: sig2str(3C) failed for signal %d",
				    PROG_NAME, sig);
				pid_to_return = 0;
				return;
			}

			debug_print("pmmd_impl: signal SIG%s to be ignored\n",
			    sig_name);
		}
	}

	if (num_env_vars > 0) {
		debug_print("pmmd_impl: env vars are :\n");
		for (uint_t ind = 0; ind < num_env_vars; ind++) {
			debug_print("pmmd_impl: %s\n", env_varsp[ind]);
		}
	}

	if (!cmdp || !(*cmdp)) {
		// No executable passed in
		debug_print("null string passed as executable");
		for (uint_t ind = 0; ind < num_env_vars; ind++) {
			delete [] (env_varsp[ind]);
		}
		delete [] env_varsp;
		error = EINVAL;
		pid_to_return = 0;
		return;
	}

	char **cmd_strings = NULL;
	uint_t num_strings = 0;
	char *identifierp = NULL;

	//
	// The command argument passed consists of an executable file name
	// and arguments to the executable, if any.
	// We separate out the strings here.
	//
	char *cmd_dupp = os::strdup(cmdp);
	char *tokenp = strtok(cmd_dupp, " ");
	if (tokenp) {
		num_strings++;
		while ((tokenp = strtok(NULL, " ")) != NULL) {
			num_strings++;
		}
	} else {
		ASSERT(0);	// Cannot happen
	}
	delete [] cmd_dupp;
	cmd_dupp = os::strdup(cmdp);
	cmd_strings = new char*[num_strings+1];
	uint_t count = 0;
	tokenp = strtok(cmd_dupp, " ");
	if (tokenp) {
		cmd_strings[count++] = os::strdup(tokenp);
		// XXX check for null
		while ((tokenp = strtok(NULL, " ")) != NULL) {
			cmd_strings[count++] = os::strdup(tokenp);
			// XXX check for null
		}
	} else {
		ASSERT(0);	// Cannot happen
	}
	ASSERT(count == num_strings);
	cmd_strings[num_strings] = NULL;
	delete [] cmd_dupp;	// XXX will this free the entire string
				// XXX considering spaces are replaced by '\0'?

	//
	// The identifier is used as an identity for the process monitored.
	// Process membership monitors rgmd and ucmmd as of now.
	// For each zone cluster, a rgmd process runs in the global zone.
	// So, multiple rgmd processes pertaining to various zone clusters
	// are monitored by the pmmd in global zone.
	// To distinguish between them, we build the identifier
	// of the format "<zone_name> <process_name>".
	// A space character cannot be a part of zonename or process name.
	//
	uint_t identifier_len =
	    os::strlen(zonenamep) + 1 + os::strlen(process_namep) + 1;
	identifierp = new char[identifier_len];
	ASSERT(identifierp);
	os::sprintf(identifierp, "%s %s", zonenamep, process_namep);
	identifierp[identifier_len-1] = '\0';

	//
	// Create the contract object.
	// Note that the command strings and the environment vars
	// variables are stored as part of the contract object.
	// They would be freed in the destructor of the contract object.
	//
	pmmd_contract *contractp =
	    new pmmd_contract(blocked_sig_set, ignored_sig_set,
	    zonenamep, identifierp, cmd_strings, err, env_varsp);
	if (contractp == NULL) {
		delete [] identifierp;
		for (uint_t ind = 0; ind < num_env_vars; ind++) {
			delete [] (env_varsp[ind]);
		}
		delete [] env_varsp;
		delete [] cmd_strings;
		error = ENOMEM;
		pid_to_return = 0;
		return;
	} else if (err != 0) {
		debug_print(
		    "%s: error from pmmd_contract ctor\n", identifierp);
		delete contractp;
		error = err;
		pid_to_return = 0;
		return;
	}

	// Add the contract object to the contract table
	sp = smart_ptr<cl_contract>(contractp);
	debug_print(
	    "adding contract object %x to contract table\n", contractp);
	if ((err = cl_contract_table::instance().add_contract(sp)) != 0) {
		if (err == ENOMEM) {
			debug_print("ENOMEM while adding contract object %x "
			    "to table\n", contractp);
		} else {
			debug_print(
			    "duplicate argument while trying to add "
			    "contract object %x to table\n", contractp);
		}
		// no need to free contractp : smart pointer will do that for us
		delete [] identifierp;
		error = err;
		pid_to_return = 0;
		return;
	}

	pid_t forked_pid = 0;
	// start the new process
	if ((err = sp->start(forked_pid)) != 0) {
		//
		// The start failed. Remove this cl_contract object from
		// the table.
		//
		debug_print(
		    "%s: start failed, error %d\n", identifierp, err);
		(void) cl_contract_table::instance().remove(sp->get_nametag());
	} else {
		debug_print(
		    "%s: start succeeded\n", identifierp);
	}

	delete [] identifierp;
	error = err;
	pid_to_return = forked_pid;

	// XXX
	// XXX check if cmd_dupp needs to be deleted.
	// XXX its better to allocate separate memory for objects
	// XXX that would be retained by the contract object,
	// XXX and use/delete any other memory in this code.
	// XXX
}
