//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pmmd_adm.cc	1.5	08/10/21 SMI"

#include <zone.h>
#include <door.h>
#include <fcntl.h>
#include <sys/os.h>
#include <sys/list_def.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <procfs.h>
#include <errno.h>
#include <stdlib.h>
#include <dirent.h>
#include <libgen.h>
#include <signal.h>

#define	PMMD_DOOR_FILE_NAME	"/var/cluster/.pmmd"
#define	MAX_BUF_LEN	8192

#define	OUTPUT_FILE	"/var/cluster/pmmd_adm_log"

#define	PROG_NAME	"pmmd_adm"

//
// lint complains some variables are not accessed, although they are
// initialized and used inside loops
//lint -e550

//
// Description of pmmd_adm :
// -------------------------
// pmmd_adm is an internal utility for use by cluster software
// to launch a process under process membership subsystem's control.
//
// Currently, only rgmd and ucmmd use process membership feature.
//
// rgmd is launched by making direct door calls to pmmd daemon.
//
// ucmmd is launched from rac_framework_lib script, and hence
// rac_framework_lib script uses pmmd_adm to launch ucmmd
// under process membership.
// After pmmd_adm returns success/failure to rac_framework_lib,
// rac_framework_lib script checks whether ucmmd is running or not.
// Hence pmmd_adm should not return until the ucmmd process
// has really exec'ed, or until the forked process dies.
//
// Requirements :
// ---------------
// (1) pmmd_adm should return failure if the process could not be forked
// (2) if the fork happened, pmmd_adm should wait until :
// (2.1) either the forked process exec's to run the executable name
// passed in to pmmd_adm; or
// (2.2) the forked process dies/exits (because the ucmmd forked is the parent
// of another child ucmmd that the parent ucmmd forks, and the parent ucmmd
// exits after forking the child ucmmd)
// In either (2.1) or (2.2) above, pmmd_adm returns success
// signifying that the fork was successful.
//
// Reasoning behind the requirement :
// ----------------------------------
// Notice that pmmd_adm forwards the request to fork ucmmd to the pmmd
// daemon. pmmd does the actual fork, and returns from the door call.
// At this point, the forked process (which is supposed to exec ucmmd)
// might not have exec'ed. So if pmmd_adm returns immediately upon
// coming back from door_call, then the rac_framework_lib script
// might not find ucmmd running if the forked process has not really exec'ed.
// Hence pmmd_adm does the following :
// (A) If pmmd_adm gets error from door_call, it assumes that ucmmd
// was never forked off, and hence it returns error back immediately.
// (B) If door_call was successful, then pmmd_adm knows the pid P of
// the forked process and the executable name that was supposed to be forked.
// Now pmmd_adm reads the /proc files for pid P.
// (B.1) The /proc/<P> directory should always exist until the forked process
// dies/exits. So if pmmd_adm is unable to open /proc/<P>, then it knows that
// the forked process died/exited, and hence pmmd_adm returns success
// as per requirement (2.2).
// (B.2) If pmmd_adm cannot read the /proc/<P>/psinfo file (this file contains
// the exec name of pid P), then it might be the case that process P is exec'ing
// or dying/exiting. So pmmd_adm sleeps for some time and retries actions
// in (B.1) and (B.2) again.
// If pmmd_adm is able to read /proc/<P>/psinfo file :
// - if the execname in /proc/<P>/psinfo is not the same as the executable name
// passed in to pmmd_adm, then pmmd_adm sleeps for some time and retries actions
// in (B.1) and (B.2) again. This is the case that the forked process P has
// not yet exec'ed.
// - if the execname in /proc/<P>/psinfo is the same as the executable name
// passed in to pmmd_adm, then pmmd_adm returns success
// as per requirement (2.1) above.
//
// Notice that in all the above scenarios of retries, pmmd_adm will retry
// for a max number of retries, and not indefinitely.
//

//
// Global variables
//
FILE *fp = NULL;	// file pointer to the log file
unsigned long sleep_time = 1;	// seconds to sleep between retries
uint_t max_retries = 5;	// max number of retries while waiting for exec to occur

//
// Usage : pmmd_adm -p <process_name>
// -z <zone name> [-e <environment>] -c <command>
// <command> includes process binary path and arguments
//
void
print_usage()
{
	// XXX will need this whenever this binary becomes a public utility
}

//
// This function will read /proc files for the given "exec_pid"
// and will wait until the process specified by the "exec_pid"
// execs to run the given "exec_namep",
// or until the process specified by the exec_pid dies/exits.
// (1) If this function is unable to open the /proc/<exec_pid> directory,
// then this function returns success, considering the fact that the process
// wa forked but the forked process died/exited.
// (2) If this function is unable to read the /proc/<exec_pid>/psinfo file
// for the process specified by "exec_pid", then the process might be exec'ing
// or dying/exiting. This function will sleep for some time in this case,
// and retry opening the /proc/<P> directory and reading the /proc/<P>/psinfo
// file again.
// (3) If this function is able to read the /proc/<exec_pid>/psinfo file :
// - if execname in psinfo file does not match the "exec_namep" passed in
// to this function, then this function would sleep for some time and
// then retry opening/reading /proc files again. Basically the function
// considers that the forked process has not exec'ed "exec_namep".
// - if this function sees that the process specified by the exec_pid
// has exec'ed to run the given "exec_namep" (/proc/<exec_pid>/psinfo file will
// give the exec name of process <exec_pid>), it will return success.
//
// In any retry case, this function will retry for a maximum num of retries
// and not indefinitely.
//
// Returns 0 on success, 1 on failure.
//
int
wait_for_launched_process_to_exec(pid_t exec_pid, const char *exec_namep)
{
	DIR *dirp;
	int fd;
	char filename[100];
	char dirname_pid[100];
	psinfo_t psinfo_pid;
	uint_t num_trials = 0;

	ASSERT(fp != NULL);
	(void) fprintf(fp, "wait_for_launched_process_to_exec: exec_pid %d, "
	    "exec_namep %s\n", exec_pid, exec_namep);

	(void) sprintf(dirname_pid, "/proc/%d", exec_pid);
	(void) sprintf(filename, "/proc/%d/psinfo", exec_pid);

	do {
		num_trials++;

		if ((dirp = opendir(dirname_pid)) == NULL) {
			(void) fprintf(fp, "open of dir %s failed "
			    "with errno %d, exec_pid %d dead\n",
			    dirname_pid, errno, exec_pid);
			return (0);
		}
		(void) closedir(dirp);

		fd = open(filename, O_RDONLY);
		if (fd == -1) {
			(void) fprintf(fp, "open of %s failed with errno %d, "
			    "will retry after %d seconds\n",
			    filename, errno, sleep_time);
			(void) sleep(sleep_time);
			continue;
		}

		if (pread(fd, &psinfo_pid,
		    sizeof (psinfo_pid), (off_t)0) == -1) {
			(void) fprintf(fp, "pread of %s failed with errno %d, "
			    "will retry after %d seconds\n",
			    filename, errno, sleep_time);
			(void) close(fd);
			(void) sleep(sleep_time);
			continue;
		}

		(void) fprintf(fp,
		    "pr_pid = %d, pr_fname = %s, pr_zoneid = %d\n",
		    psinfo_pid.pr_pid, psinfo_pid.pr_fname,
		    psinfo_pid.pr_zoneid);

		if (strcmp(psinfo_pid.pr_fname, exec_namep) == 0) {
			(void) close(fd);
			return (0);
		} else {
			(void) close(fd);
			(void) sleep(sleep_time);
			continue;
		}

	} while (num_trials < max_retries);

	(void) fprintf(fp,
	    "could not read proc info for exec_pid %d after %d retries\n",
	    exec_pid, max_retries);
	return (1);
}

void
empty_env_list(SList < char > &env_list)
{
	char *elemp = NULL;
	env_list.atfirst();
	while ((elemp = env_list.get_current()) != NULL) {
		env_list.erase(elemp);
	}
	ASSERT(env_list.empty());
}

int
main(int argc, char * const argv[])
{
	int err = 0;

	fp = fopen(OUTPUT_FILE, "a+");
	if (!fp) {
		return (1);
	}

	(void) fprintf(fp, "***** PMMD_ADM (pid = %d) *****\n", getpid());

	// Flags for the arguments
	uint_t pflag = 0;
	uint_t zflag = 0;
	uint_t eflag = 0;
	uint_t cflag = 0;

	int arg;
	char cmd_string[MAX_BUF_LEN];
	char process_name[MAX_BUF_LEN];
	char zonename[ZONENAME_MAX];
	SList < char > env;

	// Parse the arguments
	optind = 1;
	while ((arg = getopt(argc, argv, "p:z:e:c:?")) != EOF) {
		switch (arg) {
		case 'p':
			pflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				// Process name not specified
				(void) fclose(fp);
				empty_env_list(env);
				return (1);
			}
			if (os::strlen(optarg) >= MAX_BUF_LEN) {
				(void) fclose(fp);
				empty_env_list(env);
				return (1);
			}
			(void) os::strcpy(process_name, optarg);
			break;
		case 'z':
			zflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				// Zone name not specified
				(void) fclose(fp);
				empty_env_list(env);
				return (1);
			}
			if (os::strlen(optarg) >= ZONENAME_MAX) {
				(void) fclose(fp);
				empty_env_list(env);
				return (1);
			}
			(void) os::strcpy(zonename, optarg);
			break;
		case 'e':
			eflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				// Environment var and its value not specified
				(void) fclose(fp);
				empty_env_list(env);
				return (1);
			}
			env.append(optarg);
			break;
		case 'c':
			cflag = 1;
			if ((optarg == NULL) || (*optarg == NULL)) {
				// Command string not specified
				(void) fclose(fp);
				empty_env_list(env);
				return (1);
			}
			(void) os::strncpy(cmd_string, optarg, MAX_BUF_LEN - 1);
			break;
		case '?':
		default:
			print_usage();
			(void) fclose(fp);
			empty_env_list(env);
			return (1);
		}

		if (cflag) {
			break;
		}
	}

	// The command should be specified
	if (!cflag) {
		print_usage();
		(void) fclose(fp);
		empty_env_list(env);
		return (1);
	}

	// Build the command from the rest of the arguments
	for (uint_t ind = 0; ind < (uint_t)(argc-optind); ind++) {
		if ((os::strlen(cmd_string) + 1 +
		    // lint says cmd_string might not be initialized
		    //lint -e645
		    os::strlen(argv[(uint_t)optind+ind])) > MAX_BUF_LEN) {
		    //lint +e645
			// command length too long
			(void) fclose(fp);
			empty_env_list(env);
			return (1);
		}
		(void) strcat(cmd_string, " ");
		(void) strcat(cmd_string, argv[(uint_t)optind+ind]);
	}

	//
	// We pass on the signal mask of the process that forked us
	// (the same signal mask is inherited by us as well)
	// to pmmd. pmmd will set up this signal mask for the child
	// process that it forks off.
	//
	sigset_t blocked_signal_set;
	if (sigprocmask(SIG_BLOCK, NULL, &blocked_signal_set) != 0) {
		err = errno;
		(void) fprintf(fp, "sigprocmask(2) failed to query current "
		    "signal mask, error %s\n", strerror(err));

		os::sc_syslog_msg logger(PROG_NAME, "", NULL);
		//
		// SCMSGS
		// @explanation
		// pmmd_adm was unable to query its current signal mask
		// because the sigprocmask(2) function failed.
		// The message contains the system error.
		// pmmd_adm cannot do its requested action.
		// @user_action
		// Save a copy of the /var/adm/messages files on this node,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "sigprocmask(2) failed for pmmd_adm while querying "
		    "current signal mask: error %s", strerror(err));

		(void) fclose(fp);
		empty_env_list(env);
		return (err);
	}

	//
	// The process that forked us could have decided to ignore certain
	// signals. We would have inherited the same signal disposition.
	// We pass on this ignored signal set to pmmd.
	// pmmd will set the signal disposition of the child process
	// (that it forks off) for signals in this signal set to SIG_IGN.
	//

	// Initialize a signal set that will be the set of ignored signals
	sigset_t ignored_signal_set;
	if (sigemptyset(&ignored_signal_set) != 0) {
		err = errno;
		(void) fprintf(fp, "sigemptyset(3C) failed to initialize "
		    "the ignored signal set, error %s\n", strerror(err));

		os::sc_syslog_msg logger(PROG_NAME, "", NULL);
		//
		// SCMSGS
		// @explanation
		// pmmd_adm was unable to initialize a signal set
		// because the sigemptyset(3C) function failed.
		// The message contains the system error.
		// pmmd_adm cannot do its requested action.
		// @user_action
		// Save a copy of the /var/adm/messages files on this node,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "sigemptyset(3C) failed for pmmd_adm while initializing "
		    "ignored signal set: error %s", strerror(err));

		(void) fclose(fp);
		empty_env_list(env);
		return (err);
	}

	//
	// Find out which signals are ignored and
	// add such signals to the ignored signal set.
	//
	for (int sig = 1; sig <= SIGRTMAX; sig++) {
		//
		// Try converting the signal to its name.
		// If the signal number does not correspond to a valid signal,
		// then this conversion will fail; ignore such a signal number.
		// Of course, the assumption here is that
		// sig2str() works correctly.
		//
		char sig_name[SIG2STR_MAX];
		if (sig2str(sig, (char *)sig_name) != 0) {
			continue;
		}

		// Query the sigaction for this signal
		struct sigaction action;
		(void) memset(&action, 0, sizeof (struct sigaction));
		if (sigaction(sig, NULL, &action) != 0) {
			err = errno;
			(void) fprintf(fp, "sigaction(2) failed to query "
			    "disposition for signal SIG%s, error %s\n",
			    sig_name, strerror(err));

			os::sc_syslog_msg logger(PROG_NAME, "", NULL);
			//
			// SCMSGS
			// @explanation
			// pmmd_adm was unable to query the signal disposition
			// for a signal number because the sigaction(2)
			// function failed.
			// The message contains the system error.
			// pmmd_adm cannot do its requested action.
			// @user_action
			// Save a copy of the /var/adm/messages files
			// on this node, and report the problem to
			// your authorized Sun service provider.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "sigaction(2) failed for pmmd_adm while querying "
			    "disposition for signal SIG%s: error %s",
			    sig_name, strerror(err));

			(void) fclose(fp);
			empty_env_list(env);
			return (err);
		}

		if (action.sa_handler != SIG_IGN) {
			continue;
		}

		//
		// This signal is set to be ignored.
		// Add the signal number to the ignored signal set.
		//
		if (sigaddset(&ignored_signal_set, sig) != 0) {
			err = errno;
			(void) fprintf(fp, "sigaddset(3C) failed to add "
			    "signal SIG%s to a signal set, error %s\n",
			    sig_name, strerror(err));

			os::sc_syslog_msg logger(PROG_NAME, "", NULL);
			//
			// SCMSGS
			// @explanation
			// pmmd_adm was unable to add a signal number
			// to a signal set, because the sigaddset(3C)
			// function failed.
			// The message contains the system error.
			// pmmd_adm cannot do its requested action.
			// @user_action
			// Save a copy of the /var/adm/messages files
			// on this node, and report the problem to
			// your authorized Sun service provider.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "sigaddset(3C) failed for pmmd_adm while adding "
			    "signal SIG%s to ignored signal set: error %s",
			    sig_name, strerror(err));

			(void) fclose(fp);
			empty_env_list(env);
			return (err);
		}
	}

	//
	// Print out all information that we send to door server
	//

	for (int sig = 1; sig <= SIGRTMAX; sig++) {
		if (sigismember(&blocked_signal_set, sig)) {
			char sig_name[SIG2STR_MAX];
			if (sig2str(sig, (char *)sig_name) != 0) {
				//
				// Since we got the signal mask
				// from sigprocmask(2),
				// the signal should be a valid signal.
				// Else either signals API or pmmd_adm
				// has a bug; we should not proceed.
				//
				ASSERT(0);

				(void) fprintf(fp, "sig2str(3C) failed "
				    "for signal %d that is part of "
				    "blocked signal set\n", sig);

				os::sc_syslog_msg logger(PROG_NAME, "", NULL);
				//
				// SCMSGS
				// @explanation
				// pmmd_adm was unable to convert a valid
				// signal number to its signal name,
				// because the sig2str(3C) function failed.
				// @user_action
				// Save a copy of the /var/adm/messages files
				// on this node, and report the problem to
				// your authorized Sun service provider.
				//
				(void) logger.log(LOG_ERR, MESSAGE,
				    "sig2str(3C) failed for pmmd_adm while "
				    "converting signal number %d "
				    "to a signal name.", sig);

				(void) fclose(fp);
				empty_env_list(env);
				return (1);
			}

			(void) fprintf(fp, "signal SIG%s blocked\n", sig_name);
		}
	}

	for (int sig = 1; sig <= SIGRTMAX; sig++) {
		if (sigismember(&ignored_signal_set, sig)) {
			char sig_name[SIG2STR_MAX];
			if (sig2str(sig, (char *)sig_name) != 0) {
				//
				// We must be having a valid signal
				// in the ignored signal set.
				// Else either signals API or pmmd_adm
				// has a bug; we should not proceed.
				//
				ASSERT(0);

				(void) fprintf(fp, "sig2str(3C) failed "
				    "for signal %d that is part of "
				    "ignored signal set\n", sig);

				os::sc_syslog_msg logger(PROG_NAME, "", NULL);
				(void) logger.log(LOG_ERR, MESSAGE,
				    "sig2str(3C) failed for pmmd_adm while "
				    "converting signal number %d "
				    "to a signal name.", sig);

				(void) fclose(fp);
				empty_env_list(env);
				return (1);
			}

			(void) fprintf(fp, "signal SIG%s ignored\n", sig_name);
		}
	}

	(void) fprintf(fp, "cmd_string is %s\n", cmd_string);
	(void) fprintf(fp, "process is %s\n", process_name);
	(void) fprintf(fp, "zone is %s\n", zonename);
	(void) fprintf(fp, "environment vars are :\n");
	env.atfirst();
	char *env_varp = NULL;
	for (uint_t ind = 0; (env_varp = env.get_current()) != NULL;
	    env.advance(), ind++) {
		(void) fprintf(fp, "%s\n", env_varp);
	}

	// Setup door args
	uint_t data_len =
	    (sizeof (sigset_t) * 2) +	// Blocked and ignored signal sets
	    os::strlen(process_name) + 1 +
	    os::strlen(zonename) + 1 +
	    os::strlen(cmd_string) + 1;
	env.atfirst();
	for (uint_t ind = 0; (env_varp = env.get_current()) != NULL;
	    env.advance(), ind++) {
		data_len += os::strlen(env_varp) + 1;
	}
	uint_t len = 0;
	char *datap = new char[data_len];
	char *tmp_datap = datap;

	// Blocked signal set
	len = sizeof (sigset_t);
	(void) memcpy((void *)tmp_datap,
	    (const void *)&blocked_signal_set, (size_t)len);
	tmp_datap += len;

	// Ignored signal set
	len = sizeof (sigset_t);
	(void) memcpy((void *)tmp_datap,
	    (const void *)&ignored_signal_set, (size_t)len);
	tmp_datap += len;

	// Process name
	len = os::strlen(process_name);
	(void) os::strncpy(tmp_datap, process_name, len);
	tmp_datap[len] = '\0';
	tmp_datap += (len + 1);

	// Zone name
	len = os::strlen(zonename);
	(void) os::strncpy(tmp_datap, zonename, len);
	tmp_datap[len] = '\0';
	tmp_datap += (len + 1);

	// Command string
	len = os::strlen(cmd_string);
	(void) os::strncpy(tmp_datap, cmd_string, len);
	tmp_datap[len] = '\0';
	tmp_datap += (len + 1);

	// Environment variables
	env.atfirst();
	while ((env_varp = env.get_current()) != NULL) {
		env.advance();
		len = os::strlen(env_varp);
		(void) os::strncpy(tmp_datap, env_varp, len);
		tmp_datap[len] = '\0';
		tmp_datap += (len + 1);
	}

	// Make a door call
	door_arg_t door_args;
	struct ret_data_t {
		int err;
		pid_t pid;
	};
	struct ret_data_t rbuf;
	int door_fd = open(PMMD_DOOR_FILE_NAME, O_RDONLY);
	if (door_fd < 0) {
		(void) fprintf(fp, "could not open door %s errno %d\n",
		    PMMD_DOOR_FILE_NAME, errno);
		(void) fclose(fp);
		delete [] datap;
		empty_env_list(env);
		return (errno);
	}
	door_args.data_ptr = datap;
	door_args.data_size = data_len;
	door_args.desc_ptr = NULL;
	door_args.desc_num = 0;
	door_args.rbuf = (char *)&rbuf;
	door_args.rsize = sizeof (struct ret_data_t);

	if (door_call(door_fd, &door_args) == -1) {
		(void) close(door_fd);
		door_fd = -1;
		// XXX should retry in case error is EINTR
		(void) fprintf(fp, "error in door call errno %d\n", errno);
		(void) fclose(fp);
		delete [] datap;
		empty_env_list(env);
		return (errno);
	}
	(void) close(door_fd);
	door_fd = -1;

	rbuf = *((struct ret_data_t *)((void *)door_args.rbuf));
	if (rbuf.err != 0) {
		(void) fprintf(fp, "error received from door server, err %d\n",
		    rbuf.err);
		(void) fclose(fp);
		delete [] datap;
		empty_env_list(env);
		return (rbuf.err);
	}

	(void) fprintf(fp, "successfully forked program %s, pid %d\n",
	    cmd_string, rbuf.pid);

	char *cmd_string_copyp = os::strdup(cmd_string);
	if (cmd_string_copyp == NULL) {
		(void) fprintf(fp,
		    "memory allocation failure for cmd_string_copyp\n");
		(void) fclose(fp);
		delete [] datap;
		empty_env_list(env);
		return (1);
	}
	char *wait_exec_namep = strtok(cmd_string_copyp, " ");
	if (wait_exec_namep == NULL) {
		(void) fprintf(fp, "command string strtok failed\n");
		(void) fclose(fp);
		delete [] datap;
		empty_env_list(env);
		return (1);
	}

	int retval = wait_for_launched_process_to_exec(
	    rbuf.pid, basename(wait_exec_namep));
	if (retval) {
		(void) fprintf(fp, "wait_for_launched_process_to_exec "
		    "returned error %d\n", retval);
	} else {
		(void) fprintf(fp, "successfully executed program %s, pid %d\n",
		    cmd_string, rbuf.pid);
	}

	// XXX check if we need to free some memory
	delete [] wait_exec_namep;
	(void) fclose(fp);
	delete [] datap;
	empty_env_list(env);
	return (retval);
}
//lint +e550
