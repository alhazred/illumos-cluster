//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)zc_member.cc	1.6	08/08/07 SMI"

#include <orb/infrastructure/orb.h>
#include <sys/os.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <cmm/cmm_ns.h>
#include <h/membership.h>
#include <zone.h>
#include <sys/clconf_int.h>

//
// This process is launched by "sc_zc_member" SMF service
// inside a zone of a zone cluster, when the zone boots up.
// This process notifies the kernel membership subsystem
// on the local base node saying that the zone has come up.
//

//
// zc_member main routine
//
int
main(int, char * const argv[])
{
	Environment e;
	char zonename[ZONENAME_MAX];
	os::sc_syslog_msg msglog("", argv[0], NULL);

	if (clconf_lib_init() != 0) {
		//
		// SCMSGS
		// @explanation
		// This process failed to initialize clconf.
		// Support for the current zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msglog.log(SC_SYSLOG_PANIC, MESSAGE,
		    "zc_member(%d): Could not initialize clconf. Exiting.\n",
		    getpid());
		exit(1);
	}

	//
	// Exit with success without any action,
	// if running in the global zone or a native non-global zone.
	//
	if (!clconf_inside_virtual_cluster()) {
		// Running in global zone or native non-global zone
		return (0);
	}

	if (getzonenamebyid(getzoneid(), zonename, ZONENAME_MAX) == -1) {
		//
		// SCMSGS
		// @explanation
		// Failed to get the current zonename.
		// Support for the current zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msglog.log(SC_SYSLOG_PANIC, MESSAGE,
		    "zc_member(%d): Could not get zone name. Exiting.\n",
		    getpid());
		exit(1);
	}

	// Inform the membership engine that this zone has booted up
	uint32_t clid = 0;
	if (clconf_get_cluster_id(zonename, &clid)) {
		// Must be able to convert cluster name to cluster id
		ASSERT(0);
	}
	membership::engine_var engine_v = cmm_ns::get_membership_engine_ref();
	ASSERT(!CORBA::is_nil(engine_v));
	membership::engine_event_info event_info;
	event_info.mem_manager_info.mem_type = membership::ZONE_CLUSTER;
	event_info.mem_manager_info.cluster_id = clid;
	//
	// Casting as char* while assigning to string field does not
	// allocate fresh memory
	//
	event_info.mem_manager_info.process_namep = (char *)NULL;
	event_info.event = membership::NODE_UP;
	engine_v->deliver_event_to_engine(
	    membership::DELIVER_MEMBERSHIP_CHANGE_EVENT, event_info, e);
	if (e.exception() != NULL) {
		const char *exp_namep = (e.exception()->_name())?
		    (e.exception()->_name()) : "NULL";
		//
		// SCMSGS
		// @explanation
		// Failed to notify the cluster membership infrastructure
		// that this zone (that is part of a zone cluster) came up.
		// Support for this zone cluster might not work properly.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) msglog.log(SC_SYSLOG_PANIC, MESSAGE,
		    "zc_member(%d): Failure in notifying membership subsystem "
		    "that zone %s came up, exception <%s> thrown. Exiting.\n",
		    getpid(), zonename, exp_namep);
		e.clear();
		exit(1);
	}

	return (0);
}
