//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)safe_to_takeover.cc	1.8	08/05/20 SMI"

// Utility to check if it is safe to takeover HA services
// from ANY node.
// Used by LogicalHostname and SharedAddress implementations to
// bypass ping checks if it is safe to takeover from any node
// in the cluser.

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/os.h>
#include <sys/clconf_int.h>

#include <h/cmm.h>

#include <cmm/cmm_ns.h>
#include <rgm/sczones.h>

// exit codes
// 0 ==> Safe to takeover
// 1 ==> Not safe
// 2 ==> Internal error
// It is intended that the caller would consider exit code
// 2 to also mean, go ahead and do full check.

int
main(int argc)
{
	if (argc != 1) {
		exit(2);
	}

	int error = clconf_lib_init();

	if (error) {
		exit(2);
	}

	cmm::control_var		ctrlp;
	ctrlp = cmm_ns::get_control();
	if (CORBA::is_nil(ctrlp)) {
		exit(2);
	}

	int rc = 0;
	for (unsigned int i = 1; i < NODEID_MAX + 1; i++) {
		Environment e;
		bool safe;

		safe = ctrlp->safe_to_takeover_from(i, false, e);
		if (e.exception()) {
			e.clear();
			rc = 2;
			break;
		}
		if (!safe) {
			// Not Safe to take over from i
			rc = 1;
			break;
		}
	}
	return (rc);
}
