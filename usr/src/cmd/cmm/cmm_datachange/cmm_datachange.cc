/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cmm_datachange.cc	1.3	08/05/20 SMI"

//
// This utility removes the entries with keys suffixed by "cmm_version" in
// the infrastructure table of the CCR. This utility works in non-cluster
// mode.
//
#include <ccr_access.h>
#include <sys/sol_version.h>
#include <sys/os.h>
#include <sys/cladm_int.h>

#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#define	CCR_TABLE "infrastructure"
#define	MAXNODES 64

#define	ERR_NOERR	0 /* No error was found */
#define	ERR_INIT	1 /* Table initialization failed! */
#define	ERR_UPDATE	2 /* Update of a key element failed */
#define	ERR_COMMIT	3 /* Commit failed */
#define	ERR_PERM	4 /* Not allowed to run command */
#define	ERR_CLADM	5 /* cladm encountered an error */

static int sc_zonescheck(void);

int
main()
{
	ccr_access_readonly_table	rd_tab;
	ccr_access_updatable_table	updat_tab;
	char				key[256];
	uint_t				indx;
	int				err = 0;
	char				*version_key = NULL;
	bool				version_exists[MAXNODES + 1];
	bool				version_commit = false;
	int				bootflags = 0;

	//
	// Check to see if being run from non-global zone
	//
	if (sc_zonescheck() != 0) {
		return (ERR_PERM);
	}

	//
	// Check to see if being run in cluster mode.
	//

	// Get the boot flags
	if (os::cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) {
		return (ERR_CLADM);
	}
	if (bootflags & CLUSTER_BOOTED) {
		return (ERR_PERM);
	}

	//
	// Opens the infrastructure table for reading.
	//
	err = rd_tab.initialize(CCR_TABLE);
	if (err != 0) {
		return (ERR_INIT);
	}
	for (indx = 1; indx <= MAXNODES; indx++) {
		version_exists[indx] = false;
	}

	//
	// Read the table and store the information if each node
	// has the version key in the CCR table.
	//
	for (indx = 1; indx <= MAXNODES; indx++) {
		(void) sprintf(key, "cluster.nodes.%d.cmm_version", indx);
		version_key = rd_tab.query_element(key);
		if (version_key != NULL) {
			delete [] version_key;
			version_exists[indx] = true;
		}
	}
	//
	// Check to see if the version commit has happened on the cluster.
	//
	version_key = rd_tab.query_element("cluster.properties.cmm_version");
	if (version_key != NULL) {
		delete [] version_key;
		version_commit = true;
	}
	rd_tab.close();

	//
	// Opens the infrastructure table for modification.
	//
	err = updat_tab.initialize(CCR_TABLE);

	if (err != 0) {
		return (ERR_INIT);
	}
	for (indx = 1; indx <= MAXNODES; indx++) {
		//
		// We have to remove the version key entry only if it exists.
		//
		if (version_exists[indx]) {
			(void) sprintf(key, "cluster.nodes.%d.cmm_version",
			    indx);
			err = updat_tab.remove_element(key);
			if (err != 0) {
				return (ERR_UPDATE);
			}
		}
	}
	//
	// We have to remove the version commit key entry only if it exists.
	//
	if (version_commit) {
		err = updat_tab.remove_element(
		    "cluster.properties.cmm_version");
		if (err != 0) {
			return (ERR_UPDATE);
		}
	}
	if (updat_tab.commit() != 0) {
		return (ERR_COMMIT);
	}
	return (ERR_NOERR);
}

//
// Check to see if the binary is being run from within a non-global
// zone.
// Return Value:
//	ERR_NOERR If run from the global zone.
//	ERR_PERM  If run from a non-global zone.
//
static int
sc_zonescheck()
{
#if SOL_VERSION >= __s10
	zoneid_t zoneid;

	if ((zoneid = getzoneid()) < 0) {
		return (ERR_PERM);
	}
	if (zoneid != 0) {
		return (ERR_PERM);
	}
#endif
	return (ERR_NOERR);
}
