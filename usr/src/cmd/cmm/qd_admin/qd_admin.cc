//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qd_admin.cc	1.12	09/02/01 SMI"

//
// 	qd_admin.cc
//
//	This file implements "qd_admin" command and is used to scrub PGRe keys,
//	for scsi2 and software_quorum devices and scsi3 keys for scsi3 devices.
//	The command reads PGRe keys and PGRe reservations on any Sun supported
//	device.
//


#include <sys/os.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/clconf_int.h>
#include <quorum/common/quorum_pgre.h>
#include <quorum/common/shared_disk_util.h>
#include <quorum/common/type_registry.h>
#include <orb/invo/corba.h>
#include <orb/infrastructure/orb.h>
#include <nslib/ns.h>
#include <h/quorum.h>
#include <rgm/sczones.h>

#define	INIT_KEYS	NODEID_MAX	// initial number of reservation keys
#define	QD_ADMIN_CMD	"/usr/cluster/lib/sc/qd_admin"

// prints out usage message of quorum command
static void
print_usage()
{
	// provide usage for commands to inquire and clean
	(void) printf("\nUsage:\n"
	    "%s -c [quorum_scrub | quorum_inkeys | quorum_inresv] "
	    "-d device_path [-t device_type] \n", QD_ADMIN_CMD);
}

//lint -e1746

//
// do_quorum_inkeys
//
// Fills in "keylist" with the PGRe keys currently stored on a quorum device.
//
//
int
do_quorum_inkeys(quorum::quorum_device_type_var qd_v) /*lint !e1746 */
{
	int		error = 0;
	quorum::reservation_list_t	reg_list;
	Environment env;

	// intialize the reg_list structure
	reg_list.listsize = INIT_KEYS;
	reg_list.listlen = 0;
	reg_list.keylist.length(INIT_KEYS);

	if (reg_list.keylist.length() != INIT_KEYS) {
		(void) printf("Memory allocation failure.\n");
		return (1);
	}

	error = (int)qd_v->quorum_read_keys(reg_list, env);
	if (error || env.exception()) {
		(void) printf("Error reading quorum keys\n");
		env.clear();
		return (error);
	}

	if (reg_list.listlen == 0) {
		(void) printf("No keys registered.\n");
	}

	for (uint_t i = 0; i < reg_list.listlen; i++) {
		uint64_t	keyval = 0;
		for (uint_t j = 0; j < MHIOC_RESV_KEY_SIZE; j++) {
			keyval = (keyval << 8) + reg_list.keylist[i].key[j];
		}

		(void) printf("key[%d]=0x%llx.\n", i, keyval);
	}

	return (error);
}

//
// do_quorum_inresv
//
// Fills in "resvlist" with the reserved PGRe key currently stored on a
// quorum device.
//
//
int
do_quorum_inresv(const quorum::quorum_device_type_var qd_v) /*lint !e1746 */
{
	int			error = 0;
	quorum::reservation_list_t	resv_list;

	// intialize the resv_list structure
	resv_list.listsize = INIT_KEYS;
	resv_list.listlen = 0;
	resv_list.keylist.length(INIT_KEYS);

	if (resv_list.keylist.length() != INIT_KEYS) {
		(void) printf("Memory allocation failure.\n");
		return (1);
	}

	Environment env;
	error = (int)qd_v->quorum_read_reservations(resv_list, env);
	if (error || env.exception()) {
		(void) printf("Error reading quorum reservations\n");
		env.clear();
		return (error);
	}

	if (resv_list.listlen == 0) {
		(void) printf("\nNo reservations on the device.\n");
	} else {
		uint64_t	key = 0;
		for (uint_t j = 0; j < MHIOC_RESV_KEY_SIZE; j++) {
			key = (key << 8) + resv_list.keylist[0].key[j];
		}

		(void) printf("resv[0]: key=0x%llx.\n", key);
	}

	return (error);
}

//lint +e1746

int
main(int argc, char **argv)
{
	char *option = NULL;
	int c;
	char *zdevice = NULL;
	int ret_value = 0;
	bool scrub = false;
	char *type = NULL;

	if (sc_zonescheck() != 0) {
		return (1);
	}

	// get args
	while ((c = getopt(argc, argv, "c:d:t:")) != EOF) {
		switch (c) {
		case 'c' :
			// command to execute
			option = optarg;
			break;
		case 'd' :
			// device to query
			zdevice = optarg;
			break;
		case 't':
			// type of device
			type = optarg;
			break;
		default :
			(void) printf("Illegal option '%c'.\n", optopt);
			print_usage();
			exit(1);
		}
	}

	// check for required args
	if (option == NULL) {
		(void) printf("%s: option not specified.\n", QD_ADMIN_CMD);
		print_usage();
		exit(1);
	}
	if (zdevice == NULL) {
		(void) printf("%s: device not specified.\n", QD_ADMIN_CMD);
		print_usage();
		exit(1);
	}

	if (type == NULL) {
		// Device type not specified, default to SCSI2
		type = strdup("scsi2");
	}

	// Check that a supported type has been passed in as argument
	bool supported = false;
	const char *quorum_types_supported[5] =
	    {"scsi2", "scsi3", "sq_disk", "netapp_nas", "quorum_server"};
	for (unsigned int ind = 0; ind < 5; ind++) {
		if (os::strcmp(type, quorum_types_supported[ind]) == 0) {
			supported = true;
			break;
		}
	}
	if (!supported) {
		(void) printf("%s: unrecognized device type '%s' specified.\n",
		    QD_ADMIN_CMD, type);
		exit(1);
	}

	// are we issuing a option that is not valid
	if ((strcmp(option, "quorum_scrub") != 0) &&
	    (strcmp(option, "quorum_inkeys") != 0) &&
	    (strcmp(option, "quorum_inresv") != 0)) {
		(void) printf("%s: unrecognized option '%s'.\n",
		    QD_ADMIN_CMD,
		    option);
		print_usage();
		exit(1);
	}

	//
	// "scrub" operation is not supported for qd of type
	// "quorum_server".
	//
	if ((strcmp(option, "quorum_scrub") == 0) &&
	    (strcmp(type, "quorum_server") == 0)) {
		(void) printf("%s: Option '%s' is not supported with device "
		    "type '%s'.\n", QD_ADMIN_CMD, option, type);
		exit(1);
	}

	if ((ORB::initialize()) != 0) {
		(void) printf("ERROR : can't initialize ORB\n");
		return (1);
	}
	// Get a reference to the quorum device type registry.
	naming::naming_context_var	ctxp;
	Environment			e;
	quorum::device_type_registry_var	type_registry_v;

	ctxp = ns::local_nameserver();
	CORBA::Object_var	tr_v = ctxp->resolve("type_registry", e);
	if (e.exception()) {
		(void) printf("Could not get reference to the type registry."
		    "\n");
		e.clear();
		exit(1);
	}
	type_registry_v = quorum::device_type_registry::_narrow(tr_v);

	// Ask the type registry for a reference to the quorum device type.
	quorum::quorum_device_type_var	qd_v;
	qd_v = type_registry_v->get_quorum_device(type, e);
	if (e.exception()) {
		(void) printf("Type registry returned exception instead of "
		    "quorum device reference.\n");
		e.clear();
		exit(1);
	}

	// Figure out whether we should be scrubbing the device.
	if (strcmp(option, "quorum_scrub") == 0) {
		scrub = true;
	}

	// open the device, get file descriptor.
	quorum::quorum_error_t	q_error;
	q_error = qd_v->quorum_open(zdevice, scrub, NULL, e);

	// Deal with scrubbing related error/non-error messages.
	if (scrub) {
		if (q_error == 0) {
			(void) printf("\nScrubbing complete. "
			    "Use '%s -c quorum_inkeys -d %s' \n "
			    "to verify success.\n", QD_ADMIN_CMD, zdevice);
			ret_value = 0;
		} else {
			(void) printf("\n%s -c %s -d %s command failed, "
			    "errno = %d.\n", QD_ADMIN_CMD, option, zdevice,
			    (int)q_error);
			ret_value = 1;
		}
		(void) qd_v->quorum_close(e);
		e.clear();
		exit(ret_value);
	// Other open errors. EACCESS error is ok with quorum server type.
	} else if (e.exception() ||
	    (strcmp(type, "quorum_server") ? q_error :
	    q_error == quorum::QD_EIO)) {
		(void) printf("Could not open device '%s', "
		    "errno = %d.\n%s -c %s -d %s command failed.\n",
		    zdevice, q_error, QD_ADMIN_CMD, option, zdevice);
		e.clear();
		(void) qd_v->quorum_close(e);
		e.clear();
		exit(1);
	}

	if (strcmp(option, "quorum_inkeys") == 0) {

		if ((ret_value = do_quorum_inkeys(qd_v)) != 0) {
			(void) printf("\n%s -c %s -d %s command failed "
			    "errno = %d.\n", QD_ADMIN_CMD, option, zdevice,
			    ret_value);
		}

	} else if (strcmp(option, "quorum_inresv") == 0) {

		if ((ret_value = do_quorum_inresv(qd_v)) != 0) {
			(void) printf("\n%s -c %s -d %s command failed "
			    "errno = %d.\n", QD_ADMIN_CMD, option, zdevice,
			    ret_value);
		}
	}

	// clean up
	(void) qd_v->quorum_close(e);
	e.clear();

	return (ret_value);
}
