/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	QD_USERD_H
#define	QD_USERD_H

#pragma ident	"@(#)qd_userd.h	1.6	08/05/20 SMI"

/*
 * qd_userd_cmd_ret structure is used by qd_user daemon
 * to return the status (cmd_ret) and output (full_buf)
 * of command lines its been requested to execute by
 * callers.
 */
typedef struct qd_userd_cmd_ret {
	int64_t	cmd_ret;
	char full_buf[MAX_CMD_LEN * 64];
} qd_userd_ret_t;

#endif // QD_USERD_H
