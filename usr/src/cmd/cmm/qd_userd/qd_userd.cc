/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)qd_userd.cc 1.8     08/05/20 SMI"

/*
 * QD_USER daemon
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <libintl.h>
#include <locale.h>
#include <alloca.h>
#include <errno.h>
#include <stropts.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <thread.h>
#include <synch.h>
#include <door.h>
#include <sys/door.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>
#include <syslog.h>
#include <sys/qd_userd.h>
#include <rgm/sczones.h>

/*
 * Log text messages
 */
#define	MUST_BE_ROOT	gettext("this program must be run as root\n")
#define	CD_ROOT_FAILED	gettext("chdir to root failed\n")
#define	DAEMON_RUNNING	gettext("QD_USER daemon already running\n")
#define	DOOR_FAILED	gettext("Failed creating qd_userd door\n")
#define	SIGACT_FAILED	\
		gettext("Failed to install signal handler for %s: %s\n")


/*
 * Global variables.
 */
thread_t last_exec_thread;

/*
 * Module Variables
 */
static	int		door_id = -1;

//
// qd_userd_door_handler
//
// This is the function which will process incomming door requests,
// execute the command line, and return the output to the user.
//
static void
qd_userd_door_handler(void *, char *argp, size_t,
    door_desc_t *, uint_t)
{

	if (last_exec_thread != 0) {
		//
		// A previous call was made and then aborted in the kernel.
		// Kill it now.
		//
		(void) thr_kill(last_exec_thread, SIGINT);
	}

	// Set the thread id so that this thread can be killed if it hangs.
	last_exec_thread = thr_self();

	// Set up the door argument using the passed in arguments.
	char *req = (char *)argp;

	if (req == NULL) {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_CRIT, "Error in qd_userd, NULL request\n");
		last_exec_thread = (thread_t)0;
		(void) door_return((char *)req, 0, NULL, 0);
	}
	char buf[MAX_CMD_LEN];

	qd_userd_ret_t ret_buf;
	char *fbuf = ret_buf.full_buf;
	FILE *file_ptr;
	bzero(ret_buf.full_buf, MAX_CMD_LEN * 64);
	//
	// Use popen to exec the command line that was passed in. Use
	// fgets and strcat to put together a single response string.
	//
	file_ptr = popen(req, "r");
	if (file_ptr != NULL) {
		while (fgets(buf, 1024, file_ptr) != NULL) {
			fbuf = strcat(fbuf, buf);
		}
		ret_buf.cmd_ret = pclose(file_ptr);
	} else {
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		syslog(LOG_CRIT, "qd_userd: Unable to create file handle\n");
		last_exec_thread = (thread_t)0;
		(void) door_return((char *)req, 0, NULL, 0);
	}
	char *response = (char *)&ret_buf;

	// Clear the threadid since we are about to return.
	last_exec_thread = (thread_t)0;

	(void) door_return(response, sizeof (qd_userd_ret_t), NULL, 0);
}

/*
 * "ping" to see if a daemon is already running
 */
static int
daemon_exists(void)
{
	int doorh;
	door_info_t dinfo;

	// Trying opening the door we are about to create.
	doorh = open(QD_USERD_DOOR, O_RDONLY);

	// If the open failed, no one is using the door.
	if (doorh < 0) {
		return (0);
	}

	// If the door_info call failed, no one is using the door.
	if (door_info(doorh, &dinfo) < 0) {
		(void) close(doorh);
		return (0);
	}

	// If the target of the door is this process, we're ok.
	if (dinfo.di_target == getpid()) {
		(void) close(doorh);
		return (0);
	}

	//
	// Otherwise, some other process is attached to the door, so
	// we don't want this process to try to attach to it.
	//
	(void) close(doorh);
	return (1);
}

/*
 * Create the qd_userd door
 */
static int
setup_door(void)
{
	struct stat	stbuf;

	//
	// Mask all the signals before doing the door_create so that
	// the door_call handling threads will not get signals.
	//
	sigset_t new_mask, orig_mask;
	(void) sigfillset(&new_mask);
	(void) thr_sigsetmask(SIG_SETMASK, &new_mask, &orig_mask);
	(void) sigprocmask(SIG_SETMASK, &new_mask, NULL);

	/*
	 * Create the door
	 */
	door_id = door_create(qd_userd_door_handler, (void *) NULL,
	    DOOR_UNREF);

	// Restore the thread signal property.
	(void) thr_sigsetmask(SIG_SETMASK, &orig_mask, NULL);

	if (door_id < 0) {
		return (-1);
	}

	if (stat(QD_USERD_DOOR, &stbuf) < 0) {
		int newfd;
		if ((newfd = creat(QD_USERD_DOOR, 0444)) < 0) {
			return (-1);
		}
		(void) close(newfd);
	}

	if (fattach(door_id, QD_USERD_DOOR) < 0) {
		if ((errno != EBUSY) ||
		    (fdetach(QD_USERD_DOOR) < 0) ||
		    (fattach(door_id, QD_USERD_DOOR) < 0)) {
			return (-1);
		}
	}

	return (0);
}

/*
 * Main function of qd_user daemon
 */
int
main(int, char **)
{
	struct	sigaction	act;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	if (getuid() != 0) {
		syslog(LOG_CRIT, MUST_BE_ROOT);
		return (0);
	}

	/*
	 * is there a daemon already running?
	 */

	if (daemon_exists()) {
		syslog(LOG_CRIT, DAEMON_RUNNING);
		exit(1);
	}

	/*
	 * Ignore SIGHUP until all the initialization is done.
	 */
	act.sa_handler = SIG_IGN;
	(void) sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	if (sigaction(SIGHUP, &act, NULL) == -1)
		syslog(LOG_ERR, SIGACT_FAILED, strsignal(SIGHUP),
		    strerror(errno));

	/* daemonize */
	if (fork() != 0) {
		exit(0);
	}
	if (chdir("/") == -1) {
		syslog(LOG_CRIT, CD_ROOT_FAILED);
		exit(1);
	}

	(void) setsid();
	(void) close(STDIN_FILENO);
	(void) close(STDOUT_FILENO);
	(void) close(STDERR_FILENO);
	(void) open("/dev/null", O_RDWR, 0);
	(void) dup2(STDIN_FILENO, STDOUT_FILENO);
	(void) dup2(STDIN_FILENO, STDERR_FILENO);
	openlog(QD_USERD, LOG_PID, LOG_DAEMON);

	// Setup the door for quorum calls.
	if (setup_door()) {
		syslog(LOG_CRIT, DOOR_FAILED);
		exit(1);
	}

	/*
	 * wait for requests
	 */
	for (;;) {
		(void) pause();
	}
}
