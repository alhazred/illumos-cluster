//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)qd_qs_helper.cc	1.10	08/05/20 SMI"

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>    /* sockaddr_in structure */
#include <unistd.h>
#include <strings.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <rgm/sczones.h>

static int getIpAddresses(char *hostname);

void
usage()
{
	(void) printf("Usage: 	qd_qs_helper -i <hostname,[hostname,...]>\n");
	(void) printf("	Get IP address for the given hostnames.\n");
	(void) printf("	qd_qs_helper -h\n\t Prints this message.");
	exit(1);
}

//
// This program takes a list of hostnames as parameter
// and returns a list of resolved IP addresses for each
// of them.
//
// It is meant to be a helper for Quorum Server kernel module.
//
int
main(int argc, char * const argv[])
{

	int c;
	int ret = 0;

	if (sc_zonescheck() != 0)
		return (1);

	while ((c = getopt(argc, argv, "i:h")) != EOF) {
		switch (c) {
		case 'i':
			ret = getIpAddresses(optarg);
			break;
		case 'h':
		case '?':
		default:
			usage();
			/* NOTREACHED */
		}
	}
	return (ret);
}

//
// This function builds and print a list of IP addresses for every
// entry in the hostname list passed as parameter.
//
int
getIpAddresses(char *hostnames)
{
	if (!hostnames) {
		usage();
	}

	char abuf[INET6_ADDRSTRLEN];
	int error_num;
	struct hostent *hostentp;
	char **addressp;
	char *namep;

	namep = strtok(hostnames, (const char *)",");

	while (namep != NULL) {

		hostentp = getipnodebyname(namep, AF_INET6,
		    AI_ALL | AI_ADDRCONFIG | AI_V4MAPPED, &error_num);
		if (hostentp == NULL) {
			if (error_num == TRY_AGAIN) {
				(void) printf("FAIL: %s: unknown host or "
				    "invalid literal address (try again "
				    "later)\n", namep);
			} else {
				(void) printf("FAIL: %s: unknown host or "
				    "invalid literal address\n", namep);
			}
			return (1);
		}
		for (addressp = hostentp->h_addr_list; *addressp != 0;
			addressp++) {
			struct in6_addr in6;

			bcopy(*addressp, (caddr_t)&in6,
			    (unsigned int)hostentp->h_length);
			(void) printf("%s,", inet_ntop(AF_INET6, (void *)&in6,
			    abuf, sizeof (abuf)));
		}
		freehostent(hostentp);
		namep = strtok((char *)NULL, ",");
		(void) printf("\n");
	}
	return (0);
}
