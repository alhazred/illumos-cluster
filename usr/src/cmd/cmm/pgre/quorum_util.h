/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PGRE_QUORUM_UTIL_H
#define	_PGRE_QUORUM_UTIL_H

#pragma ident	"@(#)quorum_util.h	1.4	08/05/20 SMI"

/* Interfaces to quorum device implementations */

#include <sys/quorum_int.h>
#include <h/cmm.h>
#include <h/ccr_data.h>
#include <h/quorum.h>

/*
 * The PGR Emulation (PGRE) area will be divided into (64 + 1) sectors,
 * with the first sector used for the group owner info, and the rest
 * for the 64 nodes info. In unode, each such sector can be simulated
 * by a corresp data structure. PGRE_NODE_AREA_SIZE represents the number
 * of sectors assigned per node.
 */
#define	PGRE_NODE_AREA_SIZE		1

#define	PGRE_GET_NODE_BLKNO(s, n)	(s + n * PGRE_NODE_AREA_SIZE)

/*
 * quorum_scsi2_get_sblkno
 *
 * Gets the starting block number for the PGRE reserved area on
 * the alternate cylinders, and caches it for future use.
 *
 */
extern int
quorum_scsi2_get_sblkno(void *, uint64_t&);

/*
 * quorum_scsi2_key_write
 *
 * Writes the specified key on the node area indicated by the starting
 * block number in the PGRE reserved area.
 *
 */
extern int
quorum_scsi2_key_write(void *, uint64_t, quorum::registration_key_t *, bool);

/*
 * quorum_scsi2_key_read
 *
 * Reads the key from the node area indicated by the starting block
 * number in the PGRE reserved area.
 *
 */
extern int
quorum_scsi2_key_read(void *, uint64_t, quorum::registration_key_t *);

/*
 * quorum_scsi2_key_find
 *
 * Searches for the given key in the PGRE reserved area.
 * Fills in the starting block number of the corresp node area.
 *
 */
extern int
quorum_scsi2_key_find(void *, uint64_t, quorum::registration_key_t *,
    uint64_t &);

/*
 * quorum_scsi2_key_delete
 *
 * Removes the key from the node area specified by the starting
 * block number - resets the key to null.
 *
 */
extern int
quorum_scsi2_key_delete(void *, uint64_t, bool);

/*
 * quorum_scsi2_choosing_write
 *
 * Writes the value of "choosing" (Lamport's ME variable) on the node
 * area indicated by the starting block number in the PGRE reserved area.
 *
 */
extern int
quorum_scsi2_choosing_write(void *, uint64_t, bool);

/*
 * quorum_scsi2_choosing_read
 *
 * Reads "choosing" from the node area indicated by the starting block
 * number in the PGRE reserved area.
 *
 */
extern int
quorum_scsi2_choosing_read(void *, uint64_t, bool &);

/*
 * quorum_scsi2_number_write
 *
 * Writes the value of "number" (Lamport's ME variable) on the node
 * area indicated by the starting block number in the PGRE reserved area.
 *
 */
extern int
quorum_scsi2_number_write(void *, uint64_t, uint64_t);

/*
 * quorum_scsi2_number_read
 *
 * Reads "number" from the node area indicated by the starting block
 * number in the PGRE reserved area.
 *
 */
extern int
quorum_scsi2_number_read(void *, uint64_t, uint64_t &);

/*
 * quorum_scsi_er_pair_write
 *
 * Writes the specified {epoch number, CMM reconfiguration number} pair to
 * the specified PGRe sector of the disk.
 */
extern int
quorum_scsi_er_pair_write(void *, uint64_t, ccr_data::epoch_type,
    cmm::seqnum_t, bool);

/*
 * quorum_scsi_er_pair_read
 *
 * Reads the {epoch number, CMM reconfiguration number} pair from
 * the specified PGRe sector of the disk.
 */
extern int
quorum_scsi_er_pair_read(void *, uint64_t, ccr_data::epoch_type &,
    cmm::seqnum_t &, bool);

/*
 * quorum_scsi2_sector_scrub
 *
 * Initializes the PGRE sector area in question.
 *
 */
extern int
quorum_scsi2_sector_scrub(void *, uint64_t, bool);

extern int NULL_KEY(const quorum::registration_key_t&);

#endif	/* _PGRE_QUORUM_UTIL_H */
