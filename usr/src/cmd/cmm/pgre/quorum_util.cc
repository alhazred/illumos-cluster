//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)quorum_util.cc	1.7	09/02/18 SMI"


//
// 	quorum_util.cc
//
//	This file implements the kernel-mode and user-mode interfaces to
//	quorum devices. The unode version of these interfaces are in the file
//	src/unode/quorum_util. The user-mode interfaces can be used when
//	_KERNEL is not defined. The user-mode functions are part of libucmstuff
//	library and are used by pgre command.
//

#include <sys/scsi/scsi.h>
#include <sys/scsi/impl/sense.h>
#include <sys/sysmacros.h>
#include <sys/dkio.h>
#include <sys/file.h>
#include <sys/vnode.h>
#include <sys/os.h>
#include <sys/clconf_int.h>
#include <sys/didio.h>
#include <orb/infrastructure/orb_conf.h>
#include <cmm/cmm_debug.h>
#include <quorum/common/quorum_pgre.h>
#include "quorum_util.h"
#include <sys/cl_efi.h>


int
NULL_KEY(const quorum::registration_key_t& key)
{
	for (int i = 0; i < MHIOC_RESV_KEY_SIZE; i++) {
		if (key.key[i] != 0)
			return (0);
	}
	return (1);
}

#ifdef SC_EFI_SUPPORT
//
// get_efi_reserved
//
// Gets the starting block number of the EFI reserved partition.
//
static int
get_efi_reserved(int fd, uint64_t &reserved_start)
{
	int			error = EINVAL;
	int			err;
	struct partition64	prt;
	struct uuid reserved    = EFI_RESERVED;

	prt.p_partno = 0;
	do {
		if ((err = ioctl(fd, DKIOCPARTITION, &prt)) == 0) {
			if (bcmp((void *)&prt.p_type,
			    (void *)&reserved, sizeof (reserved)) == 0) {
				reserved_start = (uint64_t)prt.p_start;
				return (0);
			}
			prt.p_partno++;
		} else {
			error = errno;
		}
	} while (err == 0);
	return (error);
}
#endif // SC_EFI_SUPPORT

//
// quorum_scsi2_get_sblkno
//
// Gets the starting block number for the PGRE reserved area on
// the alternate cylinders, and caches it for future use.
// This function can be used by both kernel-land and userland.
//
//
int
quorum_scsi2_get_sblkno(void *qhandle, uint64_t& sblkno)
{
	struct dk_geom	*disk_geom;
	uint64_t	num_cyl, nblks_per_cyl;
	uint64_t	partition_offset = 0;
	int 		error = 0;
	bool		retry_flag = true;
	int *fd	= (int *)qhandle;
	if (fd == NULL)
		return (-1);

	disk_geom =
	    (struct dk_geom *)malloc(sizeof (struct dk_geom));

	if (disk_geom == NULL)
		return (ENOMEM);

	//
	// The PGRE area is in the following portion of the reserved
	// cylinders (PSARC 1999/430):
	// 	PGRE_NUM_SECTORS sectors from the 2nd last track of
	// 	the first alternate cylinder.
	//
	// There is one set of reserved cylinders for each Solaris
	// partition on a disk. On SPARC architectures, dXs2 is the entire
	// Solaris partition, which is also the entire disk. On Intel
	// architectures, dXs2 is the entire (active) Solaris partition,
	// but there may be other partitions, and the first cylinder of the
	// disk will always be reserved for the fdisk partition table.
	//
	// To calculate the location of the PGRE area, we need to first
	// find the offset of the Solaris partition on the disk, and then
	// find the offset of the PGRE cylinders within the Solaris
	// partition.
	//
	// If the disk partition format is EFI (disks > 1 TB)
	//   The PGRE area are the sectors 15-79 of the EFI V_RESERVED
	//   partition (PSARC2001-570).
	//

#if defined(__i386) || defined(__amd64)

	//
	// TODO: DKIOCEXTPARTINFO is currently supported only
	// for nevada (s11). Once its back ported to s10, remove
	// the conditional compilation for s11.
	//

#if SOL_VERSION >= __s11

	//
	// PSARC 2008/336 introduced support for booting up disks
	// which are more than 1Tb in size. This introduced the
	// new ioctl DKIOCEXTPARTINFO.  Cluster needs to support
	// devices which are more than 1Tb in size. Hence,
	// we will first try DKIOEXTPARTINFO ioctl which replaces
	// the old ioctl DKIOCPARTINFO in order to get the partition
	// information of the disk.
	// Devices which do not support this new ioctl (DKIOCEXTPARTINFO)
	// will return error ENOTTY. For such devices we will
	// continue using the old ioctl DKIOCPARTINFO.
	//

	extpart_info ext_p_info;

	error = ioctl(*fd, DKIOCEXTPARTINFO, &ext_p_info);
	if (error == ENOTTY) {
		//
		// Device does not support the DKIOCEXTPARTINFO
		// ioctl. Retry with the old ioctl DKIOCPARTINFO
		//
		retry_flag = true;
	} else {
		if (error != 0) {
			//
			// Some other error has occured.
			//
			(void) printf(
			    "quorum_scsi2_get_sblkno: ioctl DKIOCEXTPARTINFO"
			    " returned error (%d).\n", error);
			free((caddr_t)disk_geom);
			return (error);
		} else {
			partition_offset = ext_p_info.p_start;

			//
			// No need to try the old ioctl
			//
			retry_flag = false;
		}
	}

#endif // SOL_VERSION >= __s11

	part_info	p_info;

	if (retry_flag) {
		error = ioctl(*fd, DKIOCPARTINFO, &p_info);
		if (error != 0) {
			(void) printf(
			    "quorum_scsi2_get_sblkno: ioctl DKIOCPARTINFO"
			    "returned error (%d).\n", error);
			free((caddr_t)disk_geom);
			return (error);
		} else {
			partition_offset = p_info.p_start;
		}
	}

#endif // (__i386) || (__amd64)

	error = ioctl(*fd, DKIOCGGEOM, disk_geom);
#ifdef SC_EFI_SUPPORT
	// ENOTSUP indicates an EFI disk
	if (error != 0 && errno != ENOTSUP) {
#else
	if (error != 0) {
#endif // SC_EFI_SUPPORT
		(void) printf("quorum_scsi2_get_sblkno: ioctl DKIOCGGEOM "
		    "returned error (%d).\n", error);
		free((caddr_t)disk_geom);
		return (error);
	}

#ifdef SC_EFI_SUPPORT
	// ENOTSUP indicates an EFI disk
	if (error != 0 && errno == ENOTSUP) {
		uint64_t	efi_rsvstart_blk;
		if ((error = get_efi_reserved(*fd, efi_rsvstart_blk)) != 0) {
			free((caddr_t)disk_geom);
			(void) printf("quorum_scsi2_get_sblkno: unable to find"
			    " EFI V_RESERVED partition returned error (%d).\n",
			    error);
			return (error);
		}
		//
		// PSARC2001-570: sectors 15-79 of EFI V_RESERVED
		// partition is the SC PGRE area. Sector 0 is fabricated devid.
		//
		// ----------------------------------------------------------
		//   ^<-------- SC PGRE: 15-79 ----------->
		//   |
		//   EFI V_RESERVED partition start, in number of sectors
		//
		sblkno = efi_rsvstart_blk + SC_EFI_RESERVED_START;
		free((caddr_t)disk_geom);
		return (0);
	}
#endif // SC_EFI_SUPPORT

	//
	// This check is probably not necessary, but it is done
	// in the code for writing devids etc.
	//
	if (disk_geom->dkg_acyl < 2) {
		(void) printf("quorum_scsi2_get_sblkno: Number of "
		    "alternate cylinders is less than 2.\n");
		error = EINVAL;
		free((caddr_t)disk_geom);
		return (error);
	}

	//
	// To determine starting block number, consider the following
	// layout for the first alternate cylinder (not to scale):
	//
	//  -----------------------------------------------------
	// |			| First Alternate Cylinder	|
	// | Data Cylinders	|-------------------------------|
	// |			| unused| PGRE area |devid area |
	// |			|	| (65 sects)| (a track )|
	//  -----------------------------------------------------
	//

	// The total number of data cylinders + the first alt cylinder.
	num_cyl = disk_geom->dkg_ncyl + disk_geom->dkg_acyl - 1;

	// The total number of sectors/blocks contained in a cylinder.
	nblks_per_cyl = (uint64_t)disk_geom->dkg_nsect * disk_geom->dkg_nhead -
	    disk_geom->dkg_apc;

	//
	// The starting block number =
	//	the offset of the start of this partition
	//	+ the total number of blocks incl the first alternate cyl
	//	- number of blocks reserved for the devid project
	//	  (= number of blocks on the last track)
	//	- number of blocks reserved for the SCSI-2 quorum project
	//	  (= PGRE_NUM_SECTORS).
	//
	sblkno = partition_offset + num_cyl * nblks_per_cyl -
	    disk_geom->dkg_nsect - PGRE_NUM_SECTORS;

	free((caddr_t)disk_geom);

	return (error);
}

//
// quorum_scsi2_sector_read
//
// Reads the blk of PGRE data specified into the pgre_sector_t
// area passed in (allocated).
// This function can be used by both kernel-land and userland.
//
static int
quorum_scsi2_sector_read(
    vnode_t		*vnode_ptr,
    uint64_t		blk,
    pgre_sector_t	*sector,
    bool		check_data)
{
	struct uscsi_cmd 	ucmd;
	union scsi_cdb		cdb;
	uint_t			chksum, *iptr;
	int			error = 0;
	int *fd	= (int*)(vnode_ptr);
	if (fd == NULL)
		return (-1);

	bzero((caddr_t)&ucmd, sizeof (ucmd));
	bzero((caddr_t)&cdb, sizeof (cdb));
	ucmd.uscsi_flags = USCSI_READ;
	ucmd.uscsi_bufaddr = (caddr_t)sector;
	ucmd.uscsi_buflen = DEV_BSIZE;
	ucmd.uscsi_flags |= USCSI_RQENABLE;

	ucmd.uscsi_rqbuf =
	    (caddr_t)malloc((size_t)SENSE_LENGTH);

	if (ucmd.uscsi_rqbuf == NULL)
		return (ENOMEM);

	ucmd.uscsi_rqlen = (uchar_t)SENSE_LENGTH;
	cdb.scc_cmd = SCMD_READ;

	if (blk < (2<<20)) {
		FORMG0ADDR(&cdb, (unsigned)blk); //lint !e704
		FORMG0COUNT(&cdb, (uchar_t)1);
		ucmd.uscsi_cdblen = CDB_GROUP0;
	} else {
		FORMG1ADDR(&cdb, (unsigned)blk); //lint !e704 !e734
		FORMG1COUNT(&cdb, (uchar_t)1); //lint !e572 !e778
		ucmd.uscsi_cdblen = CDB_GROUP1;
		cdb.scc_cmd |= SCMD_GROUP1;
	}

	ucmd.uscsi_cdb = (caddr_t)&cdb;

	error = ioctl(*fd, USCSICMD, &ucmd);
	if (error != 0) {
		(void) printf("quorum_scsi2_sector_read: ioctl USCSICMD "
		    "returned error (%d).\n", error);
		free(ucmd.uscsi_rqbuf);
		return (error);
	}

	//
	// Calculate and compare the checksum if check_data is true.
	// Also, validate the pgres_id string at the beg of the sector.
	//
	if (check_data) {
		PGRE_CALCCHKSUM(chksum, sector, iptr);

		// Compare the checksum.
		if (PGRE_GETCHKSUM(sector) != chksum) {
			(void) printf("quorum_scsi2_sector_read: "
			    "checksum mismatch.\n");
			free(ucmd.uscsi_rqbuf);
			return (EINVAL);
		}

		//
		// Validate the PGRE string at the beg of the sector.
		// It should contain PGRE_ID_LEAD_STRING[1|2].
		//
		if ((os::strncmp((char *)sector->pgres_id, PGRE_ID_LEAD_STRING1,
		    strlen(PGRE_ID_LEAD_STRING1)) != 0) &&
		    (os::strncmp((char *)sector->pgres_id, PGRE_ID_LEAD_STRING2,
		    strlen(PGRE_ID_LEAD_STRING2)) != 0)) {
			(void) printf("quorum_scsi2_sector_read: pgre id "
			    "mismatch. The sector id is %s.\n",
			    sector->pgres_id);
			free(ucmd.uscsi_rqbuf);
			return (EINVAL);
		}

	}
	free(ucmd.uscsi_rqbuf);

	return (error);
}

//
// quorum_scsi2_sectorid_write
//
// Writes the PGRE id string at the beginning of the sector.
//
static void
quorum_scsi2_sectorid_write(pgre_sector_t *sector, bool is_owner_area)
{
	if (is_owner_area) {
		(void) os::snprintf((char *)sector->pgres_id,
		    PGRE_ID_SIZE * sizeof (uchar_t),
		    "%s", PGRE_ID_LEAD_STRING1);
	} else {
		//
		// orb_conf calls need ORB support but user processes may
		// call this function in non-cluster mode as a result
		// calls to orb_conf are not made
		//
		(void) os::snprintf((char *)sector->pgres_id,
		    PGRE_ID_SIZE * sizeof (uchar_t),
		    "%s", PGRE_ID_LEAD_STRING2);
	}
}


//
// quorum_scsi2_sector_write
//
// Writes the pgre_sector_t area passed in to the specified blk of
// PGRE area on the reserved cylinders.
// This function can be used by both kernelland and userland.
//
//
static int
quorum_scsi2_sector_write(
    vnode_t		*vnode_ptr,
    uint64_t		blk,
    pgre_sector_t	*sector,
    bool		is_owner_area)
{
	struct uscsi_cmd 	ucmd;
	union scsi_cdb		cdb;
	uint_t			chksum, *iptr;
	int			error = 0;
	int *fd = (int *)vnode_ptr;
	if (fd == NULL)
		return (-1);

	// Fill in the id field of the sector.
	quorum_scsi2_sectorid_write(sector, is_owner_area);

	// Calculate the checksum.
	PGRE_CALCCHKSUM(chksum, sector, iptr);

	// Fill in the checksum.
	PGRE_FORMCHKSUM(chksum, sector);

	bzero((caddr_t)&ucmd, sizeof (ucmd));
	bzero((caddr_t)&cdb, sizeof (cdb));
	ucmd.uscsi_flags = USCSI_WRITE;
	ucmd.uscsi_bufaddr = (caddr_t)sector;
	ucmd.uscsi_buflen = DEV_BSIZE;
	ucmd.uscsi_flags |= USCSI_RQENABLE;

	ucmd.uscsi_rqbuf =
	    (caddr_t)malloc((size_t)SENSE_LENGTH);

	if (ucmd.uscsi_rqbuf == NULL)
		return (ENOMEM);

	ucmd.uscsi_rqlen = (uchar_t)SENSE_LENGTH;
	cdb.scc_cmd = SCMD_WRITE;

	if (blk < (2<<20)) {
		FORMG0ADDR(&cdb, (unsigned)blk); //lint !e704
		FORMG0COUNT(&cdb, (uchar_t)1);
		ucmd.uscsi_cdblen = CDB_GROUP0;
	} else {
		FORMG1ADDR(&cdb, (unsigned)blk); //lint !e704 !e734
		FORMG1COUNT(&cdb, (uchar_t)1); //lint !e572 !e778
		ucmd.uscsi_cdblen = CDB_GROUP1;
		cdb.scc_cmd |= SCMD_GROUP1;
	}

	ucmd.uscsi_cdb = (caddr_t)&cdb;

	error = ioctl(*fd, USCSICMD, &ucmd);
	free(ucmd.uscsi_rqbuf);
	if (error) {
		return (error);
	}

	//
	// Validate the pgre id string after every write.
	//
	pgre_sector_t *sector_chk =
	    (pgre_sector_t *)malloc((size_t)DEV_BSIZE);
	if (sector_chk == NULL) {
		return (ENOMEM);
	}
	int error_chk =
	    quorum_scsi2_sector_read(vnode_ptr, blk, sector_chk, true);

	if (error_chk != 0) {
		(void) printf("while checking written data, "
		    "scsi2 read returned error (%d).\n", error_chk);
		free((caddr_t)sector_chk);
		return (error_chk);
	}

	//
	// Do a byte comparison of the data that we wrote
	// and the data that we just read in.
	//
	if (bcmp(sector, sector_chk, (size_t)DEV_BSIZE)) {
		(void) printf(
		    "Data read in does not match data just written.\n");
		free((caddr_t)sector_chk);
		return (EINVAL);
	}

	(void) printf("PGRE id string written is %s\n", sector_chk->pgres_id);
	free((caddr_t)sector_chk);
	return (0);
}

//
// quorum_scsi2_key_read
//
// Reads the key from the node area indicated by the starting block
// number in the PGRE reserved area.
// This function can be used by both kernelland and userland.
//
//
int
quorum_scsi2_key_read(
    void 		*qhandle,
    uint64_t 		sblkno,
    quorum::registration_key_t	*key)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// Allocate the sector buffer.
	sector =
	(pgre_sector_t *)malloc((size_t)DEV_BSIZE);
	if (sector == NULL)
		return (ENOMEM);

	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;
	//
	// Read in the contents of the corresp sector on disk.
	// Need to check the validity of the data read, last arg true.
	//
	error = quorum_scsi2_sector_read(vnode_ptr, sblkno, sector, true);
	if (error != 0) {
		(void) printf("scsi2 read returned error (%d).\n", error);
	} else {
		*key = pgre_data_ptr->pgre_key;
	}

	free((caddr_t)sector);
	return (error);
}

//
// quorum_scsi2_sector scrub
//
// Initializes the PGRE sector area in question.
//
//
int
quorum_scsi2_sector_scrub(
    void 	*qhandle,
    uint64_t 	sblkno,
    bool	is_owner_area)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	int			error = 0;



	// Allocate the sector buffer.
	sector =
	    (pgre_sector_t *)malloc((size_t)DEV_BSIZE);

	if (sector == NULL)
		return (ENOMEM);

	bzero((caddr_t)sector, sizeof (pgre_sector_t)); // Redundant?
	error = quorum_scsi2_sector_write(vnode_ptr, sblkno, sector,
	    is_owner_area);

	if (error != 0) {
		(void) printf("quorum_scsi2_sector_scrub: "
		    "quorum_scsi2_sector_write returned error (%d).\n",
		    error);
	}

	free((caddr_t)sector);
	return (error);
}

//
// quorum_scsi_er_pair_write
//
// Writes the specified {epoch number, CMM reconfiguration number} pair to
// the specified PGRe sector of the disk.
//
int
quorum_scsi_er_pair_write(
    void			*qhandle,
    uint64_t			sblkno,
    ccr_data::epoch_type	epoch,
    cmm::seqnum_t		seqnum,
    bool			verbose)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// Allocate the sector buffer.
	sector =
	(pgre_sector_t *)malloc((size_t)DEV_BSIZE);
	if (sector == NULL)
		return (ENOMEM);

	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// First read in the contents of the corresp sector on disk.
	// No need to check the validity of the data.
	//
	error = quorum_scsi2_sector_read(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		(void) printf("quorum_scsi2_er_pair_write: "
		    "quorum_scsi2_sector_read returned error (%d).\n",
		    error);
		free((caddr_t)sector);
		return (error);
	}

	// Change the [E,R] pair as specified above.
	pgre_data_ptr->last_epoch = epoch;
	pgre_data_ptr->last_reconfig = seqnum;

	// Now write the contents of sector in the corresp blk on disk.
	error = quorum_scsi2_sector_write(vnode_ptr, sblkno, sector, false);
	if (error != 0) {
		(void) printf("quorum_scsi2_er_pair_write: "
		    "quorum_scsi2_sector_write returned error (%d).\n",
		    error);
	}

	if (verbose) {
		(void) printf("quorum_scsi2_er_pair_write: wrote epoch %ld and "
		    "seqnum %llu for sblkno (0x%llx).\n",
		    epoch, seqnum, sblkno);
	}

	free((caddr_t)sector);
	return (error);
}

//
// quorum_scsi_er_pair_read
//
// Reads the {epoch number, CMM reconfiguration number} pair from
// the specified PGRe sector of the disk.
//
int
quorum_scsi_er_pair_read(
    void			*qhandle,
    uint64_t			sblkno,
    ccr_data::epoch_type	&epoch,
    cmm::seqnum_t		&seqnum,
    bool			verbose)
{
	vnode_t			*vnode_ptr = (vnode_t *)qhandle;
	pgre_sector_t		*sector;
	pgre_data_t		*pgre_data_ptr;
	int			error = 0;

	// Allocate the sector buffer.
	sector =
	(pgre_sector_t *)malloc((size_t)DEV_BSIZE);
	if (sector == NULL)
		return (ENOMEM);

	pgre_data_ptr = (pgre_data_t *)sector->pgres_data;

	//
	// Read in the contents of the corresp sector on disk.
	// Need to check the validity of the data read, last arg true.
	//
	error = quorum_scsi2_sector_read(vnode_ptr, sblkno, sector, true);
	if (error != 0) {
		(void) printf("quorum_scsi2_er_pair_read: "
		    "quorum_scsi2_sector_read returned error (%d).\n",
		    error);
	} else {
		epoch = pgre_data_ptr->last_epoch;
		seqnum = pgre_data_ptr->last_reconfig;

		if (verbose) {
			(void) printf("quorum_scsi_er_pair_read: read epoch %ld"
			    " and seqnum %llu for sblkno (0x%llx).\n",
			    epoch, seqnum, sblkno);
		}
	}

	free((caddr_t)sector);
	return (error);
}
