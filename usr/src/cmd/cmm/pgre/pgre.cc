//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)pgre.cc 1.11     08/05/20 SMI"

//
// 	pgre.cc
//
//	This file implements "pgre" command and is used to scrub PGRe keys,
//	read PGRe keys and read PGRe reservations on any Sun supported disk.
//	PGRe related information(keys, reservations) are stored in the portion
//	of the alternate cylinders that has been reserved by the Sun Cluster
//	via PSARC 1999/430.
//


#include <h/quorum.h>
#include <sys/os.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/clconf_int.h>
#include "quorum_util.h"
#include <quorum/common/quorum_pgre.h>
#include <quorum/common/quorum_util.h>
#include <rgm/sczones.h>
#include "pgre.h"

#define	INIT_KEYS	NODEID_MAX	// initial number of reservation keys
#define	INIT_RESVS	1		// initial number of disk reservations
#define	PGRE_CMD	"/usr/cluster/lib/sc/pgre"

// prints out usage message of pgre command
static void
print_usage()
{
	// provide usage for commands to inquire and clean
	(void) printf("\nUsage:\n"
	    "%s -c [pgre_scrub | pgre_inkeys | pgre_inresv] "
	    "-d disk_path.\n", PGRE_CMD);
}


//
// do_pgre_inkeys
//
// Fills in "keylist" with the PGRe keys currently stored on a quorum device.
//
//
int
do_pgre_inkeys(
    void		*qhandle,
    quorum::reservation_list_t	*keylist)
{
	int		error;
	uint32_t	i;
	uint64_t	sblkno;

	//
	// Gets the starting block number for the PGRE reserved area on
	// the alternate cylinders.
	//
	error = quorum_scsi2_get_sblkno(qhandle, sblkno);

	ASSERT(error != -1);

	if (error != 0)
		return (error);

	//
	// Issue reads for all the valid entries on disk and fill in
	// the keylist data structure. Do this sector by sector here.
	//

	quorum::registration_key_t	node_key;
	size_t			key_size = sizeof (quorum::registration_key_t);

	keylist->listlen = 0;

	// Node areas start from the 2nd sector.
	for (i = 1; i <= NODEID_MAX; i++) {
		error = quorum_scsi2_key_read(qhandle,
		    PGRE_GET_NODE_BLKNO(sblkno, i), &node_key);

		ASSERT(error != -1);
		if (error == 0) {
			// Fill in key only if the key is not null.
			if (!NULL_KEY(node_key)) {
				uint_t llen = keylist->listlen;
				uint_t lsize = keylist->listsize;
				// Fill in the keys only if space avail.
				if (llen < lsize)
					bcopy(&node_key,
					    &(keylist->keylist[llen]),
					    key_size);
				keylist->listlen++;
			}
		} else {
			return (error);
		}
	}

	if (keylist->listlen == 0)
		(void) printf("No keys registered.\n");

	if (error == 0) {
		for (i = 0; i < keylist->listlen; i++) {
			(void) printf("key[%d]=0x%llx.\n", i,
				idl_key_to_int64_key(&(keylist->keylist[i])));
		}
	}
	return (error);
}

//
// do_pgre_inresv
//
// Fills in "resvlist" with the reserved PGRe key currently stored on a
// quorum device.
//
//
int
do_pgre_inresv(
    void			*qhandle,
    quorum::reservation_list_t *resvlist)
{
	int			error;
	uint64_t		sblkno;

	//
	// Gets the starting block number for the PGRE reserved area on
	// the alternate cylinders.
	//
	error = quorum_scsi2_get_sblkno(qhandle, sblkno);
	ASSERT(error != -1);

	if (error != 0)
		return (error);

	quorum::registration_key_t 	resv_key;
	resvlist->listlen = 0;

	//
	// PGRe code has only one reserved key which is stored
	// in the 1st sector of the reserved sector.
	//
	error = quorum_scsi2_key_read(qhandle, sblkno, &resv_key);
	ASSERT(error != -1);
	if (error == 0) {
		// Reservation is valid only if not a NULL_KEY.
		if (!NULL_KEY(resv_key)) {
			bcopy(&resv_key,
			    &(resvlist->keylist[resvlist->listlen].key),
			    sizeof (quorum::registration_key_t));

			resvlist->listlen++;
		}
	}

	if (error == 0) {
		if (resvlist->listlen == 0) {
			(void) printf("\nNo reservations on the device.\n");
		} else {
			(void) printf("resv[0]: key=0x%llx.\n",
			    idl_key_to_int64_key(&(resvlist->keylist[0])));
		}
	}
	return (error);
}

//
// do_pgre_scrub
//
// Scrubs the PGRE area on the alternate cylinders of a disk.
//
int
do_pgre_scrub(void *qhandle)
{
	int 		error = 0;
	uint64_t	sblkno;
	uint_t		i;

	error = quorum_scsi2_get_sblkno(qhandle, sblkno);
	ASSERT(error != -1);

	if (error != 0)
		return (error);

	//
	// Initialize each sector one by one. Use PGRE_NUM_SECTORS
	// rather than NODEID_MAX since the former directly defines
	// the PGRE area.
	//

	// First sector is owner area.
	error = quorum_scsi2_sector_scrub(qhandle, sblkno, true);
	ASSERT(error != -1);

	if (error != 0)
		return (error);

	for (i = 1; i < PGRE_NUM_SECTORS; i++) {
		error = quorum_scsi2_sector_scrub(
		    qhandle,
		    PGRE_GET_NODE_BLKNO(sblkno, i), false);
		if (error != 0)
			return (error);

		error = quorum_scsi_er_pair_write(qhandle,
		    PGRE_GET_NODE_BLKNO(sblkno, i), (ccr_data::epoch_type) -1,
		    (cmm::seqnum_t) 0, false);
		if (error != 0)
			return (error);
	}
	return (error);
}

int
main(int argc, char **argv)
{
	char *option = NULL;
	int c;
	char *zdisk = NULL;
	int zd = -1;
	quorum::reservation_list_t klist;
	quorum::reservation_list_t rlist;
	int ret_value = 0;

	if (sc_zonescheck() != 0)
		return (1);

	// get args
	while ((c = getopt(argc, argv, "c:d:")) != EOF) {
		switch (c) {
		case 'c' :
			option = optarg;
			break;
		case 'd' :
			zdisk = optarg;
			break;
		default :
			(void) printf("Illegal option '%c'.\n", optopt);
			print_usage();
			exit(1);
		}
	}

	// check for required args
	if (option == NULL) {
		(void) printf("%s: option not specified.\n", PGRE_CMD);
		print_usage();
		exit(1);
	}
	if (zdisk == NULL) {
		(void) printf("%s: device not specified.\n", PGRE_CMD);
		print_usage();
		exit(1);
	}

	// are we issuing a option that is not valid
	if ((strcmp(option, "pgre_scrub") != 0) &&
	    (strcmp(option, "pgre_inkeys") != 0) &&
	    (strcmp(option, "pgre_inresv") != 0)) {
		(void) printf("%s: unrecognized option '%s'.\n",
		    PGRE_CMD,
		    option);
		print_usage();
		exit(1);
	}

	// open the disk, get file descriptor.
	if ((zd = open(zdisk, O_RDONLY|O_NDELAY)) == -1) {
		(void) printf("Could not open device '%s', "
		    "errno = %d.\n%s -c %s -d %s command failed.\n",
		    zdisk, errno, PGRE_CMD, option, zdisk);
		exit(1);
	}

	if (strcmp(option, "pgre_scrub") == 0) {
		ret_value = do_pgre_scrub((void *)&zd);
		if (ret_value == 0) {
			(void) printf("\nScrubbing complete. "
			    "Use '%s -c pgre_inkeys -d %s' \n "
			    "to verify success.\n", PGRE_CMD, zdisk);
		} else {
			(void) printf("\n%s -c %s -d %s command failed, "
			    "errno = %d.\n", PGRE_CMD, option, zdisk,
			    ret_value);
		}

	} else if (strcmp(option, "pgre_inkeys") == 0) {

		// intialize the klist structure
		klist.listsize = INIT_KEYS;
		klist.listlen = 0;
		klist.keylist.length(INIT_KEYS);
		if (klist.keylist.length() == 0) {
			(void) printf("Memory allocation failure.\n");
			(void) close(zd);
			exit(1);
		}
		if ((ret_value = do_pgre_inkeys((void *)&zd, &klist))
		    != 0) {
			(void) printf("\n%s -c %s -d %s command failed "
			    "errno = %d.\n", PGRE_CMD, option, zdisk,
			    ret_value);
		}

	} else if (strcmp(option, "pgre_inresv") == 0) {
		rlist.keylist.length(INIT_RESVS);
		rlist.listsize = INIT_RESVS;
		rlist.listlen = 0;

		if (rlist.keylist.length() == 0) {
			(void) printf("Memory allocation failure.\n");
			(void) close(zd);
			exit(1);
		}

		if ((ret_value = do_pgre_inresv((void *)&zd, &rlist))
		    != 0) {
			(void) printf("\n%s -c %s -d %s command failed "
			    "errno = %d.\n", PGRE_CMD, option, zdisk,
			    ret_value);
		}
	}

	// clean up
	if (zdisk != NULL)
		(void) close(zd);

	return (ret_value);
}
