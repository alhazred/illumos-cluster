/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_PGRE_H_
#define	_PGRE_H_

#pragma ident	"@(#)pgre.h	1.5	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif


#include <sys/param.h>

//
// do_pgre_inkeys
//
// Fills in "keylist" with the keys currently stored on a quorum device.
//
int do_pgre_inkeys(
    void		*qhandle,
    mhioc_key_list_t	*keylist);

//
// do_pgre_inresv
//
// Fills in "resvlist" with the reservations currently stored on a
// quorum device.
//
int do_pgre_inresv(
    void			*qhandle,
    mhioc_resv_desc_list_t	*resvlist);

//
// do_pgre_scrub
//
// Scrubs the PGRE area on the alternate cylinders of a disk
//
int do_pgre_scrub(void *qhandle);

#ifdef __cplusplus
}
#endif

#endif	/* _PGRE_H_ */
