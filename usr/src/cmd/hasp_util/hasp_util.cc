//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)hasp_util.cc	1.1	09/01/21 SMI"

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <strings.h>
#include <stdlib.h>
#include <sys/cladm_int.h>
#include <librtutils.h>
#include <ccr_access.h>

#define	DUMP_CACHEFILE_CMD	"dump_pool_ccr_cachefile"
#define	DEL_POOL_CCR_CACHEFILE	"delete_pool_ccr_cachefile"

//
// Print the usage.
//
static void
print_usage()
{
	(void) printf("Usage: hasp_util command args ...\n");
	(void) printf("where 'command' is one of the following:\n");
	(void) printf("\t %s <poolname> <filename>\n", DUMP_CACHEFILE_CMD);
	(void) printf("\t %s [-f] <poolname>\n", DEL_POOL_CCR_CACHEFILE);
	// Future commands to be added here
}

static int
dump_cachefile(int argc, char *argv[])
{
	char		ccr_tabname[MAXPATHLEN];
	err_msg_t	err;
	int		retval;
	int		bootflags;

	if (argc < 3) {
		// The poolname or filename is missing.
		print_usage();
		return (1);
	}
	char *poolname = argv[1];
	char *filename = argv[2];

	// This command should be running by user with root privileges.
	if (geteuid() != 0) {
		printf("User needs to root privileges to run this command.\n");
		return (1);
	}

	// Get the CCR table name where the cachefile contents are stored
	get_pool_ccr_tabname(poolname, ccr_tabname, MAXPATHLEN);

	// Check whether we are in cluster or non-cluster mode.
	if (cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) {
		printf("Internal error: cladm() failed\n");
		return (1);
	}

	if (bootflags & CLUSTER_BOOTED) {
		// Node is running in cluster mode
		// Use the API in librtutils to read the cachefile.
		char		*serach_dir_value;
		retval = read_zcache_from_ccr(poolname, filename,
		    &serach_dir_value, &err);
		if (retval != 0) {
			printf("Failed to read the contents of the pool '%s' "
			    "from its CCR table '%s' and dump to "
			    "file '%s' : %s\n",
			    poolname, ccr_tabname, filename, err.message);
		} else {
			printf(
			    "Successfully read the contents of the pool '%s' "
			    "from its CCR table '%s' and dumped to file '%s'\n",
			    poolname, ccr_tabname, filename);
		}
		return (retval);
	}

	// Node is in non-cluster mode.
	// Read the table contents using libqlccr API and librtutils API

	ccr_access_readonly_table	read_pool_tab;

	// Initialize the table for reading
	retval = read_pool_tab.initialize(ccr_tabname);
	if (retval != 0) {
		printf("The CCR table '%s' does not exist. "
		    "This means either the pool '%s' is not controlled by "
		    "HAStoragePlus or it has not stored the cachefile "
		    "contents in CCR yet.\n");
		read_pool_tab.close();
		return (retval);
	}

	// Read the CACHE_SIZE_KEY value. It is required to decompress the
	// contents
	char	*zcache_data_len_encoded;
	size_t	zcache_data_len;
	zcache_data_len_encoded = read_pool_tab.query_element(CACHE_SIZE_KEY);
	if (zcache_data_len_encoded == NULL) {
		printf("Failed to read key %s of CCR table %s for pool %s.\n",
		    SEARCH_DIR_KEY, ccr_tabname, poolname);
		read_pool_tab.close();
		return (1);
	}
	(void) sscanf(zcache_data_len_encoded, "%08lx", &zcache_data_len);

	// Read the compressed and encoded zfs cache data from CCR
	char	*ccr_data;
	ccr_data = read_pool_tab.query_element(CDATA_KEY);
	if (ccr_data == NULL) {
		printf("Failed to read key %s of CCR table %s for pool %s.\n",
		    CDATA_KEY, ccr_tabname, poolname);
		read_pool_tab.close();
		return (1);
	}

	void *file_data;
	// Check whether the cachefile information will be a compressed one
	if (is_cachefile_met_compression_criteria(zcache_data_len)) {
		file_data = convert_compressed_encoded_data_to_data(ccr_data,
		    strlen(ccr_data), zcache_data_len, &err);
	} else {
		size_t  decoded_data_len;
		file_data = base64_decode((uchar_t *)ccr_data,
		    strlen(ccr_data), &decoded_data_len, &err);
	}
	delete [] ccr_data;
	if (file_data == NULL) {
		printf("Failed to read the cachefile information from CCR "
		    "for pool %s : %s\n",
		    poolname, err.message);
		read_pool_tab.close();
		return (1);
	}
	if (write_contents_to_file(file_data, zcache_data_len,
	    filename, &err) != 0) {
		free(file_data);
		read_pool_tab.close();
		printf("Failed to write to %s the cachefile information in CCR "
		    "for pool %s : %s.\n",
		    filename, poolname, err.message);
		return (1);
	}
	free(file_data);
	printf("Successfully read the contents of the pool '%s' "
	    "from its CCR table '%s' and wrote to file '%s'\n",
	    poolname, ccr_tabname, filename);
	read_pool_tab.close();
	return (0);
}

static int
delete_ccr_cachefile(int argc, char *argv[])
{
	err_msg_t	err;
	int		bootflags;
	int		c;
	boolean_t	force = B_FALSE;

	while ((c = getopt(argc, argv, "f")) != -1) {
		switch (c) {
			case 'f':
				force = B_TRUE;
				break;
			case '?':
				print_usage();
				return (1);
		}
	}
	argc -= optind;
	argv += optind;
	if (argc <= 0) {
		// poolname is missing
		print_usage();
		return (1);
	}
	char *poolname = argv[0];

	// This command should be running by user with root privileges.
	if (geteuid() != 0) {
		printf("User needs to root privileges to run this command.\n");
		return (1);
	}

	// Check whether we are in cluster or non-cluster mode.
	if (cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) {
		printf("Internal error: cladm() failed\n");
		return (1);
	}

	if (!bootflags & CLUSTER_BOOTED) {
		//
		// Deletion of CCR file is not allowed in non cluster mode.
		printf("Deletion of CCR file of pool cachefile "
		    "is not allowed in cluster mode\n");
		return (1);
	}

	// Node is in cluster mode.
	// Delete the pool cachefile
	if (!force) {
		printf("It is not recomended to delete the pool cachefile "
		    "information from CCR.\n"
		    "Ensure that pool is not being used anymore and "
		    "use -f option to delete from CCR.\n");
		return (1);
	}
	if (remove_pool_table(poolname, &err) != 0) {
		printf("Failed to remove the pool %s cachefile information "
		    "from CCR : %s.\n", poolname, err.message);
		return (1);
	}
	printf("Successfully deleted the pool %s cachefile information "
	    "from CCR.\n", poolname);
	return (0);
}

int
main(int argc, char *const argv[])
{
	// Make sure user has specified the command
	if (argc < 2) {
		print_usage();
		return (1);
	}

	char *cmdname = argv[1];
	if (strcmp(cmdname, DUMP_CACHEFILE_CMD) == 0) {
		return (dump_cachefile(argc - 1, argv + 1));
	} else if (strcmp(cmdname, DEL_POOL_CCR_CACHEFILE) == 0) {
		return (delete_ccr_cachefile(argc - 1, argv + 1));
	} else {
		print_usage();
		return (1);
	}
	return (0);
}
