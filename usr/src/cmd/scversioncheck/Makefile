#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile	1.9	08/07/15 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/scversioncheck/Makefile
#
include ../Makefile.cmd

PROG = scversioncheck

.KEEP_STATE:

.PARALLEL:

MTFLAG	= -mt
CPPFLAGS += $(CL_CPPFLAGS) -DLIBVM
CPPFLAGS += -I$(SRC)/common/cl -I. -I$(SRC)/common/cl/interfaces/$(CLASS)
CPPFLAGS += -I$(SRC)/head/scadmin

SRC_OBJS = scversioncheck.o

COPY_OBJS= \
	rpc_scadmd_xdr_FILE.o

GEN_OBJS= \
	sccheck_clnt.o \
	rpc_scadmd_xdr.o

OBJS = $(SRC_OBJS) $(COPY_OBJS) $(GEN_OBJS)

CHECK_OBJS = $(SRC_OBJS) $(COPY_OBJS)
CHECK_FILES = $(CHECK_OBJS:%.o=%.c_check)

SRC_SRCS=	$(SRC_OBJS:%.o=%.cc)
COPY_SRCS=	$(COPY_OBJS:%.o=%.c)
GEN_SRCS=	$(GEN_OBJS:%.o=%.c)

SRCS=		$(SRC_SRCS) $(COPY_SRCS) $(GEN_SRCS)

GEN_HDRS = rpc_scadmd.h

POFILE	= scversioncheck.po
PIFILES = $(OBJS:%.o=%.pi)
LINTFILES += $(SRC_OBJS:%.o=%.ln)
CLOBBERFILES += $(PROG) $(OBJS) $(COPY_SRCS) $(GEN_SRCS) $(GEN_HDRS)

RPCX=           rpc_scadmd.x
ROOTRPCXDIR=    $(VROOT)/usr/cluster/include/scadmin/rpc

LIBZONECFG_32=$(REF_PROTO)/usr/lib/libzonecfg.so.1
LDLIBS += -Bstatic -lvm -lclcomm -lclos -lsczones -Bdynamic -lnsl -ldl -ldoor -lrt
$(POST_S9_BUILD)LDLIBS += $(LIBZONECFG_32)

install: all $(ROOTCLUSTPRIVPROG)

all: $(PROG)

$(OBJS): $(GEN_HDRS)


$(RPCX): $(ROOTRPCXDIR)/$(RPCX)
	$(CP) $(ROOTRPCXDIR)/$(RPCX) .
	$(CHMOD) 664 $(RPCX)

rpc_scadmd.h: $(RPCX)
	$(RPCGEN) -A -C -N -h $(RPCX) >$@

sccheck_clnt.c: $(RPCX)
	$(RPCGEN) -A -C -N -l $(RPCX) >$@

rpc_scadmd_xdr.c: $(RPCX)
	$(RPCGEN) -A -C -N -c $(RPCX) >$@

rpc_scadmd_xdr_FILE.c: ../rpc.scadmd/rpc_scadmd_xdr_FILE.c
	$(RM) $@
	$(CP) -p ../rpc.scadmd/rpc_scadmd_xdr_FILE.c .

$(PROG): $(OBJS)
	$(LINK.cc) $(OBJS) -o $@ $(LDLIBS)
	$(POST_PROCESS)

clean:

include ../Makefile.targ
