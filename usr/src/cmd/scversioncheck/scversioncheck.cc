//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)scversioncheck.cc	1.6	08/05/20 SMI"

//
// scversioncheck
//
// Command run during "scinstall -u update", to check if the versions
// being installed will allow the node to join back the cluster.
//

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <libintl.h>
#include <locale.h>
#include <sys/os.h>
#include <h/cmm.h>
#include <vm/vm_admin_int.h>
#include <vm/vm_stream.h>
#include <vm/vm_coord.h>
#include <vm/coord_vp.h>
#include <rpc/rpc.h>
#include <vm/vp_rhs_version.h>
#include "rpc/rpc_scadmd.h"
#include <rgm/sczones.h>


vm_admin_int	*local_vm_admin = NULL;
vm_coord	*local_vm_coord = NULL;
char		*local_vp_dirs[4];
vm_stream	*bsnp[NODEID_MAX+1];
vm_stream	*bscl[NODEID_MAX+1];
vm_stream	*clmsg[NODEID_MAX+1];
vm_stream	*custom = NULL;
vm_stream	*vplist = NULL;
nodeid_t	my_nodeid = 0;
char		*program_name = NULL;
char		*cl_nodename = NULL;
sc_result_cluster_vm res = {0, 0, 0, {0, NULL}, 0, 0, {0, NULL}};

#define	CLNT_CREATE_TIMEOUT_SEC 3

void
cleanup()
{

	if (local_vm_coord != NULL) {
		delete local_vm_coord;
		local_vm_coord = NULL;
	}

	if (local_vm_admin != NULL) {
		delete local_vm_admin;
		local_vm_admin = NULL;
	}

	for (uint32_t nodeid = 1; nodeid <= NODEID_MAX; nodeid++) {
		if (bsnp[nodeid] != NULL) {
			delete bsnp[nodeid];
			bsnp[nodeid] = NULL;
		}
		if (bscl[nodeid] != NULL) {
			delete bscl[nodeid];
			bscl[nodeid] = NULL;
		}
		if (clmsg[nodeid] != NULL) {
			delete clmsg[nodeid];
			clmsg[nodeid] = NULL;
		}
	}

	if (custom != NULL) {
		delete custom;
		custom = NULL;
	}

	if (vplist != NULL) {
		delete vplist;
		vplist = NULL;
	}

	xdr_free((xdrproc_t)xdr_sc_result_cluster_vm, (caddr_t)&res);
}

void
usage()
{
	(void) fprintf(stderr, gettext("usage: %s -c cluster_node [-n node_id]"
	    " [-d vp_dir1,vp_dir2,...]\n"), program_name);
	(void) fprintf(stderr, gettext("where args are:\n"
	    "\t-c cluster_node		- nodename of a node in cluster\n"
	    "\t[-n node_id]		- node id of node being upgraded\n"
	    "\t[-d vp_dir1,vp_dir2,..]  - up to 3 vp files directories\n"));
}

int
parse_options(int argc, char **argv)
{
	int c = 0;

	/* Set the program name  */
	char *np = strrchr(argv[0], '/');
	if (np) {
		program_name = ++np;
	} else {
		program_name = argv[0];
	}

	if (argc == 1) {
		usage();
		return (1);
	}

	optind = 1;
	char *dirs = NULL;
	char *token = NULL;
	int indx = 0;
	while ((c = getopt(argc, argv, "n:c:d:?")) != EOF) {
		switch (c) {
		case 'n':
			my_nodeid = (nodeid_t)atoi(optarg);
			break;
		case 'c':
			cl_nodename = optarg;
			break;
		case 'd':

			if ((dirs = strdup(optarg)) == NULL) {
				(void) fprintf(stderr,
				    gettext("%s: Internal error while "
					"getting args (%d)\n"), program_name,
				    errno);
				return (1);
			}
			token = strtok(dirs, ",");
			while ((token != NULL) &&
			    (indx <= 2)) {
				local_vp_dirs[indx++] = token;
				token = strtok(NULL, ",");
			}
			break;
		case '?':
			usage();
			return (1);
		default:
			usage();
			return (1);
		}
	}
	if (cl_nodename == 0) {
		usage();
		return (1);
	}
	return (0);
}

//
// Return a new stream for given nodeid and vp type.
//
vm_stream*
get_vm_stream(nodeid_t nodeid, vp_type vptype, sc_result_cluster_vm* scr) {

	//
	// No stream for this nodeid.
	//
	if (scr->sc_nodes_table.sc_nodes_table_val[nodeid].
	    sc_streams_by_type.sc_streams_by_type_len == 0) {
		return (NULL);
	};

	//
	// No stream of that particular vptype for this nodeid.
	//
	if (scr->sc_nodes_table.sc_nodes_table_val[nodeid].
	    sc_streams_by_type.sc_streams_by_type_val[vptype].
	    data.data_len == 0) {
		return (NULL);
	}

	//
	// There is a stream to return.
	//
	if (!vm_stream::valid_stream(
	    scr->sc_nodes_table.sc_nodes_table_val[nodeid].
	    sc_streams_by_type.sc_streams_by_type_val[vptype].
	    data.data_len,
	    scr->sc_nodes_table.sc_nodes_table_val[nodeid].
	    sc_streams_by_type.sc_streams_by_type_val[vptype].
	    data.data_val)) {
		return (NULL);
	}

	vm_stream* the_streamp = new vm_stream();
	if (the_streamp == NULL)  {
		return (NULL);
	}

	the_streamp->reinit(
	    scr->sc_nodes_table.sc_nodes_table_val[nodeid].
	    sc_streams_by_type.sc_streams_by_type_val[vptype].
	    data.data_len,
	    scr->sc_nodes_table.sc_nodes_table_val[nodeid].
	    sc_streams_by_type.sc_streams_by_type_val[vptype].
	    data.data_val,
	    true, //  Do not copy buffer.
	    false); // Do not dispose buffer later on.

	return (the_streamp);
}

int
sccheckclient_ru(char *node_name, sc_result_cluster_vm *result)
{
	CLIENT		*clnt = (CLIENT *)0;
	enum clnt_stat	clnt_status;
	struct timeval timeout;

	timeout.tv_sec = CLNT_CREATE_TIMEOUT_SEC;
	timeout.tv_usec = 0;

	clnt = clnt_create_timed(node_name, SCADMPROG, SCADMVERS,
	    "circuit_v", &timeout);
	if (clnt == (CLIENT *) NULL) {
		//
		// During install/upgrade we might try to talk to
		// scadmd on a node which is down.
		//
		// We do not log an error message in this case.
		//
		return (1);
	}

	clnt_status = scadmproc_get_cluster_vm_info_1(result, clnt);
	if (clnt_status != RPC_SUCCESS) {
		//
		// If the remote scadmd reports the following RPC error:
		//
		// procedure unavailable,
		//
		// then we tried to talk to a non upgraded version
		// of the server. This is not an error, it might
		// happen during the first rolling upgrade.
		//
		// We do not log an error message in this case.
		//
		if (clnt_status != RPC_PROCUNAVAIL) {
			(void) fprintf(stderr, gettext("%s: Could not complete"
			    " call to scadmd on node %s\n"), program_name,
			    node_name);
			clnt_perror(clnt, gettext("Status returned"));
		}
		clnt_destroy(clnt);
		return (1);
	}

	if (result->sc_res_errno != SCCONF_NOERR) {
		//
		// During install/upgrade we might talk to scadmd
		// on a node which is not in cluster (boot -x).
		//
		// We do not log an error message in this case.
		//
		if (result->sc_res_errno != SCCONF_ENOCLUSTER) {
			(void) fprintf(stderr, gettext("%s: scadmd call on "
			    "node %s returned with error %d\n"),
			    program_name, node_name, result->sc_res_errno);
		}
		clnt_destroy(clnt);
		return (1);
	}

	clnt_destroy(clnt);
	return (0);
}

//
// Return status: 0 - Node can join existing cluster.
//		  1 - Node cannot join existing cluster.
//		  2 - An error occurred (failed to check).
//
int
main(int argc, char **argv)
{

	//
	// Initialize data.
	//
	for (uint32_t nodeid = 1; nodeid <= NODEID_MAX; nodeid++) {
		bsnp[nodeid] = bscl[nodeid] = clmsg[nodeid] = NULL;
	}

	//
	// Default vp dirs (if not passed).
	//
	local_vp_dirs[0] = "/etc/cluster/vp";
	local_vp_dirs[1] = "/usr/cluster/lib/vp";
	local_vp_dirs[2] = 0;
	local_vp_dirs[3] = 0;

	//
	// I18N housekeeping
	//
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (sc_zonescheck() != 0)
		return (1);
	//
	// Parse command line arguments.
	//
	if (parse_options(argc, argv)) {
		// Usage printed within function.
		// Error case.
		return (2);
	}

#ifndef _SC_ROLLING_UPGRADE_MODE
	//
	// scversioncheck is a noop in Quantum Leap upgrade mode.
	//
	return (0);
#endif /* _SC_ROLLING_UPGRADE_MODE */

	//
	// Try to get local node id if not passed as an argument.
	//
	if (my_nodeid == 0) { //lint !e527
		nodeid_t lnid;
		if (_cladm(CL_CONFIG, CL_NODEID, &lnid) == 0) {
			my_nodeid = lnid;
		} // We can go on anyway.
	}

	//
	// Get CMM and VM info from cluster.
	//
	if (sccheckclient_ru(cl_nodename, &res) != 0) {
		cleanup();
		// Message printed within function.
		// Error case.
		return (2);
	}

	//
	// Set VM Streams information for every cluster member
	//
	for (uint32_t nodeid = 1; nodeid <= NODEID_MAX; nodeid++) {
		if (res.sc_members.sc_members_val[nodeid] != INCN_UNKNOWN) {

			if (((bsnp[nodeid] = get_vm_stream(nodeid,
			    VP_BTSTRP_NODEPAIR, &res)) == NULL) ||
			    ((bscl[nodeid] = get_vm_stream(nodeid,
				VP_BTSTRP_CLUSTER, &res)) == NULL) ||
			    ((clmsg[nodeid] = get_vm_stream(nodeid,
				VP_CLUSTER, &res)) == NULL)) {
				cleanup();
				// Error case.
				(void) fprintf(stderr,
				    gettext("%s: Internal error "
					"while retreiving vm streams\n"),
				    program_name);
				return (2);
			}
		}
	}

	//
	// Get custom version stream (stored in sequence 0, stream 0).
	//
	custom = get_vm_stream(0, (vp_type)0, &res);
	//
	// Get vp name list (stored in sequence 0, stream 1).
	//
	vplist = get_vm_stream(0, (vp_type)1, &res);

	//
	// Set CMM information.
	//
	cmm::membership_t cmm_m;

	for (uint32_t nodeid = 1; nodeid <= NODEID_MAX; nodeid++) {

		cmm_m.members[nodeid] = res.sc_members.
			sc_members_val[nodeid];

		//
		// If at this point we still do not have a nodeid,
		// it could be that this is a fresh install,
		// we pick up a free nodeid just for the version
		// check duration.
		//
		if ((my_nodeid == 0) &&
		    (cmm_m.members[nodeid] == INCN_UNKNOWN)) {
			my_nodeid = nodeid;
		}
	}
	if (my_nodeid == 0) {
		cleanup();
		// Error case.
		(void) fprintf(stderr, gettext("%s: No valid node id for local"
		    " node\n"), program_name);
		return (2);
	}

	uint32_t needed_votes = res.sc_votes_needed_for_quorum;
	uint32_t qvotes = res.sc_current_votes;

	//
	// Check that node id specified for this node is valid - it
	// should not be the same as of one of the nodes currently in
	// the cluster.
	//
	if (cmm_m.members[my_nodeid] != INCN_UNKNOWN) {
		cleanup();
		// error case
		(void) fprintf(stderr, gettext("%s: Node id already owned "
		    "by a cluster member\n"), program_name);
		return (2);
	}

	//
	// Initialize local node vm_admin.
	//
	// Cmm sequence 0 because this node actually tries to join.
	//
	local_vm_admin = new vm_admin_int(local_vp_dirs,
	    (cmm::seqnum_t)0, custom, vplist);

	if (local_vm_admin == NULL) {
		cleanup();
		(void) fprintf(stderr, gettext("%s: Internal error during "
		    "vm admin creation (%d)\n"), program_name, errno);
		// error case
		return (1);
	}

	int ret = local_vm_admin->initialize_int(bsnp[my_nodeid]);

	if (ret != 0) {
		//
		// The vm admin initialization will fail
		// if a local "Custom Node" vp does not
		// support a consistent version compatible
		// with one we got from the cluster.
		//
		cleanup();
		if (ret == VP_NOVERSIONS) {
			// Node cannot join existing cluster.
			return (1);
		}
		//
		// The intersection of the cluster vpfile set and
		// the cd vpfile set might be null, in this case
		// we can not check version compatibility.
		//
		if (ret != VP_NOVPFILES) {
			// In all other cases, we log an error.
			(void) fprintf(stderr, gettext("%s: Internal error "
			    "during vm admin initialization (%d)\n"),
			    program_name, ret);
		}
		// error case
		return (2);
	}

	//
	// Initialize local vm_coord.
	//
	local_vm_coord = new vm_coord(my_nodeid, needed_votes, local_vm_admin);
	if (local_vm_coord == NULL) {
		cleanup();
		// error case
		(void) fprintf(stderr, gettext("%s: Internal error during "
		    "vm coord creation (%d)\n"), program_name, errno);
		return (2);
	}

	//
	// Check 1 - Bootstrap NodePair Versions.
	//

	//
	// Give local btstrp_np_message to local vm_coord.
	//
	vm_stream *vmsp = local_vm_coord->btstrp_np_handshake(my_nodeid,
	    bsnp[my_nodeid]);

	if (vmsp != NULL) {
		delete vmsp;
		vmsp = NULL;
	}

	//
	// For every current nodes in the cluster.
	//
	for (uint32_t nodeid = 1; nodeid <= NODEID_MAX; nodeid++) {
		if ((cmm_m.members[nodeid] != INCN_UNKNOWN) &&
		    (nodeid != my_nodeid)) {

			vm_stream *bsnp_rv =
				local_vm_coord->btstrp_np_handshake(nodeid,
				    bsnp[nodeid]);

			if (bsnp_rv == NULL) {
				cleanup();
				// Node cannot join existing cluster.
				return (1);
			} else {
				//
				// Update local admin.
				//
				local_vm_admin->set_rvs(*bsnp_rv,
				    VP_BTSTRP_NODEPAIR,
				    nodeid);
				delete bsnp_rv;
				bsnp_rv = NULL;
			}
		}
	}

	//
	// Check 2 - Bootstrap Cluster Versions.
	//

	//
	// Update local vm_coord with Bootstrap Cluster messages
	// for every current nodes in the cluster
	//
	for (uint32_t nodeid = 1; nodeid <= NODEID_MAX; nodeid++) {
		if ((cmm_m.members[nodeid] != INCN_UNKNOWN) &&
		    (nodeid != my_nodeid)) {

			local_vm_coord->set_info(VP_BTSTRP_CLUSTER, nodeid,
			    bscl[nodeid],
			    qvotes);
		}
	}

	//
	// Add ourselves to the membership.
	//
	cmm_m.members[my_nodeid] = 1;

	//
	// Set local Bootstrap Cluster message information.
	//
	(void) local_vm_admin->new_btstrp_cl_message(bscl[my_nodeid]);
	local_vm_coord->set_info(VP_BTSTRP_CLUSTER, my_nodeid, bscl[my_nodeid],
	    qvotes);

	bool retry_calc = false;
	bool shutdown_requested = false;

	//
	// Set current and upgrade_seq to 1.
	// Upgrade_seq == current_seq since we are not doing an upgrade.
	//
	vm_stream *btstrp_cl_rv_msg = local_vm_coord->
		calculate_btstrp_cl(cmm_m, (cmm::seqnum_t) 1,
		    (cmm::seqnum_t) 1, retry_calc, shutdown_requested);

	if (btstrp_cl_rv_msg != NULL) {
		delete btstrp_cl_rv_msg;
		btstrp_cl_rv_msg = NULL;
	}

	//
	// We only check if we were asked to shutdown because of
	// incompatible versions.
	//
	if (shutdown_requested) {
		cleanup();
		// Node cannot join existing cluster.
		return (1);
	}

	//
	// Check 3 - Cluster Versions.
	//

	//
	// Update local vm_coord. with Cluster messages.
	//
	// For every current nodes in the cluster.
	//
	for (uint32_t nodeid = 1; nodeid <= NODEID_MAX; nodeid++) {
		if ((cmm_m.members[nodeid] != INCN_UNKNOWN) &&
		    (nodeid != my_nodeid)) {

			local_vm_coord->set_info(VP_CLUSTER, nodeid,
			    clmsg[nodeid],
			    0); // qvotes must be 0 for type VP_CLUSTER.
		}
	}

	//
	// Set local info.
	//
	vm_stream *lclmsg = NULL;
	(void) local_vm_admin->new_cl_message(lclmsg);
	local_vm_coord->set_info(VP_CLUSTER, my_nodeid, lclmsg, 0);

	nodeset send_to, good_nodes;

	//
	// Calculate running versions for cl vps.
	//
	vm_stream *cl_rv_msg = local_vm_coord->calculate_cl(cmm_m,
	    (cmm::seqnum_t) 1, (cmm::seqnum_t) 1,
	    send_to, good_nodes, retry_calc);

	if (cl_rv_msg != NULL) {
		delete cl_rv_msg;
		cl_rv_msg = NULL;
	}

	if (!(good_nodes.contains(my_nodeid))) {
		cleanup();
		// Node cannot join existing cluster.
		return (1);
	}

	cleanup();
	// Node can join existing cluster.
	return (0);
}
