#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)mcboot.sh	1.28	08/05/20 SMI"

# Startup script for clusters.
# Note that this script does not enable DCS so you should not boot -r
# or run drvconfig.

# turn on CLUSTER_BOOTED flag
# XXX On a 64 bit kernel, "(*cluster_bootflags)" reads 8 bytes instead of 4
echo "cluster_bootflags/W(*cluster_bootflags|3)" | adb -kw > /dev/null

node_name=`uname -n`
node_num=`/usr/sbin/clinfo -n`

# Load the orb module
m=misc/cl_comm
echo Loading $m
modload -p $m
if [ "$?" != "0" ] ; then
	echo "GALILEO BOOT FAILED: Could not load kernel module $m on node $node_name"
	exit 0;
fi

# Start up the ORB and transport and join the cluster.

/usr/cluster/lib/sc/clconfig -c
if [ "$?" != "0" ] ; then
	echo "GALILEO BOOT FAILED: Could not initialize ORB"
	exit 0;
fi

echo $node_name completed booting as node $node_num of cluster > \
	/dev/console 2>&1

exit 0
