#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)svc_farm_ff	1.4	08/05/20 SMI"
#

. /lib/svc/share/smf_include.sh

USRBIN=/usr/bin
SCLIB=/usr/cluster/lib/sc

#
#  Make the runtime linker load all shared objects at startup
#
LD_BIND_NOW=yes
export LD_BIND_NOW

#
# Load the Failfast driver into the kernel
#
DRV_DIR=/kernel/drv
DRV_NAME=ffmod

is_global_zone()
{
	# Detect whether we are running in a global or non-global zone
	ZONECMD=${USRBIN}/zonename
	if [ -x ${ZONECMD} ]; then
		if [ "`${ZONECMD}`" = "global" ]; then
			return 1
		fi
	fi

	return 0
}

load_driver()
{
    echo "Failfast driver ${DRV_NAME} \c"
    LOADED=`modinfo | grep -c ${DRV_NAME}`
    if [ "${LOADED}" -eq "0" ]; then
	/usr/sbin/modload ${DRV_DIR}/${DRV_NAME}
		
	LOADED=`modinfo | grep -c ${DRV_NAME}`
	if [ "${LOADED}" -eq "0" ]
	then
	    echo "loading failed"
	    exit $SMF_EXIT_ERR_CONFIG
	else
	    echo "loaded"
	fi
    else
	echo "already loaded"
    fi
    exit $SMF_EXIT_OK
}


unload_driver()
{
    ID=`modinfo | grep ${DRV_NAME} | awk '{print $1}'`
    if [ "${ID}" != "" ]; then
	COUNTER=120
	while [ ${COUNTER} -gt 0 ]
	do
		COUNTER=`expr ${COUNTER} - 1`

		/usr/sbin/modunload -i ${ID}

		LOADED=`modinfo | grep -c ${DRV_NAME}`
		if [ "${LOADED}" -eq "0" ]
		then
		    echo "Unloading ok"
		    exit $SMF_EXIT_OK
		fi

		# we log an error every 30 seconds in case of failure
                if [ `expr ${COUNTER} % 30` -eq 0 ]
                then
			# SCMSGS
			# @explanation
			# The svc:/system/egc/failfast:default SMF service
			# is trying to unload the EGC farm failfast module
			# @user_action
			# No action is required. This is an informational message
			# only.
			${SCLIB}/scds_syslog -p warning -t INITFARMFF -m\
                            "Waiting for the EGC failfast module to be unloaded."
                fi

		 ${USRBIN}/sleep 1
	done

	# unload failed
	# We log an error
	# SCMSGS
	# @explanation
	# The svc:/system/egc/failfast:default SMF service
	# has failed to unload the EGC farm failfast module
	# @user_action
	# Reboot the node.
	#
	${SCLIB}/scds_syslog -p error -t INITFARMFF -m \
            "Error unloading the EGC failfast module."
        exit $SMF_EXIT_ERR_CONFIG

    else
	echo "Not loaded"
    fi
    exit $SMF_EXIT_OK
}

check_driver()
{
    echo "Check Failfast driver ${DRV_NAME} \c"
    LOADED=`modinfo | grep -c ${DRV_NAME}`
    if [ "${LOADED}" -eq "0" ]; then
	echo "failfast driver not loaded"
	exit $SMF_EXIT_ERR_CONFIG
    fi
    exit $SMF_EXIT_OK
}

#
# Main starts here
#

case "$1" in
start)

    # Load the failfast driver into the kernel (global zone)
    is_global_zone || load_driver

    # Check if the driver is loaded (non-global zones)
    check_driver
    ;;

stop)
    # Unload the failfast driver (global zone)
    is_global_zone || unload_driver
    exit $SMF_EXIT_OK
    ;;
esac
