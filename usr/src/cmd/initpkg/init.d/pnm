#!/usr/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)pnm	1.25	08/05/20 SMI"

#
# /etc/init.d/pnm - Start/Stop cl_pnmd
# If in.mpathd has already been started (by Solaris) then we kill it and start
# it under pmf. If it is already under pmf then we don't do anything.
#

PATH=/usr/bin:/bin:/sbin
PNMD_TAG=pnmd
MPATHD_TAG=in.mpathd
PMFADM=/usr/cluster/bin/pmfadm
PNMD_SERVER=cl_pnmd
MPATHD_SERVER=in.mpathd
SCLIB=/usr/cluster/lib/sc

#
# daemon_exists daemonname
#
# Tests whether a daemon exists with the daemonname, running
# as root and with parent process id of 1.  We don't need
# the actual pid, just the boolean status of whether it
# exists.  Returns 0 for exists, non-zero otherwise.
#
daemon_exists()
{
        /usr/bin/pgrep -u 0 -P 1 -x $1 >/dev/null 2>&1
        return $?
}

#
# main()
#

#
# Only run this script in cluster mode. There's no support for
# running PNM in non-cluster mode.
#
/usr/sbin/clinfo >/dev/null 2>&1 || exit 0

#
# Is pmf available?
#
if [ -x $PMFADM ]; then
	pmf_avail=yes
fi

case $1 in 
'start')

	pid=`/usr/bin/pgrep $PNMD_SERVER`
	if [ ! "$pid" -a "$pmf_avail" ]; then
		# 
		# If in.mpathd is not running under pmf then kill it.
		#
		$PMFADM -L | /usr/bin/grep $MPATHD_TAG >/dev/null 2>&1 
		if [ $? -eq 0 ]; then
			mpathd_under_pmf=yes
		fi
		daemon_exists $MPATHD_SERVER
		ret=$?
		if [ ! "$mpathd_under_pmf" -a $ret -eq 0 ]; then
			/usr/bin/pkill in.mpathd
		fi 

		# Start in.mpathd under pmf if it is not already under pmf
		if [ ! "$mpathd_under_pmf" ]; then
			$PMFADM -c $MPATHD_TAG -n -1 /sbin/$MPATHD_SERVER
			if [ $? != 0 ]; then
				# SCMSGS
				# @explanation
				# Need explanation of this message!
				# @user_action
				# Need a user action for this message.
				${SCLIB}/scds_syslog -p error -t INITPNM -m "Error: Can't run pmfadm. Exiting without starting ${MPATHD_SERVER}."
				exit 1
			fi
		fi

		#
		# Start pnmd under pmf. Restart pnmd always if it
		# dies (-n -1).
		#
		$PMFADM -c $PNMD_TAG -n -1 /usr/cluster/bin/$PNMD_SERVER
		if [ $? != 0 ]; then
			# SCMSGS
			# @explanation
			# Need explanation of this message!
			# @user_action
			# Need a user action for this message.
			${SCLIB}/scds_syslog -p error -t INITPNM -m "Error: Can't run pmfadm. Exiting without starting ${PNMD_SERVER}."
			exit 1
		fi

		#
                # Do probe_&_wait till pnmd is up.
                #
                /usr/bin/sleep 1
                retries=10
		daemon_exists $PNMD_SERVER
                ret=$?
		while [ $ret -ne 0 ]
                do
                        # Give up after a couple of retries
                        if [ $retries -eq 0 ]; then
                                # SCMSGS
                                # @explanation
                                # Need explanation of this message!
                                # @user_action
                                # Need a user action for this message.
				${SCLIB}/scds_syslog -p error -t INITPNM -m "Can't start ${PNMD_SERVER}."
                                exit 1
                        fi

                        /usr/bin/sleep 3
                        retries=`/usr/bin/expr $retries - 1`
                        daemon_exists $PNMD_SERVER
			ret=$?
                done

	fi
	;;

'stop')

	pid=`/usr/bin/pgrep $PNMD_SERVER`
	if test "$pid"
	then
		if [ "$pmf_avail" != "" ]; then
			$PMFADM -l $PNMD_TAG >/dev/null 2>&1
			if [ $? = 0 ]; then
				$PMFADM -s $PNMD_TAG TERM && pnmd_killed=yes
			fi
		fi

		if [ "$pnmd_killed" = "" ]; then
			kill -TERM $pid
		fi
	fi
	
	# Wait and see if deamon still exists. Log error.
	/usr/bin/sleep 3
	daemon_exists $PNMD_SERVER
	ret=$?
	if [ $ret -eq 0 ]; then
		# SCMSGS
		# @explanation
		# Need explanation of this message!
		# @user_action
		# Need a user action for this message.
		${SCLIB}/scds_syslog -p error -t EXITPNM -m "Can't stop ${PNMD_SERVER}."
	fi
	
	;;

*)
	echo "usage: /etc/init.d/pnm {start|stop}"
	;;

esac


