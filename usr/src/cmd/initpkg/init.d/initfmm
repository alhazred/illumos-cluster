#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2005 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident "@(#)initfmm	1.2	08/05/20 SMI"
#

#
#  Make the runtime linker load all shared objects at startup
#
LD_BIND_NOW=yes
export LD_BIND_NOW

#
# Retrieve the interface list from europa config
#
get_nic_list() {
    SCXCFG=scxcfg
    SCXCFGDIR=/usr/cluster/lib/sc

    #
    # Get the local node id
    #

    NODEID=`${SCXCFGDIR}/${SCXCFG} -n`
    if [ $? -ne 0 ]
    then
        echo "$SCXCFG: Unable to retrieve localnodeid"
        return 1
    fi

    IFTYPE_PROP=farm.servers.$NODEID.network.interface.1.type
    IFTYPE=`$SCXCFGDIR/$SCXCFG -g $IFTYPE_PROP`
    if [ $? != 0 ]
    then
        echo "$SCXCFG: Unable to retrieve interface type"
        return 1
    fi

    if [ "$IFTYPE" = "simple" ]
    then
        NIC_PROP=farm.servers.$NODEID.network.interface.1.name
        NIC_LST=`$SCXCFGDIR/$SCXCFG -g $NIC_PROP`
        if [ $? -ne 0 ]
        then
            echo "$SCXCFG: Unable to retrieve interface name"
            return 1
        fi
    elif [ "$IFTYPE" = "redundant" ]
    then
        ADAP_PROP=farm.servers.$NODEID.network.interface.1.adapters
        ADAP=`$SCXCFGDIR/$SCXCFG -l $ADAP_PROP`
        if [ $? -ne 0 ]
        then
            echo "$SCXCFG: Unable to retrieve adapters list"
            return 1
        else
            NIC_LST=""
            INDEX=1
            for i in ${ADAP}
            do
		ADAP_NAME=`$SCXCFGDIR/$SCXCFG -g farm.servers.$NODEID.network.interface.1.adapters.$INDEX.name`

                if [ $? -ne 0 ]
                then
                    echo "$SCXCFG: Unable to retrieve adapter #$INDEX name"
                    return 1
                fi
                INDEX=`expr $INDEX + 1`
                NIC_LST=${NIC_LST}" "${ADAP_NAME}
            done
        fi

    fi

    return 0
}

#
# Insert a module for a given network interfaces
#
MOD_NAME=hbxmod

load_module()
{
    NIC=$1
    echo "HB module ${MOD_NAME} for ${NIC} \c"
    LOADED=`/usr/sbin/ifconfig ${NIC} modlist | grep -c ${MOD_NAME}`
    if [ "$LOADED" -eq "0" ]; then
	INS_POS=`/usr/sbin/ifconfig ${NIC} modlist | \
	    /usr/bin/awk 'index("'$NIC'",$2) == 1 { print $1 }'`
	if [ -z "${INS_POS}" ]; then
	    echo "loading failed"
	    exit 1
	fi

	/usr/sbin/ifconfig ${NIC} modinsert ${MOD_NAME}@${INS_POS}
	LOADED=`/usr/sbin/ifconfig ${NIC} modlist | grep -c ${MOD_NAME}`
	if [ "${LOADED}" -eq "0" ]; then
	    echo "loading failed"
	    exit 1
	else
	    echo "loaded"
	fi
    else
	echo "already loaded"
    fi
}

#
# Load the HB driver into the kernel
#
DRV_DIR=/kernel/drv
DRV_NAME=hbxdrv

load_driver()
{
    echo "HB driver ${DRV_NAME} \c"
    LOADED=`modinfo | grep -c ${DRV_NAME}`
    if [ "${LOADED}" -eq "0" ]; then
	/usr/sbin/modload ${DRV_DIR}/${DRV_NAME}
		
	LOADED=`modinfo | grep -c ${DRV_NAME}`
	if [ "${LOADED}" -eq "0" ]
	then
	    echo "loading failed"
	    exit 1
	else
	    echo "loaded"
	fi
    else
	echo "already loaded"
    fi
}

#
# Main starts here
#
FMMPROG=fmmd
FMMPID=`pgrep ${FMMPROG}` 
FMMDIR=/usr/cluster/lib/sc
FILE=/var/run/FMM_api_v1_door

case "$1" in
start)
    # Check whether we are a cluster and exit if not a cluster
    /usr/sbin/clinfo > /dev/null 2>&1
    if [ $? != 0 ]
    then
	exit 0
    fi

    # Check whether Europa is enabled
    /usr/cluster/lib/sc/scxcfg -G > /dev/null 2>&1
    if [ $? != 0 ]
    then
	exit 0
    fi

    # Check if the fcmmd program is running
    if [ "${FMMPID}xx" != "xx" ]
    then
        echo "${FMMPROG} pid = ${FMMPID} is already running ..."
	exit 0
    fi

    # Load the HB driver into the kernel
    load_driver

    # Build NIC_LST
    get_nic_list
    if [ $? -ne 0 ]
    then
	echo "Unable to retrieve interface list"
	exit 1
    fi

    # Insert the HB module into the stream of the NIC devices
    for NIC_DEV in ${NIC_LST}
    do
	load_module ${NIC_DEV}
    done

    # Launch fmmd program (daemonized)
    echo "starting  ${FMMDIR}/${FMMPROG}"
    ${FMMDIR}/${FMMPROG} &

    if [ $? -eq 0 ]; then
	    echo "${0} startup has succeeded ..."
    else
	    echo "${0} has failed ..."
	    exit 1
    fi

    # Wait for door server to be ready
    COUNTER=30
    while [ ${COUNTER} -gt 0 ]
    do
	COUNTER=`expr ${COUNTER} - 1`
	if [ -r ${FILE} ]
	then
		COUNTER=0
	fi
	/usr/bin/sleep 1
    done

    if [ ! -r ${FILE} ]
    then
	echo "${0} api not ready ..."
	exit 1
    fi
    ;;

stop)
    # Check if the fcmmd program is running
    if [ "${FMMPID}xx" != "xx" ]
    then
	# Stop the previous instance
	echo "Stopping ${FMMDIR}/${FMMPROG}"
	/usr/bin/pkill -x ${FMMPROG}
    else
        echo "${FMMPROG} is not running ..."
    fi
    ;;

*)
        echo "Usage: $0 { start | stop }"
        exit 1
        ;;

esac

exit 0
