#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)cleandisks	1.6	08/05/20 SMI"
#
# Copyright (c) 1999 by Sun Microsystems, Inc.
# All rights reserved.
#

# Only continue on a reconfig reboot.
if [ -z "$_INIT_RECONFIG" ]; then
	exit 0
fi

# Check if we're in clustered mode.
/usr/sbin/clinfo > /dev/null 2>&1
if [ $? != 0 ] ; then
	exit 0
fi

nodeid=`/usr/sbin/clinfo -n`

# Can't convert to cluster devnames unless the filesystem exists.
if [ ! -d /devices/node@$nodeid ]; then
	exit 0
fi

# These are directories with files which should be linked to the
# /devices/node@ files.
clean_directories="/dev/dsk /dev/rdsk /dev/term /dev/cua /dev/rmt /dev/cfg /dev/es /dev/osa/dev/dsk /dev/osa/dev/rdsk"

# These are files which are links to files which may have disappeared
# after the node@ conversion.
clean_links="/dev/tty*"

# Check if we've already converted to the new names.
not_converted=0

for dir in $clean_directories
do
	if [ ! -d $dir ]; then
		continue
	fi

	unconverted_count=`ls -l $dir | grep -c -v "/devices/node@$nodeid"`
	pseudo_count=`ls -l $dir | grep -c "devices/pseudo"`

	# /devices/pseudo entries don't need to be converted.
	if [ `/usr/bin/expr $unconverted_count - $pseudo_count` -gt 1 ]
	then
		not_converted=1
		break
	fi
done

if [ $not_converted -eq 0 ]; then
	# Exit if we already did this conversion.
	exit 0
fi

# Go through the directories, removing 'new' devices, and renaming the
# old ones.
for dir in $clean_directories
do
	if [ ! -d $dir ]; then
		continue
	fi

	for device in `ls $dir`
	do
		if [ ! -h $dir/$device ]; then
			continue
		fi

		linkpath=`ls -l $dir/$device | awk '{ print $NF }'`

		# /devices/pseudo entries don't need to be converted
		if [ `echo $linkpath | grep -c "devices/pseudo"` -gt 0 ]; then
			continue
		fi

		if [ `echo $linkpath | grep -c "node@$nodeid"` -gt 0 ]; then
			# Remove all of the new node@ entries that
			# just got created.
			rm $dir/$device
		else
			# If this isn't a node@ entry, we should change it
			# into one, so that the devices get the same names.
			newlink=`echo $linkpath | sed -e "s,/devices/,/devices/node@$nodeid/,"`
			rm $dir/$device
			ln -s $newlink $dir/$device
		fi
		
	done
done

# Now clean up the links
for file in `ls $clean_links`
do
	if [ ! -h $file ]; then
		continue
	fi

	linkpath=`ls -l $file | awk '{ print $NF }'`

	(cd `dirname $file`; if [ ! -r $linkpath ]; then rm $file; fi)
done
