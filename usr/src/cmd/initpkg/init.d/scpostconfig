#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)scpostconfig	1.16	09/02/26 SMI"
#

SC_CLUSTER=/usr/cluster/bin/cluster
SC_QUORUMCONFIG=/usr/cluster/lib/sc/scquorumconfig
POSTCONFIGFILE=/etc/cluster/ccr/global/postconfig
SCLIB=/usr/cluster/lib/sc

tmp_logfile="/tmp/postconfig.log.$$"

get_installmode()
{
        (
                LC_ALL=C; export LC_ALL
                mode=`${SC_CLUSTER} show -t global | sed -n 's/^  installmode:[   ]*\([^ ]*\).*/\1/p'`
                if [ -n "${mode}" ] && [ "${mode}" = "enabled" ]
		then
                        echo 1
                else
                        echo 0
                fi
        )

        return 0
}

# test whether we are a cluster and exit if not a cluster
/usr/sbin/clinfo > /dev/null 2>&1
if [ $? -ne 0 ]
then
	exit 0
fi

installmode=`get_installmode`
nodename=`/sbin/uname -n`

if [ ${installmode} -eq 1 ] && [ -f ${POSTCONFIGFILE} ] 
then
	tasks=`grep '^task\.' ${POSTCONFIGFILE} | awk -F. '{print $2}' | sort | uniq`

	for task in ${tasks}; do
		case "${task}" in
		'quorum')

			# Create quorum devices in 2 node clusters and do only
			# cluster initialization in >2 or 1 node clusters.
			nodecount=`grep '^task.' ${POSTCONFIGFILE} | awk -F. '{ if ($2 == "quorum") print $3 }' | wc -l`
			if [ ${nodecount} -eq 2 ]
			then
        			${SC_QUORUMCONFIG} > ${tmp_logfile} 2>&1
				exit_status=$?
				if [ ${exit_status} -eq 1 ]
				then
					/usr/bin/logger -p daemon.err -t SCPOSTCONFIG -f ${tmp_logfile}
					# SCMSGS
					# @explanation
					# The attempt to automatically
					# configure quorum disks failed on one
					# or more nodes. The last node to join
					# the cluster is supposed to configure
					# quorum. However, other nodes also
					# perform registration tasks when they
					# join the cluster. This message
					# indicates that one or more of these
					# attempts failed, which prevented the
					# last node from configuring quorum.
					# @user_action
					# Manually configure quorum disks and
					# initialize the cluster by using
					# clsetup after all nodes
					# have joined the cluster.
					${SCLIB}/scds_syslog -p error -t SCPOSTCONFIG -m "The quorum configuration task encountered a problem on node ${nodename}, manual configuration by using clsetup(1CL) might be necessary"
					cat ${tmp_logfile}
					echo "The quorum configuration task encountered a problem on node ${nodename}, manual configuration by using clsetup(1CL) might be necessary"

				elif [ ${exit_status} -eq 0 ]
				then
					/usr/bin/logger -p daemon.notice -t SCPOSTCONFIG -f ${tmp_logfile}
					# SCMSGS
					# @explanation
					# Quorum disk was configured and the
					# cluster was initialized on the last
					# of the 2 nodes to join the cluster.
					# @user_action
					# None.
					${SCLIB}/scds_syslog -p notice -t SCPOSTCONFIG -m "The quorum configuration task succeeded on node ${nodename}"
					cat ${tmp_logfile}
					echo "The quorum configuration task succeeded on node ${nodename}"

				fi
			else
        			${SC_QUORUMCONFIG} -i > ${tmp_logfile} 2>&1
				exit_status=$?
				if [ ${exit_status} -eq 1 ]
				then
					/usr/bin/logger -p daemon.err -t SCPOSTCONFIG -f ${tmp_logfile}
					# SCMSGS
					# @explanation
					# The attempt to automatically reset
					# cluster installmode or quorum votes
					# failed on one or more nodes. The
					# last node to join the cluster is
					# supposed to perform these tasks.
					# However, other nodes also perform
					# registration tasks when they join
					# the cluster. This message indicates
					# that one or more of these attempts
					# failed, which prevented the last
					# node from initializing the cluster.
					# @user_action
					# Run clsetup after all
					# nodes have joined the cluster, to
					# complete post-installation setup.
					${SCLIB}/scds_syslog -p error -t SCPOSTCONFIG -m "Cluster initialization encountered a problem on node ${nodename}, manual initialization by using clsetup(1CL) might be necessary"
					cat ${tmp_logfile}
					echo "Cluster initialization encountered a problem on node ${nodename}, manual initialization by using clsetup(1CL) might be necessary"

				fi
			fi
			rm -f ${tmp_logfile}
			break
        		;;
		*)
			# SCMSGS
			# @explanation
			# A task was found on the postconfiguration CCR file
			# that was not understood. This probably indicated
			# that the CCR file was corrupt.
			# @user_action
			# Manually configure quorum disks and initialize the
			# cluster by using clsetup after all nodes
			# have joined the cluster.
			${SCLIB}/scds_syslog -p error -t SCPOSTCONFIG -m "Unsupported task ${task} in ${POSTCONFIGFILE}"
			break
        		;;
		esac
	done
fi
