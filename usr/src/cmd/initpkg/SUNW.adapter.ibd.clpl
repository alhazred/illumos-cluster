#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2005 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident  "@(#)SUNW.adapter.ibd.clpl 1.3     08/05/20 SMI"
#
# Properties description file for cluster transport adapter "ibd"
#

#
# Transport types
#
adapters.ibd.transport_type.type	enum
adapters.ibd.transport_type.required	1
adapters.ibd.transport_type.default	dlpi
adapters.ibd.transport_type.values	dlpi

# Total bandwidth of the adapter on the wire. Values are in MB/sec
adapters.ibd.bandwidth.type	int
adapters.ibd.bandwidth.required	0
adapters.ibd.bandwidth.min	0
adapters.ibd.bandwidth.max	1250
adapters.ibd.bandwidth.default	700

#
# Percentage bandwidth consumed by scalable services
# Defaults to 80%
#
adapters.ibd.nw_bandwidth.type	int
adapters.ibd.nw_bandwidth.required	0
adapters.ibd.nw_bandwidth.min	5
adapters.ibd.nw_bandwidth.max	100
adapters.ibd.nw_bandwidth.default	80

#
# Heartbeat quantum. Values are in milliseconds
#
adapters.ibd.dlpi_heartbeat_quantum.type	int
adapters.ibd.dlpi_heartbeat_quantum.required	0
adapters.ibd.dlpi_heartbeat_quantum.default	1000
adapters.ibd.dlpi_heartbeat_quantum.min	100
adapters.ibd.dlpi_heartbeat_quantum.max	10000

adapters.ibd.dlpi_heartbeat_quantum_v1.type	int
adapters.ibd.dlpi_heartbeat_quantum_v1.required	0
adapters.ibd.dlpi_heartbeat_quantum_v1.default	1000
adapters.ibd.dlpi_heartbeat_quantum_v1.min	100
adapters.ibd.dlpi_heartbeat_quantum_v1.max	10000

#
# Heartbeat timeout. Values are in milliseconds
#
adapters.ibd.dlpi_heartbeat_timeout.type	int
adapters.ibd.dlpi_heartbeat_timeout.required	0
adapters.ibd.dlpi_heartbeat_timeout.default	10000
adapters.ibd.dlpi_heartbeat_timeout.min	500
adapters.ibd.dlpi_heartbeat_timeout.max 60000

adapters.ibd.dlpi_heartbeat_timeout_v1.type	int
adapters.ibd.dlpi_heartbeat_timeout_v1.required	0
adapters.ibd.dlpi_heartbeat_timeout_v1.default	10000
adapters.ibd.dlpi_heartbeat_timeout_v1.min	500
adapters.ibd.dlpi_heartbeat_timeout_v1.max	60000
#
# lazy_free indicates whether driver defers freeing of memory held by
# esballoc blocks or not
#
adapters.ibd.lazy_free.type    boolean
adapters.ibd.lazy_free.required	0
adapters.ibd.lazy_free.default	1
