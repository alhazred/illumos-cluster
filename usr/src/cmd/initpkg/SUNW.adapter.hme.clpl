#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 1999,2003 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident  "@(#)SUNW.adapter.hme.clpl 1.8     08/05/20 SMI"
#
# Properties description file for cluster transport adapter "hme"
#

#
# Transport types
#
adapters.hme.transport_type.type	enum
adapters.hme.transport_type.required	1
adapters.hme.transport_type.default	dlpi
adapters.hme.transport_type.values	dlpi

# Total bandwidth of the adapter on the wire. Values are in MB/sec
adapters.hme.bandwidth.type	int
adapters.hme.bandwidth.required	0
adapters.hme.bandwidth.min	0
adapters.hme.bandwidth.max	12
adapters.hme.bandwidth.default	10

#
# Percentage bandwidth consumed by scalable services
# Defaults to 80%
#
adapters.hme.nw_bandwidth.type	int
adapters.hme.nw_bandwidth.required	0
adapters.hme.nw_bandwidth.min	5
adapters.hme.nw_bandwidth.max	100
adapters.hme.nw_bandwidth.default	80

#
# Heartbeat quantum. Values are in milliseconds
#
adapters.hme.dlpi_heartbeat_quantum.type	int
adapters.hme.dlpi_heartbeat_quantum.required	0
adapters.hme.dlpi_heartbeat_quantum.default	1000
adapters.hme.dlpi_heartbeat_quantum.min	100
adapters.hme.dlpi_heartbeat_quantum.max	10000
adapters.hme.dlpi_heartbeat_quantum_v1.type	int
adapters.hme.dlpi_heartbeat_quantum_v1.required	0
adapters.hme.dlpi_heartbeat_quantum_v1.default	1000
adapters.hme.dlpi_heartbeat_quantum_v1.min	500
adapters.hme.dlpi_heartbeat_quantum_v1.max	10000

#
# Heartbeat timeout. Values are in milliseconds
#
adapters.hme.dlpi_heartbeat_timeout.type	int
adapters.hme.dlpi_heartbeat_timeout.required	0
adapters.hme.dlpi_heartbeat_timeout.default	10000
adapters.hme.dlpi_heartbeat_timeout.min	500
adapters.hme.dlpi_heartbeat_timeout.max	60000
adapters.hme.dlpi_heartbeat_timeout_v1.type	int
adapters.hme.dlpi_heartbeat_timeout_v1.required	0
adapters.hme.dlpi_heartbeat_timeout_v1.default	10000
adapters.hme.dlpi_heartbeat_timeout_v1.min	1500
adapters.hme.dlpi_heartbeat_timeout_v1.max	3600000
#
# lazy_free indicates whether driver defers freeing of memory held by
# esballoc blocks or not
#
adapters.hme.lazy_free.type    boolean
adapters.hme.lazy_free.required	0
adapters.hme.lazy_free.default	0
