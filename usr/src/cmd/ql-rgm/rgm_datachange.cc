/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_datachange.cc	1.5	09/01/13 SMI"


#include <stdio.h>
#define	__EXTENSIONS__
#include <string.h>
#include <stdlib.h>
#include <ccr_access.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#define	MAXNODES 64

#define	ERR_NOERR 		0	/* no error was found */
#define	ERR_NOMEM  	1	/* not enough swap */
#define	ERR_INVAL  	3	/* invalid input argument */
#define	ERR_INTERNAL 4	/* invalid input argument */
#define	ERR_INIT	5 /* Table initialization failed! */
#define	ERR_UPDATE	6 /* Update of a key element failed */
#define	ERR_COMMIT	7 /* Commit failed */
#define	ERR_PERM	8 /* Not allowed to run command */

#define	RGM_PROP_SEPARATOR	';'
#define	ON_OFF_SWITCH		"On_off_switch"
#define	INSTALLED_NODES_DEFAULT		"*"

/*
 * Data str defn's
 */
typedef struct ninfolist {
	char 	*n_name;
	uint_t	nid;
	struct ninfolist	*n_next;
} ninfolist_t;

typedef struct namelist {
	char		*nl_name;
	struct namelist	*nl_next;
} namelist_t;



static int add_nodename_nodeid(ninfolist_t **, uint_t, char *);
static int namelist_add_name(namelist_t **curr_namelist, char *name);
static void free_namelist(namelist_t *list_p);
static int commalist_to_namelist(char *optarg_str, namelist_t **lp);
static int update_onoff_monitored(namelist_t **rs1, char *nlist);
static char *my_strtok(char **str, char delimiter);
static char *get_nodeidlist(ninfolist_t *nl, namelist_t *nlist);
static uint_t get_nodeid(ninfolist *nl, char *s);
static void free_ninfolist(ninfolist_t *list_p);
static int sc_zonescheck();

int
main()
{
	ccr_access_readonly_table	read_tab;
	ccr_access_updatable_table	updat_tab;
	char				*datap = NULL, *p = NULL;
	uint_t				i;
	table_element_t			*te = NULL;
	char				nkey[24] = "cluster.nodes.";
	char 				rs_nkey[256] = "RS_";
	ninfolist_t			*nl = NULL;
	namelist_t			*rg_dir = NULL, *rt_dir = NULL;
	namelist_t			*rg_dir_tmp = NULL, *rt_dir_tmp = NULL;
	int				err = 0;
	char				*rs_list = NULL, *nlist = NULL;
	namelist_t			*nodelist = NULL;

	/*
	 * Resource list header and iterators
	 */
	namelist_t			*rs_iter = NULL, *rs_head = NULL;

	/*
	 * Resource datalist header and iterators
	 */
	namelist_t *rs_dhead = NULL, *rs_diter = NULL;
	if (sc_zonescheck() != 0) {
		return (ERR_PERM);
	}

	//
	// Opens the infrastructure table for reading.
	// Initialization is a must.
	//
	if (read_tab.initialize("infrastructure") != 0)
		return (ERR_INIT);
	p = nkey + strlen(nkey);
	for (i = 1; i <= MAXNODES; i++) {
		(void) sprintf(p, "%d.name", i);
		//
		// Query for the element.
		// The data value for the above element is
		// returned by the function.
		//
		datap = read_tab.query_element(nkey);
		if (datap != NULL) {
			if ((err = add_nodename_nodeid(&nl,
			    i, datap)) != 0) {
				read_tab.close();
				delete datap;
				return (err);
			}
		}
		delete datap;
	}
	//
	// Close the table before any updates are to be done on the table.
	// The close operation must happen after all reads are done.
	//
	read_tab.close();
	//
	// Read the directory table to get the resource group
	// file names.
	//
	if (read_tab.initialize("directory") != 0)
		return (ERR_INIT);
	read_tab.atfirst();
	while ((te = read_tab.next_element()) != NULL) {
		if (strncmp(te->key, "rgm_rg_", 7) == 0) {
			if ((err = namelist_add_name(&rg_dir, te->key))
			    != 0) {
				read_tab.close();
				goto finished;
			}
		} else if (strncmp(te->key, "rgm_rt_", 7) == 0) {
			if ((err = namelist_add_name(&rt_dir, te->key))
			    != 0) {
				read_tab.close();
				goto finished;
			}
		}
		delete te;
	}
	read_tab.close();

	/*
	 * Update RG tables. Change entries for Nodelist, On_off_switch
	 * and Monitored_switch
	 */
	for (rg_dir_tmp = rg_dir; rg_dir_tmp != NULL; rg_dir_tmp =
	    rg_dir_tmp->nl_next) {
		if (read_tab.initialize(rg_dir_tmp->nl_name) != 0) {
			err = ERR_INIT;
			goto finished;
		}
		nlist = read_tab.query_element("Nodelist");
		if ((err = commalist_to_namelist(nlist, &nodelist))
		    != 0) {
			delete nlist;
			read_tab.close();
			goto finished;
		}
		delete nlist;
		// Comma seperated list of nodeids
		nlist = get_nodeidlist(nl, nodelist);
		if (nlist == NULL) {
			read_tab.close();
			err = ERR_INVAL;
			goto finished;
		}

		rs_list = read_tab.query_element("Resource_list");
		if ((err = commalist_to_namelist(rs_list, &rs_head))
		    != 0) {
			delete rs_list;
			read_tab.close();
			goto finished;
		}
		delete rs_list;
		rs_iter = rs_head;
		p = rs_nkey + 3;
		while (rs_iter != NULL) {
			(void) sprintf(p, "%s", rs_iter->nl_name);
			datap = read_tab.query_element(rs_nkey);
			if ((err = namelist_add_name(&rs_dhead, datap))
			    != 0) {
				read_tab.close();
				delete datap;
				goto finished;
			}
			rs_iter = rs_iter->nl_next;
			delete datap;
		}
		rs_diter = rs_dhead;
		// Update the onoffswitch and monitored switch values.
		if ((err = update_onoff_monitored(&rs_diter, nlist)) !=
		    0) {
			goto finished;
		}
		read_tab.close();
		if (updat_tab.initialize(rg_dir_tmp->nl_name)
		    != 0) {
			err = ERR_INIT;
			goto finished;
		}
		if (updat_tab.update_element("Nodelist", nlist) != 0) {
			err = ERR_UPDATE;
			free(nlist);
			goto finished;
		}
		free(nlist);

		rs_iter = rs_head;
		p = rs_nkey + 3;
		rs_diter = rs_dhead;
		while (rs_iter != NULL) {
			(void) sprintf(p, "%s", rs_iter->nl_name);
			if (updat_tab.update_element(rs_nkey, rs_diter->nl_name)
			    != 0) {
				err = ERR_UPDATE;
				goto finished;
			}
			rs_diter = rs_diter->nl_next;
			rs_iter = rs_iter->nl_next;
		}
		if (updat_tab.commit() != 0) {
			err = ERR_COMMIT;
			goto finished;
		}
		free_namelist(rs_dhead);
		free_namelist(rs_head);
		free_namelist(nodelist);
		rs_dhead = NULL;
		rs_diter = NULL;
		nodelist = NULL;
		rs_iter = NULL;
		rs_head = NULL;
		p = NULL;

	}
	free_namelist(rg_dir);
	/*
	 * Update RT tables.
	 * Installed_nodes property updated here.
	 */
	for (rt_dir_tmp = rt_dir; rt_dir_tmp != NULL; rt_dir_tmp =
	    rt_dir_tmp->nl_next) {
		if (read_tab.initialize(rt_dir_tmp->nl_name) != 0) {
			err = ERR_INIT;
			goto finished;
		}
		nlist = read_tab.query_element("Installed_nodes");
		if (strcmp(nlist, INSTALLED_NODES_DEFAULT) != 0) {
			// Convert comma separated list to namelist
			if ((err = commalist_to_namelist(nlist, &nodelist))
			    != 0) {
				delete nlist;
				goto finished;
			}
			delete nlist;
			// Comma seperated list of nodeids
			nlist = get_nodeidlist(nl, nodelist);
			if (nlist == NULL) {
				read_tab.close();
				free(nlist);
				err = ERR_INVAL;
				goto finished;
			}

			read_tab.close();
			if (updat_tab.initialize(rt_dir_tmp->nl_name) != 0) {
				err = ERR_INIT;
				free(nlist);
				goto finished;
			}
			if (updat_tab.update_element("Installed_nodes", nlist)
			    != 0) {
				err = ERR_UPDATE;
				free(nlist);
				goto finished;
			}
			if (updat_tab.commit() != 0) {
				err = ERR_COMMIT;
				free(nlist);
				goto finished;
			}
			free(nlist);
		} else {
			read_tab.close();
			delete nlist;
		}
		nlist = NULL;
	}
	free_namelist(rt_dir);
	free_ninfolist(nl);
	return (err);

finished :
	if (nl)
		free_ninfolist(nl);
	if (rg_dir)
		free_namelist(rg_dir);
	if (rt_dir)
		free_namelist(rt_dir);
	if (rs_head)
		free_namelist(rs_head);
	if (rs_dhead)
		free_namelist(rs_dhead);
	return (err);
}

static int
namelist_add_name(namelist_t **curr_namelist, char *name)
{

	namelist_t *new_node = NULL;
	int err_code = 0;

	if ((new_node = (namelist_t *)
		malloc(sizeof (namelist_t))) == NULL) {
		return (ERR_NOMEM);
	}

	new_node->nl_name = strdup(name);
	if (new_node->nl_name == NULL)
		return (ERR_NOMEM);

	new_node->nl_next = NULL;

	if (*curr_namelist == NULL)
		// first element in the list hence set curr_namelist
		*curr_namelist = new_node;
	else {
		namelist_t *curr_node = *curr_namelist;
		// append the new node to the end of the list
		while (curr_node->nl_next != NULL)
			curr_node = curr_node->nl_next;
		curr_node->nl_next = new_node;
	}
	return (err_code);
}

static int
commalist_to_namelist(char *arg, namelist_t **lp)
{
	int status = 0;
	namelist_t *head = NULL;
	namelist_t *tail = NULL;
	namelist_t *elt = NULL;
	char 	*name = NULL;
	char *optarg_str = strdup(arg);

	char lasts[160];
	char *lasts_p = &lasts[0];

	/*
	 * for each elt in comma list
	 * make new namelist_t struct
	 * fill in name field and
	 * attach to list
	 */

	name = (char *)strtok_r(optarg_str, ",",
	    (char **)&lasts_p);
	while (name != NULL) {
		/*
		 * calloc a new namelist_t struct
		 * and space for name
		 */
		elt = (namelist_t *)calloc(1,
		    sizeof (namelist_t));

		if (elt) {
			elt->nl_name = strdup(name);
			elt->nl_next = NULL;
		}

		if (!elt || !elt->nl_name) {
			status = ERR_NOMEM;
			goto exit_err;
		}

		if (head == NULL) {
			head = elt;
		}
		if (tail) {
			tail->nl_next = elt;
		}
		tail = elt;
		elt = NULL;

		/* get next elt from comma list */
		*(name + strlen(name)) = ',';
		name = (char *)strtok_r(NULL, ",", (char **)&lasts_p);
	} /* end while */

	/*
	 * now give new list to caller
	 */
	*lp = head;
	free(optarg_str);
	return (status);

exit_err:
	if ((elt != NULL) && ((head == NULL) ||
	    (elt->nl_name == NULL))) {
		/* 2 cases where elt not yet attached to list */
		free_namelist(elt);
	}
	if (head) {
		free_namelist(head);
	}
	return (status);

} /* commalist_to_namelist */

static void
free_ninfolist(ninfolist_t *list_p)
{
	ninfolist_t *ptr = list_p;
	ninfolist_t *next = NULL;

	while (ptr != NULL) {
		if (ptr->n_name != NULL) {
			free(ptr->n_name);
		}
		next = ptr->n_next;
		free(ptr);
		ptr = next;
	}
} /* free_ninfolist */

static void
free_namelist(namelist_t *list_p)
{
	namelist_t *ptr = list_p;
	namelist_t *next = NULL;

	while (ptr != NULL) {
		if (ptr->nl_name != NULL) {
			free(ptr->nl_name);
		}
		next = ptr->nl_next;
		free(ptr);
		ptr = next;
	}
} /* free_namelist */

/*
 * Input: 1. Linked list of resource data.
 *	  2. Comma separated string of nodeids (RG nodelist)
 * Output: Modified list of resource data with
 *		1. Nodelist entry changed to nlist
 *		2. On_off_switch/Monitored_switch if enabled,
 *		changed to nlist.
 */

static int
update_onoff_monitored(namelist_t **rs1, char *nlist)
{
	namelist_t *nl = NULL;
	char  *s = NULL, *p = NULL, *endp = NULL, *q = NULL;
	int swtch;
	uint_t len;
	char *new_str = NULL, *r = NULL;

	for (nl = *rs1; nl != NULL; nl = nl->nl_next) {
		s = strdup(nl->nl_name);
		if (s == NULL) {
			return (ERR_NOMEM);
		}
		r = s;
		len = strlen(s) + strlen(nlist) * 2;
		new_str = (char *)malloc(sizeof (char) * len);
		if (new_str == NULL) {
			free(r);
			return (ERR_NOMEM);
		}
		*(new_str) = '\0';
		p = (char *)strstr(s, ON_OFF_SWITCH);
		*(p) = '\0';
		(void) strcpy(new_str, s);
		*(p) = 'O';
		s = p;
		// Next is onoff_switch followed by Monitored switch.
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		q = my_strtok(&p, '=');
		(void) strcat(new_str, q);
		(void) strcat(new_str, "=");
		swtch = strtol(p, &endp, 0);
		if (swtch != 0) {
				(void) strcat(new_str, nlist);
		} else {
				new_str = (char *)realloc(new_str,
				    len - strlen(nlist) - 1);
		}
		(void) strcat(new_str, ";");
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		q = my_strtok(&p, '=');
		(void) strcat(new_str, q);
		(void) strcat(new_str, "=");
		swtch = strtol(p, &endp, 0);
		if (*endp != '\0') {
			free(r);
			return (ERR_INTERNAL);
		}
		if (swtch != 0) {
				(void) strcat(new_str, nlist);
		} else {
				new_str = (char *)realloc(new_str,
				    len - strlen(nlist) - 1);
		}
		(void) strcat(new_str, ";");
		(void) strcat(new_str, s);
		free(nl->nl_name);
		free(r);
		nl->nl_name = new_str;
		new_str = NULL;
	}
	return (ERR_NOERR);
}

/*
 * Return the first token in the input str as delimited
 * by the delimiter or the end of the string (which
 * ever comes first.
 * The delimiter is replaced with a terminating '\0'.
 *
 * The input str is also modified to point to
 * the first character after to delimiter, i.e.
 * str is adjusted to point to the start of
 * the next token after the one just returned.
 *
 * Note that the original input string is destroyed
 * in the course of breaking-off tokens.
 */
static char *
my_strtok(char **str, char delimiter)
{
	char *temp, *temp1;
	if (*str == NULL)
		return (NULL);

	temp = (char *)strchr(*str, delimiter);
	if (temp == NULL) {
		temp = *str;
		*str = NULL;
	} else {
		temp1 = temp + sizeof (char);
		*temp = '\0';
		temp = *str;
		*str = temp1;

	}
	return (temp);
}

static char *
get_nodeidlist(ninfolist_t *nl, namelist_t *nlist)
{
	char *data;
	uint_t nid, len;
	char tmp[15];
	/*
	 * The upper bound of the permitted nodeid's is 64.
	 */
	data = (char *)malloc(sizeof (char) * 183);
	if (data == NULL)
		return (NULL);
	*(data) = '\0';
	for (; nlist != NULL; nlist = nlist->nl_next) {
		nid = get_nodeid(nl, nlist->nl_name);
		if (nid == 0) {
			free(data);
			return (NULL);
		}
		(void) sprintf(tmp, "%d,", nid);
		(void) strcat(data, tmp);
	}
	len = strlen(data);
	data[len-1] = '\0';
	data = (char *)realloc(data, len-1);
	return (data);
}

static uint_t
get_nodeid(ninfolist *nl, char *s)
{
	for (; nl != NULL; nl = nl->n_next) {
		if (strcmp(nl->n_name, s) == 0)
			return (nl->nid);
	}
	return (0);
}

/*
 * add_nodename_nodeid()
 * This function inserts an element into the ninfolist_t
 * Memory is allocated for n_name. Callers
 * responsibility to free memory allocated for nl_name.
 */
static int
add_nodename_nodeid(ninfolist_t **curr_namelist, uint_t i, char *name)
{

	ninfolist_t *new_node = NULL;
	int err_code = 0;

	if ((new_node = (ninfolist_t *)
		malloc(sizeof (ninfolist_t))) == NULL) {
		err_code = ERR_NOMEM;
		return (err_code);
	}

	new_node->n_name = strdup(name);
	if (new_node->n_name == NULL)
		return (ERR_NOMEM);
	new_node->nid = i;
	new_node->n_next = NULL;

	if (*curr_namelist == NULL)
		// first element in the list hence set curr_namelist
		*curr_namelist = new_node;
	else {
		ninfolist_t *curr_node = *curr_namelist;
		// append the new node to the end of the list
		while (curr_node->n_next != NULL)
			curr_node = curr_node->n_next;
		curr_node->n_next = new_node;
	}
	err_code = ERR_NOERR;
	return (err_code);
}

static int
sc_zonescheck() {

#if SOL_VERSION >= __s10
	zoneid_t zoneid;

	if ((zoneid = getzoneid()) < 0) {
		return (1);
	}
	if (zoneid != 0) {
		return (1);
	}
#endif
	return (0);
}
