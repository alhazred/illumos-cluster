/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)modify_ip.cc	1.8	08/06/27 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/param.h>
#include <libintl.h>
#include <locale.h>
#include <sys/os.h>
#include <sys/cladm.h>
#include <sys/clconf_int.h>

#include <rgm/sczones.h>

#define	MODIFY_IP_CMD "/usr/cluster/lib/sc/modify_ip"

static void
print_usage()
{
	(void) fprintf(stderr, gettext("\nUsage:\n"
		"%s <netaddr> <cluster_netmask> <netmask> <subnetnetmask> "
		"<maxnodes> <maxprivatenets>\n"), MODIFY_IP_CMD);
}

/*
 * Update the cluster configuration with the new
 * IP address range. Call the clconf_modify_ip
 * function with the new IP address range in terms
 * of the cluster network address, cluster netmask,
 * subnet netmask, maxnodes and maxprivnets.
 * Note that this command results in writing the
 * new IP address range and new IP address assignments
 * to the /etc/cluster/ccr/global/infrastructure.new file.
 */
int
main(int argc, char **argv)
{
	char *filename = NULL;
	int rcode = 0;
	int bootflags = 0;

	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	if (argc != 7) {
		print_usage();
		return (1);
	}

	// Make sure we are root
	if (getuid() != 0) {
		(void) fprintf(stderr,
			gettext("Must be root to run command\n"));
		return (1);
	}

	/*
	 * Make sure that we are *not* in the cluster.
	 *
	 */
	if ((os::cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
		(bootflags & CLUSTER_BOOTED)) {
		(void) fprintf(stderr, gettext("Command invalid in "
			"cluster mode.  Boot into non-cluster mode and "
			"re-run command\n"));
		return (1);
	}

	filename = os::strdup("/etc/cluster/ccr/global/infrastructure");

	// Check if the instrastructure file exists in the ccr
	if (access(filename, F_OK) != 0) {
		(void) fprintf(stderr,
			gettext("Error: %s does not exist\n"), filename);
		exit(1);
	}

	rcode = clconf_modify_ip(filename, argv[1], argv[2],
		argv[3], argv[4], argv[5], argv[6]);

	return (rcode);
}
