/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DIDADM_H
#define	_DIDADM_H

#pragma ident	"@(#)didadm.h	1.7	08/05/20 SMI"

#include <cl_errno.h>

#ifdef __cplusplus
extern "C" {
#endif

#define	CLCMD_ENV	"CLCMD_COMMAND_EXEC"
#define	EXIT_MESSAGE	"RETURN_VALUE"
#define	CLCOMMANDS_VAR_NO_MESSAGE_IDS_SET	\
			"CLCOMMANDS_NO_MESSAGE_IDS=true"

#ifdef __cplusplus
}
#endif

#endif /* _DIDADM_H */
