/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)didadm.c	1.134	09/03/11 SMI"

#include "didadm.h"
#include <clmsg_int.h>
#include <stdio.h>
#define	__EXTENSIONS__
#include <strings.h>
#include <fcntl.h>
#include <malloc.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>
#include <devid.h>
#include <ctype.h>
#include <libdevinfo.h>
#include <sys/param.h>
#include <sys/sunddi.h>
#include <sys/ddi_impldefs.h>
#include <sys/dditypes.h>
#include <sys/stat.h>
#include <sys/dkio.h>
#include <sys/errno.h>
#include <sys/didio.h>
#include <sys/quorum_int.h>
#include <sys/clconf_int.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dc/libdcs/libdcs.h>
#include <sys/mkdev.h>
#include <string.h>
#include <sys/scsi/scsi.h>
#include <assert.h>
#include <locale.h>
#include <dirent.h>
#include <ftw.h>
#include <libdid.h>
#include <didprivate.h>
#include <sys/sc_syslog_msg.h>
#include <libintl.h>
#include <sys/cladm_int.h>
#include <signal.h>
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>
#include <netdb.h>
#include <string.h>
#include <pthread.h>
#include <sys/mhd.h>

#include <libscdpm.h>
#include <sys/cl_efi.h>
#include <sys/sol_version.h>
#include "rldr_linklist.h"
#include "did_threads.h"
#if SOL_VERSION >= __s9
#define	DID_MULTI_INSTANCE
#endif
#include <rgm/sczones.h>

/*
 * scsi3 list is currently disabled until we have the right algorithm to
 * detect SCSI3 devices.
 */
#define	DISABLE_SCSI3_LIST

#ifndef	DISABLE_SCSI3_LIST
/*
 * Vendor/Product pair string. Always 24 char long.
 * For ex. "SEAGATE ST19171FCSUN9.0G".
 */
/* Path to the scsi2 only list. */
#define	SCSI2ONLYLIST	"/etc/cluster/scsi/scsi2onlydevlist"
#define	DEVSTRING_LEN	24

/*
 * Keeps track of scsi2 only device list.
 * Need to use SList to support dynamic sized array once this file
 * is converted to C++.
 */
static char	scsi2onlylist[30][DEVSTRING_LEN + 1];
static int	scsi2onlycount = 0;
#endif

#define	DID_RELOADER_CMD "/usr/cluster/lib/sc/did_reloader"

/* Tag used in syslog messages from this command */
#define	DIDADM	"didadm"

/* Time interval in ms, used in poll commands to wait */
#define	POLL_INTERVAL 10

/* Number of times to poll() before deciding to proceed. ~=10 seconds */
#define	POLL_TIMEOUT 1000

static did_device_list_t	*instlist = NULL;
static did_repl_list_t		*repllist = NULL;
static did_device_list_t	*ftwlist = NULL;
static did_device_type_t	*typelist = NULL;
static char			node_prefix[80];
static int			exit_code = 0;
static int			exit_message = 0;
static int			CLDEVICE_CMD = 0;
static rldr_devlist_t		*failed_dev_paths = NULL;
static char			**iscsi_paths = NULL;
static int			iscsi_path_count = 0;
static int			call_reloader = 0;

static void			update_driver_inst(did_device_list_t *dlistp);
static void			loadRecordDescription(int fd);
static void			showRecordDescription(void);
static did_device_list_t	*findInstanceByNum(int instnumber);
static did_device_list_t	*get_instance_from_devid(ddi_devid_t devid,
    const char *disk);
static char			*diskidtostr(char *id, int idlen);
static char			*typetostring(unsigned short type);
static void			print_error(char *, char *, ...);
static void			add_disk_path(const char *);
static void			add_tape_path(const char *);
static did_subpath_t		*find_instance_ptr(const char *inst);
static void			lock_driver(int lock);
static void			get_kernel_instances(int instance, char mode);
static void			init_instances(void);
#ifdef DID_MULTI_INSTANCE
static int			update_didconf(void);
static int			get_insts_from_didconf(unsigned int *,
				    unsigned int *);
#endif
static void			list_instances(char *instance, int list, int k);
static void			check_consistency(void);
static void			list_header(void);
static void			list_globalfencing_status(void);
static void			load_instances(int);
static boolean_t		validate_instance_name(char *instance, int len);
static int			get_instance_number(char *instance);
static void			discover_paths(void);
static void			device_cleanup(void);
static int			rm_driver_inst(int *inst, int print_error);
static int			replica_is_local(did_repl_list_t *repl);
static char			*check_dev_group(did_device_list_t *src_inst,
				    did_device_list_t *dest_inst);
static int			validate_repl_type(char *repl_type);
static did_repl_list_t		*find_repl_match(int instance);
static int			check_repl_devid(did_device_list_t *instptr,
				    ddi_devid_t devid);
static void			did_manual_unreplicate(int dev);
static int			unreplicate_device(did_repl_list_t *device,
				    char *remote_node,
				    did_repl_path_t **repl_sync_nodes);
static int			check_existing_repl_devices(char *inputfile,
				    char *remote_node,
				    did_repl_path_t **repl_sync_nodes);
static void			add_new_repl_devices(char *inputfile,
				    did_repl_path_t **repl_sync_nodes);
static did_repl_list_t		*did_remap_combine_inst(
				    did_device_list_t *src_inst,
				    did_device_list_t *dest_inst,
				    char *dev_group);
static void			did_remap_move_inst(did_device_list_t *src_inst,
				    int dest_dev, int do_ccr);
static void			did_remap(int src_dev, int dest_dev,
				    char *repl_dev_group);
static void			did_auto_remap(char *remote_node,
				    char *fmtfile);
static void			did_auto_sync_remap(int unreplicate);
static void			usage(char *name);
static int			confirm_device(char *device,
				    did_device_type_t *type);
static int			find_instance(char *inst);
static int			find_instance_by_path(char *path);
static void			verify_scsi_state(int scrub_only);
static int			repair_instance(int inst);
static int			repair_instances(char *instance, char *fmtfile);
static int			repair_all_instances(int *);
static int			get_instinfo(int inst,
					did_device_list_t **dlistp,
					did_subpath_t **subptr);
static int			getinst_next_local_path(did_subpath_t *subptr);
static int			list_instance(int inst, int longlist, int k);
static int			kget_instinfo(int inst,
					did_device_list_t **dlistp,
					did_subpath_t **subptr);
static int			lookup_devid(char *path, ddi_devid_t tdevid);
static void			add_dev_with_devid(ddi_devid_t devid,
					const char *cpath, did_device_type_t
					*devtype);
static void			add_dev_with_no_devid(const char *cpath,
					did_device_type_t *devtype);
static char			*minor_path(const char *device,
					did_device_type_t *devtype);
static char			*no_minor_path(const char *device,
					did_device_type_t *devtype);
static int			convert_conf_to_ccr(char *fmtfile,
					char *savefile);
static void			update_devid(did_device_list_t *dlistp,
					ddi_devid_t devid);
static int			did_same_dev(ddi_devid_t devid,
					ddi_devid_t target_devid,
					did_device_list_t *instptr);
static int			get_subpathdevid(did_subpath_t *subptr,
					ddi_devid_t *devid);
static int			did_update_device_service(int instance,
					int *ds_removed);
static int			switch_globalfencing(unsigned short newstatus,
					unsigned short scrub);
static int			get_globalfencing_status(void);
static int			set_disk_default_fencing(char *disks[],
				    int numdisks, unsigned short protocol,
				    unsigned short scrub);
static int			get_connected_nodes(did_device_list_t *dlistp,
				    nodeid_t *nodelist);
static int			run_reserve_on_connected_nodes(boolean_t
				    reg_scsi3, char *cmd, nodeid_t *nodelist);
static void			run_cmd_on_node(void *remote_invo_info);
static int			get_scsi3dev_list(void);
static unsigned short		scsi_fencing_inquiry(char *devname);
static int			is_quorum(did_device_list_t *dev);
static char			*get_full_did_path(did_device_list_t *dev);
static void			did_remove_repl_entry(int instance);

static char *get_fmt_inst(did_device_list_t *, did_subpath_t *);
static char *get_fmt_path(did_device_list_t *, did_subpath_t *);
static char *get_fmt_fullpath(did_device_list_t *, did_subpath_t *);
static char *get_fmt_host(did_device_list_t *, did_subpath_t *);
static char *get_fmt_name(did_device_list_t *, did_subpath_t *);
static char *get_fmt_fullname(did_device_list_t *, did_subpath_t *);
static char *get_fmt_diskid(did_device_list_t *, did_subpath_t *);
static char *get_fmt_asciidiskid(did_device_list_t *, did_subpath_t *);
static char *get_fmt_defaultfencing(did_device_list_t *, did_subpath_t *);
static char *get_fmt_replication(did_device_list_t *, did_subpath_t *);
static char *get_fmt_detectedfencing(did_device_list_t *, did_subpath_t *);

/*
 * The following three functions are only called in the special internal
 * option in didadm, -a. This special option is used by the DID reloader
 * daemon to reload DID driver instances of specific devices.
 */
static int			reload_driver_inst(int fd,
				    did_device_list_t *dlistp);
static void			reload_instance(int fd, void *args);
static int			reload_instances(char *fmtfile);

static char	*Version = "3.2";
static int	Verbose = 0;
static int	header = 0;
static char	*dformat[] = { "inst", "fullpath", "fullname" };
static int	dfmtcnt = 3;
static char	*format[10];
static int	fmtcnt = 0;
static int	do_logging = 1;
static int	need_cleanup = 0;
static int	dcs_initialized = 0;
static quorum_status_t	*quor_table = NULL;	/* List of quorum devices */
/* global algorithm to use to determine which fencing protocol to use. */
static unsigned short	cur_globalfencing_status = GLOBAL_FENCING_UNKNOWN;

struct	fmts {
	char	*fmtname;
	char	*fmt;
	char	*hdr;
	char	*(*getfmtdata)(did_device_list_t *, did_subpath_t *);
	};

static struct fmts fmts[] = {
		{ "inst",	"%-8s ", "Instance", get_fmt_inst },
		{ "instance",	"%-8s ", "Instance", get_fmt_inst },
		{ "path",	"%-20s ", "Physical Path", get_fmt_path },
		{ "fullpath",	"%-30s ", "Physical Path", get_fmt_fullpath },
		{ "host",	"%-10s ", "Host", get_fmt_host },
		{ "name",	"%-20s ", "Pseudo Path", get_fmt_name },
		{ "fullname",	"%-20s ", "Pseudo Path", get_fmt_fullname },
		{ "diskid",	"%-35s ", "Disk Id", get_fmt_diskid },
		{ "asciidiskid", "%-20s ", "Disk Id", get_fmt_asciidiskid },
		{ "replication", "%-10s", "Replication", get_fmt_replication },
		{ "detectedfencing", "%-16s", "Detected_Fencing",
		    get_fmt_detectedfencing },
		{ "defaultfencing", "%-16s", "Default_Fencing",
		    get_fmt_defaultfencing }
	};

/* Struct for making remote invocation */
struct remote_invo {
	nodeid_t	nodeid;
	char		*cmd;
};

#if SOL_VERSION >= __s10
#define	DID_UPDATE_VFSTAB_LINK "/etc/rcS.d/S68did_update_vfstab"
#else
#define	DID_UPDATE_VFSTAB_LINK "/etc/rc2.d/S72did_update_vfstab"
#endif

#define	DEFAULT_DIDPATH	"/dev/did/"

#ifdef DID_MULTI_INSTANCE

/* Solaris Driver configuration files directory */
#define	DIDCONF_DIR		"/kernel/drv"
/* DID Driver configuration file */
#define	DIDCONF_FILE		DIDCONF_DIR"/did.conf"
/* Temporary DID Driver configuration file */
#define	DIDCONF_TMP		DIDCONF_DIR"/did.conf.tmp"
/* Saved DID Driver configuration file */
#define	DIDCONF_SV		DIDCONF_DIR"/did.conf.sv"
#define	DIDCONF_LINE_SIZE	80
#define	DIDCONF_ADMIN_INSTANCE	\
	"name=\"did\" parent=\"pseudo\" device-type=\"admin\" instance="
#define	DIDCONF_DISK_INSTANCE	\
	"name=\"did\" parent=\"pseudo\" device-type=\"disk\" instance="
#define	DIDCONF_TAPE_INSTANCE	\
	"name=\"did\" parent=\"pseudo\" device-type=\"tape\" instance="

#endif

#define	SERROR "Could not locate instance \"%s\".\n"
#define	DERROR "Could not locate instance \"%d\".\n"

#define	AUTO_SYNCREPL "/usr/cluster/bin/scdidadm -YW "
#define	MANUAL_SYNCREPL "/usr/cluster/bin/scdidadm -Yw "

sc_syslog_msg_handle_t	handle;

char *repl_type = "truecopy";
char get_repl_devs_cmd[MAXPATHLEN];

int
main(int ac, char *av[])
{
	int	i;
	uint_t	j;
	char	s[80];
	char	*savefile = (char *)NULL, *fmtfile = (char *)NULL;
	int	option_set = 0;
	int	list = 0, update = 0, reconfigure = 0;
	int	repair = 0, repair_all = 0, init = 0, longlist = 0;
	int	check = 0, notsave = 0, convert = 0, cleanup = 0;
	int	lockunlock = 0, lockdriver = 0, kernel = 0;
	int	reload = 0;
	unsigned short	newglobalfencingstatus = 0;
	int	displayglobalfencingstatus = 0;
	unsigned short	setdiskdefaultfencing = 0;
	unsigned short setdiskdefaultscrub = 1;
	int	syncup = 0;
	int	libinit = 0;
	extern	char	*optarg;
	extern	int	optind;
	int	flag = REMOVABLE;
	int	err = 0;
	int	manual_remap = 0;
	int	auto_remap = 0;
	int	auto_sync_remap = 0;
	int	manual_unreplicate = 0;
	int	unreplicate;
	int	src_dev = 0;
	int	dest_dev = 0;
	char	*src_device = NULL;
	char	*dest_device = NULL;
	char	*remote_node = NULL;
	char	*cmd_name = NULL;
	char	*repl_dev_group = NULL;
	int	combine_options = 0;

	/* See if this is from the newcli */
	cmd_name = getenv(CLCMD_ENV);
	if (cmd_name) {
		CLDEVICE_CMD++;
	} else {
		/* Set an evnironment variable to turn off msg ID's */
		(void) putenv(CLCOMMANDS_VAR_NO_MESSAGE_IDS_SET);
		cmd_name = av[0];
	}

	/* Initialize and demote to normal uid */
	if (!CLDEVICE_CMD) {
		cl_auth_init();
		cl_auth_demote();
	}

	/* Initialize the clmsg library */
	if (init_clmsg(cmd_name)) {
		(void) fprintf(stderr, gettext("Could not initialize message "
		    "printing library.\n"));
		exit(CL_ENOMEM);
	}

/*lint -e668 -save */
/* Hide: Possibly passing a null pointer to strXXX(). */

	(void) setlocale(LC_ALL, "");
#if !defined(TEXT_DOMAIN)	/* Should be defined by cc -D */
#define	TEXT_DOMAIN "SYS_TEST"	/* Use this only if it weren't */
#endif
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	/* Check the zone */
	if (sc_zonescheck() != 0) {
		if (CLDEVICE_CMD)
			return (CL_EOP);
		else
			return (1);
	}

	(void) sc_syslog_msg_initialize(&handle, "Cluster.devices.did", "");

	if (ac == 1)
		usage(av[0]);

	errno = 0;
	if (gethostname(node_prefix, sizeof (node_prefix)) != 0) {
		/*
		 * gethostname failed. We fail the command.
		 */
		if (do_logging) {
			/*
			 * SCMSGS
			 * @explanation
			 * The hostname of the cluster node could not be
			 * determined.
			 * @user_action
			 * Check if the command was run prior to setting the
			 * hostname. Contact your authorized Sun service
			 * provider to determine whether a workaround or patch
			 * is avaliable.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE,
			    "%s: Unable to determine hostname. Error: %s",
			    av[0], strerror(errno));
		}
		(void) fprintf(stderr,
		    gettext("%s: Unable to determine hostname. Error: %s\n"),
		    av[0], strerror(errno));
		exit(1);
	} else if (node_prefix[0] == '\0') {
		/*
		 * gethostname returned an empty string. We fail the command.
		 */
		if (do_logging) {
			/*
			 * SCMSGS
			 * @explanation
			 * The hostname of the cluster node has not been set.
			 * @user_action
			 * Check if the hostname has been set for the node.
			 * Contact your authorized Sun service provider for
			 * further assistance.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE,
			    "%s: Hostname not set.",
			    av[0]);
		}
		(void) fprintf(stderr,
		    gettext("%s: Hostname not set.\n"),
		    av[0]);
		exit(1);
	}

	(void) strcat(node_prefix, ":");

	/* we default to truecopy if no replication type is provided */
	(void) sprintf(get_repl_devs_cmd,
	    "/usr/cluster/lib/sc/repl/get_truecopy_dev_groups");

	while ((i = getopt(ac, av,
	    "SkKm:cUo:GhilLs:rf:d:CunvVajRXF:T:t:w:W:b:zYe:g:")) != EOF) {
		/*
		 * This variable is used to determine if any command line
		 * options were given to the command. If none were given,
		 * at the completion of option processing we will print
		 * a usage message and return an error.
		 */
		option_set = 1;

		switch (i) {
		case 'o':
			{
			char *fopt = strtok(optarg, ",");

			do {
				for (j = 0; j < sizeof (fmts)/sizeof (*fmts);
				    j++)
					if (strcmp(fmts[j].fmtname, fopt)
					    == 0) {
						format[fmtcnt++] = fopt;
						break;
					}

				if (j == sizeof (fmts)/sizeof (*fmts)) {
					clerror("Invalid format: %s.\n", fopt);
					usage(av[0]);
					fmtcnt--;
				}
			} while ((fopt = strtok((char *)NULL, ",")) != NULL);
			}
			break;
		case 'c':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-c",
				    CL_AUTH_DEVICE_READ, 1);
			}

			check++;
			break;
		case 'U':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-U",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}

			convert++;
			break;
		case 'h':
			header++;
			break;
		case 'i':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-i",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}

			init++;
			break;
		case 'L':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-L",
				    CL_AUTH_DEVICE_READ, 1);
			}

			longlist++;
			break;
		case 'l':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-l",
				    CL_AUTH_DEVICE_READ, 1);
			}

			list++;
			break;
		case 'm':
			lockunlock++;
			lockdriver = atoi(optarg);
			break;
		case 'n':
			notsave++;
			break;
		case 'u':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-u",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}
			update++;
			break;
		case 'R':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-R",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}

			if ((ac - optind) == 0) {
				clerror("\'-R\' needs at least one disk.\n",
				    av[0]);
				exit(1);
			}
			if (strcmp(av[optind], "all") == 0)
				repair_all++;
			else
				repair++;
			break;
		case 'a':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-a",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}

			reload++;
			break;
		case 'j':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-a",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}

			call_reloader++;
			break;
		case 'r':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-r",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}
			reconfigure++;
			break;
		case 's':
			savefile = optarg;
			break;
		case 'S':
			syncup++;
			break;
		case 'f':
			fmtfile = optarg;
			break;
		case 'd':
			Verbose = atoi(optarg);
			break;
		case 'v':
		case 'V':
			(void) sprintf(s, gettext("Program Version [%s].\n"),
			    Version);
			(void) write(2, s, strlen(s));
			exit(0);
			break;
		case 'k':
		case 'K':
			if (!CLDEVICE_CMD)
				cl_auth_promote();
			kernel++;
			get_kernel_instances(kernel, (char)i);
			if (!CLDEVICE_CMD)
				cl_auth_demote();
			break;
		case 'C':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-C",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}
			cleanup++;
			break;
		case 'X':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-X",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}
			do_logging = 0;
			break;
		case 'G':
			/*
			 * Set or show the global default fencing configuration.
			 *
			 * scdidadm -G [pathcount|prefer3]
			 *
			 */
			if ((ac - optind) <= 1) {
				if (ac == optind) {
					/* -G only, show status */
					if (!CLDEVICE_CMD) {
						cl_auth_check_command_opt_exit(
						    av[0], "-G",
						    CL_AUTH_DEVICE_READ, 1);
					}
					displayglobalfencingstatus++;
					break;
				} else {
					if (!CLDEVICE_CMD) {
						cl_auth_check_command_opt_exit(
						    av[0], "-G",
						    CL_AUTH_DEVICE_MODIFY, 1);
					}
					if (strcmp(av[optind], "prefer3")
					    == 0) {
						newglobalfencingstatus =
						    GLOBAL_FENCING_PREFER3;
						break;
					} else if (strcmp(av[optind],
					    "pathcount") == 0) {
						newglobalfencingstatus =
						    GLOBAL_FENCING_PATHCOUNT;
						break;
					} else if (strcmp(av[optind],
					    "nofencing") == 0) {
						newglobalfencingstatus =
						    GLOBAL_NO_FENCING;
						break;
					} else if (strcmp(av[optind],
					    "nofencing-noscrub") == 0) {
						newglobalfencingstatus =
						    GLOBAL_NO_FENCING;
						setdiskdefaultscrub = 0;
						break;
					}
				}
			}

			/* Syntax error */
			clerror("Invalid argument: -G "
			    "[pathcount|prefer3|nofencing|"
			    "nofencing-noscrub].\n", av[0]);
			exit(1);
		case 'F':
			/*
			 * Change the default fencing for the specified
			 * instance(s).
			 *
			 * scdidadm -F <scsi3|useglobal|pathcount> instance
			 */
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-F",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}
			optind--;
			if (strcmp(av[optind], "pathcount") == 0) {
				setdiskdefaultfencing = FENCING_USE_PATHCOUNT;
			} else if (strcmp(av[optind], "scsi3") == 0) {
				setdiskdefaultfencing = FENCING_USE_SCSI3;
			} else if (strcmp(av[optind], "useglobal") == 0) {
				setdiskdefaultfencing = FENCING_USE_GLOBAL;
			} else if (strcmp(av[optind], "nofencing") == 0) {
				setdiskdefaultfencing = NO_FENCING;
			} else if (strcmp(av[optind],
			    "nofencing-noscrub") == 0) {
				setdiskdefaultfencing = NO_FENCING;
				setdiskdefaultscrub = 0;
			} else {
				clerror("Invalid argument: -F "
				    "{scsi3|useglobal|pathcount|nofencing|"
				    "nofencing-noscrub} disk1 [disk2...].\n",
				    av[0]);
				exit(1);
			}
			if ((ac - optind) <= 1) {
				clerror("'-F' <scsi3|useglobal|pathcount> "
				    "needs at least one disk.\n", av[0]);
				exit(1);
			}
			optind++;
			break;
		case 'T':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-C",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}
			remote_node = optarg;
			auto_remap = 1;
			break;
		case 't':
			/*
			 * -t is visible to users, so -Y is not necessary here.
			 * we will simply set combine_options to 1 to allow it.
			 */
			combine_options = 1;
		case 'w':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-C",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}
			src_device = strtok(optarg, ":");
			dest_device = strtok(NULL, ":");
			if ((src_device == NULL) || (dest_device == NULL)) {
				(void) fprintf(stderr,
				    gettext("%s: -%c needs a source and a "
				    "destination instance\n"), av[0], i);
				usage(av[0]);
			}
			if (atoi(dest_device) > DID_MAX_INSTANCE) {
				(void) fprintf(stderr,
				    gettext("%s: Destination instance (%s) is "
				    "greater than the Maximum number of DID "
				    "instances (%u) allowed\n"), av[0],
				    dest_device, DID_MAX_INSTANCE);
				exit(1);
			}

			manual_remap = 1;
			break;
		case 'W':
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-C",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}
			unreplicate =  atoi(optarg);
			auto_sync_remap = 1;
			break;
		case 'Y':
			/*
			 * This option is used together with -w or -W when
			 * a remote node invokes AUTO_SYNCREPL or
			 * MANUAL_SYNCREPL. Neither -w or -W is documented
			 * and should not be invoked by end users. Combining -Y
			 * with -w or -W can prevent users accidentally
			 * invokes -w or -W.
			 */
			combine_options = 1;
			break;
		case 'b' :
			if (!CLDEVICE_CMD) {
				cl_auth_check_command_opt_exit(av[0], "-C",
				    CL_AUTH_DEVICE_MODIFY, 1);
			}

			/* make sure the argument is a DID instance number */
			if (isdigit(optarg[0]))
				src_dev = atoi(optarg);
			else
				src_dev = -1;
			manual_unreplicate = 1;
			break;

		case 'z':
			if (CLDEVICE_CMD)
				exit_message++;
			break;
		case 'e':
			repl_type = optarg;
			(void) sprintf(get_repl_devs_cmd,
			    "/usr/cluster/lib/sc/repl/get_%s_dev_groups",
			    repl_type);
			break;
		case 'g':
			repl_dev_group = optarg;
			break;
		case '?':
		default:
			usage(av[0]);
			break;
		}
	}

	/* Promote to euid=0 */
	cl_auth_promote();

	/* Check if -w or -W is set without -Y options or vice versa. */
	if (((auto_sync_remap || manual_remap) && !combine_options) ||
	    (!auto_sync_remap && !manual_remap && combine_options)) {
		clerror("Invalid options.\n");
		usage(av[0]);
		if (CLDEVICE_CMD) {
			exit_code = CL_EINVAL;
		} else {
			exit_code = 1;
		}
		exit(exit_code);
	}

	/* For the following options, make sure we are a cluster member */
	if (!CLDEVICE_CMD && (reconfigure || cleanup || repair ||
	    newglobalfencingstatus || displayglobalfencingstatus ||
	    auto_remap || manual_remap || manual_unreplicate ||
	    setdiskdefaultfencing)) {
		int bootflags;

		if ((cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
		    !(bootflags & CLUSTER_BOOTED)) {
			if (do_logging) {
				/*
				 * SCMSGS
				 * @explanation
				 * Must be in cluster mode to execute this
				 * command.
				 * @user_action
				 * Reboot into cluster mode and retry the
				 * command.
				 */
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "%s: Not in cluster mode. Exiting...",
				    av[0]);
			}
			clerror("Not in cluster mode. Exiting...\n");
			exit(1);
		}
	}
	/*
	 * Make sure reconfigure and cleanup are not being done at the
	 * same time.
	 */
	if (reconfigure && cleanup) {
		clerror("\"-r\" and \"-C\" can't be used at the same time.\n");
		if (CLDEVICE_CMD) {
			exit(CL_EINVAL);
		} else {
			exit(1);
		}
	}

	/*
	 * Make sure manual_remap and auto-remap are not being done at the
	 * same time.
	 */
	if (manual_remap && auto_remap) {
		clerror("\"-T\" and \"-t\" can't be used at the same time.\n");
		if (CLDEVICE_CMD) {
			exit(CL_EINVAL);
		} else {
			exit(1);
		}
	}

	/*
	 * truecopy is the default setting, so a replication type may not
	 * have been set, besides, we know truecopy is a valid type.
	 */
	if (strcmp(repl_type, "truecopy") != 0) {
		if (validate_repl_type(get_repl_devs_cmd) != 0) {
			clerror("Invalid replication type specified: %s.\n",
			    repl_type);
			if (CLDEVICE_CMD) {
				exit(CL_EINVAL);
			} else {
				exit(1);
			}
		}
	}

	/* Make sure that the user specified an option. */
	if (option_set == 0)
		/* Print a usage message and exit if they didn't. */
		usage(av[0]);

	/* Generate event */
	exit_code = 0;
	cl_cmd_event_init(ac, av, &exit_code);

	if (!CLDEVICE_CMD && (cleanup || convert || repair_all ||
	    reconfigure || update || init)) {
		(void) cl_cmd_event_gen_start_event();
		(void) atexit(cl_cmd_event_gen_end_event);
	}

	/*
	 * Conversion option to convert from /etc/did.conf
	 * to using a CCR table.
	 */
	if (convert) {
		if (convert_conf_to_ccr(fmtfile, savefile)) {
			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_EINTERNAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		} else {
			if (CLDEVICE_CMD)
				exit(exit_code);
			else {
				exit_code = 0;
				exit(0);
			}
		}
	}

	/* Init libdid. */
	libinit = did_initlibrary(1, DID_NONE, fmtfile);
	if (libinit == DID_INIT_ORBFAIL) {
		/*
		 * If we couldn't initialize the ORB, try to open
		 * libdid in file access mode. This can only happen
		 * for as long as our file access mode is read only.
		 * If this changes to allow writes to the CCR data,
		 * this automatic fallback must be reconsidered.
		 */
		if (did_initlibrary(1, DID_CCR_FILE, fmtfile)
		    != DID_INIT_SUCCESS) {
			clerror("Could not initialize libdid.\n");

			if (did_geterrorstr()) {
				clerror("%s", did_geterrorstr());
			}
			if (CLDEVICE_CMD) {
				exit_code = CL_EINTERNAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
	} else if (libinit != DID_INIT_SUCCESS) {
		clerror("Could not initialize libdid.\n");

		if (did_geterrorstr()) {
			clerror("%s", did_geterrorstr());
		}
		if (CLDEVICE_CMD) {
			exit_code = CL_EINTERNAL;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	/* Load the list of device types from the did_types CCR file */
	if (did_gettypelist(&typelist)) {
		clerror("Could not load device type list.\n");

		if (did_geterrorstr()) {
			clerror("%s", did_geterrorstr());
		}
		if (CLDEVICE_CMD) {
			exit_code = CL_EINTERNAL;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	/* Obtain information about quorum devices */
	if (setdiskdefaultfencing || newglobalfencingstatus || manual_remap ||
	    auto_sync_remap || cleanup || repair || repair_all) {
		if (cluster_get_quorum_status(&quor_table) != 0) {
			clerror("Could not get quorum device information.\n");

			if (CLDEVICE_CMD) {
				exit_code = CL_EINTERNAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
	}

	if (newglobalfencingstatus || displayglobalfencingstatus ||
	    setdiskdefaultfencing) {
		if (get_globalfencing_status() != 0) {
			clerror("Could not get global fencing status.\n");
			if (did_geterrorstr()) {
				clerror("%s", did_geterrorstr());
			}

			if (CLDEVICE_CMD) {
				exit_code = CL_EINTERNAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
	}

	/*
	 * Since repair procedure only applied to one instance at a time,
	 * we don't need to read in whole instance list, which may cause
	 * problem when several repairing process is running at
	 * the same time.
	 */
	if (!repair) {

		/*
		 * did_getinstlist() might fail if a concurrent write
		 * operation on the ccr table is going on. We do sleep
		 * and retry for a few times.
		 */
		unsigned int sleep_period = 1;
		unsigned int failures = 0;
		do {
			if (did_getinstlist(&instlist, 1, typelist, fmtfile)) {
				failures++;

				(void) did_freeinstlist(instlist);
				if (failures < 4) {
					(void) sleep(sleep_period);
					sleep_period *= 2;
				}
			} else {
				break;
			}
		} while (failures < 4);

		if (failures == 4) {
			/*
			 * Check if the link to /etc/init.d/did_update_vfstab
			 * exists in the rc directory. The link is created by
			 * the postinstall script of SUNWscr and removed
			 * by /etc/init.d/did_update_vfstab after the node
			 * has run once in clustered mode.
			 */
			if (access(DID_UPDATE_VFSTAB_LINK, F_OK) != 0) {
				/*
				 * The link to /etc/init.d/did_update_vfstab
				 * does not exist in the rc directory. This
				 * node must have joined cluster before. Warn
				 * about missing DID instance list.
				 */
				clerror("Could not load DID instance list.\n");

				/*
				 * print out the error string from
				 * did_getinstlist()
				 */
				if (did_geterrorstr()) {
					clerror("%s", did_geterrorstr());
				}
			}

			if (CLDEVICE_CMD) {
				exit_code = CL_EINTERNAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
	}

	if (did_getrepllist(&repllist) < 0) {
		clerror("Could not load DID replication list.\n");
		if (CLDEVICE_CMD) {
			exit_code = CL_EINTERNAL;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	if (cleanup)
		device_cleanup();

	if (reconfigure) {
		/* Read in the scsi3 only list. */
		if (get_scsi3dev_list()) {
			if (CLDEVICE_CMD) {
				exit_code = CL_EINTERNAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
		discover_paths();
	}

	if (update)
		load_instances(call_reloader);

	if (check)
		check_consistency();

	if (repair) {
		printf("optind is %d: ac is %d\n", optind, ac);
		for (i = optind; i < ac; i++) {
			(void) repair_instances(av[i], fmtfile);
			optind++;
		}
	}

	if (reload) {
		exit_code = reload_instances(fmtfile);
	}

	if (displayglobalfencingstatus) {
		list_globalfencing_status();
	}

	if (setdiskdefaultfencing) {
		if (set_disk_default_fencing(&av[optind], ac - optind,
		    setdiskdefaultfencing, setdiskdefaultscrub) != 0) {
			if (CLDEVICE_CMD)
				exit(exit_code);
			else {
				exit_code = 1;
				exit(1);
			}
		}

	}

	if (repair_all) {
		if (repair_all_instances(&err) == 0) {
			/*
			 * Nothing more to do.
			 */
			if (err != 0) {
				if (CLDEVICE_CMD)
					exit(exit_code);
				else {
					exit_code = 1;
					exit(1);
				}
			} else {
				exit_code = 0;
				exit(0);
			}
		}
	}

	/*
	 * Move DID instance src_dev to dest_dev, combining them if dest_dev
	 * exists and is a replica of src_dev.
	 */
	if (manual_remap) {
		/* convert char *instance to instance number */
		src_dev = get_instance_number(src_device);
		dest_dev = get_instance_number(dest_device);

		if ((src_dev <= 0) || (dest_dev <= 0)) {
			clerror("Invalid remap option argument.\n");
			usage(av[0]);

			if (CLDEVICE_CMD) {
				exit_code = CL_EINVAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
		did_remap(src_dev, dest_dev, repl_dev_group);
	}

	/*
	 * Determine which devices are replicas of each other and combine their
	 * DID instances.
	 */
	if (auto_remap)
		did_auto_remap(remote_node, fmtfile);

	/* this is a call from an auto_remap on another node */
	if (auto_sync_remap)
		/*lint -e644 */
		did_auto_sync_remap(unreplicate);
		/*lint +e644 */

	/* undocumented interface to backout replication changes */
	if (manual_unreplicate)
		did_manual_unreplicate(src_dev);

	if (list || longlist) {
		if (header)
			list_header();
		if ((ac - optind) == 0) {
			list_instances((char *)NULL, longlist, kernel);
		}
		for (i = optind; i < ac; i++) {
			list_instances(av[i], longlist, kernel);
			optind++;
		}
	}

	if (lockunlock)
		lock_driver(lockdriver);

	if ((savefile || reconfigure || cleanup || repair_all) && !notsave) {
		/*
		 * We only permit did_saveinstlist() to kick out "junk"
		 * DID instance entry for repair and cleanup cases.
		 */
		if (reconfigure || repair_all) {
			flag = NON_REMOVABLE;
			if (repair_all)
				flag |= FORCE_UPDATE;
		}
		if (did_saveinstlist(instlist, savefile ? savefile : fmtfile,
		    node_prefix, flag)) {
			/*lint -e746 */
			if (errno == ENOTEMPTY) {
				clwarning("Not saving DID instance list to "
				    "file.\n");
			} else {
				clerror("Could not save DID instance list to "
				    "file.\n");
				if (did_geterrorstr()) {
					clerror("%s", did_geterrorstr());
				}
				if (CLDEVICE_CMD) {
					if (!exit_code)
						exit_code = CL_EIO;
					exit(exit_code);
				} else {
					exit_code = 1;
					exit(1);
				}
			}
		}
		if (reconfigure)
			(void) scdpm_update_db(SCDPM_UPDATE_REGISTER, instlist);

		if (cleanup)
			(void) scdpm_update_db(SCDPM_UPDATE_CLEANUP, instlist);
	}

	if (init)
		init_instances();

	if (Verbose)
		showRecordDescription();

	if (syncup)
		if (did_syncgdevs()) {
			if (!CLDEVICE_CMD) {
				clerror("Could not execute \"scgdevs\" on all "
				    "cluster nodes.\n");
			} else {
				clerror("Could not execute \"cldevice "
				    "populate\" on all cluster nodes.\n");
			}
			if (did_geterrorstr()) {
				clerror("%s", did_geterrorstr());
			}
			if (CLDEVICE_CMD) {
				exit_code = CL_EINTERNAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}

	if (newglobalfencingstatus) {
		if (switch_globalfencing(newglobalfencingstatus,
		    setdiskdefaultscrub)) {
			if (did_geterrorstr()) {
				clerror("%s", did_geterrorstr());
			}
		}
	}

	if (need_cleanup) {
		if (do_logging) {
			if (!CLDEVICE_CMD)
				/*
				 * SCMSGS
				 * @explanation
				 * Invalid disk path found during DID
				 * reconfiguration.
				 * @user_action
				 * Remove the invalid disk path and run
				 * 'scdidadm -C' and rerun 'scdidadm -r'.
				 */
				(void) sc_syslog_msg_log(handle, LOG_NOTICE,
				    MESSAGE, "DID reconfiguration "
				    "discovered an invalid disk path.  You "
				    "must remove this path before you can add "
				    "a new path.  Run 'scdidadm -C' then "
				    "rerun 'scdidadm -r'.\n");
			else {
				/*
				 * SCMSGS
				 * @explanation
				 * Invalid disk path found during DID
				 * reconfiguration.
				 * @user_action
				 * Remove the invalid disk path and run
				 * 'cldevice clear' and rerun
				 * 'cldevice refresh'.
				 */
				(void) sc_syslog_msg_log(handle, LOG_NOTICE,
				    MESSAGE, "DID reconfiguration "
				    "discovered an invalid disk path.  You "
				    "must remove this path before you can add "
				    "a new path.  Run 'cldevice clear' then "
				    "rerun 'cldevice refresh'.\n");
			}
		}

		clmessage("DID reconfiguration discovered an invalid "
		    "disk path.\n");
		clmessage("You must remove invalid disk paths before you can "
		    "add new paths.");

		if (CLDEVICE_CMD) {
			clmessage("Run 'cldevice clear' to remove the "
			    "invalid disk paths.\n");
		} else {
			clmessage("Run 'scdidadm -C' to remove the "
			    "invalid disk paths.\n");
		}
	}

	sc_syslog_msg_done(&handle);

	if (cleanup || repair || repair_all)
		/*
		 * Make sure all disks have correct scsi state.
		 * We are worried about new disks that have been added during
		 * repair and cleanup operations that have crossed the
		 * scsi-2 / scsi-3 connectivity boundary.
		 */
		verify_scsi_state(0);

	if (quor_table != NULL) {
		cluster_release_quorum_status(quor_table);
		quor_table = NULL;
	}

	if (exit_message)
		(void) printf("%s %d\n", EXIT_MESSAGE, exit_code);

	return (exit_code);
}

static int
convert_conf_to_ccr(char *fmtfile, char *savefile)
{
	int	err;
	did_subpath_t		*subptr;
	did_device_list_t	*dlistp;
	uint_t			node_prefix_len = strlen(node_prefix);


	if (did_initlibrary(1, DID_CONF_FILE, fmtfile)) {
		if (did_geterrorstr()) {
			print_error("convert_conf_to_ccr", gettext("Could not "
			    "initialize libdid.\n%s"), did_geterrorstr());
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		} else {
			print_error("convert_conf_to_ccr", gettext("Could not "
			    "initialize libdid.\n"));
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		}
	}

	if (did_gettypelist(&typelist)) {
		if (did_geterrorstr()) {
			print_error("convert_conf_to_ccr", gettext("Could not "
			    "load device type list.\n%s"), did_geterrorstr());
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		} else {
			print_error("convert_conf_to_ccr", gettext("Could not "
			    "load device type list.\n"));
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		}
	}

	if (did_getinstlist(&instlist, 1, typelist, fmtfile)) {
		if (did_geterrorstr()) {
			print_error("convert_conf_to_ccr", gettext("Could "
			    "not load DID instance list.\n%s"),
			    did_geterrorstr());
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		} else {
			print_error("convert_conf_to_ccr", gettext("Could "
			    "not load DID instance list.\n"));
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		}
	}

	if (instlist == NULL) {
		/* Check that we got instances back. If not, exit cleanly */
		return (0);
	}

	/*
	 * Confirm that all devids on this node match. They may not,
	 * due to changes to the sd/ssd drivers in Solaris 8.
	 */
	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {

		ddi_devid_t		devid;

		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next)
			if (strncmp(subptr->device_path, node_prefix,
			    node_prefix_len))
				continue;
			else {
				if (get_subpathdevid(subptr, &devid) == 0) {
					if (did_same_dev(devid,
					    dlistp->target_device_id,
					    NULL) != 0) {
						update_devid(dlistp, devid);
					}
				}

				/*
				 * Error path if we are unable to get the
				 * current device ID is to just fall
				 * through, and use the old one for
				 * upgrade.
				 */
			}
	}

	if (did_initlibrary(1, DID_CCR_FILE, fmtfile)) {
		if (did_geterrorstr()) {
			print_error("convert_conf_to_ccr",
			    gettext("Could not initialize libdid\n%s"),
			    did_geterrorstr());
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		} else {
			print_error("convert_conf_to_ccr",
			    gettext("Could not initialize libdid\n"));
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		}
	}

	if ((err = did_saveinstlist(instlist, savefile ? savefile : fmtfile,
	    node_prefix, REMOVABLE)) != 0) {
		if (err == -2) {
			clerror("\"%s\" exists - DID configuration migration "
			    "will be skipped.\n", "did_instances");

			if (CLDEVICE_CMD) {
				exit_code = CL_EEXIST;
				exit(exit_code);
			} else {
				exit_code = 2;
				exit(2);
			}
		} else {
			if (did_geterrorstr()) {
				print_error("convert_conf_to_ccr",
				    gettext("Could not save did instance list "
				    "to file: %s."), did_geterrorstr());
				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EINTERNAL;
			} else {
				print_error("convert_conf_to_ccr",
				    gettext("Could not save did instance list "
				    "to file.\n"));
				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EINTERNAL;
			}
		}
	}

	return (0);
}

static void
update_devid(did_device_list_t *dlistp, ddi_devid_t devid)
{
	impl_devid_t	*idevid = (impl_devid_t *)devid;
	size_t		len = DEVID_GETLEN(idevid);
	char		*newid = NULL;

	/* Make sure we have enough space. */
	if (len > dlistp->diskid_len) {
		newid = (char *)malloc(len);
		if (newid == NULL) {
			/*
			 * Could not allocate memory to change ID. Print
			 * an error, and return without modifying anything.
			 */
			clerror("Device ID for instance \"%d\" has changed, "
			    "but the configuration file cannot be updated.\n",
			    dlistp->instance);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_ENOMEM;
			return;
		}
		free(dlistp->diskid);
		dlistp->diskid = newid;
	}

	dlistp->diskid_len = (unsigned short) len;
	dlistp->type = DEVID_GETTYPE(idevid);

	bzero(dlistp->diskid, dlistp->diskid_len);
	(void) memcpy(dlistp->diskid, ((impl_devid_t *)devid)->did_id,
	    dlistp->diskid_len);

	dlistp->target_device_id = devid;
}

/*
 * File tree walk helper functions
 */

/*ARGSUSED*/
static int
check_path_disk(
	const char		*path,
	const struct stat	*statp,
	int			type)
{
	/*
	 * Ignore non-files (e.g., directories)
	 */
	if (type != FTW_F)
		return (0);

	if (strcmp(&path[strlen(path) - 2], "s2") == 0)
			add_disk_path(path);

	return (0);
}

/*ARGSUSED*/
static int
check_path_tape(
	const char		*path,
	const struct stat	*statp,
	int			type)
{
	/*
	 * Ignore non-files (e.g., directories)
	 */
	if (type != FTW_F)
		return (0);

	if (strcmp(&path[strlen(path) - 1], "l") == 0) {
		add_tape_path(path);
	}

	return (0);
}

static void
add_disk_path(const char *cpath)
{
	ddi_devid_t		devid = NULL;
	int			fd;
	struct dk_cinfo		dki_info;
	struct stat		st;
	did_device_type_t	*devtype;

	/*
	 * Avert a possible catastrophy. /dev/rdsk/rdsk gets
	 * created somehow as a link back to /dev/rdsk, and DID
	 * shouldn't create subpaths for everything under the link.
	 * This can happen if someone made a mistake when 'disabling'
	 * Veritas DMP by linking /dev/vx/rdmp to /dev/rdsk.
	 */
	if (strstr(cpath, "/dev/rdsk/rdsk")) {
		if (do_logging) {
			/*
			 * SCMSGS
			 * @explanation
			 * scdidadm has discovered a suspect logical path
			 * under /dev/rdsk. It will not add it to subpaths for
			 * a given instance.
			 * @user_action
			 * Check to see that the symbolic links under
			 * /dev/rdsk are correct.
			 */
			(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
			    "did discovered faulty path, ignoring: %s", cpath);
		}
		clerror("DID discrovered faulty path \"%s\", ignoring.\n",
		    cpath);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ESTATE;
		return;
	}

	for (devtype = typelist; devtype != NULL; devtype = typelist->next) {
		if (strcmp(devtype->type_name, "disk") == 0)
			break;
	}

	if (devtype == NULL) {
		clerror("Unable to find device type \"%s\".\n", "disk");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ETYPE;
		return;
	}

	if ((fd = open(cpath, O_RDONLY|O_NDELAY)) < 0) {
		/*
		 * CDROM under volmgt control.
		 */
		if (errno == EBUSY) {
			return;
		}
		clerror("Unable to open \"%s\" - %s.\n", cpath,
		    strerror(errno));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		return;
	}

	if (stat(cpath, &st) != 0) {
		clerror("Could not stat \"%s\" - %s.\n", cpath,
		    strerror(errno));
		clwarning("Path not loaded - \"%s\".\n", cpath);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		return;
	}

	if (devid_get(fd, &devid) != 0) {

		if (ioctl(fd, DKIOCINFO, (caddr_t)&dki_info) == -1) {
			clerror("ioctl(%s) error - %s.\n",
			    "DKIOCINFO", strerror(errno));
			return;
		}

		if (dki_info.dki_ctype == DKC_CDROM)
			add_dev_with_no_devid(cpath, devtype);

	} else {
		add_dev_with_devid(devid, cpath, devtype);
	}

	(void) close(fd);
}

int
get_iscsi_flag(const char *cpath) {
	char *devices_path = NULL;
	char *dev_path = NULL, *lasts = NULL;
	int biSCSI = 0;
	int path_len, flags = 0;

	devices_path = malloc(MAXPATHLEN);
	if (devices_path == NULL) {
		did_logmsg(LOG_ERR, DIDADM, "Not enough memory \n");
		return (flags);
	}
	if ((path_len = resolvepath(cpath, devices_path, MAXPATHLEN)) > 0) {
	    devices_path[path_len] = '\0';
	/*
	 * Remove the last piece of the /devices path that contains
	 * access details
	 */
	    dev_path = strrchr(devices_path, ':');
	    *dev_path = '\0';

	/* Remove the prefix "/devices/" from the path */
	    dev_path = strtok_r(devices_path, "/", &lasts);
	    snprintf(devices_path, MAXPATHLEN, "/%s", lasts);

	/* Now the path that can be searched in the array */
	    if ((iscsi_paths != NULL) &&
		    bsearch(&devices_path, iscsi_paths, iscsi_path_count,
			sizeof (char*), string_compare) != NULL) {
		biSCSI = 1;
	    }
	}
	flags = biSCSI ? ISCSI_T_PROTOCOL : DEFAULT_T_PROTOCOL;
	free(devices_path);
	return (flags);
}

void
add_dev_with_devid(ddi_devid_t devid, const char *cpath,
    did_device_type_t *devtype)
{

	impl_devid_t		*idevid;
	did_device_list_t	*instptr, *ip;
	did_subpath_t		*subptr = NULL, *tmptr;
	char			*log_name;
	int			alloced = 0;
	int			numsubpaths = 0;
	uint_t			node_prefix_len = strlen(node_prefix);

	idevid = (impl_devid_t *)devid;
	if ((instptr = get_instance_from_devid(devid, cpath)) == 0) {

		/*
		 * If we could not find an instance for this devid,
		 * we now should check if this subpath already is
		 * in an existing instance. It may be that the device
		 * in question has been accidentally recabled or otherwise
		 * modified, eg. firmware upgrade, and we need to guard
		 * against allocating a new instance in this circumstance.
		 */
		if ((subptr = find_instance_ptr(no_minor_path(cpath, devtype)))
		    != NULL) {
			/*
			 * If we found an instance for this subpath
			 * we can just return.
			 */
			clerror("Device ID \"%s\" does not match physical "
			    "device ID for \"d%d\".\n", subptr->device_path,
			    subptr->instptr->instance);
			clwarning("Device \"%s\" might have been replaced.\n",
			    subptr->device_path);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EEXIST;
			return;
		}
		/*
		 * No matching path found on this node so we need to
		 * malloc and populate the instptr.
		 */
		instptr = (did_device_list_t *)malloc(sizeof (*instptr));
		if (instptr == NULL) {
			print_error("add_dev_with_devid", gettext(
			    "Cannot malloc memory.\n"));
			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_ENOMEM;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
		(void) memset(instptr, 0, sizeof (*instptr));

		instptr->diskid_len = DEVID_GETLEN(idevid);
		/*
		 * - DEVID_HINT_SIZE;
		 */
		instptr->type = DEVID_GETTYPE(idevid);

		/* Assign diskid */
		instptr->diskid = (char *)malloc(instptr->diskid_len);
		if (instptr->diskid == NULL) {
			print_error("add_dev_with_devid", gettext(
			    "Cannot malloc memory.\n"));
			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_ENOMEM;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
		(void) memcpy(instptr->diskid,
		    ((impl_devid_t *)devid)->did_id,
		    instptr->diskid_len);

		instptr->target_device_id = devid;
		instptr->device_type = devtype;

		/*
		 * Intialize the default_fencing per disk to use the global
		 * value.
		 */
		instptr->default_fencing = FENCING_USE_GLOBAL;
		instptr->detected_fencing = scsi_fencing_inquiry((char *)cpath);

	}
	/*
	 * Here we found an instance which maps to this device id.
	 */
	if ((subptr = find_instance_ptr(no_minor_path(cpath, devtype)))
	    != NULL) {
		if (subptr->instptr != instptr) {
			/*
			 * But this subpath belongs to another instance!!
			 */
			clerror("Path \"%s\" and device ID are inconsistent "
			    " for instances \"d%d\" and \"d%d\".\n",
			    subptr->device_path,
			    instptr->instance,
			    subptr->instptr->instance);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_ESTATE;
			return;

		} else if ((subptr->instptr == instptr) &&
		    (instptr->instance != 0)) {
			/*
			 * If we found an instance for this subpath, and
			 * there's already an instance number allocated,
			 * we can just return.
			 *
			 * We need to update the default fencing if it is
			 * currently being set to N/A. It is possible during
			 * upgrade.
			 */
			if (instptr->detected_fencing == FENCING_NA) {
				instptr->detected_fencing =
				    scsi_fencing_inquiry((char *)cpath);
			}

			/* Set iSCSI flag on Upgrade */
			instptr->flags |= get_iscsi_flag(cpath);

			return;
		}
	} else {
		/*
		 * Populate the subpath if one does not exist for this
		 * instance.
		 */
		alloced = 1;
		subptr = (did_subpath_t *)malloc(sizeof (*subptr));
		if (subptr == NULL) {
			print_error("add_dev_with_devid", gettext(
			    "Cannot malloc memory\n"));
			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_ENOMEM;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
		(void) memset(subptr, 0, sizeof (*subptr));
		(void) sprintf(subptr->device_path,
		    "%s%s", node_prefix, cpath);
		(void) strcpy(subptr->device_path,
		    no_minor_path(subptr->device_path, devtype));

	}

	/*
	 * While we are discovering a new diskpath on local node,
	 * we should make sure it isn't new due to recabling of
	 * existing disk. We can do the check by looking if there is
	 * any other valid device path (not necessarily with same devid)
	 * existing on local node in same DID instance. If there is,
	 * above code which removing moved diskpath will ultimately
	 * take care of it, so we can add current diskpath into
	 * instance safely. Otherwise, it means the old diskpath
	 * in the instance is obsoleted. We should log an error and
	 * return early.
	 */
	tmptr = instptr->subpath_listp;
	while (tmptr) {
		if (strncmp(tmptr->device_path, node_prefix,
		    node_prefix_len) == 0) {
			if (confirm_device(tmptr->device_path,
			    instptr->device_type) == 0) {
				/*
				 * Device path not valid.
				 */
				clerror("DID subpath \"%s\" is invalid.\n",
				    tmptr->device_path);

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_ESTATE;

				need_cleanup = 1;
				if (alloced)
					free(subptr);
				return;
			}

		}
		tmptr = tmptr->next;
	}

	/*
	 * Before insert subpath into instance's list, we should make
	 * sure the path isn't there yet.
	 */
	for (tmptr = instptr->subpath_listp; tmptr != NULL;
	    tmptr = tmptr->next) {
		numsubpaths++;
		if (strcmp(tmptr->device_path, subptr->device_path) == 0)
			/* Path is already there */
			return;
	}

	/*
	 * The number of subpath is more than 2 and the detected fencing
	 * is scsi2. We should print a warning and allow addition.
	 */
	if (instptr->detected_fencing == SCSI2_FENCING &&
	    numsubpaths > 2) {
		clwarning("DID instance \"%d\" has been detected to support "
		    "SCSI2 Reserve/Release protocol only. Adding path \"%s\" "
		    "creates more than 2 paths to this device and can lead to "
		    "unexpected node panics.\n", instptr->instance,
		    subptr->device_path);
	}

	/* Check if this path is a iSCSI device or not and set flags */
	instptr->flags |= get_iscsi_flag(cpath);

	/* Insert the subpath onto the instance's list of paths. */
	subptr->instptr = instptr;
	subptr->next = instptr->subpath_listp;
	instptr->subpath_listp = subptr;

	if (instptr->instance == 0) {
		/*
		 * Add to the ftwlist which will be passed to the
		 * the library for instance number allocation
		 * once the treewalk has concluded.
		 */
		if (ftwlist == NULL) {
			ftwlist = instptr;
		} else {
			ip = ftwlist;
			/*
			 * We need to assure that this instptr has not already
			 * been added to the ftwlist.
			 */
			while (ip->next != NULL && ip != instptr)
				ip = ip->next;
			if (ip != instptr) /* if not already added */
				ip->next = instptr;
		}
	} else {
		/*
		 * if we've already allocated an instance for this subpath
		 * then report the creation of same now. Otherwise
		 * We'll report it when we've batch allocated this instance
		 * along with the rest of the ftwlist.
		 */
		log_name = get_fmt_name(instptr, subptr);
		if (log_name && do_logging) {
			/*
			 * SCMSGS
			 * @explanation
			 * Informational message from scdidadm.
			 * @user_action
			 * No user action required.
			 */
			(void) sc_syslog_msg_log(handle, LOG_NOTICE, ADDED,
			    "did subpath %s created for instance %d.",
			    cpath, instptr->instance);
		}
		if (Verbose || !CLDEVICE_CMD) {
			clmessage("DID subpath \"%s\" created for instance "
			    "\"%d\".\n", cpath, instptr->instance);
		}
	}
}

void
add_dev_with_no_devid(const char *cpath, did_device_type_t *devtype)
{

	did_device_list_t	*instptr, *ip;
	did_subpath_t		*subptr = NULL;

	subptr = find_instance_ptr(no_minor_path(cpath, devtype));
	if (subptr != NULL)
		return;

	/*
	 * Since the devid for this device is not useful,
	 * as long as there is not a matching subpath
	 * for the device, we assume that it is new. This
	 * means that things like multi-ported tapes will
	 * show up in the namespace once for each connection
	 * they have to a cluster node.
	 */
	instptr = (did_device_list_t *)malloc(sizeof (*instptr));
	if (instptr == NULL) {
		print_error("add_dev_with_no_devid", gettext(
		    "Cannot malloc memory.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	(void) memset(instptr, 0, sizeof (*instptr));

	instptr->diskid_len = 0;
	instptr->type = DEVID_NONE;
	instptr->diskid = 0;

	instptr->default_fencing = FENCING_USE_GLOBAL;
	instptr->detected_fencing = FENCING_NA;

	instptr->target_device_id = (ddi_devid_t)NULL;
	instptr->device_type = devtype;
	instptr->flags |= DEFAULT_T_PROTOCOL;


	subptr = (did_subpath_t *)malloc(sizeof (*subptr));
	if (subptr == NULL) {
		print_error("add_dev_with_no_devid", gettext(
		    "Cannot malloc memory.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	(void) memset(subptr, 0, sizeof (*subptr));
	(void) sprintf(subptr->device_path, "%s%s", node_prefix, cpath);
	(void) strcpy(subptr->device_path,
	    no_minor_path(subptr->device_path, devtype));

	subptr->instptr = instptr;
	subptr->next = instptr->subpath_listp;
	instptr->subpath_listp = subptr;

	/*
	 * We don't have an instance allocated yet,
	 * ask the library to give us a new one. The
	 * subpath structure, and its associated instance
	 * data structure need to be fully populated at
	 * the time of this call.
	 * Add to the ftwlist which will be passed to the
	 * the library for instance number allocation
	 * once the treewalk has concluded.
	 */
	if (ftwlist == NULL) {
		ftwlist = instptr;
	} else {
		ip = ftwlist;
		while (ip->next != NULL)
			ip = ip->next;
		ip->next = instptr;
	}
}

static void
add_tape_path(const char *cpath)
{

	did_device_type_t	*devtype;


	for (devtype = typelist; devtype != NULL; devtype = typelist->next) {
		if (strcmp(devtype->type_name, "tape") == 0)
			break;
	}

	if (devtype == NULL) {
		clerror("Unable to find device type \"%s\".\n", "tape");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ETYPE;
		return;
	}

	add_dev_with_no_devid(cpath, devtype);

}

/*
 * Check to see if the device id passed in matches the Solaris 8 device id for
 * the specified device, as computed by the libdid device id emulation code.
 * The variable 'local' is used to identify devices which the emulation code
 * does not handle, such as ide and cd-rom drives.  A value of 1 indicates this.
 * Returns 0 if the device id matches, -1 if not.
 */
static int
did_match_solaris8_devid(const char *disk, ddi_devid_t ccr_devid, int *local)
{
	unsigned char	s8_devid[MAXNAMELEN];
	did_device_list_t	dev1;
	int		emulated_match;

	if (local != NULL)
		*local = 0;
	switch (get_solaris_devid((char *)disk, 8, s8_devid, &dev1)) {
	case 0 :
		/* got the device id, compare it to CCR */
		emulated_match = did_same_dev(dev1.target_device_id, ccr_devid,
		    NULL);
		break;
	case 1 :
		/* not a device we are emulating, must be local device */
		emulated_match = -1;
		if (local != NULL)
			*local = 1;
		break;
	case -1	 :
		/* fall through */
	case -2 :
		/* fall through */
	default :
		emulated_match = -1;
	}

	return (emulated_match);
}

static int
did_same_dev(ddi_devid_t devid, ddi_devid_t target_devid,
    did_device_list_t *instptr)
{
	int ret;

	if (devid && target_devid) {
		ret = devid_compare(devid, target_devid);
		if ((ret != 0) && (instptr != NULL))
			/* see if this is a replicated device */
			ret = check_repl_devid(instptr, devid);
		return (ret);
	} else
		return (-1);
}

static did_device_list_t *
get_instance_from_devid(ddi_devid_t devid, const char *cpath)
{
	did_device_list_t	*instptr = instlist;
	unsigned char		s8_devid[MAXNAMELEN];
	did_device_list_t	dev1;

	while (instptr) {
		if (did_same_dev(devid, instptr->target_device_id,
		    instptr) == 0)
			return (instptr);

		instptr = instptr->next;
	}
	instptr = ftwlist;
	while (instptr) {
		if (did_same_dev(devid, instptr->target_device_id,
		    instptr) == 0)
			return (instptr);

		instptr = instptr->next;
	}

	/*
	 * Check Solaris 8 devid as well in case we have just upgraded
	 * from Solaris 8 to Solaris 9, in which case the CCR
	 * will be storing Solaris 8 device ids, but the kernel
	 * is Solaris 9.
	 *
	 * So, we do the following :
	 * - Fetch the Solaris 8 devid for the current disk (ie. cpath)
	 *   and search for it on the instlist.
	 */

	instptr = instlist;

	/*
	 * Get the Solaris 8 devid for the current disk.
	 */
	if (get_solaris_devid((char *)cpath, 8, s8_devid, &dev1) != 0) {
		return (0);
	}

	while (instptr) {
		if (did_same_dev(dev1.target_device_id,
		    instptr->target_device_id, NULL) == 0)
			return (instptr);

		instptr = instptr->next;
	}

	return (0);
}

static char *
physicalToLogical(char *ppath)
{
	static char	device[MAXPATHLEN];
	char		lpath[MAXPATHLEN];
	char		path[MAXPATHLEN];
	struct dirent	*dentry;
	DIR		*dirf;
	/*lint -e785 */
	char		*searchpaths[MAXPATHLEN] = { "/dev/dsk", "/dev/rmt" };
	/*lint +e785 */
	int		count = 0;

	while (searchpaths[count]) {
		dirf = opendir(searchpaths[count]);
		while ((dentry = readdir(dirf)) != NULL) {
			(void) sprintf(path, "%s/%s", searchpaths[count],
			    dentry->d_name);
			if (readlink(path, lpath, MAXPATHLEN) <= 0)
				continue;
			if (strstr(lpath, ppath)) {
				(void) closedir(dirf);
				(void) memset(device, 0, sizeof (device));
				if (strcmp(searchpaths[count],
				    "/dev/dsk") == 0) {
					return (strncpy(device, path,
					    (strlen(path) - 2)));
				} else if (strcmp(searchpaths[count],
				    "/dev/rmt") == 0) {
					return (strncpy(device, path,
					    sizeof (device) - 1));
				} else {
					return (NULL);
				}
			}
		}
		(void) closedir(dirf);
		count++;
	}
	return (NULL);
}

#define	MAX_DEVIDLEN	1024

/*ARGSUSED*/
static void
get_kernel_instances(int instance, char mode)
{
	int	fd, inst, count;
	did_init_t	*didinitp = (did_init_t *)malloc(sizeof (*didinitp));
	char	*logpath;

	if (didinitp == NULL) {
		print_error("get_kernel_instances", gettext(
		    "Cannot malloc memory.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	didinitp->device_id = (ddi_devid_t)malloc(MAX_DEVIDLEN);
	if (didinitp->device_id == NULL) {
		print_error("get_kernel_instances", gettext(
		    "Cannot malloc memory.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	if ((fd = open("/dev/did/admin", O_RDONLY)) < 0) {
		perror("/dev/did/admin");
		exit_code = 1;
		exit(1);
	}
	if (ioctl(fd, IOCDID_INSTCNT, &inst) == 0 && inst > 0) {
		int		testinst;
		int		testpath;
		impl_devid_t	*idevid;

		idevid = (impl_devid_t *)didinitp->device_id;
		testinst = 1;
		while (inst) {
			int	i;

			(void) memset(didinitp->device_id, 0, MAX_DEVIDLEN);
			testpath = 1;
			count = testinst;
			if (ioctl(fd, IOCDID_PATHCNT, &count) != 0) {
				perror("IOCDID_PATHCNT");
				testinst++;
				inst--;
				continue;
			} else if (!count) {
				testinst++;
				continue;
			}
			inst--;
			didinitp->instance = testinst;

			if (ioctl(fd, IOCDID_INSTINFO, didinitp) != 0) {
				perror("IOCDID_INSTINFO");
				testinst++;
				continue;
			}

			if (idevid->did_id != NULL) {
				for (i = 0; i < 44; i++)
					/*lint -e662 -e661 */
					/*
					 * Possible creation of out-of-bounds
					 * pointer(43 beyond end of data) by
					 * operator '[
					 */
					if (!isprint((idevid->did_id)[i]))
						break;
					/*lint +e662 +e661 */
				if (mode != 'K') {
					(void) printf("#\n# %-*.*s\n#\n",
					    i, i, idevid->did_id);
				}
				(void) printf("instance\t%d\n", testinst);
				if (mode != 'K') {
					(void) printf("\ttype %s\n",
					    typetostring(
					    DEVID_GETTYPE(idevid)));
					(void) printf("\tdiskid %s\n",
					    diskidtostr(idevid->did_id,
					    DEVID_GETLEN(idevid)));
				}
			} else {
				if (mode != 'K') {
					(void) printf("#\n#\n#\n");
				}
				(void) printf("instance\t%d\n", testinst);
				if (mode != 'K') {
					(void) printf("\ttype %s\n",
					    typetostring(DEVID_NONE));
					(void) printf("\tdiskid %s\n", "0");
				}
			}
			while (count) {
				count--;
				didinitp->instance = testinst;
				didinitp->state = testpath;
				if (ioctl(fd, IOCDID_PATHINFO, didinitp) != 0) {
					perror("IOCDID_PATHINFO");
					testpath++;
					continue;
				}

				(void) printf("\t");
				if (mode != 'K') {
					(void) printf("%s", node_prefix);
				}
				if ((logpath =
				    physicalToLogical(didinitp->subpath))
				    != NULL) {
					(void) printf("%s", logpath);
				} else {
					print_error("get_kernel_instances",
					    gettext("\nCannot find logical "
					    "device path.\n"));

					if (CLDEVICE_CMD && !exit_code)
						exit_code = CL_EINTERNAL;
				}
				if (mode == 'K') {
					(void) printf("\tstate: %d\n",
					    didinitp->state);
				} else {
					(void) printf("\n");
				}
				testpath++;
			}
			testinst++;
		}
	}
	(void) close(fd);
}

static void
lock_driver(int lock)
{
	int	fd;

	if ((fd = open("/dev/did/admin", O_RDONLY)) < 0) {
		clerror("Cannot open %s - %s.\n", "/dev/did/admin",
		    strerror(errno));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_EIO;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	if (ioctl(fd, lock ? IOCDID_STICK : IOCDID_UNSTICK, 0) != 0) {
		clerror("ioctl(%s) error - %s.\n",
		    lock ? "IOCDID_STICK" : "IOCDID_UNSTICK",
		    strerror(errno));
	}
	(void) close(fd);
}

static void
init_instances()
{
	int	fd;
#ifdef DID_MULTI_INSTANCE
	int res = update_didconf();

	if (res != 0) {
		/*
		 * Something is wrong about did.conf handling,
		 * exit a special value. Note that scdidadm exit
		 * value is not checked in bootcluster script.
		 */
		exit_code = 97;
		exit(97); /* XXX - Is this important? */
	}
	/*
	 * Now execute IOCDID_INITDONE
	 */
#endif

	if ((fd = open("/dev/did/admin", O_RDONLY)) < 0) {
		clerror("Cannot open %s - %s.\n", "/dev/did/admin",
		    strerror(errno));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_EIO;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	if (ioctl(fd, IOCDID_INITDONE, 0) != 0) {
		clerror("ioctl(%s) error - %s.\n",
		    "IOCDID_INITDONE", strerror(errno));
	}
	(void) close(fd);
}

static void
verify_scsi_state(int scrub_only)
{
	char *run_reserve =
	    "/usr/cluster/lib/sc/run_reserve";
	char *run_reserve_cmd =
	    "/usr/cluster/lib/sc/run_reserve -c node_join -x";
	char *scrub_only_cmd =
	    "/usr/cluster/lib/sc/run_reserve -c node_join -x -X";
	struct stat stat_buf;
	int bootflags;
	nodeid_t nodelist[NODEID_MAX];
	int i;

	/* make sure run_reserve script exists */
	if (stat(run_reserve, &stat_buf) != 0)
		return;

	/* make sure we're booted in cluster mode */
	if (cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0)
		return;
	if ((bootflags & (CLUSTER_CONFIGURED | CLUSTER_BOOTED)) !=
	    (CLUSTER_CONFIGURED | CLUSTER_BOOTED))
		return;

	/* set this up to run on all nodes */
	for (i = 0; i < NODEID_MAX; i++)
		nodelist[i] = 1;

	if (scrub_only == 1)
		(void) run_reserve_on_connected_nodes(B_FALSE, scrub_only_cmd,
		    nodelist);
	else
		(void) run_reserve_on_connected_nodes(B_FALSE, run_reserve_cmd,
		    nodelist);
}

/* Set the default fencing protocol for the specified list of disks. */
static int
set_disk_default_fencing(char *disks[], int numdisks, unsigned short protocol,
    unsigned short scrub)
{
	did_device_list_t	*dlistp = NULL;
	char			cmd[MAXNAMELEN];
	char			*full_did_path;
	char			*resvcmd;
	nodeid_t		nodelist[NODEID_MAX];

	boolean_t		keys_exist = B_FALSE;
	boolean_t		can_have_keys = B_TRUE;
	boolean_t		needs_keys = B_FALSE;
	boolean_t		scrub_keys_only = B_FALSE;

	boolean_t		reg_scsi2 = B_FALSE;
	boolean_t		reg_scsi3 = B_FALSE;
	boolean_t		reg_nofence = B_FALSE;
	int			i;
	/*
	 * Indicate if we have updated any of those devices' scsi3 keys, if
	 * yes, we need to undo it in case of error.
	 */
	int			scsi3keysupdated = 0;
	int			exit_err = 0;

	resvcmd = "/usr/cluster/lib/sc/reserve -c switch_device_scsi_protocol";

	(void) sighold(SIGINT);

	/* Grab CCR lock */
	if (did_holdccrlock()) {
		clerror("Cannot lock CCR.\n");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		goto err_return;
	}

	for (i = 0; i < numdisks; i++) {
		char	*instance = disks[i];
		int	inst;

		/* Get the instance number for this device */
		inst = get_instance_number(instance);
		if (inst < 0) {
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_ENOENT;

			exit_err = 1;
			continue;
		}

		if (did_getinst(&dlistp, inst) == -1) {
			clerror(DERROR, inst);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_ENOENT;

			exit_err = 1;
			continue;
		}

		if (strcmp(dlistp->device_type->type_name, "disk") != 0) {
			clerror("Device instance \"%s\" is not a disk.\n",
			    instance);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_ENOENT;

			exit_err = 1;
			continue;
		}

		if (dlistp->default_fencing == protocol) continue;

		if (is_quorum(dlistp)) {
			clwarning("Device instance \"%s\" is currently a "
			    "quorum device.\n", instance);
			clerror("Cannot change SCSI fencing protocol for "
			    "device instance \"%s\".\n", instance);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_ESTATE;

			exit_err = 1;
			continue;
		}

		/* does the device currently have PGR keys? */
		if (dlistp->default_fencing == FENCING_USE_SCSI3)
			keys_exist = B_TRUE;
		else if ((dlistp->default_fencing == FENCING_USE_PATHCOUNT) &&
		    (did_get_num_paths(dlistp) > 2))
			keys_exist = B_TRUE;
		else if ((dlistp->default_fencing == FENCING_USE_GLOBAL) &&
		    (cur_globalfencing_status == GLOBAL_FENCING_PREFER3))
			keys_exist = B_TRUE;
		else if ((dlistp->default_fencing == FENCING_USE_GLOBAL) &&
		    (cur_globalfencing_status == GLOBAL_FENCING_PATHCOUNT) &&
		    (did_get_num_paths(dlistp) > 2))
			keys_exist = B_TRUE;

		/* is the device able to use PGR keys? */
		if (dlistp->detected_fencing == SCSI2_FENCING)
			can_have_keys = B_FALSE;

		/* should the device end up with PGR keys? */
		if (protocol == FENCING_USE_SCSI3)
			needs_keys = B_TRUE;
		else if ((protocol == FENCING_USE_PATHCOUNT) &&
		    (did_get_num_paths(dlistp) > 2))
			needs_keys = B_TRUE;
		else if ((protocol == FENCING_USE_GLOBAL) &&
		    (cur_globalfencing_status == GLOBAL_FENCING_PREFER3))
			needs_keys = B_TRUE;
		else if ((protocol == FENCING_USE_GLOBAL) &&
		    (cur_globalfencing_status == GLOBAL_FENCING_PATHCOUNT) &&
		    (did_get_num_paths(dlistp) > 2))
			needs_keys = B_TRUE;

		/* are we turning fencing off and scrubbing? */
		if ((protocol ==  NO_FENCING) && (scrub != 0))
			scrub_keys_only = B_TRUE;
		else if ((protocol == FENCING_USE_GLOBAL) &&
		    (cur_globalfencing_status == GLOBAL_NO_FENCING) &&
		    (scrub != 0))
			scrub_keys_only = B_TRUE;

		if (!can_have_keys && needs_keys) {
			clwarning("Cannot change SCSI fencing protocol for "
			    "instance \"%s\" to SCSI-3 PGR. This device "
			    "was detected to only support SCSI-2 "
			    "Reserve/Release.\n", instance);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EOP;

			exit_err = 1;
			continue;
		}

		/*
		 * If we are turning fencing off and not scrubbing, the disk
		 * state doesn't matter.
		 */
		if ((protocol ==  NO_FENCING) && (scrub == 0))
			needs_keys = keys_exist;

		if (!keys_exist && needs_keys)
			reg_scsi3 = B_TRUE;
		else if (keys_exist && scrub_keys_only)
			reg_nofence = B_TRUE;
		else if (keys_exist && !needs_keys)
			reg_scsi2 = B_TRUE;

		if (reg_scsi3 || reg_scsi2 || reg_nofence) {

			full_did_path = get_full_did_path(dlistp);

			if (reg_scsi2) {
				/* Switch from SCSI3 to SCSI2 */
				(void) sprintf(cmd, "%s -d %s -P scsi2",
				    resvcmd, full_did_path);
			} else if (reg_scsi3) {
				/* Switch from SCSI2 to SCSI3 */
				(void) sprintf(cmd, "%s -d %s "
				    "-P scsi3", resvcmd, full_did_path);
			} else {
				/* Switch to no fencing */
				(void) sprintf(cmd, "%s -d %s "
				    "-P nofence", resvcmd, full_did_path);
			}

			free(full_did_path);

			if (get_connected_nodes(dlistp, nodelist)
			    != 0) {
				goto err_return;
			}

			/* Run the cmd on the connected nodes */
			if (run_reserve_on_connected_nodes
			    (reg_scsi3, cmd,
			    nodelist)) {
				scsi3keysupdated++;
			}
		}

		/* Make the changes and commit to the CCR */
		dlistp->default_fencing = protocol;
		/* Save it to the CCR */
		if (did_saveinst(dlistp, 0)) {
			clerror("Cannot save changes to CCR.\n");

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;

			goto err_return;
		}

		if (Verbose) {
			clmessage("Successfully modified default "
			    "fencing for instance \"%s\".\n", instance);
		}
	}

	if (did_releaseccrlock() != 0) {
		clerror("Cannot save changes to CCR.\n");
		if (did_geterrorstr()) {
			clerror("%s", did_geterrorstr());
		}
		goto err_return;
	}
	(void) sigrelse(SIGINT);

	if (exit_err)
		return (1);
	else
		return (0);

err_return:
	if (scsi3keysupdated) {
		/*
		 * Need to undo the scsi3 key changes by running
		 * run_reserve -c node_join on all nodes to update scsi
		 * registrations.
		 */
		verify_scsi_state(0);
	}
	(void) did_abortccrlock();
	(void) sigrelse(SIGINT);
	return (1);
}

/*
 * Identify the list of nodes that are connected to the specified device and
 * set the corresponding entry in nodelist to 1. The nodelist can be used when
 * calling run_reserve_on_connected_nodes() to execute reserve cmd on the
 * selected nodes.
 *
 */
static int
get_connected_nodes(did_device_list_t *dlistp, nodeid_t *nodelist)
{
	did_subpath_t		*subptr = NULL;
	char			*nodename = NULL;
	nodeid_t		nodeid;
	char			*path;

	(void) memset(nodelist, 0, NODEID_MAX * sizeof (nodelist));

	/* Go through the connected node list */
	for (subptr = dlistp->subpath_listp; subptr != NULL;
	    subptr = subptr->next) {
		path = strdup(subptr->device_path);
		if (path == NULL) {
			break;
		}

		nodename = strtok(path, ":");
		if (nodename == NULL) {
			clerror("Subpath \"%s\" does not contain a nodename.\n",
			    path);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return (1);

		} else if (did_getnodeid(nodename, &nodeid)
		    != 0) {
			clerror("Could not get node ID for node %s.\n",
			    nodename);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return (1);
		} else {
			nodelist[nodeid] = 1;
		}
	}
	return (0);
}

/*
 * Invocate the specified reserve cmd on all or selected nodes to update
 * scsi3 keys on affected disks after switching of default fencing protocol.
 */
static int
run_reserve_on_connected_nodes(boolean_t reg_scsi3, char *cmd,
    nodeid_t *nodelist)
{
	int			i;
	pthread_t 		thlist[NODEID_MAX];
	struct remote_invo 	remote_invo_info_list[NODEID_MAX];
	int			run_parallel;

	if (reg_scsi3) {
		/* register scsi3 key can run in parallel. */
		run_parallel = 1;
	} else {
		/* scrub of scsi3 keys can only be done on one node at a time */
		run_parallel = 0;
	}

	(void) memset(thlist, 0, sizeof (thlist));

	/*
	 * Call run_reserve on all or selected nodes to update the scsi3 keys.
	 */
	for (i = 0; i < (int)quor_table->num_nodes; i++) {
		if (quor_table->nodelist[i].state == QUORUM_STATE_OFFLINE ||
		    nodelist[quor_table->nodelist[i].nid] == 0) {
			/* skip this node if it is offline or not set to run */
			continue;
		}

		remote_invo_info_list[i].cmd = cmd;
		remote_invo_info_list[i].nodeid = quor_table->nodelist[i].nid;

		clmessage("Updating shared devices on node %d\n",
		    quor_table->nodelist[i].nid);

		if (run_parallel) {
			if (pthread_create(&thlist[i], NULL,
			    (void *(*)(void *))run_cmd_on_node,
			    (void *)(&remote_invo_info_list[i])) != 0) {
				(void) printf(gettext("Failed to start thread "
				    "to run reserve on node %d\n"),
				    quor_table->nodelist[i].nid);
			}
		} else {
			run_cmd_on_node((void *)(&remote_invo_info_list[i]));
		}
	}

	/* wait for threads to finish */
	if (run_parallel) {
		for (i = 0; i < (int)quor_table->num_nodes; i++) {
			if (thlist[i] != 0) {
				(void) pthread_join(thlist[i], NULL);
			}
		}
	}
	return (1);
}

/* Run cmd on the specified node */
static void
run_cmd_on_node(void *remote_invo_info)
{
	clconf_errnum_t		clconf_err;

	struct remote_invo *invo_info = (struct remote_invo *)remote_invo_info;
	/*
	 * Execute the command on the remote node or local node.
	 */
	clconf_err = clconf_do_execution(invo_info->cmd,
	    invo_info->nodeid, NULL, B_TRUE, B_TRUE, B_TRUE);
	if (clconf_err != CL_NOERROR && clconf_err == CL_NO_CLUSTER) {
		/*
		 * If not no error or the node is down.
		 */
		clerror("Failed to execute command \"%s\" on node %d.\n",
		    invo_info->cmd, invo_info->nodeid);
		clwarning("Run command again manually on node %d.\n",
		    invo_info->nodeid);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ESTATE;
	}
}

static int
repair_instance(int inst)
{
	did_device_list_t	*dlistp;
	did_subpath_t		*subptr;
	ddi_devid_t	devid;
	char		path[MAXPATHLEN+1];
	int		fd;
	uint_t		node_prefix_len = strlen(node_prefix);

	/* Generate event */
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	if (did_holdccrlock()) {
		clerror("Cannot lock CCR.\n");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		(void) did_releaseccrlock();
		return (1);
	}

	if (did_getinst(&dlistp, inst) == -1) {
		clerror(DERROR, inst);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ENOENT;
		(void) did_releaseccrlock();
		return (3);
	}

	for (subptr = dlistp->subpath_listp; subptr;
	    subptr = subptr->next) {
		if (strncmp(subptr->device_path, node_prefix,
		    node_prefix_len) == 0)
			break;
	}

	if (subptr == NULL) {
		/*
		 * This instance has no path on local node
		 */
		clerror("Instance \"%d\" has no path on local node.\n", inst);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ENOENT;
		(void) did_releaseccrlock();
		return (1);
	}
	(void) strncpy(path, strchr(subptr->device_path, ':')
	    + 1, MAXNAMELEN-3);

	if ((fd = open(minor_path(path, dlistp->device_type),
	    O_RDONLY|O_NDELAY)) == -1) {
		clerror("Could not open \"%s\" - %s.\n",
		    minor_path(path, dlistp->device_type),
		    strerror(errno));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		(void) did_releaseccrlock();
		return (-1);
	}
	if (dlistp->target_device_id != NULL) {
		if (devid_get(fd, &devid) == 0) {
			impl_devid_t		*idevid;

			idevid = (impl_devid_t *)devid;
			if (did_same_dev(devid, dlistp->target_device_id,
			    dlistp) != 0) {
				dlistp->diskid_len = DEVID_GETLEN(idevid);
				dlistp->type = DEVID_GETTYPE(idevid);
				dlistp->diskid =
				    (char *)malloc(dlistp->diskid_len);
				if (dlistp->diskid == NULL) {
					print_error("repair_instance",
					    gettext("Cannot malloc memory.\n"));
					(void) did_releaseccrlock();

					if (CLDEVICE_CMD) {
						if (!exit_code)
							exit_code = CL_EIO;
						exit(exit_code);
					} else {
						exit_code = 1;
						exit(1);
					}
				}
				(void) memcpy(dlistp->diskid,
				    ((impl_devid_t *)devid)->did_id,
				    dlistp->diskid_len);

				dlistp->target_device_id = devid;
			} else {
				if (Verbose || !CLDEVICE_CMD) {
					clmessage("Device ID for the device "
					    "matches the data base.\n");
				}
			}
		} else {
			clerror("Could not get device ID for \"%s\" - %s.\n",
			    path, strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
		}
	} else {
		clerror("Cannot move device path for instance \"%d\" - "
		    "instance has no device ID.\n", inst);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ENOENT;
	}

	(void) close(fd);

	if (exit_code == 0) {
		/* Reset the detected_fencing */
		dlistp->detected_fencing = scsi_fencing_inquiry(path);
	}

	if (did_saveinst(dlistp, 0)) {
		(void) did_abortccrlock();
		clerror("Unable to save instance \"%d\".\n", inst);

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_EINTERNAL;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	(void) did_releaseccrlock();
	/*
	 * Update the driver tables
	 */
	update_driver_inst(dlistp);

	return (0);
}

/*
 * repair_instances - generates an instance number from the character input
 * and then executes repairs on that instance.
 */
static int
repair_instances(char *instance, char *fmtfile)
{
	int inst;
	assert(instance != NULL);

	if ('0' <= *instance && *instance <= '9') {
		(void) repair_instance(atoi(instance));
	} else if (*instance == 'd') {
		(void) repair_instance(atoi(instance+1));
	} else {
		/*
		 * if instlist has not been populated yet
		 * lets populate instlist or else find_instance() will fail.
		 */
		if (instlist == NULL) {
			if (did_getinstlist(&instlist, 1, typelist, fmtfile)) {
				clerror("Could not load DID instance list.\n");

				if (did_geterrorstr()) {
					clerror("%s", did_geterrorstr());
				}

				if (CLDEVICE_CMD) {
					if (!exit_code)
						exit_code = CL_EINTERNAL;
					exit(exit_code);
				} else {
					exit_code = 2;
					exit(2);
				}
			}
		}
		inst = find_instance(instance);
		if (inst < 0) {
			clerror(SERROR, instance);

			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_ENOENT;
				exit(exit_code);
			} else {
				exit_code = 2;
				exit(2);
			}
		}
		(void) repair_instance(inst);
	}
	return (0);
}

/*
 * repair_all_instances will update device info for all devices listed
 * in the did_instances CCR file (if necessary) as well as uploading the
 * new config to the DID driver.
 */
static int
repair_all_instances(int *err)
{
	did_device_list_t	*dlistp;
	did_device_list_t	*prevp = NULL;
	did_subpath_t		*subptr;
	ddi_devid_t		devid;
	char			path[MAXPATHLEN+1];
	int			fd;
	int			dirty = 0;
	uint_t			node_prefix_len = strlen(node_prefix);

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		int subpath_found = 0;
		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next) {
			if (strncmp(subptr->device_path, node_prefix,
			    node_prefix_len) == 0) {
				(void) strncpy(path,
				    strchr(subptr->device_path, ':')
				    + 1, MAXNAMELEN-3);
				subpath_found++;
				break;
			}
		}
		if (!subpath_found) {
			/*
			 * No connection from this node. Remove
			 * this instance from the instlist.
			 */
			if (dlistp == instlist)
				instlist = instlist->next;
			else
				prevp->next = dlistp->next;
			continue;
		}
		prevp = dlistp;
		if (dlistp->target_device_id != NULL) {
			/*
			 * suppress lint error re. path possibly not
			 * initialized. We will not get here if that's
			 * the case.
			 */
			/*lint -e645 */
			if ((fd = open(minor_path(path, dlistp->device_type),
			    O_RDONLY|O_NDELAY)) == -1) {
				clerror("Could not open \"%s\" - %s.\n",
				    minor_path(path, dlistp->device_type),
				    strerror(errno));

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EIO;
				*err = -1;
				continue;
			}

			/*lint +e645 */
			if (devid_get(fd, &devid) == 0) {
				impl_devid_t		*idevid;

				idevid = (impl_devid_t *)devid;
				if (did_same_dev(devid,
				    dlistp->target_device_id, dlistp) != 0) {
					dlistp->diskid_len =
					    DEVID_GETLEN(idevid);
					dlistp->type = DEVID_GETTYPE(idevid);
					dlistp->diskid =
					    (char *)malloc(dlistp->diskid_len);
					if (dlistp->diskid == NULL) {
						print_error(
						    "repair_all_instances",
						    gettext("malloc error.\n"));

						if (CLDEVICE_CMD && !exit_code)
							exit_code = CL_ENOMEM;
					}
					(void) memcpy(dlistp->diskid,
					    ((impl_devid_t *)devid)->did_id,
					    dlistp->diskid_len);

					dlistp->target_device_id = devid;
					dirty++;
				}
			} else {
				clerror("Could not get device ID for \"%s\" "
				    "- %s.\n",
				    minor_path(path, dlistp->device_type),
				    strerror(errno));

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EINTERNAL;
			}
			(void) close(fd);
			if (exit_code == 0) {
				/* Reset the detected_fencing */
				dlistp->detected_fencing =
				    scsi_fencing_inquiry(path);
			}
			/*
			 * Update the driver tables whether anything's
			 * changed or not.
			 */
			update_driver_inst(dlistp);
		}
	}
	if (dirty) {
		return (1);
	}
	return (0);
}


static int
get_subpathdevid(did_subpath_t *subptr, ddi_devid_t *devid)
{
	char		lpath[MAXPATHLEN];
	char		path[MAXPATHLEN];
	struct stat	st;
	int		fd, len;

	(void) strncpy(path, strchr(subptr->device_path, ':') + 1,
	    MAXNAMELEN-3);

	if ((len = readlink(minor_path(path, subptr->instptr->device_type),
	    lpath, MAXPATHLEN)) <= 0) {
		clerror("Could not read symbolic link for \"%s\" - %s.\n",
		    minor_path(path, subptr->instptr->device_type),
		    strerror(errno));
		clwarning("Path not loaded - \"%s\".\n",
		    minor_path(path, subptr->instptr->device_type));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		return (-1);
	}
	lpath[len] = '\0';

	if (stat(minor_path(path, subptr->instptr->device_type),
	    &st) != 0) {
		clerror("Could not stat \"%s\" - %s.\n",
		    minor_path(path, subptr->instptr->device_type),
		    strerror(errno));
		clwarning("Path not loaded - \"%s\".\n",
		    minor_path(path, subptr->instptr->device_type));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		return (-1);
	}

	switch (st.st_mode & S_IFMT) {
	case S_IFBLK:
	case S_IFCHR:
		break;
	default:
		clerror("Device path \"%s\" is not a device node.\n",
		    minor_path(path, subptr->instptr->device_type));
		clwarning("Path not loaded - \"%s\".\n",
		    minor_path(path, subptr->instptr->device_type));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (-1);
	}
	if (subptr->instptr->diskid != NULL) {

		if ((fd = open(minor_path(path,
		    subptr->instptr->device_type), O_RDONLY|O_NDELAY)) == -1) {
			clerror("Could not open \"%s\" to verify device ID "
			    "- %s.\n", minor_path(path,
			    subptr->instptr->device_type),
			    strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (-1);
		}
		if (devid_get(fd, devid) == 0) {
			(void) close(fd);
			return (0);
		} else {
			clerror("Could not get device ID for \"%s\" - %s.\n",
			    minor_path(path, subptr->instptr->device_type),
			    strerror(errno));
			(void) close(fd);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (-1);
		}
	} else {
		devid = NULL;
		return (-1);
	}
}

static void
check_devid(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	ddi_devid_t	devid;
	char		disk[MAXPATHLEN];
	int		local;

	if (get_subpathdevid(subptr, &devid) == 0) {
		(void) sprintf(disk, "%ss2",
		    strchr(subptr->device_path, ':') + 1);
		/*
		 * Check Solaris 8 devid as well in case we have just upgraded
		 * from Solaris 8 to Solaris 9, in which case the CCR
		 * will be storing Solaris 8 device ids, but the kernel
		 * is Solaris 9.
		 */
		if (did_same_dev(devid, dlistp->target_device_id,
		    dlistp) != 0) {
			if (did_match_solaris8_devid(disk,
			    dlistp->target_device_id, &local) == 0) {
				if (CLDEVICE_CMD)
					clwarning("Device ID for \"%s\" needs "
					    "to be updated, run 'cldevice "
					    "repair' to update.\n",
					    subptr->device_path);
				else
					clwarning("Device ID for \"%s\" needs "
					    "to be updated, run 'scdidadm -R' "
					    "to update.\n",
					    subptr->device_path);

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_ESTATE;

			} else if (local != 1) {
				clerror("Device ID \"%s\" does not match "
				    "physical device ID for \"d%d\".\n",
				    subptr->device_path,
				    subptr->instptr->instance);
				clwarning("Device \"%s\" might have been "
				    "replaced.\n", subptr->device_path);

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_ESTATE;
			}
		}
	}
}

static void
check_consistency()
{
	did_subpath_t *subptr;
	did_device_list_t	*dlistp;
	uint_t			node_prefix_len = strlen(node_prefix);

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {

		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next)
			if (strncmp(subptr->device_path, node_prefix,
			    node_prefix_len))
				continue;
			else
				check_devid(dlistp, subptr);
	}
}

/*
 * Function to check if there is any non-digit character
 * in the string instance.
 */
static boolean_t
validate_instance_name(char *instance, int len)
{
	int i;

	/* Boundary condition for didadm -l d */
	if (len == 0)
		return (B_FALSE);

	for (i = 0; i < len; i++) {
		if (!isdigit((int)*(instance + i)))
			return (B_FALSE);
	}

	return (B_TRUE);
}

static int
get_instance_number(char *instance)
{
	int inst;

	if ('0' <= *instance && *instance <= '9') {
		if (validate_instance_name(instance, (int)strlen(instance)))
			return (atoi(instance));
		else {
			clerror(SERROR, instance);
			return (-1);
		}
	} else if (*instance == 'd') {
		if (validate_instance_name(instance+1, (int)strlen(instance+1)))
			return (atoi(instance+1));
		else {
			clerror(SERROR, instance);
			return (-1);
		}
	} else {
		inst = find_instance(instance);
		if (inst < 0) {
			clerror(SERROR, instance);
			if (!CLDEVICE_CMD) {
				exit_code = 2;
				exit(2);
			} else
				return (inst);
		} else
			return (inst);
	}
}


static void
list_instances(char *instance, int longlist, int kernel)
{
	did_device_list_t	*dlistp;

	if (instance) {
		int	inst, result;

		/* Get the instance number for this device */
		inst = get_instance_number(instance);
		if (inst < 0) {
			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_ENOENT;
			return;
		}

		result = list_instance(inst, longlist, kernel);
		if (result) {
			clerror(DERROR, inst);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_ENOENT;
		}
		return;
	}
	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		(void) list_instance(dlistp->instance, longlist, kernel);
	}
}

static void
list_header()
{
	uint_t	i, j, wcnt = (uint_t)fmtcnt;
	char	**wfmt = format;

	if (fmtcnt == 0) {
		wcnt = (uint_t)dfmtcnt;
		wfmt = dformat;
	}

	for (i = 0; i < wcnt; i++) {
		for (j = 0; j < sizeof (fmts)/sizeof (*fmts); j++) {
			if (strcmp(fmts[j].fmtname, wfmt[i]))
				continue;

			(void) printf(fmts[j].fmt, fmts[j].hdr);
		}
	}
	(void) printf("\n");
}

static void
list_globalfencing_status()
{
	char *globalfencingstr = NULL;
	if (cur_globalfencing_status == GLOBAL_FENCING_UNKNOWN) {
		clerror("Unable to retrieve the global default fencing "
		    "settings.\n");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	} else if (did_globalfencingtostr(cur_globalfencing_status,
	    &globalfencingstr) != 0) {
		if (did_geterrorstr())
			clerror("Error lookup: %s\n", did_geterrorstr());

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	} else if (cur_globalfencing_status == GLOBAL_NO_FENCING) {
		(void) printf(gettext("\nFencing is currently disabled for "
		    "disks using the global fencing setting.\n"));
	} else {
		(void) printf(gettext("\nThe cluster is currently configured"
		    " to use %s to determine fencing protocol for "
		    "shared devices.\n"), globalfencingstr);
		free(globalfencingstr);
	}
}

static int
list_instance(int inst, int longlist, int kernel)
{
	char	**wfmt = format;
	did_device_list_t	*dlistp;
	did_subpath_t	*subptr = NULL;
	uint_t	i, j, wcnt = (uint_t)fmtcnt;

	if (longlist) {
		if ((dlistp = findInstanceByNum(inst)) == NULL) {
			return (-1);
		}
	} else {
		if (kernel) {
			if (kget_instinfo(inst, &dlistp, &subptr) == -1)
				return (-1);
		} else {
			if (get_instinfo(inst, &dlistp, &subptr) == -1)
				return (-1);
		}
	}

	if (fmtcnt == 0) {
		wcnt = (uint_t)dfmtcnt;
		wfmt = dformat;
	}

	if (longlist)
		subptr = dlistp->subpath_listp;

	if (subptr == NULL)
		return (0);

	do {
		int pr_newline = 0;
		for (i = 0; i < wcnt; i++) {
			for (j = 0; j < sizeof (fmts)/sizeof (*fmts); j++) {
				if (strcmp(fmts[j].fmtname, wfmt[i]))
					continue;
				if ((!longlist) &&
				    (getinst_next_local_path(subptr) != 0))
					continue;
				(void) printf(fmts[j].fmt,
					    (*fmts[j].getfmtdata)(dlistp,
					    subptr));
				pr_newline++;
			}
		}
		if (pr_newline)
			(void) printf("\n");
		subptr = subptr->next;
	} while (subptr);
	return (0);
}

/*ARGSUSED*/
static int
kget_instinfo(int inst, did_device_list_t **dlistp, did_subpath_t **subptr)
{
	return (-1);
}

static int
get_instinfo(int inst, did_device_list_t **dlistp, did_subpath_t **subptr)
{
	uint_t node_prefix_len = strlen(node_prefix);

	for (*dlistp = instlist; *dlistp; *dlistp = (*dlistp)->next) {
		if ((*dlistp)->instance != inst)
			continue;

		for (*subptr = (*dlistp)->subpath_listp; *subptr;
		    *subptr = (*subptr)->next)
			if (strncmp((*subptr)->device_path, node_prefix,
			    node_prefix_len)
			    == 0)
				return (0);
	}
	return (-1);
}

/*
 * Given a did subpath pointer, return 0 if the subpath is for this node
 * otherwise return -1.
 */
static int
getinst_next_local_path(did_subpath_t *subptr)
{
	if (strncmp(subptr->device_path, node_prefix,
	    strlen(node_prefix)) == 0)
		return (0);
	return (-1);
}


static did_subpath_t	*
find_instance_ptr(const char *inst)
{
	uint_t	instlen = strlen(inst);
	uint_t	pathlen;
	did_device_list_t	*dlistp;
	did_subpath_t	*subptr;
	uint_t		node_prefix_len = strlen(node_prefix);

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next) {
			if ((pathlen = strlen(subptr->device_path)) < instlen)
				continue;
			if (strncmp(subptr->device_path, node_prefix,
			    node_prefix_len))
				continue;
			if (strcmp(subptr->device_path+pathlen-instlen, inst)
			    == 0)
				return (subptr);
		}
	}
	for (dlistp = ftwlist; dlistp; dlistp = dlistp->next) {
		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next) {
			if ((pathlen = strlen(subptr->device_path)) < instlen)
				continue;
			if (strncmp(subptr->device_path, node_prefix,
			    node_prefix_len))
				continue;
			if (strcmp(subptr->device_path+pathlen-instlen, inst)
			    == 0)
				return (subptr);
		}
	}
	return (0);
}

static int
find_instance(char *inst)
{
	uint_t	instlen = strlen(inst);
	uint_t	pathlen;
	did_device_list_t	*dlistp;
	did_subpath_t	*subptr;
	uint_t	node_prefix_len = strlen(node_prefix);

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next) {
			if ((pathlen = strlen(subptr->device_path)) < instlen)
				continue;
			if (strcmp(subptr->device_path+pathlen-instlen, inst))
				continue;
			if (instlen == pathlen)
				return (dlistp->instance);
			if (strncmp(subptr->device_path, node_prefix,
			    node_prefix_len) == 0)
				return (dlistp->instance);
		}
	}
	return (-1);
}

/* given a device path, return the DID instance for it */
static int
find_instance_by_path(char *path)
{
	did_device_list_t *dlistp;
	did_subpath_t *subptr;

	for (dlistp = instlist; dlistp; dlistp = dlistp->next)
		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next)
			if (strcmp(path, subptr->device_path) == 0)
				return (dlistp->instance);
	return (-1);
}

static void
did_reloader()
{
	FILE *p;
	int i, loop_stop = 0;
	int lfp, i_pid;
	char lock_file[MAXPATHLEN];
	char c_pid[MAXPATHLEN];
	/*
	 * Check if the did reloader daemon is running
	 * and send a signal to exit
	 */
	snprintf(lock_file, MAXPATHLEN, "%s/%s", RUNNING_DIR, LOCK_FILE);
	lfp = open(lock_file, O_RDONLY);
	if (lfp > 0) {
	    if (read(lfp, c_pid, MAXPATHLEN) > 0) {
		if (lockf(lfp, F_TLOCK, 0) < 0) {
		    /* Daemon is running */
		    i_pid = atoi(c_pid);
		    if (i_pid > 0) {
			if (sigsend(P_PID, i_pid, SIGHUP) == 0) {
			    /* Wait for signal to be delivered */
			    while (lockf(lfp, F_TEST, 0) < 0) {
				poll(NULL, NULL, POLL_INTERVAL);
				if (++loop_stop > POLL_TIMEOUT) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon could not be restarted
		 * because the existing daemon is not responding to
		 * signals.
		 * @user_action
		 * Stop the DID reloader daemon manually and try
		 * again.
		 */
				    did_logmsg(LOG_ERR, DIDADM,
				"DID reloader does not respond to signals\n");
				    close(lfp);
				    return;
				}
			    }
			}
		    }
		}
	    }
	    close(lfp);
	}

	if (failed_dev_paths && failed_dev_paths->size > 0) {
	    iscsi_dev *dev = failed_dev_paths->head;
	    if ((p = popen(DID_RELOADER_CMD, "w")) == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon could not be started using popen
		 * system call.
		 * @user_action
		 * Check the processes on the system and limits on the
		 * system.
		 */
		did_logmsg(LOG_ERR, DIDADM, "DID reloader cannot be started\n");
		return;
	    }
	    while (dev->next != dev->next->next) {
		fprintf(p, "%s\n", dev->next->dev_path);
		dev = dev->next;
	    }
	    pclose(p);
	}
	if (failed_dev_paths) {
	    freeRldrList(failed_dev_paths);
	    failed_dev_paths = NULL;
	}
}

static void
load_instances(int call_reload)
{
	int	fd;

	if ((fd = open("/dev/did/admin", O_RDONLY)) < 0) {
		clerror("Cannot open %s - %s.\n", "/dev/did/admin",
		    strerror(errno));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_EIO;
			exit(CL_EIO);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	loadRecordDescription(fd);
	(void) close(fd);
	if (call_reload)
	    did_reloader();
}

static void
sort_instances()
{
	/*
	 * Use qsort, canonical form into an array.
	 */
}


static void
discover_paths()
{
	did_device_list_t	*ip, *old = instlist;
	char			*log_name;

	get_iscsi_devices(&iscsi_paths, &iscsi_path_count);

	if (ftw("/dev/rdsk", check_path_disk, 5)) {
		/*
		 * There is nothing much we can do if /dev/rdsk does not
		 * exists.  Exit the program with status of 1.
		 */
		print_error("discover_paths",
		    gettext("Cannot walk /dev/rdsk.\n"));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ESTATE;
	}
	if (ftw("/dev/rmt", check_path_tape, 5)) {
		/*
		 * SCMSGS
		 * @explanation
		 * Error in accessing /dev/rmt directory when trying to
		 * discover new paths under that directory. Because of the
		 * error, we may not discover any new tape devices added to
		 * this system.
		 * @user_action
		 * If the error is "No such file or directory" or "Not a
		 * directory" and there are tape devices attached to this host
		 * intended for access, create /dev/rmt directory by hand and
		 * run scgdevs command. If the error is "Permission denied"
		 * and there are tape devices attached to this host intended
		 * for access, check and fix the permissions in /dev/rmt and
		 * run scgdevs command. If there are no tape devices attached
		 * to this host, no action is required. For complete list of
		 * errors, refer to the ftw(3C) man page.
		 */
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "scdidadm: ftw(/dev/rmt) failed attempting to discover "
		    "new paths: %s, ignoring it", strerror(errno));
	}
	if (ftwlist != NULL) {
		if (did_allocinstances(ftwlist, &instlist, node_prefix) != 0) {
			print_error("discover_paths",
			    gettext("Could not allocate instances.\n"));
		}
		/*
		 * The following assumes that did_allocinstances appends
		 * instlist to ftwlist before reassigning instlist to ftwlist.
		 */
		ip = ftwlist;
		while (ip != NULL && ip != old) {
			did_subpath_t *subptr = NULL;

			log_name = get_fmt_name(ip, subptr);
			if (ip->instance < 0) {
				/*
				 * This instance had already been allocated
				 * The instance value is the actual instance
				 * value * -1. Normalize it and continue.
				 */
				ip->instance *= -1;
			} else {
				if (log_name && do_logging) {
				    /* BEGIN CSTYLED */
				    /*
				     * CSTYLED is used to prevent cstyle
				     * complaints about block comments
				     * indented with spaces.
				     */
				    /*
				     * SCMSGS
				     * @explanation
				     * Informational message from scdidadm.
				     * @user_action
				     * No user action required.
				     */
				    /* END CSTYLED */
				    (void) sc_syslog_msg_log(handle,
				    LOG_NOTICE, ADDED,
				    "did instance %d created.",
				    ip->instance);
				}
				(void) printf(
				    gettext("did instance %d created.\n"),
				    ip->instance);
			}
			for (subptr = ip->subpath_listp; subptr;
				subptr = subptr->next) {
				if (log_name && do_logging) {
					(void) sc_syslog_msg_log(handle,
					    LOG_NOTICE, ADDED,
					    "did subpath %s created "
					    "for instance %d.",
					    subptr->device_path, ip->instance);
				}
				(void) printf(
				    gettext("did subpath %s created for "
				    "instance %d.\n"),
				    subptr->device_path, ip->instance);
			}
			ip = ip->next;
		}
	}
	sort_instances(); /* XXX - this doesn't do anything */
	free_iscsi_devices(iscsi_paths, iscsi_path_count);
}

static int
get_globalfencing_status()
{
	/* Get the current setting */
	if (did_getglobalfencingstatus(&cur_globalfencing_status)) {
		return (1);
	}
	return (0);
}

static int
switch_globalfencing(unsigned short newstatus,
    unsigned short scrub)
{
	did_device_list_t	*dlistp;
	int			dirty = 0;

	/* Get the current setting */
	assert(cur_globalfencing_status != GLOBAL_FENCING_UNKNOWN);

	if (newstatus == cur_globalfencing_status) {
		/* No need to do anything, return */
		return (0);
	}

	(void) sighold(SIGINT);
	/* Grab CCR lock to prevent changes to did_instances */
	if (did_holdccrlock()) {
		clerror("Cannot lock CCR.\n");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (1);
	}

	if (did_setglobalfencingstatus(newstatus) != 0) {
		clerror("Unable to save changes to the CCR.\n");
		(void) did_abortccrlock();

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (1);
	}

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		/*
		 * We want to keep the quorum device's default fencing
		 * unchanged if it is set to use the global setting.
		 */
		if (is_quorum(dlistp)) {
			if (dlistp->default_fencing == FENCING_USE_GLOBAL) {
				switch (cur_globalfencing_status) {
				case GLOBAL_FENCING_PATHCOUNT:
					clwarning("Device instance d%d is a "
					    "quorum device - fencing protocol "
					    "remains PATHCOUNT for the "
					    "device.\n", dlistp->instance);
					dlistp->default_fencing =
					    FENCING_USE_PATHCOUNT;
					dirty++;
					break;
				case GLOBAL_FENCING_PREFER3:
					clwarning("Device instance d%d is a "
					    "quorum device - fencing protocol "
					    "remains SCSI-3 for the device.\n",
					    dlistp->instance);
					dlistp->default_fencing =
					    FENCING_USE_SCSI3;
					dirty++;
					break;
				case GLOBAL_NO_FENCING:
					clwarning("Device instance d%d is a "
					    "quorum device - fencing protocol "
					    "remains disabled for the "
					    "device.\n", dlistp->instance);
					dlistp->default_fencing =
					    NO_FENCING;
					dirty++;
					break;
				default:
					assert(0);
					break;
				}
			}
		} else {
			if (newstatus == GLOBAL_FENCING_PREFER3) {
				if ((dlistp->default_fencing ==
				    FENCING_USE_GLOBAL) &&
				    (dlistp->detected_fencing ==
				    SCSI2_FENCING)) {
					clwarning("Device instance d%d "
					    "supports SCSI-2 only and set "
					    "to use pathcount fencing"
					    " protocol.\n",
					    dlistp->instance);
					dlistp->default_fencing =
					    FENCING_USE_PATHCOUNT;
					dirty++;
				}
			}
		}

		if (dirty) {
			if (did_saveinst(dlistp, 0)) {
				clerror("Unable to save changes to the CCR.\n");

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EINTERNAL;

				/* Restore the global status changes */
				(void) did_setglobalfencingstatus(
				    cur_globalfencing_status);

				(void) did_abortccrlock();
				return (1);
			}
		}
		dirty = 0;
	}

	/* Save the changes if necessary and release the ccr lock */
	(void) did_releaseccrlock();

	/*
	 * call run_reserve -c node_join on all nodes to update the scsi
	 * key registrations.
	 */
	if (scrub != 0) {
		if (newstatus == GLOBAL_NO_FENCING)
			/*
			 * if we are turning fencing off, we just need to clean
			 * disks
			 */
			verify_scsi_state(1);
		else
			verify_scsi_state(0);
	}

	(void) sigrelse(SIGINT);

	return (0);
}

static int
didfindmaxinst(did_device_type_t *type)
{
	did_device_list_t *instp = instlist;
	int maxinst;

	maxinst = type->start_default_nodes;
	while (instp) {

		if (instp->device_type == type) {
			if ((type->next_default_node > 0) &&
			    (instp->instance > maxinst))
				maxinst = instp->instance;
			else if ((type->next_default_node < 0) &&
			    (instp->instance < maxinst))
				maxinst = instp->instance;
		}
		instp = instp->next;
	}

	return (maxinst);
}

static did_init_type_t *
didtype_init(did_device_type_t *tlistp)
{
	did_init_type_t *tinitp;
	int i;

	tinitp = (did_init_type_t *)malloc(sizeof (*tinitp));
	if (tinitp == NULL) {
		print_error("didtype_init", gettext(
		    "Cannot malloc memory.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	(void) memset(tinitp, 0, sizeof (*tinitp));

	tinitp->dev_num = tlistp->type_num;
	(void) strcpy(tinitp->device_type, tlistp->type_name);
	tinitp->minor_count = tlistp->minor_count;

	for (i = 0; i < tlistp->minor_count; i++) {
		(void) strcpy(tinitp->minor_name[i], tlistp->minor_name[i]);
		tinitp->minor_type[i] = tlistp->device_class[i];
		tinitp->minor_number[i] = tlistp->minor_num[i];
	}

	tinitp->default_nodes = tlistp->default_nodes;
	tinitp->start_default_nodes = tlistp->start_default_nodes;
	tinitp->next_default_node = tlistp->next_default_node;

	tinitp->cur_max_instance = didfindmaxinst(tlistp);

	return (tinitp);
}

/*
 * Adds the given device path to the list of devices that the DID
 * reloader daemon uses. The DID reloader daemon will load the DID
 * drivers on these devices.
 */
static void
addDevicetoReloader(char *devpath)
{
	if (failed_dev_paths == NULL) {
	    if (initRldrList(&failed_dev_paths) < 0) {
		did_logmsg(LOG_ERR, DIDADM, "Not enough memory \n");
	    }
	}
	if (insertAfter(failed_dev_paths, NULL, devpath) < 0) {
		did_logmsg(LOG_ERR, DIDADM, "Not enough memory \n");
	}
}

static did_init_t *
didsubpath_init(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	did_init_t	*didinitp;
	ddi_devid_t	devid;
	int		len, i, fd;
	struct	stat	st;
	char		path[MAXPATHLEN];
	char		*minorPath;
	did_device_type_t	*devtype;
	char		basepath[MAXPATHLEN], fullpath[MAXPATHLEN];
	char		devdir[MAXPATHLEN];
	char		disk[MAXPATHLEN];
	int		local_device;
	char		*strp = NULL;
#ifdef SC_EFI_SUPPORT
	int		efi_disk = 0;
#endif /* SC_EFI_SUPPORT */

	devtype = dlistp->device_type;

	if (devtype == NULL) {
		clerror("Could not find device type.\n");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (0);
	}

	didinitp = (did_init_t *)malloc(sizeof (*didinitp));
	if (didinitp == NULL) {
		print_error("didsubpath_init", gettext(
		    "Cannot malloc memory.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	(void) memset((void *)didinitp, 0, sizeof (*didinitp));
	didinitp->instance = dlistp->instance;
	didinitp->dev_num = devtype->type_num;

	(void) strncpy(didinitp->subpath, strchr(subptr->device_path, ':') + 1,
	    MAXNAMELEN-3);

	if ((len = readlink(minor_path(didinitp->subpath, devtype),
	    path, MAXPATHLEN)) <= 0) {
		clerror("Could not read symbolic link for \"%s\" - %s.\n",
		    minor_path(didinitp->subpath, devtype),
		    strerror(errno));
		clwarning("Path not loaded - \"%s\".\n",
		    minor_path(didinitp->subpath, devtype));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		goto abort;
	}
	path[len] = '\0';


	(void) strcpy(devdir, minor_path(didinitp->subpath, devtype));
	*(strrchr(devdir, '/') + 1) = '\0';
	(void) strcpy(basepath, path);
	if ((strp = strrchr(basepath, ':')) == NULL) {
		clwarning("Invalid device path - \"%s\" - missing \":\". "
		    "Skipped.\n", path);
		goto abort;
	} else {
		*(strp + 1) = '\0';
	}
	minorPath = minor_path(didinitp->subpath, devtype);
	if (call_reloader &&
		(dlistp->flags & ISCSI_T_PROTOCOL) == ISCSI_T_PROTOCOL) {
		/*
		 * Store iSCSI device paths so that a reloader daemon can
		 * try these devices later. This avoid unnecessary delay
		 * of trying to stat a iSCSI device.
		 * Do this optimisation only if the reloader daemon will
		 * be launched.
		 */
	    addDevicetoReloader(minorPath);
	    goto abort;
	}
	if (stat(minorPath, &st) != 0) {
		clerror("Could not stat \"%s\" - %s.\n", path, strerror(errno));
		clwarning("Path node loaded - \"%s\".\n", path);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;

		/*
		 * Store device paths so that a daemon can try these
		 * devices later
		 */
		addDevicetoReloader(minorPath);
		goto abort;
	}

	switch (st.st_mode & S_IFMT) {
	case S_IFBLK:
	case S_IFCHR:
		break;
	default:
		clerror("Device path \"%s\" is not a device node.\n", path);
		clwarning("Path not loaded - \"%s\".\n", path);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		goto abort;
	}
	if ((fd = open(minor_path(didinitp->subpath, devtype),
	    O_RDONLY|O_NDELAY)) == -1) {
		clerror("Could not open \"%s\" to verfiy device ID - %s.\n",
		    minor_path(didinitp->subpath, devtype),
		    strerror(errno));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		goto abort;
	}

#ifdef SC_EFI_SUPPORT
	efi_disk = did_efi_disk(minor_path(didinitp->subpath, devtype));
#endif /* SC_EFI_SUPPORT */

	if (devid_get(fd, &devid) == 0) {
		(void) close(fd);
		if (did_same_dev(devid, dlistp->target_device_id,
		    dlistp) != 0) {
			/*
			 * Check Solaris 8 devid in case we have just upgraded
			 * from Solaris 8 to Solaris 9, in which case the CCR
			 * will be storing Solaris 8 device ids, but the kernel
			 * is Solaris 9.
			 */
			(void) sprintf(disk, "%ss2", didinitp->subpath);
			if (did_match_solaris8_devid(disk,
			    dlistp->target_device_id, &local_device) != 0) {
				/*
				 * We don't emulate Solaris 8 device ids for
				 * non-sd and non-ssd devices (ide).  But since
				 * these are local devices, the chances of them
				 * being reconfigured are negligible.
				 */
				if (local_device == 0) {
					clerror("Device ID for device \"%s\" "
					    "does not match physical disk's "
					    "ID.\n", didinitp->subpath);

					if (CLDEVICE_CMD && !exit_code)
						exit_code = CL_ESTATE;

					if (lookup_devid(path,
					    dlistp->target_device_id) == 0) {
						clwarning("Device \"%s\" now "
						    "appears at \"%s\".\n",
						    didinitp->subpath, path);
						clwarning("The cables or the "
						    "drives might have been "
						    "swapped.\n");
					} else {
						clwarning("The drive might "
						    "have been replaced.\n");
					}
					goto abort;
				}
			}
		}

		didinitp->device_id = dlistp->target_device_id;
	} else if (dlistp->type == DEVID_NONE) {
		(void) close(fd);
		didinitp->device_id = NULL;
	} else {
		(void) close(fd);
		clerror("Could not get device ID for \"%s\" - %s.\n",
		    didinitp->subpath, strerror(errno));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		goto abort;
	}

	(void) strcpy(didinitp->subpath, path + strlen("../../devices/"));
	(void) strcpy(path, strchr(didinitp->subpath, '/'));
	if ((strp = strrchr(path, ':')) == NULL) {
		clwarning("Invalid device path - \"%s\" - missing \":\". "
		    "Skipped.\n", path);
		goto abort;
	} else {
		*(strp + 1) = '\0';
	}
	(void) strcpy(didinitp->subpath, path);

	for (i = 0; i < devtype->minor_count; i++) {

		(void) sprintf(fullpath, "%s%s%s", devdir, basepath,
		    devtype->cur_minor_name[i]);
#ifdef SC_EFI_SUPPORT
		if (efi_disk == DID_DISK_TYPE_EFI) {
			/*
			 * For EFI, on sparc and i386/amd64, there is no name
			 * ending with :h in /devices, there is instead a name
			 * ending with :wd representing whole disk.
			 */
			if (strcmp(devtype->cur_minor_name[i], "h") == 0) {
				(void) sprintf(fullpath, "%s%swd",
				    devdir, basepath);
				/*
				 * There can be sometimes no wd name, depending
				 * Solaris version, even if disk is EFI.
				 */
				if (stat(fullpath, &st) != 0) {
					(void) sprintf(fullpath, "%s%sh",
					    devdir, basepath);
				}
			} else {
				if (strcmp(devtype->cur_minor_name[i],
				    "h,raw") == 0) {
					(void) sprintf(fullpath, "%s%swd,raw",
					    devdir, basepath);
					/*
					 * There can be sometimes no wd,raw
					 * name, depending Solaris version,
					 * even if disk is EFI.
					 */
					if (stat(fullpath, &st) != 0) {
						(void) sprintf(fullpath,
						    "%s%sh,raw",
						    devdir, basepath);
					}
				}
			}
		}
		if (efi_disk == DID_DISK_TYPE_UNKNOWN) {
			/*
			 * DKIOCGGEOM can fail because of a shared
			 * disk that is 'reserved'. In this case,
			 * we are unable to know if disk is EFI or not, so
			 * try :h, if it fails, try :wd in next block.
			 * So we will check :h, if it fails, we check :wd
			 * in next block, if :wd also fails in next block,
			 * there is a real problem that is caught in next block.
			 */
			if (strcmp(devtype->cur_minor_name[i], "h") == 0) {
				if (stat(fullpath, &st) != 0) {
					(void) sprintf(fullpath, "%s%swd",
					    devdir, basepath);
				}
			} else {
				if (strcmp(devtype->cur_minor_name[i],
				    "h,raw") == 0) {
					if (stat(fullpath, &st) != 0) {
						(void) sprintf(fullpath,
						    "%s%swd,raw",
						    devdir, basepath);
					}
				}
			}
		}
		/*
		 * An EFI labeled disk with > 1TB size may have links for
		 * s0-s6 only in /devices. This breaks the assumption
		 * that /devices has entries for s0-s15 for an EFI disk.
		 * Also, EFI labeled disks have s0-s6 only as usable
		 * slices(and s8 as reserved) irrespective of the
		 * platform (sparc/amd64), so here we do not stat()
		 * to slices above s8(:i or :i,raw).
		 * In future, if Solaris supports more that s0-s6 as
		 * usable slices for EFI labeled disks, we will need to
		 * revisit this part of the code.
		 */
		if (efi_disk == DID_DISK_TYPE_EFI) {
			if (devtype->cur_minor_name[i][0] >= 'i') {
				didinitp->devtlist[i] = NODEV;
				continue;
			}
		}
#endif /* SC_EFI_SUPPORT */
		if (stat(fullpath, &st) != 0) {
			clerror("Could not stat \"%s\" - %s.\n", fullpath,
			    strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			goto abort;
		}
		didinitp->devtlist[i] = st.st_rdev;
	}
	return (didinitp);
abort:
	free(didinitp);
	return (0);
}

/*
 * This function should only be called for disk devices.
 */
static int
lookup_devid(char *path, ddi_devid_t tdevid)
{
	did_device_list_t	*dlistp, *dp;
	did_subpath_t	*subptr;
	ddi_devid_t	devid;
	int		fd;

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		if (get_instinfo(dlistp->instance, &dp, &subptr) == -1)
			continue;
		(void) strncpy(path, strchr(subptr->device_path, ':')+1,
		    MAXNAMELEN-3);
		(void) strcat(path, "s2");

		if ((fd = open(path, O_RDONLY|O_NDELAY)) == -1) {
			continue;
		}

		/*
		 * Check Solaris 8 devid as well in case we have just upgraded
		 * from Solaris 8 to Solaris 9, in which case the CCR
		 * will be storing Solaris 8 device ids, but the kernel
		 * is Solaris 9.
		 */
		if (devid_get(fd, &devid) == 0) {
			(void) close(fd);
			if ((did_same_dev(devid, tdevid, NULL) != 0) &&
			    (did_match_solaris8_devid(path, tdevid,
			    NULL) != 0))
				continue;
			path[strlen(path)-2] = '\0';
			return (0);
		} else {
			(void) close(fd);
			continue;
		}
	}
	return (-1);
}

/*ARGSUSED*/
static void
free_didsubpath(did_init_t *didinitp)
{
}

/*ARGSUSED*/
static void
free_didtype(did_init_type_t *didtypep)
{
}

/*
 * The following function are only called in the special internal
 * option in didadm, -a. This special option is used by the DID reloader
 * daemon to reload DID driver instances of specific devices.
 *
 * The function takes the file descriptor to the DID admin file
 * and the single DID device as input
 */
static int
reload_driver_inst(int fd, did_device_list_t *dlistp)
{
	did_subpath_t		*subptr;
	did_init_t		*didinitp;
	int			subpath_number = 1;
	uint_t			node_prefix_len = strlen(node_prefix);
	did_device_type_t	*tlistp;
	did_init_type_t		*didtypep;
	int ret = 0;
	char errString[MAXPATHLEN];


	tlistp = dlistp->device_type;
	didtypep = didtype_init(tlistp);
	if (ioctl(fd, IOCDID_INITTYPE, didtypep) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * IOCDID_INITTYPE ioctl failed on the device.
		 * @user_action
		 * No user action is required. Contact the Sun service
		 * provider to see if a workaround or a patch is
		 * available.
		 */
		sprintf(errString, "ioctl(%s) error for type \"%s\" - %s\n",
			"IOCDID_INITTYPE", tlistp->type_name, strerror(errno));
		did_logmsg(LOG_ERR, DIDADM, errString);
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		ret = -1;
	}
	free_didtype(didtypep);
	for (subptr = dlistp->subpath_listp; subptr;
	    subptr = subptr->next) {

		/*
		 * Load only the devices which have a direct
		 * connection to this node into the actual
		 * did driver.
		 */
		if (strncmp(subptr->device_path, node_prefix,
		    node_prefix_len)) {
			if (Verbose) {
		/*
		 * SCMSGS
		 * @explanation
		 * The remote path to the device is not handled on this
		 * node.
		 * @user_action
		 * This is a informational message, there is no user
		 * action.
		 */
			    sprintf(errString, "Skipped remote path \"%s\".\n",
				    subptr->device_path);
			    did_logmsg(LOG_DEBUG, DIDADM, errString);
			}
			continue;
		}

		if ((didinitp = didsubpath_init(dlistp, subptr)) == 0)
			continue;

		didinitp->state = subpath_number;
		if (ioctl(fd, IOCDID_INIT, didinitp) != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * IOCDID_INIT ioctl failed on the device.
			 * @user_action
			 * No user action is required. This is a internal
			 * error. Check the error message string for
			 * more specific information about the error.
			 */
			sprintf(errString,
				"ioctl(%s) error on path \"%s\" - %s\n",
				"IOCDID_INIT", subptr->device_path,
				strerror(errno));
			did_logmsg(LOG_ERR, DIDADM, errString);
			free_didsubpath(didinitp);
			continue;
		}
		free_didsubpath(didinitp);
		subpath_number++;
	}
	return (ret);
}

/*
 * The following function are only called in the special internal
 * option in didadm, -a. This special option is used by the DID reloader
 * daemon to reload DID driver instances of specific devices.
 */
void
reload_instance(int fd, void *args)
{
	int instlen, retVal = 0;
	uint_t	pathlen;
	did_device_list_t	*dlistp, *cur_dlistp = NULL;
	did_subpath_t	*subptr;
	iscsi_dev *dev_info = NULL;
	char *instance, *slice;
	uint_t	node_prefix_len = strlen(node_prefix);

	dev_info = (iscsi_dev *)args;
	instance = dev_info->dev_path;
	/*
	 * XXX TODO: Remove these 2 debug syslog messages
	 *  before shipping this feature.
	 */
	did_logmsg(LOG_DEBUG, DIDADM, "Reloading DID driver for\n");

	/* remove slice if present */
	if ((slice = strrchr(instance, 's')) != NULL) {
	    *slice = '\0';
	}
	did_logmsg(LOG_DEBUG, DIDADM, instance);
	instlen = strlen(instance);

	cur_dlistp = NULL;
	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
	    for (subptr = dlistp->subpath_listp; subptr;
		subptr = subptr->next) {
		if ((pathlen = strlen(subptr->device_path)) < instlen)
			continue;
		if (strcmp(subptr->device_path+pathlen-instlen, instance))
			continue;
		if (strncmp(subptr->device_path, node_prefix,
		    node_prefix_len) == 0) {
			cur_dlistp = dlistp;
			break;
		}
	    }
	}
	if (cur_dlistp == NULL) {
		return;
	}
	retVal = reload_driver_inst(fd, cur_dlistp);
	if (retVal == 0) {
	    dev_info->didload_complete = 1;
	}
}

/*
 *
 * The following function are only called in the special internal
 * option in didadm, -a. This special option is used by the DID reloader
 * daemon to reload DID driver instances of specific devices.
 *
 * reload_instances - Reloads the DID driver on that instances given via
 * stdin.
 */
static int
reload_instances(char *fmtfile)
{
	int fd, retVal = 0;
	iscsi_dev *dev = NULL;
	char buf[MAXPATHLEN];
	rldr_devlist_t *devices = NULL;

	if (initRldrList(&devices) < 0) {
		did_logmsg(LOG_ERR, DIDADM, "Out of Memory");
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ENOMEM;
		return (-1);
	}
	while (fgets(buf, MAXPATHLEN, stdin) != NULL) {
	    if (insertAfter(devices, NULL, buf) < 0) {
		did_logmsg(LOG_ERR, DIDADM, "Out of Memory");
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ENOMEM;
		freeRldrList(devices);
		return (-1);
	    }
	}
	if (devices->size == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * This internal command option requires a disk path.
		 * @user_action
		 * This is a debug message, no user action is required.
		 */
		did_logmsg(LOG_DEBUG, DIDADM,
			"\'-a\' needs at least one disk.\n");
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		freeRldrList(devices);
		return (-1);
	}

	/*
	 * if instlist has not been populated yet
	 * lets populate instlist.
	 */
	if (instlist == NULL) {
	    if (did_getinstlist(&instlist, 1, typelist, fmtfile)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon could not load all the DID
		 * devices into memory.
		 * @user_action
		 * Check whether the node is in cluster mode and CCR is
		 * valid.
		 */
		did_logmsg(LOG_ERR, DIDADM,
			"Could not load DID instance list.\n");
		if (did_geterrorstr()) {
			did_logmsg(LOG_ERR, DIDADM, did_geterrorstr());
		}
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		freeRldrList(devices);
		return (-1);
	    }
	}
	if ((fd = open("/dev/did/admin", O_RDONLY)) < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon could operate on DID
		 * devices.
		 * @user_action
		 * Check whether the node is in cluster mode and CCR is
		 * valid.
		 */
		did_logmsg(LOG_ERR, DIDADM, "Unable to open /dev/did/admin");
		retVal = -1;
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		goto error;
	}

	dev = devices->head;
	while (dev->next != dev->next->next) {
	    reload_instance(fd, (void *)dev->next);
	    if (dev->next->didload_complete != 1) {
		retVal = -1;
	    }
	    dev = dev->next;
	}
	if (ioctl(fd, IOCDID_INITDONE, 0) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * IOCDID_INITDONE ioctl failed on the DID device.
		 * @user_action
		 * No user action is required.
		 * Check the error message string for more specific
		 * information about the error.
		 */
		sprintf(buf, "ioctl(%s) error - %s.\n",
			"IOCDID_INITDONE", strerror(errno));
		did_logmsg(LOG_ERR, DIDADM, buf);
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		retVal = -1;
	}
error:
	freeRldrList(devices);
	(void) close(fd);
	return (retVal);
}

static void
update_driver_inst(did_device_list_t *dlistp)
{
	did_subpath_t		*subptr;
	did_init_t		*didinitp;
	int			fd;
	int			subpath_number = 1;
	uint_t			node_prefix_len = strlen(node_prefix);

	if ((fd = open("/dev/did/admin", O_RDONLY)) < 0) {
		clerror("Unable to open %s - %s.\n",
		    "/dev/did/admin");

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_EIO;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	for (subptr = dlistp->subpath_listp; subptr;
	    subptr = subptr->next) {
		if (strncmp(subptr->device_path, node_prefix,
		    node_prefix_len)) {
			if (Verbose)
				clwarning("Skipped remote path \"%s\".\n",
				    subptr->device_path);
			continue;
		}

		if ((didinitp = didsubpath_init(dlistp, subptr)) == 0)
			continue;

		/*
		 * Load only the devices which have a direct
		 * connection to this node into the actual
		 * did driver.
		 */
		didinitp->state = subpath_number;
		if (ioctl(fd, IOCDID_FORCE_INIT, didinitp) != 0) {
			clerror("ioctl(%s) error on path \"%s\" - %s.\n",
			    "IOCDID_FORCE_INIT", subptr->device_path,
			    strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			free_didsubpath(didinitp);
			continue;
		}
		free_didsubpath(didinitp);
		subpath_number++;
	}
	(void) close(fd);
}

static void
loadRecordDescription(int fd)
{
	did_device_list_t	*dlistp;
	did_device_type_t	*tlistp;
	did_subpath_t		*subptr;
	did_init_t		*didinitp;
	did_init_type_t		*didtypep;
	uint_t			node_prefix_len = strlen(node_prefix);

	for (tlistp = typelist; tlistp; tlistp = tlistp->next) {
		didtypep = didtype_init(tlistp);
		if (ioctl(fd, IOCDID_INITTYPE, didtypep) != 0) {
			clerror("ioctl(%s) error for type \"%s\" - %s.\n",
			    "IOCDID_INITTYPE", tlistp->type_name,
			    strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
		}
		free_didtype(didtypep);
	}

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next) {
			if (strncmp(subptr->device_path, node_prefix,
			    node_prefix_len)) {
				if (Verbose)
					clwarning("Skipped remote path "
					    "\"%s\".\n", subptr->device_path);
				continue;
			}

			if ((didinitp = didsubpath_init(dlistp, subptr)) == 0)
				continue;

			/*
			 * Load only the devices which have a direct
			 * connection to this node into the actual
			 * did driver.
			 */
			if (ioctl(fd, IOCDID_INIT, didinitp) != 0) {
				clerror("ioctl(%s) error on path \"%s\" - "
				    "%s.\n", "IOCDID_INIT", subptr->device_path,
				    strerror(errno));

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EIO;
			}
			free_didsubpath(didinitp);
		}
	}
}

static char *
diskidtostr(char *id, int idlen)
{
	int	i;
	char	*sp, *s;

	if (idlen == 0) {
		sp = (char *)malloc(2);
		if (sp == NULL) {
			print_error("diskidtostr", gettext(
			    "Cannot malloc memory.\n"));

			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_ENOMEM;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
		(void) strcpy(sp, "0");
		return (sp);
	}

	sp = (char *)malloc((uint_t)idlen*2+1);
	if (sp == NULL) {
		print_error("diskidtostr", gettext(
		    "Cannot malloc memory.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	s = sp;

	for (i = 0; i < idlen; i++) {
		(void) sprintf(sp, "%02x", id[i] & 0xff);
		sp += 2;
	}
	return (s);
}

static char *
minor_path(const char *device, did_device_type_t *devtype)
{
	static char	new_device[MAXPATHLEN];
	uint_t	discover_minor_len, device_len;

	if (device == NULL) {
		print_error("minor_path", gettext("device path is null\n"));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	}
	if ((devtype == NULL) || (devtype->discovery_minor == NULL)) {
		print_error("minor_path",
		    gettext("cannot determine device type for %s\n"), device);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	}

	discover_minor_len = strlen(devtype->discovery_minor);
	device_len = strlen(device);

	if (device_len > (MAXPATHLEN - discover_minor_len - 1)) {
		print_error("minor_path",
		    gettext("device name longer than the max path length.\n"));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	}
	if (discover_minor_len >= device_len) {
		print_error("minor_path",
		    gettext("minor name longer than the device name.\n"));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	}

	if (strncmp(device + device_len - discover_minor_len,
	    devtype->discovery_minor, discover_minor_len) == 0) {
		(void) strcpy(new_device, device);
		return (new_device);
	}

	(void) sprintf(new_device, "%s%s", device, devtype->discovery_minor);

	return (new_device);
}

static char *
no_minor_path(const char *device, did_device_type_t *devtype)
{
	static char	new_device[MAXPATHLEN];
	uint_t	 discover_minor_len, device_len;

	if (device == NULL) {
		print_error("no_minor_path", gettext("device path is null\n"));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	}
	if ((devtype == NULL) || (devtype->discovery_minor == NULL)) {
		print_error("no_minor_path",
		    gettext("cannot determine device type for %s\n"), device);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	}

	discover_minor_len = strlen(devtype->discovery_minor);
	device_len = strlen(device);

	if (device_len >
	    (MAXPATHLEN - discover_minor_len - 1)) {
		print_error("no_minor_path",
		    gettext("device name longer than the max path length.\n"));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	}
	if (discover_minor_len >= device_len) {
		print_error("no_minor_path",
		    gettext("minor name longer than the device name.\n"));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
	}
	if (strncmp(device + device_len - discover_minor_len,
	    devtype->discovery_minor, discover_minor_len) != 0) {
		(void) strcpy(new_device, device);
		return (new_device);
	}

	(void) strcpy(new_device, device);
	new_device[strlen(new_device) - discover_minor_len] = '\0';

	return (new_device);
}

struct	devidtype {
	char		*type_name;
	unsigned short	type;
};

static struct devidtype diskidtypes[] = {
	{ "DEVID_NONE",		DEVID_NONE },
	{ "DEVID_SCSI3_WWN",	DEVID_SCSI3_WWN },
	{ "DEVID_SCSI_SERIAL",	DEVID_SCSI_SERIAL },
	{ "DEVID_FAB",		DEVID_FAB },
	{ "DEVID_ENCAP",	DEVID_ENCAP },
#ifdef DEVID_ATA_SERIAL
	{ "DEVID_ATA_SERIAL",	DEVID_ATA_SERIAL },
#endif /* DEVID_ATA_SERIAL */
#ifdef DEVID_SCSI3_VPD_T10
	{ "DEVID_SCSI3_VPD_T10",	DEVID_SCSI3_VPD_T10 },
#endif /* DEVID_SCSI3_VPD_T10 */
#ifdef DEVID_SCSI3_VPD_EUI
	{ "DEVID_SCSI3_VPD_EUI",	DEVID_SCSI3_VPD_EUI },
#endif /* DEVID_SCSI3_VPD_EUI */
#ifdef DEVID_SCSI3_VPD_NAA
	{ "DEVID_SCSI3_VPD_NAA",	DEVID_SCSI3_VPD_NAA }
#endif /* DEVID_SCSI3_VPD_NAA */
	};

static char *
typetostring(unsigned short type)
{
	uint_t	i;

	for (i = 0; i < sizeof (diskidtypes) / sizeof (diskidtypes[0]); i++) {
		if (type == diskidtypes[i].type)
			return (diskidtypes[i].type_name);
	}
	return ("DEVID_NONE");
}

static did_device_list_t	*
findInstanceByNum(int instnumber)
{
	did_device_list_t	*dlistp;

	for (dlistp = instlist; dlistp; dlistp = dlistp->next)
		if (dlistp->instance == instnumber)
			return (dlistp);
	return (0);
}

static void
showRecordDescription(void)
{
	did_device_list_t	*dlistp;
	did_subpath_t		*subptr;
	int			i;

	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		for (i = 0; i < dlistp->diskid_len; i++)
			if (!isprint(dlistp->diskid[i]))
				break;
		if (dlistp->diskid)
			(void) printf("#\n# %-*.*s\n#\n", i, i,
			    dlistp->diskid);
		else
			(void) printf("#\n#\n#\n");
		(void) printf("instance\t%d\n", dlistp->instance);
		(void) printf("\ttype %s\n", typetostring(dlistp->type));
		(void) printf("\tdiskid %s\n", diskidtostr(dlistp->diskid,
		    dlistp->diskid_len));

		for (subptr = dlistp->subpath_listp; subptr;
		    subptr = subptr->next) {
			(void) printf("\tsubpath %s\n", subptr->device_path);
		}
	}
}

/*ARGSUSED*/
static char *
get_fmt_inst(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	static	char instspace[10];

	(void) sprintf(instspace, "%d", dlistp->instance);
	return (instspace);
}

/*ARGSUSED*/
static char *
get_fmt_host(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	static	char path[MAXPATHLEN];

	(void) strcpy(path, subptr->device_path);
	*strchr(path, ':') = '\0';
	return (path);
}

/*ARGSUSED*/
static char *
get_fmt_path(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	static char path[MAXPATHLEN];

	(void) strcpy(path, subptr->device_path);
	(void) strcpy(path, strchr(path, ':') + 1);
	return (path);
}

/*ARGSUSED*/
static char *
get_fmt_fullpath(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	static char path[MAXPATHLEN];

	(void) strcpy(path, subptr->device_path);
	return (path);
}

/*ARGSUSED*/
static char *
get_fmt_name(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	static	char name[MAXPATHLEN];

	if (strcmp(dlistp->device_type->type_name, "disk") == 0)
		(void) sprintf(name, "d%d", dlistp->instance);
	else if (strcmp(dlistp->device_type->type_name, "tape") == 0)
		(void) sprintf(name, "%d",
		    abs(dlistp->device_type->start_default_nodes -
		    dlistp->instance) + 1);
	return (name);
}

/*ARGSUSED*/
static char *
get_fmt_fullname(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	static	char path[MAXPATHLEN];

	if (strcmp(dlistp->device_type->type_name, "disk") == 0)
		(void) sprintf(path, "%srdsk/d%d", DEFAULT_DIDPATH,
		    dlistp->instance);
	else if (strcmp(dlistp->device_type->type_name, "tape") == 0)
		(void) sprintf(path, "%srmt/%d", DEFAULT_DIDPATH,
		    abs(dlistp->device_type->start_default_nodes -
		    dlistp->instance) + 1);
	return (path);
}

/*ARGSUSED*/
static char *
get_fmt_diskid(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	return (diskidtostr(dlistp->diskid, dlistp->diskid_len));
}

/*ARGSUSED*/
static char *
get_fmt_asciidiskid(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	char	*asciidiskid;
	static char asciidiskidstring[MAXNAMELEN + 1];

	asciidiskid = did_diskidtoascii(dlistp->diskid, dlistp->diskid_len);
	if (asciidiskid == NULL || *asciidiskid == 0) {
		(void) sprintf(asciidiskidstring, "none");
	}

	(void) strcpy(asciidiskidstring, asciidiskid);
	free(asciidiskid);
	return (asciidiskidstring);
}

/*ARGSUSED*/
static char *
get_fmt_defaultfencing(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	char	*defaultfencing;
	static char defaultfencingstring[15];

	if (did_diskfencingtostr(dlistp->default_fencing,
	    &defaultfencing) != 0) {
		if (did_geterrorstr()) {
			clerror("Error lookup: %s.\n", did_geterrorstr());

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		}
	} else {
		(void) strcpy(defaultfencingstring, defaultfencing);
		free(defaultfencing);
	}
	return (defaultfencingstring);
}

/*ARGSUSED*/
static char *
get_fmt_detectedfencing(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	char	*detectedfencing;
	static char detectedfencingstring[15];

	if (did_diskfencingtostr(dlistp->detected_fencing,
	    &detectedfencing) != 0) {
		if (did_geterrorstr()) {
			clerror("Error lookup: %s.\n", did_geterrorstr());

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		}
	} else {
		strcpy(detectedfencingstring, detectedfencing);
		free(detectedfencing);
	}
	return (detectedfencingstring);
}

/*ARGSUSED*/
static char *
get_fmt_replication(did_device_list_t *dlistp, did_subpath_t *subptr)
{
	did_repl_list_t *repl;

	if ((repl = find_repl_match(dlistp->instance)) == NULL)
		return ("none");
	else
		return (repl->repl_type);
}

static void
usage(char *name)
{
	uint_t	i;

	(void) fprintf(stderr, gettext("Usage: %s [disk]\n"), name);
	(void) fprintf(stderr,
	    gettext("\t -c check the configuration file against disks\n"));
	(void) fprintf(stderr,
	    gettext("\t -R perform repair procedure against the disks\n"));
	(void) fprintf(stderr,
	    gettext("\t -G Set or show the global default fencing status\n"));
	(void) fprintf(stderr,
	    gettext("\t -F change the fencing protocol for the specified "
	    "disk(s).\n"));
	(void) fprintf(stderr,
	    gettext("\t -u upload the path information to the driver\n"));
	(void) fprintf(stderr,
	    gettext("\t -i initialize the did driver\n"));
	(void) fprintf(stderr,
	    gettext("\t -r reconfigure did, search for all disks\n"));
	(void) fprintf(stderr,
	    gettext("\t -C clean up DID mappings to nonexistent devices\n"));
	(void) fprintf(stderr,
	    gettext("\t -T remote_node -e replication_type    configure "
	    "replicated devices\n"));
	(void) fprintf(stderr,
	    gettext("\t -t x:y move DID instance x to DID instance y\n"));
	(void) fprintf(stderr,
	    gettext("\t -t x:y -e replication_type -g replication_group "
	    "combine DID instance x with DID instance y\n"));
	(void) fprintf(stderr,
	    gettext("\t -b x    split a combined DID instance x\n"));
	(void) fprintf(stderr,
	    gettext("\t -U Upgrade from did.conf format to CCR format\n"));
	(void) fprintf(stderr,
	    gettext("\t -v Print program version\n"));
	(void) fprintf(stderr,
	    gettext("\t -l list device mappings for this host\n"));
	(void) fprintf(stderr,
	    gettext("\t -L list device mappings for all hosts\n"));
	(void) fprintf(stderr,
	    gettext("\t -h Print header for device listing\n"));
	(void) fprintf(stderr,
	    gettext("\t -o format where format is one of the following:\n"));
	(void) fprintf(stderr, "\t");
	for (i = 0; i < sizeof (fmts)/sizeof (*fmts); i++)
		(void) fprintf(stderr, "%s ", fmts[i].fmtname);
	(void) fprintf(stderr, "\n");

	if (CLDEVICE_CMD) {
		exit_code = CL_EINVAL;
		exit(CL_EINVAL);
	} else {
		exit_code = 1;
		exit(1);
	}
}

/*
 *   print_error should be called like
 *	 print_error(function_name, format, arg1, ...);
 */
void
print_error(char *function_name, char *fmt, ...)
{
	char buffer[BUFSIZ];

	va_list ap;
	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, fmt);
	/*lint +e40 */

	(void) sprintf(buffer, "Error in %s(): %s", function_name, fmt);
	clerror(buffer, ap);

	va_end(ap);

	if (CLDEVICE_CMD) {
		if (!exit_code)
			exit_code = CL_EINTERNAL;
		exit(exit_code);
	} else {
		exit_code = 1;
		exit(1);
	}
}

/*
 * Returns 1 if device exists, and 0 if it does not.
 */
static int
confirm_device(char *device, did_device_type_t *type)
{
	char		*fullname = NULL;
	struct stat	st;

	fullname = (char *)malloc(MAXNAMELEN);
	if (fullname == NULL) {
		print_error("confirm_device",
		    gettext("Out of memory. Exiting.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	(void) strncpy(fullname, strchr(device, ':')+1,
	    MAXNAMELEN-3);
	(void) strcat(fullname, type->discovery_minor);

	if (stat(fullname, &st) != 0) {
		if (errno == ENOENT) {
			free(fullname);
			return (0);
		} else {
			print_error("confirm_device",
			    gettext("Could not read device file: %s (%s)\n"),
			    fullname, strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
		}
	}

	free(fullname);
	return (1);
}

static int
delete_device(did_subpath_t *subp)
{
	did_device_list_t	*instp = subp->instptr;
	did_device_list_t	*curinst, *previnst;
	did_subpath_t		*cursub, *prevsub;

	cursub = subp->instptr->subpath_listp;
	prevsub = NULL;

	while (cursub) {
		/* Remove device from the subpath list. */
		if (cursub == subp) {
			if (prevsub)
				prevsub->next = cursub->next;
			if (instp->subpath_listp == cursub)
				instp->subpath_listp = cursub->next;
			free(cursub);
			break;
		} else {
			prevsub = cursub;
			cursub = cursub->next;
		}
	}

	if (instp->subpath_listp == NULL) {
		/*
		 * Delete the instance from instlist, since there are
		 * no subpaths left.
		 */
		curinst = instlist;
		previnst = NULL;

		while (curinst) {
			if (curinst == instp) {
				if (previnst)
					previnst->next = curinst->next;
				if (instlist == instp)
					instlist = instp->next;
				free(curinst);
				break;
			} else {
				previnst = curinst;
				curinst = curinst->next;
			}
		}
	}

	return (0);
}

/*
 * Remove the entry in replicated_devices for a given instance if the instance
 * is replicated.
 */
static void
did_remove_repl_entry(int instance)
{
	char didpath[MAXPATHLEN];
	did_repl_list_t repl;

	(void) sprintf(didpath, "/dev/did/rdsk/d%d", instance);
	if (did_holdccrlock()) {
		clerror("Cannot lock CCR.\n");
		(void) did_releaseccrlock();

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return;
	}
	if (check_for_repl_device(didpath, &repl) == DID_REPL_TRUE) {
		if (did_rm_repl_entry(instance) != 0) {
			clerror("Cannot remove entry from %s CCR table.\n",
			    "replicated_devices");

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		}
	}
	(void) did_releaseccrlock();
}

/*
 * Remove this node from the device service mapping to instance.
 * If no more nodes remain in this service, remove the service.
 * ds_removed is set to 1 if this was the last node and the ds was removed,
 * ds_removed set to 0 otherwise.
 */
static int
did_update_device_service(int instance, int *ds_removed)
{
	/*
	 * Unregister this node from the DCS service.
	 */
	char	didname[MAXNAMELEN];
	char	nodename[MAXNAMELEN];
	nodeid_t	this_node;
	dc_error_t	dc_err = 0;
	dc_replica_seq_t *nodes_seq;

	*ds_removed = 0;
	(void) sprintf(didname, "dsk/d%d", instance);
	(void) gethostname(nodename, MAXHOSTNAMELEN);
	if (did_getnodeid(nodename, &this_node) != 0) {
		clerror("Could not get node ID for node %s.\n",
		    nodename);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		goto node_remove_failure;
	}
	if (!dcs_initialized) {
		dc_err = dcs_initialize();
		if (dc_err != DCS_SUCCESS) {
			clerror("Cannot initialize DCS library.\n");

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			goto node_remove_failure;
		} else {
			dcs_initialized++;
		}
	}
	dc_err = dcs_remove_node(didname, this_node);
	if (dc_err != DCS_SUCCESS) {
		goto node_remove_failure;
	}
	/*
	 * SCMSGS
	 * @explanation
	 * Informational message from scdidadm.
	 * @user_action
	 * No user action required.
	 */
	(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
	    "Removing node %s from device service %s\n", nodename, didname);

	/*
	 * check to see if this was the
	 * last node removed from the service.
	 * If so, remove the device service.
	 */
	dc_err = dcs_get_service_parameters(didname, NULL,
	    NULL, &nodes_seq, NULL, NULL, NULL);
	if (dc_err != DCS_SUCCESS) {
		if (dc_err == DCS_ERR_SERVICE_NAME)
			/*
			 * No service by this name was found.
			 * This is OK since there could be a race
			 * if the user is running scdidadm -C on
			 * all nodes simultaneously.
			 */
			return (0);
		/*
		 * SCMSGS
		 * @explanation
		 * No service by this name was found when executing scdidadm
		 * -C. This service might have been removed by another
		 * instance of this command that was issed from another node.
		 * @user_action
		 * No user action required.
		 */
		(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
		    "Could not get service parameters "
		    "for device service %s, error = %d\n", didname, dc_err);
		return (1);
	}
	if (nodes_seq->count == 0) {
		*ds_removed = 1;
		/*
		 * No more nodes in this service. Remove it.
		 */
		dc_err = dcs_remove_service(didname);
		if (dc_err != DCS_SUCCESS) {
			if (dc_err != DCS_ERR_SERVICE_NAME)
				/*
				 * Again, there may be a race.
				 */
				goto service_remove_failure;
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * Informational message from scdidadm.
			 * @user_action
			 * No user action required.
			 */
			(void) sc_syslog_msg_log(handle,
			    LOG_NOTICE, MESSAGE, "Removing Device "
			    "service %s\n", didname);
		}
	}
	dcs_free_dc_replica_seq(nodes_seq);
	return (0);

node_remove_failure:
	/*
	 * SCMSGS
	 * @explanation
	 * Failed to remove the indicated node from a device service because
	 * of the indicated error.
	 * @user_action
	 * Contact your authorized Sun service provider to determine whether a
	 * workaround or patch is available.
	 */
	(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
	    "Could not remove node %s from device "
	    "service %s, error = %d\n", nodename, didname, dc_err);
	return (1);

service_remove_failure:
	/*
	 * SCMSGS
	 * @explanation
	 * scdidadm failed to remove a device service because of indicated
	 * error.
	 * @user_action
	 * Contact your authorized Sun service provider to determine whether a
	 * workaround or patch is available.
	 */
	(void) sc_syslog_msg_log(handle,
	    LOG_ERR, MESSAGE,
	    "Couldn't remove Device service %s, error = %d\n",
	    didname, dc_err);
	return (1);
}

static void
device_cleanup()
{
	did_device_list_t	*instp, *nextinst;
	did_subpath_t		*subp, *nextsub;
	ddi_devid_t		devid = NULL;
	struct	dk_cinfo	dki_info;
	int	fd;
	char	path[MAXNAMELEN];
	int	ds_removed;
	uint_t	node_prefix_len = strlen(node_prefix);

	for (instp = instlist; instp; instp = nextinst) {
		int	num_local_subpaths = 0;
		int	num_subpaths_deleted = 0;
		int	instance = instp->instance;

		nextinst = instp->next;
		/*
		 * First enumerate the number of subpaths on this node.
		 * Then cleanup. If no local subpaths are left after cleanup is
		 * performed, we will unregister the DCS service for this
		 * device on this node. If all subpaths (from all nodes) have
		 * now been deleted, remove the DCS service itself and the
		 * replicated_devices entry (for replicated devices).
		 */
		for (subp = instp->subpath_listp; subp; subp = nextsub) {
			nextsub = subp->next;

			/* Only look at the subpaths on our node. */
			if (strncmp(subp->device_path, node_prefix,
			    node_prefix_len) == 0) {
				num_local_subpaths++;
				/*
				 * See if underlying device exists
				 * in /dev tree
				 */
				if (confirm_device(subp->device_path,
				    instp->device_type) == 0) {

					/*
					 * Delete the DID device if the
					 * underlying device is gone.
					 */
					if (delete_device(subp)) {
						print_error("device_cleanup",
						    gettext("Cannot delete "
						    "device %s\n"),
						    subp->device_path);

						if (CLDEVICE_CMD && !exit_code)
							exit_code =
							    CL_EINTERNAL;
					}
					num_subpaths_deleted++;
					continue;
				}
				/*
				 * Skip tape drive.
				 */
				if (strstr(subp->device_path, "/dev/rmt")
				    != 0)
					continue;

				/*
				 * See if the underlying subpath match the
				 * devid in the structure. Otherwise, delete
				 * it.
				 */
				(void) strncpy(path,
				    strchr(subp->device_path, ':')+1,
				    MAXNAMELEN-3);
				(void) strcat(path, "s2");

				if ((fd = open(path, O_RDONLY|O_NDELAY))
				    < 0) {
					if (subp->instptr->target_device_id ==
					    NULL && errno == EBUSY)
						/* CDROM, skip it */
						continue;
					if (delete_device(subp)) {
						print_error("device_cleanup",
						    gettext("Cannot delete "
						    "device %s\n"),
						    subp->device_path);

						if (CLDEVICE_CMD && !exit_code)
							exit_code =
							    CL_EINTERNAL;
					}
					num_subpaths_deleted++;
					continue;
				}

				/*
				 * Check Solaris 8 devid as well in case we have
				 * just upgraded from Solaris 8 to Solaris 9, in
				 * which case the CCR will be storing Solaris 8
				 * device ids, but the kernel is Solaris 9.
				 */
				if (devid_get(fd, &devid) == 0) {
					if ((did_same_dev(devid,
					    subp->instptr->target_device_id,
					    subp->instptr) != 0) &&
					    (did_match_solaris8_devid(path,
					    subp->instptr->target_device_id,
					    NULL) != 0)) {
						/*
						 * Remove this subpath
						 */
						if (delete_device(subp)) {
							print_error(
							    "device_cleanup",
							    gettext(
							    "Cannot delete "
							    "device %s\n"),
							    subp->device_path);

							if (CLDEVICE_CMD &&
							    !exit_code)
								exit_code =
								CL_EINTERNAL;
						}
						num_subpaths_deleted++;
					}
				} else {
					/*
					 * If device is CDROM drive, skip it.
					 */
					dki_info.dki_ctype = 0;
					if (ioctl(fd, DKIOCINFO,
					    (caddr_t)&dki_info) == -1) {
						clerror("ioctl(%s) error - "
						    "%s.\n", "DKIOCINFO",
						    strerror(errno));
						/*
						 * Fall through and
						 * remove this subpath
						 */
					}
					if (dki_info.dki_ctype == DKC_CDROM)
						continue;
					if (delete_device(subp)) {
						print_error("device_cleanup",
						    gettext("Cannot delete "
						    "device %s\n"),
						    subp->device_path);

						if (CLDEVICE_CMD && !exit_code)
							exit_code =
							    CL_EINTERNAL;
					}
					num_subpaths_deleted++;
				}
				(void) close(fd);
			}
		}
		if ((num_local_subpaths > 0) &&
		    (num_local_subpaths == num_subpaths_deleted)) {
			(void) did_update_device_service(instance, &ds_removed);
			if (ds_removed == 1)
				did_remove_repl_entry(instance);
			/*
			 * Set DID driver did_device_ids property to zero
			 * and unregister deviceID for this instance.
			 */
			(void) rm_driver_inst(&instance, 1);
		}
	}
}

/* Read in the scsi3 only device list from the disk */
static int
get_scsi3dev_list()
{
#ifdef	DISABLE_SCSI3_LIST
	return (0);
#else
	/* Will use Slist once this file is written in C++ */
	assert(scsi2onlycount == 0);
	FILE	*fptr = fopen(SCSI2ONLYLIST, "ro");

	if (!fptr) {
		clerror("Cannot open file \"%s\" - %s.\n",
		    SCSI2ONLYLIST, strerror(errno));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (1);
	}

	while (fgets(scsi2onlylist[scsi2onlycount], DEVSTRING_LEN + 1, fptr)) {
		scsi2onlycount++;
	}

	fclose(fptr);
	return (0);
#endif
}

/*
 * Inquiry if a given device is a scsi2 only device.
 */
static unsigned short
scsi_fencing_inquiry(char *devname)
{
	struct scsi_inquiry	inq;
	struct uscsi_cmd	ucmd;
	union scsi_cdb		cdb;
	int			fd;
	int			i;
	char			buf[MAXPATHLEN];
	unsigned short		ret = FENCING_NA;
	int			status;
	mhioc_inkeys_t		keylist;
	mhioc_key_list_t	klist;

	assert(devname);

	if (devname[strlen(devname) - 2] == 's' ||
	    devname[strlen(devname) - 3] == 's') {
		/*
		 * If a slice number is provided, open the file directly.
		 * Disks on x86 can have 16 slices so we are checking both
		 * "sxx" and "sx" cases here.
		 */
		fd = open(devname, O_RDONLY|O_NDELAY);
		(void) sprintf(buf, "%s", devname);
	} else {
		/* Else, try slice0 and slice2 until we can open one. */
		for (i = 2; i >= 0; i -= 2) {
			(void) sprintf(buf, "%ss%d", devname, i);
			fd = open(buf, O_RDONLY|O_NDELAY);
			if (fd != -1) {
				break;
			}
		}
	}

	if (fd == -1) {
		clerror("Cannot open \"%s\".\n", buf);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		goto do_return;
	}

	(void) memset((char *)&inq, 0, sizeof (inq));
	(void) memset((char *)&ucmd, 0, sizeof (ucmd));
	(void) memset((char *)&cdb, 0, sizeof (union scsi_cdb));

	cdb.scc_cmd = SCMD_INQUIRY;
	FORMG0COUNT(&cdb, sizeof (inq));
	ucmd.uscsi_cdb = (caddr_t)&cdb;
	ucmd.uscsi_cdblen = CDB_GROUP0;
	ucmd.uscsi_bufaddr = (caddr_t)&inq;
	ucmd.uscsi_buflen = sizeof (inq);
	ucmd.uscsi_flags = USCSI_READ;
	status = ioctl(fd, USCSICMD, &ucmd);
	if (status || ucmd.uscsi_status) {
		clerror("Inquiry on device \"%s\" failed.\n", buf);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		goto do_return;
	}

	/*
	 * Check if the device supports scsi3
	 * Persistent Reserve In Read Keys
	 */
	keylist.li = &klist;
	keylist.li->list = (mhioc_resv_key_t *)
	    malloc(sizeof (mhioc_resv_key_t));
	keylist.li->listsize = 1;
	keylist.li->listlen = 0;
	status = ioctl(fd, MHIOCGRP_INKEYS, &keylist);

	if (status < 0) {
		/* This means ioctl has returned error */
		if (errno == ENOTSUP) {
			/* This device supports scsi2 only */
			ret = SCSI2_FENCING;
			goto do_return;
		}
	}

	/* ioctl() couldn't determine scsi-2 or scsi-3 */
	ret = FENCING_NA;

do_return:
	(void) close(fd);
	return (ret);
}

/*
 * Get the full did path from a did_device_list_t instance. Caller
 * needs to release the string returned.
 */
static char *
get_full_did_path(did_device_list_t *dev)
{
	char *did_path;
	char *full_did_path;

	did_path = did_get_did_path(dev);
	if (did_path == NULL) {
		print_error("get_full_did_path",
		    gettext("Out of memory. Exiting.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	full_did_path = (char *)malloc(2 * MAXPATHLEN);
	if (full_did_path == NULL) {
		print_error("get_full_did_path",
		    gettext("Out of memory. Exiting.\n"));

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	(void) sprintf(full_did_path, "/dev/did/%ss2", did_path);

	free(did_path);
	return (full_did_path);
}

/* Check if a did_device_list_t is a quorum device */
static int
is_quorum(did_device_list_t *dev)
{
	int i;
	int ret = 0;
	char *full_did_path;

	full_did_path = get_full_did_path(dev);

	for (i = 0; i < (int)quor_table->num_quorum_devices; i++) {
		if (strcmp(full_did_path,
		    quor_table->quorum_device_list[i].gdevname) == 0) {
			ret = 1;
			goto do_return;
		}
	}
do_return:
	free(full_did_path);
	return (ret);
}


#ifdef DID_MULTI_INSTANCE

static int
update_didconf(void)
{
	did_device_type_t	*devtype;
	did_device_list_t	*dlistp;
	/* Number of DID disk instances in current did.conf */
	unsigned int		conf_disks = 0;
	/* Number of DID tape instances in current did.conf */
	unsigned int		conf_tapes = 0;
	/* Number of DID disk instances in incore list */
	unsigned int		disk_instances = 0;
	/* Number of DID tape instances in incore list */
	unsigned int		tape_instances = 0;
	/* Number of DID disk instances to set in did.conf */
	unsigned int		rdisk_instances = 0;
	/* Number of DID tape instances to set in did.conf */
	unsigned int		rtape_instances = 0;
	/* default number of DID disks, from CCR */
	unsigned int		disk_default = 0;
	/* default number of DID tapes, from CCR */
	unsigned int		tape_default = 0;
	unsigned int		i;
	int			res;
	FILE			*ptr;
	char			cmd[DIDCONF_LINE_SIZE];
	boolean_t		gaps_in_instances = B_FALSE;
	struct stat		st;

	/*
	 * Found the did_device_type_t default nodes for
	 * DID disk and DID tapes.
	 */
	for (devtype = typelist; devtype != NULL; devtype = devtype->next) {
		if (strcmp(devtype->type_name, "disk") == 0) {
			disk_default = (unsigned int)devtype->default_nodes;
		} else {
			if (strcmp(devtype->type_name, "tape") == 0) {
				tape_default =
				    (unsigned int)devtype->default_nodes;
			}
		}
	}
	if (disk_default == 0) {
		clerror("Unable to read disk \"%s\" from CCR.\n",
		    "default_nodes");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (1);
	}
	if (tape_default == 0) {
		clerror("Unable to read tape \"%s\" from CCR.\n",
		    "default_nodes");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (1);
	}

	/*
	 * Count DID disk instances and DID tapes instances
	 * that are in the incore list of DID instances.
	 */
	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		if (strcmp(dlistp->device_type->type_name, "disk") == 0) {
			disk_instances++;
		}
		if (strcmp(dlistp->device_type->type_name, "tape") == 0) {
			tape_instances++;
		}
	}

	/*
	 * Here we compute the number of disk instances
	 * to set in the did.conf file ('rdisk_instances').
	 * The did.conf file must contain at least
	 * 'disk_instances' disk instances, and as disk instances
	 * are created by packets of 'disk_default', rounded up to
	 * 'disk_default'.
	 */
	if ((disk_instances % disk_default) != 0) {
		/* round up to disk_default */
		rdisk_instances =
		    ((disk_instances / disk_default) + 1) * disk_default;
	} else {
		/* is already a multiple of disk_default */
		rdisk_instances = disk_instances;
	}
	if (rdisk_instances == 0) {
		/* no incore disks, set up to disk_default */
		rdisk_instances = disk_default;
	}

	/*
	 * Here we compute the number of tape instances
	 * to set in the did.conf file ('rtape_instances').
	 * The did.conf file must contain at least
	 * 'tape_instances' tape instances, and as tape instances
	 * are created by packets of 'tape_default', rounded up to
	 * 'tape_default'.
	 */
	if ((tape_instances % tape_default) != 0) {
		/* round up to disk_default */
		rtape_instances =
		    ((tape_instances / tape_default) + 1) * tape_default;
	} else {
		/* is already a multiple of disk_default */
		rtape_instances = tape_instances;
	}
	if (rtape_instances == 0) {
		/* no incore tapes, set up to tape_default */
		rtape_instances = tape_default;
	}

	/*
	 * DID maximum instances is DID_MAX_INSTANCE, verify that
	 * this limit is not reached.
	 */
	if (rdisk_instances + rtape_instances > DID_MAX_INSTANCE) {
		clerror("Maximum number of DID instances (%u) has been "
		    "reached - disk(%u), tape(%u).\n", DID_MAX_INSTANCE,
		    rdisk_instances, rtape_instances);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EOP;
		return (1);
	}

	/*
	 * Now check to see if there are any DID disk instances that
	 * have an instance number greater than rdisk_instances.
	 * This can occur on configurations with say Hitachi and truecopy,
	 * where different physcical luns in different disk enclosures
	 * are associated together and kept in sync with utilities like
	 * Truecopy. The association of different physical luns can
	 * leave gaps in the DID numbering scheme (e.g d1-d20, d50-d70).
	 */
	for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
		if ((strcmp(
		    dlistp->device_type->type_name, "disk") == 0) &&
		    (dlistp->instance > (int)rdisk_instances)) {
			gaps_in_instances = B_TRUE;
		}
	}

	/*
	 * Now compare with disk and tapes instances currently in did.conf.
	 * conf_disks is the number of DID disk instances found
	 * in current did.conf, and conf_tapes is the number of DID tape
	 * instances found in current did.conf.
	 * If gaps_in_instances is true, then we need to update the did.conf
	 */
	res = get_insts_from_didconf(&conf_disks, &conf_tapes);
	if ((gaps_in_instances) || (res != 0) ||
	    (res == 0 && rdisk_instances != conf_disks) ||
	    (res == 0 && rtape_instances != conf_tapes)) {
		/*
		 * An error occurs with the command or disk instances number
		 * or tape instances number are not as expected, in both case,
		 * update /kernel/drv/did.conf file with expected number. A copy
		 * of original file is created first.
		 */
		/* save DIDCONF_FILE into DIDCONF_SV */
		(void) sprintf(cmd, "/bin/cp %s %s", DIDCONF_FILE, DIDCONF_SV);
		res = system(cmd);
		if (res != 0) {
			/*
			 * It is possible that the file is missing,
			 * simply show the error and continue in any
			 * case, the copy is not mandatory.
			 */
			clwarning("Error while copying \"%s\" to \"%s\" - "
			    "%s.\n", DIDCONF_FILE, DIDCONF_SV, strerror(errno));
		}

		/*
		 * Create the temporary DIDCONF_TMP file and populate it.
		 * This file will then be the did.conf file, if successfully
		 * created.
		 */
		ptr = fopen(DIDCONF_TMP, "w");
		if (ptr == NULL) {
			clerror("Unable to write \"%s\" - %s.\n",
			    DIDCONF_TMP, strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (1);
		}
		res = fprintf(ptr,
		    "#\n# Copyright 2006 Sun Microsystems, Inc.\n");
		if (res < 0) {
			clerror("Unable to write \"%s\" - %s.\n",
			    DIDCONF_TMP, strerror(errno));
			(void) fclose(ptr);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (1);
		}
		res = fprintf(ptr,
		    "#\n# Generated file, don't modify manually\n#\n");
		if (res < 0) {
			clerror("Unable to write \"%s\" - %s.\n",
			    DIDCONF_TMP, strerror(errno));
			(void) fclose(ptr);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (1);
		}
		res = fprintf(ptr,
		    "name=\"did\" parent=\"pseudo\" device-type=\"admin\" "
		    "instance=%d;\n", 0);
		if (res < 0) {
			clerror("Unable to write \"%s\" - %s.\n",
			    DIDCONF_TMP, strerror(errno));
			(void) fclose(ptr);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (1);
		}
		for (i = 1; i <= rdisk_instances; i++) {
			res = fprintf(ptr,
			    "name=\"did\" parent=\"pseudo\" "
			    "device-type=\"disk\" instance=%d;\n", i);
			if (res < 0) {
				clerror("Unable to write \"%s\" - %s.\n",
				    DIDCONF_TMP, strerror(errno));
				(void) fclose(ptr);

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EIO;
				return (1);
			}
		}
		/*
		 * Create did.conf entries for situations where we have DID
		 * devices with a greater instance number than rdsk_instance.
		 */
		if (gaps_in_instances) {
			for (dlistp = instlist; dlistp; dlistp = dlistp->next) {
				if ((strcmp(dlistp->device_type->type_name,
				    "disk") == 0) &&
				    (dlistp->instance > (int)rdisk_instances)) {
					res = fprintf(ptr,
					    "name=\"did\" parent=\"pseudo\" "
					    "device-type=\"disk\" "
					    "instance=%d;\n",
					    dlistp->instance);
					if (res < 0) {
						clerror("Unable to write "
						    "\"%s\" - %s.\n",
						    DIDCONF_TMP,
						    strerror(errno));
						(void) fclose(ptr);
						if (CLDEVICE_CMD && !exit_code)
							exit_code = CL_EIO;
						return (1);
					}
				}
			}
		}
		for (i = DID_MAX_INSTANCE + 1 - rtape_instances;
		    i < (DID_MAX_INSTANCE + 1); i++) {
			res = fprintf(ptr,
			    "name=\"did\" parent=\"pseudo\" "
			    "device-type=\"tape\" instance=%d;\n", i);
			if (res < 0) {
				clerror("Unable to write \"%s\" - %s.\n",
				    DIDCONF_TMP, strerror(errno));
				(void) fclose(ptr);

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EIO;
				return (1);
			}
		}
		if (fsync(fileno(ptr)) != 0) {
			clerror("Unable to fsync \"%s\" - %s.\n",
			    DIDCONF_TMP, strerror(errno));
			(void) fclose(ptr);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (1);
		}
		(void) fclose(ptr);
		/* Save stat info */
		if (stat(DIDCONF_FILE, &st) != 0) {
			clerror("Could not stat \"%s\" - %s.\n", DIDCONF_FILE,
				strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (1);
		}
		/* move DIDCONF_TMP to DIDCONF_FILE */
		res = rename(DIDCONF_TMP, DIDCONF_FILE);
		if (res != 0) {
			clerror("Error while renaming \"%s\" to \"%s\" - %s.\n",
			    DIDCONF_TMP, DIDCONF_FILE, strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (1);
		}
		/* Restore original did.conf ownership */
		if (chown(DIDCONF_FILE, st.st_uid, st.st_gid) != 0) {
			clerror("Could not chown \"%s\" - %s.\n", DIDCONF_FILE,
				strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (1);
		}
		/* Clean up */
		(void) unlink(DIDCONF_TMP);
		/* As everything is ok, cleanup also saved did.conf file */
		(void) unlink(DIDCONF_SV);
		/*
		 * Now force DID driver to re-read did.conf
		 */
		(void) sprintf(cmd,
		    "/usr/sbin/update_drv -f did >/dev/null 2>&1");
		res = system(cmd);
		if (res != 0) {
			clerror("Error executing command \"%s\" - %s.\n",
			    cmd, strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
			return (1);
		}
	}
	return (0);
}

static int
get_insts_from_didconf(unsigned int *disks, unsigned int *tapes)
{
	FILE *fi;
	char *res;
	char *tmp;
	unsigned int size = 0;
	unsigned int disksize, tapesize, adminsize;
	unsigned int i;
	static boolean_t found_instance[DID_MAX_INSTANCE + 1];
	static char line[DIDCONF_LINE_SIZE];

	/*
	 * Because we can have gaps in the DID naming scheme
	 * (e.g d1-d20, d40-d60). The variables num_disks and num_tapes are
	 * used to make sure we can skip over the gaps and process all the
	 * DID instances that are currently defined in did.conf
	 */

	unsigned int num_disks = 0;
	unsigned int num_tapes = 0;

	*disks = 0;
	*tapes = 0;

	disksize = strlen(DIDCONF_DISK_INSTANCE);
	tapesize = strlen(DIDCONF_TAPE_INSTANCE);
	adminsize = strlen(DIDCONF_ADMIN_INSTANCE);

	fi = fopen(DIDCONF_FILE, "r");
	if (fi == NULL) {
		clerror("Cannot open \"%s\" - %s.\n", DIDCONF_FILE,
		    strerror(errno));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		return (1);
	}
	while (fgets(line, DIDCONF_LINE_SIZE, fi) != NULL) {
		if (strncmp(DIDCONF_DISK_INSTANCE, line, disksize) == 0) {
			size = disksize;
			num_disks++;
		} else if (strncmp(DIDCONF_TAPE_INSTANCE, line, tapesize)
		    == 0) {
			size = tapesize;
			num_tapes++;
		} else if (strncmp(DIDCONF_ADMIN_INSTANCE, line, adminsize)
		    == 0) {
			size = adminsize;
		}
		res = line + size;
		tmp = res;
		if (tmp == NULL) {
			continue;
		}
		/* There must be now digits followed by ';' */
		while (isdigit(*tmp) != 0) {
			tmp++;
		}
		if ((tmp != res) && (*tmp == ';') &&
		    (*(tmp + 1) == '\n') && (*(tmp + 2) == '\0')) {
			*tmp = '\0';
			(void) sscanf(res, "%u", &i);
			if (i > DID_MAX_INSTANCE) {
				continue;
			}
			found_instance[i] = B_TRUE;
		}
	}
	if (found_instance[0] == B_FALSE) {
		(void) fclose(fi);
		return (1);
	}
	for (i = 1; i < (DID_MAX_INSTANCE + 1); i++) {
		if (found_instance[i] == B_FALSE) {
			if (num_disks < 1) {
				break;
			}
		}
		*disks = *disks + 1;
		num_disks--;
	}
	for (i = DID_MAX_INSTANCE; i >= 1; i--) {
		if (found_instance[i] == B_FALSE) {
			if (num_tapes < 1) {
				break;
			}
		}
		*tapes = *tapes + 1;
		num_tapes--;
	}
	if (disks == 0 || tapes == 0) {
		(void) fclose(fi);
		return (1);
	}
	(void) fclose(fi);
	return (0);
}
#endif

static int
exec_command(char **argv)
{
	int results = 0;

	pid_t pid = fork();
	if (pid == 0) {
		if (execv(argv[0], argv) == -1) {
			clerror("Cannot execute \"%s\" - %s.\n",
			    argv[0], strerror(errno));
			exit(1);
		}
		exit(0);
	} else {
		int status = 0;
		while ((status = waitpid(pid, &results, 0)) != pid) {
			if (status == -1 && errno == EINTR)
				continue;
			else {
				status = -1;
				break;
			}
		}
		if (status != pid) {
			clerror("Error waiting on command result - %s\n",
			    strerror(errno));
			return (CL_EINTERNAL);
		}
	}

	/* Return the exit code */
	return (WEXITSTATUS((uint_t)results));
}

/*
 * Make sure a specified replication type is a legal replication type by
 * making sure the associated replication script exists.
 */
int
validate_repl_type(char *repl_cmd)
{
	struct stat st;

	return (stat(repl_cmd, &st));
}

/*
 * See if there is a replicated device (with the same instance number) for the
 * specified DID instance.
 */
static did_repl_list_t	*
find_repl_match(int instance)
{
	did_repl_list_t *trans;

	for (trans = repllist; trans; trans = trans->next)
		if (trans->instance == instance)
			return (trans);

	return (NULL);
}

/*
 * See if the device id returned from the local subpath in 'instptr' matches
 * the device id stored for its replicated device, if there is one.
 *
 * Return:
 * 2  - this is not a replicated device
 * 1  - this is a replicated device and the device id does not match
 * 0  - this is a replicated device and the device id matches
 */
static int
check_repl_devid(did_device_list_t *instptr, ddi_devid_t devid)
{
	did_device_list_t replinst;
	did_repl_list_t *replptr;

	if ((replptr = find_repl_match(instptr->instance)) == NULL)
		return (2);

	replinst.type = instptr->type;
	did_strtodiskid(replptr->diskid, &replinst);
	if (replinst.type)
		did_set_type(replinst.target_device_id, replinst.type);

	if (did_same_dev(devid, replinst.target_device_id, NULL) == 0)
		return (0);
	else
		return (1);
}

/*
 * See if any of the merged paths for a replicated device are lcal to this node.
 */
static int
replica_is_local(did_repl_list_t *repl)
{
	did_repl_path_t	*pathptr;
	uint_t		node_prefix_len = strlen(node_prefix);

	for (pathptr = repl->device_path_list; pathptr;
	    pathptr = pathptr->next)
		if (strncmp(pathptr->device_path, node_prefix,
		    node_prefix_len) == 0)
			return (1);

	return (0);
}

/*
 * Verify that the specified instances are replicas of each other (belong to
 * the same dev_group and pair_volume).  If so, return the name of the dev_group
 * otherwise return NULL.
 */
static char *
check_dev_group(did_device_list_t *src_inst, did_device_list_t *dest_inst)
{
	did_subpath_t *sublistp, *sublistp2;
	char *prog[6];
	FILE *fp;
	char *dev_group, pair[MAXNAMELEN], device[MAXPATHLEN];
	char dev_group2[MAXNAMELEN], pair2[MAXNAMELEN], device2[MAXPATHLEN];
	char lorr[MAXNAMELEN], lorr2[MAXNAMELEN];
	char tcdev_out[MAXPATHLEN];

	/* get a unique, temp file name */
	(void) tmpnam(&tcdev_out[0]);

	dev_group = (char *)malloc(MAXNAMELEN);
	if (dev_group == NULL) {
		print_error("check_dev_group", gettext(
		    "Can't malloc memory.\n"));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ENOMEM;
		return (NULL);
	}

	/* Build the command string */
	prog[0] = get_repl_devs_cmd;
	prog[1] = "-f";
	prog[2] = tcdev_out;
	prog[3] = "-r";
	prog[4] = "NULL";
	prog[5] = NULL;

	/* exec GET_DEV command */
	switch (exec_command(prog)) {

	case 0:
	case 2:
		break;
	case 1:
		(void) did_releaseccrlock();
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (NULL);
	case 3:
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EOP;
		return (NULL);
	default:
		return (NULL);
	}

	if ((fp = fopen(tcdev_out, "r")) == NULL) {
		clerror("Cannot to open input file \"%s\" - %s.\n",
		    tcdev_out, strerror(errno));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		return (NULL);
	}

	while (fscanf(fp, "%s %s %s %s %s %s %s %s", dev_group, pair, lorr,
	    device, dev_group2, pair2, lorr2, device2) != EOF) {
		if (strcmp(dev_group, dev_group2) != 0) {
			clerror("Invalid input on remap: dev_groups do"
			    "not match.\n");
			(void) fprintf(stderr, "%s %s %s %s\n%s %s %s %s\n",
			    dev_group, pair, lorr, device, dev_group2, pair2,
			    lorr2, device2);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}

		if (strcmp(pair, pair2) != 0) {
			clerror("Invalid input on remap: pair volumes"
			    "do not match.\n");
			(void) fprintf(stderr, "%s %s %s %s\n%s %s %s %s\n",
			    dev_group, pair, lorr, device, dev_group2, pair2,
			    lorr2, device2);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}

		/* erase s2 from device string */
		*strstr(device, "s2") = '\0';
		*strstr(device2, "s2") = '\0';

		for (sublistp = dest_inst->subpath_listp; sublistp;
		    sublistp = sublistp->next)
			if (strstr(sublistp->device_path, device) != NULL)
				for (sublistp2 = src_inst->subpath_listp;
				    sublistp2; sublistp2 = sublistp2->next)
					if (strstr(sublistp2->device_path,
					    device2) != NULL) {
						(void) fclose(fp);
						return (dev_group);
					}

		for (sublistp = src_inst->subpath_listp; sublistp;
		    sublistp = sublistp->next)
			if (strstr(sublistp->device_path, device) != NULL)
				for (sublistp2 = dest_inst->subpath_listp;
				    sublistp2; sublistp2 = sublistp2->next)
					if (strstr(sublistp2->device_path,
					    device2) != NULL) {
						(void) fclose(fp);
						return (dev_group);
					}
	}

	(void) fclose(fp);
	return (NULL);
}

/*
 * Unreplicate a replicated device that is no longer valid in the replication
 * configuration.  Remove the replica device path from the previously combined
 * instance and, if the original DID instance for the replica path is available
 * recreate a seperate DID instance for the replica path.  If the original
 * instance is no longer availble, the user will have to run scgdevs to create
 * the new instances.
 *
 * Return values:
 *
 * -1 - error
 *  1 - device completely unreplicated
 *  2 - path only removed, could not add new path, old instance reallocated
 */
static int
unreplicate_device(did_repl_list_t *device, char *remote_node,
    did_repl_path_t **repl_sync_nodes)
{
	did_device_list_t *old_inst, *old_inst2, *new_inst, *trans;
	did_subpath_t *subp;
	char local_node[MAXNAMELEN];
	int remove_only = 0;
	did_repl_path_t *pathp;
	int is_repl_path;
	did_repl_path_t *rtrans, *trans2, *tnext;
	int in_sync_list;
	uint_t	node_prefix_len = strlen(node_prefix);

	/* make sure the stale replica path is from this node */
	if (replica_is_local(device) == 0) {
		/* need to erase ':' from node_prefix */
		(void) strcpy(local_node, node_prefix);
		local_node[strlen(local_node) - 1] = '\0';
		clerror("Stale replica \"%s\".\n",
		    device->device_path_list->device_path);
		if (!CLDEVICE_CMD) {
			clmessage("Run 'scdidadm -T %s' on node %s.\n",
			    local_node, remote_node);
		} else {
			clmessage("Run 'cldevice replicate -S %s -D "
			    " %s'.\n", local_node, remote_node);
		}

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ESTATE;
		return (-1);
	}

	if (Verbose || !CLDEVICE_CMD) {
		clmessage("Removing stale replica pair for instance \"%d\".\n",
		    device->instance);
	}

	/*
	 * See if the original instance is available.  If not, the user will
	 * need to run scgdevs to add the new split path, all we will do is
	 * remove a path.
	 */
	if (did_getinst(&new_inst, device->orig_inst) != -1) {
		clwarning("Old instance \"%d\" has been reallocated.\n");
		if (!CLDEVICE_CMD) {
			clwarning("To complete replication update, run '%s'.\n",
			    "/usr/cluster/bin/scgdevs");
		} else {
			clwarning("To complete replication update, run '%s'.\n",
			    "/usr/cluster/bin/cldevice populate");
		}
		remove_only = 1;
	}

	/*
	 * Grab the existing, replicated instance, we'll grab 2 copies to
	 * facilitate splitting it into 2 different instances.
	 */
	/* we will remove the replica device paths from this one */
	if (did_getinst(&old_inst, device->instance) == -1) {
		clerror("Cannot load instance \"%d\".\n", device->instance);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (-1);
	}
	/* we will remove the non-replica device paths from this one */
	if (did_getinst(&old_inst2, device->instance) == -1) {
		clerror("Cannot load instance \"%d\".\n", device->instance);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (-1);
	}

	/* update DID driver */
	/* remove the old instance from the driver */
	if (rm_driver_inst(&(old_inst->instance), 1) != 0)
		return (-1);

	/* update did_instances table */
	/* remove replicated path(s) from existing instance */
	for (pathp = device->device_path_list; pathp; pathp = pathp->next)
		for (subp = old_inst->subpath_listp; subp; subp = subp->next)
			if (strcmp(pathp->device_path,
			    subp->device_path) == 0) {
				if (delete_device(subp)) {
					clerror("Unable to delete replicated "
					    "path \"%s\".\n",
					    subp->device_path);

					if (CLDEVICE_CMD && !exit_code)
						exit_code = CL_EINTERNAL;
					return (-1);
				}
				break;
			}
	if (did_saveinst(old_inst, 0)) {
		clerror("Cannot save instance \"%d\".\n", old_inst->instance);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (-1);
	}

	/* update replicated_devices table */
	if (did_rm_repl_entry(device->instance) != 0) {
		clerror("Unable to remove entry for DID instance \"%d\" from "
		    "%s CCR table.\n", device->instance, "replicated_devices");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;

		return (-1);
	}
	/* remove from dpm */
	for (trans = instlist; trans; trans = trans->next)
		if (trans->instance == device->instance) {
			trans->instance = device->orig_inst;
			break;
		}
	(void) scdpm_update_db(SCDPM_UPDATE_CLEANUP, instlist);

	if (remove_only == 1)
		return (2);

	/* remove non-replicated path(s) from existing instance */
	for (subp = old_inst2->subpath_listp; subp; subp = subp->next) {
		is_repl_path = 0;
		for (pathp = device->device_path_list; pathp;
		    pathp = pathp->next)
			if (strcmp(pathp->device_path,
			    subp->device_path) == 0) {
				is_repl_path = 1;
				break;
			}

		if (is_repl_path == 1)
			continue;

		if (delete_device(subp)) {
			clerror("Unable to delete replicated path \"%s\".\n",
			    subp->device_path);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return (-1);
		}
	}

	/* construct new, split instance */
	old_inst2->instance = device->orig_inst;
	did_strtodiskid(device->diskid, old_inst2);
	if (old_inst2->type)
		did_set_type(old_inst2->target_device_id, old_inst2->type);

	/* save instance to did_instances and update DID driver */
	if (did_saveinst(old_inst2, 1)) {
		clerror("Unable to save instance \"%d\".\n",
		    old_inst2->instance);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (-1);
	}
	update_driver_inst(old_inst2);
	init_instances();

	/* add to dpm */
	(void) scdpm_update_db(SCDPM_UPDATE_REGISTER, old_inst2);

	/*
	 * Keep track of any other nodes connected to this replica so we can
	 * make a remote call to sync things on them.
	 */
	for (rtrans = device->device_path_list; rtrans; rtrans = tnext) {
		tnext = rtrans->next;
		/* skip this node */
		if (strncmp(rtrans->device_path, node_prefix,
		    node_prefix_len) == 0) {
			free(rtrans);
			continue;
		}

		/* grab just the node name */
		(void) strtok(rtrans->device_path, ":");

		/* see if this is already on our list */
		in_sync_list = 0;
		for (trans2 = *repl_sync_nodes; trans2; trans2 = trans2->next)
			if (strcmp(rtrans->device_path,
			    trans2->device_path) == 0) {
				in_sync_list = 1;
				break;
			}
		if (in_sync_list == 1)
			continue;

		rtrans->next = *repl_sync_nodes;
		*repl_sync_nodes = rtrans;
	}

	return (1);
}

/*
 * Make sure all currently configured replicated devices are still valid
 * given the current replication configuration, that is, they are still replica
 * pairs and still have the same replication group.
 *
 * return values:
 *
 * -1 - error occurred, code should continue on and try to add any new replicas
 *  0 - no devices had to be unreplicated
 *  1 - devices had to be unreplicated, path successfully moved to old instance
 *  2 - devices had to be unreplicated, some old instances are currently in use
 */
static int
check_existing_repl_devices(char *inputfile, char *remote_node,
    did_repl_path_t **repl_sync_nodes)
{
	FILE *fp;
	char dev_group[MAXNAMELEN], pair[MAXNAMELEN], device[MAXPATHLEN];
	char dev_group2[MAXNAMELEN], pair2[MAXNAMELEN], device2[MAXPATHLEN];
	char lorr[MAXNAMELEN], lorr2[MAXNAMELEN];
	int src_dev, dest_dev;
	did_repl_list_t *trans, *prevp, *nextp;
	int had_to_unreplicate = 0;
	int no_input = 1;

	if ((fp = fopen(inputfile, "r")) == NULL) {
		clerror("Cannot open file \"%s\" - %s.\n", inputfile,
		    strerror(errno));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		return (-1);
	}

	while (fscanf(fp, "%s %s %s %s %s %s %s %s", dev_group, pair, lorr,
	    device, dev_group2, pair2, lorr2, device2) != EOF) {
		no_input = 0;
		if (strcmp(dev_group, dev_group2) != 0) {
			clerror("Invalid input on auto-remap: dev_groups do"
			    "not match.\n");
			(void) fprintf(stderr,
			    gettext("%s %s %s %s\n%s %s %s %s\n"), dev_group,
			    pair, lorr, device, dev_group2, pair2, lorr2,
			    device2);
			(void) fclose(fp);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return (-1);
		}
		if (strcmp(pair, pair2) != 0) {
			clerror("Invalid input on auto-remap: pair volumes"
			    "do not match.\n");
			(void) fprintf(stderr,
			    gettext("%s %s %s %s\n%s %s %s %s\n"), dev_group,
			    pair, lorr, device, dev_group2, pair2, lorr2,
			    device2);
			(void) fclose(fp);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return (-1);
		}
		if (strcmp(lorr, "L") != 0) {
			clerror("Invalid input on auto-remap: Device is "
			    "not local.\n");
			(void) fprintf(stderr,
			    gettext("%s %s %s %s\n"), dev_group, pair, lorr,
			    device);
			(void) fclose(fp);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return (-1);
		}
		if (strcmp(lorr2, "R") != 0) {
			clerror("Invalid input on auto-remap: Device is "
			    "not remote.\n");
			(void) fprintf(stderr,
			    gettext("%s %s %s %s\n"), dev_group, pair, lorr,
			    device);
			(void) fclose(fp);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return (-1);
		}

		/* remove slice if present */
		if (device[strlen(device) - 2] == 's')
			device[strlen(device) - 2] = '\0';
		src_dev = find_instance_by_path(device);
		if (src_dev == -1) {
			clerror("Unable to locate device \"%s\".\n", device);
			(void) fclose(fp);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return (-1);
		}

		/* remove slice if present */
		if (device2[strlen(device2) - 2] == 's')
			device2[strlen(device2) - 2] = '\0';
		dest_dev = find_instance_by_path(device2);
		if (dest_dev == -1) {
			clerror("Unable to locate device \"%s\".\n", device2);
			(void) fclose(fp);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return (-1);
		}

		/* mark this if it is already replicated and dev_groups match */
		if (src_dev == dest_dev)
			if ((trans = find_repl_match(src_dev)) != NULL)
				if (strcmp(trans->dev_group, dev_group) == 0)
					trans->remove = -1;
	}

	(void) fclose(fp);

	if (no_input == 1)
		/*
		 * There was no input, so we could not check, can happen if
		 * the horcm daemon is not running.
		 */
		return (0);

	for (trans = repllist; trans; trans = trans->next)
		if (trans->remove == 0) {
			had_to_unreplicate = unreplicate_device(trans,
			    remote_node, repl_sync_nodes);
		}

	/* cleanup repllist */
	if (had_to_unreplicate != 0) {
		/* remove devices from repllist */
		for (trans = repllist; trans; trans = nextp) {
			nextp = trans->next;
			if (trans->remove == 0) {
				if (trans == repllist)
					repllist = trans->next;
				else {
					/*lint -e530 */
					prevp->next = trans->next;
					/*lint +e530 */
				}
			} else
				prevp = trans;
		}
	}

	return (had_to_unreplicate);
}

/* check the existing replication configuration and add any new replica pairs */
static void
add_new_repl_devices(char *inputfile, did_repl_path_t **repl_sync_nodes)
{
	FILE *fp;
	char dev_group[MAXNAMELEN], pair[MAXNAMELEN], device[MAXPATHLEN];
	char dev_group2[MAXNAMELEN], pair2[MAXNAMELEN], device2[MAXPATHLEN];
	char lorr[MAXNAMELEN], lorr2[MAXNAMELEN];
	int src_dev, dest_dev;
	did_device_list_t *src_inst, *dest_inst;
	did_repl_list_t *replret = NULL;
	did_repl_path_t *trans, *trans2, *tnext;
	int in_sync_list;
	uint_t	node_prefix_len = strlen(node_prefix);

	if ((fp = fopen(inputfile, "r")) == NULL) {
		clerror("Cannot open file \"%s\" - %s.\n", inputfile,
		    strerror(errno));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		return;
	}

	while (fscanf(fp, "%s %s %s %s %s %s %s %s", dev_group, pair, lorr,
	    device, dev_group2, pair2, lorr2, device2) != EOF) {
		if (strcmp(dev_group, dev_group2) != 0) {
			clerror("Invalid input on auto-remap: dev_groups do"
			    "not match.\n");
			(void) fprintf(stderr,
			    gettext("%s %s %s %s\n%s %s %s %s\n"), dev_group,
			    pair, lorr, device, dev_group2, pair2, lorr2,
			    device2);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}
		if (strcmp(pair, pair2) != 0) {
			clerror("Invalid input on auto-remap: pair volumes"
			    "do not match.\n");
			(void) fprintf(stderr,
			    gettext("%s %s %s %s\n%s %s %s %s\n"), dev_group,
			    pair, lorr, device, dev_group2, pair2, lorr2,
			    device2);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}
		if (strcmp(lorr, "L") != 0) {
			clerror("Invalid input on auto-remap: Device is "
			    "not local.\n");
			(void) fprintf(stderr,
			    gettext("%s %s %s %s\n"), dev_group, pair, lorr,
			    device);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}
		if (strcmp(lorr2, "R") != 0) {
			clerror("Invalid input on auto-remap: Device is "
			    "not remote.\n");
			(void) fprintf(stderr,
			    gettext("%s %s %s %s\n"), dev_group, pair, lorr,
			    device);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}

		/* remove slice if present */
		if (device[strlen(device) - 2] == 's')
			device[strlen(device) - 2] = '\0';
		src_dev = find_instance_by_path(device);
		if (src_dev == -1) {
			clerror("Unable to locate device \"%s\".\n", device);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}

		/* remove slice if present */
		if (device2[strlen(device2) - 2] == 's')
			device2[strlen(device2) - 2] = '\0';
		dest_dev = find_instance_by_path(device2);
		if (dest_dev == -1) {
			clerror("Unable to locate device \"%s\".\n", device2);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}

		if (src_dev == dest_dev)
			/* this has already been remapped */
			continue;

		(void) printf("%s %s %s %s\n", dev_group, pair, lorr, device);
		(void) printf("%s %s %s %s\n", dev_group2, pair2, lorr2,
		    device2);

		if (did_getinst(&src_inst, src_dev) == -1) {
			clerror("Unable to get instance \"%d\".\n", src_dev);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}
		if (did_getinst(&dest_inst, dest_dev) == -1) {
			clerror("Unable to get instance \"%d\".\n", dest_dev);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}

		replret = did_remap_combine_inst(src_inst, dest_inst,
		    dev_group);

		/*
		 * Keep track of any other nodes connected to this replica so
		 * we can make a remote call to sync things on them.
		 */
		if (replret != NULL) {
			for (trans = replret->device_path_list; trans;
			    trans = tnext) {
				tnext = trans->next;
				/* skip this node */
				if (strncmp(trans->device_path, node_prefix,
				    node_prefix_len) == 0) {
					free(trans);
					continue;
				}

				/* grab just the node name */
				(void) strtok(trans->device_path, ":");

				/* see if this is already on our list */
				in_sync_list = 0;
				for (trans2 = *repl_sync_nodes; trans2;
				    trans2 = trans2->next)
					if (strcmp(trans->device_path,
					    trans2->device_path) == 0) {
						in_sync_list = 1;
						break;
					}
				if (in_sync_list == 1)
					continue;

				trans->next = *repl_sync_nodes;
				*repl_sync_nodes = trans;
			}
			free(replret);
		}
	}

	(void) fclose(fp);
}

/* remove the specified DID instance from the DID driver */
static int
rm_driver_inst(int *inst, int print_err)
{
	int fd;

	/* remove the instance from the driver */
	if ((fd = open("/dev/did/admin", O_RDONLY)) < 0) {
		clerror("Unable to open %s - %s.\n",
		    "/dev/did/admin");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EIO;
		return (-1);
	}

	if (ioctl(fd, IOCDID_RMINST, inst) != 0) {
		if ((print_err == 1) || (errno != ENXIO)) {
			clerror("Unable to remove driver instance \"%d\" "
			    "- %s.\n", *inst, strerror(errno));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EIO;
		}
		(void) close(fd);
		return (-1);
	}

	(void) close(fd);
	return (0);
}

/*
 * Combine one DID instance with another DID instance.  This is only allowed for
 * 2 DID instances which are replicas (controller-based replication) of each
 * other.  It is used to merge the local replica DID instance in with the remote
 * DID replica instance (we move the local replica so that we can easily update
 * the DID driver with the new information).
 */
static did_repl_list_t
*did_remap_combine_inst(did_device_list_t *src_inst,
    did_device_list_t *dest_inst, char *dev_group)
{
	did_subpath_t *sublistp;
	int src_dev = src_inst->instance;
	int dest_dev = dest_inst->instance;
	int retval;
	did_repl_list_t *data;
	did_device_list_t *trans;
	did_repl_path_t *newpath = NULL;
	int ds_removed;
	uint_t	node_prefix_len = strlen(node_prefix);
	int free_dev_group = 0;

	if (Verbose || !CLDEVICE_CMD) {
		clmessage("Combining device instance \"%d\" with \"%d\".\n",
		    src_dev, dest_dev);
	}

	/*
	 * Double check subpaths, if the instance exists, subpath should not
	 * be NULL, but check anyway to avoid possible core dump.
	 */
	if (dest_inst->subpath_listp == NULL) {
		clerror("Destination instance \"%d\" has no subpaths.\n",
		    dest_dev);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (NULL);
	}

	/* make sure destination dev does not have a local subpath */
	for (sublistp = dest_inst->subpath_listp; sublistp;
	    sublistp = sublistp->next) {
		if (strncmp(sublistp->device_path, node_prefix,
		    node_prefix_len) == 0) {
			clerror("Cannot set up device replication, destination "
			    "device \"%d\" has a path on this node.\n",
			    dest_dev);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EOP;
			return (NULL);
		}
	}

	if (dev_group == NULL) {
		/*
		 * This is a manual remapping, make sure instances are replicas
		 * of each other.  dev_group needs to be freed in this function.
		 */
		dev_group = check_dev_group(src_inst, dest_inst);
		free_dev_group = 1;
		if (dev_group == NULL) {
			clerror("Device \"%d\" and \"%d\" are not paired "
			    "replicas.\n", src_dev, dest_dev);
			clerror("Cannot set up device replication.\n");

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EOP;
			return (NULL);
		}
	}

	/* create repl struct and add to repllist */
	data = (did_repl_list_t *)malloc(sizeof (*data));
	if (data == NULL) {
		print_error("did_remap_combine_inst", gettext(
		    "Can't malloc memory.\n"));

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ENOMEM;
	}
	data->diskid = NULL;
	if (did_diskidtostr(src_inst->diskid, src_inst->diskid_len,
	    &(data->diskid))) {
		clerror("Unable to construct disk ID information for device "
		    "\"%d\".\n", dest_dev);
		if (free_dev_group == 1)
			free(dev_group);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (NULL);
	}
	data->instance = dest_dev;
	data->orig_inst = src_dev;
	(void) strcpy(data->repl_type, repl_type);
	(void) strcpy(data->dev_group, dev_group);
	/* we are done with dev_group */
	if (free_dev_group == 1)
		free(dev_group);

	data->device_path_list = NULL;
	for (sublistp = src_inst->subpath_listp; sublistp;
	    sublistp = sublistp->next) {
		newpath = (did_repl_path_t *)malloc(sizeof (*newpath));
		if (newpath == NULL) {
			print_error("did_remap_combine_inst", gettext(
			    "Can't malloc memory.\n"));

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_ENOMEM;
		}
		(void) strcpy(newpath->device_path, sublistp->device_path);
		newpath->next = data->device_path_list;
		data->device_path_list = newpath;
	}

	data->next = repllist;
	repllist = data;

	/* find end of the destination subpath list, then add source subpaths */
	/*lint -e722 */
	for (sublistp = dest_inst->subpath_listp; sublistp->next;
	    sublistp = sublistp->next);
	/*lint +e722 */
	sublistp->next = src_inst->subpath_listp;

	/* remove the old instance from the driver */
	if (rm_driver_inst(&src_dev, 1) != 0)
		return (NULL);

	/* save destination instance */
	update_driver_inst(dest_inst);

	init_instances();
	src_inst->instance = src_dev;

	/* mark this device as replicated */
	retval = did_add_repl_entry(data);
	if (retval == -2) {
		clwarning("Destination instance \"%d\" is already "
		    "replicated.\n", dest_dev);
		return (NULL);
	} else if (retval == -1) {
		clerror("Unable to update %s CCR table.\n",
		    "replicated_devices");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (NULL);
	}

	/* save the new, combined instance */
	if (did_saveinst(dest_inst, 0)) {
		(void) did_abortccrlock();
		clerror("Unable to save instance \"%d\".\n", dest_dev);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (NULL);
	}

	/* saving with a NULL subpath list will remove the instance */
	src_inst->subpath_listp = NULL;
	if (did_saveinst(src_inst, 0)) {
		(void) did_abortccrlock();
		clerror("Unable to save instance \"%d\".\n", src_dev);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return (NULL);
	}

	/* remove old rawdisk device group */
	(void) did_update_device_service(src_dev, &ds_removed);

	/* update dpm */
	for (trans = instlist; trans; trans = trans->next)
		if (trans->instance == src_dev)
			trans->instance = dest_dev;
	/* have to call both since we both add and remove devices */
	(void) scdpm_update_db(SCDPM_UPDATE_REGISTER, instlist);
	(void) scdpm_update_db(SCDPM_UPDATE_CLEANUP, instlist);

	return (data);
}

/*
 * This is used to move an existing DID instance to a new DID instance.  The
 * new DID instance must not exist.
 */
static void
did_remap_move_inst(did_device_list_t *inst, int dev, int do_ccr)
{
	did_subpath_t *tpath;
	int src_dev, dest_dev;
	char didpath[MAXPATHLEN];
	did_repl_list_t data, *replt;
	did_device_list_t *inst2, *trans;
	int ds_removed;

	if (do_ccr == 1) {
		src_dev = inst->instance;
		dest_dev = dev;
		/*
		 * Get a second copy of inst, since the device id for inst will
		 * be modified when it is uploaded to the driver if it is a
		 * replicated device, and we don't want to save the replicated
		 * device id in did_instances, it goes in replicated_devices.
		 * We will use inst for uploading to the driver, and inst for
		 * saving to the ccr.
		 */
		if (did_getinst(&inst2, src_dev) == -1) {
			clerror("Cannot load instance \"%d\".\n", src_dev);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return;
		}
	} else {
		src_dev = dev;
		dest_dev = inst->instance;
	}

	if (do_ccr == 1 && !CLDEVICE_CMD) {
		clmessage("Moving instance \"%d\" to \"%d\".\n", src_dev,
		    dest_dev);
	}

	/* update instlist */
	for (trans = instlist; trans; trans = trans->next)
		if (trans->instance == src_dev) {
			trans->instance = dest_dev;
			break;
		}

	/* remove the old instance from the driver */
	if (rm_driver_inst(&src_dev, do_ccr) != 0)
		return;

	/*
	 * Update repllist if this is a replicated device so we can match the
	 * replicated devid.
	 */
	if ((replt = find_repl_match(src_dev)) != NULL)
		replt->instance = dest_dev;

	/* add new instance to driver */
	inst->instance = dest_dev;
	update_driver_inst(inst);
	init_instances();

	/* update dpm */
	/* have to call both since we both add and remove devices */
	(void) scdpm_update_db(SCDPM_UPDATE_REGISTER, trans);
	(void) scdpm_update_db(SCDPM_UPDATE_CLEANUP, instlist);

	/* remove old rawdisk device group */
	(void) did_update_device_service(src_dev, &ds_removed);

	if (do_ccr == 0)
		return;

	/* remove old instance from the ccr */
	/*lint -e644 */
	tpath = inst2->subpath_listp;
	/*lint +e644 */
	inst2->subpath_listp = NULL;
	/* saving with a NULL subpath list will remove the instance */
	if (did_saveinst(inst2, 0)) {
		(void) did_abortccrlock();
		clerror("DID re-mapping failed, cannot save instance \"%d\".\n",
		    src_dev);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return;
	}

	/* add the new instance to the ccr */
	inst2->instance = dest_dev;
	inst2->subpath_listp = tpath;
	if (did_saveinst(inst2, 1)) {
		(void) did_abortccrlock();
		clerror("Cannot save instance \"%d\" - DID instance \"%d\" no "
		    "longer mapped.\n", dest_dev, src_dev);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ENOENT;
		return;
	}

	/* update replicated_devices if this is a replicated device */
	(void) sprintf(didpath, "/dev/did/rdsk/d%ds2", src_dev);
	if (check_for_repl_device(didpath, &data) == DID_REPL_TRUE) {
		if (did_rm_repl_entry(src_dev) == 0) {
			data.instance = dest_dev;
			if (did_add_repl_entry(&data) != 0) {
				clerror("Unable to update %s CCR table.\n",
				    "replicated_devices");

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EINTERNAL;
				return;
			}
		} else {
			clerror("Unable to remove instance \"%d\" from %s CCR "
			    "table.\n", "replicated_devices", src_dev);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return;
		}
	}
}


static void
did_remap(int src_dev, int dest_dev, char *repl_dev_group)
{
	did_device_list_t *src_inst, *dest_inst;
	int src_exists = 1;
	int dest_exists = 1;
	char syncrepl_call[MAXNAMELEN];
	did_repl_list_t *replret = NULL;
	did_repl_path_t *trans;
	char *exec_node;
	uint_t	node_prefix_len = strlen(node_prefix);

	if (did_holdccrlock()) {
		clerror("Unable to lock DID CCR.\n");
		(void) did_releaseccrlock();

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return;
	}

	if (did_getinst(&src_inst, src_dev) == -1)
		src_exists = 0;

	if (did_getinst(&dest_inst, dest_dev) == -1)
		dest_exists = 0;

	/* don't allow quorum devices to be moved */
	if (src_exists == 1) {
		if (is_quorum(src_inst)) {
			clerror("Device instance \"%d\" is a quorum device.\n",
			    src_inst->instance);
			clwarning("Moving quorum devices is not allowed.\n");

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EOP;
			return;
		}
	}
	if (dest_exists == 1) {
		if (is_quorum(dest_inst)) {
			clerror("Device instance \"%d\" is a quorum device.\n",
			    dest_inst->instance);
			clwarning("Moving quorum devices is not allowed.\n");

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EOP;
			return;
		}
	}

	if ((src_exists == 1) && (dest_exists == 1)) {
		/* we are combining instances */
		replret = did_remap_combine_inst(src_inst, dest_inst,
		    repl_dev_group);
	} else if ((src_exists == 1) && (dest_exists == 0))
		/* we are moving an instance and need to update ccr */
		did_remap_move_inst(src_inst, dest_dev, 1);
	else if ((src_exists == 0) && (dest_exists == 1))
		/*
		 * We are moving/combining an instance, other node updated
		 * the ccr.
		 */
		did_remap_move_inst(dest_inst, src_dev, 0);
	else {
		clerror("Could not locate device instance \"%d\".\n", src_dev);

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_ENOENT;
	}

	(void) did_releaseccrlock();

	if (replret != NULL) {
		for (trans = replret->device_path_list; trans;
		    trans = trans->next) {
			/* don't run on local node */
			if (strncmp(trans->device_path, node_prefix,
			    node_prefix_len) == 0)
				continue;
			exec_node = strtok(trans->device_path, ":");
			if (exec_node == NULL) {
				clerror("Could not extract node name from "
				    "\"%s\".\n", trans->device_path);

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EINTERNAL;
				continue;
			}
			(void) sprintf(syncrepl_call, "%s %d:%d",
			    MANUAL_SYNCREPL, src_dev, dest_dev);
			if (did_syncrepl(exec_node, syncrepl_call)) {
				clerror("Could not synchronize replication on "
				    "node %s.\n", exec_node);

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EINTERNAL;
			}
		}
		free(replret);
	}
}

static void
did_auto_remap(char *remote_node, char *fmtfile)
{
	char *prog[6];
	char tcdev_out[MAXPATHLEN];
	int check_retval;
	char syncrepl_call[MAXNAMELEN];
	did_repl_path_t *repl_sync_nodes = NULL;
	did_repl_path_t *trans;

	if (strncmp(remote_node, node_prefix, strlen(node_prefix) - 1) == 0) {
		/* local host was specified, not remote */
		clerror("Invalid remote replication host.\n");

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINVAL;
		return;
	}

	if (Verbose || !CLDEVICE_CMD) {
		clmessage("Remapping instances for devices replicated with "
		    "%s...\n", remote_node);
	}

	if (did_holdccrlock()) {
		clerror("Cannot lock DID CCR.\n");
		(void) did_releaseccrlock();

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return;
	}

	/* get a unique, temp file name */
	(void) tmpnam(&tcdev_out[0]);

	/* Build the command string */
	prog[0] = get_repl_devs_cmd;
	prog[1] = "-f";
	prog[2] = tcdev_out;
	prog[3] = "-r";
	prog[4] = remote_node;
	prog[5] = NULL;

	/* exec GET_DEV command */
	switch (exec_command(prog)) {

	case 0:
	case 2:
		break;
	case 1:
		(void) did_releaseccrlock();
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return;
	case 3:
		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EOP;
		return;
	default:
		(void) did_releaseccrlock();
		return;
	}

	check_retval = check_existing_repl_devices(tcdev_out, remote_node,
	    &repl_sync_nodes);
	if (check_retval == 1) {
		/*
		 * Commit the unreplication changes to the ccr by releasing and
		 * then re-acquiring the CCR lock.
		 */
		(void) did_releaseccrlock();
		if (did_holdccrlock()) {
			clerror("Cannot lock DID CCR.\n");
			(void) did_releaseccrlock();

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return;
		}

		(void) did_freeinstlist(instlist);
		if (did_getinstlist(&instlist, 1, typelist, fmtfile)) {
			clerror("Could not load DID instance list.\n");
			(void) did_releaseccrlock();

			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_EINTERNAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}
	} else if (check_retval == 2) {
		/* user has to run scgdevs, then re-run scdidadm -T */
		(void) did_releaseccrlock();
		return;
	}

	add_new_repl_devices(tcdev_out, &repl_sync_nodes);

	(void) did_releaseccrlock();

	for (trans = repl_sync_nodes; trans; trans = trans->next) {
		(void) sprintf(syncrepl_call, "%s %d", AUTO_SYNCREPL,
		    check_retval);
		if (did_syncrepl(trans->device_path, syncrepl_call)) {
			clerror("Could not synchronize replication on node "
			    "%s.\n", trans->device_path);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
		}
	}
}

/*
 * A remote call from 'scdidadm -T' on another node to sync up the replication
 * information for replicas shared with this node.
 */
static void
did_auto_sync_remap(int unreplicate) {
	did_repl_list_t *rptr;
	did_device_list_t *dinst;
	int inst, count, found_local_path;
	int fd;
	did_subpath_t *trans;
	did_init_t *didinitp = (did_init_t *)malloc(sizeof (*didinitp));
	uint_t	node_prefix_len = strlen(node_prefix);

	if (did_holdccrlock()) {
		clerror("Unable to lock CCR.\n");
		(void) did_releaseccrlock();

		if (CLDEVICE_CMD && !exit_code)
			exit_code = CL_EINTERNAL;
		return;
	}

	/* replicate - make sure all replicas are correctly in the driver */
	for (rptr = repllist; rptr; rptr = rptr->next) {
		if (replica_is_local(rptr) != 1)
			continue;
		if (did_getinst(&dinst, rptr->instance) != 0) {
			clerror("Unable to load instance \"%d\".\n",
			    rptr->instance);

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			continue;
		}

		/*
		 * The first step of moving (removing from driver) will fail if
		 * this replica is already in sync.
		 */
		did_remap_move_inst(dinst, rptr->orig_inst, 0);
	}

	if (unreplicate == 0) {
		(void) did_releaseccrlock();
		return;
	}

	/* unreplicate  - make sure everything in the driver is still in ccr */
	if (didinitp == NULL) {
		print_error("get_kernel_instances", gettext(
		    "Can't malloc memory.\n"));
		(void) did_releaseccrlock();

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	didinitp->device_id = (ddi_devid_t)malloc(MAX_DEVIDLEN);
	if (didinitp->device_id == NULL) {
		print_error("get_kernel_instances", gettext(
		    "Can't malloc memory.\n"));
		(void) did_releaseccrlock();

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_ENOMEM;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	if ((fd = open("/dev/did/admin", O_RDONLY)) < 0) {
		clerror("Cannot open %s - %s.\n",
		    "/dev/did/admin",
		    strerror(errno));
		(void) did_releaseccrlock();

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_EIO;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}
	if (ioctl(fd, IOCDID_INSTCNT, &inst) == 0 && inst > 0) {
		int		testinst;

		testinst = 1;
		while (inst) {
			(void) memset(didinitp->device_id, 0, MAX_DEVIDLEN);
			count = testinst;
			if (ioctl(fd, IOCDID_PATHCNT, &count) != 0) {
				clerror("ioctl(%s) error - %s.\n",
				    "IOCDID_PATHCNT", strerror(errno));

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EIO;
				testinst++;
				inst--;
				continue;
			} else if (!count) {
				testinst++;
				continue;
			}
			inst--;
			/* see if this instance is configured for this node */
			if (did_getinst(&dinst, testinst) != 0) {
				clerror("Unable to load instance \"%d\".\n",
				    testinst);

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EINTERNAL;
				testinst++;
				continue;
			}
			found_local_path = 0;
			for (trans = dinst->subpath_listp; trans;
			    trans = trans->next) {
				if (strncmp(trans->device_path, node_prefix,
				    node_prefix_len) == 0) {
					found_local_path = 1;
					break;
				}
			}
			if (found_local_path == 1) {
				testinst++;
				continue;
			}

			if (ioctl(fd, IOCDID_RMINST, &testinst) != 0) {
				clerror("Unable to remove stale driver "
				    "instance \"%d\" - %s.\n", testinst,
				    strerror(errno));

				if (CLDEVICE_CMD && !exit_code)
					exit_code = CL_EIO;
			}
			testinst++;
		}
	}
	(void) close(fd);

	(void) did_releaseccrlock();

	/*
	 * add any new devices from unreplication
	 * Zero is passed to load_instances so that did reloader is not
	 * called for this case.
	 */
	load_instances(0);
	init_instances();
}

/* undocumented interface for backing out replication changes */
static void
did_manual_unreplicate(int dev)
{
	did_repl_list_t *replp;
	did_repl_path_t *repl_sync_nodes = NULL;
	did_repl_path_t *pathp;
	char syncrepl_call[MAXNAMELEN];

	if (dev < 0) {
		clerror("Invalid DID instance number specified.\n");
		usage("scdidadm");

		if (CLDEVICE_CMD) {
			if (!exit_code)
				exit_code = CL_EINVAL;
			exit(exit_code);
		} else {
			exit_code = 1;
			exit(1);
		}
	}

	if (dev > 0) {
		for (replp = repllist; replp; replp = replp->next)
			if (replp->instance == dev)
				break;

		if (replp == NULL) {
			clerror("Device \"%d\" is not a replicated device - "
			    "cannot unreplicate.\n", dev);

			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_EINVAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}

		if (replica_is_local(replp) == 0) {
			clerror("Replica \"%d\" is not local to this node - "
			    "cannot unreplicate.\n", dev);

			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_EOP;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}

		if (did_holdccrlock()) {
			clerror("Unable to lock DID CCR.\n");
			(void) did_releaseccrlock();

			if (CLDEVICE_CMD && !exit_code)
				exit_code = CL_EINTERNAL;
			return;
		}

		if (unreplicate_device(replp, NULL, &repl_sync_nodes) < 0) {
			clerror("Failed to unreplicate \"%d\".\n", dev);

			if (CLDEVICE_CMD) {
				if (!exit_code)
					exit_code = CL_EINTERNAL;
				exit(exit_code);
			} else {
				exit_code = 1;
				exit(1);
			}
		}

		(void) did_releaseccrlock();

		for (pathp = repl_sync_nodes; pathp; pathp = pathp->next) {
			(void) sprintf(syncrepl_call, "%s %d", AUTO_SYNCREPL,
			    1);
			if (did_syncrepl(pathp->device_path, syncrepl_call)) {
				clerror("Could not synchronize replication on "
				    "node %s.\n", pathp->device_path);

				if (CLDEVICE_CMD) {
					if (!exit_code)
						exit_code = CL_EINTERNAL;
					exit(exit_code);
				} else {
					exit_code = 1;
					exit(1);
				}
			}
		}
	}

	if (dev == 0) {
		(void) printf("unreplicating all local replicas\n");
		for (replp = repllist; replp; replp = replp->next)
			did_manual_unreplicate(replp->instance);
	}
}
