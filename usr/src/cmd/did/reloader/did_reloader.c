/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)did_reloader.c 1.7     08/12/19 SMI"

#define	    __EXTENSIONS__
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <strings.h>
#include <syslog.h>
#include <errno.h>
#include <libsysevent.h>
#include <pthread.h>
#include <sys/nvpair.h>
#include <sys/sunddi.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sysevent/dr.h>
#include <sys/sysevent/eventdefs.h>
#include <sys/sysevent_impl.h>
#include <sys/rgm/scutils.h>
#include <sys/wait.h>
#include <string.h>
#include "libdid.h"
#include "rldr_linklist.h"
#include "did_threads.h"

/* Tag used in syslog messages from this daemon */
#define	RELOADER    "reloader"

#define	PID_SZ		10
#define	PING_INTERVAL	30
/*
 * didadm options
 *   -a reload the did driver for the given set of disk paths
 */
#define	DIDADM_CMD    "/usr/sbin/didadm -a"

static rldr_devlist_t *all_devices = NULL;
static rldr_devlist_t *success_list = NULL;
void query_device(void *arg);

/*
 * This is the signal handler for the DID reloader daemon. Currently it
 * exits after receiving SIGHUP.
 */
void
signal_handler(int sig)
{
	if (sig == SIGHUP) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon was asked to exit by a SIGHUP
		 * signal.
		 * @user_action
		 * This is a debug message, no user action is necessary.
		 */
		did_logmsg(LOG_DEBUG, RELOADER,
			"DID reloader exiting due to hangup signal");
		exit(0);
	}
}


/*
 * This function opens a file, stores the process id of the daemon and
 * locks it. The process id stored in the lock file can be used by other
 * processes to send a signal to the daemon. scgdevs sends SIGHUP to
 * this process this way.
 */
void
daemonize()
{
	int lfp, pidLen;
	char str[PID_SZ];

	make_daemon();

	umask(027); /* set newly created file permissions */
	chdir(RUNNING_DIR); /* change running directory */
	lfp = open(LOCK_FILE, O_RDWR|O_CREAT, 0640);
	if (lfp < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon cannot be started because
		 * the file /var/run/did_reloader.lock cannot be
		 * created.
		 * @user_action
		 * Check file and directory permissions.
		 */
	    did_logmsg(LOG_ERR, RELOADER,
		    "Unable to create the lock file for DID reloader");
	    exit(EPERM); /* can not open */
	}
	if (lockf(lfp, F_TLOCK, 0) < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon is started again, when there is a
		 * process that already holds the lock to file
		 * /var/run/did_reloader.lock.
		 * @user_action
		 * This is a informational message, no user action is
		 * necessary.
		 */
	    did_logmsg(LOG_INFO, RELOADER,
		    "There is already an instance of DID reloader running.\n");
	    exit(0); /* can not lock */
	}

	snprintf(str, PID_SZ, "%d\n", getpid());
	pidLen = strlen(str);
	if (write(lfp, str, pidLen) != pidLen) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon cannot be started because
		 * the file /var/run/did_reloader.lock cannot be
		 * written to.
		 * @user_action
		 * Check file and directory permissions.
		 */
	    did_logmsg(LOG_ERR, RELOADER,
		    "DID Reloader cannot write to lock file ");
	    exit(EPERM); /* can not open */
	}
	(void) fsync(lfp);
	signal(SIGHUP, signal_handler);
}

/*
 * This function calls the didadm command with a special internal
 * option(didadm -a) to load the DID drivers for list of devices.
 * A link list of valid device path is passed into the function.
 */
int
update_did_drv(rldr_devlist_t *xSuccessList)
{
	FILE *p;
	int ret = -1;
	iscsi_dev *dev = xSuccessList->head;

	if ((p = popen(DIDADM_CMD, "w")) == NULL) {
	    return (ret);
	}
	while (dev->next != dev->next->next) {
	    if (dev->next->dev_path != NULL) {
		fprintf(p, "%s\n", dev->next->dev_path);
	    }
	    dev = dev->next;
	}
	ret = pclose(p);
	if (ret != -1) {
	/*
	 * If pclose succeeds, returns the exit status
	 * of the command that was executed. Returns -1 in all other
	 * cases.
	 */
	    if (WIFEXITED(ret)) {
		return (WEXITSTATUS((uint_t)ret));
	    } else {
		ret = -1;
	    }
	}
	return (ret);
}

/*
 * This function is called by every thread that tries to tickle the
 * device. This function determines if a particular device is ready
 * for the DID driver to be loaded on it.
 */
void
query_device(void *arg)
{
	int fd;
	struct  stat    st;
	char *path;
	iscsi_dev *device;
	char *stripped_path, *lasts;

	device = (iscsi_dev *)arg;
	path = device->dev_path;
	/*
	 * path should not be null. If it is ignore it and
	 * it will get removed
	 */

	if (path == NULL) {
	    return;
	}

	/*
	 * the stat call tickles the device and causes iscsi devices
	 * to be attached to the host.
	 *
	 * Remove the white spaces at the
	 * end of the string. If it is not removed stat() will fail.
	 */
	stripped_path = (char *)strtok_r(path, " \t\n", &lasts);
	if (stripped_path != NULL) {
	    if (stat(stripped_path, &st) == 0) {
		device->attach_complete = 1;
	    } else {
		device->attach_complete = 0;
	    }
	}
}

/*
 * This function is the function in the main loop of the daemon. This
 * checks if a given list of devices are ready and accessible for the
 * DID drivers to be loaded on them. Every device check is done in a
 * seperate thread.
 */
int
device_monitor()
{
	iscsi_dev *dev = all_devices->head;
	iscsi_dev *next = NULL;

	/*
	 * Scatter the device attach into seperate threads and gather
	 * the results. The code assumes that a iSCSI device attach
	 * for a device is a expensive operation.
	 */
	while (dev->next != dev->next->next) {
	    addTasktoThreadPool(query_device, (void *)dev->next);
	    dev = dev->next;
	}
	wait4tasksinThreadPool();

	dev = all_devices->head;
	while (dev->next != dev->next->next) {
	    if (dev->next->attach_complete == 1) {
	/*
	 * move this device to the success list
	 */
		moveElementAfter(all_devices, dev, success_list);
	    } else {
		/* Go to the next element in the list */
		dev = dev->next;
	    }
	}
	/* Load the DID for all the devices that are now accessible */
	if (success_list->size > 0) {
	    if (update_did_drv(success_list) >= 0) {
		emptyRldrList(success_list);
	    } else {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon could not reload the DID
		 * drivers.
		 * @user_action
		 * Reload the DID drivers manually using scdidadm
		 * command.
		 */
		did_logmsg(LOG_ERR, RELOADER, "DID Driver Repair Failed");
		/*
		 * Move the device paths back to the all_devices list
		 * so that they can be tried again
		 */
		dev = success_list->head;
		while (dev->next != dev->next->next) {
		    moveElementAfter(success_list, dev, all_devices);
		    dev = dev->next;
		}
	    }
	}
	    /* No devices to monitor, stop the daemon */
	if (all_devices->size == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon completed successfully.
		 * @user_action
		 * This is a debug message, no user action is necessary.
		 */
	    did_logmsg(LOG_DEBUG, RELOADER, "DID Reloader is complete.");
	    return (0);
	}
	return (-1);
}

void
main(int argc, char* argv[])
{
	char buf[MAXPATHLEN];

	if (initRldrList(&all_devices) < 0) {
	    did_logmsg(LOG_ERR, RELOADER, "Not Enough memory");
	    goto cleanup;
	}
	if (initRldrList(&success_list) < 0) {
	    did_logmsg(LOG_ERR, RELOADER, "Not Enough memory");
	    goto cleanup;
	}
	while (fgets(buf, MAXPATHLEN, stdin) != NULL) {
	    if (insertAfter(all_devices, NULL, buf) < 0) {
		did_logmsg(LOG_ERR, RELOADER, "Not Enough memory");
		goto cleanup;
	    }
	}
	daemonize();
	if (initDIDthreadpool() < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The DID reloader daemon cannot initialise threads to use
		 * signal.
		 * @user_action
		 * Check the system limits for number of threads.
		 */
	    did_logmsg(LOG_ERR, RELOADER,
		    "DID Daemon Failed to initialise threads");
	    exit(EAGAIN);
	}
	while (1) {
	/*
	 * This time interval is arbitrary at this point. This may
	 * need fine tuning.
	 */
	    if (device_monitor() == 0) {
		break;
	    }
	    sleep(PING_INTERVAL);
	}
	finiDIDthreadpool();
cleanup:
	freeRldrList(success_list);
	freeRldrList(all_devices);
	exit(0);
}
