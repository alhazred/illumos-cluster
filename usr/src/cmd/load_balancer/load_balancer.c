/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)load_balancer.c	1.13	08/05/20 SMI"

/*
 * lbcmd(1M)
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <errno.h>
#include <scha_lb.h>
#include <locale.h>
#include <sys/clconf_int.h>
#include <rgm/sczones.h>

#define	GET_POLICY "-get_policy"
#define	GET_WEIGHTS "-get_weights"
#define	SET_WEIGHT "-set_weight"
#define	SET_WEIGHTS "-set_weights"
#define	GET_PERF_PARAMS "-get_perf_params"
#define	SET_PERF_PARAMS "-set_perf_params"
#define	SET_PERF_LOAD "-set_perf_load"
#define	SET_PARAMS "-set_params"

static void usage(char *name);
static void show_err(int err_num);
static char *cmd_name;

int
main(int argc, char **argv)
{
	int err_num = 0;
	int status = 0;
	char *resource;

	if ((cmd_name = (char *)rindex(argv[0], '/')) == NULL)
		cmd_name = argv[0];
	else
		cmd_name++;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (sc_zonescheck() != 0)
		return (1);

	if (argc < 3) {
		usage(cmd_name);
		exit(-1);
	}

	resource = argv[1];

	if (strcmp(argv[2], GET_POLICY) == 0) {
		int policy;
		err_num = scha_ssm_lb_get_policy_type(resource,
			&policy);
		if (err_num != 0) {
			show_err(err_num);
			status = -1;
		} else {
			switch (policy) {
			case LB_WEIGHTED:
				(void) printf(gettext("Weighted policy\n"));
				break;
			case LB_PERF:
				(void) printf(gettext("Perf monitor policy\n"));
				break;
			case LB_ST:
				(void) printf(gettext("Sticky policy\n"));
				break;
			case LB_SW:
				(void) printf(gettext(
				    "Sticky wildcard policy\n"));
				break;
			default:
				(void) printf(gettext("Unknown policy\n"));
				break;
			}
		}
	} else if (strcmp(argv[2], SET_WEIGHTS) == 0) {
		int i, num_nodes;
		struct weight_info weights;
		weights.num_nodes = NODEID_MAX + 1;
		num_nodes = argc - 2;

		weights.weights = (int *)malloc(sizeof (int)
			* (uint_t)weights.num_nodes);
		if (weights.weights == NULL) {
			show_err(ENOMEM);
			status = -1;
		}
		bzero(weights.weights, sizeof (weights.weights));
		for (i = 1; i < num_nodes; i++) {
			weights.weights[i] = atoi(argv[2+i]);
		}
		err_num = scha_ssm_lb_set_distribution(resource, &weights);
		if (err_num != 0) {
			show_err(err_num);
			status = -1;
		}
		free(weights.weights);
	} else if (strcmp(argv[2], GET_WEIGHTS) == 0) {
		struct dist_info *dist;
		err_num = scha_ssm_lb_get_distribution(resource, &dist);
		if (err_num != 0) {
			show_err(err_num);
			status = -1;
		} else {
			int i;
			for (i = 1; i < dist->num_nodes; i++) {
				(void) printf(gettext("%d\n"),
				    dist->weights[i]);
			}
			free(dist);
		}
	} else if (strcmp(argv[2], SET_WEIGHT) == 0) {
		if (argc != 5) {
			usage(cmd_name);
			status = -1;
		} else {
			int node, weight;
			node = atoi(argv[3]);
			weight = atoi(argv[4]);
			err_num = scha_ssm_lb_set_one_distribution(resource,
				node, weight);
			if (err_num != 0) {
				show_err(err_num);
				status = -1;
			}
		}
	} else if (strcmp(argv[2], GET_PERF_PARAMS) == 0) {
		struct perf_mon_parameters *params;
		err_num =
		    scha_ssm_lb_get_perf_mon_parameters(resource, &params);
		if (err_num != 0) {
			show_err(err_num);
			status = -1;
		} else {
			(void) printf(gettext("Frequency: %d\n"),
				params->update_frequency);
			(void) printf(gettext("Scale rate: %d\n"),
				params->scale_rate);
			(void) printf(gettext("Min weight: %d\n"),
				params->min_weight);
			(void) printf(gettext("Max weight: %d\n"),
				params->max_weight);
			(void) printf(gettext("Quiet width: %d\n"),
				params->quiet_width);
			(void) printf(gettext("Floor: %d\n"), params->floor);
		}
	} else if (argc == 4 && strcmp(argv[2], SET_PARAMS) == 0) {
		int policy;
		err_num = scha_ssm_lb_get_policy_type(resource, &policy);
		if (err_num != 0) {
			show_err(err_num);
		} else {
			void *buf;
			err_num = scha_ssm_lb_parse_lb_info(argv[3], policy,
				&buf);
			if (err_num == EINVAL) {
				(void) fprintf(stderr,
				    gettext("%s: parse error\n"), argv[3]);
			} else if (err_num != 0) {
				show_err(err_num);
			} else {
				err_num = scha_ssm_lb_set_lb_info(resource,
					policy, buf);
				if (err_num < 0) {
					if (err_num == EINVAL) {
						(void) fprintf(stderr,
						    gettext("%s: invalid\n"),
						    argv[3]);
					} else {
						show_err(err_num);
					}
				} else if (status > 0) {
					(void) fprintf(stderr,
					    gettext("Note: number of "
					    "weights unexpected\n"));
				}
				free(buf);
			}
		}
	} else if (strcmp(argv[2], SET_PERF_PARAMS) == 0) {
		if (argc != 9) {
			usage(cmd_name);
			status = -1;
		} else {
			struct perf_mon_parameters params;
			params.update_frequency = (uint_t)atoi(argv[3]);
			params.scale_rate = (uint_t)atoi(argv[4]);
			params.min_weight = (uint_t)atoi(argv[5]);
			params.max_weight = (uint_t)atoi(argv[6]);
			params.quiet_width = (uint_t)atoi(argv[7]);
			params.floor = (uint_t)atoi(argv[8]);

			err_num = scha_ssm_lb_set_perf_mon_parameters(resource,
				&params);
			if (err_num != 0) {
				show_err(err_num);
				status = -1;
			}
		}
	} else if (argc == 4 && strcmp(argv[2], SET_PERF_LOAD) == 0) {
		/* Get load from stdin */
		int node, load;
		node = atoi(argv[3]);
		while (1) {
			char buf[50];
			if (fgets(buf, sizeof (buf), stdin) == 0) {
				break;
			}
			load = atoi(buf);
			err_num = scha_ssm_lb_set_perf_mon_status(resource,
			    node, (uint_t)load);
			if (err_num != 0) {
				show_err(err_num);
				status = -1;
			}
		}
	} else if (argc == 5 && strcmp(argv[2], SET_PERF_LOAD) == 0) {
		int node, load;
		node = atoi(argv[3]);
		load = atoi(argv[4]);
		err_num = scha_ssm_lb_set_perf_mon_status(resource, node,
		    (uint_t)load);
		if (err_num != 0) {
			show_err(err_num);
			status = -1;
		}
	} else {
		usage(cmd_name);
		status = -1;
	}
	return (status);
}

void
usage(char *name)
{
	(void) fprintf(stderr,
		gettext("Usage: %s resource options, where options:\n"),
		name);
	(void) fprintf(stderr, gettext("%s\n"), GET_POLICY);
	(void) fprintf(stderr, gettext("%s\n"), GET_WEIGHTS);
	(void) fprintf(stderr, gettext("%s node weight\n"), SET_WEIGHT);
	(void) fprintf(stderr, gettext("%s weight-list\n"), SET_WEIGHTS);
	(void) fprintf(stderr, gettext("%s\n"), GET_PERF_PARAMS);
	(void) fprintf(stderr, gettext("%s param-string\n"), SET_PARAMS);
	(void) fprintf(stderr, gettext("%s "
		"freq scale min-weight max-weight quiet-width floor\n"),
		SET_PERF_PARAMS);
	(void) fprintf(stderr, gettext("%s node load\n"), SET_PERF_LOAD);
	(void) fprintf(stderr, gettext("%s node < load\n"), SET_PERF_LOAD);
}

void
show_err(int err_num)
{
	char *err_str;

	err_str = strerror(err_num);
	if (err_str != NULL)
		(void) fprintf(stderr, "%s: %s\n", cmd_name, err_str);
	else
		(void) fprintf(stderr,
		    gettext("%s: Unknown error %d\n"), cmd_name, err_num);
}
