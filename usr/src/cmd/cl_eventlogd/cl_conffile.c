/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_conffile.c 1.4     08/05/20 SMI"

#include <fcntl.h>
#include <malloc.h>
#include <strings.h>
#include <unistd.h>
#include <assert.h>

#include "cl_conffile.h"

#define	CL_CONFFILE_DEFAULT_ALLOCATION	64
#define	CL_CONFFILE_DEFAULT_ENTRIES	4


/*
 * This file parses the eventlog configuration file.
 *
 * The grammar of this configuration is :
 *
 * - lines beginning with # are comments
 * - other lines contain 6 fields separated by spaces and/or tabs. The last
 *   field is optional.
 * - a single field is a string without spaces and tabs.
 * - the value - for the first 4 fields is a wildcard.
 *
 * The fields are :
 *
 * - Class
 * - Subclass
 * - Vendor
 * - Publisher
 * - Plugin location
 * - Plugin parameters (optional)
 */

static int
readline(int fd, char **line)
{
	char c;
	char *result = NULL;
	int pos = 0;
	int size = CL_CONFFILE_DEFAULT_ALLOCATION;
	int beginning_of_line = 1;
	int err;

	(*line) = NULL;

	result = (char*)malloc(CL_CONFFILE_DEFAULT_ALLOCATION*sizeof (char));
	if (result == NULL) {
		return (1);
	}

	do {
		err = read(fd, &c, 1);

		if (err == 1) {
			switch (c) {
			case '\n':
				err = 2;
				break;

			case ' ':
			case '\t':
				/*
				 * If we are at the beginning of the line,
				 * skip it
				 */
				if (beginning_of_line) {
					break;
				}
				/* No break intentionally */

			default:
				beginning_of_line = 0;

				if ((pos+1) == size) {
					char *new_res = NULL;

					new_res = (char*) realloc(result,
					    size*2);
					if (new_res == NULL) {
						free(result);
						return (1);
					}
					result = new_res;
					size *= 2;
				}

				result[pos] = c;
				pos++;
			}
		}
	} while (err == 1);

	result[pos] = '\0';

	if (pos == 0) {
		free(result);
		result = NULL;
	}


	switch (err) {
	case 0 :
		/* EOF */
		if (result == NULL) {
			return (2);
		}
		break;

	case 2 :
		/* Normal case, end of line */
		break;

	default:
		/* An error on the read occured */
		if (result) {
			free(result);
			result = NULL;
		}
		return (1);
	}

	(*line) = result;
	return (0);
}

static int
parse_single_entry(int fd, int *nb_entries, int *nb_allocated,
    cl_conffile_entry_t **entries, boolean_t *entry_found)
{
	char *line = NULL;
	int err = 0;
	int size = 0;
	char *cur_line = NULL;
	int param_field_present;

	(*entry_found) = B_FALSE;

	err = readline(fd, &line);
	switch (err) {
	case 0:
		/* Normal case, continue */
		break;

	case 1:
		/* Error, return error */
		return (1);

	case 2 :
		/* Reached EOF */
		return (0);

	default:
		/* Shouldn't be here */
		assert(0);
		return (1);
	}

	if (line == NULL) {
		/* The line was made only by spaces */
		(*entry_found) = B_TRUE;
		return (0);
	}

	if (line[0] == '#') {
		/* This is a comment */
		free(line);
		line = NULL;
		(*entry_found) = B_TRUE;
		return (0);
	}

	if ((*nb_entries) == (*nb_allocated)) {
		cl_conffile_entry_t *new_entries = NULL;

		new_entries = (cl_conffile_entry_t*)realloc((*entries),
		    (*nb_allocated)*2*sizeof (cl_conffile_entry_t));
		if (new_entries == NULL) {
			free(line);
			return (1);
		}

		(*entries) = new_entries;
		(*nb_allocated) *= 2;
	}

	cur_line = line;

	(*entries)[*nb_entries].ev_class = cur_line;
	size = strcspn(cur_line, " \t");
	cur_line[size] = '\0';
	cur_line += size + 1 + strspn(cur_line+size+1, " \t");

	(*entries)[*nb_entries].ev_subclass = cur_line;
	size = strcspn(cur_line, " \t");
	cur_line[size] = '\0';
	cur_line += size + 1 + strspn(cur_line+size+1, " \t");

	(*entries)[*nb_entries].vendor = cur_line;
	size = strcspn(cur_line, " \t");
	cur_line[size] = '\0';
	cur_line += size + 1 + strspn(cur_line+size+1, " \t");

	(*entries)[*nb_entries].publisher = cur_line;
	size = strcspn(cur_line, " \t");
	cur_line[size] = '\0';
	cur_line += size + 1 + strspn(cur_line+size+1, " \t");

	(*entries)[*nb_entries].plugin_location = cur_line;
	size = strcspn(cur_line, " \t");
	if (size + strspn(cur_line+size+1, " \t") == strlen(cur_line)) {
		param_field_present = 0;
	} else {
		param_field_present = 1;
	}
	cur_line[size] = '\0';

	if (param_field_present) {
		cur_line += size + 1 + strspn(cur_line+size+1, " \t");
	} else {
		cur_line = NULL;
	}

	(*entries)[*nb_entries].plugin_parameter = cur_line;

	(*nb_entries)++;
	(*entry_found) = B_TRUE;

	return (0);
}

static int
parse_conf_file(int fd,
		int *nb_entries,
		cl_conffile_entry_t **entries)
{
	boolean_t entry_found = B_TRUE;
	int res = 0;
	int nb_allocated = 0;

	(*nb_entries) = 0;
	(*entries) = NULL;

	(*entries) = (cl_conffile_entry_t*)malloc(CL_CONFFILE_DEFAULT_ENTRIES *
	    sizeof (cl_conffile_entry_t));
	if ((*entries) == NULL) {
		return (1);
	}
	nb_allocated = CL_CONFFILE_DEFAULT_ENTRIES;

	entry_found = B_TRUE;
	while (entry_found) {
		res = parse_single_entry(fd, nb_entries, &nb_allocated,
		    entries, &entry_found);
		if (res) {
			cl_conffile_free((*nb_entries), (*entries));
			(*nb_entries) = 0;
			(*entries) = NULL;
			return (1);
		}
	}

	return (0);
}


/*
 * cl_conffile API implementation
 */

int
cl_conffile_parse(char *filename, int *nb_entries,
    cl_conffile_entry_t **entries)
{
	int fd = -1;
	int res = 0;

	fd = open(filename, O_RDONLY);
	if (fd == -1) {
		return (1);
	}

	*entries = NULL;
	res = parse_conf_file(fd, nb_entries, entries);

	close(fd);

	return (res);
}

/*
 * The whole line in the configuration file is stored in a single
 * memory area.
 *
 * eb_subclass, ... are only pointers within that memory chunk.
 *
 * To free the entry, there is therefore only a need to free the whole
 * region, pointed by the first field (ev_class).
 *
 * NOTE : you therefore HAVE TO use cl_conffile_free to free your memory
 * and not try to free it yourself.
 */

void
cl_conffile_free(int nb_entries, cl_conffile_entry_t *entries)
{
	int i;

	for (i = 0; i < nb_entries; i++) {
		if (entries[i].ev_class) {
			free(entries[i].ev_class);
			entries[i].ev_class = NULL;
		}

		if (entries[i].ev_subclass) {
			entries[i].ev_subclass = NULL;
		}

		if (entries[i].vendor) {
			entries[i].vendor = NULL;
		}

		if (entries[i].publisher) {
			entries[i].publisher = NULL;
		}

		if (entries[i].plugin_location) {
			entries[i].plugin_location = NULL;
		}

		if (entries[i].plugin_parameter) {
			entries[i].plugin_parameter = NULL;
		}
	}

	free(entries);
}
