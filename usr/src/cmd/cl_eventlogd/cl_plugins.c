/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_plugins.c 1.5     08/05/20 SMI"

#include <dlfcn.h>
#include <link.h>
#include <strings.h>
#include <malloc.h>
#include <pthread.h>
#include <syslog.h>

#include "cl_plugins.h"
#include "cl_conffile.h"
#include "cl_eventlog_plugin.h"

/*
 * Type declarations
 */

typedef cl_eventlog_plugin_ops_t *(*get_plugin_ops_t)();

typedef struct {
	void *dl_handle;
	cl_eventlog_plugin_ops_t *plugin_ops;
	cl_conffile_entry_t *conffile_entry;
	int initialized;
	void *cookie;
	pthread_mutex_t mutex;
} cl_plugin_t;

typedef struct {
	int nb_plugins;
	cl_conffile_entry_t *conffile_entries;
	cl_plugin_t *plugins;
} cl_plugin_global_t;

/*
 * Global datas
 */

static cl_plugin_global_t *global_datas = NULL;
/* When NULL, then the module is not initialized */

/*
 * Private routines
 */

static int
simple_field_match(char *conffile_field, char *event_field)
{
	char *simple_value = NULL;
	char *conffile_cpy = NULL;
	int res;

/*
 * - is a wildcard character.
 *
 * This is the syntax of the Solaris sysconfeventd.conf file
 */

	if (strcmp(conffile_field, "-") == 0) {
		return (1);
	}

	if (strchr(conffile_field, ',') == NULL) {
		/*
		 * The config file contains a single field.
		 * Simply compare the strings
		 */
		res = strcmp(conffile_field, event_field) == 0 ? 1 : 0;
		return (res);
	}

	/*
	 * The conffile field contains several values.
	 * Compare against all of them ...
	 */

	res = 0;

	conffile_cpy = strdup(conffile_field);
	if (!conffile_cpy) {
		return (0);
	}

	simple_value = strtok(conffile_cpy, ",");

	while (simple_value) {
		if (strcmp(simple_value, event_field) == 0) {
			res = 1;
			break;
		}
		simple_value = strtok(NULL, ",");
	}

	free(conffile_cpy);

	return (res);
}

static int
event_matching(sysevent_t *event, cl_conffile_entry_t *conf)
{
	char *vendor_name;
	char *pub_name;

	if (!simple_field_match(conf->ev_class,
				    sysevent_get_class_name(event))) {
		return (0);
	}

	if (!simple_field_match(conf->ev_subclass,
				    sysevent_get_subclass_name(event))) {
		return (0);
	}

	vendor_name = sysevent_get_vendor_name(event);
	if (!simple_field_match(conf->vendor, vendor_name)) {
		free(vendor_name);
		return (0);
	}
	free(vendor_name);

	pub_name = sysevent_get_pub_name(event);
	if (!simple_field_match(conf->publisher, pub_name)) {
		free(pub_name);
		return (0);
	}
	free(pub_name);

	/* All the parameters matched */

	return (1);
}

static int
plugins_init()
{
	int i;
	int err;
	cl_plugin_t *cur_plugin = NULL;

	err = 0;
	for (i = 0; i < global_datas->nb_plugins; i++) {
		cur_plugin = global_datas->plugins + i;

		err = cur_plugin->plugin_ops->init(
		    cur_plugin->conffile_entry->plugin_parameter,
		    &(cur_plugin->cookie));
		if (err) {
			/*
			 * destroy callbacks will be called in
			 * cl_plugins_destroy
			 */
			return (err);
		}
		cur_plugin->initialized = 1;
	}

	return (0);
}

/*
 * Implementation of the cl_plugins API routines
 */

int
cl_plugins_init(char *cl_conffile)
{
	int err;
	int i;
	cl_plugin_t *cur_plugin = NULL;
	get_plugin_ops_t plugin_getops;

	if (global_datas) {
		return (1);
	}

	global_datas = (cl_plugin_global_t*)malloc(sizeof (cl_plugin_global_t));
	if (!global_datas) {
		return (1);
	}

	err = cl_conffile_parse(cl_conffile,
				&global_datas->nb_plugins,
				&global_datas->conffile_entries);
	if (err) {
		free(global_datas);
		global_datas = NULL;
		return (1);
	}

	global_datas->plugins = (cl_plugin_t*)malloc(global_datas->nb_plugins
	    *sizeof (cl_plugin_t));
	if (!global_datas->plugins) {
		cl_conffile_free(global_datas->nb_plugins,
		    global_datas->conffile_entries);
		free(global_datas);
		global_datas = NULL;
		return (1);
	}
	bzero(global_datas->plugins,
	    global_datas->nb_plugins*sizeof (cl_plugin_t));

	for (i = 0; i < global_datas->nb_plugins; i++) {
		err = 0;

		cur_plugin = global_datas->plugins + i;
		cur_plugin->initialized = 0;
		cur_plugin->cookie = NULL;
		cur_plugin->conffile_entry = global_datas->conffile_entries + i;
		cur_plugin->dl_handle = NULL;

		err = pthread_mutex_init(&(cur_plugin->mutex), NULL);

		if (!err) {
			cur_plugin->dl_handle = dlopen(
				    cur_plugin->conffile_entry->plugin_location,
				    RTLD_NOW);
			plugin_getops = NULL;
			if (cur_plugin->dl_handle) {
				plugin_getops = (get_plugin_ops_t)dlsym(
					    cur_plugin->dl_handle,
					    "get_plugin_ops");
				if (!plugin_getops) {
				dlclose(cur_plugin->dl_handle);
					cur_plugin->dl_handle = NULL;
				}
			}
			if (plugin_getops) {
				cur_plugin->plugin_ops = (*plugin_getops)();
				if (!cur_plugin->plugin_ops) {
					dlclose(cur_plugin->dl_handle);
					cur_plugin->dl_handle = NULL;
				}
			}
		}

		if (!cur_plugin->dl_handle) {
			/* Error. Close all the dl_handle and return */
			int j;

			for (j = 0; j < i; j++) {
				(int)pthread_mutex_destroy(
				    &(global_datas->plugins[j].mutex));
				dlclose(global_datas->plugins[j].dl_handle);
			}
			if (!err) {
				(int)pthread_mutex_destroy(
				    &(cur_plugin->mutex));
			}
			cl_conffile_free(global_datas->nb_plugins,
			    global_datas->conffile_entries);
			free(global_datas->plugins);
			free(global_datas);
			global_datas = NULL;
			return (1);
		}
	}

	err = plugins_init();
	if (err) {
		cl_plugins_destroy();
		return (err);
	}

	return (0);
}

void
cl_plugins_destroy()
{
	int i;
	cl_plugin_t *cur_plugin = NULL;

	if (global_datas) {
		for (i = 0; i < global_datas->nb_plugins; i++) {
			cur_plugin = global_datas->plugins + i;
			if (cur_plugin->initialized) {
				pthread_mutex_lock(&(cur_plugin->mutex));
				cur_plugin->plugin_ops->
				    destroy(cur_plugin->cookie);
				cur_plugin->cookie = NULL;
				cur_plugin->initialized = 0;
			}
			pthread_mutex_destroy(&(cur_plugin->mutex));
			dlclose(global_datas->plugins[i].dl_handle);
		}
		free(global_datas->plugins);
		cl_conffile_free(global_datas->nb_plugins,
		    global_datas->conffile_entries);
		free(global_datas);
		global_datas = NULL;
	}
}

int
cl_plugins_dispatch(sysevent_t *event)
{
	int i, err, tmp_err;
	cl_plugin_t *cur_plugin = NULL;

	if (!global_datas) {
		/* The module is not initialized */
		return (1);
	}

	err = 0;

	for (i = 0; i < global_datas->nb_plugins; i++) {
		cur_plugin = global_datas->plugins + i;
		if (event_matching(event, cur_plugin->conffile_entry)) {
			/* Call the plugin routine */
			pthread_mutex_lock(&(cur_plugin->mutex));
			tmp_err = cur_plugin->plugin_ops-> event_log(event,
			    cur_plugin->cookie);
			if (tmp_err != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_ERR,
			"cl_plugins_dispatch failed for plugin %s [%d]\n",
			cur_plugin->conffile_entry->plugin_location, err);

				if (err == 0) {
					err = tmp_err;
				}
			}

			pthread_mutex_unlock(&(cur_plugin->mutex));
		}
	}

	return (err);
}
