/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)commandlog_plugin.c	1.5	08/05/20 SMI"

/*
 * This file contains the implementation of the Command Logging plugin
 *
 * This plugin simply writes the events in a text format to
 * /var/cluster/logs/commandlog.
 *
 * This plugin can also be taken as an example to write other plugins for
 * the event logging framework.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <libsysevent.h>
#include <libnvpair.h>
#include <string.h>
#include <sys/cl_events.h>
#include <syslog.h>
#include <sys/time.h>
#include <fcntl.h>
#include <stdlib.h>

#include "cl_eventlogd.h"
#include "cl_eventlog_plugin.h"

#define	CL_CMDLOG_FILE	"/var/cluster/logs/commandlog"
#define	CMD_START "START"
#define	CMD_END "END"

/*
 * API declaration
 */

static int  commandlog_plugin_init(char *plugin_parameter, void **cookie);
static void commandlog_plugin_destroy(void *cookie);
static int  commandlog_plugin_event_log(sysevent_t *ev, void *cookie);

cl_eventlog_plugin_ops_t commandlog_plugin_ops = {
	commandlog_plugin_init,
	commandlog_plugin_destroy,
	commandlog_plugin_event_log
};

cl_eventlog_plugin_ops_t *
get_plugin_ops()
{
	return (&commandlog_plugin_ops);
}

/*
 * Type declarations
 */

typedef struct {
	FILE *log;
} commandlog_plugin_data_t;

/*
 * Public API routines implementation
 */

/*
 *  commandlog_plugin_init()
 *  Open cluster Command log file for writing.
 *
 *  The plugin parameter is ignored for now
 */

static int
commandlog_plugin_init(char *plugin_parameter, void **cookie)
{
	int flags;
	commandlog_plugin_data_t *datas = NULL;
	int log_fd;

	(*cookie) = NULL;

	datas = (commandlog_plugin_data_t*)malloc(
			sizeof (commandlog_plugin_data_t));
	if (!datas) {
		return (1);
	}

	datas->log = fopen(CL_CMDLOG_FILE, "a");
	if (datas->log == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Cannot open command log - Terminating.");
		free(datas);
		return (1);
	}

	log_fd = fileno(datas->log);

	if ((flags = fcntl(log_fd, F_GETFL, 0)) == -1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Cannot get mode flags - Terminating.");
		fclose(datas->log);
		free(datas);
		return (1);
	}

	if (fcntl(log_fd, F_SETFL, flags | O_SYNC) == -1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Cannot set mode flags - Terminating.");
		fclose(datas->log);
		free(datas);
		return (1);
	}

	(*cookie) = datas;

	return (0);
}

/*
 * commandlog_plugin_destroy()
 * Close cluster command log.
 *
 */

static void
commandlog_plugin_destroy(void *cookie)
{
	commandlog_plugin_data_t *datas = (commandlog_plugin_data_t*)cookie;

	(void) fclose(datas->log);
	free(datas);
}

/*
 * commandlog_plugin_event_log()
 *   Format a cluster command log entry and write it to disk.
 *   Cluster command log entries are packed name/value lists
 *   that are stored as variable length records.
 *
 */

static int
commandlog_plugin_event_log(sysevent_t *ev, void *cookie)
{

	commandlog_plugin_data_t *datas = (commandlog_plugin_data_t*)cookie;
	struct timeval timestamp;
	struct tm *time_fields = NULL;

	char *nodename = NULL;
	char *loginname = NULL;
	char *cmd_name = NULL;
	char *cmd_opt = NULL;
	char *publisher = NULL;
	int32_t *exit_status;
	uint32_t *pid;

	nvlist_t *nvlist = NULL;

	publisher = sysevent_get_pub_name(ev);
	if ((publisher != NULL) &&
	    (strcmp(publisher, CL_EVENT_PUB_CMD_START) != 0) &&
	    (strcmp(publisher, CL_EVENT_PUB_CMD_END) != 0)) {
		free(publisher);
		return (0);
	}

	if (sysevent_get_attr_list(ev, &nvlist) == -1) {
		free(publisher);
		return (1);
	}

	if (nvlist_lookup_uint32(nvlist, CL_EVENT_TS_SEC,
	    (uint32_t*)&timestamp.tv_sec)) {
		nvlist_free(nvlist);
		free(publisher);
		return (1);
	}

	if (nvlist_lookup_uint32(nvlist, CL_EVENT_TS_USEC,
	    (uint32_t*)&timestamp.tv_usec)) {
		nvlist_free(nvlist);
		free(publisher);
		return (1);
	}
	time_fields = localtime((time_t*)&timestamp.tv_sec);

	if (nvlist_lookup_string(nvlist, CL_EVENT_NODE, &nodename)) {
		nvlist_free(nvlist);
		free(publisher);
		return (1);
	}

	if (nvlist_lookup_uint32(nvlist, CL_EVENT_CMD_PID,
		(uint32_t*)&pid)) {
		nvlist_free(nvlist);
		free(publisher);
		return (1);
	}

	if (nvlist_lookup_string(nvlist, CL_EVENT_CMD_LOGIN, &loginname)) {
		nvlist_free(nvlist);
		free(publisher);
		return (1);
	}

	if (nvlist_lookup_string(nvlist, CL_EVENT_CMD_NAME, &cmd_name)) {
		nvlist_free(nvlist);
		free(publisher);
		return (1);
	}

	if (nvlist_lookup_string(nvlist, CL_EVENT_CMD_OPT, &cmd_opt)) {
		nvlist_free(nvlist);
		free(publisher);
		return (1);
	}

	if (strcmp(publisher, CL_EVENT_PUB_CMD_START) == 0) {
		fprintf(datas->log,
			"%02d/%02d/%04d %02d:%02d:%02d %s %d %s %s - %s %s\n",
			time_fields->tm_mon+1,
			time_fields->tm_mday,
			time_fields->tm_year+1900,
			time_fields->tm_hour,
			time_fields->tm_min,
			time_fields->tm_sec,
			nodename,
			pid,
			loginname,
			CMD_START,
			cmd_name,
			cmd_opt);
	} else if (strcmp(publisher, CL_EVENT_PUB_CMD_END) == 0) {
		nvlist_lookup_int32(nvlist, CL_EVENT_CMD_RES,
			(int32_t*)&exit_status);
		fprintf(datas->log,
		"%02d/%02d/%04d %02d:%02d:%02d %s %d %s %s %d\n",
		time_fields->tm_mon+1,
		time_fields->tm_mday,
		time_fields->tm_year+1900,
		time_fields->tm_hour,
		time_fields->tm_min,
		time_fields->tm_sec,
		nodename,
		pid,
		loginname,
		CMD_END,
		exit_status);
	}

	free(publisher);
	fflush(datas->log);
	return (0);
}
