/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * This file contains the API that has to be implemented by the event
 * logging framework plugins.
 */

#ifndef _CL_EVENLOG_PLUGIN_H_
#define	_CL_EVENLOG_PLUGIN_H_

#pragma ident	"@(#)cl_eventlog_plugin.h	1.4	08/05/20 SMI"

#include <libsysevent.h>

/*
 * The following structure defines the operations implemented by the
 * plugins
 *
 * - init
 *   This routine is called to initialize the plugin. plugin_parameter is
 *   an input string which is the extra field coming from the configuration
 *   file for that plugin instance.
 *   private_datas is an output parameter that will be passed back as a
 *   cookie to the other API calls
 *
 * - destroy
 *   This routine is called when the plugin is unloaded. All the resources
 *   allocated by the plugin should be freed.
 *   cookie is the value returned by init
 *
 * - event_log
 *   This routine is called each time an event matching the rule in the
 *   configuration file is received.
 *   The ev parameter is the event itself.
 *   The cookie parameter is the value returned by init.
 */

typedef struct {
	int (*init)(char *plugin_parameter, void **private_datas);

	void (*destroy)(void *cookie);

	int (*event_log)(sysevent_t *ev, void *cookie);

} cl_eventlog_plugin_ops_t;

cl_eventlog_plugin_ops_t *get_plugin_ops();

#endif /* _CL_EVENLOG_PLUGIN_H_ */
