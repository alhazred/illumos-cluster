/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * This include file contains the routine needed to interface with the
 * plugins.
 *
 * Given an event, this code is responsible for finding the relevant
 * plugins and invoking them
 */

#ifndef _CL_PLUGINS_H_
#define	_CL_PLUGINS_H_

#pragma ident	"@(#)cl_plugins.h	1.4	08/05/20 SMI"

#include <libsysevent.h>

/*
 * The following routines initialize and destroy the plugins module.
 *
 * The init routine takes the name of the configuration file as a single
 * argument
 * It returns 0 in case of success, any other value otherwise
 *
 * The destroy routine doesn't take any argument and returns nothing
 */

int cl_plugins_init(char *cl_conffile);
void cl_plugins_destroy(void);

/*
 * The following routine dispatches an event to the relevant plugins
 *
 * It returns 0 in case of success - any other value otherwise
 */

int cl_plugins_dispatch(sysevent_t *event);

#endif /* _CL_PLUGINS_H_ */
