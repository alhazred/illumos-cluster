/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_eventlogd.c	1.14	09/01/26 SMI"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>
#include <syslog.h>
#include <locale.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <thread.h>
#include <synch.h>
#include <errno.h>
#include <libsysevent.h>
#include <sys/sysevent.h>
#include <sys/cl_events.h>
#include <sys/cl_events_int.h>
#include <sys/sol_version.h>
#include <fcntl.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>

#include "cl_eventlogd.h"
#include "cl_plugins.h"
#include <rgm/sczones.h>

#include <sys/sysevent/eventdefs.h>

static int count = 0;

static sysevent_handle_t *sysevent_hp = NULL;

static mutex_t global_mutex;

/*
 * Event handler callback. Invoked by syseventd to deliver an event.
 * The implementation of this function is OS release dependent.
 * For S8, we have to make use of the SLM and private Solaris
 * interfaces to get event delivery. For S9 and beyond, we make
 * use of the general purpose event delivery channels.
 *
 * The SYSEVENTD_CHAN constant defined for Solaris 9 and above, will
 * tell us what to do...
 *
 */

#ifdef SYSEVENTD_CHAN
void
#else
int
#endif
handle_event(sysevent_t *ev)
{
	int err;

	count = count + 1;

	err = cl_plugins_dispatch(ev);
	if (err) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "cl_plugins_dispatch failed [%d]\n", err);
	}
#ifdef SYSEVENTD_CHAN
	return;
#else
	return (0);
#endif
}

/*
 * subscribe()
 *   Subscribe to cluster events. The implementation of this function
 *   is OS dependent. See handle_event() for more information.
 *
 */

int
subscribe()
{
#ifdef	SYSEVENTD_CHAN
	char	*subclass_list[] = { NULL, NULL };

	sysevent_hp = sysevent_bind_handle(handle_event);

	if (sysevent_hp == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "sysevent_bind_handle(): %m");
		return (1);
	}

	/* Subscribe for cluster events */
	subclass_list[0] = EC_SUB_ALL;
	if (sysevent_subscribe_event(sysevent_hp, EC_CLUSTER,
	    (const char **)subclass_list, 1) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to subscribe to the specified event.
		 * @user_action
		 * Save a copy of the /var/adm/messages file of this node and
		 * contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		syslog(LOG_ERR, "sysevent_subscribe_event() for %s failed.",
		    EC_CLUSTER);
		return (1);
	}

#if	SOL_VERSION >= __s10
	/* Subscribe for zfs cachefile change events */
	subclass_list[0] = ESC_ZFS_CONFIG_SYNC;
	if (sysevent_subscribe_event(sysevent_hp, EC_ZFS,
	    (const char **)subclass_list, 1) != 0) {
		syslog(LOG_ERR, "sysevent_subscribe_event() for %s failed.",
		    EC_ZFS);
		return (1);
	}
#endif
#else
	sysevent_hp = cl_event_open_channel(CL_EVENTLOG_DOOR);
	if (sysevent_hp == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "cl_event_open_channel(): %m");
		return (1);
	}

	if (cl_event_bind_channel(sysevent_hp, handle_event) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "cl_event_bind_channel(): %m");
		return (1);
	}
#endif
	return (0);
}

/*
 * unsubscribe()
 *   Terminate cluster event subscription. The implementation of this
 *   function is OS dependent. See handle_event() for more information.
 *
 */

void
unsubscribe()
{
	if (sysevent_hp == NULL)
		return;
#ifdef SYSEVENTD_CHAN
	sysevent_unbind_handle(sysevent_hp);
#else
	(void) cl_event_close_channel(sysevent_hp);
#endif
	sysevent_hp = NULL;
}

/*
 * sig_handler
 * --------------
 * Signal handler for the SIGHUP and SIGTERM signals.
 *
 * This function runs in a dedicated signal handling thread.  The thread
 * should be created with a signal mask blocking all signals.
 *
 * Uses an infinite loop around sigwait, to wait for all signals.
 * Handles SIGTERM and SIGHUP as described. Continues on all others.
 *
 */

static void
sig_handler(void)
{
	/*
	 * block all signals for all threads
	 */
	sigset_t sig_set;
	int err, sig;

	if (sigfillset(&sig_set) != 0) {
		/*
		 * Suppress lint message about "call to __errno() not
		 * made in the presence of a prototype."
		 * lint is unable to see the prototype for the __errno()
		 * function to which errno is macroed.
		 */
		err = errno; /*lint !e746 */
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider for assistance in diagnosing and correcting
		 * the problem.
		 */
		syslog(LOG_ERR, "sig_handler sigfillset: %s", strerror(err));
		exit(err);
	}
	while (1) {
		if (sigwait(&sig_set, &sig) != 0) {
			err = errno;
			/*
			 * SCMSGS
			 * @explanation
			 * The daemon was unable to configure its signal
			 * handling functionality, so it is unable to run.
			 * @user_action
			 * Save the syslog messages file. Contact your
			 * authorized Sun service provider for assistance
			 * in diagnosing and correcting the problem.
			 */
			syslog(LOG_ERR, "sig_handler sigwait: %s",
			    strerror(err));
			exit(err);
		}

		switch (sig) {
		case SIGHUP:
			/*
			 * SCMSGS
			 * @explanation
			 * The cl_eventlogd daemon has recieved a SIGHUP signal,
			 * which indicates that it must reconfigure.
			 * @user_action
			 * This message is informational only, and does not
			 * require user action.
			 */
			syslog(LOG_INFO, "Restarting on signal %d.", sig);

			(void) mutex_lock(&global_mutex);
			unsubscribe();

			cl_plugins_destroy();

			err = cl_plugins_init(CL_EVENTLOG_CONFFILE);
			if (err) {
				syslog(LOG_ERR,
				    "cl_plugins_init failed. Exiting [%s]\n",
				    CL_EVENTLOG_CONFFILE);
				exit(1);
			}

			(void) subscribe();

			(void) mutex_unlock(&global_mutex);

			break;
		case SIGTERM:
			/*
			 * SCMSGS
			 * @explanation
			 * The cl_eventlogd daemon has recieved a SIGTERM
			 * signal, which indicates that it must exit.
			 * @user_action
			 * This message is informational only, and does not
			 * require user action.
			 */
			syslog(LOG_ERR, "Going down on signal %d.", sig);

			(void) mutex_lock(&global_mutex);

			unsubscribe();
			cl_plugins_destroy();

			(void) mutex_unlock(&global_mutex);

			exit(0);
			break;
		default:
			/*
			 * SCMSGS
			 * @explanation
			 * The cl_eventlogd daemon has received a signal,
			 * possibly caused by an operator-initiated kill(1)
			 * command. The signal is ignored.
			 * @user_action
			 * This message is informational only, and does not
			 * require user action.
			 */
			syslog(LOG_ERR, "Unexpected signal %d.", sig);
			break;
		}
	}
}

/*
 * init_signal_handlers
 * ---------------------
 * Blocks all signals in this thread, which will be the parent of the
 * other threads, so that they will inherit the signal mask.
 *
 * Creates a thread to handle all the signals.  It runs the sig_handler
 * method below.
 *
 * If we fail in any of this, exit the daemon.
 */

static void
init_signal_handlers(void)
{
	/*
	 * block all signals for all threads
	 */
	sigset_t sig_set;
	int err;
	pthread_t sig_thr;

	if (sigfillset(&sig_set) != 0) {
		err = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider for assistance in diagnosing and correcting
		 * the problem.
		 */
		syslog(LOG_ERR, "init_signal_handlers sigfillset: %s",
		    strerror(err));
		exit(err);
	}

	if ((err = pthread_sigmask(SIG_BLOCK, &sig_set, NULL)) != 0) {
		char *err_str = strerror(err);
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider for assistance in diagnosing and correcting
		 * the problem.
		 */
		syslog(LOG_ERR, "init_signal_handlers pthread_sigmask: %s",
		    err_str ? err_str : "");
		exit(err);
	}

	/*
	 * now spawn the thread that will actually handle the signals
	 */
	if ((err = pthread_create(&sig_thr, NULL,
	    (void *(*)(void *))sig_handler, NULL)) != 0) {
		char *err_str = strerror(err);
		/*
		 * SCMSGS
		 * @explanation
		 * Internal error.
		 * @user_action
		 * Save the syslog messages file. Contact your authorized Sun
		 * service provider for assistance in diagnosing and correcting
		 * the problem.
		 */
		syslog(LOG_ERR, "init_signal_handlers pthread_create: %s",
		    err_str ? err_str : "");
		exit(err);
	}
}

int
main(int argc, char * const argv[])
{
	uint_t fd;
	pid_t pid;
	struct rlimit rlim;
	int err;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		exit(1);

	if (argc == 0)
		exit(1);

	/* Check for root uid */

	if (getuid() != 0) {
		(void) fprintf(stderr,
		    gettext("Must be root to run %s.\n"), argv[0]);
		exit(1);
	}

	pid = fork();
	if (pid < 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Cannot fork: %m.");
		exit(1);
	}
	if (pid > 0)
		exit(0);

	/* Child continues from here... */

	(void) getrlimit(RLIMIT_NOFILE, &rlim);
	for (fd = 0; fd < rlim.rlim_max; fd++)
		(void) close((int)fd);

	errno = 0;	/*lint !e746 */

	(void) open("/dev/null", O_RDONLY);
	(void) open("/dev/null", O_WRONLY);
	(void) dup(1);

	(void) chdir("/");
	(void) umask(022);
	(void) setsid();

	openlog("cl_eventlogd", LOG_CONS|LOG_PID, LOG_DAEMON);

	err = cl_plugins_init(CL_EVENTLOG_CONFFILE);
	if (err) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "cl_plugins_init failed. Exiting [%s]\n",
		    CL_EVENTLOG_CONFFILE);
		exit(1);
	}

	(void) mutex_init(&global_mutex, USYNC_THREAD, NULL);

	init_signal_handlers();
	(void) subscribe();

	do {
		(void) pause();
	} while (1);
	/* NOT REACHED */
}
