/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)gds_plugin.c 1.7     08/05/20 SMI"

/*
 * This file contains the implementation of the GDS plugin
 *
 * This plugin simply writes the events in a text and/or binary format
 * under /var/cluster/logs/DS.
 *
 * This plugin can also be taken as an example to write other plugins for
 * the event logging framework.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <libsysevent.h>
#include <libnvpair.h>
#include <string.h>
#include <sys/cl_events.h>
#include <syslog.h>
#include <sys/time.h>

#include "cl_eventlogd.h"
#include "cl_eventlog_plugin.h"

/*
 * API declaration
 */

static int gds_plugin_init(char *parameter, void **private_datas);
static void gds_plugin_destroy(void *cookie);
static int gds_plugin_event_log(sysevent_t *ev, void *cookie);

cl_eventlog_plugin_ops_t gds_plugin_ops = {
	gds_plugin_init,
	gds_plugin_destroy,
	gds_plugin_event_log
};

cl_eventlog_plugin_ops_t *
get_plugin_ops()
{
	return (&gds_plugin_ops);
}

/*
 * Type declarations
 */

#define	LOG_ROOTDIR "/var/cluster/logs/DS"
#define	DEFAULT_NBR_RES 4
#define	DIR_BUF_SIZE 1024

typedef struct {
	char *rname;
	char *rgname;
	FILE *start_stop_log;
	FILE *probe_log;
	FILE *bin_log;
} gds_perres_t;

#define	GDS_PLUGIN_MODE_BINARY	0x1
#define	GDS_PLUGIN_MODE_TEXT	0x2

typedef struct {
	int output_mode;
	int nb_res;
	int nb_res_allocated;
	gds_perres_t *resources;
} private_datas_t;

/*
 * Private routines
 */

/*
 * This routine checks that the given directory exists and is a directory.
 *
 * It will create one if needed.
 */

static int
check_dir(const char *path)
{
	struct stat buf;
	int err;

	err = stat(path, &buf);
	if (err) {
		if (errno != ENOENT) {
			return (1);
		}
	}

	if (!err) {
		/* Check that the entry is a directory */
		if (!(buf.st_mode & S_IFDIR)) {
			return (1);
		}
		return (0);
	}

	/* The directory does not exist and has to be created */

	err = mkdir(path, S_IRWXU | S_IRGRP | S_IXGRP);

	return (err);
}

/*
 * This routine looks for the resource entry in the list of resources
 * contained in the input parameter data.
 *
 * The result is returned in result.
 */

static int
find_res_entry(char *rgname, char *rname, int output_mode,
    private_datas_t *datas, gds_perres_t **result)
{
	int i;
	gds_perres_t *new_res = NULL;
	gds_perres_t *cur_res = NULL;
	char dir_buf[DIR_BUF_SIZE];

	(*result) = NULL;

	for (i = 0; i < datas->nb_res; i++) {
		if ((strcmp(datas->resources[i].rgname, rgname) == 0) &&
		    (strcmp(datas->resources[i].rname, rname) == 0)) {
			(*result) = datas->resources + i;
			break;
		}
	}

	if (*result) {
		return (0);
	}

	/* The resource has not been created yet */

	if (datas->nb_res == datas->nb_res_allocated) {
		new_res = realloc(datas->resources, datas->nb_res_allocated
				*sizeof (gds_perres_t)*2);
		if (!new_res) {
			return (1);
		}

		datas->resources = new_res;
		datas->nb_res_allocated *= 2;
	}

	cur_res = datas->resources + datas->nb_res;

	cur_res->rname = strdup(rname);
	if (!cur_res->rname) {
		return (1);
	}

	cur_res->rgname = strdup(rgname);
	if (!cur_res->rgname) {
		free(cur_res->rname);
		return (1);
	}

	snprintf(dir_buf, DIR_BUF_SIZE, "%s/%s", LOG_ROOTDIR, rgname);
	if (check_dir(dir_buf)) {
		free(cur_res->rname);
		free(cur_res->rgname);
		return (1);
	}

	snprintf(dir_buf, DIR_BUF_SIZE, "%s/%s/%s", LOG_ROOTDIR, rgname, rname);
	if (check_dir(dir_buf)) {
		free(cur_res->rname);
		free(cur_res->rgname);
		return (1);
	}

	cur_res->start_stop_log = NULL;
	cur_res->probe_log = NULL;
	cur_res->bin_log = NULL;

	if (output_mode & GDS_PLUGIN_MODE_TEXT) {
		snprintf(dir_buf, DIR_BUF_SIZE, "%s/%s/%s/start_stop_log.txt",
		    LOG_ROOTDIR, rgname, rname);
		cur_res->start_stop_log = fopen(dir_buf, "a");
		if (!cur_res->start_stop_log) {
			free(cur_res->rname);
			free(cur_res->rgname);
			return (1);
		}

		snprintf(dir_buf, DIR_BUF_SIZE, "%s/%s/%s/probe_log.txt",
		    LOG_ROOTDIR, rgname, rname);
		cur_res->probe_log = fopen(dir_buf, "a");
		if (!cur_res->probe_log) {
			fclose(cur_res->start_stop_log);
			free(cur_res->rname);
			free(cur_res->rgname);
			return (1);
		}
	}

	if (output_mode & GDS_PLUGIN_MODE_BINARY) {
		snprintf(dir_buf, DIR_BUF_SIZE, "%s/%s/%s/log.dat",
		    LOG_ROOTDIR, rgname, rname);
		cur_res->bin_log = fopen(dir_buf, "a");
		if (!cur_res->bin_log) {
			if (output_mode & GDS_PLUGIN_MODE_TEXT) {
				fclose(cur_res->start_stop_log);
				fclose(cur_res->probe_log);
			}
			free(cur_res->rname);
			free(cur_res->rgname);
			return (1);
		}
	}

	(*result) = cur_res;
	datas->nb_res++;

	return (0);
}

/*
 * The routine writes an event in an ASCII format in a text file.
 */

static int
print_event_to_text_file(sysevent_t *ev, nvlist_t *nvlist, FILE *output)
{
	struct timeval timestamp;
	struct tm *time_fields = NULL;
	cl_event_severity_t severity;
	char *label = NULL;
	char *message = NULL;
	char *nodename = NULL;
	char *severity_string = NULL;

	if (nvlist_lookup_uint32(nvlist, CL_EVENT_TS_SEC,
	    (uint32_t*)&timestamp.tv_sec)) {
		return (1);
	}

	if (nvlist_lookup_uint32(nvlist, CL_EVENT_TS_USEC,
	    (uint32_t*)&timestamp.tv_usec)) {
		return (1);
	}

	if ((!label) && (strcmp(sysevent_get_subclass_name(ev),
	    ESC_CLUSTER_GDS_START) == 0)) {
		label = "START";
	}

	if ((!label) && (strcmp(sysevent_get_subclass_name(ev),
	    ESC_CLUSTER_GDS_STOP) == 0)) {
		label = "STOP";
	}

	if ((!label) && (strcmp(sysevent_get_subclass_name(ev),
	    ESC_CLUSTER_GDS_PROBE) == 0)) {
		label = "PROBE";
	}

	if ((!label) && (strcmp(sysevent_get_subclass_name(ev),
	    ESC_CLUSTER_GDS_OTHER) == 0)) {
		label = "-";
	}

	if (nvlist_lookup_uint32(nvlist, CL_EVENT_SEVERITY,
	    (uint32_t*)&severity)) {
		severity_string = "?";
	}

	if (!severity_string) {
		switch (severity) {
		case CL_EVENT_SEV_INFO:
			severity_string = "INFO";
			break;

		case CL_EVENT_SEV_WARNING:
			severity_string = "WARNING";
			break;

		case CL_EVENT_SEV_ERROR:
			severity_string = "ERROR";
			break;

		case CL_EVENT_SEV_CRITICAL:
			severity_string = "CRITICAL";
			break;

		case CL_EVENT_SEV_FATAL:
			severity_string = "FATAL";
			break;

		default:
			severity_string = "?";
		}
	}

	if (!label) {
		return (1);
	}

	if (nvlist_lookup_string(nvlist, CL_EVENT_NODE, &nodename)) {
		return (1);
	}

	if (nvlist_lookup_string(nvlist, CL_STATUS_MSG, &message)) {
		return (1);
	}

	time_fields = localtime((time_t*)&timestamp.tv_sec);

	fprintf(output, "%02d/%02d/%04d %02d:%02d:%02d %s %s-%s> %s\n",
		time_fields->tm_mon+1,
		time_fields->tm_mday,
		time_fields->tm_year+1900,
		time_fields->tm_hour,
		time_fields->tm_min,
		time_fields->tm_sec,
		nodename,
		label,
		severity_string,
		message);
	fflush(output);

	return (0);
}

/*
 * This routine writes an event in a binary format in the binary file.
 */

static int
print_event_to_bin_file(sysevent_t *ev,
			nvlist_t *nvlist_input,
			FILE *output)
{
	char *buf = NULL;
	size_t buflen;
	nvlist_t *nvlist = NULL;
	pid_t ev_pid;
	char *vendor_name;
	char *pub_name;

	if (nvlist_dup(nvlist_input, &nvlist, NV_UNIQUE_NAME)) {
		return (1);
	}

	/* Add event class to the log entry. */

	if (nvlist_add_string(nvlist, CL_LOG_EV_CLASS,
	    sysevent_get_class_name(ev)) != 0) {
		nvlist_free(nvlist);
		return (1);
	}

	/* Add event subclass to the log entry. */

	if (nvlist_add_string(nvlist, CL_LOG_EV_SUBCLASS,
	    sysevent_get_subclass_name(ev)) != 0) {
		nvlist_free(nvlist);
		return (1);
	}

	/* Add event vendor... */

	vendor_name = sysevent_get_vendor_name(ev);
	if (nvlist_add_string(nvlist, CL_LOG_EV_VENDOR, vendor_name) != 0) {
		nvlist_free(nvlist);
		free(vendor_name);
		return (1);
	}
	free(vendor_name);

	/* Add event publisher... */

	pub_name = sysevent_get_pub_name(ev);
	if (nvlist_add_string(nvlist, CL_LOG_EV_PUB, pub_name) != 0) {
		nvlist_free(nvlist);
		free(pub_name);
		return (1);
	}
	free(pub_name);

	/* Add event publisher's PID... */

	sysevent_get_pid(ev, &ev_pid);
	if (nvlist_add_int32(nvlist, CL_LOG_EV_PID, (int32_t)ev_pid) != 0) {
		nvlist_free(nvlist);
		return (1);
	}

	/* Pack and print to file */

	if (nvlist_pack(nvlist, &buf, &buflen, NV_ENCODE_NATIVE, 0)) {
		return (1);
	}

	fwrite(&buflen, sizeof (buflen), 1, output);
	fwrite(buf, buflen, 1, output);
	fflush(output);

	free(buf);
	nvlist_free(nvlist);

	return (0);
}

/*
 * This routine is the main entry point to log an event.
 *
 * Depending on the configuration, it will log the event in the text and/or
 * binary file.
 */

static int
logevent(sysevent_t *ev, int output_mode, nvlist_t *nvlist, gds_perres_t *entry)
{
	int err1 = 0, err2 = 0;

	if (output_mode & GDS_PLUGIN_MODE_TEXT) {
		if (strcmp(sysevent_get_subclass_name(ev),
		    ESC_CLUSTER_GDS_PROBE) != 0) {
			/* This is not a probe event */
			err1 = print_event_to_text_file(ev, nvlist,
			    entry->start_stop_log);
		} else {
			err1 = print_event_to_text_file(ev, nvlist,
			    entry->probe_log);
		}
	}

	if (output_mode & GDS_PLUGIN_MODE_BINARY) {
		err2 = print_event_to_bin_file(ev, nvlist, entry->bin_log);
	}

	if (err1 || err2)
		return (1);
	else
		return (0);
}

/*
 * API implementation
 */

/*
 * This is the init routine for the plugin interface.
 *
 * It creates the needed data structures and put them in the cookie
 * parameter
 */

static int
gds_plugin_init(char *parameter,
		void **private_datas)
{
	int err, output_mode;
	private_datas_t *datas = NULL;

	(*private_datas) = NULL;

	output_mode = 0;

	if (parameter) {
		/* Parse the arguments to find the mode */
		char *parameter_cpy = strdup(parameter);
		char *arg = NULL;

		if (parameter_cpy == NULL) {
			return (1);
		}

		arg = strtok(parameter_cpy, ",");
		while (arg) {
			if (strcmp(arg, "binary") == 0) {
				output_mode |= GDS_PLUGIN_MODE_BINARY;
			}
			if (strcmp(arg, "text") == 0) {
				output_mode |= GDS_PLUGIN_MODE_TEXT;
			}

			arg = strtok(NULL, ",");
		}
		free(parameter_cpy);
		parameter_cpy = NULL;
	} else {
		output_mode = GDS_PLUGIN_MODE_TEXT;
	}

	err = check_dir(LOG_ROOTDIR);
	if (err) {
		return (err);
	}

	datas = (private_datas_t *)malloc(sizeof (private_datas_t));
	if (!datas) {
		return (1);
	}

	datas->output_mode = output_mode;
	datas->nb_res = 0;
	datas->nb_res_allocated = DEFAULT_NBR_RES;
	datas->resources = (gds_perres_t*)malloc(
	    datas->nb_res_allocated*sizeof (gds_perres_t));
	if (!datas->resources) {
		free(datas);
		return (1);
	}

	(*private_datas) = datas;

	return (0);
}

/*
 * This routine is called when the plugin is unloaded.
 *
 * All the resources have to be freed ...
 */

static void
gds_plugin_destroy(void *cookie)
{
	int i;
	private_datas_t *datas = (private_datas_t*)cookie;

	for (i = 0; i < datas->nb_res; i++) {
		if (datas->resources[i].rname) {
			free(datas->resources[i].rname);
			datas->resources[i].rname = NULL;
		}

		if (datas->resources[i].rgname) {
			free(datas->resources[i].rgname);
			datas->resources[i].rgname = NULL;
		}

		if (datas->resources[i].start_stop_log) {
			fclose(datas->resources[i].start_stop_log);
			datas->resources[i].start_stop_log = NULL;
		}

		if (datas->resources[i].probe_log) {
			fclose(datas->resources[i].probe_log);
			datas->resources[i].probe_log = NULL;
		}

		if (datas->resources[i].bin_log) {
			fclose(datas->resources[i].bin_log);
			datas->resources[i].bin_log = NULL;
		}
	}

	free(datas->resources);
	datas->resources = NULL;

	free(datas);
}

/*
 * This routine has to be called whenever an event has to be logged.
 */

static int
gds_plugin_event_log(sysevent_t *ev, void *cookie)
{
	int err;
	nvlist_t *nvlist = NULL;
	private_datas_t *datas = (private_datas_t*)cookie;
	char *rname = NULL;
	char *rgname = NULL;
	gds_perres_t *res_entry;

	if (sysevent_get_attr_list(ev, &nvlist) == -1) {
		return (1);
	}

	err = nvlist_lookup_string(nvlist, CL_R_NAME, &rname);
	if (err) {
		nvlist_free(nvlist);
		return (1);
	}

	if (nvlist_lookup_string(nvlist, CL_RG_NAME, &rgname)) {
		nvlist_free(nvlist);
		return (1);
	}

	if (find_res_entry(rgname, rname, datas->output_mode,
	    datas, &res_entry)) {
		nvlist_free(nvlist);
		return (1);
	}

	if (logevent(ev, datas->output_mode, nvlist, res_entry)) {
		nvlist_free(nvlist);
		return (1);
	}

	nvlist_free(nvlist);
	return (0);
}
