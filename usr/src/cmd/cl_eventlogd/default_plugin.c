/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)default_plugin.c 1.6     08/05/20 SMI"

/*
 * This file contains the implementation of the default plugin
 *
 * This plugin simply writes the events in a binary format on a given file.
 */

#include <stdio.h>
#include <malloc.h>
#include <syslog.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include "cl_eventlogd.h"
#include "cl_eventlog_plugin.h"

#define	CL_EVENTLOG_FILE	"/var/cluster/logs/eventlog"

/*
 * API routines declaration
 */

static int open_event_log(char *plugin_parameter, void **cookie);
static void close_event_log(void *cookie);
static int write_event_log_entry(sysevent_t *ev, void *cookie);

/*
 * Operations to register the plugin with the event logging framework
 */

static cl_eventlog_plugin_ops_t default_plugin_ops = {
	open_event_log,
	close_event_log,
	write_event_log_entry
};

cl_eventlog_plugin_ops_t *
get_plugin_ops()
{
	return (&default_plugin_ops);
}

/*
 * Some type declarations
 */

typedef struct {
	FILE *log;
} default_plugin_data_t;

/*
 * Plugin API routines implementation
 */

/*
 * open_event_log()
 *  Open cluster event log file for writing.
 *
 *  The plugin parameter is ignored for now
 */

static int
open_event_log(char *plugin_parameter, void **cookie)
{
	int flags;
	default_plugin_data_t *datas = NULL;
	int log_fd;

	(*cookie) = NULL;

	datas = (default_plugin_data_t*)malloc(sizeof (default_plugin_data_t));
	if (!datas) {
		return (1);
	}

	datas->log = fopen(CL_EVENTLOG_FILE, "a");
	if (datas->log == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Cannot open event log - Terminating.");
		free(datas);
		return (1);
	}

	log_fd = fileno(datas->log);

	if ((flags = fcntl(log_fd, F_GETFL, 0)) == -1) {
		syslog(LOG_ERR, "Cannot get mode flags - Terminating.");
		fclose(datas->log);
		free(datas);
		exit(1);
	}

	if (fcntl(log_fd, F_SETFL, flags | O_SYNC) == -1) {
		syslog(LOG_ERR, "Cannot set mode flags - Terminating.");
		fclose(datas->log);
		free(datas);
		return (1);
	}

	(*cookie) = datas;

	return (0);
}

/*
 * close_event_log()
 *   Close cluster event log.
 *
 */

static void
close_event_log(void *cookie)
{
	default_plugin_data_t *datas = (default_plugin_data_t*)cookie;

	(void) fclose(datas->log);
	free(datas);
}

/*
 * write_event_log_entry()
 *   Format a cluster event log entry and write it to disk.
 *   Cluster event log entries are packed name/value lists
 *   that are stored as variable length records.
 *
 * Each log entry contains the following name/value pairs:
 *   "ev_class"		string		event class
 *   "ev_subclass"	string		event subclass
 *   "ev_vendor"	string		event vendor
 *   "ev_pub"		string		event publisher
 *   "ev_pid"		uint32		event publisher's PID
 *   "ev_data"		byte array	a packed name/value
 *					representing the
 *					attribute list of the
 *					event being logged.
 *
 * When the entry is written to disk, a four-byte value representing
 * the size of the entry is written first, followed by the entry
 * itself.
 *
 */

static int
write_event_log_entry(sysevent_t *ev, void *cookie)
{
	default_plugin_data_t *datas = (default_plugin_data_t*)cookie;
	char *le_buf = NULL;
	size_t le_len = 0;
	nvlist_t *nvl = NULL;
	nvlist_t *nvl_le = NULL;

	char *buf = NULL;
	size_t buflen = 0;
	pid_t ev_pid;
	char *vendor_name;
	char *pub_name;

	/* Allocate name-value list */

	if (nvlist_alloc(&nvl_le, NV_UNIQUE_NAME, 0) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Cannot allocate log entry");
		return (1);
	}

	/* Add event class to the log entry. */

	if (nvlist_add_string(nvl_le, CL_LOG_EV_CLASS,
		sysevent_get_class_name(ev)) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Cannot create log entry component %s.",
		    "class");
		nvlist_free(nvl_le);
		return (1);
	}

	/* Add event subclass to the log entry. */

	if (nvlist_add_string(nvl_le, CL_LOG_EV_SUBCLASS,
		sysevent_get_subclass_name(ev)) != 0) {
		syslog(LOG_ERR, "Cannot create log entry component %s.",
		    "subclass");
		nvlist_free(nvl_le);
		return (1);
	}

	/* Add event vendor... */

	vendor_name = sysevent_get_vendor_name(ev);
	if (nvlist_add_string(nvl_le, CL_LOG_EV_VENDOR, vendor_name) != 0) {
		syslog(LOG_ERR, "Cannot create log entry component %s.",
		    "vendor");
		nvlist_free(nvl_le);
		free(vendor_name);
		return (1);
	}
	free(vendor_name);

	/* Add event publisher... */

	pub_name = sysevent_get_pub_name(ev);
	if (nvlist_add_string(nvl_le, CL_LOG_EV_PUB, pub_name) != 0) {
		syslog(LOG_ERR, "Cannot create log entry component %s.",
		    "pub");
		nvlist_free(nvl_le);
		free(pub_name);
		return (1);
	}
	free(pub_name);

	/* Add event publisher's PID... */

	sysevent_get_pid(ev, &ev_pid);
	if (nvlist_add_int32(nvl_le, CL_LOG_EV_PID, (int32_t)ev_pid) != 0) {
		syslog(LOG_ERR, "Cannot create log entry component %s.",
		    "pub");
		nvlist_free(nvl_le);
		return (1);
	}

	/*
	 * At this point, we are ready to add actual event attribute list
	 * to the cluster log entry. The data itself is a name-value
	 * list, so we first pack it and then add it as a byte array to
	 * the event log entry.
	 *
	 */

	if (sysevent_get_attr_list(ev, &nvl) != 0 || nvl == NULL) {
		syslog(LOG_ERR, "Cannot create log entry component %s.",
		    "attr-1");
		nvlist_free(nvl_le);
		return (1);
	}

	/* Pack the attribute list... */

	if (nvlist_pack(nvl, &buf, &buflen, NV_ENCODE_NATIVE, 0) != 0) {
		syslog(LOG_ERR, "Cannot create log entry component %s.",
		    "attr-2");
		nvlist_free(nvl);
		nvlist_free(nvl_le);
		return (1);
	}

	/* Add event attributes to the log entry. */

	if (nvlist_add_byte_array(nvl_le, CL_LOG_EV_DATA,
	    (uchar_t *)buf, buflen) != 0) {
		syslog(LOG_ERR, "Cannot create log entry component %s.",
		    "data");
		nvlist_free(nvl);
		nvlist_free(nvl_le);
		return (1);
	}

	nvlist_free(nvl);
	free(buf);
	buf = NULL;

	/*
	 * Now pack the entire event log entry, and write it out.
	 *
	 */

	buflen = 0;

	if (nvlist_pack(nvl_le, &buf, &buflen, NV_ENCODE_NATIVE, 0) != 0) {
		syslog(LOG_ERR, "Cannot create log entry component %s.",
		    "log entry");
		nvlist_free(nvl_le);
		return (1);
	}

	if (buf != NULL) {
		le_len = sizeof (buflen) + buflen;
		le_buf = (char *)malloc(le_len);
		if (le_buf) {
			(void) memcpy(le_buf, &buflen, sizeof (buflen));
			(void) memcpy(le_buf + sizeof (buflen), buf, buflen);

			if (fwrite(le_buf, le_len, 1, datas->log) == 0)
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_ERR, "Cannot write log entry");
			(void) fflush(datas->log);
		}

		nvlist_free(nvl_le);
		free(buf);
		free(le_buf);
		buf = NULL;
		buflen = 0;
		le_buf = NULL;
		le_len = 0;
	} else {
		syslog(LOG_ERR, "Cannot write log entry");
		nvlist_free(nvl_le);
	}

	return (0);
}
