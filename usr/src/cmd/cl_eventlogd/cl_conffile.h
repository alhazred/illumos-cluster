/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_CONFFILE_H_
#define	_CL_CONFFILE_H_

#pragma ident	"@(#)cl_conffile.h	1.4	08/05/20 SMI"

/*
 * This include file contains the needed routines to parse the cl_eventlog
 * configuration file.
 */

typedef struct {
	char *ev_class;
	char *ev_subclass;
	char *vendor;
	char *publisher;
	char *plugin_location;
	char *plugin_parameter;
} cl_conffile_entry_t;

/*
 * The cl_conffile_parse routine parses a configuration file and returns
 * all the found entries in the entries parameter.
 *
 * - filename is an input parameter specifying the location of the
 *   configuration file.
 *
 * - nb_entries is an output parameter specifying the number of found
 *   entries
 *
 * - entries is an output parameter specifying the list of found entries.
 *
 * cl_conffile_parse returns 0 in case of success, any other value in case
 * of failure.
 *
 * NOTE ! The entries parameter should be freed only using the
 * cl_conffile_free routine.
 */

int cl_conffile_parse(char *filename, int *nb_entries,
    cl_conffile_entry_t **entries);

/*
 * The cl_conffile_free routine frees the memory allocated by
 * cl_conffile_parse. nb_entries should be the value returned by
 * cl_conffile_parse.
 *
 * After cl_conffile_free has been called, the entries parameter cannot be
 * used anymore.
 */

void cl_conffile_free(int nb_entries, cl_conffile_entry_t *entries);

#endif /* _CL_CONFFILE_H_ */
