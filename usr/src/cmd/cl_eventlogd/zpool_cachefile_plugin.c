/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)zpool_cachefile_plugin.c	1.2	09/01/26 SMI"

/*
 * This file contains the implementation of the plugin which updates the CCR
 * with latest cachefile contents of a ZFS pool when a ZFS cachefile change
 * event is receieved.
 *
 * This plugin logs to file cachefile_activity.log for debugging purposes.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <errno.h>
#include <libsysevent.h>
#include <libnvpair.h>
#include <string.h>
#include <sys/cl_events.h>
#include <sys/sol_version.h>
#include <syslog.h>
#include <sys/time.h>
#include <libgen.h>
#include <stdarg.h>
#include <unistd.h>

#include "cl_eventlog_plugin.h"

#if	SOL_VERSION >= __s10
#include <sys/fs/zfs.h>
#endif
#include <sys/sysevent/eventdefs.h>
#include <librtutils.h>

/*
 * API declaration.
 */

int zpool_cachefile_plugin_init(char *parameter, void **private_datas);
void zpool_cachefile_plugin_destroy(void *cookie);
int zpool_cachefile_plugin_sync(sysevent_t *ev, void *cookie);

cl_eventlog_plugin_ops_t zpool_cachefile_plugin_ops = {
	zpool_cachefile_plugin_init,
	zpool_cachefile_plugin_destroy,
	zpool_cachefile_plugin_sync
};

cl_eventlog_plugin_ops_t *
get_plugin_ops()
{
	return (&zpool_cachefile_plugin_ops);
}

/*
 * Debug log file declarations.
 */

#define	LOG_DIR			"/var/cluster/run/HAStoragePlus/log"
#define	LOG_FILENAME		"cachefile_activity.log"
#define	LOG_LINE_LEN_MAX	256
#define	FILE_MODE		0755

#define	TIME_FORMAT_LEN		20

/*
 * Logging levels.
 */
#define	ZC_PLUGIN_INFO	1
#define	ZC_PLUGIN_WARN	2
#define	ZC_PLUGIN_ERROR	3

typedef struct {
	/*
	 * File pointer to log the debug message by this plugin.
	 */
	FILE	*log_fp;
} zpool_cachefile_plugin_data_t;

/*
 * Private routines
 */

/*
 * Routine to log the given message along with the time.
 */
static void
log_message(FILE *fp, int log_level, const char *msg, ...)
{
	char	log_msg[LOG_LINE_LEN_MAX];
	char	cur_time[TIME_FORMAT_LEN];
	time_t  clock_time;

	va_list	ap;
	va_start(ap, msg);	/*lint !e40 */
	(void) vsnprintf(log_msg, LOG_LINE_LEN_MAX, msg, ap);
	va_end(ap);

	(void) time(&clock_time);
	(void) snprintf(cur_time, TIME_FORMAT_LEN, ctime(&clock_time));

	if (log_level == ZC_PLUGIN_INFO) {
		(void) fprintf(fp, "%s %s: %s\n", cur_time, "INFO", log_msg);
	} else if (log_level == ZC_PLUGIN_WARN) {
		(void) fprintf(fp, "%s %s: %s\n", cur_time, "WARN", log_msg);
	} else if (log_level == ZC_PLUGIN_ERROR) {
		(void) fprintf(fp, "%s %s: %s\n", cur_time, "ERROR", log_msg);
	} else {
		(void) fprintf(fp, "%s : %s\n", cur_time, log_msg);
	}
	(void) fflush(fp);
}

/*
 * API implementation
 */

/*
 * This is the init routine for the plugin interface.
 *
 * It creates the needed data structures and put them in the cookie
 * parameter
 */

int
zpool_cachefile_plugin_init(char *parameter, void **cookie)
{
	int	rc;
	char	file_name[MAXPATHLEN];
	zpool_cachefile_plugin_data_t *data = NULL;

	(*cookie) = NULL;
	/*
	 * Create the log direcory.
	 */
	if (mkdirp(LOG_DIR, FILE_MODE) == -1) {
		rc = errno;	/*lint !e746 */
		if (rc != EEXIST) {
			/*
			 * SCMSGS
			 * @explanation
			 * The ZFS pool cachefile plugin failed to create
			 * specified directory.
			 * @user_action
			 * Check the log message for the reason of failure and
			 * try to resolve the problem. Otherwise contact your
			 * authorized Sun service provider.
			 */
			syslog(LOG_ERR, "ZFS pool cachefile plugin failed to "
			    "create directory '%s' : %s.",
			    LOG_DIR, strerror(rc));
			return (rc);
		}
	}
	/*
	 * Create the cachefiles location directory.
	 */
	if (mkdirp(HASP_ZPOOLS_CACHEFILES_DIR, FILE_MODE) == -1) {
		rc = errno;
		if (rc != EEXIST) {
			syslog(LOG_ERR, "ZFS pool cachefile plugin failed to "
			    "create directory '%s' : %s.",
			    HASP_ZPOOLS_CACHEFILES_DIR, strerror(rc));
			return (rc);
		}
	}
	/*
	 * Allocate memory for this plugin cookie.
	 */
	if ((data = (zpool_cachefile_plugin_data_t *)malloc(
	    sizeof (zpool_cachefile_plugin_data_t))) == NULL) {
		rc = errno;
		syslog(LOG_ERR, "Failed to allocate memory.");
		return (rc);
	}
	/*
	 * Obtain the file pointer to log cachefile activity.
	 */
	(void) snprintf(file_name, MAXPATHLEN, "%s/%s",
	    LOG_DIR, LOG_FILENAME);
	data->log_fp = fopen(file_name, "w");
	if (data->log_fp == NULL) {
		rc = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to open the specific file.
		 * @user_action
		 * Check for specific error in the error messages and try to
		 * resolve the problem. Otherwise contact your authorized Sun
		 * service provider.
		 */
		syslog(LOG_ERR, "ZFS pool cachefile plugin failed to open "
		    "the file '%s/%s' : %s.",
		    LOG_DIR, LOG_FILENAME, strerror(rc));
		return (rc);
	}

	log_message(data->log_fp, ZC_PLUGIN_INFO,
	    "Created the log file to record the activity of "
	    "ZFS pools cachefiles.");

	*cookie = data;

	return (0);
}/*lint !e715 !e818 */

/*
 * This routine is called when the plugin is unloaded.
 * All the resources needs to be freed.
 */

void
zpool_cachefile_plugin_destroy(void *cookie)
{
	zpool_cachefile_plugin_data_t *data;
	data = (zpool_cachefile_plugin_data_t *)cookie;
	(void) fclose(data->log_fp);
	(void) free(data);
}

/*
 * This routine is called whenever an ZFS event is received to cl_eventlogd
 */

int
zpool_cachefile_plugin_sync(sysevent_t *ev, void *cookie)
{
#if	SOL_VERSION >= __s10
	nvlist_t *nvlist = NULL;
	char	*pool_name;
	char	*ev_cname;
	char	*ev_scname;
	char	cachefile_path[MAXPATHLEN];
	char	zcache_ccr_tab[MAXPATHLEN];
	boolean_t zcache_ccr_tab_exists;
	zpool_cachefile_plugin_data_t *data;
	err_msg_t err;
	int	rc;

	data = (zpool_cachefile_plugin_data_t *)cookie;

	ev_cname = sysevent_get_class_name(ev);
	ev_scname = sysevent_get_subclass_name(ev);

	/*
	 * Ignore non ZFS events.
	 */
	if ((strcmp(EC_ZFS, ev_cname) != 0)) {
		return (0);
	}
	/*
	 * Ignore ZFS events other than cachefile changes.
	 */
	if ((strcmp(ESC_ZFS_CONFIG_SYNC, ev_scname)) != 0) {
		return (0);
	}

	/*
	 * Retrieve the poolname for which the event is received.
	 */
	if (sysevent_get_attr_list(ev, &nvlist) != 0) {
		rc = errno;
		log_message(data->log_fp, ZC_PLUGIN_ERROR,
		    "Failed to retrieve attributes of event class '%s' "
		    "and subclass '%s' : %s.",
		    ev_cname, ev_scname, strerror(rc));
		return (rc);
	}
	if (nvlist_lookup_string(nvlist, ZFS_EV_POOL_NAME, &pool_name) != 0) {
		rc = errno;
		log_message(data->log_fp, ZC_PLUGIN_ERROR,
		    "Failed to look up the value of '%s' of event class '%s' "
		    "and subclass '%s' : %s.",
		    ZFS_EV_POOL_NAME, ev_cname, ev_scname, strerror(rc));
		nvlist_free(nvlist);
		return (rc);
	}
	log_message(data->log_fp, ZC_PLUGIN_INFO,
	    "Received a ZFS event '%s' for the ZFS pool '%s'.",
	    sysevent_get_subclass_name(ev), pool_name);

	get_pool_cachefile_path(pool_name, cachefile_path, MAXPATHLEN);
	get_pool_ccr_tabname(pool_name, zcache_ccr_tab, MAXPATHLEN);

	/*
	 * If there is no cachefile exists for the pool for which we received
	 * the event, that pool is not under HAStoragePlus control.
	 */
	if (access(cachefile_path, R_OK) != 0) {
		rc = errno;
		nvlist_free(nvlist);
		if (rc == ENOENT) {
			return (0);
		}
		log_message(data->log_fp, ZC_PLUGIN_ERROR,
		    "The cachefile path '%s' for the ZFS pool '%s' "
		    "is not accessible: %s.",
		    cachefile_path, pool_name, strerror(rc));
		return (rc);
	}

	/*
	 * Check the existence of CCR table.
	 * Non-existence of table is fine, which means HAStoragePlus has not
	 * yet created the table or the pool is not under its control.
	 */
	if (check_pool_table(pool_name, &zcache_ccr_tab_exists, &err)) {
		log_message(data->log_fp, ZC_PLUGIN_ERROR,
		    "Failed to check the existence of CCR table "
		    "for pool %s : %s.",
		    pool_name, err.message);
		nvlist_free(nvlist);
		return (1);
	}
	if (!zcache_ccr_tab_exists) {
		nvlist_free(nvlist);
		return (0);
	}

	/*
	 * Update the CCR with latest cachefile contents.
	 */
	if (dump_zcache_to_ccr(pool_name, cachefile_path, NULL, &err) != 0) {
		log_message(data->log_fp, ZC_PLUGIN_ERROR,
		    "Failed to update the CCR with cachefile %s contents for "
		    "pool %s : %s",
		    cachefile_path, pool_name, err.message);
		/*
		 * SCMSGS
		 * @explanation
		 * ZFS pool cachefile plugin detected that there is change in
		 * specfified pool configuration, but failed to update the
		 * changes to specfified CCR table.
		 * This will not affect the pool operations in any way.
		 * An update failure of cachefile to CCR misses the chance for
		 * HAStoragePlus to import the pool faster.
		 * @user_action
		 * Contact your authorized Sun service provider with
		 * /var/adm/messages of all nodes to determine whether a
		 * workaround or patch is available.
		 */
		syslog(LOG_WARNING,
		    "ZFS pool cachefile plugin failed to update the CCR %s "
		    "with latest cachefile %s contents of pool %s : %s.",
		    zcache_ccr_tab, cachefile_path, pool_name, err.message);
		nvlist_free(nvlist);
		return (1);
	}

	log_message(data->log_fp, ZC_PLUGIN_INFO,
	    "Successfully updated the CCR %s with cachefile %s contents for "
	    "pool %s: %s", zcache_ccr_tab, cachefile_path, pool_name,
	    err.message);
	nvlist_free(nvlist);
#endif
	return (0);
}
