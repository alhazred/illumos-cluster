/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslmthresh.cc	1.12	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>		// for qsort, ...
#include <string.h>		// strtok
#include <libgen.h>
#include <assert.h>
#include <locale.h>		// for locale
#include <libintl.h>		// for gettext
#include <string.h>		// for memset

// cluster
#include <orb/infrastructure/orb.h>
#include <scha.h>
#include <rgm/sczones.h>

// Command line utilities
#include <sys/cl_auth.h>
#include <sys/cl_cmd_event.h>
#include <sys/cl_eventdefs.h>

// Ganymede
#include <scslm_ccr.h>
#include <scslm_cli.h>
#include <scslm.h>		// for error management
#include <scslm_cacao.h>


/* this variable is used to generate the correct event at exit */
#define	EXIT() { scslm_perror(NULL); exit(scslm_errno); }

// size of buffer receiving the command sent to cacao
#define	COMMAND_BUFFER_SIZE  8192

/* List of possible actions performed by this CLI */
typedef enum {
	ACTION_UNKNOWN = 0,
	ACTION_CHANGE,
	ACTION_PRINT,
	ACTION_STATUS,
	ACTION_SIZE
} scslmthresh_action_t;

static scslmthresh_action_t G_action = ACTION_UNKNOWN;


/* group for an option - i.e after a -m, -t or -y */
typedef enum {
	PARAM_GROUP_MO = 1040,	/* 1040 is to differentiate enum */
	PARAM_GROUP_THRESHOLD,
	PARAM_GROUP_PROPERTY,
	PARAM_GROUP_TAIL
} param_group_t;

/*
 * type indicating of a parameter (or a group of parameter) is mandatory
 * or not. Mandatory means "in every context"
 */
typedef enum {
	PARAM_MANDATORY,
	PARAM_OPTIONAL
} param_type_t;


typedef struct param {
	/*
	 * the format of an option is:
	 *    -<group> <key>=<value>,...
	 */
	const char *key;
	const char *value;
	param_type_t type;
	param_group_t group;
	/*
	 * function to be called to validate a parameter is returns a
	 * new value (for instance, short names to long names) or null
	 * if invalid
	 */
	const char *(*translate)(const char *P_key, const char *P_value);
} param_t;

typedef struct threshold_status {
	char *mot;
	char *ki;
	char *node;
	char *status;
	char *mo;
	char *value;
	struct threshold_status *next;
} threshold_status_t;
/* forward definition */
static const char *
check_direction(const char *P_key, const char *P_value);
static const char *
translate_motype(const char *P_key, const char *P_motype);
static const char *
translate_ki(const char *P_key, const char *P_ki);
static const char *
check_severity(const char *P_key, const char *P_value);

/* This enum is gathering all the possible keys */
enum
{
	OPT_MONAME,
	OPT_MOTYPE,
	OPT_NODENAME,
	OPT_KINAME,
	OPT_DIRECTION,
	OPT_SEVERITY,
	OPT_VALUELIMIT,
	OPT_RESETVALUE,
	NB_CLI_OPTIONS
};

/*
 * Fill the option list
 */
param_t G_opt[NB_CLI_OPTIONS + 1] = {
	{
		STR_MONAME,
		NULL,
		PARAM_OPTIONAL,
		PARAM_GROUP_MO,
		NULL},
	{
		STR_MOTYPE,
		NULL,
		PARAM_OPTIONAL,
		PARAM_GROUP_MO,
		translate_motype},
	{
		STR_NODENAME,
		NULL,
		PARAM_OPTIONAL,
		PARAM_GROUP_MO,
		NULL},
	{
		STR_KINAME,
		NULL,
		PARAM_MANDATORY,
		PARAM_GROUP_THRESHOLD,
		translate_ki},
	{
		STR_DIRECTION,
		NULL,
		PARAM_OPTIONAL,
		PARAM_GROUP_THRESHOLD,
		check_direction},
	{
		STR_SEVERITY,
		NULL,
		PARAM_OPTIONAL,
		PARAM_GROUP_THRESHOLD,
		check_severity},
	{
		STR_VALUELIMIT,
		NULL,
		PARAM_OPTIONAL,
		PARAM_GROUP_PROPERTY,
		NULL},
	{
		STR_RESETVALUE,
		NULL,
		PARAM_OPTIONAL,
		PARAM_GROUP_PROPERTY,
		NULL},
	{
		NULL,
		NULL,
		PARAM_MANDATORY,
		PARAM_GROUP_TAIL,
		NULL}
};



static threshold_status_t *G_threshold_status_list = NULL;

/*
 * This structure contains the list of threshold (read from CCR,
 * modified/printed and then written to CCR
 */
static threshold_list_t *G_threshold_list = NULL;

char *G_cmd;			// argv[0]

/*
 * ********************************************************************
 *
 * usage
 * ********************************************************************
 */
static void
usage() {
	scslm_perror(NULL);
	(void) fprintf(stderr,
	    "\n%s: %s -c|-p [options]\n"
	    "\n"
	    "Print thresholds (-p) options\n"
	    "\t-m " STR_MOTYPE "=<" STR_MOTYPE ">,"
	    STR_MONAME "=<" STR_MONAME ">[,"
	    STR_NODENAME "=<" STR_NODENAME ">]\n"
	    "\t-t " STR_KINAME"=<" STR_KINAME ">,"
	    STR_DIRECTION "=<" STR_DIRECTION ">,"
	    STR_SEVERITY"=<" STR_SEVERITY ">\n"
	    "\n"
	    "Change thresholds (-c) options\n"
	    "\t-m " STR_MOTYPE "=<" STR_MOTYPE ">,"
	    STR_MONAME "=<" STR_MONAME ">[,"
	    STR_NODENAME "=<" STR_NODENAME ">]\n"
	    "\t-t " STR_KINAME"=<" STR_KINAME ">,"
	    STR_DIRECTION "=<" STR_DIRECTION ">,"
	    STR_SEVERITY"=<" STR_SEVERITY ">\n"
	    "\t-y " STR_VALUELIMIT "=<value>," STR_RESETVALUE "=<value>\n"
	    "\n",
	    gettext("usage"), basename(G_cmd));

	/*
	 * We do not use EXIT macro here as this macro is
	 * displaying the error string that is already displayed
	 * before the usage
	 */
	exit(scslm_errno);
}

/*
 * ********************************************************************
 *
 * translate_motype from command line (short notation) to internal
 * (long notation). This function is called when parsing the arguments
 * of the CLI
 *
 * ********************************************************************
 */
static const char *
translate_motype(const char *P_key, const char *P_mot)
{

	const char *returned_value = make_long_mot(P_mot);

	if (returned_value == NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" is not valid for \"%s\".\n"),
		    P_mot, P_key);
	}

	return (returned_value);
}

/*
 * ********************************************************************
 *
 * translate_ki from command line (short notation) to internal (long
 * notation)/ This function is calle dwhen parsing the arguments of the
 * CLI
 *
 * ********************************************************************
 */
static const char *
translate_ki(const char *P_key, const char *P_ki)
{
	const char *returned_value = make_long_ki(P_ki);

	if (returned_value == NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" is not valid for \"%s\".\n"),
		    P_ki, P_key);
	}

	return (returned_value);
}

/*
 * ********************************************************************
 *
 * check direction (return NULL if not valid).
 *
 * ********************************************************************
 */
static const char *
check_direction(const char *P_key, const char *P_value) {
	if (strcmp(STR_FALLING, P_value) == 0 ||
	    strcmp(STR_RISING, P_value) == 0) {
		return (P_value);
	}

	(void) scslm_seterror(SCSLM_EINVAL,
	    gettext("Acceptable values for the property \"%s\" "
		"are either \"%s\" or \"%s\".\n"), P_key,
	    STR_RISING, STR_FALLING);
	return (NULL);
}


/*
 * ********************************************************************
 *
 * check the severity (called when parsing)
 *
 * ********************************************************************
 */
static const char *
check_severity(const char *P_key, const char *P_value) {
	if (strcmp(STR_WARNING, P_value) == 0 ||
	    strcmp(STR_FATAL, P_value) == 0) {
		return (P_value);
	}

	(void) scslm_seterror(SCSLM_EINVAL,
	    gettext("Acceptable values for the property \"%s\" "
		"are either \"%s\" or \"%s\".\n"), P_key,
	    STR_WARNING, STR_FATAL);
	return (NULL);
}




/*
 * ********************************************************************
 *
 * input parsing
 *
 * This function is filling G_opt
 * ********************************************************************
 */
static scslm_error_t
parse_input(
    param_group_t P_what,
    param_type_t P_type,	// optional/mandatory
    char *P_optLetter,		// letter used on Command Line
    char *P_input)
{
	unsigned int i = 0;
	char *token;
	char *pos;
	const char *key;
	const char *value;
	int found;

	if (P_input == NULL) {
		if (P_type == PARAM_MANDATORY)
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("missing option \"%s\".\n"),
			    P_optLetter);
		goto end;
	}

	token = strtok(P_input, ",");
	while (token != NULL) {
		found = 0;
		pos = strchr(token, '=');
		if (pos == NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("\"%s\" has not the format key=value.\n"),
			    token);
			goto end;
		}
		*pos = '\0';

		key = strdup(token);
		value = strdup(pos + 1);
		*pos = '=';

		token = strtok(NULL, ",");

		for (i = 0; i < NB_CLI_OPTIONS; i++) {
			const char *new_value; // modified by translation

			if (G_opt[i].group  != P_what)
				continue;

			if (strcmp(key, G_opt[i].key) != 0)
				continue;

			if (G_opt[i].value != NULL) {
				(void) scslm_seterror(SCSLM_EINVAL,
				    gettext("\"%s\" is specified twice.\n"),
				    key);
				goto end;
			}

			found = 1;
			if (G_opt[i].translate != NULL) {

				/* scslm_errno is set by this function */
				new_value = G_opt[i].translate(key, value);

				if (new_value == NULL)
					goto end;
			} else
				new_value = value;


			/*
			 * Now that the translation is done and valid,
			 * we use the new value.
			 */

			G_opt[i].value = new_value;
			break;
		}
		/*
		 * If found is 0, it mean that the key does not exist
		 * for this option */
		if (found == 0) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("\"%s\" is not valid.\n"),
			    key);
			goto end;
		}
	}

	/*
	 * New check that all arguments have been found for this
	 * option */
	for (i = 0; i < NB_CLI_OPTIONS; i++) {
		if (G_opt[i].group  != P_what)
			continue;

		if ((G_opt[i].value == NULL) &&
		    (G_opt[i].type == PARAM_MANDATORY)) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("\"%s\" must be specified.\n"),
			    G_opt[i].key);
			goto end;
		}
	}


end:
	return (scslm_errno);
}


/*
 * ********************************************************************
 *
 * This function is called when reading the CCR table. Each line is
 * passed through (key, data) couple
 *
 * ********************************************************************
 */
scslm_error_t
store_in_threshold_list(const char *P_key, char *P_value, void *)
{
	unsigned int L_id_in_ccr = 0;
	unsigned int L_field;
	threshold_t *L_threshold;
	scslm_error_t err;

	/* extract the threshold id and the field */
	err = threshold_parse_ccr_key(P_key, &L_id_in_ccr, &L_field);

	if (err != SCSLM_OK) {
		goto end;
	}

	/* USe the id to get the threshold data */
	L_threshold = threshold_list_extract(
	    G_threshold_list,
	    L_id_in_ccr);

	/* If the threshold does not exist -> create it */
	if (L_threshold == NULL) {
		L_threshold = threshold_allocate();

		if (L_threshold == NULL) {
			err = scslm_seterror(SCSLM_ENOMEM,
			    gettext("failed to allocate memory\n"));
			goto end;
		}

		err = threshold_list_insert(
		    &G_threshold_list,
		    L_id_in_ccr,
		    L_threshold);

		if (err != SCSLM_NOERR)
			goto end;
	}

	/* update the field */
	err = threshold_set_field(L_threshold, L_field, P_value);
end:
	return (err);
}

/*
 * ********************************************************************
 *
 * Fill the threshold list global variable with the CCR content
 *
 * ********************************************************************
 */
static void
fill_threshold_list_from_ccr(int *P_creation_needed)
{
	if (scslm_read_table_ccr(SCSLMTHRESH_TABLE,
		store_in_threshold_list,
		&G_threshold_list, 1 /* keep_alive */) != SCSLM_NOERR) {
		*P_creation_needed = 1;

		scslm_clearerror();
	}
	else
		*P_creation_needed = 0;
}



/*
 * ********************************************************************
 *
 * verify the nodename of a threshold is valid
 *
 * The nodename, in some conditions (ie. when host-level metrics are
 * requested), is stored in object-instance.
 *
 * This function also transforms a nodeid into a nodename and stores
 * it back to the right field
 *
 * ********************************************************************
 */
scslm_error_t
check_nodename(const char *P_name)
{
	scha_cluster_t cl_handle;
	int is_handled_init = 0;
	scha_err_t scha_err;
	int id = -1;

	assert(P_name != NULL);

	if ((scha_err = scha_cluster_open(&cl_handle)) != SCHA_ERR_NOERR) {
		(void) scslm_seterror(SCSLM_EINTERNAL,
		    gettext("scha_cluster_open failed with %d.\n"),
		    scha_err);
		goto end;
	}

	is_handled_init = 1;

	scha_err = scha_cluster_get(cl_handle,
	    SCHA_NODEID_NODENAME, P_name, &id);
	if (scha_err != SCHA_ERR_NOERR) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" is not "
			"a configured \"%s\".\n"),
		    P_name,
		    STR_NODENAME);
		goto end;
	}

end:
	if (is_handled_init)
		(void) scha_cluster_close(cl_handle); //lint !e644

	return (scslm_errno);
}

/*
 * ********************************************************************
 *
 * check the options globally for the modify....These checks are
 * performed at a more complex level than just presence/absence of one
 * parameter. It checks global consistency
 *
 * ********************************************************************
 */
scslm_error_t
check_options_for_modify() {

	sensor_t *L_sensor_list = NULL;
	sensor_t *ptr;
	ki_t *ki_ptr;

	const char *L_motype	= G_opt[OPT_MOTYPE].value;
	const char *L_node	= G_opt[OPT_NODENAME].value;
	const char *L_oi	= G_opt[OPT_MONAME].value;
	const char *L_ki	= G_opt[OPT_KINAME].value;
	const char *L_value	= G_opt[OPT_VALUELIMIT].value;
	const char *L_rearm	= G_opt[OPT_RESETVALUE].value;
	const char *L_direction	= G_opt[OPT_DIRECTION].value;
	const char *L_severity	= G_opt[OPT_SEVERITY].value;

	/*
	 * Check that all options are filled. some options may
	 * be mandatory in some case and optional in others. In
	 * such cases, the option is tagged optional. We must check
	 * now that this option has been provided in mandatory cases
	 */

	if (L_severity == NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" must be specified.\n"),
		    G_opt[OPT_SEVERITY].key);
		goto end;
	}

	if (L_ki == NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" must be specified.\n"),
		    G_opt[OPT_KINAME].key);
		goto end;
	}

	if (L_motype == NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" must be specified.\n"),
		    G_opt[OPT_MOTYPE].key);
		goto end;
	}
	if (L_direction == NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" must be specified.\n"),
		    G_opt[OPT_DIRECTION].key);
		goto end;
	}

	if (L_oi == NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("a \"%s\" must be specified "
			"with \"%s = %s\".\n"),
		    STR_MONAME,
		    STR_MOTYPE,
		    make_short_mot(L_motype));
		goto end;
	}

	/* there is no nodename with an HOST but an MONAME */
	if (strcmp(L_motype, MOT_HOST) == 0) {
		if (L_node != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("a \"%s\" must *not* be specified "
				"with \"%s = %s\".\n"),
			    STR_NODENAME,
			    STR_MOTYPE,
			    make_short_mot(L_motype));
			goto end;
		}

		/* let's check the object instance as it's a node */
		if (check_nodename(L_oi) != SCSLM_NOERR)
			goto end;
	} else if (strcmp(L_motype, MOT_RESOURCEGROUP) != 0) {
		if (L_node == NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
				gettext("a \"%s\" must be specified "
					"with \"%s = %s\".\n"),
				STR_NODENAME,
				STR_MOTYPE,
				make_short_mot(L_motype));
			goto end;
		}
	} else {
		/* if the node name is set, we have to check it */
		if (L_node != NULL) {
			if (check_nodename(L_node) != SCSLM_NOERR)
				goto end;
		}
	}

	/* check the wring case: value=,rearm=<XX> */
	if ((L_value != NULL) && (L_rearm != NULL) &&
	    (strlen(L_value) == 0) && (strlen(L_rearm) != 0)) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("bad configuration of " STR_VALUELIMIT
			" and " STR_RESETVALUE " parameters\n"));
		goto end;
	}


	/* now check (motype, ki) is a valid couple */
	if (scslm_read_table_ccr(SCSLMADM_TABLE,
		store_in_sensor_list,
		(void *) &L_sensor_list,
		0 /* keep_alive */) != SCSLM_NOERR)
		goto end;

	ptr = L_sensor_list;
	while (ptr != NULL) {
		ki_ptr = ptr->ki_list;

		while (ki_ptr != NULL) {
			if ((strcmp(L_motype, ki_ptr->motype) == 0) &&
			    (strcmp(L_ki, ki_ptr->name) == 0))
				/* couple is found */
				goto end;
			ki_ptr = ki_ptr->next;
		}
		ptr = ptr->next;
	}

	// The couple does not exist
	(void) scslm_seterror(SCSLM_EINVAL,
	    gettext("There is no \"%s\" for the type \"%s\".\n"),
	    L_ki, L_motype);
end:
	if (L_sensor_list != NULL)
		free_sensor_list(&L_sensor_list);
	return (scslm_errno);
}



/*
 * ********************************************************************
 * MODIFY A THRESHOLD
 *
 * ********************************************************************
 */

static scslm_error_t
modify(int P_create_table) {
	threshold_t *L_thresh = NULL;
	unsigned int L_value_id;
	unsigned int L_rearm_id;
	unsigned int L_thresh_id;

	/* Create a local threshold with the given parameters */
	if ((L_thresh = threshold_allocate()) == NULL)
		goto end;

	if (threshold_set_field(L_thresh,
		THRESH_MONAME, G_opt[OPT_MONAME].value) != SCSLM_NOERR)
		goto end;

	if (threshold_set_field(L_thresh,
		THRESH_MOTYPE, G_opt[OPT_MOTYPE].value) != SCSLM_NOERR)
		goto end;

	if (threshold_set_field(L_thresh,
		THRESH_NODENAME, G_opt[OPT_NODENAME].value) != SCSLM_NOERR)
		goto end;

	if (threshold_set_field(L_thresh,
		THRESH_KINAME, G_opt[OPT_KINAME].value) != SCSLM_NOERR)
		goto end;


	/* Find a matching threshold in the known list (from CCR) */
	L_thresh_id =
	    threshold_list_get_matching_id(G_threshold_list, L_thresh);

	/* If no threshold are found, insert the created one */
	if (L_thresh_id == INVALID_THRESH_ID) {
		/*  create a new threshol if it does not exist */
		L_thresh_id = threshold_list_get_new_id(G_threshold_list);
		if (threshold_list_insert(&G_threshold_list,
			L_thresh_id, L_thresh) != SCSLM_NOERR)
			goto end;
	} else {
		threshold_t *L_found_thresh = NULL;
		/*
		 * otherwise get the already existing one as the
		 * reference */
		L_found_thresh = threshold_list_extract(G_threshold_list,
		    L_thresh_id);


		/*
		 * Check that the scope of the found threshold is the
		 * same as the one given on the command line.  This
		 * check avoids to modify a threshold on the cluster,
		 * i.e with non specified node, with data concerning
		 * one node (i.e with a specified node).
		 *
		 * This test is meaningless in the context where The
		 * object type is node.
		 */

		if ((G_opt[OPT_MOTYPE].value != NULL) &&
		    (strcmp(G_opt[OPT_MOTYPE].value, MOT_HOST) != 0)) {
			if (((*L_thresh)[THRESH_NODENAME] == NULL) &&
			    ((*L_found_thresh)[THRESH_NODENAME] != NULL)) {
				(void) scslm_seterror(SCSLM_EPROP,
				    gettext("A node specific threshold "
					"already exists.\n"));
				goto end;
			}

			if (((*L_thresh)[THRESH_NODENAME] != NULL) &&
			    ((*L_found_thresh)[THRESH_NODENAME] == NULL)) {
				(void) scslm_seterror(SCSLM_EPROP,
				    gettext("A cluster-wide threshold "
					"already exists.\n"));
				goto end;
			}
		}

		threshold_free(&L_thresh);
		L_thresh = L_found_thresh;
	}


	/* assign the threshold values (value + rearm) */
	if (strcmp(G_opt[OPT_DIRECTION].value, STR_RISING) == 0) {
		if (strcmp(G_opt[OPT_SEVERITY].value, STR_WARNING) == 0) {
			L_value_id = THRESH_RISING_WARNING_VALUE;
			L_rearm_id = THRESH_RISING_WARNING_REARM;
		} else {
			L_value_id = THRESH_RISING_FATAL_VALUE;
			L_rearm_id = THRESH_RISING_FATAL_REARM;

		}
	} else {		// FALLING
		if (strcmp(G_opt[OPT_SEVERITY].value, STR_WARNING) == 0) {
			L_value_id = THRESH_FALLING_WARNING_VALUE;
			L_rearm_id = THRESH_FALLING_WARNING_REARM;
		} else {
			L_value_id = THRESH_FALLING_FATAL_VALUE;
			L_rearm_id = THRESH_FALLING_FATAL_REARM;
		}
	}

	if (G_opt[OPT_VALUELIMIT].value != NULL)
		if (threshold_set_field(L_thresh, L_value_id,
			G_opt[OPT_VALUELIMIT].value) != SCSLM_NOERR)
			goto end;


	if (G_opt[OPT_RESETVALUE].value != NULL)
		if (threshold_set_field(L_thresh, L_rearm_id,
			G_opt[OPT_RESETVALUE].value) != SCSLM_NOERR)
			goto end;
	/*
	 * Before updating the CCR, lets' check the validity of the
	 * new threshold */
	if (threshold_is_ok(L_thresh) != SCSLM_NOERR)
		goto end;

	/* And update CCR */
	if (scslmthresh_update_table_ccr(SCSLMTHRESH_TABLE,
		G_threshold_list, P_create_table) != SCSLM_NOERR)
		goto end;

	/* post an event */
	(void) sc_publish_event(ESC_CLUSTER_SLM_CONFIG_CHANGE,
	    CL_EVENT_PUB_SLM,
	    CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_CONFIG_ACTION,
	    SE_DATA_TYPE_UINT32, CL_EVENT_CONFIG_PROP_CHANGED,
	    CL_CCR_TABLE_NAME, SE_DATA_TYPE_STRING, SCSLMTHRESH_TABLE,
	    NULL);
end:
	return (scslm_errno);
}

/*
 * ********************************************************************
 *
 * ********************************************************************
 */
void
store_status(char *buffer)
{
	char *ptr = buffer;
	unsigned int startindex = 0;
	unsigned int endindex = 0;
	char *node = NULL;
	char *status = NULL;
	char *value = NULL;
	char *mo = NULL;
	char tmp[1024];
	const char *ki;
	const char *mot;

	threshold_status_t *entry = (threshold_status_t*)
	    calloc(1, sizeof (threshold_status_t));
	if (entry == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("failed to allocate memory\n"));
		goto end;
	}

	/* MOT */
	while (! (*ptr == ' ' || *ptr == '\0')) {
		endindex++;
		ptr++;
	}

	/* extract MOT */
	(void) strncpy(tmp, buffer + startindex, endindex - startindex);
	tmp[endindex - startindex] = '\0';

	/* convert MOT to short name */
	mot = make_short_mot(tmp);
	if (mot == NULL) {
		(void) scslm_seterror(SCSLM_EINTERNAL,
		    gettext("Internal error\n"));
		goto end;
	}


	ptr++;
	endindex++;
	startindex = endindex;

	/* KI */

	while (! (*ptr == ' ' || *ptr == '\0')) {
		endindex++;
		ptr++;
	}

	/* extract KI */
	(void) strncpy(tmp, buffer + startindex, endindex - startindex);
	tmp[endindex - startindex] = '\0';

	/* convert KI to short name */
	ki = make_short_ki(tmp);
	if (ki == NULL) {
		(void) scslm_seterror(SCSLM_EINTERNAL,
		    gettext("Internal error\n"));
		goto end;
	}

	ptr++;
	endindex++;
	startindex = endindex;

	/* Node */
	while (! (*ptr == ' ' || *ptr == '\0')) {
		endindex++;
		ptr++;
	}

	node = (char*)calloc(1, endindex - startindex + 1);
	if (node == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("failed to allocate memory\n"));
		goto end;
	}
	(void) strncpy(node, buffer + startindex, endindex - startindex);

	ptr++;
	endindex++;
	startindex = endindex;

	/* status */
	while (! (*ptr == ' ' || *ptr == '\0')) {
		endindex++;
		ptr++;
	}

	status = (char*)calloc(1, endindex - startindex + 1);
	if (status == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("failed to allocate memory\n"));
		goto end;
	}

	(void) strncpy(status, buffer + startindex, endindex - startindex);

	ptr++;
	endindex++;
	startindex = endindex;

	/* Value */
	while (! (*ptr == ' ' || *ptr == '\0')) {
		endindex++;
		ptr++;
	}

	value = (char*)calloc(1, endindex - startindex + 1);
	if (value == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("failed to allocate memory\n"));
		goto end;
	}
	(void) strncpy(value, buffer + startindex, endindex - startindex);

	/*
	 * MO is everything that's left in the buffer
	 * (in case there is a space in the MO name)
	 */

	ptr++;
	startindex = endindex + 1;
	endindex = strlen(buffer);

	mo = (char*)calloc(1, endindex - startindex + 1);
	if (mo == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("failed to allocate memory\n"));
		goto end;
	}
	(void) strncat(mo, buffer + startindex, endindex - startindex);



	/* save the threshold properties */

	entry->mot = (char *)mot;
	entry->ki = (char *)ki;
	entry->node = node;
	entry->status = status;
	entry->value = value;
	entry->mo = mo;
	entry->next = NULL;


	/* insert entry in the list */
	if (G_threshold_status_list == NULL) {
		G_threshold_status_list = entry;
	} else {
		threshold_status_t *list_ptr = G_threshold_status_list;
		while (list_ptr != NULL) {
			if (list_ptr->next == NULL) {
				list_ptr->next = entry;
				goto end;
			}
			list_ptr = list_ptr->next;
		}
	}


end:
	if (scslm_errno != SCSLM_NOERR) {
		if (entry != NULL) {
			free(entry);
		}

		if (node != NULL) {
			free(node);
		}

		if (mo != NULL) {
			free(mo);
		}

		if (value != NULL) {
			free(value);
		}

		if (status != NULL) {
			free(status);
		}
	}
}

/*
 * ********************************************************************
 *
 * ********************************************************************
 */

static int
compare(const void *p1, const void *p2)
{
	return (strcmp(* ((char **)p1), * ((char **)p2)));
}

void
print_status()
{
	unsigned int L_mot_len = strlen(STR_MOTYPE);
	unsigned int L_mo_len = strlen(STR_MONAME);
	unsigned int L_node_len = strlen(STR_NODENAME);
	unsigned int L_status_len = strlen(STR_STATUS);
	unsigned int L_ki_len = strlen(STR_KINAME);
	unsigned int L_value_len = strlen(STR_CURRENTVALUE);
	unsigned int L_total_len = 0;
	unsigned int ii = 0;
	unsigned int L_nb_thresh = 0;

	sensor_t *L_sensor_list = NULL; // read from CCR table

	/*
	 * This array is containing the string to print. This
	 * array is sorted before being displayed
	 */

	char **L_lines_to_print = NULL;

	threshold_status_t *ptr = G_threshold_status_list;
	const char *L_print_format = "%-*s %-*s %-*s %-*s %-*s %-*s\n";
	char *L_buffer = NULL;


	/* Read CCR table to get units */
	if (scslm_read_table_ccr(SCSLMADM_TABLE,
		store_in_sensor_list,
		(void *)&L_sensor_list,
		0 /* keep_alive */) != SCSLM_NOERR)
		goto end;

	while (ptr != NULL) {
		ki_t *L_associated_ki;
		const char *L_long_ki;
		const char *L_long_mot;
		unsigned int len;

		len = strlen(ptr->mot);
		L_mot_len = max(L_mot_len, len);
		L_long_mot = make_long_mot(ptr->mot);

		len = strlen(ptr->mo);
		L_mo_len = max(L_mo_len, len);

		len = strlen(ptr->node);
		L_node_len = max(L_node_len, len);

		len = strlen(ptr->status);
		L_status_len = max(L_status_len, len);

		len = strlen(ptr->ki);
		L_ki_len = max(L_ki_len, len);
		L_long_ki = make_long_ki(ptr->ki);

		L_associated_ki = fetch_ki_from_sensor_list(L_sensor_list,
		    L_long_ki, L_long_mot);

		len = strlen(ptr->value);
		if (L_associated_ki != NULL)
			len += 1 /* space */
			    + strlen(gettext(L_associated_ki->unit));
		L_value_len = max(L_value_len, len);

		ptr = ptr->next;
		// get the number of thresholds;
		L_nb_thresh++;
	}

	if (L_nb_thresh == 0) {
		goto end;
	}


	/* print header */
	(void) fprintf(stdout, L_print_format,
	    L_node_len, STR_NODENAME,
	    L_mot_len, STR_MOTYPE,
	    L_ki_len, STR_KINAME,
	    L_mo_len, STR_MONAME,
	    L_value_len, STR_CURRENTVALUE,
	    L_status_len, STR_STATUS);

	// unrecognized L_print_format
	//lint -e557
	(void) fprintf(stdout,
	    "%1$.*2$s %1$.*3$s %1$.*4$s %1$.*5$s %1$.*6$s %1$.*7$s\n",
	    RULE,
	    L_node_len, L_mot_len, L_ki_len,
	    L_mo_len, L_value_len, L_status_len);
	//lint +e557

	L_total_len = L_node_len + 1
	    + L_mot_len + 1
	    + L_ki_len + 1
	    + L_mo_len + 1
	    + L_value_len + 1
	    + L_status_len + 1;

	/*
	 * allocate an array that will contain the string to
	 * be displayed. The string will be sorted
	 */
	L_lines_to_print = (char **)malloc(L_nb_thresh * sizeof (char *));

	if (L_lines_to_print == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("failed to allocate memory\n"));
		goto end;
	}

	/*
	 * allocate a string for the "value unit" display.
	 * The size takes into account the tailling "\0"
	 */
	L_buffer = (char *)malloc((L_value_len + 1) * sizeof (char));

	if (L_buffer == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("failed to allocate memory\n"));
		goto end;
	}

	(void) memset(L_lines_to_print, (int)L_nb_thresh, sizeof (char *));

	/* for each threshold create the line to display */
	ptr = G_threshold_status_list;
	ii = 0;

	while (ptr != NULL) {
		ki_t *L_associated_ki;
		const char *L_long_ki;
		const char *L_long_mot;

		L_lines_to_print[ii] = (char *)malloc(L_total_len);

		if (L_lines_to_print[ii] == NULL) {
			(void) scslm_seterror(SCSLM_ENOMEM,
			    gettext("failed to allocate memory\n"));
			goto end;
		}

		L_long_mot = make_long_mot(ptr->mot);
		L_long_ki = make_long_ki(ptr->ki);


		L_associated_ki = fetch_ki_from_sensor_list(L_sensor_list,
		    L_long_ki, L_long_mot);

		/*
		 * To format (value, unit), we have to add 1 to the
		 * length for the '\0'
		 */
		(void) snprintf(L_buffer, L_value_len + 1,
		    "%s %s",
		    ptr->value,
		    ((L_associated_ki == NULL) ? "" :
			gettext(L_associated_ki->unit)));

		(void) snprintf(L_lines_to_print[ii], L_total_len,
		    L_print_format,
		    L_node_len, ptr->node,
		    L_mot_len, ptr->mot,
		    L_ki_len, ptr->ki,
		    L_mo_len, ptr->mo,
		    L_value_len, L_buffer,
		    L_status_len, ptr->status);
		ptr = ptr->next;
		ii++;
	}

	qsort((void *) L_lines_to_print, L_nb_thresh, sizeof (char *), compare);

	for (ii = 0; ii < L_nb_thresh; ii++)
		(void) puts(L_lines_to_print[ii]);



end:

	if (L_lines_to_print != NULL) {
		for (ii = 0; ii < L_nb_thresh; ii++)
			if (L_lines_to_print[ii] != NULL)
				free(L_lines_to_print[ii]);
		free(L_lines_to_print);
	}

	if (L_buffer != NULL)
		free(L_buffer);
	free_sensor_list(&L_sensor_list);
}

/*
 * ********************************************************************
 *
 * ********************************************************************
 */
void
delete_status_list()
{
	threshold_status_t *ptr = G_threshold_status_list;

	while (ptr != NULL) {
		threshold_status_t *tmp = ptr;
		ptr = ptr->next;

		delete (tmp->node);
		delete (tmp->status);
		delete (tmp->mo);
		delete (tmp);
	}
}


/*
 * ********************************************************************
 * call ScslmthreshCommand via cacaocsc.
 *
 * Params:
 *   o P_args: command to call
 *
 * Returned value:
 *   o status
 *
 * ********************************************************************
 */
static scslm_error_t
call_ScslmThreshCommand(const char *P_argv)
{
	FILE *fp = NULL;
	char *tmp;		// temp pointer
	char buffer[BUFSIZ];

	int port = -1;
	const char *cacaocsc_path;
	char cacaocsc_port[128];

	static char command[COMMAND_BUFFER_SIZE] = "";
	char	*cmd_ptr = command;

	cacaocsc_path = get_cacaocsc_path();
	if (cacaocsc_path == NULL)
		goto end;

	cmd_ptr = strcat(cmd_ptr, cacaocsc_path);

	if (scslmadm_get_command_stream_adapter_port(&port) != SCSLM_NOERR)
		goto end;

	(void) sprintf(cacaocsc_port, " -p %d", port);
	cmd_ptr = strcat(cmd_ptr, cacaocsc_port);
	cmd_ptr = strcat(cmd_ptr, CACAOCSC_SCSLMTHRESH_CMD);

	cmd_ptr = strcat(cmd_ptr, P_argv);
	cmd_ptr = strcat(cmd_ptr, CACAOCSC_ARGS_END);
	cmd_ptr = strcat(cmd_ptr, " 2>&1");

	if ((fp = popen(command, "r")) == NULL) {
		(void) scslm_seterror(SCSLM_EINTERNAL,
		    gettext("failed to execute \"%s\".\n"), command);
		goto end;
	}

	while (fgets(buffer, BUFSIZ, fp) != NULL) {
		if (strncmp(buffer, "java.lang.Exception", 19) == 0) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("A Problem occured with Ganymede "
				"Cacao Module.\n"));
			goto end;
		}

		if (strncmp("cacaocsc:", buffer, 9) == 0) {
			(void) scslm_seterror(SCSLM_EINTERNAL,
			    gettext("A Problem occured with %s"), buffer);
			goto end;
		}

		if (strncmp("ERROR: ", buffer, 6) == 0) {
			int L_error_code;
			/* extract error code */
			(void) sscanf(buffer, "ERROR: %ld", &L_error_code);

			switch (L_error_code) {
			case 1:
				(void) scslm_seterror(SCSLM_EINTERNAL,
				    gettext("A protocol error occured"
					" with Cacao.\n"));
				break;
			case 2:
				(void) scslm_seterror(SCSLM_EINTERNAL,
				    gettext("Cacao had an internal error.\n"));
				break;
			default:
				(void) scslm_seterror(SCSLM_EINTERNAL,
				    gettext("Unexpected error returned"
					" by Cacao (error = %d).\n"),
				    L_error_code);
			}
			goto end;
		}

		/* remove Cariage return at the end */
		tmp = index(buffer, '\n');
		if (tmp != NULL)
			*tmp = '\0';

		/* the last line is containing a "NL" (ascii=10) only */
		if (tmp == buffer) {
			break;
		}

		store_status(buffer);
		if (scslm_errno != SCSLM_NOERR) {
			goto end;
		}
	}

	if (scslm_errno == SCSLM_NOERR) {
		print_status();
	}

end:
	if (fp != NULL)
		(void) pclose(fp);

	delete_status_list();

	return (scslm_errno);
}


/*
 * ********************************************************************
 * print a threshold
 *
 * ********************************************************************
 */

static scslm_error_t
print() {
	threshold_t *L_thresh = threshold_allocate();

	if (L_thresh == NULL)
		goto end;

	if (G_opt[OPT_SEVERITY].value != NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" must not be specified.\n"),
		    G_opt[OPT_SEVERITY].key);
		goto end;
	}

	if (G_opt[OPT_DIRECTION].value != NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" must not be specified.\n"),
		    G_opt[OPT_DIRECTION].key);
		goto end;
	}

	if (threshold_set_field(L_thresh,
		THRESH_MONAME, G_opt[OPT_MONAME].value) != SCSLM_NOERR)
		goto end;

	if (threshold_set_field(L_thresh,
		THRESH_MOTYPE, G_opt[OPT_MOTYPE].value) != SCSLM_NOERR)
		goto end;

	if (threshold_set_field(L_thresh,
		THRESH_NODENAME, G_opt[OPT_NODENAME].value) != SCSLM_NOERR)
		goto end;

	if (threshold_set_field(L_thresh,
		THRESH_KINAME, G_opt[OPT_KINAME].value) != SCSLM_NOERR)
		goto end;

	threshold_list_print(G_threshold_list, L_thresh);
end:
	if (L_thresh)
		threshold_free(&L_thresh);
	return (scslm_errno);

}

/*
 * ********************************************************************
 * print thresholds status
 *
 * ********************************************************************
 */
scslm_error_t
status()
{
	char *L_param = NULL;
	size_t L_total_size = 1; // start with '\0'

	char *L_moname_param = NULL;
	char *L_motype_param = NULL;
	char *L_nodename_param = NULL;
	char *L_kiname_param = NULL;

	const char *L_moname_option = " -moname ";
	const char *L_motype_option = " -motype ";
	const char *L_nodename_option = " -nodename ";
	const char *L_kiname_option = " -kiname ";

	if (G_opt[OPT_SEVERITY].value != NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" must not be specified.\n"),
		    G_opt[OPT_SEVERITY].key);
		goto end;
	}

	if (G_opt[OPT_DIRECTION].value != NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("\"%s\" must not be specified.\n"),
		    G_opt[OPT_DIRECTION].key);
		goto end;
	}

	if (G_opt[OPT_MONAME].value != NULL) {
		size_t L_size = strlen(L_moname_option) +
		    strlen(G_opt[OPT_MONAME].value);
		L_moname_param = (char*) malloc(L_size + 1);
		if (L_moname_param == NULL) {
			(void) scslm_seterror(SCSLM_ENOMEM,
			    gettext("failed to allocate memory\n"));
			goto end;
		}

		(void) snprintf(L_moname_param, L_size, "%s%s",
		    L_moname_option, G_opt[OPT_MONAME].value);

		L_total_size += strlen(L_moname_param);
	}

	if (G_opt[OPT_MOTYPE].value != NULL) {
		size_t L_size = strlen(L_motype_option) +
		    strlen(G_opt[OPT_MOTYPE].value);
		L_motype_param = (char*) malloc(L_size  + 1);
		if (L_motype_param == NULL) {
			(void) scslm_seterror(SCSLM_ENOMEM,
			    gettext("failed to allocate memory\n"));
			goto end;
		}

		(void) snprintf(L_motype_param, L_size, "%s%s",
		    L_motype_option, G_opt[OPT_MOTYPE].value);

		L_total_size += strlen(L_motype_param);
	}

	if (G_opt[OPT_NODENAME].value != NULL) {
		size_t L_size = strlen(L_nodename_option) +
		    strlen(G_opt[OPT_NODENAME].value);
		L_nodename_param = (char*) malloc(L_size + 1);
		if (L_nodename_param == NULL) {
			(void) scslm_seterror(SCSLM_ENOMEM,
			    gettext("failed to allocate memory\n"));
			goto end;
		}

		(void) snprintf(L_nodename_param, L_size, "%s%s",
		    L_nodename_option, G_opt[OPT_NODENAME].value);

		L_total_size += strlen(L_nodename_param);
	}

	if (G_opt[OPT_KINAME].value != NULL) {
		size_t L_size = strlen(L_kiname_option) +
		    strlen(G_opt[OPT_KINAME].value);
		L_kiname_param = (char*) malloc(L_size + 1);
		if (L_kiname_param == NULL) {
			(void) scslm_seterror(SCSLM_ENOMEM,
			    gettext("failed to allocate memory\n"));
			goto end;
		}

		(void) snprintf(L_kiname_param, L_size, "%s%s",
		    L_kiname_option, G_opt[OPT_KINAME].value);

		L_total_size += strlen(L_kiname_param);
	}

	if (L_total_size > 1) {
		L_param = (char*)calloc(1, L_total_size);
		if (L_param == NULL) {
			(void) scslm_seterror(SCSLM_ENOMEM,
			    gettext("failed to allocate memory\n"));
			goto end;
		}

		(void) snprintf(L_param, L_total_size, "%s%s%s%s",
		    (L_moname_param != NULL?L_moname_param:""),
		    (L_motype_param != NULL?L_motype_param:""),
		    (L_nodename_param != NULL?L_nodename_param:""),
		    (L_kiname_param != NULL?L_kiname_param:""));
	} else {
		L_param = strdup("");
	}

	(void) call_ScslmThreshCommand(L_param);
end:
	if (L_moname_param != NULL) {
		free(L_moname_param);
	}
	if (L_motype_param != NULL) {
		free(L_motype_param);
	}
	if (L_nodename_param != NULL) {
		free(L_nodename_param);
	}
	if (L_kiname_param != NULL) {
		free(L_kiname_param);
	}
	if (L_param != NULL) {
		free(L_param);
	}
	return (scslm_errno);
}

/*
 * ***********************************
 * check action
 * ***********************************
 */


static void
set_action(scslmthresh_action_t P_action, int P_option)
{
	static int L_option = '\0';
	if (G_action != ACTION_UNKNOWN) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Incompatible options, "
			"cannot mix -%c and -%c options.\n"),
		    L_option, P_option);
		usage();
	} else {
		G_action = P_action;
		L_option = P_option;
	}
}


/*
************************************
* Main
************************************
*/


int
main(int argc, char *argv[])
{
	int c;
	char *m_opt = NULL;
	char *t_opt = NULL;
	char *y_opt = NULL;
	int need_to_create_table = 0;

	scslm_error_t err;

	// parse options
	G_cmd = argv[0];

	/* Initialize, demote to normal uid, check */
	/* authorization and then promote to euid=0 */
	cl_auth_init();
	cl_auth_demote();

	// set locale
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	while ((c = getopt(argc, argv, "acpsm:t:y:")) != EOF) {
		switch (c) {
		case 'c': set_action(ACTION_CHANGE, c); break;
		case 'p': set_action(ACTION_PRINT, c); break;
		case 's': set_action(ACTION_STATUS, c); break;
		case 'm': m_opt = strdup(optarg); break;
		case 't': t_opt = strdup(optarg); break;
		case 'y': y_opt = strdup(optarg); break;
		default:
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("Option \"%c\" is not supported.\n"),
			    optopt);
			usage();
			break;
		}
	}

	/* check no parameter are left behind */
	if (optind < argc) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Unknown argument %s.\n"),
		    argv[optind]);
		usage();
	}

	/* This command can only be called from global zone */
	if (sc_zonescheck() != 0) {
		(void) scslm_seterror(SCSLM_EACCESS,
		    gettext("Current user is not allowed to "
			"execute this command.\n"));
		EXIT(); //lint !e527
	}

	/* CHECK AUTHORITY */
	switch (G_action) {
	case ACTION_CHANGE:
		cl_auth_check_command_opt_exit(*argv, NULL,
		    CL_AUTH_SYSTEM_MODIFY, SCSLM_EACCESS);
		break;
	case ACTION_PRINT:
	case ACTION_STATUS:
		cl_auth_check_command_opt_exit(*argv, NULL,
		    CL_AUTH_SYSTEM_READ, SCSLM_EACCESS);
		break;
	case ACTION_UNKNOWN:	// for lint
	case ACTION_SIZE:	// for lint
	default:
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Unknown action.\n"));
		usage();
	}

	cl_auth_promote();

	/* Generate event */
	cl_cmd_event_init(argc, argv, (int *)&scslm_errno);
	(void) cl_cmd_event_gen_start_event();
	(void) atexit(cl_cmd_event_gen_end_event);

	/* Initialize long names <=> short names conversion */
	if (glossary_initialize() != SCSLM_NOERR)
		EXIT();

	fill_threshold_list_from_ccr(&need_to_create_table);

	switch (G_action) {
	case ACTION_CHANGE:
		/* Now parse each options */
		if (parse_input(PARAM_GROUP_MO, PARAM_MANDATORY,
			"-m", m_opt) != SCSLM_NOERR)
			usage();

		if (parse_input(PARAM_GROUP_THRESHOLD, PARAM_MANDATORY,
			"-t", t_opt) != SCSLM_NOERR)
			usage();

		if (parse_input(PARAM_GROUP_PROPERTY, PARAM_MANDATORY,
			"-y", y_opt) != SCSLM_NOERR)
			usage();

		err = check_options_for_modify();
		if (err != SCSLM_NOERR)
			usage();

		err = modify(need_to_create_table);

		break;
	case ACTION_PRINT:
		if (y_opt != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("-p option cannot be "
				"used with -y option.\n"));
			usage();
		}

		if (parse_input(PARAM_GROUP_MO, PARAM_OPTIONAL,
			"-m", m_opt) != SCSLM_NOERR)
			usage();

		if (parse_input(PARAM_GROUP_THRESHOLD, PARAM_OPTIONAL,
			"-t", t_opt) != SCSLM_NOERR)
			usage();

		err = print();
		break;
	case ACTION_STATUS:
		if (y_opt != NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("-s option cannot be "
				"used with -y option.\n"));
			usage();
		}

		if (parse_input(PARAM_GROUP_MO, PARAM_OPTIONAL,
			"-m", m_opt) != SCSLM_NOERR)
			usage();

		if (parse_input(PARAM_GROUP_THRESHOLD, PARAM_OPTIONAL,
			"-t", t_opt) != SCSLM_NOERR)
			usage();

		err = status();
		break;
	case ACTION_UNKNOWN:	// for lint
	case ACTION_SIZE:	// for lint
	default:
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Unknown action.\n"));
		usage();
	}

	if (G_threshold_list)
		(void) threshold_list_free(&G_threshold_list);
	EXIT(); //lint !e527

	return (SCSLM_NOERR);
}
