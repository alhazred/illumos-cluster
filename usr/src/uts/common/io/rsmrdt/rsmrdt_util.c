/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rsmrdt_util.c	1.8	08/05/20 SMI"

#include <sys/types.h>
#include <sys/param.h>
#include <sys/user.h>
#include <sys/buf.h>
#include <sys/systm.h>
#include <sys/vm.h>
#include <sys/uio.h>
#include <vm/seg.h>
#include <sys/stat.h>

#include <sys/time.h>
#include <sys/varargs.h>

#include <sys/rsm/rsm.h>

/* lint -w2 */

extern char *vsprintf_len(size_t, char *, const char *, va_list);
extern char *sprintf(char *buf, const char *fmt, ...);

/*
 * From kadb, read printfs using : *rsmrdt_dbg/s
 */
#define	RSMRDT_BUFSIZE	0x10000

char    rsmrdt_buf[RSMRDT_BUFSIZE];
char    *rsmrdt_dbg = rsmrdt_buf;
char    *rsmrdt_buf_end = rsmrdt_buf;
char    *rsmrdt_buf_top = rsmrdt_buf + RSMRDT_BUFSIZE - 256;
kmutex_t rsmrdt_buf_lock;

int rsmrdtdbg_category = RSM_KERNEL_ALL;
#ifdef DEBUG
int rsmrdtdbg_level = RSM_DEBUG;
#else
int rsmrdtdbg_level = RSM_NOTICE;
#endif

void dbprintf(char *fmt, ...) {
	va_list ap;
	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, fmt);
	/*lint +e40 */
	mutex_enter(&rsmrdt_buf_lock);
	(void) vsprintf_len(255, rsmrdt_buf_end, fmt, ap);
	rsmrdt_buf_end += strlen(rsmrdt_buf_end);
	if (rsmrdt_buf_end > rsmrdt_buf_top) {
		rsmrdt_buf_end = rsmrdt_buf;
	}
	va_end(ap);
	mutex_exit(&rsmrdt_buf_lock);
}

void
dbg_printf(int msg_category, int msg_level, char *fmt, ...)
{
	if ((msg_category & rsmrdtdbg_category) &&
	    (msg_level <= rsmrdtdbg_level)) {
		va_list	ap;
		/*lint -e40 Undeclared identifier (__builtin_va_alist) */
		va_start(ap, fmt);
		/*lint +e40 */
		mutex_enter(&rsmrdt_buf_lock);
		(void) sprintf(rsmrdt_buf_end, "%16" PRIx64 ":",
			(uint64_t)curthread->t_did);
		rsmrdt_buf_end += 17;
		(void) vsprintf_len(255, rsmrdt_buf_end, fmt, ap);
		rsmrdt_buf_end += strlen(rsmrdt_buf_end);
		if (rsmrdt_buf_end > rsmrdt_buf_top) {
			rsmrdt_buf_end = rsmrdt_buf;
		}
		mutex_exit(&rsmrdt_buf_lock);
		va_end(ap);
	}
}
