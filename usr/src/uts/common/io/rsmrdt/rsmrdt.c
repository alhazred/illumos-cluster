/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * RSMRDT driver for RAC
 *
 */

#pragma ident	"@(#)rsmrdt.c	1.70	08/05/20 SMI"

const char opsrsm_version[] = "@(#)rsmrdt.c 1.70     08/05/20 SMI";

#include <sys/types.h>
#include <sys/errno.h>
#include <sys/debug.h>
#include <sys/stropts.h>
#include <sys/stream.h>
#include <sys/strlog.h>
#include <sys/cmn_err.h>
#include <sys/kmem.h>
#include <sys/conf.h>
#include <sys/stat.h>
#include <sys/dlpi.h>
#include <sys/modctl.h>
#include <sys/kstat.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/strsun.h>
#include <sys/taskq.h>
#include <sys/open.h>
#include <sys/uio.h>
#include <sys/cpuvar.h>
#include <sys/atomic.h>

#include <sys/rsm/rsm_common.h>
#include <sys/rsm/rsmpi.h>

#include "rsmrdt.h"		/* This driver's data structures */

/* inter-module dependencies */
char	_depends_on[] =	 "misc/rsmops";

/*
 * Lock hierarchy:
 *
 * opsrsmp->opsrsm_lock
 * opsrsmdevlock
 *
 *	rd->rd_lock
 *	rd->rd_xmit_lock
 *	rd->rd_net_lock
 *
 *	opsrsm->opsrsm_dest_lock
 *	opsrsm->opsrsm_runq_lock
 *
 * rd->rd_nlb_lock -- currently never taken while another lock is held
 * opsrsmattlock
 * opsrsmdbglock
 */


/*
 * Defining DEBUG on the compile line (-DDEBUG) will compile
 * debugging code into the driver.  Whether any debug output actually gets
 * printed depends on the value of opsrsmdbg, which determines the class of
 * messages that the user is interested in, and opsrsmdbgmode, which
 * determines how the user wants the messages to be produced.
 *
 * See the #defines for D1(), D2(), etc.  below for which bits in opsrsmdbg
 * cause which messages to get printed.
 *
 * The various types of output are controlled by bits in opsrsmdbgmode, as
 * follows.  Multiple types of output may be used at once, if desired.
 *
 * (opsrsmdbgmode & 1)	Use debugging log.
 * (opsrsmdbgmode & 2)	Use kernel printfs.
 */

#ifndef lint

#ifdef DEBUG

int opsrsmdbg = 0x0100;
int opsrsmdbgmode = 0x1;
static void opsrsmconsole(const char *, ...);

/* opsrsm function enter/exit, parameters, return values. */
#define	D1								\
	if (opsrsmdbg & 0x01)						\
	    opsrsmdebug

/* Additional function debugging. */
#define	D2								\
	if (opsrsmdbg & 0x02) 						\
	    opsrsmdebug

/* rsmpi interface routine enter/exit, parameters, return */
#define	D4								\
	if (opsrsmdbg & 0x08) 						\
	    opsrsmdebug

/* Latency timing output. */
#define	D5								\
	if (opsrsmdbg & 0x10) 						\
	    opsrsmdebug

/* Excessive debugging output */
#define	D6								\
	if (opsrsmdbg & 0x20) 						\
	    opsrsmdebug

/* debug message on the console */
#define	DINFO								\
	opsrsmconsole

/* error message logged to the debug buffer */
#define	DERR								\
	if (opsrsmdbg & 0x100) 						\
	    opsrsmdebug

#else /* DEBUG */

#define	D1	if (0) printf
#define	D2	if (0) printf
#define	D4	if (0) printf
#define	D5	if (0) printf
#define	D6	if (0) printf
#define	DINFO	if (0) printf
#define	DERR	if (0) printf

#endif /* DEBUG */

#else /* lint */

#ifdef DEBUG
int opsrsmdbg;
int opsrsmdbgmode;
#endif

#define	D1	printf
#define	D2	printf
#define	D4	printf
#define	D5	printf
#define	D6	printf
#define	DINFO	printf
#define	DERR	printf
#endif /* lint */

/*
 * Function prototypes.
 */
static int	opsrsm_open(dev_t *, int, int, struct cred *);
static int	opsrsm_close(dev_t, int, int, struct cred *);
static int	opsrsm_attach(dev_info_t *, ddi_attach_cmd_t);
static int	opsrsm_detach(dev_info_t *, ddi_detach_cmd_t);
static int	opsrsm_info(dev_info_t *, ddi_info_cmd_t, void *, void **);
static int	opsrsm_chpoll(dev_t, short, int, short *, struct pollhead **);
static int	opsrsm_ioctl(dev_t, int, intptr_t, int, cred_t *, int *);

static void	opsrsmwsrv(void *);
static int	opsrsmcrexfer(opsrsm_t *, opsrsm_dest_t *);
static int	opsrsmsconn(opsrsm_t *, opsrsm_dest_t *, int);
static int	opsrsmconnxfer(opsrsm_t *, opsrsm_dest_t *);
static int	opsrsmsack(opsrsm_dest_t *);
static int	opsrsmsaccept(opsrsm_t *opsrsmp, opsrsm_dest_t *rd);

static opsrsm_dest_t *opsrsm_connect(int, opsrsmresource_t *);
static opsrsm_dest_t *opsrsmmkdest(adapter_t *, rsm_addr_t);
static void	opsrsmsconntmo(void *);
static void	opsrsmacktmo(void *);
static void	opsrsmaccepttmo(void * arg);
static void	opsrsmfreedesttmo(void *);

static void	opsrsmmsghdlr_req_connect(opsrsm_dest_t *, opsrsm_msg_t *);
static void	opsrsmmsghdlr_con_accept(opsrsm_dest_t *, opsrsm_msg_t *);
static void	opsrsmmsghdlr_syncdqe(opsrsm_dest_t *, opsrsm_msg_t *);
static void	opsrsmmsghdlr_default(opsrsm_dest_t *, opsrsm_msg_t *);

static int	opsrsmgetstate(opsrsm_dest_t *);
static void	opsrsmsetstate(opsrsm_dest_t *, int);
static int	opsrsmmovestate(opsrsm_dest_t *, int, int newstate);
static int	opsrsmread(opsrsm_dest_t *, int, int, int, ushort_t sap);
static int	opsrsmuninit(adapter_t *adapterp);
static boolean_t	opsrsmdest_refcnt_0(opsrsm_dest_t *);
static int	opsrsmfreedest(adapter_t *adapter, rsm_addr_t);

/* LINTED: E_STATIC_FUNC_CALLD_NOT_DEFINED */
static void	opsrsmdebug(const char *, ...);
static void	opsrsmerror(dev_info_t *,  const char *, ...);
static void	opsrsmkstatinit(opsrsm_t *);
static void	opsrsmkstatremove(opsrsm_t *opsrsmp);
static void	opsrsmgetparam(dev_info_t *, opsrsm_t *);
static void	opsrsmtakedown(opsrsm_t *, int);

static void	opsrsmfreebuf(opsrsmbuf_t *);
static void	opsrsmputfqe(opsrsm_dest_t *, int);
static void	opsrsmputfqe_nolock(opsrsm_dest_t *, int);
static opsrsm_queued_fqe_t *opsrsm_queued_fqe_alloc(opsrsm_dest_t *);
static void opsrsm_queued_fqe_free(opsrsm_dest_t *, opsrsm_queued_fqe_t *);

static void	opsrsmputdqes(opsrsm_dest_t *);
static int	opsrsmavailfqe(opsrsm_dest_t *);
static int	opsrsmavailfqe2(opsrsm_dest_t *);
static int	opsrsmgetfqe(opsrsm_dest_t *, int *);
static int	opsrsmgetdqe(opsrsm_dest_t *, int *, int *, int *, ushort_t *);
static int	opsrsmsendmsg(opsrsm_dest_t *, uint8_t, opsrsm_msg_t *);

rsm_intr_hand_ret_t opsrsm_rsm_intr_handler(rsm_controller_object_t *,
    rsm_intr_q_op_t, rsm_addr_t, void *, size_t, rsm_intr_hand_arg_t);

static opsrsm_failover_info_t *opsrsm_finfo_add(opsrsm_dest_t *);
static opsrsm_failover_info_t *opsrsm_finfo_lookup_by_local_skey(uint32_t);
static opsrsm_failover_info_t *opsrsm_finfo_lookup_by_remote_skey(uint32_t);
static int opsrsm_finfo_wait(uint32_t);
static void opsrsm_finfo_wakeup(opsrsm_failover_info_t *, int);
static void opsrsm_finfo_init(void);
static void opsrsm_finfo_fini(void);
static void opsrsm_finfo_destroy(void *);
static void opsrsm_failover_thread(void *);
static void opsrsm_lostconn(opsrsm_dest_t *);
static void opsrsm_reset_all_rps(opsrsm_dest_t *rd);
static void opsrsmmsghdlr_finfo(opsrsm_dest_t *, opsrsm_msg_t *);
static void opsrsm_option_rexmit_end(mblk_t *, opsrsm_dest_t *);
static int opsrsm_finfo_sendmsg(opsrsm_dest_t *, uint8_t, uint32_t);
static mblk_t *opsrsm_alloc_ack_msg(uint32_t);
static void opsrsm_queued_msg_send(opsrsm_dest_t *);
static void opsrsm_queued_msg_flush(opsrsm_dest_t *);
static void opsrsm_queued_msg_append(opsrsm_dest_t *, opsrsm_queued_msg_t *);

extern void apply_on_all_adapters(void (*)(adapter_t *, void *), void *);
static uint32_t opsrsm_pending_bytes = 0;

static kmutex_t opsrsm_flow_tmo_lock;
static timeout_id_t opsrsm_flow_tmo_id;
static int opsrsm_flow_tmo_retries = 0;

static void opsrsm_flow_tmo(void *);
static void opsrsm_flow_tmo_cancel(void);
static void opsrsm_flow_enable(adapter_t *, void *);
static void opsrsm_sync_flow_ctl(void *);
static void opsrsm_sync_flow_tmo(void *);
static void opsrsm_set_sync_flow_tmo(opsrsm_dest_t *);
static void opsrsm_cancel_sync_flow_tmo(opsrsm_dest_t *);
static void opsrsm_check_flow_ctl(opsrsm_dest_t *);
static int opsrsmdemux_loopback(mblk_t *);
static void opsrsm_status_check_tmo(void *);

taskq_t *opsrsm_failover_taskq;
taskq_t *opsrsm_events_taskq;

#define	OPSRSM_Q_LEN(q) ((q)->q_len)
#define	OPSRSM_Q_HEAD(q) ((q)->q_head)
#define	OPSRSM_Q_NEXT(q, mp) ((mp)->b_next)

#define	OPSRSM_Q_INIT(q) {	\
	(q)->q_head = NULL;	\
	(q)->q_tail = NULL;	\
	(q)->q_len = 0;		\
}

#define	OPSRSM_Q_APPEND(q, mp) {		\
	ASSERT((mp)->b_next == NULL);		\
	if ((q)->q_head == NULL) {		\
		(q)->q_head = (mp);		\
		(q)->q_tail = (mp);		\
	} else {				\
		(q)->q_tail->b_next = (mp);	\
		(q)->q_tail = (mp);		\
	}					\
	(q)->q_len++;				\
}

#define	OPSRSM_Q_REMOVE(q, mp) {		\
	ASSERT((q)->q_len > 0);			\
	(mp) = (q)->q_head;			\
	if ((q)->q_head == (q)->q_tail) {	\
		(q)->q_tail = NULL;		\
	}					\
	(q)->q_head = (mp)->b_next;		\
	(mp)->b_next = NULL;			\
	(q)->q_len--;				\
}

#define	OPSRSM_Q_FLUSH(q) {			\
	while ((q)->q_head != NULL) {		\
		mblk_t *mp;			\
						\
		mp = (q)->q_head;		\
		(q)->q_head = mp->b_next;	\
		mp->b_prev = mp->b_next = NULL;	\
		mp->b_cont = NULL;		\
		freemsg(mp);			\
	}					\
	(q)->q_tail = NULL;			\
	(q)->q_len = 0;				\
}

#define	OPSRSM_Q_CONCAT(q1, q2) {		\
	if ((q1)->q_head == NULL) {		\
		(q1)->q_head = (q2)->q_head;	\
		(q1)->q_tail = (q2)->q_tail;	\
		(q1)->q_len = (q2)->q_len;	\
	} else {				\
		if ((q2)->q_len > 0) {		\
			(q1)->q_tail->b_next = (q2)->q_head;	\
			(q1)->q_tail = (q2)->q_tail;		\
			(q1)->q_len += (q2)->q_len;		\
		}						\
	}							\
	(q2)->q_head = NULL;					\
	(q2)->q_tail = NULL;					\
	(q2)->q_len = 0;					\
}

#define	OPSRSM_REACHED_STATIC_DATA_THRESHOLD(rd) \
	((rd)->rd_data_collected >= opsrsmdev-> \
	opsrsm_param.opsrsm_data_threshold)

#define	OPSRSM_REACHED_DATA_THRESHOLD(rd)				\
	((opsrsmdev->opsrsm_param.opsrsm_adaptive_intr == 1) ?		\
	((rd)->rd_data_collected >= (rd)->rd_adaptive_threshold) :	\
	(OPSRSM_REACHED_STATIC_DATA_THRESHOLD(rd)))

#define	OPSRSM_ADAPT_THRESHOLD(rd, pktlen) { 				\
	if (opsrsmdev->opsrsm_param.opsrsm_adaptive_intr == 1) {	\
		uint32_t diff =						\
			(uint32_t)ddi_get_lbolt() - (rd)->rd_last_sent;	\
		(rd)->rd_last_sent = (uint32_t)ddi_get_lbolt();		\
		if (diff == 0) {					\
			(rd)->rd_pkt_freq++;				\
			if ((rd)->rd_pkt_freq > opsrsmdev->		\
			    opsrsm_param.opsrsm_adaptive_rate) {	\
				(rd)->rd_adaptive_threshold += pktlen;	\
				(rd)->rd_pkt_freq = 0;			\
			}						\
			if ((rd)->rd_adaptive_threshold >		\
			    opsrsmdev->opsrsm_param.			\
			    opsrsm_data_threshold)			\
				(rd)->rd_adaptive_threshold =		\
				opsrsmdev->opsrsm_param.		\
				opsrsm_data_threshold;			\
		} else {						\
			uint32_t reduce = 2 * pktlen * diff;		\
			if (reduce > (rd)->rd_adaptive_threshold) {	\
				(rd)->rd_adaptive_threshold = 0;	\
			} else {					\
				(rd)->rd_adaptive_threshold -= reduce;	\
			}						\
		}							\
	}								\
}

#define	OPSRSM_NO_PENDING_WRITES(rd) \
	((rd)->rd_writes_completed == OPSRSM_Q_LEN(&(rd)->rd_pendq))

#define	OPSRSM_RSREF(rp) { 		\
	mutex_enter(&(rp)->rs_lock);	\
	(rp)->rs_refcnt++;		\
	mutex_exit(&(rp)->rs_lock);	\
}

#define	OPSRSM_RSUNREF(rp) { 				\
	mutex_enter(&(rp)->rs_lock);			\
	(rp)->rs_refcnt--;				\
	if ((rp)->rs_refcnt == 0) {			\
		cv_broadcast(&(rp)->rs_close_cv);	\
	}						\
	mutex_exit(&(rp)->rs_lock);			\
}

#define	OPSRSM_LOOPBACK		0x100b
#define	OPSRSM_IS_LOOPBACK(rd)	((uint32_t)(rd) == OPSRSM_LOOPBACK)

static int opsrsm_start_batch(opsrsm_dest_t *, uint32_t);
static int opsrsm_end_batch(opsrsm_dest_t *);
static void opsrsm_xmit_tmo(void *);
static void opsrsm_fqe_tmo(void *);
static void opsrsm_dispatch_tmo(void *);
static void opsrsm_set_xmit_tmo(opsrsm_dest_t *, int);
static void opsrsm_set_fqe_tmo(opsrsm_dest_t *, int);
static void opsrsm_cancel_xmit_tmo(opsrsm_dest_t *);
static void opsrsm_cancel_fqe_tmo(opsrsm_dest_t *);
static void opsrsmxmit_thread(void *);
static int opsrsmxmit(opsrsm_dest_t *, mblk_t *);
static int opsrsmrexmit(opsrsm_dest_t *);
static int opsrsm_write_data(opsrsm_dest_t *, mblk_t *);
static int opsrsm_sync_dqe(opsrsm_dest_t *);
static int opsrsm_sync_fqe(opsrsm_dest_t *);
static void opsrsm_wake_senders(opsrsm_dest_t *, short);
static void opsrsm_sync_dqe_tmo(void *);
static void opsrsm_sync_fqe_tmo(void *);
static void opsrsmdemux(mblk_t *, opsrsm_dest_t *);
static void opsrsm_event_thread(void *);
static void opsrsm_event_add(opsrsm_dest_t *, uint32_t);

static opsrsmresource_t *opsrsmresource_alloc(minor_t *);
static opsrsmresource_t *opsrsm_resstruct_alloc();
static opsrsmresource_t *opsrsmresource_free(minor_t rnum);
static opsrsmresource_t *opsrsmresource_lookup(minor_t, int);
static void opsrsmresource_destroy(void);
static int opsrsm_resstruct_free(minor_t);
static void opsrsmresource_init(void);
static void opsrsmresource_fini(void);
static struct opsrsmresource_table opsrsm_resource;

static opsrsm_failover_info_t *opsrsm_finfo_list;
static kmutex_t opsrsm_finfo_lock;
static kcondvar_t opsrsm_finfo_cv;
static int opsrsm_failover_threads;
static int opsrsm_failover_max_retries = 6000;
static int opsrsm_failover_destruct_time = 3000;
static int opsrsm_queued_msg_max_retries = 2000;

int rsmrdt_adapterinit(adapter_t *);
int rsmrdt_adapterfini(adapter_t *);
void rsmrdt_failover(adapter_t *, rsm_addr_t);
int rsmrdt_check_openhandles(void);

/* LINTED: E_STATIC_FUNC_CALLD_NOT_DEFINED */
extern mblk_t	*desballoc(unsigned char *, size_t, uint_t, frtn_t *);
extern void rsmrdt_pathmanager_init(void);
extern void rsmrdt_pathmanager_cleanup(void);
extern rsm_addr_t rsmrdt_get_remote_hwaddr(adapter_t *, rsm_node_id_t);
extern adapter_t *rsmrdt_select_adapter(rsm_node_id_t, int);
extern void rsmrdt_get_remote_ids(adapter_t *, rsm_addr_t, int *, int *);

/*
 * The opsrsm driver implements a reference count scheme for destination
 * structures.  The idea behind the scheme is to prevent the driver from
 * deleting a destination structure while it is being used elsewhere, for
 * example in a message handling routine.  (Failures to protect against
 * this occurrence have led to a fair array of baffling bugs over the
 * lifetime of the driver.)
 *
 * The following set of macros implement the reference count scheme,
 * translation from RSM address to destination structure, and removal of
 * destinations from the run queue.  All must be intertwined, since
 * otherwise it would be possible to get a destination pointer from an RSM
 * address , or from the run queue, but have some other part of the driver
 * delete the destination before you could bump its reference count.  The
 * incorporation of reference count code in FINDDEST/MAKEDEST/GETRUNQ
 * solves this race condition.
 */

/*
 * FINDDEST attempts to find the destination with RSM address rsm_addr.  If the
 * destination exists, rd is set to point to it.  If the destination exists,
 * isdel is set to indicate whether the destination is currently being deleted
 * (nonzero implies a delete is in progress).  If the destination exists and
 * is not being deleted, its reference count is increased by one.
 */
#define	FINDDEST(rd, isdel, rsm_addr, adapter) {		\
	mutex_enter(&adapter->opsrsm_dest_lock);		\
	(rd) = (((rsm_addr) >= RSM_MAX_DESTADDR) ? NULL :	\
	    (adapter)->opsrsm_desttbl[(rsm_addr)]);		\
	if (rd)							\
		if (((isdel) = (rd)->rd_dstate) == 0) {		\
			(rd)->rd_refcnt++;			\
			D6("FINDDEST ctlr %d addr %ld refcnt++ is %d\n", \
			    adapter->instance, rsm_addr,	\
			    (rd)->rd_refcnt);			\
		}						\
	mutex_exit(&(adapter)->opsrsm_dest_lock);		\
}


/*
 * MAKEDEST attempts to find the destination with RSM address rsm_addr.  If the
 * destination exists, rd and isdel are set as in the description of FINDDEST,
 * above.  If the destination does not exist, a new destination structure is
 * allocated and installed, rd is set to point to it, and isnew is set to 1.
 */
#define	MAKEDEST(rd, isdel, isnew, rsm_addr, adapter) {		\
	mutex_enter(&(adapter)->opsrsm_dest_lock); 		\
	(rd) = (((rsm_addr) >= RSM_MAX_DESTADDR) ? NULL : 	\
	    (adapter)->opsrsm_desttbl[(rsm_addr)]); 		\
	if (!(rd)) { 						\
		(rd) = opsrsmmkdest((adapter), (rsm_addr)); 	\
		(isnew) = 1;					\
	}							\
	if (rd)							\
		if (((isdel) = (rd)->rd_dstate) == 0) {		\
			(rd)->rd_refcnt++;			\
			D6("MAKEDEST ctlr %d addr %ld refcnt++ is %d\n", \
			    adapter->instance, (uint64_t)rsm_addr,	\
			    (rd)->rd_refcnt);			\
		}						\
	mutex_exit(&(adapter)->opsrsm_dest_lock);		\
}


/*
 * GETRUNQ attempts to return the destination which is at the head of opsrsm's
 * run queue.  If the run queue is non-empty, the head of the queue is removed,
 * and rd is set to point to it; otherwise, rd is set to NULL.  If rd is
 * nonzero, isdel is set to 1 if the destination pointed to by rd is being
 * deleted, or to 0 otherwise.  Finally, if rd is nonzero, and isdel is zero,
 * then rd's reference count is increased by one.
 */
#define	GETRUNQ(rd, isdel, adapterp) {				\
	mutex_enter(&(adapterp)->opsrsm_dest_lock);		\
	mutex_enter(&(adapterp)->opsrsm_runq_lock);		\
	rd = (adapterp)->opsrsm_runq;				\
	if (rd) {						\
		(adapterp)->opsrsm_runq = rd->rd_next;		\
		if (((isdel) = (rd)->rd_dstate) == 0) {		\
			(rd)->rd_refcnt++;			\
			D6("GETRUNQ ctlr %d addr %ld refcnt++ is %d\n", \
			    adapterp->instance,			\
			    (rd)->rd_rsm_addr,			\
			    (rd)->rd_refcnt);			\
		}						\
	}							\
	mutex_exit(&(adapterp)->opsrsm_runq_lock);		\
	mutex_exit(&(adapterp)->opsrsm_dest_lock);		\
}


/*
 * REFDEST checks to see if the destination pointed to by rd is currently being
 * deleted.  If so, isdel is set to a nonzero value; otherwise, it is set to
 * zero, and the destination's reference count is incremented.
 */
#define	REFDEST(rd, isdel) {					\
	mutex_enter(&(rd)->rd_adapter->opsrsm_dest_lock);	\
	if (((isdel) = (rd)->rd_dstate) == 0) {			\
		(rd)->rd_refcnt++;				\
	}							\
	mutex_exit(&(rd)->rd_adapter->opsrsm_dest_lock);	\
}


/*
 * UNREFDEST decrements the reference count of the destination pointed to by
 * rd.  If the reference count becomes zero, we start the deletion process for
 * the destination.
 */
#define	UNREFDEST(rd) {						\
	mutex_enter(&(rd)->rd_adapter->opsrsm_dest_lock);		\
	D6("UNREFDEST ctlr %d addr %ld refcnt-- is %d\n",	\
	    (rd)->rd_adapter->instance, (rd)->rd_rsm_addr,	\
	    (rd)->rd_refcnt - 1);				\
	if (--(rd)->rd_refcnt <= 0) {				\
		mutex_exit(&(rd)->rd_adapter->opsrsm_dest_lock);	\
		if (opsrsmdest_refcnt_0(rd)) { rd = NULL; }	\
	} else							\
		mutex_exit(&(rd)->rd_adapter->opsrsm_dest_lock);	\
}



/* Local Static def's */

/*
 * Lock and variable to allow attach routines to initialize global mutexes
 */

static kmutex_t opsrsmattlock;	/* Protects opsrsmdbginit  */

/*
 * Pointer to 'opsrsm' global structure.
 */
opsrsm_t *opsrsmdev = NULL;	/* Head of list */

static kmutex_t opsrsmdevlock;	/* Protects list contents */
static void *opsrsm_state;	/* opaque handle for soft state structs */

extern rsm_node_id_t rsmrdt_my_nodeid;

/*
 * ****************************************************************
 *                                                               *
 * B E G I N   BASIC MODULE BOILERPLATE                          *
 *                                                               *
 * ****************************************************************
 */


/* Module Loading/Unloading and Autoconfiguration declarations */

/*
 * cb_ops contains the driver entry points and is roughly equivalent
 * to the cdevsw and bdevsw  structures in previous releases.
 *
 * dev_ops contains, in addition to the pointer to cb_ops, the routines
 * that support loading and unloading our driver.
 *
 */

static struct cb_ops opsrsm_cb_ops = {
	opsrsm_open,		/* cb_open */
	opsrsm_close,		/* cb_close */
	nodev,			/* cb_strategy */
	nodev,			/* cb_print */
	nodev,			/* cb_dump */
	nodev,			/* cb_read */
	nodev,			/* cb_write */
	opsrsm_ioctl,		/* cb_ioctl */
	nodev,			/* cb_devmap */
	nodev,			/* cb_mmap */
	nodev,			/* cb_segmap */
	opsrsm_chpoll,		/* cb_chpoll */
	ddi_prop_op,		/* cb_prop_op */
	NULL,			/* cb_stream */
	D_NEW | D_MP,		/* cb_flag */
	CB_REV,			/* rev */
	nodev,			/* int (*cb_aread)() */
	nodev			/* int (*cb_awrite)() */
};

static struct dev_ops opsrsm_ops = {
	DEVO_REV,		/* devo_rev */
	0,			/* devo_refcnt */
	opsrsm_info,		/* devo_getinfo */
	nulldev,		/* devo_identify */
	nulldev,		/* devo_probe */
	opsrsm_attach,		/* devo_attach */
	opsrsm_detach,		/* devo_detach */
	nodev,			/* devo_reset */
	&opsrsm_cb_ops,		/* devo_cb_ops */
	(struct bus_ops *)NULL,	/* devo_bus_ops */
	nulldev			/* power */
};


/*
 * Module linkage information for the kernel.
 */
static struct modldrv modldrv = {
	&mod_driverops,
	"Reliable Datagram Transport driver - v1.0",
	&opsrsm_ops,
};

static struct modlinkage modlinkage = {
#ifdef _LP64
	MODREV_1, { (void *) &modldrv, NULL, NULL, NULL, NULL, NULL, NULL }
#else
	MODREV_1, { (void *) &modldrv, NULL, NULL, NULL }
#endif
};

/*
 * Module Loading and Installation Routines.
 */

/*
 * Module Installation
 * Install the driver, initialize soft state system, initialize opsrsmattlock
 */

int
_init(void)
{
	int status;

	status = ddi_soft_state_init(&opsrsm_state, sizeof (opsrsm_t), 1);
	if (status != 0) {
#ifdef DEBUG
		cmn_err(CE_CONT,
		    "opsrsm:_init - soft_state_init failed: 0x%x\n", status);
#endif /* DEBUG */
		return (status);
	}

	/* initialize global locks here */
	mutex_init(&opsrsmattlock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&opsrsmdevlock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&opsrsm_flow_tmo_lock, NULL, MUTEX_DRIVER, NULL);
	opsrsm_flow_tmo_id = 0;

	opsrsm_events_taskq = taskq_create("events", 8, maxclsyspri, 1, 8,
	    TASKQ_PREPOPULATE);

	opsrsm_finfo_init();
	opsrsmresource_init();

	status = mod_install(&modlinkage);
	if (status != DDI_SUCCESS) {
		mutex_destroy(&opsrsmattlock);
		mutex_destroy(&opsrsmdevlock);
	}

	/* Init rsmrdt pm client */
	rsmrdt_pathmanager_init();

	return (status);
}

/*
 * Module Removal
 */

int
_fini(void)
{
	int status;

	if ((status = mod_remove(&modlinkage)) != 0) {
		DERR("opsrsm_fini - mod_remove failed: 0x%x\n", status);
		return (status);
	}

	/* Un-init the rsmrdt pm client */
	rsmrdt_pathmanager_cleanup();

	ddi_soft_state_fini(&opsrsm_state);
	opsrsmresource_fini();
	opsrsm_finfo_fini();
	opsrsm_flow_tmo_cancel();
	taskq_destroy(opsrsm_events_taskq);

	mutex_destroy(&opsrsm_flow_tmo_lock);
	mutex_destroy(&opsrsmattlock);
	mutex_destroy(&opsrsmdevlock);

	return (status);
}

/*
 * Return Module Info.
 */

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}



/*
 * Autoconfiguration Routines
 */


/*
 * Attach the device, create and fill in the device-specific structure.
 */

static int
opsrsm_attach(dev_info_t *dip, ddi_attach_cmd_t cmd)
{
	opsrsm_t *opsrsmp;
	int instance;
	int progress = 0;
	minor_t rnum;

	D1("opsrsmattach: dip 0x%p, cmd %d", (void *)dip, cmd);

	if (cmd != DDI_ATTACH) {
		return (DDI_FAILURE);
	}

	/*
	 * Allocate soft data structure
	 */

	instance = ddi_get_instance(dip);

	if (ddi_soft_state_zalloc(opsrsm_state, instance) != DDI_SUCCESS) {
		DERR("opsrsmattach: bad state zalloc, returning DDI_FAILURE");
		return (DDI_FAILURE);
	}

	opsrsmp = ddi_get_soft_state(opsrsm_state, instance);
	if (opsrsmp == NULL) {
		return (DDI_FAILURE);
	}

	/*
	 * Stuff private info into dip.
	 */
	opsrsmp->opsrsm_dip = dip;
	ddi_set_driver_private(dip, (caddr_t)opsrsmp);

	/*
	 * Get device parameters from the device tree and save them in our
	 * per-device structure for later use.
	 */
	opsrsmgetparam(dip, opsrsmp);

	/*
	 * Initialize kernel statistics.
	 */
	opsrsmkstatinit(opsrsmp);
	progress |= OPSRSM_ATT_KSTAT;
	/*
	 * Link this per-device structure in with the rest.
	 */
	mutex_enter(&opsrsmdevlock);

	opsrsmdev = opsrsmp;
	mutex_exit(&opsrsmdevlock);

	/*
	 * Create minor number
	 */
	if (opsrsmresource_alloc(&rnum) == NULL) {
		DERR("opsrsmattach: Unable to get minor number\n");
		opsrsmtakedown(opsrsmp, progress);
		return (DDI_FAILURE);
	}

	D1("opsrsmattach: rnum %d : ddi %d", rnum, ddi_get_instance(dip));

	/*
	 * Create the filesystem device node.
	 */
	if (ddi_create_minor_node(dip, OPSRSMNAME, S_IFCHR,
	    rnum, DDI_PSEUDO, NULL) != DDI_SUCCESS) {
		DERR("opsrsmattach: bad create_minor_node, returning "
		    "DDI_FAILURE");
		opsrsmtakedown(opsrsmp, progress);
		return (DDI_FAILURE);
	}

	progress |= OPSRSM_ATT_MINOR;

	opsrsmp->opsrsm_max_batch_size = 0;
	opsrsmp->opsrsm_min_batch_size = 0;

	ddi_report_dev(dip);

	D1("opsrsmattach: returning DDI_SUCCESS");
	return (DDI_SUCCESS);
}

/*
 * Detach - Free resources allocated in attach
 */

/*ARGSUSED*/
static int
opsrsm_detach(dev_info_t *dip, ddi_detach_cmd_t cmd)
{
	int instance;
	opsrsm_t *opsrsmp;

	D1("opsrsmdetach: dip 0x%p, cmd %d", (void *)dip, cmd);

	if (cmd != DDI_DETACH) {
		return (DDI_FAILURE);
	}

	if (rsmrdt_check_openhandles() != 0) {
		DERR("opsrsmdetach: Failed to detach due to open handles");
		return (DDI_FAILURE);
	}

	instance = ddi_get_instance(dip);
	opsrsmp = ddi_get_soft_state(opsrsm_state, instance);
	if (opsrsmp == NULL) {
		return (DDI_FAILURE);
	}


	/*
	 * Release all our resources. At this point, all attachment
	 * setup must have completed, so must all be torn down.
	 */
	opsrsmtakedown(opsrsmp, OPSRSM_ATT_ALL);

	/*
	 * Free resource table
	 */
	opsrsmresource_destroy();
	return (DDI_SUCCESS);
}

/*ARGSUSED*/
static int
opsrsm_info(dev_info_t *dip, ddi_info_cmd_t infocmd, void *arg, void **result)
{
	switch (infocmd) {
	case DDI_INFO_DEVT2DEVINFO:
		if (opsrsmdev !=  NULL) {
			*result = opsrsmdev->opsrsm_dip;
			return (DDI_SUCCESS);
		} else {
			return (DDI_FAILURE);
		}

	case DDI_INFO_DEVT2INSTANCE:
		*result = 0;
		return (DDI_SUCCESS);

	default:
		return (DDI_FAILURE);
	}
}

/*
 * Return local node id.
 */
/* ARGSUSED */
static int
opsrsm_ioctl_getnodeid(opsrsmresource_t *rp, intptr_t arg, int mode)
{
	if (rsmrdt_my_nodeid == (rsm_node_id_t)-1)
		return (ENXIO);

	(void) ddi_copyout((caddr_t)&rsmrdt_my_nodeid, (caddr_t)arg,
		sizeof (rsmrdt_my_nodeid), mode);
	return (0);
}

/*
 * Return a unique(wrt the local node) number associated with
 * the communication endpoint.
 */
static int
opsrsm_ioctl_bind(opsrsmresource_t *rp, intptr_t arg, int mode)
{
	if (ddi_copyout((caddr_t)&rp->rs_lportnum, (caddr_t)arg,
	    sizeof (rp->rs_lportnum), mode) != DDI_SUCCESS) {
		DERR("ioctl_bind: unable to copyout portnum");
		return (EFAULT);
	}
	rp->rs_state |= OPSRSM_RS_BOUND;
	return (0);
}

/*
 * The local communication endpoint will simply remember and will use
 * the address specified here as the target address for all the future
 * outgoing messages.
 *
 * Note that it will not verify the validity of the remote "portnum".
 * It's up to the applications to make sure the remote endpoint
 * exists before send.
*/
static int
opsrsm_ioctl_connect(opsrsmresource_t *rp, intptr_t arg, int mode)
{
	rsmrdt_connect_arg_t io_args;

	if ((rp->rs_state & OPSRSM_RS_BOUND) == 0) {
		DERR("ioctl_connect: port not bound yet");
		return (EADDRNOTAVAIL);
	}

	rw_enter(&opsrsm_resource.opsrsmrct_lock, RW_READER);
	if (opsrsm_resource.opsrsmrc_flag == OPSRSMRC_UNLOAD_INPROGRESS) {
		DERR("ioctl_connect: Unloading in progress");
		rw_exit(&opsrsm_resource.opsrsmrct_lock);
		return (ENETDOWN);
	}
	rw_exit(&opsrsm_resource.opsrsmrct_lock);

	if (rp->rs_dest != NULL) {
		DERR("ioctl_connect: reconnect not supported");
		return (EISCONN);
	}

	/*
	 * Copy in the connect ioctl arg structure
	 */
	(void) ddi_copyin((caddr_t)arg, (caddr_t)&io_args, sizeof (io_args),
		mode);

	D1("ioctl_connect: nodeid = %d", io_args.nodeid);
	D1("ioctl_connect: portnum = %d", io_args.portnum);

	if (io_args.nodeid < 0)
		return (EINVAL);

	if (io_args.nodeid == (int)rsmrdt_my_nodeid) {
		/*
		 * Loopback mode
		 */
		mutex_enter(&rp->rs_lock);
		rp->rs_dest = (opsrsm_dest_t *)OPSRSM_LOOPBACK;
		rp->rs_local_skey = 0;
		rp->rs_rportnum = io_args.portnum;
		mutex_exit(&rp->rs_lock);
	} else {
		opsrsm_dest_t *rd = NULL;

		mutex_enter(&rp->rs_lock);
		rp->rs_nodeid = io_args.nodeid;
		rp->rs_rportnum = io_args.portnum;
		rp->rs_state |= OPSRSM_RS_CONNECTING;
		mutex_exit(&rp->rs_lock);

		rd = opsrsm_connect(rp->rs_nodeid, rp);
		mutex_enter(&rp->rs_lock);
		rp->rs_dest = rd;
		if (rd != NULL) {
			rp->rs_local_skey = rd->rd_local_skey;
			rp->rs_state |= OPSRSM_RS_REFDEST;
		} else {
			rp->rs_local_skey = 0;
		}
		rp->rs_state &= ~OPSRSM_RS_CONNECTING;
		cv_broadcast(&rp->rs_conn_cv);
		mutex_exit(&rp->rs_lock);

		if (rd == NULL) {
			return (ENETDOWN);
		}
	}
	return (0);
}


#define	RETRY_DELAY 1
static int opsrsm_connect_max_retries = 20;

static opsrsm_dest_t *
opsrsm_connect(int nodeid, opsrsmresource_t *rp)
{
	adapter_t *adp = NULL;
	rsm_addr_t rem_hwaddr;
	opsrsm_dest_t *rd;
	int isdel = 0, isnew = 0;
	int newdest = 0;
	uint32_t old_skey = 0;

again:;
	/* Find local adapter */
	if (newdest > opsrsm_connect_max_retries) {
		DINFO("connect: failed to connect to node %d\n", nodeid);
		return (NULL);
	}
	adp = rsmrdt_select_adapter((rsm_node_id_t)nodeid, newdest);
	if (adp == NULL) {
		DINFO("connect: node %d is unreachable\n", nodeid);
		return (NULL);
	}

	/*
	 * if path down happens after we've chosen an adapter, that's
	 * still ok because the connection handshake will fail.
	 */

	/* Find remote hw addr */
	rem_hwaddr = rsmrdt_get_remote_hwaddr(adp, (rsm_node_id_t)nodeid);
	if (rem_hwaddr == (rsm_addr_t)-1 || rem_hwaddr > RSM_MAX_DESTADDR) {
		if (opsrsmdev->opsrsm_param.rsmrdt_enable_loadbalance) {
			if (adp->sel_cnt > 0) adp->sel_cnt--;
		}
		return (NULL);
	}

	MAKEDEST(rd, isdel, isnew, rem_hwaddr, adp);
	if (isdel) {
		if (opsrsmdev->opsrsm_param.rsmrdt_enable_loadbalance) {
			if (adp->sel_cnt > 0) adp->sel_cnt--;
		}
		/*
		 * need sufficient delay to ensure the old rd gets freed up
		 * completely before MAKEDEST gets called again
		 */
		delay(RETRY_DELAY);
		goto again;
	}
	if (isnew) {
		isnew = 0;
		if (rd == NULL) goto again;
		(void) opsrsmmovestate(rd, OPSRSM_STATE_NEW,
		    OPSRSM_STATE_S_REQ_CONNECT);
	}
	if (rd->rd_local_skey != old_skey) {
		old_skey = rd->rd_local_skey;
		newdest++;
	}

	mutex_enter(&rd->rd_xmit_lock);
	if (rd->rd_nodeid != nodeid) {
		cmn_err(CE_PANIC, "invalid nodeid %d, expected %d\n",
		    rd->rd_nodeid, nodeid);
	}
	if (rd->rd_xmit_state < OPSRSM_XMIT_BARRIER_CLOSED) {
		if (rd->rd_xmit_state != OPSRSM_XMIT_DISCONNECTED) {
			int retval;

			retval = cv_wait_sig(&rd->rd_conn_cv,
			    &rd->rd_xmit_lock);
			if (retval == 0) {
				mutex_exit(&rd->rd_xmit_lock);
				if (opsrsmdev->opsrsm_param.
				    rsmrdt_enable_loadbalance) {
					if (adp->sel_cnt > 0)
						adp->sel_cnt--;
				}
				UNREFDEST(rd);
				return (NULL);
			} else {
				if (rd->rd_xmit_state >=
				    OPSRSM_XMIT_BARRIER_CLOSED) {
					mutex_exit(&rd->rd_xmit_lock);
					return (rd);
				} else {
					mutex_exit(&rd->rd_xmit_lock);
					if (opsrsmdev->opsrsm_param.
					    rsmrdt_enable_loadbalance) {
						if (adp->sel_cnt > 0)
							adp->sel_cnt--;
					}
					UNREFDEST(rd);
					goto again;
				}
			}
		} else {
			mutex_exit(&rd->rd_xmit_lock);
			if (opsrsmdev->opsrsm_param.
			    rsmrdt_enable_loadbalance) {
				if (adp->sel_cnt > 0) adp->sel_cnt--;
			}
			UNREFDEST(rd);
			/*
			 * if rd is still in DISCONNECTED state, we need to
			 * wait until it is completely freed up.
			 */
			if (rp != NULL) {
				mutex_enter(&rp->rs_lock);
				cv_broadcast(&rp->rs_conn_cv);
				mutex_exit(&rp->rs_lock);
			}
			delay(RETRY_DELAY);
			goto again;
		}
	}
	mutex_exit(&rd->rd_xmit_lock);
	return (rd);
}

/*
 * RSMRDT_IOCTL_SENDMSG deals with one message at a time. It maintains
 * message boundary, and guarantees either the whole message, or none
 * of it is delivered to the destination successfully.
 *
 * EMSGSIZE is returned when the message is too big to be handled as a
 * single message by the opsrsm driver.
 *
 * Note that SKGXP library doesn't require the send socket to have a
 * portnum associated with it. In other words, the send side can send
 * msgs through a socket without calling ioctl.bind() first.
 *
 * RSMRDT_IOCTL_SENDMSG is always non-blocking.
 *
 * Normally messages are guaranteed to be delivered eventually to the
 * destination endpoint, in the same order RSMRDT_IOCTL_SENDMSG calls
 * are made. There is one notable exception. That is, if the destination
 * endpoint doesn't exist, the msg will be dropped by the receiving node.
 */
static int
opsrsm_ioctl_sendmsg(opsrsmresource_t *rp, intptr_t arg, int mode)
{
	rsmrdt_send_arg_t io_args;
	struct iovec vptr[OPSRSM_MAXVECS];
#ifdef _MULTI_DATAMODEL
	rsmrdt_send_arg32_t io_args32;
	struct iovec32 vptr32[OPSRSM_MAXVECS];
	model_t model;
#endif /* _MULTI_DATAMODEL */
	uio_t phys_uio;
	mblk_t *mp;
	int bytecount = 0;
	uint_t nvecs;
	int i;
	int err = 0;

	if ((rp->rs_state & OPSRSM_RS_NORECVR) != 0) {
		DERR("ioctl_sendmsg: Receiver doesn't exist");
		return (ESRCH);
	}

	if ((rp->rs_state & OPSRSM_RS_PKEYMISMATCH) != 0) {
		DERR("ioctl_sendmsg: pkey mismatch");
		return (EACCES);
	}

	if (rp->rs_dest == NULL) {
		return (ENOTCONN);
	}
#ifdef _MULTI_DATAMODEL
	model = ddi_model_convert_from(mode & FMODELS);
	if (model == DDI_MODEL_ILP32) {
		/*
		 * Copy in sendmsg arg structure to driver buffer
		 */
		ddi_copyin((caddr_t)arg, &io_args32, sizeof (io_args32),
		    mode);

		/*
		 * Find number of iovecs for this message
		 */
		nvecs = io_args32.iovcnt;

		if (nvecs > OPSRSM_MAXVECS) {
			DERR("ioctl_sendmsg: invalid vec size 0x%x", nvecs);
			return (EINVAL);
		}

		D1("ioctl_sendmsg: nvecs = %d, sz = 0x%x, vptr32 = %lx",
		    nvecs, nvecs * sizeof (struct iovec32), vptr32);

		/*
		 * Copy in iovec structures to driver buffer
		 */
		if (ddi_copyin((struct iovec32 *)io_args32.iov, (caddr_t)vptr32,
			nvecs * sizeof (struct iovec32), mode)) {
			DERR("ioctl_sendmsg: invalid iovec pointer");
			err = EFAULT;
			goto done;
		}

		/*
		 * Find out the size of this message
		 */
		bytecount = 0;
		for (i = 0; i < nvecs; i++) {
			ssize32_t iovlen32 = vptr32[i].iov_len;
			bytecount += iovlen32;
			if (iovlen32 < 0 || bytecount < 0) {
				err = EINVAL;
				goto done;
			}
			vptr[i].iov_len = iovlen32;
			vptr[i].iov_base = (caddr_t)vptr32[i].iov_base;
		}
	} else
#endif /* _MULTI_DATAMODEL */
	{
		/*
		 * Copy in sendmsg arg structure to driver buffer
		 */
		(void) ddi_copyin((caddr_t)arg, (caddr_t)&io_args,
			sizeof (io_args), mode);

		/*
		 * Find number of iovecs for this message
		 */
		nvecs = io_args.iovcnt;

		if (nvecs > OPSRSM_MAXVECS) {
			DERR("ioctl_sendmsg: invalid vec size 0x%x", nvecs);
			return (EINVAL);
		}

		D1("ioctl_sendmsg: nvecs = %d, sz = 0x%x, vptr = %p",
		    nvecs, nvecs * (int)sizeof (iovec_t), vptr);

		/*
		 * Copy in iovec structures to driver buffer
		 */
		if (ddi_copyin(io_args.iov, (caddr_t)vptr,
		    (size_t)nvecs * sizeof (iovec_t), mode)) {
			DERR("ioctl_sendmsg: invalid iovec pointer");
			err = EFAULT;
			goto done;
		}

		/*
		 * Find out the size of this message
		 */
		bytecount = 0;
		for (i = 0; i < (int)nvecs; i++) {
			ssize_t iovlen = vptr[i].iov_len;
			bytecount += iovlen;
			if (iovlen < 0 || bytecount < 0) {
				err = EINVAL;
				goto done;
			}
		}
	}

	D1("ioctl_sendmsg: Message Size 0x%x", bytecount);

	/*
	 * Check if message + header size is bigger than MTU size
	 */
	if ((bytecount + OPSRSM_CACHELINE_SIZE) >
		OPSRSM_MAX_BUFFER_SIZE_DFLT) {
		DERR("ioctl_sendmsg: message too big");
		err = EMSGSIZE;
		goto done;
	}

	/*
	 * Allocate mblk
	 */
	mp = allocb(OPSRSM_CACHELINE_SIZE + OPSRSM_MESSAGE_HDRSZ +
		(size_t)bytecount + OPSRSM_CACHELINE_SIZE, BPRI_LO);
	if (mp == NULL) {
		DERR("ioctl_sendmsg: allocb failed");
		err = ENOMEM;
		goto done;
	}
	mp->b_rptr = (uchar_t *)OPSRSM_CACHELINE_ROUNDUP(mp->b_rptr);

	/*
	 * Stuff in the message header
	 */
	OPSRSM_MESSAGE_HDRPTR(mp)->lportnum = rp->rs_lportnum;
	OPSRSM_MESSAGE_HDRPTR(mp)->rportnum = rp->rs_rportnum;
	OPSRSM_MESSAGE_HDRPTR(mp)->msg_sz = (uint32_t)bytecount;
	OPSRSM_MESSAGE_HDRPTR(mp)->nodeid = (int)rsmrdt_my_nodeid;
	OPSRSM_MESSAGE_HDRPTR(mp)->pkey = rp->rs_pkey;
	OPSRSM_MESSAGE_HDRPTR(mp)->seqno = 0;
	OPSRSM_MESSAGE_HDRPTR(mp)->option = 0;

	D1("ioctl_sendmsg: lportnum = %d, rportnum = %d, msz_sz = 0x%x",
		rp->rs_lportnum, rp->rs_rportnum, bytecount);
	D1("ioctl_sendmsg: my node id = %d", rsmrdt_my_nodeid);

	/*
	 * Initialize uio structure
	 */
	phys_uio.uio_iov = vptr;
	phys_uio.uio_iovcnt = (int)nvecs;
	phys_uio.uio_resid = bytecount;
	phys_uio.uio_segflg = UIO_USERSPACE;

	if (uiomove((caddr_t)mp->b_rptr + OPSRSM_MESSAGE_HDRSZ,
		(size_t)bytecount, UIO_WRITE, &phys_uio)) {
		DERR("ioctl_sendmsg: uiomove failed");
		err = EFAULT;
		freemsg(mp);
		goto done;
	}

	mp->b_wptr = mp->b_rptr + bytecount + OPSRSM_MESSAGE_HDRSZ;
	mp->b_prev = mp->b_cont = NULL;

	if (OPSRSM_IS_LOOPBACK(rp->rs_dest)) {
		err = opsrsmdemux_loopback(mp);
	} else {
		int isdel = 0;

		mutex_enter(&rp->rs_lock);
		if ((rp->rs_state & OPSRSM_RS_FAILOVER) != 0) {
			opsrsm_dest_t *new_rd = NULL;

			ASSERT((rp->rs_state & OPSRSM_RS_REFDEST) == 0);
			mutex_exit(&rp->rs_lock);
			err = opsrsm_finfo_wait(rp->rs_local_skey);
			if (err == 0) {
				mutex_enter(&rp->rs_lock);
				rp->rs_state |= OPSRSM_RS_CONNECTING;
				mutex_exit(&rp->rs_lock);
				new_rd = opsrsm_connect(rp->rs_nodeid, rp);
			}
			mutex_enter(&rp->rs_lock);
			rp->rs_dest = new_rd;
			rp->rs_state &= ~OPSRSM_RS_CONNECTING;
			if (new_rd != NULL) {
				rp->rs_local_skey = new_rd->rd_local_skey;
				rp->rs_state &= ~OPSRSM_RS_FAILOVER;
				rp->rs_state |= OPSRSM_RS_REFDEST;
				cv_broadcast(&rp->rs_conn_cv);
			} else {
				rp->rs_local_skey = 0;
				freemsg(mp);
				cv_broadcast(&rp->rs_conn_cv);
				mutex_exit(&rp->rs_lock);
				goto done;
			}
		}
		REFDEST(rp->rs_dest, isdel);
		if (isdel != 0) {
			err = ENETDOWN;
			freemsg(mp);
			mutex_exit(&rp->rs_lock);
			goto done;
		}

		if ((bytecount + OPSRSM_CACHELINE_SIZE) >
		    (int)rp->rs_dest->rd_buffer_size) {
			err = EMSGSIZE;
			freemsg(mp);
			mutex_exit(&rp->rs_lock);
			goto done;
		}
		mutex_exit(&rp->rs_lock);
		err =  opsrsmxmit(rp->rs_dest, mp);
		if (err != EWOULDBLOCK) err = 0;
	}
done:;
	return (err);
}


/*
 * RSMRDT_IOCTL_RECVMSG can return more than one msg at a time.
 *
 * If a message is too long to fit in the supplied buffer, excessive
 * bytes will be discarded.
 *
 * It will return EWOULDBLOCK if there is no message in the receive
 * queue. Caller can then use poll() to poll for the POLLIN event.
 *
 * In case of memory allocation errors, it will not drop the packets.
 */

static int
opsrsm_ioctl_recvmsgs(opsrsmresource_t *rp, intptr_t arg, int mode)
{
	rsmrdt_recvmsgs_arg_t io_args;
	rdt_recvmsg_t *rm_ptr;
	struct iovec vptr[OPSRSM_MAXVECS];
#ifdef _MULTI_DATAMODEL
	rsmrdt_recvmsgs_arg32_t io_args32;
	rsmrdt_recvmsg32_t *rm_ptr32;
	struct iovec32 vptr32[OPSRSM_MAXVECS];
	model_t model;
#endif /* _MULTI_DATAMODEL */
	int nmsgs, count;
	int err = 0;
	int32_t total_bytes = 0;

	if ((rp->rs_state & OPSRSM_RS_BOUND) == 0) {
		DERR("ioctl_recvmsgs: port not bound yet");
		return (EADDRNOTAVAIL);
	}

	/*
	 * Copy in recvmsgs arg structure to driver buffer
	 */
#ifdef _MULTI_DATAMODEL
	model = ddi_model_convert_from(mode & FMODELS);
	if (model == DDI_MODEL_ILP32) {
		ddi_copyin((caddr_t)arg, (caddr_t)&io_args32,
		    sizeof (io_args32), mode);

		io_args.msgcnt = io_args32.msgcnt;
		io_args.timeout = io_args32.timeout;
	} else
#endif /* _MULTI_DATAMODEL */
	(void) ddi_copyin((caddr_t)arg, (caddr_t)&io_args, sizeof (io_args),
		mode);

	/*
	 * Check if messages are waiting in the queue
	 */
	mutex_enter(&rp->rs_lock);
	if (OPSRSM_Q_LEN(&rp->rs_recvq) == 0) {
		if (io_args.timeout == 0) {
			mutex_exit(&rp->rs_lock);
			return (EWOULDBLOCK);
		} else if (io_args.timeout > 0) {
			clock_t	timeout_time;
			int retval;
			/*
			 * Wait only for 'timeout' time.
			 */
			timeout_time = ddi_get_lbolt();
			timeout_time += drv_usectohz
			    ((clock_t)io_args.timeout * (clock_t)1000);
			rp->rs_state |= OPSRSM_RS_SIG;
			retval = cv_timedwait_sig(&rp->rs_cv, &rp->rs_lock,
			    timeout_time);
			rp->rs_state &= ~OPSRSM_RS_SIG;
			if (retval == 0) {
				mutex_exit(&rp->rs_lock);
				return (EINTR);
			} else if (OPSRSM_Q_LEN(&rp->rs_recvq) == 0) {
				mutex_exit(&rp->rs_lock);
				err = 0;
				count = 0;
				goto done;
			}
		} else {
			int retval;

			/*
			 * Wait until you get the wakeup signal
			 */
			rp->rs_state |= OPSRSM_RS_SIG;
			retval = cv_wait_sig(&rp->rs_cv, &rp->rs_lock);
			rp->rs_state &= ~OPSRSM_RS_SIG;
			if (retval == 0) {
				mutex_exit(&rp->rs_lock);
				return (EINTR);
			} else if (OPSRSM_Q_LEN(&rp->rs_recvq) == 0) {
				mutex_exit(&rp->rs_lock);
				return (EWOULDBLOCK);
			}
		}
	}
	mutex_exit(&rp->rs_lock);

	/*
	 * Find number of messages
	 */
	nmsgs = (int)io_args.msgcnt;
	if (nmsgs > (int)opsrsmdev->opsrsm_param.opsrsm_max_recv_msgs ||
	    nmsgs < 0) {
		DERR("ioctl_recvmsgs: invalid nmsgs");
		return (EINVAL);
	}

	D1("ioctl_recvmsgs: nmsgs = %d\n", nmsgs);

	/*
	 * Copy in receive messages structures to driver buffer
	 */
#ifdef _MULTI_DATAMODEL
	if (model == DDI_MODEL_ILP32) {
		rm_ptr32 = (rsmrdt_recvmsg32_t *)rp->rs_rmptr;
		if (ddi_copyin((rsmrdt_recvmsg32_t *)io_args32.msg_iov,
		    (caddr_t)rm_ptr32, nmsgs * sizeof (rsmrdt_recvmsg32_t),
		    mode)) {
			DERR("ioctl_recvmsgs: cannot copy in msg structs");
			return (EFAULT);
		}

	} else
#endif /* _MULTI_DATAMODEL */
	{
		rm_ptr = (rdt_recvmsg_t *)rp->rs_rmptr;
		if (ddi_copyin(io_args.msg_iov, (caddr_t)rm_ptr,
			(size_t)nmsgs * sizeof (rdt_recvmsg_t), mode)) {
			DERR("ioctl_recvmsgs: cannot copy in msg structs");
			return (EFAULT);
		}
	}

	count = 0;
	do {
		uint_t nvecs;
		int i;
		uint32_t bytecount;
		uint32_t org_msglen;
		int nodeid;
		uint32_t portnum;
		mblk_t *mp;

#ifdef _MULTI_DATAMODEL
		if (model == DDI_MODEL_ILP32) {
			/*
			 * Find number of iovecs for this message
			 */
			nvecs = rm_ptr32[count].iovcnt;

			if (nvecs > OPSRSM_MAXVECS) {
				DERR("ioctl_recvmsgs: invalid vec size");
				err = EINVAL;
				break;
			}

			D1("ioctl_recvmsgs: nvecs = %d", nvecs);

			/*
			 * Copy in iovec structures to driver buffer
			 */
			if (ddi_copyin((struct iovec32 *)rm_ptr32[count].iov,
			    (caddr_t)vptr32, nvecs * sizeof (struct iovec32),
			    mode)) {
				DERR("ioctl_recvmsgs: invalid iovec pointer");
				err = EFAULT;
				break;
			}

			/*
			 * Calculate buffer size
			 */
			bytecount = 0;
			for (i = 0; i < nvecs; i++) {
				if (vptr32[i].iov_len < 0) {
					DERR("ioctl_recvmsgs: invalid iovlen");
					err = EINVAL;
					break;
				}
				bytecount += vptr32[i].iov_len;

				vptr[i].iov_len = vptr32[i].iov_len;
				vptr[i].iov_base = (caddr_t)vptr32[i].iov_base;
			}
		} else
#endif /* _MULTI_DATAMODEL */
		{
			/*
			 * Find number of iovecs for this message
			 */
			nvecs = rm_ptr[count].iovcnt;

			if (nvecs > OPSRSM_MAXVECS) {
				DERR("ioctl_recvmsgs: invalid vec size");
				err = EINVAL;
				break;
			}

			D1("ioctl_recvmsgs: nvecs = %d", nvecs);

			/*
			 * Copy in iovec structures to driver buffer
			 */
			if (ddi_copyin(rm_ptr[count].iov, (caddr_t)vptr,
			    (size_t)nvecs * sizeof (iovec_t), mode)) {
				DERR("ioctl_recvmsgs: invalid iovec pointer");
				err = EFAULT;
				break;
			}

			/*
			 * Calculate buffer size
			 */
			bytecount = 0;
			for (i = 0; i < (int)nvecs; i++) {
				if (vptr[i].iov_len < 0) {
					DERR("ioctl_recvmsgs: invalid iovlen");
					err = EINVAL;
					break;
				}
				bytecount += (uint32_t)vptr[i].iov_len;
			}
		}

		if (err != 0) break;

		/*
		 * Grab a buffer
		 */
		mutex_enter(&rp->rs_lock);
		OPSRSM_Q_REMOVE(&rp->rs_recvq, mp);
		if (OPSRSM_Q_LEN(&rp->rs_recvq) == 0) {
			rp->rs_events = 0;
		}
		mutex_exit(&rp->rs_lock);
		ASSERT(mp != NULL && MBLKL(mp) >= (int)OPSRSM_MESSAGE_HDRSZ);
		total_bytes += MBLKL(mp);

		/*
		 * Extract the length from the header
		 *
		 * Truncate the message if the incoming message size
		 * is greater than buffer size.
		 */

		org_msglen = OPSRSM_MESSAGE_HDRPTR(mp)->msg_sz;
		nodeid = OPSRSM_MESSAGE_HDRPTR(mp)->nodeid;
		portnum = OPSRSM_MESSAGE_HDRPTR(mp)->lportnum;
		if (OPSRSM_MESSAGE_HDRPTR(mp)->msg_sz < bytecount) {
			bytecount = OPSRSM_MESSAGE_HDRPTR(mp)->msg_sz;
		}

		/*
		 * Initialize uio structure
		 */
		if (bytecount > 0) {
			uio_t phys_uio;

			phys_uio.uio_iov = vptr;
			phys_uio.uio_iovcnt = (int)nvecs;
			phys_uio.uio_segflg = UIO_USERSPACE;
			phys_uio.uio_resid = (ssize_t)bytecount;

			if (uiomove((caddr_t)mp->b_rptr + OPSRSM_MESSAGE_HDRSZ,
				bytecount, UIO_READ, &phys_uio)) {
				DERR("ioctl_recvmsg: uiomove failed");
				err = EFAULT;
				freemsg(mp);
				mp = NULL;
				break;
			}
		}

		opsrsmdev->opsrsm_packets_consumed++;
		freemsg(mp);

		/*
		 * Return the number of bytes received and original message
		 * length.
		 */
#ifdef _MULTI_DATAMODEL
		if (model == DDI_MODEL_ILP32) {
			rm_ptr32[count].bytes_recvd = bytecount;
			rm_ptr32[count].msglen = org_msglen;
			rm_ptr32[count].portnum = portnum;
			rm_ptr32[count].nodeid = nodeid;

			if (ddi_copyout((caddr_t *)&(rm_ptr32[count]),
			    (caddr_t)&((((rsmrdt_recvmsg32_t *)
			    (io_args32.msg_iov))[count])),
			    sizeof (rsmrdt_recvmsg32_t), mode)
			    != DDI_SUCCESS) {
				DERR("ioctl_recvmsgs:unable to copyout rdata");
				err = EFAULT;
				break;
			}
		} else
#endif /* _MULTI_DATAMODEL */
		{
			rm_ptr[count].bytes_recvd = bytecount;
			rm_ptr[count].msglen = org_msglen;
			rm_ptr[count].portnum = portnum;
			rm_ptr[count].nodeid = nodeid;

			if (ddi_copyout((caddr_t *)&(rm_ptr[count]),
			    (caddr_t)&((((rdt_recvmsg_t *)
			    (io_args.msg_iov))[count])),
			    sizeof (rdt_recvmsg_t), mode) != DDI_SUCCESS) {
				DERR("ioctl_recvmsgs:unable to copyout rdata");
				err = EFAULT;
				break;
			}
		}

		/*
		 * Increment the received messages count
		 */
		count++;

	} while (OPSRSM_Q_LEN(&rp->rs_recvq) > 0 && count < nmsgs);

	atomic_add_32(&opsrsm_pending_bytes, -total_bytes);
	/*
	 * Copy out the number of messages received
	 */
done:;
#ifdef _MULTI_DATAMODEL
	if (model == DDI_MODEL_ILP32) {
		if (ddi_copyout((caddr_t)&count,
		    (caddr_t)&(((rsmrdt_recvmsgs_arg32_t *)arg)->msgcnt),
		    sizeof (uint32_t), mode) != DDI_SUCCESS) {
			DERR("ioctl_recvmsgs: unable to copyout buffer count");
			err = EFAULT;
		}
	} else
#endif /* _MULTI_DATAMODEL */
	if (ddi_copyout((caddr_t)&count,
	    (caddr_t)&(((rsmrdt_recvmsgs_arg_t *)arg)->msgcnt),
	    sizeof (uint32_t), mode) != DDI_SUCCESS) {
		DERR("ioctl_recvmsgs: unable to copyout buffer count");
		err = EFAULT;
	}

	return (err);
}

/*
 * This call is used to set some per-endpoint parameters.
 * o per fd protection key
 *
 */
static int
opsrsm_ioctl_setparam(opsrsmresource_t *rp, intptr_t arg, int mode)
{
	void 		*value;
	rsmrdt_getsetparam_arg_t io_args;
	int 		error = RSM_SUCCESS;
#ifdef _MULTI_DATAMODEL
	rsmrdt_getsetparam_arg32_t io_args32;

	model_t model = ddi_model_convert_from(mode & FMODELS);

	/*
	 * Copy in the setparam ioctl arg structure
	 */
	if (model == DDI_MODEL_ILP32) {
		ddi_copyin((caddr_t)arg, (caddr_t)&io_args32,
		    sizeof (io_args32), mode);
		io_args.cmd = io_args32.cmd;
		io_args.size = io_args32.size;
	} else
#endif /* _MULTI_DATAMODEL */
	/*
	 * Copy in the setparam ioctl arg structure
	 */
	(void) ddi_copyin((caddr_t)arg, (caddr_t)&io_args, sizeof (io_args),
	    mode);

	value = (void *)kmem_zalloc(io_args.size, KM_NOSLEEP);
	if (value == NULL) {
		DERR("ioctl_setparam: kmem_zalloc failed");
		return (ENOMEM);
	}

#ifdef _MULTI_DATAMODEL
	if (model == DDI_MODEL_ILP32) {
		if (ddi_copyin((caddr_t)io_args32.value, (caddr_t)value,
		    io_args32.size, mode)) {
			kmem_free((void *)value, io_args32.size);
			DERR("ioctl_setparam: cannot copy args");
			return (EFAULT);
		}
	} else
#endif /* _MULTI_DATAMODEL */
	if (ddi_copyin((caddr_t)io_args.value, (caddr_t)value,
	    io_args.size, mode)) {
		kmem_free((void *)value, io_args.size);
		DERR("ioctl_setparam: cannot copy args");
		return (EFAULT);
	}

	switch (io_args.cmd) {
	case RDT_MAXMSGSIZE:
		/*
		 * Buffer length must be multiple of 64 (0x40) and
		 * must be between 64 and 64k bytes.
		 * Add the cache line size.
		 */
		if (((*(uint_t *)value & ~OPSRSM_CACHELINE_MASK) == 0) &&
		    (*(int *)value > 0) &&
		    (*(uint_t *)value <= OPSRSM_MAX_BUFFER_SIZE_DFLT)) {
			opsrsmdev->opsrsm_param.opsrsm_buffer_size =
				*(uint_t *)value + OPSRSM_CACHELINE_SIZE;
			D1("ioctl_setparam: MTU sz 0x%x", *(uint_t *)value);
		} else {
			DERR("ioctl_setparam: invalid MTU sz\n");
			error = EINVAL;
		}
		break;
	case RDT_PROTECTION_KEY:
		/*
		 * Set the pkey
		 */
		rp->rs_pkey = *(uint32_t *)value;
		D1("ioctl_setparam: pkey 0x%x", rp->rs_pkey);
		break;
	default:
		DERR("ioctl_setparam: invalid cmd\n");
		error = EINVAL;
	}

	kmem_free((void *)value, io_args.size);
	return (error);
}

static int
opsrsm_ioctl_getparam(opsrsmresource_t *rp, intptr_t arg, int mode)
{
	void 		*value;
	rsmrdt_getsetparam_arg_t io_args;
#ifdef _MULTI_DATAMODEL
	rsmrdt_getsetparam_arg32_t io_args32;

	model_t model = ddi_model_convert_from(mode & FMODELS);

	/*
	 * Copy in the getparam ioctl arg structure
	 */
	if (model == DDI_MODEL_ILP32) {
		ddi_copyin((caddr_t)arg, (caddr_t)&io_args32,
		    sizeof (io_args32), mode);
		io_args.cmd = io_args32.cmd;
		io_args.size = io_args32.size;
	} else
#endif /* _MULTI_DATAMODEL */
	/*
	 * Copy in the getparam ioctl arg structure
	 */
	(void) ddi_copyin((caddr_t)arg, (caddr_t)&io_args, sizeof (io_args),
	    mode);

	value = (void *)kmem_zalloc(io_args.size, KM_NOSLEEP);
	if (value == NULL) {
		DERR("ioctl_getparam: kmem_zalloc failed");
		return (ENOMEM);
	}

#ifdef _MULTI_DATAMODEL
	if (model == DDI_MODEL_ILP32) {
		if (ddi_copyin((caddr_t)io_args32.value, (caddr_t)value,
		    io_args32.size, mode)) {
			kmem_free((void *)value, io_args32.size);
			DERR("ioctl_getparam: cannot copy args");
			return (EFAULT);
		}
	} else
#endif /* _MULTI_DATAMODEL */
	if (ddi_copyin((caddr_t)io_args.value, (caddr_t)value,
	    io_args.size, mode)) {
		kmem_free((void *)value, io_args.size);
		DERR("ioctl_getparam: cannot copy args");
		return (EFAULT);
	}

	switch (io_args.cmd) {
	case RDT_MAXMSGSIZE:
		/*
		 * Get the message size
		 * Subtract the cache line size.
		 */
#ifdef _MULTI_DATAMODEL
		if (model == DDI_MODEL_ILP32) {
			*(uint_t *)value =
			    opsrsmdev->opsrsm_param.opsrsm_buffer_size -
			    OPSRSM_CACHELINE_SIZE;
			D1("ioctl_getparam: MTU sz 0x%x", *(uint_t *)value);
		} else
#endif /* _MULTI_DATAMODEL */
		{
			*(size_t *)value =
			    opsrsmdev->opsrsm_param.opsrsm_buffer_size -
			    OPSRSM_CACHELINE_SIZE;

			D1("ioctl_getparam: MTU sz 0x%x", *(size_t *)value);
		}
		break;
	case RDT_PROTECTION_KEY:
		/*
		 * Get the protection key
		 */
		*(uint32_t *)value = rp->rs_pkey;

		D1("ioctl_getparam: pkey 0x%x", *(uint32_t *)value);
		break;
	default:
		DERR("ioctl_getparam: invalid cmd\n");
		kmem_free((void *)value, io_args.size);
		return (EINVAL);
	}

#ifdef _MULTI_DATAMODEL
	if (model == DDI_MODEL_ILP32) {
		if (ddi_copyout((caddr_t)value, (caddr_t)io_args32.value,
		    io_args32.size, mode) != DDI_SUCCESS) {
			kmem_free((void *)value, io_args32.size);
			DERR("ioctl_getparam: unable to copyout value");
			return (EFAULT);
		}
	} else
#endif /* _MULTI_DATAMODEL */
	if (ddi_copyout((caddr_t)value, (caddr_t)io_args.value,
	    io_args.size, mode) != DDI_SUCCESS) {
		kmem_free((void *)value, io_args.size);
		DERR("ioctl_getparam: unable to copyout value");
		return (EFAULT);
	}
	kmem_free((void *)value, io_args.size);
	return (0);
}

/*
 * Free minor resource
 */
static int
opsrsm_resstruct_free(minor_t rnum)
{
	opsrsmresource_t	*rp;
	opsrsm_queue_t		*q;
	int32_t			total_bytes = 0;

	/*
	 * remove resource from global table
	 */
	rp = opsrsmresource_free(rnum);
	if (rp == NULL) {
		return (DDI_FAILURE);
	}
	/*
	 * check if refcnt is 0 before we destroy rp. if it
	 * is non-zero, we need to wait until it becomes zero.
	 */
	mutex_enter(&rp->rs_lock);
	while (rp->rs_refcnt > 0) {
		cv_wait(&rp->rs_close_cv, &rp->rs_lock);
	}
	/*
	 * we can be sure that no other thread can increment
	 * rp->rs_refcnt since we already removed rp from the
	 * global table.
	 */
	ASSERT(rp->rs_refcnt == 0);
	mutex_exit(&rp->rs_lock);
	/*
	 * at this point, no other thread has a reference to
	 * rp. we can safely cleanup rp without holding
	 * rs_lock.
	 */

	/*
	 * flush all remaining messages in the recvq
	 */
	q = &rp->rs_recvq;
	while ((q)->q_head != NULL) {
		mblk_t *mp;

		mp = (q)->q_head;
		total_bytes += MBLKL(mp);
		(q)->q_head = mp->b_next;
		mp->b_prev = mp->b_next = NULL;
		mp->b_cont = NULL;
		freemsg(mp);
	}
	(q)->q_tail = NULL;
	(q)->q_len = 0;
	atomic_add_32(&opsrsm_pending_bytes, -total_bytes);

	/*
	 * if rs_dest is still valid, we need to release our
	 * reference to it.
	 */
	if (rp->rs_dest != NULL && !OPSRSM_IS_LOOPBACK(rp->rs_dest) &&
	    (rp->rs_state & OPSRSM_RS_FAILOVER) == 0 &&
	    (rp->rs_state & OPSRSM_RS_REFDEST) != 0) {
		rp->rs_state &= ~OPSRSM_RS_REFDEST;
		if (opsrsmdev->opsrsm_param.rsmrdt_enable_loadbalance) {
			if (rp->rs_dest->rd_adapter->sel_cnt > 0)
				rp->rs_dest->rd_adapter->sel_cnt--;
		}
		UNREFDEST(rp->rs_dest);
	}

	/*
	 * cleanup and free the resource structure
	 */
	if (rp->rs_pollhd.ph_list != NULL)
		pollhead_clean(&rp->rs_pollhd);
	mutex_destroy(&rp->rs_lock);
	cv_destroy(&rp->rs_cv);
	cv_destroy(&rp->rs_conn_cv);
	cv_destroy(&rp->rs_close_cv);
	kmem_free((void *)rp->rs_rmptr, opsrsmdev->
	    opsrsm_param.opsrsm_max_recv_msgs * sizeof (rdt_recvmsg_t));
	kmem_free((void *)rp, sizeof (*rp));

	return (DDI_SUCCESS);
}


/*
* Allocate a resource struct
*/
static opsrsmresource_t *
opsrsm_resstruct_alloc()
{
	opsrsmresource_t *rp;

	rp = (opsrsmresource_t *)kmem_zalloc(sizeof (*rp), KM_SLEEP);
	if (rp == NULL) {
		DERR("opsrsm_resstruct_alloc: kmem_zalloc failed");
		return (NULL);
	}

	rp->rs_rmptr = (void *)kmem_zalloc(
			opsrsmdev->opsrsm_param.opsrsm_max_recv_msgs *
			sizeof (rdt_recvmsg_t), KM_SLEEP);
	if (rp->rs_rmptr == NULL) {
		DERR("opsrsm_resstruct_alloc: kmem_zalloc failed");
		kmem_free((void *)rp, sizeof (*rp));
		return (NULL);
	}

	rp->rs_events = 0;
	rp->rs_nodeid = 0;
	rp->rs_lportnum = 0;
	rp->rs_rportnum = 0;
	rp->rs_dest = NULL;
	rp->rs_pollhd.ph_list = NULL;
	rp->rs_refcnt = 0;
	rp->rs_state = 0;
	rp->rs_poll_index = -1;
	rp->rs_pkey = 0;
	rp->rs_local_skey = 0;
	OPSRSM_Q_INIT(&rp->rs_recvq);

	mutex_init(&rp->rs_lock, NULL, MUTEX_DRIVER, NULL);
	cv_init(&rp->rs_cv, NULL, CV_DRIVER, NULL);
	cv_init(&rp->rs_conn_cv, NULL, CV_DRIVER, NULL);
	cv_init(&rp->rs_close_cv, NULL, CV_DRIVER, NULL);

	return (rp);
}

/*ARGSUSED*/
static int
opsrsm_ioctl(dev_t dev, int cmd, intptr_t arg, int mode, cred_t *cred,
	int *rvalp)
{
	int error = RSM_SUCCESS;
	opsrsmresource_t *rp;
	minor_t rnum;

	rnum = getminor(dev);
	rp = opsrsmresource_lookup(rnum, OPSRSM_RO_DEFAULT);
	if (rp == NULL) {
		return (ENXIO);
	}

	D1("opsrsm_ioctl: rnum = %d ; rp = %p", rnum, rp);

	switch (cmd) {
	case RSMRDT_IOCTL_BIND:
		error = opsrsm_ioctl_bind(rp, arg, mode);
		break;
	case RSMRDT_IOCTL_CONNECT:
		error = opsrsm_ioctl_connect(rp, arg, mode);
		break;
	case RSMRDT_IOCTL_SENDMSG:
		error = opsrsm_ioctl_sendmsg(rp, arg, mode);
		break;
	case RSMRDT_IOCTL_RECVMSGS:
		error = opsrsm_ioctl_recvmsgs(rp, arg, mode);
		break;
	case RSMRDT_IOCTL_GETPARAM:
		error = opsrsm_ioctl_getparam(rp, arg, mode);
		break;
	case RSMRDT_IOCTL_SETPARAM:
		error = opsrsm_ioctl_setparam(rp, arg, mode);
		break;
	case RSMRDT_IOCTL_GETNODEID:
		error = opsrsm_ioctl_getnodeid(rp, arg, mode);
		break;
	default:
		DERR("opsrsm_ioctl: cmd not supported\n");
		error = DDI_FAILURE;
	}
	return (error);
}

/* ********************* Driver Open/Close/Poll *************** */

/* ARGSUSED */
static int
opsrsm_open(dev_t *devp, int flag, int otyp, struct cred *cred)
{
	minor_t rnum;
	opsrsmresource_t *rp;

	/*
	 * Char only
	 */
	if (otyp != OTYP_CHR) {
		return (EINVAL);
	}

	rw_enter(&opsrsm_resource.opsrsmrct_lock, RW_READER);
	if (opsrsm_resource.opsrsmrc_flag == OPSRSMRC_UNLOAD_INPROGRESS) {
		DERR("opsrsm_open: Unloading in progress");
		rw_exit(&opsrsm_resource.opsrsmrct_lock);
		return (ENODEV);
	}
	rw_exit(&opsrsm_resource.opsrsmrct_lock);

	/*
	 * Only zero can be opened, clones are used for resources.
	 */
	if (getminor(*devp) != OPSRSM_DRIVER_MINOR) {
		DERR("opsrsm_open: bad minor %d\n", getminor(*devp));
		return (ENODEV);
	}

	/*
	 * - allocate new minor number
	 * - update devp argument to new device
	 */
	if ((rp = opsrsmresource_alloc(&rnum)) != NULL) {
		*devp = makedevice(getmajor(*devp), rnum);
		rp->rs_lportnum = rnum;
	} else {
		return (ENOMEM);
	}

	return (DDI_SUCCESS);
}

/* ARGSUSED */
static int
opsrsm_close(dev_t dev, int flag, int otyp, struct cred *cred)
{
	minor_t rnum = getminor(dev);

	/*
	 * Char only
	 */
	if (otyp != OTYP_CHR) {
		return (EINVAL);
	}
	D1("opsrsm_close: rnum = %d", rnum);

	/*
	 * remove resource from resource table and destroy resource
	 */
	if (opsrsm_resstruct_free(rnum) != DDI_SUCCESS) {
		DERR("opsrsm_close: cannot free resource structure\n");
		return (DDI_FAILURE);
	}
	return (DDI_SUCCESS);
}

/*ARGSUSED*/
static int
opsrsm_chpoll(dev_t dev, short events, int anyyet, short *reventsp,
	struct pollhead **phpp)
{
	opsrsmresource_t *rp;
	minor_t rnum;
	int error = 0;

	rnum = getminor(dev);
	rp = opsrsmresource_lookup(rnum, OPSRSM_RO_DEFAULT);
	if (rp == NULL) {
		return (ENXIO);
	}

	D1("opsrsm_chpoll: rnum = %d : rp = %p\n", rnum, rp);

	*reventsp = 0;

	/*
	 * Valid device events are:
	 * POLLIN | POLLRDNORM | POLLOUT | POLLERR
	 */
	if ((events & POLLIN) != 0) {
		mutex_enter(&rp->rs_lock);
		if ((events & POLLOUT) != 0) {
			error = ENOTSUP;
			mutex_exit(&rp->rs_lock);
			goto done;
		}
		if ((rp->rs_events & POLLIN) != 0 ||
		    OPSRSM_Q_LEN(&rp->rs_recvq) > 0) {
			*reventsp = POLLIN;
		} else {
			if (!anyyet) {
				*phpp = &rp->rs_pollhd;
			}
		}
		mutex_exit(&rp->rs_lock);
	} else if ((events & POLLOUT) != 0) {
		boolean_t not_full;
		opsrsm_dest_t *rd;
		int isdel;

		if (OPSRSM_IS_LOOPBACK(rp->rs_dest)) {
			*reventsp = POLLOUT;
			goto done;
		}
		if (rp->rs_dest == NULL) {
			error = ENOTCONN;
			goto done;
		}

		mutex_enter(&rp->rs_lock);
		if ((rp->rs_state & OPSRSM_RS_FAILOVER) != 0) {
			*reventsp = POLLOUT;
			mutex_exit(&rp->rs_lock);
			goto done;
		}
		rd = rp->rs_dest;
		REFDEST(rd, isdel);
		if (isdel != 0) {
			error = ENETDOWN;
			mutex_exit(&rp->rs_lock);
			goto done;
		}
		mutex_exit(&rp->rs_lock);

		mutex_enter(&rd->rd_sendq_lock);
		not_full = (OPSRSM_Q_LEN(&rd->rd_sendq) < opsrsmdev->
		    opsrsm_param.opsrsm_max_queued_pkts);
		mutex_exit(&rd->rd_sendq_lock);

		if (not_full) {
			*reventsp = POLLOUT;
		} else {
			if (!anyyet) {
				if (rd->rd_dstate == 0)
					*phpp = &rd->rd_pollhd;
			}
		}
		UNREFDEST(rd);
	} else if ((events & POLLERR) != 0) {
		error = ENOTSUP;
	}

done:;
	return (error);
}



/*
 * Undo tasks done by opsrsmattach(), either because we're detaching or because
 * attach() got partly done then failed.  progress is a bitmap that tells
 * us what has been done so far.
 */
static void
opsrsmtakedown(
	opsrsm_t *opsrsmp,	/* OPSRSM device (RSM controller) pointer */
	int progress)	/* Mask of RSMPI_ATT_xxx values */
{
	int instance;
	dev_info_t *dip;

	D1("opsrsmtakedown: opsrsmp 0x%p, progress 0x%x",
	    (void *)opsrsmp, progress);

	ASSERT(opsrsmp);

	dip = opsrsmp->opsrsm_dip;
	instance = ddi_get_instance(dip);

	if (progress & OPSRSM_ATT_KSTAT) {
		opsrsmkstatremove(opsrsmp);
		progress &= ~OPSRSM_ATT_KSTAT;
	}

	if (progress & OPSRSM_ATT_MINOR) {
		ddi_remove_minor_node(dip, NULL);
		progress &= ~OPSRSM_ATT_MINOR;
	}

	ASSERT(progress == 0);

	ddi_soft_state_free(opsrsm_state, instance);

	D1("opsrsmtakedown: returning DDI_SUCCESS");
}


/*
 * ****************************************************************
 *                                                               *
 * E N D   BASIC MODULE BOILERPLATE                              *
 *                                                               *
 * ****************************************************************
 */


/*
 * ****************************************************************
 *                                                               *
 * B E G I N   STATUS REPORTING STUFF                            *
 *                                                               *
 * ****************************************************************
 */

/*
 * This routine makes the data in our kernel statistics structure reflect
 * the current state of the device; it's called whenever a user requests
 * the kstat data.  Basically, all we do is copy the stats from the RSMPI
 * controller structure, where they're maintained, to the kstat's data
 * portion.
 */
static int
opsrsmstat_kstat_update(
	kstat_t *ksp,	/* Pointer to kstat that will be updated */
	int rw)		/* Indicates read or write (we don't support write) */
{
	opsrsm_t *opsrsmp;
	opsrsm_stat_t *opsrsmsp;

	if (rw == KSTAT_WRITE)
		return (EACCES);

	opsrsmp = (opsrsm_t *)ksp->ks_private;
	opsrsmsp = (opsrsm_stat_t *)ksp->ks_data;

	opsrsmsp->rsm_ipackets.value.ul = (uint_t)opsrsmp->opsrsm_ipackets;
	opsrsmsp->rsm_ipackets64.value.ui64 = opsrsmp->opsrsm_ipackets;
	opsrsmsp->rsm_ierrors.value.ul = opsrsmp->opsrsm_ierrors;
	opsrsmsp->rsm_opackets.value.ul = (uint_t)opsrsmp->opsrsm_opackets;
	opsrsmsp->rsm_opackets64.value.ui64 = opsrsmp->opsrsm_opackets;
	opsrsmsp->rsm_oerrors.value.ul = opsrsmp->opsrsm_oerrors;
	opsrsmsp->rsm_collisions.value.ul = opsrsmp->opsrsm_collisions;

	opsrsmsp->rsm_xfers.value.ul = opsrsmp->opsrsm_xfers;
	opsrsmsp->rsm_xfer_pkts.value.ul = opsrsmp->opsrsm_xfer_pkts;
	opsrsmsp->rsm_syncdqes.value.ul = opsrsmp->opsrsm_syncdqes;
	opsrsmsp->rsm_lbufs.value.ul = opsrsmp->opsrsm_lbufs;
	opsrsmsp->rsm_nlbufs.value.ul = opsrsmp->opsrsm_nlbufs;
	opsrsmsp->rsm_pullup.value.ul = opsrsmp->opsrsm_pullup;
	opsrsmsp->rsm_pullup_fail.value.ul = opsrsmp->opsrsm_pullup_fail;
	opsrsmsp->rsm_starts.value.ul = opsrsmp->opsrsm_starts;
	opsrsmsp->rsm_start_xfers.value.ul = opsrsmp->opsrsm_start_xfers;
	opsrsmsp->rsm_fqetmo_hint.value.ul = opsrsmp->opsrsm_fqetmo_hint;
	opsrsmsp->rsm_fqetmo_drops.value.ul = opsrsmp->opsrsm_fqetmo_drops;
	opsrsmsp->rsm_no_fqes.value.ul = opsrsmp->opsrsm_no_fqes;
	opsrsmsp->rsm_pending_writes.value.ul = opsrsmp->opsrsm_pending_writes;
	opsrsmsp->rsm_pkts_queued.value.ul = opsrsmp->opsrsm_pkts_queued;
	opsrsmsp->rsm_pkts_discarded.value.ul = opsrsmp->opsrsm_pkts_discarded;
	opsrsmsp->rsm_pkts_pending.value.ul = opsrsmp->opsrsm_pkts_pending;
	opsrsmsp->rsm_last_sendq_len.value.ul = opsrsmp->opsrsm_last_sendq_len;
	opsrsmsp->rsm_last_pendq_len.value.ul = opsrsmp->opsrsm_last_pendq_len;
	opsrsmsp->rsm_last_wr_comp.value.ul = opsrsmp->opsrsm_last_wr_comp;
	opsrsmsp->rsm_errs.value.ul = opsrsmp->opsrsm_errs;
	opsrsmsp->rsm_in_bytes.value.ul = (uint_t)opsrsmp->opsrsm_in_bytes;
	opsrsmsp->rsm_in_bytes64.value.ui64 = opsrsmp->opsrsm_in_bytes;
	opsrsmsp->rsm_out_bytes.value.ul = (uint_t)opsrsmp->opsrsm_out_bytes;
	opsrsmsp->rsm_out_bytes64.value.ui64 = opsrsmp->opsrsm_out_bytes;
	opsrsmsp->rsm_intr_send_errs.value.ul = opsrsmp->opsrsm_intr_send_errs;
	opsrsmsp->rsm_max_batch_size.value.ul = opsrsmp->opsrsm_max_batch_size;
	opsrsmsp->rsm_min_batch_size.value.ul = opsrsmp->opsrsm_min_batch_size;
	opsrsmsp->rsm_put_fqes.value.ul = opsrsmp->opsrsm_put_fqes;
	opsrsmsp->rsm_queued_fqes.value.ul = opsrsmp->opsrsm_queued_fqes;
	opsrsmsp->rsm_packets_consumed.value.ul =
		opsrsmp->opsrsm_packets_consumed;
	return (0);
}

/*
 * This routine initializes the kernel statistics structures for an
 * OPSRSM device.
 */
static void
opsrsmkstatinit(
	opsrsm_t *opsrsmp)	/* OPSRSM device (RSM controller) pointer */
{
	struct kstat *ksp;
	opsrsm_stat_t *opsrsmsp;

	/*
	 * We create a kstat for the device, then create a whole bunch of
	 * named stats inside that first kstat.
	 */
	if ((ksp = kstat_create("rsmrdt", ddi_get_instance(opsrsmp->opsrsm_dip),
	    NULL, "net", KSTAT_TYPE_NAMED, sizeof (opsrsm_stat_t) /
	    sizeof (kstat_named_t), 0)) == NULL) {
		opsrsmerror(opsrsmp->opsrsm_dip, "kstat_create failed");
		return;
	}
	opsrsmsp = (opsrsm_stat_t *)(ksp->ks_data);

	/*
	 * The first five named stats we create have well-known names, and are
	 * used by standard SunOS utilities (e.g., netstat).  (There is actually
	 * a sixth well-known stat, called "queue", which we don't support.)
	 */
	kstat_named_init(&opsrsmsp->rsm_ipackets, "ipackets", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_ierrors, "ierrors", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_opackets, "opackets", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_oerrors, "oerrors", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_collisions, "collisions",
	    KSTAT_DATA_ULONG);

	/*
	 * MIB II kstat variables
	 */
	kstat_named_init(&opsrsmsp->rsm_in_bytes, "rbytes", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_out_bytes, "obytes", KSTAT_DATA_ULONG);

	/*
	 * PSARC 1997/198
	 */
	kstat_named_init(&opsrsmsp->rsm_ipackets64, "ipackets64",
		KSTAT_DATA_ULONGLONG);
	kstat_named_init(&opsrsmsp->rsm_opackets64, "opackets64",
		KSTAT_DATA_ULONGLONG);
	kstat_named_init(&opsrsmsp->rsm_in_bytes64, "rbytes64",
		KSTAT_DATA_ULONGLONG);
	kstat_named_init(&opsrsmsp->rsm_out_bytes64, "obytes64",
		KSTAT_DATA_ULONGLONG);


	/*
	 * The remainder of the named stats are specific to our driver, and
	 * are extracted using the kstat utility.
	 */
	kstat_named_init(&opsrsmsp->rsm_xfers, "xfers", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_xfer_pkts, "xfer_pkts",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_syncdqes, "syncdqes", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_lbufs, "lbufs", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_nlbufs, "nlbufs", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_pullup, "pullup", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_pullup_fail, "pullup_fail",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_starts, "starts", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_start_xfers, "start_xfers",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_fqetmo_hint, "fqetmo_hint",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_fqetmo_drops, "fqetmo_drops",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_no_fqes, "no_fqes",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_pending_writes, "pending_writes",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_pkts_queued, "pkts_queued",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_pkts_discarded, "pkts_discarded",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_pkts_pending, "pkts_pending",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_last_sendq_len, "last_sendq_len",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_last_pendq_len, "last_pendq_len",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_last_wr_comp, "last_wr_comp",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_errs, "errs", KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_intr_send_errs, "intr_send_errs",
	    KSTAT_DATA_ULONG);

	kstat_named_init(&opsrsmsp->rsm_min_batch_size, "min_batch_size",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_max_batch_size, "max_batch_size",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_put_fqes, "put_fqes",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_queued_fqes, "queued_fqes",
	    KSTAT_DATA_ULONG);
	kstat_named_init(&opsrsmsp->rsm_packets_consumed, "packets_consumed",
	    KSTAT_DATA_ULONG);

	ksp->ks_update = opsrsmstat_kstat_update;
	ksp->ks_private = (void *) opsrsmp;
	opsrsmp->opsrsm_ksp = ksp;
	kstat_install(ksp);

}

/*
 * This routine removes any kstats we might have created.
 */
static void
opsrsmkstatremove(
	opsrsm_t *opsrsmp)	/* OPSRSM device (RSM controller) pointer */
{

	if (opsrsmp->opsrsm_ksp)
		kstat_delete(opsrsmp->opsrsm_ksp);
}

/*
 * Print an error message to the console.
 */
static void
opsrsmerror(
	dev_info_t *dip,	/* Dev info for the device in question */
	const char *fmt,	/* Format of output */
	...)			/* Parameters for output */
{
	char name[16];
	char buff[1024];
	va_list ap;

	if (dip) {
		(void) sprintf(name, "%s%d", ddi_get_name(dip),
			ddi_get_instance(dip));
	} else {
		(void) sprintf(name, "opsrsm");
	}

	/* lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, fmt);
	/* lint +e40 */
	(void) vsprintf(buff, fmt, ap);
	va_end(ap);

	D1("%s:\t%s", name, buff);
#ifdef DEBUG
	cmn_err(CE_CONT, "%s:\t%s", name, buff);
#endif /* DEBUG */
}


#ifdef DEBUG

/*
 * The following variables support the debug log buffer scheme.
 */

char opsrsmdbgbuf[0x80000];	/* The log buffer */
int opsrsmdbgsize = sizeof (opsrsmdbgbuf);	/* Size of the log buffer */
size_t opsrsmdbgnext;		/* Next byte to write in buffer (note */
				/*  this is an index, not a pointer */
int opsrsmdbginit = 0;		/* Nonzero if opsrsmdbglock's inited */
kmutex_t opsrsmdbglock;

/*
 * Add the string str to the end of the debug log, followed by a newline.
 */
static void
opsrsmdbglog(char *str)
{
	size_t length, remlen;

	/*
	 * If this is the first time we've written to the log, initialize it.
	 */
	if (!opsrsmdbginit) {
		mutex_enter(&opsrsmattlock);
		if (!opsrsmdbginit) {
			mutex_init(&opsrsmdbglock, NULL, MUTEX_DRIVER,
			    NULL);
			bzero(opsrsmdbgbuf, sizeof (opsrsmdbgbuf));
			opsrsmdbgnext = 0;
			opsrsmdbginit = 1;
		}
		mutex_exit(&opsrsmattlock);
	}

	mutex_enter(&opsrsmdbglock);

	/*
	 * Note the log is circular; if this string would run over the end,
	 * we copy the first piece to the end and then the last piece to
	 * the beginning of the log.
	 */
	length = strlen(str);

	remlen = (size_t)sizeof (opsrsmdbgbuf) - opsrsmdbgnext;

	if (length > remlen) {
		if (remlen)
			bcopy(str, opsrsmdbgbuf + opsrsmdbgnext, remlen);
		str += remlen;
		length -= remlen;
		opsrsmdbgnext = 0;
	}

	bcopy(str, opsrsmdbgbuf + opsrsmdbgnext, length);
	opsrsmdbgnext += length;

	if (opsrsmdbgnext >= sizeof (opsrsmdbgbuf))
		opsrsmdbgnext = 0;
	opsrsmdbgbuf[opsrsmdbgnext++] = '\n';

	mutex_exit(&opsrsmdbglock);
}


/*
 * Add a printf-style message to whichever debug logs we're currently using.
 */
static void
opsrsmdebug(const char *fmt, ...)
{
	char buff[512];
	va_list ap;

	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, fmt);
	/*lint +e40 */
	(void) vsprintf(buff, fmt, ap);
	va_end(ap);

	if (opsrsmdbgmode & 0x1)
		opsrsmdbglog(buff);
	if (opsrsmdbgmode & 0x2)
		cmn_err(CE_CONT, "%s\n", buff);
}

static void
opsrsmconsole(const char *fmt, ...)
{
	char buff[512];
	va_list ap;

	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, fmt);
	/*lint +e40 */
	(void) vsprintf(buff, fmt, ap);
	va_end(ap);

	cmn_err(CE_CONT, "%s", buff);
}

#endif


/*
 * ****************************************************************
 *                                                               *
 * E N D   STATUS REPORTING STUFF                                *
 *                                                               *
 * ****************************************************************
 */


/*
 * ****************************************************************
 *                                                               *
 * B E G I N   BASIC STREAMS OPERATIONS                          *
 *                                                               *
 * ****************************************************************
 */


/*
 * Write service routine.  This routine processes any messages put on the queue
 * via a putq() in the write put routine.  It also handles any destinations put
 * on the destination run queue.
 */
static void
opsrsmwsrv(void *arg)
{
	adapter_t *adapterp;
	opsrsm_t *opsrsmp = opsrsmdev;
	opsrsm_dest_t *rd;
	int isdel = 0;

	D5("opsrsmwsrv: time 0x%llx", gethrtime());

	adapterp = (adapter_t *)arg;

	/*
	 * rd's refcnt is incremented by GETRUNQ
	 */
	GETRUNQ(rd, isdel, adapterp);
	while (rd) {
		int oldstate, delete;

		if (isdel) {
			D2("opsrsmwsrv: dest 0x%p being deleted, ignored",
			    (void *)rd);
			GETRUNQ(rd, isdel, adapterp);
			continue;
		}

		mutex_enter(&rd->rd_lock);
		delete = 0;

		oldstate = opsrsmgetstate(rd);
		D5("opsrsmwsrv: running state %s time 0x%llx",
		    OPSRSM_STATE_STR(oldstate), gethrtime());
		switch (oldstate) {

		case OPSRSM_STATE_S_XFER: {
			cmn_err(CE_PANIC, "impossible state\n");
			break;
		}

		case OPSRSM_STATE_S_REQ_CONNECT: {
			if (opsrsmcrexfer(opsrsmp, rd) != 0 ||
			    opsrsmsconn(opsrsmp, rd, 0) != 0) {
				opsrsmsetstate(rd, OPSRSM_STATE_DELETING);
				delete = 1;
			}
			break;
		}

		case OPSRSM_STATE_S_NEWCONN: {
			if (opsrsmcrexfer(opsrsmp, rd) != 0 ||
			    opsrsmconnxfer(opsrsmp, rd) != 0 ||
			    opsrsmsaccept(opsrsmp, rd) != 0) {
				opsrsmsetstate(rd, OPSRSM_STATE_DELETING);
				delete = 1;
			}
			break;
		}

		case OPSRSM_STATE_S_CONNXFER_ACCEPT: {
			if (opsrsmconnxfer(opsrsmp, rd) != 0 ||
			    opsrsmsaccept(opsrsmp, rd) != 0) {
				opsrsmsetstate(rd, OPSRSM_STATE_DELETING);
				delete = 1;
			}
			break;
		}

		case OPSRSM_STATE_S_CONNXFER_ACK: {
			if (opsrsmconnxfer(opsrsmp, rd) != 0 ||
			    opsrsmsack(rd) != 0) {
				opsrsmsetstate(rd, OPSRSM_STATE_DELETING);
				delete = 1;
			}
			break;
		}

		/*
		 * Delete this connection.  This causes a message
		 * to be sent to the remote side when RSM_SENDQ_DESTROY
		 * is called, so there is no need to send an additional
		 * message.
		 */
		case OPSRSM_STATE_S_DELETE: {
			opsrsmsetstate(rd, OPSRSM_STATE_DELETING);
			delete = 1;
			break;
		}

		/*
		 * Retry the SCONN.
		 */
		case OPSRSM_STATE_S_SCONN: {
			if (opsrsmsconn(opsrsmp, rd, 1) != 0) {
				opsrsmsetstate(rd, OPSRSM_STATE_DELETING);
				delete = 1;
			}
			break;
		}

		default:
			D1("opsrsm: bad state %s in wsrv "
			    " for dest 0x%lx", OPSRSM_STATE_STR(oldstate),
			    (uintptr_t)rd);
			cmn_err(CE_PANIC, "opsrsm: bad state %s in wsrv "
			    " for dest 0x%lx", OPSRSM_STATE_STR(oldstate),
			    (uintptr_t)rd);
			break;
		}

		mutex_exit(&rd->rd_lock);

		if (delete)
			(void) opsrsmfreedest(adapterp, rd->rd_rsm_addr);

		UNREFDEST(rd);

		GETRUNQ(rd, isdel, adapterp);
	}

	D1("opsrsmwsrv: returning");
}




/*
 * ****************************************************************
 *                                                               *
 * E N D       BASIC STREAMS OPERATIONS                          *
 *                                                               *
 * ****************************************************************
 */


/*
 * ****************************************************************
 *                                                               *
 * B E G I N   NEW DATA TRANSFER LOGIC                           *
 *                                                               *
 * ****************************************************************
 */

static int
opsrsm_start_batch(opsrsm_dest_t *rd, uint32_t start_time)
{
	int err = 0;

	switch (rd->rd_xmit_state) {
	case OPSRSM_XMIT_BARRIER_CLOSED:
	case OPSRSM_XMIT_RETRY_DATA:
		err = RSM_OPEN_BARRIER_REGION(rd->rd_adapter->rsmrdt_ctlr_obj,
		    rd->rd_rxferhand, &rd->rd_barrier);
		ASSERT(err == RSM_SUCCESS);
		if (rd->rd_xmit_state == OPSRSM_XMIT_BARRIER_CLOSED) {
			rd->rd_xmit_state = OPSRSM_XMIT_BARRIER_OPENED;
			rd->rd_start_time = start_time;
			rd->rd_data_collected = 0;
			rd->rd_writes_completed = 0;
			err = 0;
		}
		break;
	default:
		cmn_err(CE_PANIC, "invalid state = %d\n", rd->rd_xmit_state);
		break;
	}
	return (err);
}

static int
opsrsm_end_batch(opsrsm_dest_t *rd)
{
	int err = 0;
	uint32_t qlen = 0;

	switch (rd->rd_xmit_state) {
	case OPSRSM_XMIT_BARRIER_OPENED:
	case OPSRSM_XMIT_RETRY_DATA:
		err = RSM_CLOSE_BARRIER(rd->rd_adapter->rsmrdt_ctlr_obj,
		    &rd->rd_barrier);
		if (err != RSM_SUCCESS) {
			rd->rd_xmit_state = OPSRSM_XMIT_RETRY_DATA;
			break;
		}
		qlen = (uint32_t)OPSRSM_Q_LEN(&rd->rd_pendq);
		opsrsmputdqes(rd);

		mutex_enter(&rd->rd_freeq_lock);
		OPSRSM_Q_CONCAT(&rd->rd_freeq, &rd->rd_pendq);
		mutex_exit(&rd->rd_freeq_lock);

		opsrsmdev->opsrsm_xfers++;
		opsrsmdev->opsrsm_xfer_pkts += qlen;
		opsrsmdev->opsrsm_max_batch_size =
		    max(opsrsmdev->opsrsm_max_batch_size, qlen);

		if (opsrsmdev->opsrsm_min_batch_size == 0) {
			opsrsmdev->opsrsm_min_batch_size = qlen;
		} else {
			opsrsmdev->opsrsm_min_batch_size =
			    min(opsrsmdev->opsrsm_min_batch_size, qlen);
		}

		rd->rd_start_time = 0;
		rd->rd_data_collected = 0;
		rd->rd_writes_completed = 0;
		rd->rd_nretries = 0;
		rd->rd_xmit_state = OPSRSM_XMIT_BARRIER_CLOSED;
		break;
	case OPSRSM_XMIT_DISCONNECTED:
		err = RSM_SUCCESS;
		break;
	default:
		cmn_err(CE_PANIC, "invalid state = %d\n", rd->rd_xmit_state);
		break;
	}

	if (err != RSM_SUCCESS) {
		opsrsmdev->opsrsm_collisions++;
		if (++rd->rd_nretries > opsrsmdev->
		    opsrsm_param.opsrsm_retry_limit) {
			rd->rd_nretries = 0;
			err = ENETDOWN;
		} else {
			opsrsm_set_xmit_tmo(rd, opsrsmdev->opsrsm_param.
			    opsrsm_retry_delay);
		}
	} else {
		err = 0;
	}
	return (err);
}

static void
opsrsm_dispatch_tmo(void *arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;

	/* keep rescheduling itself until opsrsmxmit_thread is dispatched */
	if (taskq_dispatch(rd->rd_adapter->opsrsm_taskq, opsrsmxmit_thread,
	    rd, KM_NOSLEEP) == 0) {
		(void) timeout(opsrsm_dispatch_tmo, rd, 1);
	}
}

static void
opsrsm_xmit_tmo(void *arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;

	mutex_enter(&rd->rd_tmo_lock);
	if (rd->rd_xmit_tmo_id == 0) {
		mutex_exit(&rd->rd_tmo_lock);
		return;
	}
	if (taskq_dispatch(rd->rd_adapter->opsrsm_taskq, opsrsmxmit_thread,
	    rd, KM_NOSLEEP) == 0) {
		rd->rd_xmit_tmo_id = timeout(opsrsm_xmit_tmo, rd,
		    rd->rd_xmit_tmo_int);
	} else {
		rd->rd_xmit_tmo_id = 0;
		rd->rd_xmit_tmo_int = 0;
	}
	mutex_exit(&rd->rd_tmo_lock);
}

static void
opsrsm_set_xmit_tmo(opsrsm_dest_t *rd, int interval)
{
	int isdel = 0;

	mutex_enter(&rd->rd_tmo_lock);
	if (rd->rd_xmit_tmo_id != 0) {
		goto out;
	}
	REFDEST(rd, isdel);
	if (isdel != 0) goto out;
	rd->rd_xmit_tmo_int = interval;
	rd->rd_xmit_tmo_id = timeout(opsrsm_xmit_tmo, rd, rd->rd_xmit_tmo_int);
out:;
	mutex_exit(&rd->rd_tmo_lock);
}

static void
opsrsm_cancel_xmit_tmo(opsrsm_dest_t *rd)
{
	timeout_id_t tmoid;

	mutex_enter(&rd->rd_tmo_lock);
	if (rd->rd_xmit_tmo_id == 0) {
		mutex_exit(&rd->rd_tmo_lock);
		return;
	}
	UNREFDEST(rd);
	tmoid = rd->rd_xmit_tmo_id;
	rd->rd_xmit_tmo_id = 0;
	mutex_exit(&rd->rd_tmo_lock);
	(void) untimeout(tmoid);
}

static void
opsrsm_wake_senders(opsrsm_dest_t *rd, short events)
{
	pollwakeup(&rd->rd_pollhd, events);
}

static void
opsrsm_fqe_tmo(void *arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;

	mutex_enter(&rd->rd_tmo_lock);
	if (rd->rd_fqe_tmo_id == 0) {
		mutex_exit(&rd->rd_tmo_lock);
		return;
	}
	if (opsrsmavailfqe(rd)) {
		if (taskq_dispatch(rd->rd_adapter->opsrsm_taskq,
		    opsrsmxmit_thread, rd, KM_NOSLEEP) != 0) {
			rd->rd_fqe_tmo_id = 0;
			rd->rd_fqe_tmo_int = 0;
			mutex_exit(&rd->rd_tmo_lock);
			return;
		}
	}
	rd->rd_fqe_tmo_id = timeout(opsrsm_fqe_tmo, rd, rd->rd_fqe_tmo_int);
	mutex_exit(&rd->rd_tmo_lock);
}

static void
opsrsm_set_fqe_tmo(opsrsm_dest_t *rd, int interval)
{
	int isdel = 0;

	mutex_enter(&rd->rd_tmo_lock);
	if (rd->rd_fqe_tmo_id != 0) {
		goto out;
	}
	REFDEST(rd, isdel);
	if (isdel != 0) goto out;
	rd->rd_fqe_tmo_int = interval;
	rd->rd_fqe_tmo_id = timeout(opsrsm_fqe_tmo, rd, rd->rd_fqe_tmo_int);
out:;
	mutex_exit(&rd->rd_tmo_lock);
}

static void
opsrsm_cancel_fqe_tmo(opsrsm_dest_t *rd)
{
	timeout_id_t tmoid;

	mutex_enter(&rd->rd_tmo_lock);
	if (rd->rd_fqe_tmo_id == 0) {
		mutex_exit(&rd->rd_tmo_lock);
		return;
	}
	UNREFDEST(rd);
	tmoid = rd->rd_fqe_tmo_id;
	rd->rd_fqe_tmo_id = 0;
	mutex_exit(&rd->rd_tmo_lock);
	(void) untimeout(tmoid);
}

static int
opsrsm_write_data(opsrsm_dest_t *rd, mblk_t *mp)
{
	uint_t bufnum;
	int write_err;
	uint_t pktlen;
	uint_t start_offset, end_offset;
	uchar_t *srcaddr, *endaddr;

	pktlen = (uint_t)MBLKL(mp);
	if (pktlen > rd->rd_rbuflen)
		pktlen = rd->rd_rbuflen;

	bufnum = (uint_t)mp->b_prev;
	srcaddr = mp->b_rptr;
	start_offset = (uint_t)((uint64_t)srcaddr & OPSRSM_CACHELINE_OFFSET);
	ASSERT(start_offset == 0);
	endaddr = srcaddr + pktlen;
	end_offset = (uint_t)(OPSRSM_CACHELINE_ROUNDUP(endaddr) -
	    (uint64_t)endaddr);

	ASSERT((pktlen + start_offset + end_offset) <= rd->rd_rbuflen);
	ASSERT(((rd->rd_rbufoff + (off_t)(bufnum * rd->rd_rbuflen)) &
	    OPSRSM_CACHELINE_OFFSET) == 0);

	D6("write_data: put 0x%x bytes at segoffset 0x%lx from addr 0x%p",
	    pktlen + start_offset + end_offset, rd->rd_rbufoff +
	    (off_t)(bufnum * rd->rd_rbuflen), (void *)(srcaddr - start_offset));

	write_err = RSM_PUT(rd->rd_adapter->rsmrdt_ctlr_obj, rd->rd_rxferhand,
	    rd->rd_rbufoff + (off_t)(bufnum * rd->rd_rbuflen),
	    srcaddr - start_offset,
	    (size_t)(pktlen + start_offset + end_offset));

	if (write_err != RSM_SUCCESS) DERR("write_err = %d\n", write_err);
	return (0);
}

static int
opsrsm_sync_dqe(opsrsm_dest_t *rd)
{
	opsrsm_t *opsrsmp = opsrsmdev;
	uint64_t start_offset, end_offset;
	opsrsm_dqe_t *new_shdwdqw_o = NULL;
	rsm_send_t send_obj;
	ushort_t new_dqw_seq = 0;
	rsm_barrier_t dq_barrier;
	opsrsm_msg_t msg;
	uint32_t msg_cnt;
	int stat = RSM_SUCCESS;

	mutex_enter(&rd->rd_net_lock);
	/* If network down, nothing to do either */
	if (rd->rd_stopq) {
		D1("opsrsm_sync_dqe: stopq on, done");
		mutex_exit(&rd->rd_net_lock);
		return (0);
	}

	/* If nothing's queued, nothing to do */
	if (rd->rd_shdwdqw_i == rd->rd_shdwdqw_o) {
		D1("opsrsm_sync_dqe: no work, done");
		if (rd->rd_retry_int) goto retry_int;
		mutex_exit(&rd->rd_net_lock);
		return (0);
	}

	stat = RSM_OPEN_BARRIER_REGION(rd->rd_adapter->rsmrdt_ctlr_obj,
	    rd->rd_rxferhand, &dq_barrier);

	if (stat != RSM_SUCCESS) {
		goto done;
	}
	/*
	 * remember any updates to the DQ; commit changes when
	 * opsrsm_end_batch succeeds
	 */
	new_shdwdqw_o = rd->rd_shdwdqw_o;
	new_dqw_seq = rd->rd_dqw_seq;

	/*
	 * If we've wrapped around, so that the next element to go comes from
	 * a lower address than where we started, do it in two segments.
	 */
	if (new_shdwdqw_o > rd->rd_shdwdqw_i) {
		/*
		 * handle elements from current (shdwdqw_o) to end of list
		 * (shdwdqw_l)
		 */
		opsrsm_dqe_t *tmpdqe = new_shdwdqw_o;
		/*
		 * update entries being sent with current sequence number
		 */
		while (tmpdqe <= rd->rd_shdwdqw_l) {
			tmpdqe->s.dq_seqnum =
			    new_dqw_seq & OPSRSM_DQE_SEQ_MASK;
			tmpdqe++;
		}

		/*
		 * get DQE offset for these DQ entries
		 */
		start_offset = (uint64_t)((char *)new_shdwdqw_o -
		    (char *)rd->rd_shdwdqw_f);
		end_offset = (uint64_t)((char *)(rd->rd_shdwdqw_l + 1) -
		    (char *)rd->rd_shdwdqw_f);
		D6("opsrsm_sync_dqe: start 0x%lx end 0x%lx", start_offset,
		    end_offset);

		/*
		 * Round down and up to 64-byte boundaries
		 */
		start_offset = start_offset & OPSRSM_CACHELINE_MASK;
		end_offset = OPSRSM_CACHELINE_ROUNDUP(end_offset);
		D6("opsrsm_sync_dqe: start 0x%lx end 0x%lx", start_offset,
		    end_offset);

		/*
		 * Push to remote side
		 */
		stat = RSM_PUT(rd->rd_adapter->rsmrdt_ctlr_obj,
		    rd->rd_rxferhand, rd->rd_dqw_f_off + (off_t)start_offset,
		    ((char *)rd->rd_shdwdqw_f) + start_offset,
		    (size_t)(end_offset - start_offset));

		if (stat != RSM_SUCCESS) {
			goto done;
		}
		/*
		 * Successfully processed these entries.
		 * Wrap around to the beginning of DQE list, and
		 * update the sequence number for the next round.
		 */
		new_shdwdqw_o = rd->rd_shdwdqw_f;
		new_dqw_seq++;
		if (new_dqw_seq == 0)
			new_dqw_seq++;
	}

	/*
	 * Handle remaining sequential DQEs
	 */
	if (new_shdwdqw_o != rd->rd_shdwdqw_i) {
		opsrsm_dqe_t *tmpdqe = new_shdwdqw_o;
		while (tmpdqe < rd->rd_shdwdqw_i) {
			tmpdqe->s.dq_seqnum =
			    new_dqw_seq & OPSRSM_DQE_SEQ_MASK;
			tmpdqe++;
		}

		/*
		 * get DQE offset for these DQ entries
		 */
		start_offset = (uint64_t)((char *)new_shdwdqw_o -
		    (char *)rd->rd_shdwdqw_f);
		end_offset = (uint64_t)((char *)rd->rd_shdwdqw_i -
		    (char *)rd->rd_shdwdqw_f);
		D6("opsrsm_sync_dqe: start 0x%lx end 0x%lx", start_offset,
		    end_offset);

		/*
		 * Round down and up to 64-byte cacheline boundaries
		 */
		start_offset = start_offset & OPSRSM_CACHELINE_MASK;
		end_offset = OPSRSM_CACHELINE_ROUNDUP(end_offset);
		D6("opsrsm_sync_dqe: start 0x%lx end 0x%lx", start_offset,
		    end_offset);

		/*
		 * Push to remote side
		 */
		stat = RSM_PUT(rd->rd_adapter->rsmrdt_ctlr_obj,
		    rd->rd_rxferhand, rd->rd_dqw_f_off + (off_t)start_offset,
		    ((char *)rd->rd_shdwdqw_f) + start_offset,
		    (size_t)(end_offset - start_offset));

		if (stat != RSM_SUCCESS) {
			goto done;
		}
		new_shdwdqw_o = rd->rd_shdwdqw_i;
	}

	stat = RSM_CLOSE_BARRIER(rd->rd_adapter->rsmrdt_ctlr_obj, &dq_barrier);

done:;
	if (stat != RSM_SUCCESS) {
		/* set timer to retry */
		opsrsmdev->opsrsm_oerrors++;
		if (rd->rd_sync_dqe_tmo_id == 0 &&
		    rd->rd_state == OPSRSM_STATE_W_READY) {
			rd->rd_sync_dqe_tmo_id = timeout(opsrsm_sync_dqe_tmo,
			    rd, (clock_t)opsrsmp->
			    opsrsm_param.opsrsm_sync_tmo);
		}
		mutex_exit(&rd->rd_net_lock);
		return (stat);
	} else {
		rd->rd_shdwdqw_o = new_shdwdqw_o;
		rd->rd_dqw_seq = new_dqw_seq;
	}

retry_int:
	rd->rd_retry_int = B_FALSE;
	msg_cnt = rd->rd_pkts_delivered;
	rd->rd_pkts_delivered = 0;

	msg.p.hdr.reqtype = OPSRSM_MSG_SYNC_DQE;
	msg.p.hdr.seqno = 0;
	msg.p.hdr.opsrsm_version = OPSRSM_VERSION;
	msg.p.m.syncdqe.rcv_segid = rd->rd_rxfersegid;
	msg.p.m.syncdqe.msg_cnt = msg_cnt;

	send_obj.is_data = &msg;
	send_obj.is_size = sizeof (opsrsm_msg_t);
	send_obj.is_flags = RSM_DLPI_SQFLAGS;
	send_obj.is_wait = 0;
	mutex_exit(&rd->rd_net_lock);
	/*
	 * send interrupt to remote node. need to release rd_net_lock
	 * first because RSM_SEND can block.
	 */
	stat = RSM_SEND(rd->rd_adapter->rsmrdt_ctlr_obj, rd->rsm_sendq,
	    &send_obj, NULL);

	mutex_enter(&rd->rd_net_lock);
	if (stat == RSMERR_CONN_ABORTED) {
		DERR("RSM_SEND: connection aborted");
		opsrsmdev->opsrsm_intr_send_errs++;
		mutex_exit(&rd->rd_net_lock);
		opsrsm_lostconn(rd);
		return (stat);
	} else if (stat != RSM_SUCCESS) {
		opsrsmdev->opsrsm_collisions++;
		rd->rd_nretries++;
		rd->rd_retry_int = B_TRUE;
		rd->rd_pkts_delivered += msg_cnt;
		if (rd->rd_sync_dqe_tmo_id == 0) {
			rd->rd_sync_dqe_tmo_id = timeout(opsrsm_sync_dqe_tmo,
			    rd, (clock_t)opsrsmp->
			    opsrsm_param.opsrsm_sync_tmo);
		}
		mutex_exit(&rd->rd_net_lock);
		return (stat);
	}
	mutex_exit(&rd->rd_net_lock);

	/* free up only the messages that were delivered */
	mutex_enter(&rd->rd_freeq_lock);
	if (!rd->rd_freeq_freeze) {
		opsrsm_queue_t *q;
		uint_t cnt = 0;

		q = &rd->rd_freeq;
		while ((q)->q_head != NULL) {
			mblk_t *mp;

			cnt++;
			mp = (q)->q_head;
			if ((q)->q_head == (q)->q_tail) {
				ASSERT(mp->b_next == NULL);
				ASSERT((q)->q_len == 1);
				(q)->q_tail = NULL;
			}
			(q)->q_head = mp->b_next;
			mp->b_prev = mp->b_next = NULL;
			mp->b_cont = NULL;
			freemsg(mp);
			(q)->q_len--;
			if (cnt == msg_cnt) {
				break;
			}
		}
	}
	mutex_exit(&rd->rd_freeq_lock);
	return (0);
}


static int
opsrsm_sync_fqe(opsrsm_dest_t *rd)
{
	uint64_t start_offset, end_offset;
	opsrsm_fqe_t *new_shdwfqw_o = NULL;
	ushort_t new_fqw_seq = 0;
	rsm_barrier_t fq_barrier;
	int stat = RSM_SUCCESS;

	mutex_enter(&rd->rd_fqr_lock);

	ASSERT((rd->rd_fqr_flags & OPSRSM_FQR_LOCKED) == 0);
	/*
	 * setting this flag guarantees that no other
	 * thread can access the shadow queue pointers
	 * (rd_shdwfqw_*) used by the RSM calls below.
	 */
	rd->rd_fqr_flags |= OPSRSM_FQR_LOCKED;
	new_shdwfqw_o = rd->rd_shdwfqw_o;
	new_fqw_seq = rd->rd_fqw_seq;

	/* If nothing's queued, nothing to do */
	if (rd->rd_shdwfqw_i == rd->rd_shdwfqw_o) {
		boolean_t putfqes;

		D1("opsrsmsyncfqe: no work, done");
		putfqes = (rd->rd_queued_fqe_list != NULL);
		if (!putfqes) {
			rd->rd_fqr_flags &= ~OPSRSM_FQR_LOCKED;
		}
		mutex_exit(&rd->rd_fqr_lock);
		if (putfqes) goto done;
		return (0);
	}

	/* If network down, nothing to do either */
	if (rd->rd_stopq) {
		D1("opsrsmsyncfqe: stopq on, done");
		rd->rd_fqr_flags &= ~OPSRSM_FQR_LOCKED;
		mutex_exit(&rd->rd_fqr_lock);
		return (0);
	}

	mutex_exit(&rd->rd_fqr_lock);

	stat = RSM_OPEN_BARRIER_REGION(rd->rd_adapter->rsmrdt_ctlr_obj,
	    rd->rd_rxferhand, &fq_barrier);

	if (stat != RSM_SUCCESS) {
		goto done;
	}

	/*
	 * If we've wrapped around, so that the next element to go comes from
	 * a lower address than where we started, do it in two segments.
	 */
	if (new_shdwfqw_o > rd->rd_shdwfqw_i) {
		/*
		 * Process the elements from the current to the end of
		 * the list, then adjust pointers to point to start of
		 * list.
		 */

		/*
		 * Set the sequence numbers in these FQEs.
		 */
		opsrsm_fqe_t *tmpfqe = new_shdwfqw_o;
		while (tmpfqe <= rd->rd_shdwfqw_l) {
			tmpfqe->s.fq_seqnum =
			    new_fqw_seq & OPSRSM_FQE_SEQ_MASK;
			tmpfqe++;
		}

		/*
		 * Get FQE offsets for FQ range being updated
		 */
		start_offset = (uint64_t)((char *)new_shdwfqw_o -
		    (char *)rd->rd_shdwfqw_f);
		end_offset = (uint64_t)((char *)(rd->rd_shdwfqw_l + 1) -
		    (char *)rd->rd_shdwfqw_f);

		D6("opsrsmsyncfqe: start 0x%lx end 0x%lx", start_offset,
		    end_offset);

		/*
		 * Round down and up to 64-byte boundaries
		 */
		start_offset = start_offset & OPSRSM_CACHELINE_MASK;
		end_offset = OPSRSM_CACHELINE_ROUNDUP(end_offset);

		D6("opsrsmsyncfqe: start 0x%lx end 0x%lx", start_offset,
		    end_offset);

		/*
		 * Push to remote side
		 */

		stat = RSM_PUT(rd->rd_adapter->rsmrdt_ctlr_obj,
		    rd->rd_rxferhand, rd->rd_fqw_f_off + (off_t)start_offset,
		    ((char *)rd->rd_shdwfqw_f) + start_offset,
		    (size_t)(end_offset - start_offset));

		if (stat != RSM_SUCCESS) {
			goto done;
		}
		/*
		 * Successfully processed these entries.
		 * Wrap around to the beginning of FQE list, and
		 * update sequence number for the next round
		 */
		new_shdwfqw_o = rd->rd_shdwfqw_f;
		new_fqw_seq++;
		if (new_fqw_seq == 0)
			new_fqw_seq++;
	}

	/*
	 * Handle remaining sequential FQEs
	 */
	if ((stat == RSM_SUCCESS) && (new_shdwfqw_o != rd->rd_shdwfqw_i)) {
		opsrsm_fqe_t *tmpfqe = new_shdwfqw_o;
		/*
		 * Set the sequence numbers in these FQEs.
		 */
		while (tmpfqe < rd->rd_shdwfqw_i) {
			tmpfqe->s.fq_seqnum =
			    new_fqw_seq & OPSRSM_FQE_SEQ_MASK;
			tmpfqe++;
		}

		/*
		 * Get FQE offsets for FQ range being updated
		 */
		start_offset = (uint64_t)((char *)new_shdwfqw_o -
		    (char *)rd->rd_shdwfqw_f);
		end_offset = (uint64_t)((char *)rd->rd_shdwfqw_i -
		    (char *)rd->rd_shdwfqw_f);
		D6("opsrsmsyncfqe: start 0x%lx end 0x%lx", start_offset,
		    end_offset);

		/*
		 * Round down and up to 64-byte boundaries
		 */
		start_offset = start_offset & OPSRSM_CACHELINE_MASK;
		end_offset = OPSRSM_CACHELINE_ROUNDUP(end_offset);
		D6("opsrsmsyncfqe: start 0x%lx end 0x%lx", start_offset,
		    end_offset);

		/*
		 * Push to remote side
		 */
		stat = RSM_PUT(rd->rd_adapter->rsmrdt_ctlr_obj,
		    rd->rd_rxferhand, rd->rd_fqw_f_off + (off_t)start_offset,
		    ((char *)rd->rd_shdwfqw_f) + start_offset,
		    (size_t)(end_offset - start_offset));

		if (stat != RSM_SUCCESS) {
			goto done;
		}
		new_shdwfqw_o = rd->rd_shdwfqw_i;
	}
	stat = RSM_CLOSE_BARRIER(rd->rd_adapter->rsmrdt_ctlr_obj, &fq_barrier);

done:;
	mutex_enter(&rd->rd_fqr_lock);
	ASSERT((rd->rd_fqr_flags & OPSRSM_FQR_LOCKED) != 0);
	if (stat != RSM_SUCCESS) {
		opsrsmdev->opsrsm_errs++;
		if (rd->rd_sync_fqe_tmo_id == 0 &&
		    rd->rd_state == OPSRSM_STATE_W_READY) {
			rd->rd_sync_fqe_tmo_id = timeout(opsrsm_sync_fqe_tmo,
			    rd, (clock_t)opsrsmdev->
			    opsrsm_param.opsrsm_sync_tmo);
		}
	} else {
		rd->rd_shdwfqw_o = new_shdwfqw_o;
		rd->rd_fqw_seq = new_fqw_seq;
	}

	/*
	 * an interrupt thread might have enqueued fqe entries
	 * while we were in the RSM calls. we now need to take
	 * these entries and update the actual shadow fq. we also
	 * need to schedule a sync_fqe event after updating our
	 * shadow fq.
	 */
	if (rd->rd_queued_fqe_list != NULL &&
	    rd->rd_state == OPSRSM_STATE_W_READY) {
		opsrsm_queued_fqe_t *q, *qfqe = rd->rd_queued_fqe_list;

		for (;;) {
			q = qfqe;
			qfqe = qfqe->qf_next;

			q->qf_next = NULL;
			ASSERT(q->qf_bufnum != -1);
			opsrsmputfqe_nolock(rd, q->qf_bufnum);
			opsrsm_queued_fqe_free(rd, q);
			if (qfqe == NULL) break;
		}
		rd->rd_queued_fqe_list = NULL;
		rd->rd_queued_fqe_tail = NULL;
		opsrsm_event_add(rd, OPSRSM_EVT_SYNC_FQE);
	}
	rd->rd_fqr_flags &= ~OPSRSM_FQR_LOCKED;
	mutex_exit(&rd->rd_fqr_lock);
	return (0);
}

static void
opsrsm_queued_msg_append(opsrsm_dest_t *rd, opsrsm_queued_msg_t *qmsg)
{
	mutex_enter(&rd->rd_msgs_lock);
	if (rd->rd_msgs == NULL) {
		rd->rd_msgs = qmsg;
		rd->rd_msgs_tail = qmsg;
	} else {
		rd->rd_msgs_tail->qm_next = qmsg;
		rd->rd_msgs_tail = qmsg;
	}
	mutex_exit(&rd->rd_msgs_lock);
}

static void
opsrsm_queued_msg_flush(opsrsm_dest_t *rd)
{
	opsrsm_queued_msg_t *qmsg;
	int cnt = 0;

	mutex_enter(&rd->rd_msgs_lock);
	while (rd->rd_msgs != NULL) {
		cnt++;
		qmsg = rd->rd_msgs;
		rd->rd_msgs = qmsg->qm_next;
		kmem_free(qmsg, sizeof (opsrsm_queued_msg_t));
	}
	mutex_exit(&rd->rd_msgs_lock);
	if (cnt > 0) {
		DINFO("0x%x flushed %d queued msgs\n",
		    rd->rd_local_skey, cnt);
	}
}

/*
 * This function is called by the event thread to send
 * queued interrupt messages to a peer node. some interrupt
 * messages need to be delivered in this manner because RSMPI
 * prohibits the sending of interrupt messages inside an
 * RSM interrupt handler or callout.
 */
static void
opsrsm_queued_msg_send(opsrsm_dest_t *rd)
{
	opsrsm_queued_msg_t *qmsg;
	rsm_send_t send_obj;
	boolean_t more_msgs;
	int status;

	mutex_enter(&rd->rd_msgs_lock);
	qmsg = rd->rd_msgs;
	if (qmsg != NULL) {
		rd->rd_msgs = qmsg->qm_next;
		if (rd->rd_msgs == NULL) {
			rd->rd_msgs_tail = NULL;
		}
	} else {
		mutex_exit(&rd->rd_msgs_lock);
		return;
	}
	mutex_exit(&rd->rd_msgs_lock);

	send_obj.is_data = &qmsg->qm_msg;
	send_obj.is_size = sizeof (opsrsm_msg_t);
	send_obj.is_flags = RSM_DLPI_SQFLAGS;
	send_obj.is_wait = 0;
	status = RSM_SEND(rd->rd_adapter->rsmrdt_ctlr_obj, rd->rsm_sendq,
	    &send_obj, NULL);

	mutex_enter(&rd->rd_msgs_lock);
	if (status != RSM_SUCCESS) {
		if (++qmsg->qm_retries > opsrsm_queued_msg_max_retries) {
			DINFO("0x%x cannot send msg %d, err = %d, "
			    "retrying...", rd->rd_local_skey,
			    qmsg->qm_msg.p.hdr.reqtype, status);
			qmsg->qm_retries = 0;
		}
		qmsg->qm_next = rd->rd_msgs;
		rd->rd_msgs = qmsg;
		if (rd->rd_msgs_tail == NULL) {
			rd->rd_msgs_tail = qmsg;
		}
	} else {
		kmem_free(qmsg, sizeof (opsrsm_queued_msg_t));
	}
	more_msgs = (rd->rd_msgs != NULL);
	mutex_exit(&rd->rd_msgs_lock);

	if (more_msgs) {
		if (status != RSM_SUCCESS) {
			delay(1);
		}
		opsrsm_event_add(rd, OPSRSM_EVT_SEND_MSG);
	}
}

static void
opsrsm_event_add(opsrsm_dest_t *rd, uint32_t evt_type)
{
	mutex_enter(&rd->rd_evt_lock);
	rd->rd_evt_flags |= evt_type;
	cv_signal(&rd->rd_evt_cv);
	mutex_exit(&rd->rd_evt_lock);
}

/*
 * This thread is used for processing events that cannot
 * be done in interrupt context.
 */
static void
opsrsm_event_thread(void *arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;
	boolean_t sync_dqe, sync_fqe, send_msg;
	int events;

	mutex_enter(&rd->rd_evt_lock);
again:;
	events = 0;
	sync_dqe = ((rd->rd_evt_flags & OPSRSM_EVT_SYNC_DQE) != 0);
	if (sync_dqe) {
		events++;
		rd->rd_evt_flags &= ~OPSRSM_EVT_SYNC_DQE;
	}
	sync_fqe = ((rd->rd_evt_flags & OPSRSM_EVT_SYNC_FQE) != 0);
	if (sync_fqe) {
		events++;
		rd->rd_evt_flags &= ~OPSRSM_EVT_SYNC_FQE;
	}
	send_msg = ((rd->rd_evt_flags & OPSRSM_EVT_SEND_MSG) != 0);
	if (send_msg) {
		events++;
		rd->rd_evt_flags &= ~OPSRSM_EVT_SEND_MSG;
	}

	if ((rd->rd_evt_flags & OPSRSM_EVT_STOP) != 0) {
		rd->rd_evt_flags |= OPSRSM_EVT_DONE;
		cv_signal(&rd->rd_evt_wait_cv);
		mutex_exit(&rd->rd_evt_lock);
		DINFO("0x%x event thread exiting\n", rd->rd_local_skey);
		return;
	}
	if (events == 0 || rd->rd_evt_flags == 0) {
		cv_wait(&rd->rd_evt_cv, &rd->rd_evt_lock);
		goto again;
	}
	mutex_exit(&rd->rd_evt_lock);

	if (sync_dqe) {
		(void) opsrsm_sync_dqe(rd);
	}
	if (sync_fqe) {
		(void) opsrsm_sync_fqe(rd);
	}
	if (send_msg) {
		opsrsm_queued_msg_send(rd);
	}

	mutex_enter(&rd->rd_evt_lock);
	goto again;
}

static void
opsrsm_sync_dqe_tmo(void *arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;
	if (rd->rd_sync_dqe_tmo_id == 0) {
		return;
	}
	rd->rd_sync_dqe_tmo_id = 0;
	opsrsm_event_add(rd, OPSRSM_EVT_SYNC_DQE);
}

static void
opsrsm_sync_fqe_tmo(void *arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;
	if (rd->rd_sync_fqe_tmo_id == 0) {
		return;
	}
	rd->rd_sync_fqe_tmo_id = 0;
	opsrsm_event_add(rd, OPSRSM_EVT_SYNC_FQE);
}

static void
opsrsm_status_check_tmo(void *arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;

	if (rd->rd_status_tmo_id == 0 ||
	    rd->rd_state != OPSRSM_STATE_W_READY) {
		return;
	}
	if (OPSRSM_Q_LEN(&rd->rd_sendq) > 0 &&
	    ((uint32_t)ddi_get_lbolt() - rd->rd_last_sent) > 1000) {
		DINFO("RDT packets queued but not sent for %d "
		    "seconds: active_threads = %d, availfqe = %d, "
		    "qlen = %d, rd->rd_dstate = %d\n",
		    ((uint32_t)ddi_get_lbolt() - rd->rd_last_sent)/100,
		    rd->rd_active_threads, opsrsmavailfqe2(rd),
		    OPSRSM_Q_LEN(&rd->rd_sendq), rd->rd_dstate);

		if (((uint32_t)ddi_get_lbolt() - rd->rd_last_sent) > 9000) {
			opsrsm_lostconn(rd);
		}
		opsrsm_set_fqe_tmo(rd, (clock_t)opsrsmdev->
		    opsrsm_param.opsrsm_xmit_delay);
	}
	rd->rd_status_tmo_id = timeout(opsrsm_status_check_tmo, rd, 1000);
}

static int
opsrsmrexmit(opsrsm_dest_t *rd)
{
	int error = 0;
	mblk_t *mp;

	switch (rd->rd_xmit_state) {
	case OPSRSM_XMIT_RETRY_DATA:
		error = opsrsm_start_batch(rd, 0);
		mp = OPSRSM_Q_HEAD(&rd->rd_pendq);
		while (mp) {
			error = opsrsm_write_data(rd, mp);
			mp = OPSRSM_Q_NEXT(&rd->rd_pendq, mp);
		}
		ASSERT(mp == NULL);
		error = opsrsm_end_batch(rd);
		break;
	default:
		cmn_err(CE_PANIC, "opsrsmrexmit: invalid state = %d\n",
		    rd->rd_xmit_state);
		break;
	}
	return (error);
}

static void
opsrsmxmit_thread(void *arg)
{
	(void) opsrsmxmit((opsrsm_dest_t *)arg, NULL);
}

static int
opsrsmxmit(opsrsm_dest_t *rd, mblk_t *mp)
{
	opsrsm_t *opsrsmp = opsrsmdev;
	boolean_t discarded = B_FALSE;
	boolean_t do_sync = B_FALSE;
	boolean_t nofqe, nopendw;
	int err = 0, pkts_sent, sendq_len;

	mutex_enter(&rd->rd_sendq_lock);
	rd->rd_active_threads++;
	if (mp != NULL) {
		if (OPSRSM_Q_LEN(&rd->rd_sendq) >=
		    opsrsmp->opsrsm_param.opsrsm_max_queued_pkts) {
			opsrsmp->opsrsm_pkts_discarded++;
			freemsg(mp);
			mp = NULL;
			discarded = B_TRUE;
			err = EWOULDBLOCK;
		} else {
			uint32_t seqno = OPSRSM_MESSAGE_HDRPTR(mp)->seqno;
			if (seqno == 0) {
				OPSRSM_MESSAGE_HDRPTR(mp)->seqno =
				    rd->rd_next_lseqno;
				OPSRSM_MESSAGE_HDRPTR(mp)->skey =
				    rd->rd_local_skey;

				rd->rd_next_lseqno++;
				if (rd->rd_next_lseqno == 0)
					rd->rd_next_lseqno++;
			}
			opsrsmp->opsrsm_pkts_queued++;
			OPSRSM_Q_APPEND(&rd->rd_sendq, mp);
			mp->b_prev = (mblk_t *)0;
		}
		if (!discarded && rd->rd_active_threads > ncpus_online) {
			rd->rd_active_threads--;
			opsrsmp->opsrsm_last_sendq_len =
			    (uint32_t)OPSRSM_Q_LEN(&rd->rd_sendq);
			mutex_exit(&rd->rd_sendq_lock);
			UNREFDEST(rd);
			return (err);
		}
	}
	mutex_exit(&rd->rd_sendq_lock);

	mutex_enter(&rd->rd_xmit_lock);
	if (rd->rd_xmit_state < OPSRSM_XMIT_BARRIER_CLOSED) {
		if (rd->rd_xmit_state == OPSRSM_XMIT_DISCONNECTED &&
		    !discarded) {
			err = ENETDOWN;
		}
		mutex_enter(&rd->rd_sendq_lock);
		rd->rd_active_threads--;
		opsrsmp->opsrsm_last_sendq_len =
			(uint32_t)OPSRSM_Q_LEN(&rd->rd_sendq);
		mutex_exit(&rd->rd_sendq_lock);
		mutex_exit(&rd->rd_xmit_lock);
		UNREFDEST(rd);
		return (err);
	}
	if (rd->rd_xmit_state > OPSRSM_XMIT_BARRIER_OPENED) {
		boolean_t dont_rexmit = B_FALSE;

		dont_rexmit = (mp != NULL);
		if (dont_rexmit || opsrsmrexmit(rd) != 0) {
			if (!discarded) err = 0;
			mutex_enter(&rd->rd_sendq_lock);
			rd->rd_active_threads--;
			opsrsmp->opsrsm_last_sendq_len =
			    (uint32_t)OPSRSM_Q_LEN(&rd->rd_sendq);
			mutex_exit(&rd->rd_sendq_lock);
			mutex_exit(&rd->rd_xmit_lock);
			UNREFDEST(rd);
			return (err);
		} else {
			mutex_exit(&rd->rd_xmit_lock);
			(void) opsrsm_sync_dqe(rd);
			mutex_enter(&rd->rd_xmit_lock);
		}
	} else if (discarded) {
		mutex_enter(&rd->rd_sendq_lock);
		rd->rd_active_threads--;
		opsrsmp->opsrsm_last_sendq_len =
			(uint32_t)OPSRSM_Q_LEN(&rd->rd_sendq);
		mutex_exit(&rd->rd_sendq_lock);
		mutex_exit(&rd->rd_xmit_lock);
		UNREFDEST(rd);
		return (err);
	}

	nofqe = B_FALSE;
	nopendw = B_TRUE;
	pkts_sent = 0;
	opsrsm_cancel_xmit_tmo(rd);
	for (;;) {
		mblk_t *nmp;
		int bufnum, qlen, pktlen;

		if (rd->rd_xmit_state < OPSRSM_XMIT_BARRIER_CLOSED ||
		    rd->rd_xmit_state > OPSRSM_XMIT_BARRIER_OPENED) {
			mutex_enter(&rd->rd_sendq_lock);
			rd->rd_active_threads--;
			mutex_exit(&rd->rd_sendq_lock);
			break;
		}
		mutex_enter(&rd->rd_sendq_lock);
		qlen = OPSRSM_Q_LEN(&rd->rd_sendq);
		if (qlen == 0) {
			rd->rd_active_threads--;
			mutex_exit(&rd->rd_sendq_lock);
			break;
		}
		if (opsrsmgetfqe(rd, &bufnum) == 0) {
			opsrsm_set_fqe_tmo(rd, (clock_t)opsrsmp->
			    opsrsm_param.opsrsm_xmit_delay);
			nofqe = B_TRUE;
			rd->rd_active_threads--;
			mutex_exit(&rd->rd_sendq_lock);
			break;
		}
		OPSRSM_Q_REMOVE(&rd->rd_sendq, nmp);
		mutex_exit(&rd->rd_sendq_lock);

		if (rd->rd_xmit_state == OPSRSM_XMIT_BARRIER_CLOSED) {
			ASSERT(OPSRSM_Q_LEN(&rd->rd_pendq) == 0 &&
			    rd->rd_start_time == 0 &&
			    rd->rd_data_collected == 0 &&
			    rd->rd_writes_completed == 0);
			err = opsrsm_start_batch(rd, (uint32_t)nmp->b_prev);
		}
		opsrsmp->opsrsm_pkts_pending++;
		OPSRSM_Q_APPEND(&rd->rd_pendq, nmp);
		nmp->b_prev = (mblk_t *)bufnum;
		pktlen = MBLKL(nmp);
		mutex_exit(&rd->rd_xmit_lock);

		err = opsrsm_write_data(rd, nmp);
		pkts_sent++;

		mutex_enter(&rd->rd_xmit_lock);
		ASSERT(rd->rd_xmit_state == OPSRSM_XMIT_BARRIER_OPENED ||
		    rd->rd_xmit_state == OPSRSM_XMIT_DISCONNECTED);
		rd->rd_writes_completed++;
		rd->rd_data_collected += (uint32_t)pktlen;
		OPSRSM_ADAPT_THRESHOLD(rd, (uint32_t)pktlen);
		if (opsrsmdev->opsrsm_param.opsrsm_adaptive_intr == 0) {
			rd->rd_last_sent = (uint32_t)ddi_get_lbolt();
		}
		nopendw = OPSRSM_NO_PENDING_WRITES(rd);
		if (nopendw && OPSRSM_REACHED_DATA_THRESHOLD(rd)) {
			err = opsrsm_end_batch(rd);
			if (err != 0) {
				mutex_enter(&rd->rd_sendq_lock);
				rd->rd_active_threads--;
				mutex_exit(&rd->rd_sendq_lock);
				break;
			} else {
				mutex_exit(&rd->rd_xmit_lock);
				(void) opsrsm_sync_dqe(rd);
				mutex_enter(&rd->rd_xmit_lock);
			}
		} else if (!nopendw) {
			opsrsmp->opsrsm_pending_writes++;
			nopendw = B_TRUE;
		}
	}
	if (nofqe) opsrsmp->opsrsm_no_fqes++;
	if (pkts_sent == 0 && mp == NULL) {
		rd->rd_adaptive_threshold = 0;
		rd->rd_pkt_freq = 0;
		opsrsmp->opsrsm_starts++;
	}

	nopendw = OPSRSM_NO_PENDING_WRITES(rd);
	if (nopendw && (rd->rd_xmit_state == OPSRSM_XMIT_BARRIER_OPENED)) {
		if (OPSRSM_REACHED_DATA_THRESHOLD(rd) ||
		    (pkts_sent == 0 && mp == NULL) ||
		    (nofqe && OPSRSM_Q_LEN(&rd->rd_pendq) == opsrsmp->
		    opsrsm_param.opsrsm_buffers)) {
			err = opsrsm_end_batch(rd);
			if (err == 0) {
				do_sync = B_TRUE;
			}
		} else {
			opsrsm_set_xmit_tmo(rd,
				(int)opsrsmp->opsrsm_param.opsrsm_xmit_delay);
		}
	} else if (!nopendw) {
		opsrsmp->opsrsm_pending_writes++;
		nopendw = B_TRUE;
	}
	opsrsmp->opsrsm_last_pendq_len =
		(uint32_t)OPSRSM_Q_LEN(&rd->rd_pendq);
	mutex_exit(&rd->rd_xmit_lock);

	if (do_sync) {
		(void) opsrsm_sync_dqe(rd);
	}

	sendq_len = OPSRSM_Q_LEN(&rd->rd_sendq);
	opsrsmp->opsrsm_last_sendq_len = (uint32_t)sendq_len;
	if (sendq_len <= 1024) {
		opsrsm_wake_senders(rd, POLLOUT);
	}
	UNREFDEST(rd);

	if (err != ENETDOWN) err = 0;
	if (discarded) err = EWOULDBLOCK;
	return (err);
}

/*
 * Callback routine, called when an desballoc'ed buffer is eventually freed.
 */
static void
opsrsmfreebuf(
	opsrsmbuf_t *rbp)	/* Structure describing freed buffer */
{
	opsrsm_dest_t *rd = rbp->rb_rd;
	int delflg, zerflg;

	D1("opsrsmfreebuf: rbp 0x%p", (void *)rbp);

	/*
	 * Find out if this is the last outstanding buffer, and whether we're
	 * being deleted.
	 */
	mutex_enter(&rd->rd_nlb_lock);

	rd->rd_nlb--;
	delflg = rd->rd_nlb_del;
	zerflg = (rd->rd_nlb == 0);

	mutex_exit(&rd->rd_nlb_lock);

	/*
	 * If we're being deleted, we don't put this buffer on the free queue.
	 * Also, if we're being deleted, and this was the last outstanding
	 * buffer, we do an UNREF.  Otherwise we send this buffer to the other
	 * system for reuse.
	 */
	if (delflg) {
		if (zerflg)
			UNREFDEST(rd);
	} else {
		opsrsmputfqe(rd, rbp->rb_bufnum);
		opsrsm_event_add(rd, OPSRSM_EVT_SYNC_FQE);
	}

	D1("opsrsmfreebuf: done");
}

static void
opsrsmdemux(mblk_t *mp, opsrsm_dest_t *rd)
{
	opsrsmresource_t *rp;
	uint32_t rportnum = OPSRSM_MESSAGE_HDRPTR(mp)->rportnum;

	/* control messages do not belong here */
	if (OPSRSM_MESSAGE_HDRPTR(mp)->option != 0) {
		freemsg(mp);
		return;
	}
	if (rd != NULL) {
		uint32_t seq_key, seq_no;

		seq_key = OPSRSM_MESSAGE_HDRPTR(mp)->skey;
		seq_no = OPSRSM_MESSAGE_HDRPTR(mp)->seqno;
		ASSERT(seq_no != 0);
		if (seq_key == rd->rd_remote_skey) {
			if (seq_no == rd->rd_next_rseqno) {
				rd->rd_next_rseqno++;
				if (rd->rd_next_rseqno == 0)
					rd->rd_next_rseqno++;
			} else {
				cmn_err(CE_PANIC, "opsrsmdemux: seqno = %d "
				    "expected seqno = %d\n", seq_no,
				    rd->rd_next_rseqno);
				freemsg(mp);
				return;
			}
		} else {
			opsrsm_failover_info_t *finfo =
			    opsrsm_finfo_lookup_by_remote_skey(seq_key);

			if (finfo == NULL) {
				cmn_err(CE_CONT, "opsrsmdemux: cannot find "
				    "skey = 0x%x\n", seq_key);
				freemsg(mp);
				return;
			} else {
				if (seq_no != finfo->fi_next_rseqno) {
					/*
					 * We get here if a rexmitted packet is
					 * a duplicate or if it is out of
					 * sequence.
					 */
					DERR("received duplicate packet, "
					    "seqno = %d, expected = %d\n",
					    seq_no, finfo->fi_next_rseqno);
					freemsg(mp);
					return;
				} else {
					finfo->fi_next_rseqno++;
					if (finfo->fi_next_rseqno == 0)
						finfo->fi_next_rseqno++;
				}
			}
		}
	}

	rp = opsrsmresource_lookup(rportnum, OPSRSM_RO_INCREFCNT);
	if (rp == NULL) {
		opsrsm_queued_msg_t *qmsg;

		DERR("opsrsmdemux: port %d has no receiver, dropping message",
		    rportnum);

		/*
		 * Do not send error notifications if a segment is about to
		 * be torn down.
		 */
		if (rd != NULL && rd->rd_state != OPSRSM_STATE_W_READY) {
			freemsg(mp);
			return;
		}
		qmsg = (opsrsm_queued_msg_t *)kmem_zalloc(sizeof (*qmsg),
		    KM_NOSLEEP);
		if (qmsg == NULL) {
			DERR("opsrsmdemux: cannot allocate qmsg");
			freemsg(mp);
			return;
		}
		/*
		 * Tell the sender that receiver doesn't exist.
		 */
		qmsg->qm_msg.p.hdr.reqtype = RSMRDT_MSG_SEND_ERR;
		qmsg->qm_msg.p.hdr.seqno = 0;
		qmsg->qm_msg.p.hdr.opsrsm_version = OPSRSM_VERSION;
		qmsg->qm_msg.p.m.senderr.sender_portnum =
		    OPSRSM_MESSAGE_HDRPTR(mp)->lportnum;
		qmsg->qm_msg.p.m.senderr.sender_pkey =
		    OPSRSM_MESSAGE_HDRPTR(mp)->pkey;
		qmsg->qm_msg.p.m.senderr.errstate = OPSRSM_RS_NORECVR;
		qmsg->qm_retries = 0;

		opsrsm_queued_msg_append(rd, qmsg);
		opsrsm_event_add(rd, OPSRSM_EVT_SEND_MSG);
		freemsg(mp);
		return;
	}

	if (rp->rs_pkey != OPSRSM_MESSAGE_HDRPTR(mp)->pkey) {
		opsrsm_queued_msg_t *qmsg;

		DERR("opsrsmdemux: Invalid pkey: sender %d local %d, "
		    "dropping message", OPSRSM_MESSAGE_HDRPTR(mp)->pkey,
		    rp->rs_pkey);

		/*
		 * Do not send error notifications if a segment is about to
		 * be torn down.
		 */
		if (rd != NULL && rd->rd_state != OPSRSM_STATE_W_READY) {
			freemsg(mp);
			OPSRSM_RSUNREF(rp);
			return;
		}

		qmsg = (opsrsm_queued_msg_t *)kmem_zalloc(sizeof (*qmsg),
		    KM_NOSLEEP);
		if (qmsg == NULL) {
			DERR("opsrsmdemux: cannot allocate qmsg");
			freemsg(mp);
			OPSRSM_RSUNREF(rp);
			return;
		}
		/*
		 * Tell the sender about pkey mismatch.
		 */
		qmsg->qm_msg.p.hdr.reqtype = RSMRDT_MSG_SEND_ERR;
		qmsg->qm_msg.p.hdr.seqno = 0;
		qmsg->qm_msg.p.hdr.opsrsm_version = OPSRSM_VERSION;
		qmsg->qm_msg.p.m.senderr.sender_portnum =
		    OPSRSM_MESSAGE_HDRPTR(mp)->lportnum;
		qmsg->qm_msg.p.m.senderr.sender_pkey =
		    OPSRSM_MESSAGE_HDRPTR(mp)->pkey;
		qmsg->qm_msg.p.m.senderr.errstate = OPSRSM_RS_PKEYMISMATCH;
		qmsg->qm_retries = 0;

		opsrsm_queued_msg_append(rd, qmsg);
		opsrsm_event_add(rd, OPSRSM_EVT_SEND_MSG);
		freemsg(mp);
		OPSRSM_RSUNREF(rp);
		return;
	}

	if ((rp->rs_state & OPSRSM_RS_BOUND) == 0) {
		DERR("opsrsmdemux: port %d not bound (rp->rs_state = %d), "
		    "dropping message", rportnum, rp->rs_state);
		freemsg(mp);
		OPSRSM_RSUNREF(rp);
		return;
	}

	mutex_enter(&rp->rs_lock);
	OPSRSM_Q_APPEND(&rp->rs_recvq, mp);
	atomic_add_32(&opsrsm_pending_bytes, MBLKL(mp));

	if ((rp->rs_state & OPSRSM_RS_SIG) != 0) {
		cv_signal(&rp->rs_cv);
	}
	if ((rp->rs_events & POLLIN) == 0) {
		rp->rs_events |= POLLIN;
		mutex_exit(&rp->rs_lock);
		pollwakeup(&rp->rs_pollhd, POLLIN);
	} else {
		mutex_exit(&rp->rs_lock);
	}

	OPSRSM_RSUNREF(rp);
}

static int
opsrsmdemux_loopback(mblk_t *mp)
{
	opsrsmresource_t *rp;
	uint32_t rportnum = OPSRSM_MESSAGE_HDRPTR(mp)->rportnum;

	rp = opsrsmresource_lookup(rportnum, OPSRSM_RO_INCREFCNT);
	if (rp == NULL) {
		freemsg(mp);
		return (ESRCH);
	}

	if (rp->rs_pkey != OPSRSM_MESSAGE_HDRPTR(mp)->pkey) {
		freemsg(mp);
		OPSRSM_RSUNREF(rp);
		return (EACCES);
	}

	if ((rp->rs_state & OPSRSM_RS_BOUND) == 0) {
		DERR("opsrsmdemux: port %d not bound (rp->rs_state = %d), "
		    "dropping message", rportnum, rp->rs_state);
		freemsg(mp);
		OPSRSM_RSUNREF(rp);
		return (EADDRNOTAVAIL);
	}

	mutex_enter(&rp->rs_lock);
	if ((uint_t)OPSRSM_Q_LEN(&rp->rs_recvq) >=
	    opsrsmdev->opsrsm_param.opsrsm_max_loopback_pkts) {
		mutex_exit(&rp->rs_lock);
		freemsg(mp);
		OPSRSM_RSUNREF(rp);
		return (EWOULDBLOCK);
	}
	OPSRSM_Q_APPEND(&rp->rs_recvq, mp);
	atomic_add_32(&opsrsm_pending_bytes, MBLKL(mp));

	if ((rp->rs_state & OPSRSM_RS_SIG) != 0) {
		cv_signal(&rp->rs_cv);
	}
	if ((rp->rs_events & POLLIN) == 0) {
		rp->rs_events |= POLLIN;
		mutex_exit(&rp->rs_lock);
		pollwakeup(&rp->rs_pollhd, POLLIN);
	} else {
		mutex_exit(&rp->rs_lock);
	}
	OPSRSM_RSUNREF(rp);
	return (0);
}


/*
 * opsrsmread() takes the packet described by the arguments and sends it
 * upstream.
 */
static int
opsrsmread(
	opsrsm_dest_t *rd,	/* Destination pointer */
	int bufnum,	/* Index of buffer containing packet */
	int offset,	/* Offset of packet within buffer */
	int length,	/* Length of packet */
	ushort_t sap)	/* SAP for packet */
{
	opsrsm_t *opsrsmp = opsrsmdev;
	mblk_t *mp;
	boolean_t canloan = B_FALSE;
	boolean_t nobufs = B_FALSE;
	caddr_t bufptr;
	int buffree = 0;
	int calc_sz;

	D1("opsrsmread: rd 0x%p, bufnum %d, offset %d, length %d, sap 0x%x",
	    (void *)rd, bufnum, offset, length, sap);

	bufptr = (caddr_t)rd->rd_lbuf + ((uint_t)bufnum * rd->rd_lbuflen);

	/* Figure out if we can loan this buffer up or not */
	mutex_enter(&rd->rd_nlb_lock);
	nobufs = (rd->rd_rawmem_base_size > freemem * PAGESIZE) &&
	    ((rd->rd_sstate & OPSRSM_RSMS_LXFER_C) != 0);
	if (nobufs || rd->rd_nlb < (opsrsmp->opsrsm_param.opsrsm_buffers -
	    opsrsmp->opsrsm_param.opsrsm_buffers_retained)) {
		rd->rd_nlb++;
		canloan = B_TRUE;
	}
	mutex_exit(&rd->rd_nlb_lock);


	if (canloan) {
		/*
		 * We make the mblk cover the whole buffer in case anybody
		 * wants the leading/trailing space; below we adjust the
		 * rptr/wptr to describe the actual packet.
		 */
		mp = desballoc((uchar_t *)bufptr, rd->rd_lbuflen,
		    BPRI_LO, &(rd->rd_bufbase+bufnum)->rb_frtn);

		if (mp == NULL) {
			mutex_enter(&rd->rd_nlb_lock);
			rd->rd_nlb--;
			mutex_exit(&rd->rd_nlb_lock);

			opsrsmputfqe(rd, bufnum);
			buffree = 1;

			opsrsmp->opsrsm_ierrors++;
			cmn_err(CE_PANIC, "desballoc failed, dropping "
			    "packet\n");
			return (1);
		}
		mp->b_rptr += offset;
		mp->b_wptr = mp->b_rptr + length;

		opsrsmp->opsrsm_lbufs++;
	} else {
		/*
		 * We make the destination (within the new mblk) have the
		 * same address mod 64 as our source, so that the kernel
		 * bcopy is as efficient as possible.  (This is a sun4u
		 * bcopy optimization, not a RSM optimization.)
		 */
		mp = allocb((size_t)(length + 0x40), BPRI_LO);
		if (mp) {
			intptr_t dstoffset = (intptr_t)mp->b_rptr;

			dstoffset = offset - (dstoffset & 0x3f);
			if (dstoffset < 0)
				dstoffset += 0x40;

			mp->b_rptr += dstoffset;
			mp->b_wptr = mp->b_rptr + length;
			bcopy((void *)(bufptr + offset), (void *)mp->b_rptr,
			    (size_t)length);

			opsrsmp->opsrsm_nlbufs++;
			opsrsmputfqe(rd, bufnum);
			buffree = 1;
		} else {
			opsrsmputfqe(rd, bufnum);
			buffree = 1;
			opsrsmp->opsrsm_ierrors++;
			cmn_err(CE_PANIC, "allocb failed, dropping packet\n");
			return (1);
		}
	}

	calc_sz = (int)(OPSRSM_MESSAGE_HDRPTR(mp)->msg_sz +
	    OPSRSM_MESSAGE_HDRSZ);
	if (length != calc_sz) {
		cmn_err(CE_PANIC, "corrupted packet, length = %d, "
		    "calculated size = %d\n", length, calc_sz);
	}

	if (OPSRSM_MESSAGE_HDRPTR(mp)->option == OPSRSM_OPT_REXMIT_END) {
		opsrsm_option_rexmit_end(mp, rd);
	} else {
		opsrsmp->opsrsm_in_bytes += (uint32_t)length;
		opsrsmp->opsrsm_ipackets++;
		/*
		 * Do demux right here.
		 */
		opsrsmdemux(mp, rd);
	}
	D1("opsrsmread: canloan was %d, done", canloan);
	return (buffree);
}

/*
 * ****************************************************************
 *                                                               *
 * E N D       NEW DATA TRANSFER LOGIC                           *
 *                                                               *
 * ****************************************************************
 */

/*
 * ****************************************************************
 *                                                               *
 * B E G I N   RSM FAILOVER                                      *
 *                                                               *
 * ****************************************************************
 */

static opsrsm_failover_info_t *
opsrsm_finfo_add(opsrsm_dest_t *rd)
{
	opsrsm_failover_info_t *finfo = opsrsm_finfo_list;

	D1("adding finfo for dest = %p, remote_skey = %d\n",
	    rd, rd->rd_remote_skey);
	mutex_enter(&opsrsm_finfo_lock);
	while (finfo != NULL) {
		if (finfo->fi_dest == rd &&
		    finfo->fi_remote_skey == rd->rd_remote_skey &&
		    finfo->fi_local_skey == rd->rd_local_skey) {
			mutex_exit(&opsrsm_finfo_lock);
			return (finfo);
		}
		finfo = finfo->fi_next;
	}
	ASSERT(finfo == NULL);
	finfo = (opsrsm_failover_info_t *)kmem_zalloc(sizeof (*finfo),
	    KM_SLEEP);
	ASSERT(finfo != NULL);
	finfo->fi_nodeid = rd->rd_nodeid;
	finfo->fi_dest = rd;
	finfo->fi_remote_skey = rd->rd_remote_skey;
	finfo->fi_local_skey = rd->rd_local_skey;
	finfo->fi_next_rseqno = 0;
	finfo->fi_retval = -1;
	finfo->fi_waiters = 0;
	finfo->fi_status = 0;
	finfo->fi_last_accessed = ddi_get_lbolt();
	OPSRSM_Q_INIT(&finfo->fi_rexmitq);
	cv_init(&finfo->fi_cv, NULL, CV_DRIVER, NULL);
	cv_init(&finfo->fi_wait_cv, NULL, CV_DRIVER, NULL);
	mutex_init(&finfo->fi_lock, NULL, MUTEX_DRIVER, NULL);

	finfo->fi_next = opsrsm_finfo_list;
	opsrsm_finfo_list = finfo;
	mutex_exit(&opsrsm_finfo_lock);
	return (finfo);
}

static opsrsm_failover_info_t *
opsrsm_finfo_lookup_by_remote_skey(uint32_t skey)
{
	opsrsm_failover_info_t *finfo = opsrsm_finfo_list;

	mutex_enter(&opsrsm_finfo_lock);
	while (finfo != NULL) {
		if (finfo->fi_remote_skey == skey) {
			break;
		}
		finfo = finfo->fi_next;
	}
	if (finfo != NULL) {
		finfo->fi_last_accessed = ddi_get_lbolt();
	}
	mutex_exit(&opsrsm_finfo_lock);
	return (finfo);
}

static opsrsm_failover_info_t *
opsrsm_finfo_lookup_by_local_skey(uint32_t skey)
{
	opsrsm_failover_info_t *finfo = opsrsm_finfo_list;

	mutex_enter(&opsrsm_finfo_lock);
	while (finfo != NULL) {
		if (finfo->fi_local_skey == skey) {
			break;
		}
		finfo = finfo->fi_next;
	}
	if (finfo != NULL) {
		finfo->fi_last_accessed = ddi_get_lbolt();
	}
	mutex_exit(&opsrsm_finfo_lock);
	return (finfo);
}

static int
opsrsm_finfo_wait(uint32_t skey)
{
	opsrsm_failover_info_t *finfo = opsrsm_finfo_list;
	int error;

	mutex_enter(&opsrsm_finfo_lock);
	while (finfo != NULL) {
		if (finfo->fi_local_skey == skey) {
			break;
		}
		finfo = finfo->fi_next;
	}
	if (finfo == NULL) {
		mutex_exit(&opsrsm_finfo_lock);
		return (0);
	}

	finfo->fi_last_accessed = ddi_get_lbolt();
	error = finfo->fi_retval;
	if (error == -1) {
		int retval;

		finfo->fi_waiters++;
		retval = cv_wait_sig(&finfo->fi_cv, &opsrsm_finfo_lock);
		finfo->fi_waiters--;
		if (retval == 0) {
			error = EINTR;
		} else {
			error = finfo->fi_retval;
		}
	}
	mutex_exit(&opsrsm_finfo_lock);
	return (error);
}

static void
opsrsm_finfo_wakeup(opsrsm_failover_info_t *finfo, int retval)
{
	mutex_enter(&opsrsm_finfo_lock);
	finfo->fi_retval = retval;
	cv_broadcast(&finfo->fi_cv);
	mutex_exit(&opsrsm_finfo_lock);
}

static void
opsrsm_finfo_init(void)
{
	opsrsm_finfo_list = NULL;
	opsrsm_failover_taskq = taskq_create("failover", 8, maxclsyspri, 1, 8,
	    TASKQ_PREPOPULATE);
	opsrsm_failover_threads = 0;
	mutex_init(&opsrsm_finfo_lock, NULL, MUTEX_DRIVER, NULL);
	cv_init(&opsrsm_finfo_cv, NULL, CV_DRIVER, NULL);
}

static void
opsrsm_finfo_fini(void)
{
	opsrsm_failover_info_t *finfo = opsrsm_finfo_list, *f;

	mutex_enter(&opsrsm_finfo_lock);
	if (opsrsm_failover_threads > 0) {
		DINFO("cannot detach yet, failover_threads = %d, "
		    "need to wait for approx. %d secs\n",
		    opsrsm_failover_threads, 180 * opsrsm_failover_threads);
		cv_wait(&opsrsm_finfo_cv, &opsrsm_finfo_lock);
	}
	while (finfo != NULL) {
		ASSERT(finfo->fi_waiters == 0);
		OPSRSM_Q_FLUSH(&finfo->fi_rexmitq);
		cv_destroy(&finfo->fi_cv);
		cv_destroy(&finfo->fi_wait_cv);
		mutex_destroy(&finfo->fi_lock);
		f = finfo;
		finfo = finfo->fi_next;
		kmem_free(f, sizeof (*f));
	}
	mutex_exit(&opsrsm_finfo_lock);
	taskq_destroy(opsrsm_failover_taskq);
	cv_destroy(&opsrsm_finfo_cv);
	mutex_destroy(&opsrsm_finfo_lock);
}


static void
opsrsm_finfo_destroy(void *arg)
{
	opsrsm_failover_info_t *finfo = (opsrsm_failover_info_t *)arg;
	opsrsm_failover_info_t *fptr = opsrsm_finfo_list;

	mutex_enter(&opsrsm_finfo_lock);
	/*
	 * delay the destruction if it was touched recently
	 */
	if ((ddi_get_lbolt() - finfo->fi_last_accessed) <= 3000) {
		mutex_exit(&opsrsm_finfo_lock);
		(void) timeout(opsrsm_finfo_destroy, finfo, 3000);
		return;
	}
	/*
	 * delay the destruction if there are waiters yet to
	 * be woken up.
	 */
	if (finfo->fi_waiters > 0) {
		mutex_exit(&opsrsm_finfo_lock);
		(void) timeout(opsrsm_finfo_destroy, finfo, 10);
		return;
	}
	DINFO("failover: destroying finfo, local_skey 0x%x, "
	    "remote_skey 0x%x\n", finfo->fi_local_skey,
	    finfo->fi_remote_skey);
	if (fptr == finfo) {
		opsrsm_finfo_list = finfo->fi_next;
	} else {
		while (fptr != NULL) {
			if (fptr->fi_next == finfo) break;
			fptr = fptr->fi_next;
		}
		ASSERT(fptr != NULL);
		fptr->fi_next = finfo->fi_next;
	}

	ASSERT(finfo->fi_waiters == 0);
	cv_destroy(&finfo->fi_cv);
	cv_destroy(&finfo->fi_wait_cv);
	mutex_destroy(&finfo->fi_lock);
	kmem_free(finfo, sizeof (*finfo));
	opsrsm_failover_threads--;
	ASSERT(opsrsm_failover_threads >= 0);
	if (opsrsm_failover_threads == 0) {
		cv_broadcast(&opsrsm_finfo_cv);
	}
	mutex_exit(&opsrsm_finfo_lock);
}

static void
opsrsm_dispatch_failover(void *arg)
{
	opsrsm_failover_info_t *finfo = (opsrsm_failover_info_t *)arg;

	ASSERT(opsrsm_failover_taskq != NULL);
	if (taskq_dispatch(opsrsm_failover_taskq,
	    opsrsm_failover_thread, finfo, KM_NOSLEEP) == 0) {
		(void) timeout(opsrsm_dispatch_failover, finfo, 1);
	}
}

/* handler for failover related messages */
/*ARGSUSED*/
static void
opsrsmmsghdlr_finfo(opsrsm_dest_t *rd, opsrsm_msg_t *msg)
{
	opsrsm_failover_info_t *finfo;
	uint32_t skey = msg->p.m.finfoquery.skey;
	uint32_t flag = 0;

	finfo = opsrsm_finfo_lookup_by_local_skey(skey);
	if (finfo == NULL) {
		return;
	}
	switch (msg->p.hdr.reqtype) {
	case OPSRSM_MSG_FINFO_DEMUX_DONE:
		flag = OPSRSM_FINFO_DEMUX_DONE;
		if ((finfo->fi_status & flag) != 0) break;
		DINFO("failover: 0x%x peer node demux done\n", skey);
		break;
	case OPSRSM_MSG_FINFO_REPLY:
		flag = OPSRSM_FINFO_DEMUX_DONE | OPSRSM_FINFO_OLD_PROTO;
		DINFO("failover: 0x%x peer node uses old protocol\n", skey);
		break;
	case OPSRSM_MSG_FINFO_REXMIT_ACK:
		flag = OPSRSM_FINFO_REXMIT_ACKED;
		DINFO("failover: 0x%x rexmit marker acked\n", skey);
		break;
	default:
		cmn_err(CE_PANIC, "unknown message type\n");
	}

	mutex_enter(&finfo->fi_lock);
	finfo->fi_status |= flag;
	cv_broadcast(&finfo->fi_wait_cv);
	mutex_exit(&finfo->fi_lock);
}

static void
opsrsm_option_rexmit_end(mblk_t *mp, opsrsm_dest_t *rd)
{
	opsrsm_queued_msg_t *qmsg;
	opsrsm_ack_msg_t *ack;
	opsrsm_failover_info_t *finfo;

	ack = (opsrsm_ack_msg_t *)((caddr_t)mp->b_rptr +
	    OPSRSM_MESSAGE_HDRSZ);
	finfo = opsrsm_finfo_lookup_by_local_skey(ack->am_skey);
	if (finfo == NULL) {
		freemsg(mp);
		return;
	}
	freemsg(mp);

	mutex_enter(&finfo->fi_lock);
	finfo->fi_status |= OPSRSM_FINFO_REXMIT_DONE;
	cv_broadcast(&finfo->fi_wait_cv);
	mutex_exit(&finfo->fi_lock);

	if (rd->rd_state != OPSRSM_STATE_W_READY) {
		return;
	}
	qmsg = kmem_zalloc(sizeof (opsrsm_queued_msg_t), KM_NOSLEEP);
	if (qmsg == NULL) {
		return;
	}

	qmsg->qm_msg.p.hdr.reqtype = OPSRSM_MSG_FINFO_REXMIT_ACK;
	qmsg->qm_msg.p.hdr.seqno = 0;
	qmsg->qm_msg.p.hdr.opsrsm_version = OPSRSM_VERSION;
	qmsg->qm_msg.p.m.finfoquery.skey = finfo->fi_remote_skey;
	qmsg->qm_retries = 0;

	opsrsm_queued_msg_append(rd, qmsg);
	opsrsm_event_add(rd, OPSRSM_EVT_SEND_MSG);
}

static int
opsrsm_finfo_sendmsg(opsrsm_dest_t *rd, uint8_t type, uint32_t skey)
{
	rsm_send_t send_obj;
	opsrsm_msg_t msg;
	int retval;

	msg.p.hdr.reqtype = type;
	msg.p.hdr.seqno = 0;
	msg.p.hdr.opsrsm_version = OPSRSM_VERSION;
	msg.p.m.finfoquery.skey = skey;
	send_obj.is_data = &msg;
	send_obj.is_size = sizeof (opsrsm_msg_t);
	send_obj.is_flags = RSM_DLPI_SQFLAGS;
	send_obj.is_wait = 0;
	retval = RSM_SEND(rd->rd_adapter->rsmrdt_ctlr_obj, rd->rsm_sendq,
	    &send_obj, NULL);

	return (((retval == RSM_SUCCESS) ? 0 : retval));
}

/*
 * Create a special packet that is used as a marker to the end of
 * a retransmitted stream of packets.
 */
static mblk_t *
opsrsm_alloc_ack_msg(uint32_t skey)
{
	mblk_t *mp;
	opsrsm_ack_msg_t *msg;

	mp = allocb(OPSRSM_CACHELINE_SIZE + OPSRSM_MESSAGE_HDRSZ +
	    sizeof (opsrsm_ack_msg_t) + OPSRSM_CACHELINE_SIZE, BPRI_LO);
	if (mp == NULL) {
		return (NULL);
	}
	mp->b_rptr = (uchar_t *)OPSRSM_CACHELINE_ROUNDUP(mp->b_rptr);

	OPSRSM_MESSAGE_HDRPTR(mp)->lportnum = 0;
	OPSRSM_MESSAGE_HDRPTR(mp)->rportnum = 0;
	OPSRSM_MESSAGE_HDRPTR(mp)->msg_sz = sizeof (opsrsm_ack_msg_t);
	OPSRSM_MESSAGE_HDRPTR(mp)->nodeid = (int)rsmrdt_my_nodeid;
	OPSRSM_MESSAGE_HDRPTR(mp)->pkey = 0;
	OPSRSM_MESSAGE_HDRPTR(mp)->seqno = 0;
	OPSRSM_MESSAGE_HDRPTR(mp)->option = OPSRSM_OPT_REXMIT_END;

	msg = (opsrsm_ack_msg_t *)((caddr_t)mp->b_rptr + OPSRSM_MESSAGE_HDRSZ);
	msg->am_skey = skey;

	mp->b_prev = mp->b_cont = NULL;
	mp->b_wptr = mp->b_rptr + sizeof (opsrsm_ack_msg_t) +
	    OPSRSM_MESSAGE_HDRSZ;

	return (mp);
}

/*
 * This thread handles packet retransmission
 */
static void
opsrsm_failover_thread(void *arg)
{
	opsrsm_failover_info_t *finfo = (opsrsm_failover_info_t *)arg;
	int qlen, isdel, retries, error = 0;
	opsrsm_dest_t *rd;
	clock_t stime, wait_time, curr_time;

	/* wait time interval size is 100ms */
	wait_time = drv_usectohz(100000);

	/* reconnect to the remote node. */
	rd = opsrsm_connect(finfo->fi_nodeid, NULL);
	if (rd == NULL) {
		cmn_err(CE_CONT, "failover: 0x%x failed to reconnect "
		    "to node %d\n", finfo->fi_local_skey, finfo->fi_nodeid);
		error = ENETDOWN;
		goto done;
	}

	retries = 0;
	stime = ddi_get_lbolt();
	for (;;) {
		/* tell remote node that demux has completed */
		error = opsrsm_finfo_sendmsg(rd, OPSRSM_MSG_FINFO_DEMUX_DONE,
		    finfo->fi_remote_skey);

		mutex_enter(&finfo->fi_lock);
		/*
		 * wait for DEMUX_DONE message from remote node.
		 * this is necessary because we must not start
		 * retransmission before the remote node has demuxed
		 * all remaining packets in its delivery queue.
		 */
		if ((finfo->fi_status & OPSRSM_FINFO_DEMUX_DONE) != 0) {
			break;
		}
		curr_time = ddi_get_lbolt();
		(void) cv_timedwait(&finfo->fi_wait_cv, &finfo->fi_lock,
		    curr_time + wait_time);

		/* exit from loop if connection got reset */
		if (rd->rd_state != OPSRSM_STATE_W_READY) {
			error = ENETDOWN;
			break;
		}
		if (++retries > opsrsm_failover_max_retries) {
			error = ETIMEDOUT;
			opsrsm_lostconn(rd);
			break;
		}
		mutex_exit(&finfo->fi_lock);
	}
	mutex_exit(&finfo->fi_lock);
	curr_time = ddi_get_lbolt();
	if (error != 0) {
		DINFO("failover: 0x%x step 1 error %d, wait time %d ms\n",
		    finfo->fi_local_skey, error,
		    drv_hztousec(curr_time - stime)/1000);
		goto done;
	}
	DINFO("failover: 0x%x demux done in %d ms, retries = %d\n",
	    finfo->fi_local_skey, drv_hztousec(curr_time - stime)/1000,
	    retries);
	ASSERT((finfo->fi_status & OPSRSM_FINFO_DEMUX_DONE) != 0);

	/*
	 * retransmission is done by queueing the packets in the rexmitq
	 * to the sendq of the rd and dispatching a thread to drain the
	 * sendq.
	 */
	qlen = OPSRSM_Q_LEN(&finfo->fi_rexmitq);
	DINFO("failover: 0x%x rexmiting remaining %d packets\n",
	    finfo->fi_local_skey, qlen);
	mutex_enter(&rd->rd_sendq_lock);
	/*
	 * need to increment the refcnt before dispatching the xmit
	 * thread to make sure that the rd does not disappear before
	 * the xmit thread runs.
	 */
	REFDEST(rd, isdel);
	if (isdel == 0) {
		mblk_t *mp;
		uint32_t skey;

		if (qlen > 0) {
			OPSRSM_Q_CONCAT(&rd->rd_sendq, &finfo->fi_rexmitq);
		}
		/*
		 * a marker is appended at the end of the retransmitted
		 * stream. an acknowledgement will be sent back when the
		 * receiver gets this marker. send local skey if old proto
		 * is used.
		 */
		if ((finfo->fi_status & OPSRSM_FINFO_OLD_PROTO) != 0) {
			skey = finfo->fi_local_skey;
		} else {
			skey = finfo->fi_remote_skey;
		}
		mp = opsrsm_alloc_ack_msg(skey);
		if (mp != NULL) {
			OPSRSM_Q_APPEND(&rd->rd_sendq, mp);
			opsrsm_dispatch_tmo((void *)rd);
		} else {
			error = ENOMEM;
			UNREFDEST(rd);
			opsrsm_lostconn(rd);
		}
	} else {
		/*
		 * failure to increment the refcnt indicates that the
		 * rd is about to be torn down.
		 */
		error = ENETDOWN;
	}
	mutex_exit(&rd->rd_sendq_lock);
	if (error != 0) {
		DINFO("failover: 0x%x step 2 error %d\n",
		    finfo->fi_local_skey, error);
		goto done;
	}

	retries = 0;
	stime = ddi_get_lbolt();
	mutex_enter(&finfo->fi_lock);
	/*
	 * we need to wait for two conditions:
	 * REXMIT_DONE - this indicates that the remote node
	 *		 has finished retransmitting packets to us.
	 * REXMIT_ACKED - this indicates that we have finished
	 *		  retransmitting packets to the remote node.
	 *
	 * Note that for backward compatibility with an older version
	 * of RDT we also need to check the OLD_PROTO flag. if this
	 * flag is set, we do not wait for the REXMIT_DONE condition.
	 * this has to be done because the older protocol only transmits
	 * a marker if there are packets to rexmit and the REXMIT_DONE
	 * condition cannot be met if the marker never arrives.
	 */
	for (;;) {
		uint_t flag;

		flag = OPSRSM_FINFO_REXMIT_DONE | OPSRSM_FINFO_OLD_PROTO;
		if ((finfo->fi_status & OPSRSM_FINFO_REXMIT_ACKED) != 0 &&
		    (finfo->fi_status & flag) != 0) {
			break;
		}
		curr_time = ddi_get_lbolt();
		(void) cv_timedwait(&finfo->fi_wait_cv, &finfo->fi_lock,
		    curr_time + wait_time);
		if (rd->rd_state != OPSRSM_STATE_W_READY) {
			error = ENETDOWN;
			break;
		}
		if (++retries > opsrsm_failover_max_retries) {
			error = ETIMEDOUT;
			opsrsm_lostconn(rd);
			break;
		}
	}
	mutex_exit(&finfo->fi_lock);
	curr_time = ddi_get_lbolt();
	if (error != 0) {
		DINFO("failover: 0x%x step 3 error %d, wait time %d ms\n",
		    finfo->fi_local_skey, error, (curr_time - stime) * 10);
		goto done;
	}
	ASSERT((finfo->fi_status & OPSRSM_FINFO_REXMIT_ACKED) != 0);
	DINFO("failover: 0x%x retransmission done in %d ms\n",
	    finfo->fi_local_skey, drv_hztousec(curr_time - stime)/1000);

done:;
	if (rd != NULL) {
		if (opsrsmdev->opsrsm_param.rsmrdt_enable_loadbalance) {
			if (rd->rd_adapter->sel_cnt > 0)
				rd->rd_adapter->sel_cnt--;
		}
		UNREFDEST(rd);
	}
	qlen = OPSRSM_Q_LEN(&finfo->fi_rexmitq);
	if (qlen > 0) {
		DINFO("failover: 0x%x reconnect failed, discarding "
		    "%d packets\n", finfo->fi_local_skey, qlen);
	}
	OPSRSM_Q_FLUSH(&finfo->fi_rexmitq);
	/* senders can now be woken up */
	opsrsm_finfo_wakeup(finfo, error);

	/* schedule a timeout to destroy the finfo structure */
	(void) timeout(opsrsm_finfo_destroy, finfo,
	    opsrsm_failover_destruct_time * wait_time);
}


static void
opsrsm_reset_rp(opsrsmresource_t *rp, opsrsm_dest_t *rd)
{
	mutex_enter(&rp->rs_lock);
	if ((rp->rs_state & OPSRSM_RS_CONNECTING) != 0) {
		cv_wait(&rp->rs_conn_cv, &rp->rs_lock);
	}
	if (rp->rs_dest == rd && rp->rs_local_skey == rd->rd_local_skey) {
		rp->rs_state |= OPSRSM_RS_FAILOVER;
		if ((rp->rs_state & OPSRSM_RS_REFDEST) != 0) {
			if (opsrsmdev->opsrsm_param.
			    rsmrdt_enable_loadbalance) {
				if (rd->rd_adapter->sel_cnt > 0)
					rp->rs_dest->rd_adapter->sel_cnt--;
			}
			rp->rs_state &= ~OPSRSM_RS_REFDEST;
			UNREFDEST(rp->rs_dest);
		}
	}
	mutex_exit(&rp->rs_lock);
}

static void
opsrsm_reset_all_rps(opsrsm_dest_t *rd)
{
	int i, j;
	opsrsmresource_blk_t *blk;
	opsrsmresource_t *rp;

	rw_enter(&opsrsm_resource.opsrsmrct_lock, RW_READER);
	for (i = 0; i < opsrsm_resource.opsrsmrc_len; i++) {
		blk = opsrsm_resource.opsrsmrc_root[i];
		if (blk != NULL && blk->opsrsmrcblk_avail < OPSRSMRC_BLKSZ) {
			for (j = 0; j < OPSRSMRC_BLKSZ; j++) {
				rp = blk->opsrsmrcblk_blks[j];
				if (rp != NULL) {
					opsrsm_reset_rp(rp, rd);
				}
			}
		}
	}
	rw_exit(&opsrsm_resource.opsrsmrct_lock);
}

void
rsmrdt_failover(adapter_t *adapterp, rsm_addr_t hwaddr)
{
	opsrsm_dest_t *rd;
	int isdel = 0;

	/*
	 * Scan through all the rds associated with this adapter and
	 * mark them down.
	 */

	DINFO("failover: entering, adapter 0x%p\n", adapterp);

	FINDDEST(rd, isdel, hwaddr, adapterp);
	if (isdel || !rd) {
		goto out;
	}

	DINFO("failover: local_skey 0x%x, rd 0x%p\n", rd->rd_local_skey, rd);
	opsrsm_lostconn(rd);
	UNREFDEST(rd);

out:;
	/* call lower failover function */
	DINFO("failover: exiting, adapter 0x%p\n", adapterp);
}

/*
 * ****************************************************************
 *                                                               *
 * B E G I N   RSM SETUP/TAKEDOWN                                *
 *                                                               *
 * ****************************************************************
 */

int
rsmrdt_check_openhandles() {
	int rval = -1;

	while (rw_tryenter(&opsrsm_resource.opsrsmrct_lock, RW_WRITER) == 0) {
		delay(1);
	}
	if (opsrsm_resource.opsrsmrc_cnt <= 1) {
		/*
		 * Can unload module.
		 */
		opsrsm_resource.opsrsmrc_flag = OPSRSMRC_UNLOAD_INPROGRESS;
		rval = 0;
	}
	rw_exit(&opsrsm_resource.opsrsmrct_lock);
	return (rval);
}

/*
 * Initialize per adapter RSMRDT resources.
 * Return 0 on success, nonzero on error.
 */
int
rsmrdt_adapterinit(adapter_t *adapterp)
{
	char tqname[32];

	/*
	 * Initialize mutexes for this device.
	 */
	mutex_init(&adapterp->opsrsm_dest_lock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&adapterp->opsrsm_runq_lock, NULL, MUTEX_DRIVER, NULL);
	cv_init(&adapterp->opsrsm_uninit_cv, NULL, CV_DRIVER, NULL);

	(void) sprintf(tqname, "opsrsm%d", adapterp->adapterid);
	adapterp->opsrsm_taskq = taskq_create(tqname, 4, maxclsyspri, 1, 4,
		TASKQ_PREPOPULATE);
	return (0);
}


/*
 * Un-initialize per adapter RSMRDT resources.
 * Returns 0 if completely successful.
 * Returns -1 if not in a state where uninitialize makes sense.
 */
int
rsmrdt_adapterfini(adapter_t *adapterp)
{
	/*
	 * If we can't release all destination and RSMPI resources, we can't
	 * detach.  The user will have to try later to unload the driver.
	 */
	D1("rsmrdt_adapterfini: adapterp->adapterid = %d\n",
	    adapterp->adapterid);
	if (opsrsmuninit(adapterp) != 0) {
		return (-1);
	}
	taskq_destroy(adapterp->opsrsm_taskq);
	adapterp->opsrsm_taskq = NULL;

	cv_destroy(&adapterp->opsrsm_uninit_cv);
	mutex_destroy(&adapterp->opsrsm_runq_lock);
	mutex_destroy(&adapterp->opsrsm_dest_lock);

	return (0);
}


/*
 * Un-initialize OPSRSM resources.  Returns 0 if completely successful.
 * Returns -1 if not in a state where uninitialize makes sense.  Returns >0
 * if uninitialize was started, but hasn't completed because not all
 * connections have been torn down yet.
 */
static int
opsrsmuninit(adapter_t *adapterp)
{
	int dests_not_cleaned_up;
	int total_refcnt = 0;
	rsm_addr_t i;

	D1("opsrsmuninit: adapterp 0x%p", (void *)adapterp);

	for (i = 0; i < RSM_MAX_DESTADDR; i++)
		total_refcnt += opsrsmfreedest(adapterp, i);

	mutex_enter(&adapterp->opsrsm_dest_lock);
	dests_not_cleaned_up = adapterp->opsrsm_numdest;
	if (total_refcnt > adapterp->opsrsm_numdest) {
		mutex_exit(&adapterp->opsrsm_dest_lock);
		DERR("opsrsmuninit: total_refcnt = %d", total_refcnt);
		return (dests_not_cleaned_up);
	}

	if (dests_not_cleaned_up > 0) {
		cv_wait(&adapterp->opsrsm_uninit_cv,
			&adapterp->opsrsm_dest_lock);
		dests_not_cleaned_up = adapterp->opsrsm_numdest;
	}
	mutex_exit(&adapterp->opsrsm_dest_lock);

	D1("opsrsmuninit: returning %d", dests_not_cleaned_up);
	return (dests_not_cleaned_up);
}

/*
 * Get all the opsrsm parameters out of the device tree and store them in a
 * OPSRSM device (RSM controller) structure.
 */
static void
opsrsmgetparam(
	dev_info_t *dip,	/* Device's info pointer */
	opsrsm_t *opsrsmp)	/* OPSRSM device (RSM controller) pointer */
{
	struct opsrsm_param *sp = &opsrsmp->opsrsm_param;

	/* Get parameters */

	sp->opsrsm_buffers = (ushort_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-buffers", OPSRSM_BUFFERS_DFLT);
	sp->opsrsm_buffer_size = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-buffer-size", OPSRSM_BUFFER_SIZE_DFLT) +
	    OPSRSM_CACHELINE_SIZE;
	sp->opsrsm_queue_size = (ushort_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-queue-size", OPSRSM_QUEUE_SIZE_DFLT);
	sp->opsrsm_buffers_retained = (ushort_t)ddi_getprop(DDI_DEV_T_ANY,
	    dip, 0, "rsmrdt-buffers-retained", OPSRSM_BUFFERS_RETAINED_DFLT);
	sp->opsrsm_max_queued_pkts = (ushort_t)ddi_getprop(DDI_DEV_T_ANY,
	    dip, 0, "rsmrdt-max-queued-pkts", OPSRSM_MAX_QUEUED_PKTS_DFLT);
	sp->opsrsm_msg_init_tmo = ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-msg-init-tmo", OPSRSM_MSG_INIT_TMO_DFLT);
	sp->opsrsm_msg_max_tmo = ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-msg-max-tmo", OPSRSM_MSG_MAX_TMO_DFLT);
	sp->opsrsm_msg_drop_tmo = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-msg-drop-tmo", OPSRSM_MSG_DROP_TMO_DFLT);
	sp->opsrsm_ack_tmo = ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-ack-tmo", OPSRSM_ACK_TMO_DFLT);
	sp->opsrsm_sync_tmo = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-sync-tmo", OPSRSM_SYNC_TMO_DFLT);
	sp->opsrsm_fqe_sync_size = (ushort_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-fqe-sync-size", OPSRSM_FQE_SYNC_SIZE_DFLT);
	sp->opsrsm_retry_limit = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-retry-limit", OPSRSM_RETRY_LIMIT_DFLT);
	sp->opsrsm_retry_delay = ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-retry-delay", OPSRSM_RETRY_DELAY_DFLT);
	sp->opsrsm_xmit_delay = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-xmit-delay", OPSRSM_XMIT_DELAY_DFLT);
	sp->opsrsm_data_threshold = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-data-threshold", OPSRSM_DATA_THRESHOLD_DFLT);
	sp->opsrsm_max_recv_msgs = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-max-recv-msgs", OPSRSM_MAX_RECV_MSGS_DFLT);
	sp->opsrsm_adaptive_intr = (ushort_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-adaptive-intr", OPSRSM_ADAPTIVE_INTR_DFLT);
	sp->opsrsm_adaptive_rate = (ushort_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-adaptive-rate", OPSRSM_ADAPTIVE_RATE_DFLT);
	sp->opsrsm_mem_hi_wat = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-mem-hi-wat", OPSRSM_MEM_HI_WAT_DFLT);
	sp->opsrsm_mem_lo_wat = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-mem-lo-wat", OPSRSM_MEM_LO_WAT_DFLT);
	sp->opsrsm_recv_hi_wat = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-recv-hi-wat", OPSRSM_RECV_HI_WAT_DFLT);
	sp->opsrsm_recv_lo_wat = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-recv-lo-wat", OPSRSM_RECV_LO_WAT_DFLT);
	sp->opsrsm_flow_tmo_int = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip, 0,
	    "rsmrdt-flow-tmo-int", OPSRSM_FLOW_TMO_INT_DFLT);
	sp->opsrsm_max_loopback_pkts = (uint_t)ddi_getprop(DDI_DEV_T_ANY, dip,
	    0, "rsmrdt-max-loopback-pkts", OPSRSM_MAX_LOOPBACK_PKTS_DFLT);
	sp->rsmrdt_enable_loadbalance = (ushort_t)ddi_getprop(DDI_DEV_T_ANY,
	    dip, 0, "rsmrdt-enable-loadbalance",
	    RSMRDT_ENABLE_LOADBALANCE_DFLT);

	/*
	 * Sanity check parameters, modify if needed.  Note that we mainly
	 * check to make sure parameters won't make the driver malfunction;
	 * we don't necessarily prevent them from being stupid.
	 */

	/* Need to have at least one buffer. */

	if (sp->opsrsm_buffers == 0)
		sp->opsrsm_buffers = 1;

	/* Buffer length must be multiple of 64 (0x40). */

	if (sp->opsrsm_buffer_size & ~OPSRSM_CACHELINE_MASK) {
		sp->opsrsm_buffer_size &= OPSRSM_CACHELINE_MASK;
	}

	/*
	 * Must have at least one more queue element then the number of
	 * buffers.  This is so that we can track when all queue elements
	 * need to be flushed to remote.
	 */

	if (sp->opsrsm_queue_size <= sp->opsrsm_buffers) {
		sp->opsrsm_queue_size = sp->opsrsm_buffers + 1;
	}

	/* Can't retain more buffers than we have. */

	if (sp->opsrsm_buffers_retained > sp->opsrsm_buffers) {
		sp->opsrsm_buffers_retained = sp->opsrsm_buffers;
	}

	/* Have to be able to queue at least 1 packet. */

	if (sp->opsrsm_max_queued_pkts < 1) {
		sp->opsrsm_max_queued_pkts = 1;
	}

	if (sp->opsrsm_msg_init_tmo < 1) {
		sp->opsrsm_msg_init_tmo = 1;
	}
	if (sp->opsrsm_msg_max_tmo < 1) {
		sp->opsrsm_msg_max_tmo = 1;
	}
	if (sp->opsrsm_ack_tmo < 1) {
		sp->opsrsm_ack_tmo = 1;
	}
	if (sp->opsrsm_sync_tmo < 1) {
		sp->opsrsm_sync_tmo = 1;
	}
	if (sp->opsrsm_retry_limit < 1) {
		sp->opsrsm_retry_limit = 1;
	}
	if (sp->opsrsm_retry_delay < 1) {
		sp->opsrsm_retry_delay = 1;
	}
	if (sp->opsrsm_xmit_delay < 1) {
		sp->opsrsm_xmit_delay = 1;
	}

	if (sp->opsrsm_max_recv_msgs < 1) {
		sp->opsrsm_max_recv_msgs = OPSRSM_MAX_RECV_MSGS_DFLT;
	}

}

/*
 * ****************************************************************
 *                                                               *
 * E N D       RSM SETUP/TAKEDOWN                                *
 *                                                               *
 * ****************************************************************
 */


/*
 * ****************************************************************
 *                                                               *
 * B E G I N   CONNECTION DATA STRUCTURE MANAGEMENT              *
 *                                                               *
 * ****************************************************************
 */


/*
 * Create the indicated destination structure, and return a pointer to it.
 * NOTE:  this should never be called directly; use the MAKEDEST macro
 * instead.  The macro checks that the destination structure does not yet
 * exist before calling this function.
 */


static opsrsm_dest_t *
opsrsmmkdest(adapter_t *adapterp,
	rsm_addr_t rsm_addr)	/* Address of destination to find/create */
{
	opsrsm_dest_t *rd;
	clock_t lbolttime;
	int adapterid, nodeid;

	D1("opsrsmmkdest:cltr %d, rsmaddr %ld", adapterp->instance, rsm_addr);

	/* Is the destination reasonable? */

	if (rsm_addr >= RSM_MAX_DESTADDR) {
		DERR("opsrsmmkdest: too big, returning NULL");
		return (NULL);
	}

	if ((rd = adapterp->opsrsm_desttbl[rsm_addr]) != NULL) {
		return (rd);
	}

	ASSERT(MUTEX_HELD(&adapterp->opsrsm_dest_lock));

	/* retrieve the remote adapter id and remote node id */
	rsmrdt_get_remote_ids(adapterp, rsm_addr, &adapterid, &nodeid);
	if (adapterid == -1 || nodeid == -1) {
		DERR("opsrsmmkdest: Unable to find remote ids\n");
		return (NULL);
	}
	D1("opsrsmmkdest: Remote adapter id %d\n", adapterid);

	if ((rd = (opsrsm_dest_t *)kmem_zalloc(sizeof (*rd), KM_NOSLEEP)) ==
	    NULL) {
		DERR("opsrsmmkdest: can't alloc, returning NULL");
		return (NULL);
	}

	rd->rd_evt_taskq = taskq_create("rd_events", 1, maxclsyspri, 1, 1,
	    TASKQ_PREPOPULATE);

	if (rd->rd_evt_taskq == NULL) {
		kmem_free(rd, sizeof (*rd));
		return (NULL);
	}
	mutex_init(&rd->rd_msgs_lock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&rd->rd_evt_lock, NULL, MUTEX_DRIVER, NULL);
	cv_init(&rd->rd_evt_cv, NULL, CV_DRIVER, NULL);
	cv_init(&rd->rd_evt_wait_cv, NULL, CV_DRIVER, NULL);
	rd->rd_evt_flags = 0;
	rd->rd_msgs = NULL;
	rd->rd_msgs_tail = NULL;

	if (taskq_dispatch(rd->rd_evt_taskq, opsrsm_event_thread,
	    rd, KM_NOSLEEP) == 0) {
		mutex_destroy(&rd->rd_msgs_lock);
		mutex_destroy(&rd->rd_evt_lock);
		cv_destroy(&rd->rd_evt_cv);
		cv_destroy(&rd->rd_evt_wait_cv);
		kmem_free(rd, sizeof (*rd));
		return (NULL);
	}

	rd->rd_buffer_size = opsrsmdev->opsrsm_param.opsrsm_buffer_size;
	rd->rd_rsm_addr = rsm_addr;
	rd->rd_rem_adapterid = adapterid;
	rd->rd_adapter = adapterp;

	mutex_init(&rd->rd_net_lock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&rd->rd_fqr_lock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&rd->rd_xmit_lock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&rd->rd_lock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&rd->rd_sendq_lock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&rd->rd_tmo_lock, NULL, MUTEX_DRIVER, NULL);
	mutex_init(&rd->rd_freeq_lock, NULL, MUTEX_DRIVER, NULL);
	cv_init(&rd->rd_conn_cv, NULL, CV_DRIVER, NULL);

	/*
	 * Use the time to generate a pseudo-random initial sequence
	 * number.
	 */
	(void) drv_getparm(LBOLT, &lbolttime);
	rd->rd_nseq = (ushort_t)lbolttime;

	rd->rd_state = OPSRSM_STATE_NEW;
	rd->rd_xmit_state = OPSRSM_XMIT_UNINITIALIZED;
	rd->rd_nodeid = nodeid;
	rd->rd_start_time = 0;
	rd->rd_data_collected = 0;
	rd->rd_writes_completed = 0;
	rd->rd_active_threads = 0;
	rd->rd_xmit_tmo_id = 0;
	rd->rd_fqe_tmo_id = 0;
	rd->rd_sync_dqe_tmo_id = 0;
	rd->rd_sync_fqe_tmo_id = 0;
	rd->rd_xmit_tmo_int = 0;
	rd->rd_fqe_tmo_int = 0;
	rd->rd_refcnt = 1;
	rd->rd_pollhd.ph_list = NULL;
	rd->rd_events = 0;
	rd->rd_adaptive_threshold = 0;
	rd->rd_last_sent = (uint32_t)lbolttime;
	rd->rd_pkt_freq = 0;
	rd->rd_freeq_freeze = B_FALSE;
	rd->rd_next_rseqno = 1;
	rd->rd_next_lseqno = 1;
	rd->rd_local_skey = (uint32_t)gethrtime();
	rd->rd_local_skey ^= (uint32_t)rd + (uint32_t)rsm_addr;
	rd->rd_remote_skey = 0;
	rd->rd_retry_int = B_FALSE;
	rd->rd_freeq_freeze = B_FALSE;
	rd->rd_freed = B_FALSE;
	rd->rd_unpublish_errs = 0;
	OPSRSM_Q_INIT(&rd->rd_sendq);
	OPSRSM_Q_INIT(&rd->rd_pendq);
	OPSRSM_Q_INIT(&rd->rd_freeq);
	rd->rd_remote_flow_stop = 0;
	rd->rd_remote_flow_ctl = 0;
	rd->rd_flow_ctl = NULL;
	rd->rd_flow_tmo_id = 0;
	rd->rd_pkts_delivered = 0;
	rd->rd_status_tmo_id = 0;
	rd->rd_queued_fqe_freelist = NULL;
	rd->rd_queued_fqe_list = NULL;
	rd->rd_queued_fqe_tail = NULL;
	rd->rd_queued_fqe_array = NULL;
	rd->rd_queued_fqe_cnt = 0;
	rd->rd_fqr_flags = 0;

	adapterp->opsrsm_desttbl[rsm_addr] = rd;
	adapterp->opsrsm_numdest++;

	D1("opsrsmmkdest: created new dest, returning 0x%p", (void *)rd);
	return (rd);
}

/*
 * Destination deletion
 *
 * As mentioned above (way above), we maintain a reference count on all
 * destinations, which is incremented and decremented around uses of the
 * destination structure.  When this reference count goes to zero, we delete
 * the destination.
 *
 * Because of the possibility of other threads trying to use the destination
 * while we're deleting it, deletion is actually a multiple-step process,
 * which works as follows.
 *
 * 1. When a destination is created, its dstate (deletion state) is set to
 *    zero, and its reference count is set to one.
 *
 * 2. When the service routine or some other routine decides that a destination
 *    should be deleted, it calls opsrsmfreedest().  That routine sets dstate
 *    to 1 and cancels any pending sync timeouts.  It then decrements the
 *    destination's reference count.  This deletes the reference set in
 *    opsrsmmkdest. (Note that since dstate is now 1, the FINDDEST and REFDEST
 *    macros will now note that the destination is being deleted; thus, any
 *    interrupt referring to the destination will no longer modify the
 *    reference count.)
 *
 * 3. Soon after this, opsrsmdest_refcnt_0 is called.  (This may either be
 *    directly from opsrsmfreedest(), or perhaps from another routine if it
 *    was running concurrently with freedest() and its UNREF happened last).
 *    This routine sees that dstate is 1, and immediately queues a timeout
 *    which will execute opsrsmfreedesttmo().  (This is necessary because we
 *    may not be able to do everything in the phase 1 deletion from the routine
 *    that we're currently in.)
 *
 * 4. opsrsmfreedesttmo() runs, it checks if there are any outstanding
 *    loaned-up buffers.  If so, it sets a flag to cause the loan returning
 *    code to decrement the refcnt, and returns without performing cleanup.
 *    When all loaned buffers are returned and the refcnt is decremented, we
 *    go back to step 3, above.  When opsrsmfreedesttmo() finally runs with
 *    no loaned buffers, gets rid of most of the OPSRSM resources attached
 *    to the destination.  It also throws away any queued packets, gets
 *    rid of any allocated DVMA resources.  It changes dstate to 2, takes
 *    this destination structure out of the base-ID => destination table.
 *    It then decrements the reference count that had been added by
 *    opsrsmdest_refcnt_0().
 *
 * 5. When the reference count becomes 0, opsrsmdest_refcnt_0 is again called.
 *    It notices that dstate is 2, and frees the destination structure.
 */

/*
 * A destination's reference count went to 0, deal with it.
 */
static boolean_t
opsrsmdest_refcnt_0(
	opsrsm_dest_t *rd)	/* Destination pointer */
{
	opsrsm_t *opsrsmp = opsrsmdev;
	adapter_t *adapterp = rd->rd_adapter;
	boolean_t freed = B_FALSE;

	mutex_enter(&adapterp->opsrsm_dest_lock);

	D1("opsrsmdest_refcnt_0: rd 0x%p (addr %ld ctlr %d), refcnt %d, "
	    "dstate %d",
	    (void *)rd, rd->rd_rsm_addr, adapterp->instance,
	    rd->rd_refcnt, rd->rd_dstate);

	if (rd->rd_dstate == 1) {
		rd->rd_refcnt++;	/* Inline REFDEST */

		DINFO("failover: 0x%x start destruction\n", rd->rd_local_skey);
		/*
		 * We may be called from a routine that can't actually do the
		 * work that needs to be done, so we schedule a thread to do
		 * the next phase of the deletion.
		 */
		(void) taskq_dispatch(opsrsm_events_taskq, opsrsmfreedesttmo,
		    rd, KM_SLEEP);

	} else if (rd->rd_dstate == 2) {

		/* Destroy all the mutexes */
		DINFO("failover: 0x%x end destruction\n", rd->rd_local_skey);
		opsrsm_queued_msg_flush(rd);

		mutex_destroy(&rd->rd_lock);
		mutex_destroy(&rd->rd_net_lock);
		mutex_destroy(&rd->rd_fqr_lock);
		mutex_destroy(&rd->rd_xmit_lock);
		mutex_destroy(&rd->rd_nlb_lock);
		mutex_destroy(&rd->rd_sendq_lock);
		mutex_destroy(&rd->rd_tmo_lock);
		mutex_destroy(&rd->rd_freeq_lock);
		mutex_destroy(&rd->rd_msgs_lock);
		mutex_destroy(&rd->rd_evt_lock);

		cv_destroy(&rd->rd_conn_cv);
		cv_destroy(&rd->rd_evt_cv);
		cv_destroy(&rd->rd_evt_wait_cv);

		/*
		 * Free any allocated memory hanging off the dest structure.
		 */
		if (rd->rd_queued_fqe_array) {
			rd->rd_queued_fqe_freelist = NULL;
			rd->rd_queued_fqe_list = NULL;
			rd->rd_queued_fqe_tail = NULL;
			kmem_free(rd->rd_queued_fqe_array,
			    opsrsmp->opsrsm_param.opsrsm_queue_size *
			    sizeof (opsrsm_queued_fqe_t));
			rd->rd_queued_fqe_array = NULL;
			rd->rd_queued_fqe_cnt = 0;
		}

		if (rd->rd_cached_fqr) {
			kmem_free(rd->rd_cached_fqr,
			    sizeof (*rd->rd_cached_fqr) * rd->rd_num_fqrs);
		}
		if (rd->rd_shdwfqw_f) {
			kmem_free(rd->rd_shdwfqw_f,
			    sizeof (*rd->rd_shdwfqw_f) * rd->rd_num_fqws);
		}
		if (rd->rd_shdwdqw_f) {
			kmem_free(rd->rd_shdwdqw_f,
			    sizeof (*rd->rd_shdwdqw_f) * rd->rd_num_dqws);
		}
		if (rd->rd_bufbase) {
			kmem_free(rd->rd_bufbase,
			    opsrsmp->opsrsm_param.opsrsm_buffers *
			    sizeof (*rd->rd_bufbase));
		}
		if (rd->rd_rawmem_base_addr) {
			kmem_free(rd->rd_rawmem_base_addr,
			    rd->rd_rawmem_base_size);
		}

		/* Finally free the dest structure */

		kmem_free(rd, sizeof (*rd));
		freed = B_TRUE;

		adapterp->opsrsm_numdest--;
		D1("opsrsmdest_refcnt_0: freed rd data structures");
	}

	if (freed && adapterp->opsrsm_numdest <= 0) {
		cv_signal(&adapterp->opsrsm_uninit_cv);
	}
	mutex_exit(&adapterp->opsrsm_dest_lock);

	D1("opsrsmdest_refcnt_0: done");
	return (freed);
}

/*
 * Do deletion work.
 */
static void
opsrsmfreedesttmo(void * arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;
	int bufnum, offset, length;
	opsrsm_failover_info_t *finfo;
	ushort_t sap;
	int err, cnt;
	boolean_t read_pkts = B_FALSE;

	/*
	 * See if there are any more outstanding loaned buffers.  If so,
	 * set flag so that freebuf will eventually do an UNREF when it
	 * frees the last buffer.  This removes the reference added in
	 * opsrsmdest_refcnt_0(), causing the count to again go to 0.
	 * opsrsmdest_refcnt_0() will again be called, increment the refcnt
	 * and cause this routine to be called to complete cleanup.
	 */

	mutex_enter(&rd->rd_nlb_lock);

	rd->rd_nlb_del = 1;
	if (rd->rd_nlb != 0) {
		DERR("opsrsmfreedesttmo: loaned buffers outstanding %d, dest "
		    "%ld", rd->rd_nlb, rd->rd_rsm_addr);
		mutex_exit(&rd->rd_nlb_lock);
		return;
	}

	mutex_exit(&rd->rd_nlb_lock);

	/*
	 * Perform the sendq destroy first -- this notifies the
	 * remote side that the connection is going away, so
	 * it can immediately start cleaning up.  This helps
	 * to avoid a situation where a segment is unpublished
	 * while there is still a connection to it (which is legal,
	 * but can cause overhead in some specific RSM drivers).
	 */
	if (rd->rd_sstate & OPSRSM_RSMS_RXFER_S) {
		ASSERT(rd->rsm_sendq);
		D4("opsrsmfreedesttmo: destroying sendq\n");
		err = RSM_SENDQ_DESTROY(rd->rd_adapter->rsmrdt_ctlr_obj,
			rd->rsm_sendq);
		if (err) {
			DERR("RSM_SENDQ_DESTROY failed! err %d\n", err);
		}
		rd->rd_sstate &= ~OPSRSM_RSMS_RXFER_S;
	}

	if (rd->rd_sstate & OPSRSM_RSMS_RXFER_C) {
		ASSERT(rd->rd_rxferhand);
		D1("opsrsmfreedesttmo: disconnecting from remote segment\n");
		err = RSM_DISCONNECT(rd->rd_adapter->rsmrdt_ctlr_obj,
			rd->rd_rxferhand);
		if (err) {
			DERR("RSM_DISCONNECT failed! err %d\n", err);
		}
		rd->rd_sstate &= ~OPSRSM_RSMS_RXFER_C;
	}

	if (rd->rd_sstate & OPSRSM_RSMS_LXFER_P) {
		ASSERT(rd->rd_lxferhand);
		D1("opsrsmfreedesttmo: unpublishing local segment\n");
retry:;
		err = RSM_UNPUBLISH(rd->rd_adapter->rsmrdt_ctlr_obj,
		    rd->rd_lxferhand);

		if (err && rd->rd_unpublish_errs < RSMRDT_UNPUBLISH_TRY) {
			D1("RSM_UNPUBLISH failed! err %d\n", err);
			opsrsmdev->opsrsm_ierrors++;
			rd->rd_unpublish_errs++;
			/*
			 * if RSM_UNPUBLISH fails, we need a slight delay
			 * before retrying.
			 */
			delay(1);
			goto retry;
		}
		rd->rd_unpublish_errs = 0;
		rd->rd_sstate &= ~OPSRSM_RSMS_LXFER_P;
	}

	if (rd->rd_sstate & OPSRSM_RSMS_LXFER_C) {
		ASSERT(rd->rd_lxferhand);
		D1("opsrsmfreedesttmo: destroying local segment\n");
		err = RSM_SEG_DESTROY(rd->rd_adapter->rsmrdt_ctlr_obj,
			rd->rd_lxferhand);
		if (err) {
			DERR("RSM_SEG_DESTROY failed! err %d\n", err);
		}
		rd->rd_sstate &= ~OPSRSM_RSMS_LXFER_C;
		read_pkts = B_TRUE;
	}

	/* empty out packets remaining in the buffer pool */

	if (read_pkts) {
		cnt = 0;
		/* Loop through all valid DQE's and process their packets. */
		while (opsrsmgetdqe(rd, &bufnum, &offset, &length, &sap)) {
			/* Don't try to send up DQE with zero length */
			cnt++;
			if (length)
				(void) opsrsmread(rd, bufnum, offset, length,
				    sap);
			else {
				cmn_err(CE_PANIC, "received corrupted "
				    "packet\n");
			}
		}
		if (cnt > 0) {
			DINFO("failover: 0x%x read %d remaining packets\n",
			    rd->rd_local_skey, cnt);
		}
	}
	opsrsm_wake_senders(rd, POLLOUT);

	/* Take out of desttbl */
	mutex_enter(&rd->rd_adapter->opsrsm_dest_lock);
	rd->rd_adapter->opsrsm_desttbl[rd->rd_rsm_addr] = NULL;
	ASSERT(rd->rd_dstate == 1);
	rd->rd_dstate = 2;
	mutex_exit(&rd->rd_adapter->opsrsm_dest_lock);

	finfo = opsrsm_finfo_lookup_by_local_skey(rd->rd_local_skey);
	ASSERT(finfo != NULL);

	OPSRSM_Q_CONCAT(&finfo->fi_rexmitq, &rd->rd_freeq);
	OPSRSM_Q_CONCAT(&finfo->fi_rexmitq, &rd->rd_pendq);
	OPSRSM_Q_CONCAT(&finfo->fi_rexmitq, &rd->rd_sendq);
	finfo->fi_next_rseqno = rd->rd_next_rseqno;

	/*
	 * no need to rexmit if there are no open fds or if driver
	 * is being unloaded
	 */
	if (opsrsm_resource.opsrsmrc_flag == OPSRSMRC_UNLOAD_INPROGRESS) {
		DINFO("failover: rexmit thread not dispatched\n");
		OPSRSM_Q_FLUSH(&finfo->fi_rexmitq);
		opsrsm_finfo_wakeup(finfo, ENETDOWN);
		if (opsrsm_resource.opsrsmrc_flag !=
		    OPSRSMRC_UNLOAD_INPROGRESS) {
			mutex_enter(&opsrsm_finfo_lock);
			opsrsm_failover_threads++;
			mutex_exit(&opsrsm_finfo_lock);
			(void) timeout(opsrsm_finfo_destroy, finfo, 10);
		}
	} else {
		DINFO("failover: rd 0x%p adapter 0x%p (cltr %d) addr 0x%llx\n",
		    rd, rd->rd_adapter, rd->rd_adapter->instance,
		    rd->rd_rsm_addr);
		DINFO("failover: local_skey 0x%x, remote_skey 0x%x, "
		    "finfo 0x%p\n", rd->rd_local_skey, rd->rd_remote_skey,
		    finfo);
		mutex_enter(&opsrsm_finfo_lock);
		opsrsm_failover_threads++;
		mutex_exit(&opsrsm_finfo_lock);
		if (OPSRSM_Q_LEN(&finfo->fi_rexmitq) == 0 &&
		    finfo->fi_remote_skey == 0) {
			/*
			 * this occurs when segment establishment
			 * failed. there is no need to keep the finfo
			 * since no data transfer ever occurred.
			 */
			opsrsm_finfo_wakeup(finfo, ENETDOWN);
			(void) timeout(opsrsm_finfo_destroy, finfo, 10);
		} else {
			(void) timeout(opsrsm_dispatch_failover, finfo, 100);
		}
	}
	/* Make sure dest isn't on service queue */
	mutex_enter(&rd->rd_adapter->opsrsm_runq_lock);

	if (rd->rd_adapter->opsrsm_runq == rd)
		rd->rd_adapter->opsrsm_runq = rd->rd_next;
	else {
		opsrsm_dest_t *lastrd = rd->rd_adapter->opsrsm_runq;

		while (lastrd) {
			if (lastrd->rd_next == rd) {
				lastrd->rd_next = rd->rd_next;
				break;
			}
			lastrd = lastrd->rd_next;
		}
	}

	mutex_exit(&rd->rd_adapter->opsrsm_runq_lock);

	ASSERT(rd->rd_sstate == 0);

	/*
	 * Removes the reference added in opsrsmdest_refcnt_0().
	 */
	UNREFDEST(rd);

	D1("opsrsmfreedesttmo: done");
}


/*
 * Start the deletion process for a destination.
 */
static int
opsrsmfreedest(adapter_t *adapter, rsm_addr_t rsm_addr)
{
	opsrsm_dest_t *rd;
	timeout_id_t tmoid;
	int refcnt = 0;
	opsrsm_t *opsrsmp = opsrsmdev;

	D2("opsrsmfreedest: remote rsmaddr %ld", rsm_addr);
	mutex_enter(&adapter->opsrsm_dest_lock);
	rd = adapter->opsrsm_desttbl[rsm_addr];
	if (rd == NULL || rd->rd_dstate != 0) {
#ifdef DEBUG
		if (rd != NULL) {
			cmn_err(CE_CONT, "opsrsmfreedest: dstate = %d, "
			    "exiting\n", rd->rd_dstate);
		}
#endif /* DEBUG */
		mutex_exit(&adapter->opsrsm_dest_lock);
		return (refcnt);
	}
	if (rd->rd_freed) {
#ifdef DEBUG
		cmn_err(CE_CONT, "opsrsmfreedest: already freed\n");
#endif /* DEBUG */
		mutex_exit(&adapter->opsrsm_dest_lock);
		return (refcnt);
	}
	rd->rd_freed = B_TRUE;
	(void) opsrsm_finfo_add(rd);
	mutex_exit(&adapter->opsrsm_dest_lock);
	mutex_enter(&rd->rd_xmit_lock);

	mutex_enter(&rd->rd_freeq_lock);
	rd->rd_freeq_freeze = B_TRUE;
	mutex_exit(&rd->rd_freeq_lock);

	rd->rd_xmit_state = OPSRSM_XMIT_DISCONNECTED;
	cv_broadcast(&rd->rd_conn_cv);
	mutex_exit(&rd->rd_xmit_lock);

	opsrsm_reset_all_rps(rd);

	mutex_enter(&adapter->opsrsm_dest_lock);
	D1("opsrsmfreedest: opsrsmp 0x%p (cltr %d) rsmaddr %ld",
	    (void *)opsrsmp, rd->rd_adapter->instance, rsm_addr);
	rd->rd_dstate = 1;
	refcnt = rd->rd_refcnt;
	mutex_exit(&adapter->opsrsm_dest_lock);

	mutex_enter(&rd->rd_evt_lock);
	rd->rd_evt_flags |= OPSRSM_EVT_STOP;
	cv_signal(&rd->rd_evt_cv);
	cv_wait(&rd->rd_evt_wait_cv, &rd->rd_evt_lock);
	ASSERT((rd->rd_evt_flags & OPSRSM_EVT_DONE) != 0);
	mutex_exit(&rd->rd_evt_lock);

	opsrsm_queued_msg_flush(rd);
	taskq_destroy(rd->rd_evt_taskq);
	rd->rd_evt_taskq = NULL;

	/*
	 * Turn off any timeouts.  The sync timeout reschedules itself, so we
	 * have to go to great lengths to kill it.
	 */
	mutex_enter(&rd->rd_xmit_lock);
	tmoid = rd->rd_tmo_id;
	rd->rd_tmo_id = 0;
	rd->rd_stopq = B_TRUE;
	mutex_exit(&rd->rd_xmit_lock);

	if (tmoid)
		(void) untimeout(tmoid);

	tmoid = rd->rd_sync_dqe_tmo_id;
	while (tmoid) {
		(void) untimeout(tmoid);
		/*
		 * untimeout guarantees the either the function was
		 * cancelled, or it has completed.  If timeout was
		 * cancelled before the function ran, the timout id will
		 * not have changed.
		 */
		if (tmoid == rd->rd_sync_dqe_tmo_id)
			rd->rd_sync_dqe_tmo_id = 0;
		tmoid = rd->rd_sync_dqe_tmo_id;
	}

	tmoid = rd->rd_sync_fqe_tmo_id;
	while (tmoid) {
		(void) untimeout(tmoid);
		if (tmoid == rd->rd_sync_fqe_tmo_id)
			rd->rd_sync_fqe_tmo_id = 0;
		tmoid = rd->rd_sync_fqe_tmo_id;
	}

	opsrsm_cancel_xmit_tmo(rd);
	opsrsm_cancel_fqe_tmo(rd);
	opsrsm_cancel_sync_flow_tmo(rd);
	opsrsm_wake_senders(rd, POLLOUT);
	if (rd->rd_pollhd.ph_list != NULL)
		pollhead_clean(&rd->rd_pollhd);

	D1("opsrsmfreedest: done");

	/* remove reference added in opsrsmmkdest() */
	UNREFDEST(rd);
	return (refcnt);
}

/*
 * ****************************************************************
 *                                                               *
 * E N D       CONNECTION DATA STRUCTURE MANAGEMENT              *
 *                                                               *
 * ****************************************************************
 */




/*
 * ****************************************************************
 *                                                               *
 * B E G I N   MAIN STATE MACHINE                                *
 *                                                               *
 * ****************************************************************
 */


/*
 * We change a destination's state in a number of routines; we define these
 * macros to make sure it gets done the same way every time.
 */
#define	OPSRSM_SETSTATE(rd, adapter, routine, newstate)			\
	rd->rd_state = (ushort_t)newstate;				\
		if (OPSRSM_SCHED_STATE(newstate)) {			\
			rd->rd_next = adapter->opsrsm_runq;		\
			adapter->opsrsm_runq = rd;			\
			D1(routine ": added to runq");	        	\
			if (adapter->opsrsm_taskq) {			\
				(void) taskq_dispatch(adapter->opsrsm_taskq,\
				opsrsmwsrv, adapter, KM_NOSLEEP);	\
				D1(routine ": enabled 0x%p",		\
				    (void *)adapter->opsrsm_taskq);	\
			}				        	\
		}							\


/*
 * This routine processes a notification that a destination has become
 * unreachable.  Delete our record of it, so that when it comes back up we
 * will re-establish our association.  We do this by changing its state to
 * S_DELETE; the service routine will then start the deletion
 * process.
 *
 * Since other parts of the driver may have operations in progress that
 * involve this destination, most of the time we cannot just whack the
 * state to the new value.  Instead, we record (in rd_estate) that the
 * connection was lost.  The next time someone else attempts to change the
 * state, the state change routines recognize that there is a pending event
 * and change the state to the one we wanted instead.  (There are
 * exceptions in cases where the new state indicates that we've enabled
 * some sort of timeout; in this case, we may wait until the following
 * state change to take note of the event.)
 */
static void
opsrsm_lostconn(opsrsm_dest_t *rd)
{
	adapter_t *adapter = rd->rd_adapter;

	D1("opsrsm_lostconn: rd 0x%p (addr %ld ctlr %d)", (void *)rd,
	    rd->rd_rsm_addr, adapter->instance);

	mutex_enter(&adapter->opsrsm_runq_lock);
	if ((rd->rd_state == OPSRSM_STATE_W_READY) ||
	    (rd->rd_state == OPSRSM_STATE_NEW) ||
	    (rd->rd_state == OPSRSM_STATE_W_ACCEPT) ||
	    (rd->rd_state == OPSRSM_STATE_W_ACK) ||
	    (rd->rd_state == OPSRSM_STATE_W_FQE)) {
		/* LINTED: E_CONSTANT_CONDITION */
		OPSRSM_SETSTATE(rd, adapter, "opsrsm_lostconn",
		    OPSRSM_STATE_S_DELETE);
	} else {
		rd->rd_estate = OPSRSM_STATE_S_DELETE;
	}
	DERR("opsrsm_lostconn: state now %s, estate now %s",
	    OPSRSM_STATE_STR(rd->rd_state), OPSRSM_STATE_STR(rd->rd_estate));

	mutex_exit(&adapter->opsrsm_runq_lock);

	/*
	 * Stop trying to flush queue entries to the other side.
	 */
	rd->rd_stopq = B_TRUE;
	D1("opsrsm_lostconn: done");
}


/*
 * Figure out what state transition should actually occur after an event
 * has happened.
 */
static int
opsrsmestate_newstate(opsrsm_dest_t *rd, int newstate)
{
	int retval = newstate;

	/*
	 * If we're going to a state where we've just set a timeout, don't
	 * mess with the state.  When the timeout happens, it will change
	 * state again, and we'll nab 'em there.  If we're about to delete
	 * rd, don't bother worrying about the event.
	 */
	switch (newstate) {
	case OPSRSM_STATE_W_SCONNTMO:
	case OPSRSM_STATE_W_ACCEPT:
	case OPSRSM_STATE_W_ACK:
	case OPSRSM_STATE_W_FQE:
	case OPSRSM_STATE_DELETING:
	case OPSRSM_STATE_S_DELETE:
		return (retval);
	default:
		break;
	}

	if (rd->rd_estate) {
		retval = rd->rd_estate;
		rd->rd_estate = OPSRSM_STATE_NEW; /* clear event state */
	}

	D1("opsrsmestate_newstate: %d %d -> %d", rd->rd_estate,
	    newstate, retval);

	return (retval);
}


/*
 * Return destination's state, then set its state to INPROGRESS.
 */
static int
opsrsmgetstate(
	opsrsm_dest_t *rd)	/* Destination pointer */
{
	int state;

	D1("opsrsmgetstate: rd 0x%p", (void *)rd);

	mutex_enter(&rd->rd_adapter->opsrsm_runq_lock);

	state = rd->rd_state;
	rd->rd_state = OPSRSM_STATE_INPROGRESS;

	mutex_exit(&rd->rd_adapter->opsrsm_runq_lock);

	D1("opsrsmgetstate: returning %s", OPSRSM_STATE_STR(state));

	return (state);
}

/*
 * Set destination's state; must be preceded by a getstate call.  (i.e.,
 * destination's current state must be INPROGRESS.)
 */
static void
opsrsmsetstate(
	opsrsm_dest_t *rd,	/* Destination pointer */
	int newstate)	/* State to set */
{
	adapter_t *adapter = rd->rd_adapter;

	D1("opsrsmsetstate: rd 0x%p, newstate %s", (void *)rd,
	    OPSRSM_STATE_STR(newstate));

	mutex_enter(&adapter->opsrsm_runq_lock);

	if (rd->rd_state == OPSRSM_STATE_INPROGRESS) {
		if (rd->rd_estate)
			newstate = opsrsmestate_newstate(rd, newstate);
		OPSRSM_SETSTATE(rd, adapter, "opsrsmsetstate", newstate);
	} else {
		D1("opsrsm: setstate without getstate");
		cmn_err(CE_PANIC, "opsrsm: setstate without getstate");
	}

	mutex_exit(&adapter->opsrsm_runq_lock);

	D1("opsrsmsetstate: done");
}


/*
 * Set state to newstate iff state is oldstate.  Return 1 if move happened,
 * else 0.
 */
static int
opsrsmmovestate(
	opsrsm_dest_t *rd,	/* Destination pointer */
	int oldstate,	/* State to check against */
	int newstate)	/* State to set if check succeeds */
{
	adapter_t *adapter = rd->rd_adapter;
	int retval;

	D1("opsrsmmovestate: rd 0x%p, oldstate %s, newstate %s",
	    (void *)rd, OPSRSM_STATE_STR(oldstate), OPSRSM_STATE_STR(newstate));

	mutex_enter(&adapter->opsrsm_runq_lock);

	if (rd->rd_state == oldstate) {
		if (rd->rd_estate)
			newstate = opsrsmestate_newstate(rd, newstate);
		OPSRSM_SETSTATE(rd, adapter, "opsrsmmovestate", newstate);
		retval = 1;
		D1("opsrsmmovestate: state changed, returning 1");
	} else {
		retval = 0;
		D1("opsrsmmovestate: oldstate really %s, returning 0",
		    OPSRSM_STATE_STR(rd->rd_state));
	}

	mutex_exit(&adapter->opsrsm_runq_lock);

	return (retval);
}



/*
 * ****************************************************************
 *                                                               *
 * E N D       MAIN STATE MACHINE                                *
 *                                                               *
 * ****************************************************************
 */



/*
 * ****************************************************************
 *                                                               *
 * B E G I N      HANDLERS FOR INCOMING RSM MESSAGES             *
 *                                                               *
 * ****************************************************************
 */


/*
 * Handlers for the various messages that may arrive.  All of these happen
 * during interrupt handling, and will not actually use RSMPI calls.
 * Rather, they will schedule actions to happen.
 */


/*
 * Received CONNECT REQUEST message.  Cause this side to set up
 * connection to xfer segment and send back an ACCEPT message.
 *
 * We must have everything set up before sending the ACCEPT.
 * However, we must not transmit any data until we receive the ACK
 * of the ACCEPT.
 */
static void
opsrsmmsghdlr_req_connect(opsrsm_dest_t *rd, opsrsm_msg_t *msg)
{
	adapter_t *adapter = rd->rd_adapter;
	boolean_t utmo = B_FALSE;
	timeout_id_t tmoid = NULL;

	D1("opsrsmmsghdlr_req_connect: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, adapter->instance);

	/*
	 * xmit lock guarantees that timeout has really been set
	 * for any wait conditions.
	 */
	mutex_enter(&rd->rd_xmit_lock);
	mutex_enter(&adapter->opsrsm_runq_lock);

	if (rd->rd_segid_valid) {
		/*
		 * Another connect message - is it a duplicate?
		 * If so, just ignore.  Otherwise, there is a
		 * problem, so force a connection teardown.
		 */

		mutex_exit(&adapter->opsrsm_runq_lock);
		mutex_exit(&rd->rd_xmit_lock);

		if ((rd->rd_rxfersegid != msg->p.m.con_request.send_segid) ||
		    (rd->rd_lastconnmsg_seq != msg->p.hdr.seqno)) {
			/* Not the same connect request, drop connection */
			opsrsm_lostconn(rd);
		}

		return;
	}

	/* remember the message sequence number of this connection request */
	rd->rd_lastconnmsg_seq = msg->p.hdr.seqno;

	if (rd->rd_state == OPSRSM_STATE_W_ACCEPT) {
		/*
		 * Crossed connection requests.  If we're the higher
		 * numbered address, cancel the ACCEPT timeout and accept
		 * the remote request.  If we're the lower numbered
		 * address, ignore this request because the remote side
		 * will accept ours.  If the W_ACCEPT timeout expires prior
		 * to cancelling the timeout, the timeout function will
		 * notice the state is no longer W_ACCEPT, and will not
		 * cause the connection to be torn down.  If the timeout
		 * has already occured (and the rd state is S_DELETE),
		 * we're out of luck, and will have to wait for a new
		 * connection request from the remote side.
		 */
		if (rd->rd_rsm_addr >
		    adapter->rsmrdt_attr.attr_controller_addr) {
			rd->rd_segid_valid = B_TRUE;
			rd->rd_rxfersegid = msg->p.m.con_request.send_segid;
			/* LINTED: E_CONSTANT_CONDITION */
			OPSRSM_SETSTATE(rd, adapter,
			    "opsrsmmsghdlr_req_connect",
			    OPSRSM_STATE_S_CONNXFER_ACCEPT);
			utmo = B_TRUE;
			tmoid = rd->rd_tmo_id;
			rd->rd_tmo_id = 0;
			rd->rd_tmo_int = 0;
		}
	} else {

		/*
		 * Save away the connection information.  If possible,
		 * change the state to cause the request to be immediately
		 * acted upon.  If the state is currently INPROGRESS
		 * in the early stages of connection (during crexfer
		 * or the start of sconn), then this request will
		 * eventually be noticed when sconn() is called.  The
		 * sconn() function will notice that the segid is valid,
		 * and perform the CONNXER_ACCEPT tasks instead.
		 *
		 * If this rd's state was in a later stage of the
		 * connection dance (or after a connection exists), a
		 * previous connection request should have been received,
		 * the new connection request will not be expected, and
		 * this will have been caught by noticing the segid was
		 * already valid, and cause a failure, above.
		 */

		rd->rd_segid_valid = B_TRUE;
		rd->rd_rxfersegid = msg->p.m.con_request.send_segid;

		if (rd->rd_state == OPSRSM_STATE_NEW) {
			/*
			 * No connection was in progress.  Start a new
			 * connection setup process.
			 */
			/* LINTED: E_CONSTANT_CONDITION */
			OPSRSM_SETSTATE(rd, adapter,
			    "opsrsmmsghdlr_req_connect",
			    OPSRSM_STATE_S_NEWCONN);

		} else if (rd->rd_state == OPSRSM_STATE_W_SCONNTMO) {
			/*
			 * Accept this request instead of resending our
			 * connect request.  Cancel the timeout.  If the
			 * SCONNTMO timeout function is called prior to
			 * cancelling the timeout, it will notice the state
			 * is no longer W_SCONNTMO, and will not cause a
			 * new connection request to be sent.  If the
			 * timeout already occured (and rd is in the
			 * S_SCONN state), the sconn() function will notice
			 * that the segid is valid, and perform the
			 * CONNXER_ACCEPT tasks instead.
			 */
			/* LINTED: E_CONSTANT_CONDITION */
			OPSRSM_SETSTATE(rd, adapter,
			    "opsrsmmsghdlr_req_connect",
			    OPSRSM_STATE_S_CONNXFER_ACCEPT);
			utmo = B_TRUE;
			tmoid = rd->rd_tmo_id;
			rd->rd_tmo_id = 0;
			rd->rd_tmo_int = 0;
		}
	}

	mutex_exit(&adapter->opsrsm_runq_lock);
	mutex_exit(&rd->rd_xmit_lock);

	if (utmo)
		(void) untimeout(tmoid);
}



/*
 * Received ACCEPT message.  Cause this side to set up a connection
 * to the remote transfer segment and send back an ACK message.
 */
static void
opsrsmmsghdlr_con_accept(opsrsm_dest_t *rd, opsrsm_msg_t *msg)
{
	adapter_t *adapter = rd->rd_adapter;
	boolean_t utmo = B_FALSE;
	timeout_id_t tmoid;

	D1("opsrsmmsghdlr_con_accept: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, adapter->instance);

	/*
	 * xmit lock protects segid field
	 */
	mutex_enter(&rd->rd_xmit_lock);
	mutex_enter(&adapter->opsrsm_runq_lock);

	if (rd->rd_state == OPSRSM_STATE_W_ACCEPT &&
	    rd->rd_lxfersegid == msg->p.m.con_accept.rcv_segid) {
		rd->rd_segid_valid = B_TRUE;
		rd->rd_rxfersegid = msg->p.m.con_accept.send_segid;
		utmo = B_TRUE;
		tmoid = rd->rd_tmo_id;
		rd->rd_tmo_id = 0;
		/* LINTED: E_CONSTANT_CONDITION */
		OPSRSM_SETSTATE(rd, adapter, "opsrsmmsghdlr_con_accept",
		    OPSRSM_STATE_S_CONNXFER_ACK);
		mutex_exit(&adapter->opsrsm_runq_lock);
		mutex_exit(&rd->rd_xmit_lock);

		if (utmo)
			(void) untimeout(tmoid);
	} else {
		mutex_exit(&adapter->opsrsm_runq_lock);
		mutex_exit(&rd->rd_xmit_lock);
		opsrsm_lostconn(rd);
		return;
	}

}


/*
 * Received ACK message.  Now ok to proceed with DLPI data transfer.
 */
static void
opsrsmmsghdlr_con_ack(opsrsm_dest_t *rd, opsrsm_msg_t *msg)
{
	adapter_t *adapter = rd->rd_adapter;
	boolean_t utmo = B_FALSE;
	timeout_id_t tmoid;

	D1("opsrsmmsghdlr_con_ack: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, adapter->instance);

	mutex_enter(&adapter->opsrsm_runq_lock);

	if (rd->rd_state == OPSRSM_STATE_W_ACK &&
	    msg->p.m.con_ack.rcv_segid == rd->rd_lxfersegid &&
	    msg->p.m.con_ack.send_segid == rd->rd_rxfersegid) {
		int isdel = 0;

		utmo = B_TRUE;
		tmoid = rd->rd_tmo_id;
		rd->rd_tmo_id = 0;
		/* LINTED: E_CONSTANT_CONDITION */
		/*lint -e778 */
		OPSRSM_SETSTATE(rd, adapter, "opsrsmmsghdlr_con_ack",
		    OPSRSM_STATE_W_READY);
		/*lint +e778 */
		mutex_exit(&adapter->opsrsm_runq_lock);
		if (utmo) {
			(void) untimeout(tmoid);
		}
		if (rd->rd_status_tmo_id == 0) {
			rd->rd_status_tmo_id =
			    timeout(opsrsm_status_check_tmo, rd, 1);
		}
		mutex_enter(&rd->rd_evt_lock);
		rd->rd_evt_flags = OPSRSM_EVT_READY;
		cv_signal(&rd->rd_evt_cv);
		mutex_exit(&rd->rd_evt_lock);

		mutex_enter(&rd->rd_xmit_lock);
		rd->rd_xmit_state = OPSRSM_XMIT_BARRIER_CLOSED;
		cv_broadcast(&rd->rd_conn_cv);
		mutex_exit(&rd->rd_xmit_lock);

		mutex_enter(&rd->rd_sendq_lock);
		if (OPSRSM_Q_LEN(&rd->rd_sendq) > 0) {
			REFDEST(rd, isdel);
			if (isdel == 0) {
				opsrsm_dispatch_tmo((void *)rd);
			}
		}
		mutex_exit(&rd->rd_sendq_lock);
	} else {
		mutex_exit(&adapter->opsrsm_runq_lock);
		opsrsm_lostconn(rd);
		return;
	}
}

static void
opsrsm_sync_flow_tmo(void *arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;

	mutex_enter(&rd->rd_tmo_lock);
	if (rd->rd_flow_tmo_id == 0) {
		mutex_exit(&rd->rd_tmo_lock);
		return;
	}
	if (taskq_dispatch(rd->rd_adapter->opsrsm_taskq,
	    opsrsm_sync_flow_ctl, rd, KM_NOSLEEP) == 0) {
		rd->rd_flow_tmo_id = timeout(opsrsm_sync_flow_tmo, rd, 0);
	} else {
		rd->rd_flow_tmo_id = 0;
	}
	mutex_exit(&rd->rd_tmo_lock);
}

static void
opsrsm_set_sync_flow_tmo(opsrsm_dest_t *rd)
{
	int isdel = 0;

	mutex_enter(&rd->rd_tmo_lock);
	if (rd->rd_flow_tmo_id != 0) {
		goto out;
	}
	REFDEST(rd, isdel);
	if (isdel != 0) goto out;
	rd->rd_flow_tmo_id = timeout(opsrsm_sync_flow_tmo, rd, 0);
out:;
	mutex_exit(&rd->rd_tmo_lock);
}

static void
opsrsm_cancel_sync_flow_tmo(opsrsm_dest_t *rd)
{
	timeout_id_t tmoid;

	mutex_enter(&rd->rd_tmo_lock);
	if (rd->rd_flow_tmo_id == 0) {
		mutex_exit(&rd->rd_tmo_lock);
		return;
	}
	UNREFDEST(rd);
	tmoid = rd->rd_flow_tmo_id;
	rd->rd_flow_tmo_id = 0;
	mutex_exit(&rd->rd_tmo_lock);
	(void) untimeout(tmoid);
}

static void
opsrsm_sync_flow_ctl(void *arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;
	uchar_t srcaddr[64];
	rsm_barrier_t fc_barrier;
	opsrsm_flow_ctl_t *fctl;
	int err, errcnt = 0;

again:;
	fctl = (opsrsm_flow_ctl_t *)&srcaddr[0];
	fctl->fc_stop = rd->rd_remote_flow_stop;

	err = RSM_OPEN_BARRIER_REGION(rd->rd_adapter->rsmrdt_ctlr_obj,
	    rd->rd_rxferhand, &fc_barrier);
	ASSERT(err == RSM_SUCCESS);
	ASSERT((rd->rd_remote_flow_ctl & OPSRSM_CACHELINE_OFFSET) == 0);

	err = RSM_PUT(rd->rd_adapter->rsmrdt_ctlr_obj, rd->rd_rxferhand,
	    rd->rd_remote_flow_ctl, srcaddr, 64);
	ASSERT(err == RSM_SUCCESS);

	err = RSM_CLOSE_BARRIER(rd->rd_adapter->rsmrdt_ctlr_obj, &fc_barrier);
	if (err != RSM_SUCCESS) {
		opsrsmdev->opsrsm_ierrors++;
		if ((uint_t)++errcnt <=
		    opsrsmdev->opsrsm_param.opsrsm_retry_limit) {
			delay(2);
			goto again;
		} else {
			cmn_err(CE_CONT, "unable to sync flow control info\n");
		}
	}
	UNREFDEST(rd);
}

static void
opsrsm_check_flow_ctl(opsrsm_dest_t *rd)
{
	uint_t mem_lo_wat = max(opsrsmdev->opsrsm_param.opsrsm_mem_lo_wat,
	    opsrsmdev->opsrsm_param.opsrsm_buffer_size *
	    opsrsmdev->opsrsm_param.opsrsm_buffers);

	/* stop sender if pending_bytes gets too large */
	if (opsrsm_pending_bytes >
	    opsrsmdev->opsrsm_param.opsrsm_recv_hi_wat ||
	    freemem * PAGESIZE < mem_lo_wat) {
		if (rd->rd_remote_flow_stop == 0) {
			DERR("opsrsm_pending_bytes = %d "
			    "opsrsm_recv_hi_wat = %d", opsrsm_pending_bytes,
			    opsrsmdev->opsrsm_param.opsrsm_recv_hi_wat);
			rd->rd_remote_flow_stop = 1;
			if (rd->rd_state == OPSRSM_STATE_W_READY) {
				opsrsm_set_sync_flow_tmo(rd);
			}
		}
		mutex_enter(&opsrsm_flow_tmo_lock);
		if (opsrsm_flow_tmo_id == 0) {
			opsrsm_flow_tmo_retries = 0;
			opsrsm_flow_tmo_id = timeout(opsrsm_flow_tmo, NULL,
			    (long)opsrsmdev->opsrsm_param.opsrsm_flow_tmo_int);
		}
		mutex_exit(&opsrsm_flow_tmo_lock);
	}
}

/*ARGSUSED*/
static void
opsrsm_flow_enable(adapter_t *adp, void *arg)
{
	int i;

	for (i = 0; i < RSM_MAX_DESTADDR; i++) {
		opsrsm_dest_t *rd = NULL;
		int isdel = 0;

		FINDDEST(rd, isdel, i, adp);
		if (isdel != 0 || rd == NULL) {
			continue;
		}
		if (rd->rd_state != OPSRSM_STATE_W_READY) {
			UNREFDEST(rd);
			continue;
		}

		if (rd->rd_remote_flow_stop == 1) {
			rd->rd_remote_flow_stop = 0;
			opsrsm_set_sync_flow_tmo(rd);
		}
		UNREFDEST(rd);
	}
}

static void
opsrsm_flow_tmo_cancel(void)
{
	timeout_id_t tmoid;

	mutex_enter(&opsrsm_flow_tmo_lock);
	if (opsrsm_flow_tmo_id == 0) {
		mutex_exit(&opsrsm_flow_tmo_lock);
		return;
	}
	tmoid = opsrsm_flow_tmo_id;
	opsrsm_flow_tmo_id = 0;
	mutex_exit(&opsrsm_flow_tmo_lock);
	(void) untimeout(tmoid);
}

/*ARGSUSED*/
static void
opsrsm_flow_tmo(void *arg)
{
	uint_t mem_hi_wat = max(opsrsmdev->opsrsm_param.opsrsm_mem_hi_wat,
	    opsrsmdev->opsrsm_param.opsrsm_buffer_size *
	    opsrsmdev->opsrsm_param.opsrsm_buffers +
	    opsrsmdev->opsrsm_param.opsrsm_mem_hi_wat -
	    opsrsmdev->opsrsm_param.opsrsm_mem_lo_wat);

	mutex_enter(&opsrsm_flow_tmo_lock);
	if (opsrsm_flow_tmo_id == 0) {
		mutex_exit(&opsrsm_flow_tmo_lock);
		return;
	}
	opsrsm_flow_tmo_id = 0;
	if (opsrsm_pending_bytes >=
	    opsrsmdev->opsrsm_param.opsrsm_recv_lo_wat ||
	    freemem * PAGESIZE <= mem_hi_wat) {
		/* cannot unblock senders yet, rescheduling timeout */
		opsrsm_flow_tmo_id = timeout(opsrsm_flow_tmo, NULL,
		    (long)opsrsmdev->opsrsm_param.opsrsm_flow_tmo_int);
		opsrsm_flow_tmo_retries++;
		if ((opsrsm_flow_tmo_retries % 36000) == 0) {
			cmn_err(CE_CONT, "remained in flow control "
			    "condition for %d intervals\n",
			    opsrsm_flow_tmo_retries);
		}
		mutex_exit(&opsrsm_flow_tmo_lock);
		return;
	}
	opsrsm_flow_tmo_retries = 0;
	mutex_exit(&opsrsm_flow_tmo_lock);

	/* unblock senders */
	apply_on_all_adapters(opsrsm_flow_enable, NULL);
}

/*
 * Remote side has just sync'ed up the local DQE with its copy, so there
 * may be buffers to deliver.
 */
static void
opsrsmmsghdlr_syncdqe(opsrsm_dest_t *rd, opsrsm_msg_t *msg)
{
	int bufnum, offset, length;
	ushort_t sap;
	int freebufs = 0;
	uint32_t msg_cnt = 0;

	D1("opsrsmmsghdlr_syncdqe: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, rd->rd_adapter->instance);

	ASSERT(rd->rd_sstate == OPSRSM_RSMS_ALL);

	opsrsm_check_flow_ctl(rd);
	/*
	 * message sanity check
	 */
	if (msg->p.m.syncdqe.rcv_segid != rd->rd_lxfersegid ||
	    msg->p.m.syncdqe.msg_cnt == 0) {
		cmn_err(CE_CONT, "opsrsmmsghdlr_syncdqe: bad rcv_segid");
		opsrsm_lostconn(rd);
		return;
	}

	/* Loop through all valid DQE's and process their packets. */
	while (msg_cnt < msg->p.m.syncdqe.msg_cnt &&
	    opsrsmgetdqe(rd, &bufnum, &offset, &length, &sap)) {
		/* Don't try to send up DQE with zero length */
		if (length)
			freebufs += opsrsmread(rd, bufnum, offset, length, sap);
		else {
			cmn_err(CE_PANIC, "received corrupted packet\n");
			opsrsmputfqe(rd, bufnum);
			freebufs++;
		}

		if (freebufs ==
		    opsrsmdev->opsrsm_param.opsrsm_fqe_sync_size) {
			freebufs = 0;
			opsrsm_event_add(rd, OPSRSM_EVT_SYNC_FQE);
		}
		msg_cnt++;
	}
	if (msg_cnt != msg->p.m.syncdqe.msg_cnt) {
		cmn_err(CE_CONT, "DQ corruption detected, msg_cnt = %d, "
		    "dqes read = %d\n", msg->p.m.syncdqe.msg_cnt, msg_cnt);
	}
	if (freebufs) {
		opsrsm_event_add(rd, OPSRSM_EVT_SYNC_FQE);
	}
	D1("opsrsmmsghdlr_syncdqe: success");
}

static void
rsmrdtmsghdlr_senderr(opsrsm_dest_t *rd, opsrsm_msg_t *msg)
{
	opsrsmresource_t *rp;

	D1("opsrsmmsghdlr_senderr: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, rd->rd_adapter->instance);

	rp = opsrsmresource_lookup(msg->p.m.senderr.sender_portnum,
		OPSRSM_RO_DEFAULT);
	if (rp == NULL) {
		return;
	}

	if (msg->p.m.senderr.sender_pkey == rp->rs_pkey) {
		rp->rs_state |= msg->p.m.senderr.errstate;
	}
}


/* ARGSUSED */
static void
opsrsmmsghdlr_default(opsrsm_dest_t *rd, opsrsm_msg_t *msg)
{
	opsrsmerror(opsrsmdev->opsrsm_dip, "Unknown message type %d",
	    msg->p.hdr.reqtype);
}


/*
 * Handler for connection-related RSMPI messages from remote OPSRSM drivers
 */
/* ARGSUSED */
rsm_intr_hand_ret_t
opsrsm_rsm_intr_handler(rsm_controller_object_t *controller,
    rsm_intr_q_op_t operation,
    rsm_addr_t sender,
    void *data,
    size_t size,
    rsm_intr_hand_arg_t handler_arg)
{
	adapter_t *adapter = (adapter_t *)handler_arg;
	opsrsm_t *opsrsmp = opsrsmdev;
	opsrsm_dest_t *rd;
	opsrsm_msg_t *msg;
	int isdel = 0;
	int isnew = 0;

	if (adapter == NULL || opsrsmp == NULL)
		return (RSM_INTR_HAND_UNCLAIMED);
	/*
	 * We only handle RSM addresses that fit in 48 bits.
	 */
	ASSERT(sender <= (rsm_addr_t)0xffffffffffffLL);

	D1("opsrsm_intr_handle: opsrsmp 0x%p (cltr %d) sender-addr %ld",
	    (void *)opsrsmp,
	    adapter ? adapter->instance : -1, sender);

	/* Is this our interrupt? */
	mutex_enter(&adapter->mutex);
	if (controller->handle != adapter->rsmrdt_ctlr_obj.handle) {
		mutex_exit(&adapter->mutex);
		D1("opsrsm_intr_handle: bad controller handle");
		return (RSM_INTR_HAND_UNCLAIMED);
	}
	mutex_exit(&adapter->mutex);
	/*
	 * We don't really care about anything but a received packet
	 * or a queue destroy
	 */
	switch (operation) {

	case RSM_INTR_Q_OP_CREATE: {
		/*
		 * Create a dest structure, on the assumption that
		 * somebody's about to communicate with us.
		 */
		MAKEDEST(rd, isdel, isnew, sender, adapter);
		if (isdel || !rd) {
			return (RSM_INTR_HAND_CLAIMED_EXCLUSIVE);
		}
		UNREFDEST(rd);

		D1("opsrsm_intr_handle: op-create/config mkdset for addr %ld",
		    sender);

		return (RSM_INTR_HAND_CLAIMED_EXCLUSIVE);
	}

	case RSM_INTR_Q_OP_CONFIGURE:
		/* ignore configure messages */
		return (RSM_INTR_HAND_CLAIMED_EXCLUSIVE);

	case RSM_INTR_Q_OP_DESTROY: {
		/*
		 * The remote side has shut down the connection.  We need
		 * to shut local side of the connection down as well.
		 */
		FINDDEST(rd, isdel, sender, adapter);
		if (isdel || !rd) {
			return (RSM_INTR_HAND_CLAIMED_EXCLUSIVE);
		}
		D1("opsrsm_intr_handle: op-destroy for addr %ld", sender);
		opsrsm_lostconn(rd);
		UNREFDEST(rd);
		return (RSM_INTR_HAND_CLAIMED_EXCLUSIVE);
	}

	case RSM_INTR_Q_OP_RECEIVE:
		/*
		 * A DLPI message from the remote node.  Handle in the main
		 * body.
		 */
		break;

	default:
		/* ignore */
		return (RSM_INTR_HAND_UNCLAIMED);
	}

	/*
	 * Dest should already exist, having been created by the
	 * RSM_INTR_Q_OP_CREATE, above.
	 */
	FINDDEST(rd, isdel, sender, adapter);
	if (isdel) {
		return (RSM_INTR_HAND_CLAIMED_EXCLUSIVE);
	} else if (rd == NULL) {
		D1("opsrsm_rsm_intr_handler: can't finddest");
		return (RSM_INTR_HAND_CLAIMED_EXCLUSIVE);
	}


	msg = (opsrsm_msg_t *)data;

	if (msg->p.hdr.opsrsm_version != OPSRSM_VERSION) {
		/*
		 * Non-matching driver version!
		 * Toss message.
		 */
		DINFO("version mismatch: version = %d, expected = %d\n",
		    msg->p.hdr.opsrsm_version, OPSRSM_VERSION);
		UNREFDEST(rd);
		return (RSM_INTR_HAND_CLAIMED_EXCLUSIVE);
	}

	switch (msg->p.hdr.reqtype) {

	case OPSRSM_MSG_REQ_CONNECT:
		opsrsmmsghdlr_req_connect(rd, msg);
		break;

	case OPSRSM_MSG_CON_ACCEPT:
		opsrsmmsghdlr_con_accept(rd, msg);
		break;

	case OPSRSM_MSG_CON_ACK:
		opsrsmmsghdlr_con_ack(rd, msg);
		break;

		/*
		 * Maybe scan the incoming queue at this time?
		 */
	case OPSRSM_MSG_SYNC_DQE:
		opsrsmmsghdlr_syncdqe(rd, msg);
		break;

	case RSMRDT_MSG_SEND_ERR:
		rsmrdtmsghdlr_senderr(rd, msg);
		break;

	case OPSRSM_MSG_RESET:
		if (rd->rd_state == OPSRSM_STATE_W_READY) {
			(void) opsrsmgetstate(rd);
			opsrsmsetstate(rd, OPSRSM_STATE_S_DELETE);
		}
		break;
	case OPSRSM_MSG_FINFO_DEMUX_DONE:
	case OPSRSM_MSG_FINFO_REPLY:
	case OPSRSM_MSG_FINFO_REXMIT_ACK:
		opsrsmmsghdlr_finfo(rd, msg);
		break;
	default:
		opsrsmmsghdlr_default(rd, msg);
		break;
	}

	UNREFDEST(rd);

	return (RSM_INTR_HAND_CLAIMED_EXCLUSIVE);
/*
 * Supress lint Warning(550) [c:0]: isnew not accessed
 */
} /*lint !e550 */

/*
 * ****************************************************************
 *                                                               *
 * E N D       HANDLERS FOR INCOMING RSM MESSAGES                *
 *                                                               *
 * ****************************************************************
 */



/*
 * ****************************************************************
 *                                                               *
 * B E G I N   CONNECTION MANAGEMENT                             *
 *                                                               *
 * ****************************************************************
 */

/*
 * Create and initialize a transfer segment for the remote destination.  If
 * successful, return 0, else 1.  The destination's state must be
 * INPROGRESS.  In remains INPROGRESS during this function.
 */

static int
opsrsmcrexfer(opsrsm_t *opsrsmp, opsrsm_dest_t *rd)
{
	volatile opsrsm_xfer_hdr_t *xfer;
	opsrsm_fqe_t fqe;
	volatile opsrsm_fqe_t *fqep;
	opsrsm_dqe_t dqe;
	volatile opsrsm_dqe_t *dqep;
	opsrsmbuf_t *rbp;
	uint_t bufsize;
	int i, stat, bufalign = 0;
	uint32_t buf_offset, fq_offset, dq_offset;
	size_t xfer_size;
	caddr_t xfer_start;
	size_t roundup;
	size_t transport_pgsize = 0;
	rsm_access_entry_t perms;

	D1("opsrsmcrexfer: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, rd->rd_adapter->instance);

	ASSERT(rd->rd_rawmem_base_addr == NULL);
	ASSERT(rd->rd_rawmem_base_size == 0);

	bufsize = rd->rd_buffer_size;

	transport_pgsize = PAGESIZE;

	D1("opsrsmcrexfer: remote adapter id = %d", rd->rd_rem_adapterid);

	/*
	 * Make sure the remote side is responding before setting
	 * up the local xfer segment.
	 */
	stat = RSM_SENDQ_CREATE(rd->rd_adapter->rsmrdt_ctlr_obj,
	    rd->rd_rsm_addr,
	    (rsm_intr_t)(RSMRDT_INTR_T_BASE + rd->rd_rem_adapterid),
	    RSM_DLPI_QPRI, RSM_DLPI_QDEPTH, RSM_DLPI_QFLAGS,
	    RSM_RESOURCE_DONTWAIT, 0, &(rd->rsm_sendq));

	if (stat != RSM_SUCCESS) {
		cmn_err(CE_CONT, "sendq create failed, stat 0x%x\n", stat);
		return (1);
	}

	rd->rd_sstate |= OPSRSM_RSMS_RXFER_S;


	/*
	 * Allocate memory for segment.  Allow for alignment of DQE list
	 * and FQE list.  Also allow buffers to be aligned on
	 * RSM-page-sized boundaries.
	 */

	/*
	if (65536 % bufsize == 0)
		bufalign = 1;
	*/

	/*
	 * Even after round up to transport page alignments,
	 * to make sure that xfer_size can still accomodate
	 * all the queue elements, 2 * transport_pgsize has
	 * been used in the following calculation.
	 */

	xfer_size = (size_t)(sizeof (*xfer) + 64 +
	    (sizeof (opsrsm_dqe_t) * opsrsmp->opsrsm_param.opsrsm_queue_size)
	    + 64 +
	    (sizeof (opsrsm_fqe_t) * opsrsmp->opsrsm_param.opsrsm_queue_size)
	    + 64 + OPSRSM_FLOW_CTL_SZ +
	    (bufsize * (uint_t)(opsrsmp->opsrsm_param.opsrsm_buffers +
	    bufalign)) + (2 * (transport_pgsize - 1)));

	xfer_start = kmem_alloc(xfer_size, KM_NOSLEEP);
	if (!xfer_start) {
		D1("opsrsmcrexfer: can't allocate memory, returning 1");
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: crexfer, failed to alloc");
#endif /* DEBUG */
		return (1);
	}
	rd->rd_rawmem_base_addr = xfer_start;
	rd->rd_rawmem_base_size = xfer_size;

	/*
	 * Round up memory pointer and round down size to allow alignment
	 * within the transport's supported page size.
	 */
	roundup = transport_pgsize - ((uint64_t)xfer_start &
	    (transport_pgsize - 1));
	if (roundup != transport_pgsize) {
		xfer_size -= roundup;

		/* Align the xfer_start with transport_pgsize */
		xfer_start += roundup;
	}

	/* Align the xfer_size with the transport_pgsize */
	xfer_size = xfer_size & ~(transport_pgsize - 1);

	rd->rd_memory.ms_type = RSM_MEM_VADDR;
	rd->rd_memory.ms_memory.vr.length = xfer_size;
	rd->rd_memory.ms_memory.vr.as = NULL;	/* kas */
	rd->rd_memory.ms_memory.vr.vaddr = xfer_start;

	D1("opsrsmcrexfer: rawsize 0x%lx rawmem 0x%p xfersize 0x%lx "
	    "xfermem 0x%p pgsize 0x%lx\n",
	    rd->rd_rawmem_base_size,
	    (void *)rd->rd_rawmem_base_addr,
	    xfer_size,
	    (void *)xfer_start,
	    transport_pgsize);

	xfer = (volatile struct opsrsm_xfer_hdr *)xfer_start;

	/* Force FQ to start on a 64-byte boundary. */
	fq_offset = sizeof (struct opsrsm_xfer_hdr);
	fq_offset = OPSRSM_CACHELINE_ROUNDUP(fq_offset);

	/* Force DQ to start on a 64-byte boundary. */
	dq_offset = fq_offset + (sizeof (opsrsm_fqe_t) *
	    opsrsmp->opsrsm_param.opsrsm_queue_size + OPSRSM_FLOW_CTL_SZ);
	dq_offset = OPSRSM_CACHELINE_ROUNDUP(dq_offset);

	/* Force buffers to start on a 64-byte boundary. */
	buf_offset = dq_offset + (sizeof (opsrsm_dqe_t) *
	    opsrsmp->opsrsm_param.opsrsm_queue_size);
	buf_offset = OPSRSM_CACHELINE_ROUNDUP(buf_offset);

	if (bufalign == 1 && (buf_offset & (bufsize - 1)) != 0) {
		buf_offset += bufsize - (buf_offset & (bufsize - 1));
	}
	/*
	 * Note that while we set the _f and _n queue pointers and the
	 * queue lengths here, the _l pointers will be set (and the lengths
	 * may be adjusted) when we connect to the remote xfer segment (see
	 * connxfer).
	 */
	mutex_enter(&rd->rd_net_lock);

	rd->rd_fqr_f = rd->rd_fqr_n = (volatile opsrsm_fqe_t *) (xfer_start +
	    fq_offset);
	rd->rd_fqr_seq = 1;
	rd->rd_num_fqrs = opsrsmp->opsrsm_param.opsrsm_queue_size;

	/*
	 * flow control structure is located at 128 bytes before the
	 * start of the DQ
	 */
	rd->rd_flow_ctl = (volatile opsrsm_flow_ctl_t *)(xfer_start +
	    dq_offset - OPSRSM_FLOW_CTL_SZ);
	rd->rd_flow_ctl->fc_stop = 0;

	rd->rd_dqr_f = rd->rd_dqr_n = (volatile opsrsm_dqe_t *) (xfer_start +
	    dq_offset);
	rd->rd_dqr_seq = 1;
	rd->rd_num_dqrs = opsrsmp->opsrsm_param.opsrsm_queue_size;

	rd->rd_lbuf = xfer_start + buf_offset;
	rd->rd_lbuflen = bufsize;
	rd->rd_numlbufs = opsrsmp->opsrsm_param.opsrsm_buffers;

	/*
	 * Initialize the delivery and free queues:  elements in the free
	 * queue are valid, and elements in the delivery queue are invalid
	 * (seqno == 0).
	 */
	fqep = rd->rd_fqr_f;
	dqep = rd->rd_dqr_f;

	dqe.s.dq_seqnum = 0;
	dqe.s.dq_bufnum = (ushort_t)~0;

	fqe.s.fq_seqnum = 1;

	for (i = 0; i < opsrsmp->opsrsm_param.opsrsm_queue_size; i++) {
		fqe.s.fq_bufnum = (ushort_t)i;

		*fqep++ = fqe;
		*dqep++ = dqe;
	}

	mutex_exit(&rd->rd_net_lock);

	/* allocate space for queued fqes */
	mutex_enter(&rd->rd_fqr_lock);
	rd->rd_queued_fqe_array = (opsrsm_queued_fqe_t *)kmem_zalloc(opsrsmp->
	    opsrsm_param.opsrsm_queue_size * sizeof (opsrsm_queued_fqe_t),
	    KM_NOSLEEP);

	if (rd->rd_queued_fqe_array == NULL) {
		DINFO("opsrsmcrexfer: cannot alloc queued_fqe_array\n");
		mutex_exit(&rd->rd_fqr_lock);
		return (1);
	}
	/* construct freelist */
	for (i = 0; i < opsrsmp->opsrsm_param.opsrsm_queue_size; i++) {
		opsrsm_queued_fqe_t *qp;

		qp = &rd->rd_queued_fqe_array[i];
		/* enqueue element onto freelist */
		opsrsm_queued_fqe_free(rd, qp);
	}
	mutex_exit(&rd->rd_fqr_lock);

	/*
	 * Allocate and init our structures to describe loaned-up buffers.
	 */
	rbp = rd->rd_bufbase = (opsrsmbuf_t *)kmem_zalloc(opsrsmp->
	    opsrsm_param.opsrsm_buffers * sizeof (*rd->rd_bufbase), KM_NOSLEEP);

	if (rbp == NULL) {
		D1("opsrsmcrexfer: can't alloc rbp structs, returning 1");
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: abuf");
#endif /* DEBUG */
		return (1);
	}

	for (i = 0; i < rd->rd_numlbufs; i++) {
		rbp->rb_rd = rd;
		rbp->rb_frtn.free_func = opsrsmfreebuf;
		rbp->rb_frtn.free_arg = (char *)rbp;
		rbp->rb_bufnum = i;
		rbp++;
	}

	mutex_init(&rd->rd_nlb_lock, NULL, MUTEX_DRIVER, NULL);
	mutex_enter(&rd->rd_nlb_lock);
	rd->rd_nlb = 0;
	mutex_exit(&rd->rd_nlb_lock);

	/*
	 * Set everything in the header of the segment.
	 */

	xfer->rx_segsize = xfer_size;
	xfer->rx_buf_offset = buf_offset;
	xfer->rx_fq_offset = fq_offset;
	xfer->rx_dq_offset = dq_offset;
	xfer->rx_numbufs = rd->rd_numlbufs;
	xfer->rx_bufsize = rd->rd_lbuflen;
	xfer->rx_numfqes = rd->rd_num_fqrs;
	xfer->rx_numdqes = rd->rd_num_dqrs;
	xfer->rx_skey = rd->rd_local_skey;
	D1("opsrsmcrexfer: rx_buf_offset 0x%x fq_offset 0x%x dq_offset 0x%x "
	    "rd_numlbufs 0x%x rd_lbuflen 0x%x rd_num_fqrs 0x%x "
	    "rd_num_dqrs 0x%x\n",
	    buf_offset,
	    fq_offset,
	    dq_offset,
	    rd->rd_numlbufs,
	    rd->rd_lbuflen,
	    rd->rd_num_fqrs,
	    rd->rd_num_dqrs);

	xfer->rx_cookie = OPSRSM_XFER_COOKIE;

	/*
	 * Local xfer segment is now initialized; make it available to the
	 * remote node.
	 */

	stat = RSM_SEG_CREATE(rd->rd_adapter->rsmrdt_ctlr_obj,
	    &(rd->rd_lxferhand),
	    xfer_size, 0, &(rd->rd_memory), RSM_RESOURCE_DONTWAIT, 0);

	if (stat != RSM_SUCCESS) {
		D1("opsrsmcrexfer: can't create RSM segment, stat 0x%x, "
		    "return 1", stat);
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: crexfer, stat 0x%x", stat);
#endif /* DEBUG */
		return (1);
	}
	rd->rd_sstate |= OPSRSM_RSMS_LXFER_C;


	/*
	 * Publish this segment.  First try using an id that is likely
	 * to be unique.
	 */
	perms.ae_addr = rd->rd_rsm_addr;
	perms.ae_permission = RSM_PERM_RDWR;
	stat = RSMERR_SEGID_IN_USE;
	if (rd->rd_rsm_addr <=
	    (RSMRDT_SEGID_END - RSMRDT_SEGID_BASE)) {
		rd->rd_lxfersegid = RSMRDT_SEGID_BASE +
		    (uint32_t)rd->rd_rsm_addr;
		stat = (RSM_PUBLISH(rd->rd_adapter->rsmrdt_ctlr_obj,
			rd->rd_lxferhand,
		    &perms, 1, rd->rd_lxfersegid, NULL, 0));
	}
	if (stat == RSMERR_SEGID_IN_USE) {
		/* Couldn't use default id; try other ids in allowed range  */
		rd->rd_lxfersegid = RSMRDT_SEGID_BASE;
		while ((stat = (RSM_PUBLISH(rd->rd_adapter->rsmrdt_ctlr_obj,
		    rd->rd_lxferhand,
		    &perms, 1, rd->rd_lxfersegid, NULL, 0))) ==
		    RSMERR_SEGID_IN_USE && rd->rd_lxfersegid <
		    RSMRDT_SEGID_END)
			rd->rd_lxfersegid++;
	}

	if (stat != RSM_SUCCESS) {
		D1("opsrsmcrexfer: can't publish, stat 0x%x, returning 1",
		    stat);
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: expxfer, stat 0x%x", stat);
#endif /* DEBUG */
		return (1);
	}
	rd->rd_sstate |= OPSRSM_RSMS_LXFER_P;

	D1("opsrsmcrexfer: returning 0");
	return (0);
}

/*
 * Send a connect request to the remote.
 *
 * If we've received a Connect message from the destination, connect to the
 * remote transfer segment.  Otherwise, send them a Connect Request
 * message.  On success, return 0.  If the connect fails return 1.  A
 * failure in sending a Connect Request message will result in a retry
 * timeout being scheduled, but will not return 1 unless the total timeout
 * period has expired.  Destination's state must be INPROGRESS when called.
 * Destination's state is set to a new state prior to returning.
 */
static int
opsrsmsconn(
	opsrsm_t *opsrsmp,	/* OPSRSM device (RSM controller) pointer */
	opsrsm_dest_t *rd,	/* Destination pointer */
	int fromtmo)	/* 0 if this is our first attempt; nonzero if this */
			/*  is a retry, requested by a timeout routine. */
{
	int stat, seq;

	D1("opsrsmsconn: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, rd->rd_adapter->instance);

	if (rd->rd_segid_valid) {
		/*
		 * We've gotten a Connect Request from the remote side
		 * while in INPROGRESS state.  Don't send our request;
		 * instead, connect to the remote transfer segment.
		 */

		if ((stat = opsrsmconnxfer(opsrsmp, rd)) != 0) {
			return (stat);
		}
		return (opsrsmsaccept(opsrsmp, rd));
	}

	/*
	 * We haven't gotten a Connect Request from them, so we
	 * need to send one of our own.
	 */

	/*
	 * If this is a timeout retry, send the same Connect Request
	 * message we sent the first time.
	 */
	if (fromtmo) {
		seq = opsrsmsendmsg(rd, OPSRSM_REXMIT, 0);
	} else {
		opsrsm_msg_t msg;
		msg.p.m.con_request.send_segid = rd->rd_lxfersegid;
		seq = opsrsmsendmsg(rd, OPSRSM_MSG_REQ_CONNECT, &msg);
	}

	/*
	 * xmit lock guarantees new state and timeout setup both occur
	 * without an intervening state change.  See
	 * opsrsmmsghdlr_req_connect().
	 */
	mutex_enter(&rd->rd_xmit_lock);

	if (seq >= 0) {	/* Success */
		/*
		 * Set up a timeout to remind us if an ACCEPT never
		 * shows up.  This is only a 1-time timeout, no
		 * backoff is needed.
		 */

		opsrsmsetstate(rd, OPSRSM_STATE_W_ACCEPT);

		rd->rd_tmo_int = opsrsmp->opsrsm_param.opsrsm_ack_tmo;
		rd->rd_tmo_tot = 0;
		rd->rd_tmo_id = timeout(opsrsmaccepttmo, (caddr_t)rd,
		    rd->rd_tmo_int);
	} else {
		/*
		 * We couldn't send the message, set up a timeout to
		 * try again a little later.
		 */

		opsrsmsetstate(rd, OPSRSM_STATE_W_SCONNTMO);

		if (!fromtmo) {
			rd->rd_tmo_int =
			    opsrsmp->opsrsm_param.opsrsm_msg_init_tmo;
			rd->rd_tmo_tot = 0;
			D2("opsrsmsconn: !fromtmo, tmo_int %d, "
			    "tmo_tot %d", rd->rd_tmo_int,
			    rd->rd_tmo_tot);
		} else {
			/* Do exponential backoff */

			rd->rd_tmo_tot += rd->rd_tmo_int;
			rd->rd_tmo_int *= 2;

			/* If we've waited too long, fail */

			if (rd->rd_tmo_tot >=
			    (int)opsrsmp->opsrsm_param.opsrsm_msg_drop_tmo) {
				mutex_exit(&rd->rd_xmit_lock);
				(void) opsrsmgetstate(rd);
				D1("opsrsmsconn: tmo limit reached, "
				    "returning 1");
				return (1);
			}

			/* Clip timeout to maximum */

			if (rd->rd_tmo_int >
			    opsrsmp->opsrsm_param.opsrsm_msg_max_tmo)
				rd->rd_tmo_int =
				    opsrsmp->opsrsm_param.opsrsm_msg_max_tmo;

			D2("opsrsmsconn: tmo_int %d, tmo_tot %d",
			    rd->rd_tmo_int, rd->rd_tmo_tot);
		}

		rd->rd_tmo_id = timeout(opsrsmsconntmo, (caddr_t)rd,
		    rd->rd_tmo_int);
	}

	mutex_exit(&rd->rd_xmit_lock);

	D1("opsrsmsconn: returning 0");
	return (0);
}


/*
 * Connect to the transfer segment on the destination machine.  If an error
 * occurs, return 1.  Destination state must be INPROGRESS.  It remains
 * INPROGRESS during this function.
 */
static int
opsrsmconnxfer(opsrsm_t *opsrsmp, opsrsm_dest_t *rd)
{
	uint_t num_rbufs;
	int stat;
	int i;
	opsrsm_fqe_t fqe;
	volatile opsrsm_fqe_t *fqep;
	opsrsm_dqe_t dqe;
	volatile opsrsm_dqe_t *dqep;
	size_t segsize;

	D1("opsrsmconnxfer: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, rd->rd_adapter->instance);

	mutex_enter(&rd->rd_xmit_lock);

	stat = RSM_CONNECT(rd->rd_adapter->rsmrdt_ctlr_obj, rd->rd_rsm_addr,
	    rd->rd_rxfersegid, &(rd->rd_rxferhand));
	if (stat != RSM_SUCCESS) {
		D1("opsrsmconnxfer: can't connxfer, stat 0x%x, returning 1",
		    stat);
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: connxfer, stat 0x%x", stat);
#endif /* DEBUG */
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}

	rd->rd_sstate |= OPSRSM_RSMS_RXFER_C;


	/*
	 * Copy entire header struct into local memory
	 */

	stat = RSM_GET(rd->rd_adapter->rsmrdt_ctlr_obj, rd->rd_rxferhand, 0,
	    &(rd->rd_rxferhdr), sizeof (opsrsm_xfer_hdr_t));
	if (stat != RSM_SUCCESS) {
		D1("opsrsmconnxfer: can't read xfer header, stat = %d", stat);
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: read xfer header");
#endif /* DEBUG */
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}



	/*
	 * Validate header structure, extract some values from it
	 */
	if (rd->rd_rxferhdr.rx_cookie != OPSRSM_XFER_COOKIE) {
		D1("opsrsmconnxfer: badxfer, cookie 0x%x, returning 1",
		    rd->rd_rxferhdr.rx_cookie);
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: badxfer, cookie 0x%x",
		    (uint_t)rd->rd_rxferhdr.rx_cookie);
#endif /* DEBUG */
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}

	D1("opsrsmconnxfer: remote buf_offset 0x%x fq_offset 0x%x "
	    "dq_offset 0x%x rd_numbufs 0x%x rd_lbuflen 0x%x "
	    "rd_numfqes 0x%x rd_numdqes 0x%x\n",
		rd->rd_rxferhdr.rx_buf_offset,
		rd->rd_rxferhdr.rx_fq_offset,
		rd->rd_rxferhdr.rx_dq_offset,
		rd->rd_rxferhdr.rx_numbufs,
		rd->rd_rxferhdr.rx_bufsize,
		rd->rd_rxferhdr.rx_numfqes,
		rd->rd_rxferhdr.rx_numdqes);

	rd->rd_remote_skey = rd->rd_rxferhdr.rx_skey;
	segsize = rd->rd_rxferhdr.rx_segsize;

	num_rbufs = rd->rd_rxferhdr.rx_numbufs;

	D1("num_rbufs 0x%x opsrsm_queue_size 0x%x\n", num_rbufs,
		opsrsmp->opsrsm_param.opsrsm_queue_size);
	/*
	 * Must be at least one more element in queue than the
	 * number of buffers, so that we can track when all queue
	 * elements need to be flushed to remote side.
	 */
	if (num_rbufs >= opsrsmp->opsrsm_param.opsrsm_queue_size)
		num_rbufs = opsrsmp->opsrsm_param.opsrsm_queue_size - 1;

	mutex_enter(&rd->rd_net_lock);

	rd->rd_rbufoff = (off_t)rd->rd_rxferhdr.rx_buf_offset;
	rd->rd_rbuflen = rd->rd_rxferhdr.rx_bufsize;
	rd->rd_numrbuf = (ushort_t)num_rbufs;

	if ((size_t)(rd->rd_rbufoff + (off_t)(rd->rd_rbuflen * num_rbufs))
	    > segsize) {
		D1("opsrsm: badxfer, rbufoff too big");
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: badxfer, rbufoff too big");
#endif /* DEBUG */
		mutex_exit(&rd->rd_net_lock);
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}

	rd->rd_fqw_f_off = (off_t)rd->rd_rxferhdr.rx_fq_offset;
	rd->rd_num_fqws = rd->rd_rxferhdr.rx_numfqes;

	if ((size_t)(rd->rd_fqw_f_off + (off_t)(sizeof (opsrsm_fqe_t) *
	    rd->rd_num_fqws)) > segsize) {
		D1("opsrsm: badxfer, fqw_f_off too big");
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: badxfer, fqw_f_off too big");
#endif /* DEBUG */
		mutex_exit(&rd->rd_net_lock);
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}

	rd->rd_dqw_f_off = (off_t)rd->rd_rxferhdr.rx_dq_offset;
	rd->rd_num_dqws = rd->rd_rxferhdr.rx_numdqes;
	rd->rd_remote_flow_ctl = (off_t)rd->rd_rxferhdr.rx_dq_offset -
	    OPSRSM_FLOW_CTL_SZ;
	rd->rd_remote_flow_stop = 0;

	if ((size_t)(rd->rd_dqw_f_off + (off_t)(sizeof (opsrsm_dqe_t) *
	    rd->rd_num_dqws)) > segsize) {
		D1("opsrsm: badxfer, dqw_f_off too big");
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: badxfer, dqw_f_off too big");
#endif /* DEBUG */
		mutex_exit(&rd->rd_net_lock);
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}

	/*
	 * Now that we know the number of remote buffers and queue elements,
	 * shrink everything to fit and calculate the ends of all queues.
	 */


	D1("rd_numlbufs 0x%x rd_num_fqws 0x%x rd_num_dqrs 0x%x\n",
	    rd->rd_numlbufs, rd->rd_num_fqws, rd->rd_num_dqrs);
	rd->rd_numlbufs =
		min(rd->rd_numlbufs, min(rd->rd_num_fqws - 1,
		rd->rd_num_dqrs - 1));
	rd->rd_num_fqws = rd->rd_num_dqrs =
		min(rd->rd_numlbufs + 1, min(rd->rd_num_fqws, rd->rd_num_dqrs));

	D1("rd_numrbuf 0x%x rd_num_fqrs 0x%x rd_num_dqws 0x%x\n",
	    rd->rd_numrbuf, rd->rd_num_fqrs, rd->rd_num_dqws);
	rd->rd_numrbuf =
		min(rd->rd_numrbuf, min(rd->rd_num_fqrs - 1,
		rd->rd_num_dqws - 1));
	rd->rd_num_fqrs = rd->rd_num_dqws =
		min(rd->rd_numrbuf + 1, min(rd->rd_num_fqrs, rd->rd_num_dqws));

	rd->rd_fqr_l = rd->rd_fqr_f + rd->rd_num_fqrs - 1;
	/* mark last entry of free queue as invalid */
	rd->rd_fqr_l->s.fq_seqnum = 0;
	rd->rd_fqr_l->s.fq_bufnum = (ushort_t)~0;
	rd->rd_dqr_l = rd->rd_dqr_f + rd->rd_num_dqrs - 1;

	/* Create FQE cache, outgoing FQE/DQE queues */

	D1("opsrsmconnxfer: num_fqrs 0x%x num_fqws 0x%x num_dqws 0x%x",
	    rd->rd_num_fqrs, rd->rd_num_fqws, rd->rd_num_dqws);

	if ((size_t)(rd->rd_rbufoff + (off_t)(rd->rd_rbuflen * num_rbufs))
	    > segsize) {
		D1("opsrsm: badxfer, bufsize * num buf too big");
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: badxfer, bufsize * num buf too big");
#endif /* DEBUG */
		mutex_exit(&rd->rd_net_lock);
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}
	if ((size_t)(rd->rd_fqw_f_off + (off_t)(sizeof (opsrsm_fqe_t) *
	    rd->rd_num_fqws)) > segsize) {
		D1("opsrsm: badxfer, fqesize * num fqe too big");
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: badxfer, fqesize * num fqe too big");
#endif /* DEBUG */
		mutex_exit(&rd->rd_net_lock);
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}
	if ((size_t)(rd->rd_dqw_f_off + (off_t)(sizeof (opsrsm_dqe_t) *
	    (size_t)rd->rd_num_dqws)) > segsize) {
		D1("opsrsm: badxfer, dqesize * num dqe too big");
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: badxfer, dqesize * num dqe too big");
#endif /* DEBUG */
		mutex_exit(&rd->rd_net_lock);
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}

	rd->rd_cached_fqr = kmem_alloc(sizeof (*rd->rd_cached_fqr) *
	    rd->rd_num_fqrs, KM_NOSLEEP);
	rd->rd_shdwfqw_f = kmem_alloc(sizeof (*rd->rd_shdwfqw_f) *
	    rd->rd_num_fqws, KM_NOSLEEP);
	rd->rd_shdwdqw_f = kmem_alloc(sizeof (*rd->rd_shdwdqw_f) *
	    rd->rd_num_dqws, KM_NOSLEEP);

	if (rd->rd_cached_fqr == NULL || rd->rd_shdwfqw_f == NULL ||
	    rd->rd_shdwdqw_f == NULL) {
		D1("opsrsmconnxfer: can't alloc memory for shadow queues, "
		    "returning 1");
#ifdef DEBUG
		cmn_err(CE_CONT, "?opsrsm: can't get memory in connxfer");
#endif /* DEBUG */
		mutex_exit(&rd->rd_net_lock);
		mutex_exit(&rd->rd_xmit_lock);
		return (1);
	}

	/*
	 * Initialize the shadow delivery and free queues:  all elements in
	 * the free queue are valid except the last entry, and all elements in
	 * the delivery queue are invalid.  It is necessary to initialize
	 * because when we do an opsrsmsyncfqe() or opsrsmsyncdqe(), we may do
	 * an RSM_PUT of more than the newly changed entries (because we
	 * round up/down to 64 byte boundaries).
	 */

	rd->rd_shdwfqw_l = rd->rd_shdwfqw_f + rd->rd_num_fqws - 1;
	rd->rd_shdwfqw_i = rd->rd_shdwfqw_o = rd->rd_shdwfqw_l;
	/* still in first round, on last element */
	rd->rd_fqw_seq = 1;

	fqep = rd->rd_shdwfqw_f;
	fqe.s.fq_seqnum = 1;
	i = 0;
	while (fqep <= rd->rd_shdwfqw_l - 1) {
		fqe.s.fq_bufnum = (ushort_t)i++;
		*fqep++ = fqe;
	}
	/* last entry is not valid */
	fqep->s.fq_seqnum = 0;
	fqep->s.fq_bufnum = (ushort_t)~0;

	D1("opsrsmconnxfer: initialized %d fqe shadow entries", i);


	rd->rd_shdwdqw_l = rd->rd_shdwdqw_f + rd->rd_num_dqws - 1;
	rd->rd_shdwdqw_i = rd->rd_shdwdqw_o = rd->rd_shdwdqw_f;
	/* in first round, on first element */
	rd->rd_dqw_seq = 1;

	dqep = rd->rd_shdwdqw_f;
	dqe.s.dq_seqnum = 0;
	dqe.s.dq_bufnum = (ushort_t)~0;
	while (dqep <= rd->rd_shdwdqw_l) {
		*dqep++ = dqe;
	}

	stat = RSM_SET_BARRIER_MODE(rd->rd_adapter->rsmrdt_ctlr_obj,
	    rd->rd_rxferhand, RSM_BARRIER_MODE_EXPLICIT);
	ASSERT(stat == RSM_SUCCESS);

	mutex_exit(&rd->rd_net_lock);

	D1("opsrsmconnxfer: returning 0");
	mutex_exit(&rd->rd_xmit_lock);
	return (0);
}



/*
 * Send an ACCEPT message to the destination.
 * Return 1 if this send fails, else set state to W_ACK and return 0.
 * Destination's state must be INPROGRESS.
 * Destination's state is set to a new state on success.
 */
static int
opsrsmsaccept(
	opsrsm_t *opsrsmp,	/* OPSRSM device (RSM controller) pointer */
	opsrsm_dest_t *rd)	/* Destination pointer */
{
	int seq;
	opsrsm_msg_t msg;
	int retval = 0;

	D1("opsrsmsaccept: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, rd->rd_adapter->instance);

	msg.p.m.con_accept.send_segid = rd->rd_lxfersegid;
	msg.p.m.con_accept.rcv_segid = rd->rd_rxfersegid;
	seq = opsrsmsendmsg(rd, OPSRSM_MSG_CON_ACCEPT, &msg);

	mutex_enter(&rd->rd_xmit_lock);

	if (seq >= 0) {		/* Success */
		opsrsmsetstate(rd, OPSRSM_STATE_W_ACK);

		rd->rd_tmo_int =
		    opsrsmp->opsrsm_param.opsrsm_ack_tmo;
		rd->rd_tmo_tot = 0;
		rd->rd_tmo_id = timeout(opsrsmacktmo, (caddr_t)rd,
			    rd->rd_tmo_int);

	} else {		/* Failure */
		retval = 1;
	}
	mutex_exit(&rd->rd_xmit_lock);

	D1("opsrsmsack: returning %d", retval);
	return (retval);
}


/*
 * Send an ACK response to the destination.
 * Return 1 if this send fails, else set state to READY and return 0.
 * Destination's state must be INPROGRESS.
 * Destination's state is set to a new state on success.
 */
static int
opsrsmsack(
	opsrsm_dest_t *rd)	/* Destination pointer */
{
	int seq;
	opsrsm_msg_t msg;
	int retval = 0;

	D1("opsrsmsack: rd 0x%p (addr %ld ctlr %d)",
	    (void *)rd, rd->rd_rsm_addr, rd->rd_adapter->instance);

	msg.p.m.con_ack.send_segid = rd->rd_lxfersegid;
	msg.p.m.con_ack.rcv_segid = rd->rd_rxfersegid;
	seq = opsrsmsendmsg(rd, OPSRSM_MSG_CON_ACK, &msg);

	if (seq >= 0) {		/* Success */
		int isdel = 0;

		opsrsmsetstate(rd, OPSRSM_STATE_W_READY);
		if (rd->rd_status_tmo_id == 0) {
			rd->rd_status_tmo_id =
			    timeout(opsrsm_status_check_tmo, rd, 1);
		}
		mutex_enter(&rd->rd_evt_lock);
		rd->rd_evt_flags = OPSRSM_EVT_READY;
		cv_signal(&rd->rd_evt_cv);
		mutex_exit(&rd->rd_evt_lock);

		mutex_enter(&rd->rd_xmit_lock);
		rd->rd_xmit_state = OPSRSM_XMIT_BARRIER_CLOSED;
		cv_broadcast(&rd->rd_conn_cv);
		mutex_exit(&rd->rd_xmit_lock);

		mutex_enter(&rd->rd_sendq_lock);
		if (OPSRSM_Q_LEN(&rd->rd_sendq) > 0) {
			REFDEST(rd, isdel);
			if (isdel == 0) {
				opsrsm_dispatch_tmo((void *)rd);
			}
		}
		mutex_exit(&rd->rd_sendq_lock);
	} else {		/* Failure */
		retval = 1;
	}


	D1("opsrsmsack: returning %d", retval);
	return (retval);
}

/*
 * ****************************************************************
 *                                                               *
 * E N D       CONNECTION MANAGEMENT                             *
 *                                                               *
 * ****************************************************************
 */



/*
 * ****************************************************************
 *                                                               *
 * B E G I N   FREE/DELIVERY QUEUE MANAGEMENT                    *
 *                                                               *
 * ****************************************************************
 */

/*
 * allocate a queued fqe from the qfqe freelist
 */
static opsrsm_queued_fqe_t *
opsrsm_queued_fqe_alloc(opsrsm_dest_t *rd)
{
	opsrsm_queued_fqe_t *qfqe;

	ASSERT(MUTEX_HELD(&rd->rd_fqr_lock));
	qfqe = rd->rd_queued_fqe_freelist;
	if (qfqe == NULL)
		return (NULL);

	rd->rd_queued_fqe_freelist = qfqe->qf_next;
	rd->rd_queued_fqe_cnt--;
	if (rd->rd_queued_fqe_cnt < 0) {
		cmn_err(CE_PANIC, "rd %p corrupt freelist\n", rd);
	}
	qfqe->qf_next = NULL;
	qfqe->qf_bufnum = -1;

	/*
	 * this can only happen if the RSM calls in
	 * syncfqe don't return or take excessive time.
	 */
	if (rd->rd_queued_fqe_freelist == NULL) {
		DINFO("0x%x dequeued last element from qfqe freelist\n",
		    rd->rd_local_skey);
	}
	return (qfqe);
}

/*
 * enqueue an unused qfqe onto the qfqe freelist
 */
static void
opsrsm_queued_fqe_free(opsrsm_dest_t *rd, opsrsm_queued_fqe_t *qfqe)
{
	ASSERT(MUTEX_HELD(&rd->rd_fqr_lock));
	qfqe->qf_bufnum = -1;
	qfqe->qf_next = rd->rd_queued_fqe_freelist;
	rd->rd_queued_fqe_freelist = qfqe;
	rd->rd_queued_fqe_cnt++;
	if (rd->rd_queued_fqe_cnt > opsrsmdev->
	    opsrsm_param.opsrsm_queue_size) {
		cmn_err(CE_PANIC, "rd %p corrupt freelist\n", rd);
	}
}

/*
 * Queue an FQE with the specified buffer number onto the shadow FQ for
 * transmission to the remote system. If fqr_flags indicate that the
 * sync_fqe thread is still accessing the shadow FQ pointers, we do not
 * update the FQ. Instead, the FQE is enqueued and it will be consumed
 * by the sync_fqe thread when it completes its RSM calls. the sync_fqe
 * thread will then update the actual FQ and schedule a sync of the FQ.
 */
static void
opsrsmputfqe(
	opsrsm_dest_t *rd,	/* Destination pointer */
	int bufnum)	/* Number of free buffer */
{
	opsrsm_fqe_t fqe;
	boolean_t fq_corrupted;

	mutex_enter(&rd->rd_fqr_lock);
	D1("opsrsmputfqe: rd 0x%p (addr %ld ctlr %d), bufnum %d", (void *)rd,
	    rd->rd_rsm_addr, rd->rd_adapter->instance, bufnum);

	/* if sync_fqe is in progress, we defer the update of the FQ. */
	if ((rd->rd_fqr_flags & OPSRSM_FQR_LOCKED) != 0) {
		opsrsm_queued_fqe_t *qfqe = opsrsm_queued_fqe_alloc(rd);

		/* it is impossible to run out of qfqes */
		if (qfqe == NULL) {
			DINFO("0x%x qfqe freelist empty!\n",
			    rd->rd_local_skey);
			/* abort the connection */
			mutex_exit(&rd->rd_fqr_lock);
			opsrsm_lostconn(rd);
			return;
		}
		opsrsmdev->opsrsm_queued_fqes++;
		qfqe->qf_bufnum = bufnum;
		if (rd->rd_queued_fqe_list == NULL) {
			rd->rd_queued_fqe_list = qfqe;
			rd->rd_queued_fqe_tail = qfqe;
		} else {
			rd->rd_queued_fqe_tail->qf_next = qfqe;
			rd->rd_queued_fqe_tail = qfqe;
		}
		mutex_exit(&rd->rd_fqr_lock);
		return;
	}
	opsrsmdev->opsrsm_put_fqes++;
	fqe.s.fq_filler = 0;
	fqe.s.fq_bufnum = (ushort_t)bufnum;

	*rd->rd_shdwfqw_i = fqe;

	rd->rd_shdwfqw_i = (rd->rd_shdwfqw_i == rd->rd_shdwfqw_l) ?
	    rd->rd_shdwfqw_f : rd->rd_shdwfqw_i + 1;

	ASSERT(rd->rd_shdwfqw_i != rd->rd_shdwfqw_o);
	fq_corrupted = (rd->rd_shdwfqw_i == rd->rd_shdwfqw_o);

	D1("opsrsmputfqe: done");
	mutex_exit(&rd->rd_fqr_lock);
	if (fq_corrupted) opsrsm_lostconn(rd);
}

/*
 * Queue an FQE with the specified buffer number onto the shadow FQ for
 * transmission to the remote system. This call does not acquire the
 * fqr_lock.
 */
static void
opsrsmputfqe_nolock(
	opsrsm_dest_t *rd,	/* Destination pointer */
	int bufnum)	/* Number of free buffer */
{
	opsrsm_fqe_t fqe;
	boolean_t fq_corrupted;

	D1("opsrsmputfqe_nolock: rd 0x%p (addr %ld ctlr %d), bufnum %d",
	    (void *)rd, rd->rd_rsm_addr, rd->rd_adapter->instance, bufnum);

	opsrsmdev->opsrsm_put_fqes++;
	fqe.s.fq_filler = 0;
	fqe.s.fq_bufnum = (ushort_t)bufnum;

	*rd->rd_shdwfqw_i = fqe;

	rd->rd_shdwfqw_i = (rd->rd_shdwfqw_i == rd->rd_shdwfqw_l) ?
	    rd->rd_shdwfqw_f : rd->rd_shdwfqw_i + 1;

	ASSERT(rd->rd_shdwfqw_i != rd->rd_shdwfqw_o);
	fq_corrupted = (rd->rd_shdwfqw_i == rd->rd_shdwfqw_o);

	D1("opsrsmputfqe_nolock: done");
	if (fq_corrupted) opsrsm_lostconn(rd);
}

static void
opsrsmputdqes(opsrsm_dest_t *rd)
{
	mblk_t *mp;
	int qlen, cnt = 0;

	mutex_enter(&rd->rd_net_lock);
	mp = OPSRSM_Q_HEAD(&rd->rd_pendq);
	qlen = OPSRSM_Q_LEN(&rd->rd_pendq);
	while (mp) {
		int bufnum = (int)mp->b_prev;
		int pktlen = MBLKL(mp);
		opsrsm_dqe_t dqe;

		if (pktlen > (int)rd->rd_rbuflen) pktlen = (int)rd->rd_rbuflen;
		dqe.s.dq_offset = (uchar_t)0;
		dqe.s.dq_bufnum = (ushort_t)bufnum;
		dqe.s.dq_length = (ushort_t)pktlen;
		dqe.s.dq_sap = 0;
		*rd->rd_shdwdqw_i = dqe;

		rd->rd_shdwdqw_i = (rd->rd_shdwdqw_i == rd->rd_shdwdqw_l) ?
		    rd->rd_shdwdqw_f : rd->rd_shdwdqw_i + 1;

		ASSERT(rd->rd_shdwdqw_i != rd->rd_shdwdqw_o);
		mp = OPSRSM_Q_NEXT(&rd->rd_pendq, mp);
		cnt++;
	}
	if (cnt != qlen) cmn_err(CE_PANIC, "pending queue corrupted\n");
	rd->rd_pkts_delivered += (uint32_t)qlen;
	mutex_exit(&rd->rd_net_lock);
}

/*
 * Determine whether there are any available FQEs.  If so, return 1; if none
 * are available, return 0.
 */
static int
opsrsmavailfqe(
	opsrsm_dest_t *rd)	/* Destination pointer */
{
	opsrsm_fqe_t fqe;

	mutex_enter(&rd->rd_net_lock);

	D1("opsrsmavailfqe: rd 0x%p", (void *)rd);

	if (rd->rd_cached_fqr_cnt) {
		D1("opsrsmavailfqe: (cached) returning 1");

		mutex_exit(&rd->rd_net_lock);
		return (1);
	}

	fqe = *rd->rd_fqr_n;

	if (fqe.s.fq_seqnum == (rd->rd_fqr_seq & OPSRSM_FQE_SEQ_MASK)) {
		D1("opsrsmavailfqe: returning 1");

		mutex_exit(&rd->rd_net_lock);
		return (1);
	} else {
		D1("opsrsmavailfqe: seq %d, expecting %d, returning 0",
		    fqe.s.fq_seqnum, rd->rd_fqr_seq & OPSRSM_FQE_SEQ_MASK);

		mutex_exit(&rd->rd_net_lock);
		return (0);
	}
}

static int
opsrsmavailfqe2(opsrsm_dest_t *rd)
{
	opsrsm_fqe_t fqe;

	fqe = *rd->rd_fqr_n;

	if (fqe.s.fq_seqnum == (rd->rd_fqr_seq & OPSRSM_FQE_SEQ_MASK)) {
		return (1);
	} else {
		return (0);
	}
}

/*
 * Attempt to retrieve the next available FQE from the queue.  If successful,
 * return 1; if none are available, return 0.
 */
static int
opsrsmgetfqe(
	opsrsm_dest_t *rd,	/* Destination pointer */
	int *bufnum)	/* Set to number of free buffer, if we got one */
{
	opsrsm_fqe_t fqe;

	/* checks if receiver is blocking new packets */
	if (rd->rd_flow_ctl->fc_stop == 1) {
		return (0);
	}

	/* If we have FQE's cached, return one of those */
	if (rd->rd_cached_fqr_cnt) {
		*bufnum = rd->rd_cached_fqr[--rd->rd_cached_fqr_cnt];
		return (1);
	}
	/* Get next FQE */
	fqe = *rd->rd_fqr_n;

	/* Is it valid? */

	if (fqe.s.fq_seqnum == (rd->rd_fqr_seq & OPSRSM_FQE_SEQ_MASK)) {
		/* Yup, return number */

		*bufnum = fqe.s.fq_bufnum;

		/* Bump pointer, wrap if needed */

		if (rd->rd_fqr_n == rd->rd_fqr_l) {
			rd->rd_fqr_n = rd->rd_fqr_f;
			rd->rd_fqr_seq++;
			if (rd->rd_fqr_seq == 0)
				rd->rd_fqr_seq++;
		} else
			rd->rd_fqr_n++;

		/* Exercise some paranoia */

		if (*bufnum >= rd->rd_numrbuf) {
#ifdef DEBUG
			cmn_err(CE_NOTE,
			    "opsrsmgetfqe: bogus buffer %d in FQE "
			    "at 0x%lx (max %d)", *bufnum,
			    (uintptr_t)rd->rd_fqr_n, rd->rd_numrbuf);
#endif /* DEBUG */
			return (0);
		}
		return (1);
	} else {
		return (0);
	}
}

/*
 * Attempt to retrieve the next available DQE from the queue.  If successful,
 * return 1; if none are available, return 0.
 */
static int
opsrsmgetdqe(
	opsrsm_dest_t *rd,	/* Destination pointer */
	int *bufnum,	/* Buffer number, set if we find a DQE */
	int *offset,	/* Packet offset, set if we find a DQE */
	int *length,	/* Packet length, set if we find a DQE */
	ushort_t *sap)	/* Packet SAP, set if we find a DQE */
{
	opsrsm_dqe_t dqe;

	D1("opsrsmgetdqe: rd 0x%p (addr %ld ctlr %d) index %ld", (void *)rd,
	    rd->rd_rsm_addr, rd->rd_adapter->instance,
	    (size_t)((char *)rd->rd_dqr_n - (char *)rd->rd_dqr_f) /
	    sizeof (dqe));
	D2("opsrsmgetdqe: dqr_n 0x%p (index %ld) dqr_f 0x%p dqr_l 0x%p seq %d",
	    (void *)rd->rd_dqr_n,
	    (size_t)((char *)rd->rd_dqr_n - (char *)rd->rd_dqr_f) /
	    sizeof (dqe), (void *)rd->rd_dqr_f, (void *)rd->rd_dqr_l,
	    rd->rd_dqr_seq & OPSRSM_DQE_SEQ_MASK);

	/* Get next DQE */
	dqe = *rd->rd_dqr_n;

	/* Is it valid? */
	if (dqe.s.dq_seqnum == (rd->rd_dqr_seq & OPSRSM_DQE_SEQ_MASK)) {
		*bufnum = dqe.s.dq_bufnum;
		*offset = dqe.s.dq_offset;
		*length = dqe.s.dq_length;
		*sap = dqe.s.dq_sap;

		/* Exercise some paranoia */

		if (*bufnum >= rd->rd_numlbufs) {
			DINFO("opsrsmgetdqe: bogus buffer %d "
			    "in DQE at 0x%p (max %d)\n", *bufnum,
			    (void *) &dqe, rd->rd_numlbufs);
			return (0);
		}

		if (*offset > OPSRSM_CACHELINE_OFFSET) {
			DINFO("opsrsmgetdqe: bogus offset %d "
			    "in DQE at 0x%lx (max %d)\n", *offset,
			    (uintptr_t)&dqe, OPSRSM_CACHELINE_OFFSET);
			return (0);
		}

		if (*offset + *length > (int)rd->rd_lbuflen) {
			DINFO("opsrsmgetdqe: bogus offset+length %d+%d "
			    "in DQE at 0x%lx (max %d)\n",
			    *offset, *length, (uintptr_t)&dqe,
			    rd->rd_lbuflen);
			return (0);
		}

		if (rd->rd_dqr_n == rd->rd_dqr_l) {
			rd->rd_dqr_n = rd->rd_dqr_f;
			rd->rd_dqr_seq++;
			if (rd->rd_dqr_seq == 0)
				rd->rd_dqr_seq++;
		} else
			rd->rd_dqr_n++;

		D1("opsrsmgetdqe: returning 1, *bufnum %d, *offset %d, "
		    "*length %d, *sap 0x%x", *bufnum, *offset, *length, *sap);
		return (1);
	} else {
		D1("opsrsmgetdqe: seq %d, expecting %d, returning 0",
		    dqe.s.dq_seqnum, rd->rd_dqr_seq & OPSRSM_DQE_SEQ_MASK);

		return (0);
	}
}


/*
 * ****************************************************************
 *                                                               *
 * E N D       FREE/DELIVERY QUEUE MANAGEMENT                    *
 *                                                               *
 * ****************************************************************
 */



/*
 * ****************************************************************
 *                                                               *
 * B E G I N   COMMUNICATION                                     *
 *                                                               *
 * ****************************************************************
 */


/*
 * Send a message to a remote system.  Returns the sequence number of the
 * message if one was successfully sent, or -1 if the caller needs to retry
 * later.  The special message type OPSRSM_REXMIT causes us to retransmit the
 * last message we sent (unsuccessfully or successfully), without
 * incrementing the sequence number.  This cannot be called from an
 * interrupt.
 */
static int
opsrsmsendmsg(opsrsm_dest_t *rd, uint8_t msg_type, opsrsm_msg_t *msg)
{
	rsm_send_t send_obj;
	boolean_t sendq_up;
	int status;

	sendq_up = (rd->rd_sstate & OPSRSM_RSMS_RXFER_S);
	if (rd == NULL || OPSRSM_IS_LOOPBACK(rd) || !sendq_up) {
		return (0);
	}
	if (msg_type == OPSRSM_REXMIT) {
		if (rd->rsm_previous_msg_valid) {
			msg = &rd->rsm_previous_msg;
		} else {
			return (-2);
		}
	} else {
		bcopy(msg, &rd->rsm_previous_msg,
		    sizeof (rd->rsm_previous_msg));
		rd->rsm_previous_msg.p.hdr.reqtype = msg_type;
		/* rd_nseq is a ushort, and will wrap when it gets too big. */
		rd->rsm_previous_msg.p.hdr.seqno = rd->rd_nseq++;
		rd->rsm_previous_msg.p.hdr.opsrsm_version = OPSRSM_VERSION;
	}

	/*
	 * Send fails immediately if message can't be queued.
	 */
	send_obj.is_data = &rd->rsm_previous_msg;
	send_obj.is_size = sizeof (opsrsm_msg_t);
	send_obj.is_flags = RSM_DLPI_SQFLAGS;
	send_obj.is_wait = 0;

	status = RSM_SEND(rd->rd_adapter->rsmrdt_ctlr_obj, rd->rsm_sendq,
	    &send_obj, NULL);

	if (status != RSM_SUCCESS) {
		D2("opsrsmsendmsg FAILED!! status %d\n", status);
		return (-1);
	} else {
		D2("opsrsmsendmsg: succeeded\n");
		return (rd->rsm_previous_msg.p.hdr.seqno);
	}
}


/*
 * ****************************************************************
 *                                                               *
 * E N D       COMMUNICATION                                     *
 *                                                               *
 * ****************************************************************
 */




/*
 * ****************************************************************
 *                                                               *
 * B E G I N   TIMEOUT-FUNCTIONS                                 *
 *                                                               *
 * ****************************************************************
 */

/*
 * Timeout functions
 */

/*
 * Connect retransmit backoff timer has expired.  Retransmit the connect
 * request.
 */
static void
opsrsmsconntmo(void * arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;

	D1("opsrsmsconntmo: rd 0x%p (addr %ld ctlr %d)", (void *)rd,
	    rd->rd_rsm_addr, rd->rd_adapter->instance);

	rd->rd_tmo_id = 0;

	/*
	 * If the timeout was cancelled, the state will have also change
	 * from W_SCONNTMO, and opsrsmmovestate() will have no effect.
	 */
	(void) opsrsmmovestate(rd, OPSRSM_STATE_W_SCONNTMO,
	    OPSRSM_STATE_S_SCONN);

	D1("opsrsmsconntmo: done");
}


/*
 * Timer to wait for ACCEPT message from remote has expired.
 * This indicates the remote side is not reachable, so tear
 * down the OPSRSM device (controller).
 */
static void
opsrsmaccepttmo(void * arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;

	D1("opsrsmaccepttmo: rd 0x%p (addr %ld ctlr %d)", (void *)rd,
	    rd->rd_rsm_addr, rd->rd_adapter->instance);

	rd->rd_tmo_id = 0;

	/*
	 * If the timeout was cancelled, the state will have also changed
	 * from W_ACCEPT, and opsrsmmovestate() will have no effect.
	 */
	(void) opsrsmmovestate(rd, OPSRSM_STATE_W_ACCEPT,
	    OPSRSM_STATE_S_DELETE);

	D1("opsrsmaccepttmo: done");
}


/*
 * Timer to wait for ACK message from remote expired.
 * This indicates the remote side is not reachable, so tear
 * down the OPSRSM device (controller).
 */
static void
opsrsmacktmo(void * arg)
{
	opsrsm_dest_t *rd = (opsrsm_dest_t *)arg;

	D1("opsrsmacktmo: rd 0x%p (addr %ld ctlr %d)", (void *)rd,
	    rd->rd_rsm_addr, rd->rd_adapter->instance);

	rd->rd_tmo_id = 0;

	/*
	 * If the timeout was cancelled, the state will have also changed
	 * from W_ACK, and opsrsmmovestate() will have no effect.
	 */
	(void) opsrsmmovestate(rd, OPSRSM_STATE_W_ACK,
	    OPSRSM_STATE_S_DELETE);

	D1("opsrsmacktmo: done");
}

/*
 * ****************************************************************
 *                                                               *
 * E N D       TIMEOUT-FUNCTIONS                                 *
 *                                                               *
 * ****************************************************************
 */


/*
 * *********************** Resource Number Management ********************
 * We start with no resource array. Each time we run out of slots, we
 * reallocate a new larger array and copy the pointer to the new array and
 * a new resource blk is allocated and added to the hash table.
 *
 * The resource control block contains:
 *      root    - array of pointer of resource blks
 *      sz      - current size of array.
 *      len     - last valid entry in array.
 *
 * A search operation based on a resource number is as follows:
 *      index = rnum / RESOURCE_BLKSZ;
 *      ASSERT(index < resource_block.len);
 *      ASSERT(index < resource_block.sz);
 *      offset = rnum % RESOURCE_BLKSZ;
 *      ASSERT(offset >= resource_block.root[index]->base);
 *      ASSERT(offset < resource_block.root[index]->base + RESOURCE_BLKSZ);
 *      return resource_block.root[index]->blks[offset];
 *
 * A resource blk is freed with its used count reachs zero.
 */

static void
opsrsmresource_init(void)
{
	rw_init(&opsrsm_resource.opsrsmrct_lock, NULL, RW_DRIVER, NULL);
	opsrsm_resource.opsrsmrc_len = 0;
	opsrsm_resource.opsrsmrc_sz = 0;
	opsrsm_resource.opsrsmrc_cnt = 0;
	opsrsm_resource.opsrsmrc_flag = 0;
	opsrsm_resource.opsrsmrc_root = NULL;
}

static void
opsrsmresource_fini(void)
{
	rw_destroy(&opsrsm_resource.opsrsmrct_lock);
}

static opsrsmresource_t *
opsrsmresource_alloc(minor_t *rnum)
{
	int i, j, empty = -1;
	opsrsmresource_blk_t *blk;
	opsrsmresource_t *rp;

	rp = opsrsm_resstruct_alloc();
	if (rp == NULL)
		return (NULL);

	while (rw_tryenter(&opsrsm_resource.opsrsmrct_lock, RW_WRITER) == 0) {
		delay(1);
	}
	/*
	 * Try to find an empty slot
	 */
	for (i = 0; i < opsrsm_resource.opsrsmrc_len; i++) {
		blk = opsrsm_resource.opsrsmrc_root[i];
		if (blk != NULL && blk->opsrsmrcblk_avail > 0) {

			D1("Available blks %d", blk->opsrsmrcblk_avail);

			/*
			 * found an empty slot in this blk
			 */
			for (j = 0; j < OPSRSMRC_BLKSZ; j++) {
				if (blk->opsrsmrcblk_blks[j] == NULL) {
					*rnum = (minor_t)
						(j + (i * OPSRSMRC_BLKSZ));

					blk->opsrsmrcblk_blks[j] = rp;
					rp->rs_num = *rnum;
					blk->opsrsmrcblk_avail--;
					opsrsm_resource.opsrsmrc_cnt++;
					rw_exit(&opsrsm_resource.
						opsrsmrct_lock);
					return (rp);
				}
			}
		} else if (blk == NULL && empty < 0) {
			/*
			 * remember first empty slot
			 */
			empty = i;
		}
	}

	/*
	 * Couldn't find anything, allocate a new blk
	 * Do we need to reallocate the root array
	 */

	if (empty < 0) {
		if (opsrsm_resource.opsrsmrc_len ==
			opsrsm_resource.opsrsmrc_sz) {
			/*
			 * Allocate new array and copy current stuff into it
			 */
			opsrsmresource_blk_t	**p;
			uint_t newsz = (uint_t)opsrsm_resource.opsrsmrc_sz +
					OPSRSMRC_BLKSZ;

			D1("allocate new array with size %d", newsz);

			p = (opsrsmresource_blk_t **)kmem_zalloc(
				newsz * sizeof (*p), KM_SLEEP);

			if (opsrsm_resource.opsrsmrc_root) {
				uint_t oldsz;

				oldsz = (uint_t)(opsrsm_resource.opsrsmrc_sz *
					(int)sizeof (*p));

				/*
				 * Copy old data into new space and
				 * free old stuff
				 */
				bcopy(opsrsm_resource.opsrsmrc_root, p, oldsz);
				kmem_free(opsrsm_resource.opsrsmrc_root, oldsz);
			}

			opsrsm_resource.opsrsmrc_root = p;
			opsrsm_resource.opsrsmrc_sz = (int)newsz;
		}

		empty = opsrsm_resource.opsrsmrc_len;
		opsrsm_resource.opsrsmrc_len++;

		D1("opsrsmrc_len %d\n", opsrsm_resource.opsrsmrc_len);
	}

	/*
	 * Allocate a new blk
	 */
	blk = (opsrsmresource_blk_t *)kmem_zalloc(sizeof (*blk), KM_SLEEP);
	ASSERT(opsrsm_resource.opsrsmrc_root[empty] == NULL);
	opsrsm_resource.opsrsmrc_root[empty] = blk;
	blk->opsrsmrcblk_avail = OPSRSMRC_BLKSZ - 1;

	/*
	 * Allocate slot
	 */

	*rnum = (minor_t)(empty * OPSRSMRC_BLKSZ);
	blk->opsrsmrcblk_blks[0] = rp;
	rp->rs_num = *rnum;
	opsrsm_resource.opsrsmrc_cnt++;
	rw_exit(&opsrsm_resource.opsrsmrct_lock);

	return (rp);
}


static opsrsmresource_t *
opsrsmresource_free(minor_t rnum)
{
	int i, j;
	opsrsmresource_blk_t *blk;
	opsrsmresource_t *p;

	i = (int)(rnum / OPSRSMRC_BLKSZ);
	j = (int)(rnum % OPSRSMRC_BLKSZ);

	if (i >= opsrsm_resource.opsrsmrc_len) {
		D1("opsrsmresource_free done\n");
		return (NULL);
	}

	while (rw_tryenter(&opsrsm_resource.opsrsmrct_lock, RW_WRITER) == 0) {
		delay(1);
	}
	ASSERT(opsrsm_resource.opsrsmrc_root);
	ASSERT(i < opsrsm_resource.opsrsmrc_len);
	ASSERT(i < opsrsm_resource.opsrsmrc_sz);
	blk = opsrsm_resource.opsrsmrc_root[i];
	if (blk == NULL) {
		rw_exit(&opsrsm_resource.opsrsmrct_lock);
		D1("opsrsmresource_free done\n");
		return (NULL);
	}

	ASSERT(blk->opsrsmrcblk_blks[j]); /* reserved or full */

	p = blk->opsrsmrcblk_blks[j];
	blk->opsrsmrcblk_blks[j] = NULL;
	blk->opsrsmrcblk_avail++;
	if (blk->opsrsmrcblk_avail == OPSRSMRC_BLKSZ) {
		/*
		 * free this blk
		 */
		kmem_free(blk, sizeof (*blk));
		opsrsm_resource.opsrsmrc_root[i] = NULL;
	}
	opsrsm_resource.opsrsmrc_cnt--;
	rw_exit(&opsrsm_resource.opsrsmrct_lock);

	return (p);
}


static opsrsmresource_t *
opsrsmresource_lookup(minor_t rnum, int opt)
{
	int i, j;
	opsrsmresource_blk_t *blk;
	opsrsmresource_t *rp;

	/*
	 * Find resource and lock it in READER mode
	 * search for available resource slot
	 */

	i = (int)(rnum / OPSRSMRC_BLKSZ);
	j = (int)(rnum % OPSRSMRC_BLKSZ);

	if (i >= opsrsm_resource.opsrsmrc_len) {
		DERR("lookup of port %d failed", rnum);
		return (NULL);
	}

	rw_enter(&opsrsm_resource.opsrsmrct_lock, RW_READER);
	blk = opsrsm_resource.opsrsmrc_root[i];
	if (blk != NULL) {
		ASSERT(i < opsrsm_resource.opsrsmrc_len);
		ASSERT(i < opsrsm_resource.opsrsmrc_sz);

		rp = blk->opsrsmrcblk_blks[j];
		if (rp == NULL) {
			DERR("lookup of port %d failed, rp = NULL, "
			    "blk = %p", rnum, &blk->opsrsmrcblk_blks[j]);
		}

		if (rp != NULL && opt == OPSRSM_RO_INCREFCNT) {
			OPSRSM_RSREF(rp);
		}
	} else {
		DERR("lookup of port %d failed, blk = NULL", rnum);
		rp = NULL;
	}
	rw_exit(&opsrsm_resource.opsrsmrct_lock);

	return (rp);
}

static void
opsrsmresource_destroy()
{
	int i, j;

	while (rw_tryenter(&opsrsm_resource.opsrsmrct_lock, RW_WRITER) == 0) {
		delay(1);
	}
	for (i = 0; i < opsrsm_resource.opsrsmrc_len; i++) {
		opsrsmresource_blk_t	*blk;

		blk = opsrsm_resource.opsrsmrc_root[i];
		if (blk == NULL) {
			continue;
		}
		for (j = 0; j < OPSRSMRC_BLKSZ; j++) {
			if (blk->opsrsmrcblk_blks[j] != NULL) {
				D1("Not null slot %d, %lx\n", j,
				(size_t)blk->opsrsmrcblk_blks[j]);
			}
		}
		kmem_free(blk, sizeof (*blk));
		opsrsm_resource.opsrsmrc_root[i] = NULL;
	}
	if (opsrsm_resource.opsrsmrc_root) {
		i = opsrsm_resource.opsrsmrc_sz *
		(int)sizeof (opsrsmresource_blk_t *);
		kmem_free(opsrsm_resource.opsrsmrc_root, (uint_t)i);
		opsrsm_resource.opsrsmrc_root = NULL;
		opsrsm_resource.opsrsmrc_len = 0;
		opsrsm_resource.opsrsmrc_sz = 0;
		opsrsm_resource.opsrsmrc_flag = 0;
		opsrsm_resource.opsrsmrc_cnt = 0;
	}

	rw_exit(&opsrsm_resource.opsrsmrct_lock);
}
