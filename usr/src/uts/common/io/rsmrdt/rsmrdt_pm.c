/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rsmrdt_pm.c	1.23	08/05/20 SMI"
/*
 * This module provides for the management of interconnect adapters
 * inter-node connections (aka paths).  Adapter descriptors are
 * maintained on a linked list; one list per adapter devname.  Each
 * adapter descriptor heads a linked list of path descriptors.
 *
 *
 * The exported interface consists of the following functions:
 *	- rsmrdt_add_adapter
 *	- rsmrdt_remove_adapter
 *
 *      [add_path and remove_path only called for current adapters]
 *	- rsmrdt_add_path
 *	- rsmrdt_remove_path	   [a path down request is implicit]
 *
 *	- rsmrdt_path_up           [called at clock ipl for Sun Cluster]
 *	- rsmrdt_path_down         [called at clock ipl for Sun Cluster]
 *	- rsmrdt_disconnect_node   [called at clock ipl for Sun Cluster;
 *				   treat like path-down for all node paths;
 *				   can be before node_alive; always before
 *				   node_died.]
 *
 *	[node_alive and node_died are always paired]
 *	- rsmrdt_node_alive   called after the first cluster path is up
 *                            for this node
 *	- rsmrdt_node_died
 *
 *      [set the local node id]
 *      - rsmrdt_set_my_nodeid    called to set the variable rsmrdt_my_nodeid
 *                                to the local node id
 *
 * Processing for these functions is setup as a state machine supported
 * by the data structures described above.
 *
 * The functions rsmrdt_path_up, rsmrdt_path_down, and rsmrdt_disconnect_node
 * are called at clock interrupt level from the Path-Manager/RSMRDT driver.
 * Interface which precludes sleeping; so these functions may (optionally)
 * defer processing to an independent thread running at normal ipl.
 *
 *
 * lock definitions:
 *
 *	(mutex) rsmrdt_adp_listhead_base.listlock
 *			protects linked list of adapter listheads
 *			Always acquired before listhead->mutex
 *
 *      (mutex) listhead->mutex
 *			protects adapter listhead, linked list of
 *			adapters, and linked list of paths.
 *
 *      (mutex) path->mutex
 *			protects the path descriptor.
 *
 *	(mutex) adapter->mutex
 *			protects adapter descriptor contents.  used
 *			mainly for ref_cnt update.
 */

#include <sys/param.h>
#include <sys/kmem.h>
#include <sys/time.h>
#include <sys/devops.h>
#include <sys/ddi_impldefs.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/proc.h>
#include <sys/thread.h>
#include <sys/taskq.h>
#include <sys/modctl.h>

#include <sys/rsm/rsm.h>
#include "rsmrdt_path_int.h"

rsm_node_id_t rsmrdt_my_nodeid = (rsm_node_id_t)-1;

/* lint -save -e413 */
/* FUNCTION PROTOTYPES */
static void init_adapter(adapter_t *, char *, int, rsm_addr_t, int);
static adapter_t *rsmrdt_lookup_adapter(char *, int);
static adapter_t *find_adapter(int);
static path_t *lookup_path(char *, int, rsm_node_id_t, rsm_addr_t);
static path_t *find_path(rsm_node_id_t, int, rsm_node_id_t, int);
static void link_path(path_t *);
static void destroy_path(path_t *);
void rsmrdt_pathmanager_cleanup(void);
adapter_t *rsmrdt_select_adapter(rsm_node_id_t, int);
void rsmrdt_get_remote_ids(adapter_t *, rsm_addr_t, int *, int *);

static struct adapter_listhead_list rsmrdt_adp_listhead_base;
static int category = RSM_MODULE_ALL;

void rsmrdt_pathmanager_init(void);

extern opsrsm_t *opsrsmdev;
extern rsm_intr_hand_ret_t opsrsm_rsm_intr_handler(rsm_controller_object_t *,
	rsm_intr_q_op_t, rsm_addr_t, void *, size_t, rsm_intr_hand_arg_t);

extern int rsmrdt_adapterinit(adapter_t *);
extern int rsmrdt_adapterfini(adapter_t *);
extern void rsmrdt_failover(adapter_t *, rsm_addr_t);
extern int rsmrdt_check_openhandles(void);

void apply_on_all_adapters(void (*)(adapter_t *, void *), void *);

/*
 * Called from the rsmrdt module (opsrsm.c)  _init() routine
 */
void
rsmrdt_pathmanager_init(void)
{
	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_pathmanager_init enter\n"));

	/* initialization for locks and condition variables  */
	mutex_init(&rsmrdt_adp_listhead_base.listlock, NULL,
	    MUTEX_DEFAULT, NULL);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_pathmanager_init done\n"));

}

void
rsmrdt_pathmanager_cleanup(void)
{
	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_pathmanager_cleanup enter\n"));

	mutex_destroy(&rsmrdt_adp_listhead_base.listlock);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_pathmanager_cleanup done\n"));

}

void
rsmrdt_set_my_nodeid(rsm_node_id_t local_nodeid)
{
	rsmrdt_my_nodeid = local_nodeid;

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt: my node %x\n", rsmrdt_my_nodeid));

}

/*
 * EXTERNAL INTERFACES
 *
 */

/*
 *
 * If the adapter is supported by rsmpi then initialize an adapter descriptor
 * and link it to the list of adapters.  The adapter attributes are obtained
 * from rsmpi and stored in the descriptor.
 *
 */

void *
rsmrdt_add_adapter(char *name, int instance, rsm_addr_t hwaddr, int adapterid)
{
	adapter_t		*adapter;
	rsm_controller_attr_t	*attr;
	int 			rval;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_add_adapter enter\n"));

	adapter = kmem_zalloc(sizeof (adapter_t), KM_SLEEP);
	if (adapter == NULL) {
		DBG_PRINTF((category, RSM_ERR,
		    "rsmrdt_add_adapter: adapter alloc failed\n"));
		return (NULL);
	}

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt_add_adapter:name=%s(%d) id=%x hwaddr=%llx adp=%lx\n",
	    name, instance, adapterid, hwaddr, adapter));

	/* verify name length */
	if (strlen(name) >= MAXNAMELEN) {
		DBG_PRINTF((category, RSM_ERR,
		    "rsmrdt_add_adapter done: name too long\n"));
		kmem_free(adapter, sizeof (adapter_t));
		return (NULL);
	}

	/*
	 * Check if rsmpi supports this adapter type
	 */
	rval = rsm_get_controller(name, (uint_t)instance,
		&adapter->rsmrdt_ctlr_obj, RSM_VERSION);
	if (rval != RSM_SUCCESS) {
		DBG_PRINTF((category, RSM_ERR,
		    "rsmrdt_add_adapter done: get controller failed %d\n",
		    rval));
		kmem_free(adapter, sizeof (adapter_t));
		return (NULL);
	}

	/*
	 * Get adapter attributes
	 */
	if (rsm_get_controller_attr(adapter->rsmrdt_ctlr_obj.handle, &attr)
		!= RSM_SUCCESS) {
		DBG_PRINTF((category, RSM_ERR,
		    "rsmrdt_add_adapter: get_controller_attr(%d) Failed\n",
		    instance));
		(void) rsm_release_controller(name, (uint_t)instance,
		    &adapter->rsmrdt_ctlr_obj);
		kmem_free(adapter, sizeof (adapter_t));
		return (NULL);
	}

	/*
	 * NOTE : Register range of service ids for this driver with PSARC
	 */
	adapter->rsmrdt_service_id = (rsm_intr_t)(RSMRDT_INTR_T_BASE +
		adapterid);

	rval = RSM_REGISTER_HANDLER(adapter->rsmrdt_ctlr_obj,
		adapter->rsmrdt_service_id, opsrsm_rsm_intr_handler,
		(rsm_intr_hand_arg_t)adapter, NULL, 0);
	if (rval != RSM_SUCCESS) {
		DBG_PRINTF((category, RSM_ERR,
		    "rsmrdt_add_adapter: Intr handler regis. failed %d\n",
		    rval));
		(void) rsm_release_controller(name, (uint_t)instance,
			&(adapter->rsmrdt_ctlr_obj));
		kmem_free(adapter, sizeof (adapter_t));
		return (NULL);
	}

	/* Initialize an adapter descriptor and add it to the adapter list */
	init_adapter(adapter, name, instance, hwaddr, adapterid);

	/* Copy over the attributes from the pointer returned to us */
	adapter->rsmrdt_attr = *attr;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_add_adapter done\n"));

	/* return adapter pointer */
	return ((void *)adapter);
}


/*
 * Unlink the adapter descriptor.
 */
boolean_t
rsmrdt_remove_adapter(char *name, int instance)
{
	adapter_t		*adapter;
	adapter_listhead_t	*listhead;
	adapter_t		*prev, *current;
	int 			rval;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_remove_adapter enter\n"));

	DBG_PRINTF((category, RSM_DEBUG,
		"rsmrdt_remove_adapter : name %s(%d)\n", name, instance));

	adapter = rsmrdt_lookup_adapter(name, instance);
	if (adapter == NULL) {
		DBG_PRINTF((category, RSM_ERR,
			"rsmrdt_remove_adapter : adapter not found \n"));
		return (B_FALSE);
	}

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt_remove_adapter : adapter 0x%lx\n", adapter));

	/*
	 * Un-init per adapter resources used by rsmrdt driver
	 */
	if (rsmrdt_adapterfini(adapter) != 0) {
		DBG_PRINTF((category, RSM_ERR,
		"rsmrdt_remove_adapter:rsmrdt_adapterfini failed \n"));
		return (B_FALSE);
	}


	listhead = adapter->listhead;
	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	mutex_enter(&listhead->mutex);

	/* find the adapter in the list and remove it */
	prev = NULL;
	current = listhead->next_adapter;
	while (current != NULL) {
		if (adapter->instance == current->instance) {
			DBG_PRINTF((category, RSM_DEBUG,
			"rsmrdt_remove_adapter: Found adp 0x%lx\n", current));
			break;
		} else {
			prev = current;
			current = current->next;
		}
	}
	ASSERT(current != NULL);

	if (prev == NULL)
		listhead->next_adapter = current->next;
	else
		prev->next = current->next;

	listhead->adapter_count--;
	DBG_PRINTF((category, RSM_DEBUG,
		"rsmrdt_remove_adapter : adapter count %d\n",
		listhead->adapter_count));

	mutex_exit(&listhead->mutex);
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	mutex_enter(&current->mutex);
	rval = RSM_UNREGISTER_HANDLER(current->rsmrdt_ctlr_obj,
		current->rsmrdt_service_id, opsrsm_rsm_intr_handler,
		(rsm_intr_hand_arg_t)current);

	DBG_PRINTF((category, RSM_DEBUG,
		"rsmrdt_remove_adapter : Unregistered Handler : %d\n", rval));

	rval = rsm_release_controller(current->listhead->adapter_devname,
	    (uint_t)current->instance, &current->rsmrdt_ctlr_obj);

	DBG_PRINTF((category, RSM_DEBUG,
		"rsmrdt_remove_adapter: Controller released : %d\n", rval));

	mutex_exit(&current->mutex);

	mutex_destroy(&current->mutex);

	DBG_PRINTF((category, RSM_DEBUG,
		"rsmrdt_remove_adapter: free adapter 0x%lx\n", current));

	kmem_free(current, sizeof (adapter_t));

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_remove_adapter done\n"));

	return (B_TRUE);
}

/*
 * An adapter descriptor will exist from an earlier add_adapter. This
 * function initializes the path descriptor
 */
void *
rsmrdt_add_path(char *name, int instance, rsm_node_id_t remote_node,
    rsm_addr_t remote_hwaddr, int rem_adapt_instance, int rem_adapterid)
{
	path_t			*path;
	adapter_t		*adapter;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_add_path enter\n"));

	adapter = rsmrdt_lookup_adapter(name, instance);
	if (adapter == NULL) {
		DBG_PRINTF((category, RSM_ERR,
			"rsmrdt_add_path: Unable to find adapter\n"));
		return (NULL);
	}

	/* allocate new path descriptor */
	path = kmem_zalloc(sizeof (path_t), KM_SLEEP);

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt_add_path: adp = 0x%lx path = %lx rem node %x rhw %llx\n",
	    adapter, path, remote_node, remote_hwaddr));

	/*
	 * initialize path descriptor
	 */
	mutex_init(&path->mutex, NULL, MUTEX_DEFAULT, NULL);

	path->state = RSMRDT_PATH_DOWN;
	path->remote_node = remote_node;
	path->remote_hwaddr = remote_hwaddr;
	path->remote_devinst = rem_adapt_instance;
	path->remote_adapterid = rem_adapterid;
	path->local_adapter = adapter;

	/* link path descriptor on adapter path list */
	link_path(path);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_add_path done\n"));

	return ((void *)path);
}

/*
 * Remove path.
 */
void
rsmrdt_remove_path(rsm_node_id_t loc_nodeid, int loc_adapterid,
	rsm_node_id_t rem_nodeid, int rem_adapterid)
{
	path_t		*path;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_remove_path enter\n"));

	DBG_PRINTF((category, RSM_DEBUG,
		"rsmrdt_remove_path node loc(id=%x) %x rem(id=%x) %x\n",
		loc_adapterid, loc_nodeid, rem_adapterid, rem_nodeid));

	path = find_path(loc_nodeid, loc_adapterid, rem_nodeid, rem_adapterid);
	if (path == NULL) {
		DBG_PRINTF((category, RSM_ERR,
		    "rsmrdt_remove_path: path not found\n"));
		return;
	}

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt_remove_path: Found path = %lx\n", path));

	switch (path->state) {
	case RSMRDT_PATH_UP:
		path->state = RSMRDT_PATH_DOWN;
		mutex_exit(&path->mutex);
		break;
	case RSMRDT_PATH_DOWN:
		mutex_exit(&path->mutex);
		break;
	default:
		DBG_PRINTF((category, RSM_ERR,
		    "rsmrdt_remove_path: invalid path state %d\n",
		    path->state));
		mutex_exit(&path->mutex);
		return;
	}

	/*
	 * unlink from adapter path list and free path descriptor
	 */
	destroy_path(path);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_remove_path done\n"));
}

/*
 *
 */
boolean_t
rsmrdt_path_up(char *adapter_name, int adapter_instance,
    rsm_node_id_t remote_node, rsm_addr_t remote_hwaddr)
{
	path_t			*path;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_path_up enter\n"));

	path = lookup_path(adapter_name, adapter_instance,
	    remote_node, remote_hwaddr);

	if (path == NULL) {
		DBG_PRINTF((category, RSM_ERR,
			"rsmrdt_path_up: path lookup failed"));
		return (B_FALSE);
	}
	path->state = RSMRDT_PATH_UP;
	mutex_exit(&path->mutex);

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt_path_up: path = %lx\n", path));

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_path_up done\n"));

	return (B_TRUE);
}

/*
 *
 */
boolean_t
rsmrdt_path_down(rsm_node_id_t loc_nodeid, int loc_adapterid,
	rsm_node_id_t rem_nodeid, int rem_adapterid)
{
	path_t		*path;
	adapter_t	*loc_adapter;
	boolean_t	rval = B_TRUE;
	rsm_addr_t	hwaddr;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_path_down enter\n"));

	DBG_PRINTF((category, RSM_DEBUG,
		"rsmrdt_path_down node loc(id=%x) %x rem(id=%x) %x\n",
		loc_adapterid, loc_nodeid, rem_adapterid, rem_nodeid));

	path = find_path(loc_nodeid, loc_adapterid, rem_nodeid, rem_adapterid);
	if (path == NULL) {
		DBG_PRINTF((category, RSM_ERR,
		    "rsmrdt_path_down: path not found\n"));
		return (B_FALSE);
	}

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt_path_down: path = %lx\n", path));

	switch (path->state) {
	case RSMRDT_PATH_UP:
		path->state = RSMRDT_PATH_DOWN;
		loc_adapter = find_adapter(loc_adapterid);
		hwaddr = path->remote_hwaddr;
		mutex_exit(&path->mutex);
		rsmrdt_failover(loc_adapter, hwaddr);
		break;

	case RSMRDT_PATH_FAILED:
		path->state = RSMRDT_PATH_DOWN;
		mutex_exit(&path->mutex);
		break;
	case RSMRDT_PATH_DOWN:
		mutex_exit(&path->mutex);
		rval = B_FALSE;
		break;
	default:
		DBG_PRINTF((category, RSM_ERR,
		    "rsmrdt_path_down: invalid path state %d\n", path->state));
		mutex_exit(&path->mutex);
	}
	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_path_down done\n"));
	return (rval);
}

void
rsmrdt_path_fail(adapter_t *loc_adapter, rsm_node_id_t rem_nodeid,
	int rem_adapterid)
{
	path_t		*path;

	mutex_enter(&loc_adapter->listhead->mutex);
	path = loc_adapter->next_path;
	while (path != NULL) {
		if ((path->remote_node == rem_nodeid) &&
		    (path->remote_adapterid == rem_adapterid))
			break;
		else
			path = path->next_path;
	}
	if (path == NULL) {
		mutex_exit(&loc_adapter->listhead->mutex);
		return;
	}
	mutex_enter(&path->mutex);

	switch (path->state) {
	case RSMRDT_PATH_UP:
		path->state = RSMRDT_PATH_FAILED;
		break;
	case RSMRDT_PATH_DOWN:
		break;
	default:
		break;
	}
	mutex_exit(&path->mutex);
	mutex_exit(&loc_adapter->listhead->mutex);
}

/*
 *
 */
boolean_t
rsmrdt_node_alive(rsm_node_id_t remote_node)
{
	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_node_alive enter\n"));

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt_node_alive: remote_node = %x\n", remote_node));

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_node_alive done\n"));

	return (B_TRUE);
}


/*
 *
 */
boolean_t
rsmrdt_node_died(rsm_node_id_t remote_node)
{
	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_node_died enter\n"));

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt_node_died: remote_node = %x\n", remote_node));

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "rsmrdt_node_died done\n"));

	return (B_TRUE);
}

/*
 * Treat like path_down for all paths for the specified remote node.
 * Always invoked before node died.
 *
 * NOTE: This routine is not called from the cluster path interface; the
 * rsmrdt_path_down is called directly for each path.
 */
void
rsmrdt_disconnect_node(rsm_node_id_t remote_node)
{
	adapter_t		*adapter;
	adapter_listhead_t	*listhead;
	path_t			*path;
	rsm_addr_t		hwaddr;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_disconnect_node enter\n"));

	DBG_PRINTF((category, RSM_DEBUG,
	    "rsmrdt_disconnect_node: node = %x\n", remote_node));

	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	listhead = rsmrdt_adp_listhead_base.next;
	/*
	 * Scan through the list and mark all the paths, that are connected
	 * to remote node (remote_node), down.
	 */
	while (listhead != NULL) {
		mutex_enter(&listhead->mutex);
		adapter = listhead->next_adapter;
		while (adapter != NULL) {
			path = adapter->next_path;
			while (path != NULL) {
				if (path->remote_node == remote_node) {
					DBG_PRINTF((category, RSM_DEBUG,
					"Path down:adp 0x%lx path 0x%lx\n",
					adapter, path));
					/*
					 * Mark this path down. This is pretty
					 * much what rsmrdt_path_down() does.
					 */
					mutex_enter(&path->mutex);
					switch (path->state) {
					case RSMRDT_PATH_UP:
						path->state = RSMRDT_PATH_DOWN;
						hwaddr = path->remote_hwaddr;
						mutex_exit(&path->mutex);
						rsmrdt_failover(adapter,
							hwaddr);
						break;
					case RSMRDT_PATH_FAILED:
						path->state = RSMRDT_PATH_DOWN;
						mutex_exit(&path->mutex);
						break;
					case RSMRDT_PATH_DOWN:
						mutex_exit(&path->mutex);
						break;
					default:
						DBG_PRINTF((category, RSM_ERR,
						"Invalid path state %d\n",
						path->state));
						mutex_exit(&path->mutex);
					}
				}
				path = path->next_path;
			}
			adapter = adapter->next;
		}
		mutex_exit(&listhead->mutex);
		listhead = listhead->next_listhead;
	}
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_disconnect_node done\n"));
}

/*
 * Check for open handles before this module is unloaded
 */
int
rsmrdt_can_unload(void) {
	return (rsmrdt_check_openhandles());
}

/*
 * When this module unloaded, it does all the cleanup work
 */
void
rsmrdt_purge_adapterpathlist(void) {
	adapter_listhead_t 	*prev_listhead, *listhead;
	adapter_t 		*adapter;
	path_t 			*prev_path, *cur_path;
	char 			*name;
	int 			instance;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_purge_pathlist : enter \n"));

	listhead = rsmrdt_adp_listhead_base.next;
	while (listhead != NULL) {
		name = listhead->adapter_devname;
		adapter = listhead->next_adapter;
		while (adapter != NULL) {
			instance = adapter->instance;
			cur_path = adapter->next_path;
			while (cur_path != NULL) {
				DBG_PRINTF((category, RSM_DEBUG,
				"Removing:name(%d) %s adp 0x%lx path 0x%lx\n",
				instance, name, adapter, cur_path));
				/*
				 * unlink from adapter path list and free
				 * path descriptor
				 */
				prev_path = cur_path;
				cur_path = cur_path->next_path;
				destroy_path(prev_path);
			}
			/*
			 * Remove adapter
			 */
			adapter = adapter->next;
			(void) rsmrdt_remove_adapter(name, instance);
		}
		/*
		 * remove listhead
		 */
		prev_listhead = listhead;
		listhead = listhead->next_listhead;
		DBG_PRINTF((category, RSM_DEBUG,
			"rsmrdt_purge: listhead 0x%lx", prev_listhead));
		mutex_destroy(&prev_listhead->mutex);
		kmem_free(prev_listhead, sizeof (adapter_listhead_t));
	}

	/*
	 * Reset other global things
	 */
	rsmrdt_adp_listhead_base.next = NULL;

	/* Rest of the Cleanup */
	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_purge_pathlist : exit \n"));
}

/*
 *
 * ADAPTER UTILITY FUNCTIONS
 *
 */



/*
 * Allocate new adapter list head structure and add it to the beginning of
 * the list of adapter list heads.  There is one list for each adapter
 * device name (or type).
 */
static adapter_listhead_t *
init_listhead(char *name)
{
	adapter_listhead_t *listhead;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "init_listhead enter\n"));

	/* allocation and initialization */
	listhead = kmem_zalloc(sizeof (adapter_listhead_t), KM_SLEEP);
	DBG_PRINTF((category, RSM_DEBUG, "init_listhead : listhead 0x%lx\n",
		listhead));
	mutex_init(&listhead->mutex, NULL, MUTEX_DEFAULT, NULL);
	(void) strcpy(listhead->adapter_devname, name);

	/* link into list of listheads */
	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	if (rsmrdt_adp_listhead_base.next == NULL) {
		rsmrdt_adp_listhead_base.next = listhead;
		listhead->next_listhead = NULL;
	} else {
		listhead->next_listhead = rsmrdt_adp_listhead_base.next;
		rsmrdt_adp_listhead_base.next = listhead;
	}
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "init_listhead done\n"));

	return (listhead);
}


/*
 * Search the list of adapter list heads for a match on name.
 *
 */
static adapter_listhead_t *
lookup_adapter_listhead(char *name)
{
	adapter_listhead_t *listhead;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "lookup_adapter_listhead enter\n"));

	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	listhead = rsmrdt_adp_listhead_base.next;
	while (listhead != NULL) {
		if (strcmp(name, listhead->adapter_devname) == 0)
			break;
		listhead = listhead->next_listhead;
	}
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "lookup_adapter_listhead done\n"));

	return (listhead);
}

/*
 * select_adapter would start using round-robin for adapter selection
 * if retry_cnt exceeds this value.
 */
static int rsmrdt_rr_thresh = 3;

/*
 * Get the adapter list head and search for an adapter descriptor connected
 * to remote node. If successful, return the adapter else NULL.
 */
adapter_t *
rsmrdt_select_adapter(rsm_node_id_t remote_nodeid, int retry_cnt)
{
	adapter_listhead_t *prev_listhead, *listhead;
	adapter_t *adapter = NULL;
	path_t *path = NULL;
	adapter_t *sel_adapter = NULL;
	int adapter_cnt = 0;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_select_adapter enter\n"));

	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	listhead = rsmrdt_adp_listhead_base.next;
	while (listhead != NULL) {
		mutex_enter(&listhead->mutex);
		adapter = listhead->next_adapter;
		while (adapter != NULL) {
			adapter_cnt++;
			path = adapter->next_path;
			while (path != NULL) {
				if ((path->remote_node == remote_nodeid) &&
				    (path->state == RSMRDT_PATH_UP)) {
					if (opsrsmdev->opsrsm_param.
					    rsmrdt_enable_loadbalance) {
						if (sel_adapter == NULL) {
							sel_adapter = adapter;
						} else if (retry_cnt >
						    rsmrdt_rr_thresh) {
							if ((retry_cnt %
							    adapter_cnt) == 0) {
								sel_adapter =
								    adapter;
							}
						} else {
							if (adapter->sel_cnt <
							    sel_adapter->
							    sel_cnt) {
								sel_adapter =
								    adapter;
							}
						}
					} else {
						mutex_exit(&listhead->mutex);
						mutex_exit(
						    &rsmrdt_adp_listhead_base.
						    listlock);
						DBG_PRINTF((category, RSM_DEBUG,
						"Sel adp(%d) 0x%lx path 0x%lx",
						adapter->instance,
						adapter, path));
						DBG_PRINTF((category, RSM_DEBUG,
						"\n"));
						return (adapter);
					}
				}
				path = path->next_path;
			}

			adapter = adapter->next;
		}
		prev_listhead = listhead;
		listhead = listhead->next_listhead;
		mutex_exit(&prev_listhead->mutex);
	}
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_select_adapter done\n"));

	if (opsrsmdev->opsrsm_param.rsmrdt_enable_loadbalance) {
		if (sel_adapter != NULL) {
			sel_adapter->sel_cnt++;
			DBG_PRINTF((category, RSM_DEBUG,
			    "Selected adp(%d) 0x%lx\n", sel_adapter->instance,
			    sel_adapter));
			return (sel_adapter);
		}
	}

	return (NULL);
}

/*
 * Get the adapter list head corresponding to devname and search for
 * an adapter descriptor with a match on the instance number. If
 * successful, return the descriptor pointer to the caller.
 *
 */
static adapter_t *
rsmrdt_lookup_adapter(char *devname, int instance)
{
	adapter_listhead_t *listhead;
	adapter_t *current = NULL;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_lookup_adapter enter\n"));

	listhead = lookup_adapter_listhead(devname);
	if (listhead != NULL) {
		mutex_enter(&listhead->mutex);
		current = listhead->next_adapter;
		while (current != NULL) {
			if (current->instance == instance) {
				break;
			} else
				current = current->next;
		}
		mutex_exit(&listhead->mutex);
	}

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_lookup_adapter done\n"));

	return (current);
}


/*
 * Search for an adapter descriptor with a match on adapter id. If
 * successful, return the descriptor pointer to the caller.
 *
 */
static adapter_t *
find_adapter(int adapterid)
{
	adapter_listhead_t *listhead;
	adapter_t *current = NULL;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
		"find_adapter enter\n"));

	listhead = rsmrdt_adp_listhead_base.next;
	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	while (listhead != NULL) {
		mutex_enter(&listhead->mutex);
		current = listhead->next_adapter;
		while (current != NULL) {
			if (current->adapterid == adapterid) {
				mutex_exit(&listhead->mutex);
				mutex_exit(&rsmrdt_adp_listhead_base.listlock);
				return (current);
			} else
				current = current->next;
		}
		mutex_exit(&listhead->mutex);
		listhead = listhead->next_listhead;
	}
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "find_adapter done\n"));

	return (NULL);
}


void
apply_on_all_adapters(void (*func)(adapter_t *, void *), void *arg)
{
	adapter_listhead_t *listhead;
	adapter_t *current = NULL;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
		"apply_on_all_adapters enter\n"));

	listhead = rsmrdt_adp_listhead_base.next;
	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	while (listhead != NULL) {
		mutex_enter(&listhead->mutex);
		current = listhead->next_adapter;
		while (current != NULL) {
			(*func)(current, arg);
			current = current->next;
		}
		mutex_exit(&listhead->mutex);
		listhead = listhead->next_listhead;
	}
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "apply_on_all_adapters done\n"));
}

/*
 * Singly linked list. Add to the front.
 */
static void
link_adapter(adapter_t *adapter)
{
	adapter_listhead_t *listhead;
	adapter_t *current;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "link_adapter enter\n"));

	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	mutex_enter(&adapter->listhead->mutex);

	listhead = adapter->listhead;
	current = listhead->next_adapter;
	listhead->next_adapter = adapter;
	adapter->next = current;

	adapter->listhead->adapter_count++;
	DBG_PRINTF((category, RSM_DEBUG,
		"link_adapter : adapter count %d\n",
		adapter->listhead->adapter_count));

	mutex_exit(&adapter->listhead->mutex);
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "link_adapter done\n"));
}


/*
 * lookup_adapter_listhead returns with the the list of adapter listheads.
 */
static void
init_adapter(adapter_t *adapter, char *name, int instance, rsm_addr_t hwaddr,
	int adapterid)
{
	adapter_listhead_t *listhead;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "init_adapter enter\n"));

	adapter->instance = instance;
	adapter->adapterid = adapterid;
	adapter->hwaddr = hwaddr;
	adapter->sel_cnt = 0;

	mutex_init(&adapter->mutex, NULL, MUTEX_DEFAULT, NULL);

	/*
	 * Init per adapter resources used by rsmrdt driver
	 */
	(void) rsmrdt_adapterinit(adapter);

	listhead = lookup_adapter_listhead(name);
	if (listhead == NULL)  {
		listhead = init_listhead(name);
	}

	adapter->listhead = listhead;
	link_adapter(adapter);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "init_adapter done\n"));
}

/*
 *
 * PATH UTILITY FUNCTIONS
 *
 */

/*
 * Search the per adapter path list for a match on remote node and
 * remote adapter id. Returns a locked path
 *
 */
static path_t *
find_path(rsm_node_id_t loc_node, int loc_adapterid,
	rsm_node_id_t rem_nodeid, int rem_adapterid)
{
	path_t		*path;
	adapter_t	*adapter;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "find_path enter\n"));

	if (loc_node != rsmrdt_my_nodeid) {
		DBG_PRINTF((category, RSM_ERR,
		    "find_path: Invalid local nodeid\n"));
		return (NULL);
	}

	adapter = find_adapter(loc_adapterid);
	if (adapter == NULL) {
		DBG_PRINTF((category, RSM_ERR,
		    "find_path: adapter not found\n"));
		return (NULL);
	}

	mutex_enter(&adapter->listhead->mutex);
	/* start at the list head */

	path = adapter->next_path;
	while (path != NULL) {
		if ((path->remote_node == rem_nodeid) &&
		    (path->remote_adapterid == rem_adapterid)) {
			DBG_PRINTF((category, RSM_DEBUG,
			    "find_path:Found adp(id=%x) 0x%lx path 0x%lx\n",
			    adapter->adapterid, adapter, path));
			break;
		} else {
			path = path->next_path;
		}
	}

	if (path != NULL) {
		mutex_enter(&path->mutex);
	}

	mutex_exit(&adapter->listhead->mutex);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "find_path done\n"));

	return (path);
}

/*
 * Search the per adapter path list for a match on remote node and
 * hwaddr. Return a locked path.
 *
 */
static path_t *
lookup_path(char *adapter_devname, int adapter_instance,
    rsm_node_id_t remote_node, rsm_addr_t hwaddr)
{
	path_t		*current;
	adapter_t	*adapter;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "lookup_path enter\n"));

	adapter = rsmrdt_lookup_adapter(adapter_devname, adapter_instance);
	if (adapter == NULL) {
		DBG_PRINTF((category, RSM_ERR,
		"lookup_path: adapter lookup failed\n"));
		return (NULL);
	}

	mutex_enter(&adapter->listhead->mutex);

	/* start at the list head */
	current = adapter->next_path;

	while (current != NULL) {
		if ((current->remote_node == remote_node) &&
		    (current->remote_hwaddr == hwaddr))
			break;
		else
			current = current->next_path;
	}

	if (current != NULL) {
		mutex_enter(&current->mutex);
	}

	mutex_exit(&adapter->listhead->mutex);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "lookup_path done\n"));

	return (current);
}

/*
 * Add the path to the head of the (per adapter) list of paths
 */
static void
link_path(path_t *path)
{
	path_t *first_path;
	adapter_t *adapter = path->local_adapter;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "link_path enter\n"));

	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	mutex_enter(&adapter->listhead->mutex);

	first_path = adapter->next_path;
	adapter->next_path = path;
	path->next_path = first_path;

	adapter->listhead->path_count++;

	DBG_PRINTF((category, RSM_DEBUG, "link_path cnt %d\n",
		adapter->listhead->path_count));

	mutex_exit(&adapter->listhead->mutex);
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "link_path done\n"));
}

/*
 * Search the per-adapter list of paths for the specified path, beginning
 * at the head of the list.  Unlink the path and free the descriptor
 * memory.
 */
static void
destroy_path(path_t *path)
{
	path_t *prev, *current;
	adapter_t *adapter = path->local_adapter;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "destroy_path enter\n"));

	mutex_enter(&rsmrdt_adp_listhead_base.listlock);
	mutex_enter(&path->local_adapter->listhead->mutex);

	/* start at the list head */
	prev = NULL;
	current =  adapter->next_path;

	while (current != NULL) {
		if (path->remote_node == current->remote_node &&
		    path->remote_hwaddr == current->remote_hwaddr)
			break;
		else {
			prev = current;
			current = current->next_path;
		}
	}

	if (prev == NULL)
		adapter->next_path = current->next_path;
	else
		prev->next_path = current->next_path;

	path->local_adapter->listhead->path_count--;

	DBG_PRINTF((category, RSM_DEBUG, "destroy_path cnt %d\n",
		adapter->listhead->path_count));

	mutex_exit(&path->local_adapter->listhead->mutex);
	mutex_exit(&rsmrdt_adp_listhead_base.listlock);

	mutex_destroy(&current->mutex);

	DBG_PRINTF((category, RSM_DEBUG,
		"destroy_path: current 0x%lx\n", current));

	kmem_free(current, sizeof (path_t));

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE, "destroy_path done\n"));
}

/*
 * Called by rsmrdt_connect which needs the hardware address of the
 * remote adapter.  A search is done through the paths for the local
 * adapter for a match on the specified remote node.
 */
rsm_node_id_t
rsmrdt_get_remote_nodeid(adapter_t *adapter, rsm_addr_t remote_hwaddr)
{
	rsm_node_id_t remote_node = (rsm_node_id_t)-1;
	path_t	   *current = adapter->next_path;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_get_remote_nodeid enter\n"));

	mutex_enter(&adapter->listhead->mutex);
	while (current != NULL) {
		if (current->remote_hwaddr == remote_hwaddr) {
			remote_node = current->remote_node;
			break;
		}
		current = current->next_path;
	}

	if (current == NULL)
		remote_node = (rsm_node_id_t)-1;

	mutex_exit(&adapter->listhead->mutex);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
	    "rsmrdt_get_remote_nodeid done\n"));

	return (remote_node);
}

/*
 * Called by rsmrdt_connect which needs the hardware address of the
 * remote adapter.  A search is done through the paths for the local
 * adapter for a match on the specified remote node.
 */
rsm_addr_t
rsmrdt_get_remote_hwaddr(adapter_t *adapter, rsm_node_id_t remote_node)
{

	rsm_addr_t remote_hwaddr = (rsm_addr_t)-1;
	path_t	   *current = adapter->next_path;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
		"rsmrdt_get_remote_hwaddr enter\n"));

	mutex_enter(&adapter->listhead->mutex);
	while (current != NULL) {
		if (current->remote_node == remote_node) {
			remote_hwaddr = current->remote_hwaddr;
			DBG_PRINTF((category, RSM_DEBUG,
				"rsmrdt_get_remote_hwaddr:path "
				"0x%lx hwaddr 0x%llx\n",
				current, remote_hwaddr));
			break;
		}
		current = current->next_path;
	}
	if (current == NULL)
		remote_hwaddr = (rsm_addr_t)-1;
	mutex_exit(&adapter->listhead->mutex);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
		"rsmrdt_get_remote_hwaddr done\n"));

	return (remote_hwaddr);
}

/*
 * Find the remote node adapter id given local adapter and remote
 * node h/w address. A search is done through the paths for the local
 * adapter for a match on the specified remote node.
 */
void
rsmrdt_get_remote_ids(adapter_t *adapter, rsm_addr_t rsm_addr,
	int *adapterid, int *nodeid)
{
	path_t	*current = adapter->next_path;

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
		"rsmrdt_get_remote_ids enter\n"));

	mutex_enter(&adapter->listhead->mutex);
	while (current != NULL) {
		if (current->remote_hwaddr == rsm_addr) {
			*adapterid = current->remote_adapterid;
			*nodeid = (int)current->remote_node;
			DBG_PRINTF((category, RSM_DEBUG,
			    "rsmrdt_get_remote_ids: path 0x%lx aid 0x%x "
			    "nid %d\n", current, *adapterid, *nodeid));
			break;
		}
		current = current->next_path;
	}
	if (current == NULL) {
		*adapterid = -1;
		*nodeid = -1;
	}
	mutex_exit(&adapter->listhead->mutex);

	DBG_PRINTF((category, RSM_DEBUG_VERBOSE,
		"rsmrdt_get_remote_ids done\n"));
}
