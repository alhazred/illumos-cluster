/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RSMRDT_IF_H_
#define	_RSMRDT_IF_H_

#pragma ident	"@(#)rsmrdt_if.h	1.16	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <rdtapi.h>

#define	OPSRSM_MAXVECS	16

/*
 * Define the ioctls that this driver supports
 */

#define	RSMRDT_IOCTL		('O' << 8)
#define	RSMRDT_IOCTL_BIND	(RSMRDT_IOCTL|1) /* bind to local endpoint */
#define	RSMRDT_IOCTL_CONNECT	(RSMRDT_IOCTL|2) /* bind to remote endpoint */
#define	RSMRDT_IOCTL_SENDMSG	(RSMRDT_IOCTL|3) /* send msg to rem endpoint */
#define	RSMRDT_IOCTL_RECVMSGS	(RSMRDT_IOCTL|4) /* receive messages */
#define	RSMRDT_IOCTL_GETPARAM	(RSMRDT_IOCTL|5) /* set per-endpoint params */
#define	RSMRDT_IOCTL_SETPARAM	(RSMRDT_IOCTL|6) /* set per-endpoint params */
#define	RSMRDT_IOCTL_GETNODEID	(RSMRDT_IOCTL|7) /* get local nodeid */

/*
 * Define the arguments that each ioctl expects and the values that
 * 	it returns.
 * 	-1 is returned if called with unsupported ioctl code and
 *	errno will be set to ENOTTY
 */

/*
 * RSMRDT_IOCTL_CONNECT - initiate a connection.
 * 	The local communication endpoint will simply remember and will use
 * 	the address specified here as the target address for all the future
 * 	outgoing messages.
 * 	It will not verify validity of the remote 'portnum'. It's up to the
 * 	applications to make sure the remote endpoint exists.
 *
 *    Calling:
 * 	rsmrdt_connect_arg_t - struct that contains destination endpoint
 *	node-id and port number.
 *
 *    Returns:
 *	-1 on error, errno is set:
 * 		EINVAL  - Invalid argument.
 *		EBADF	- hdl is not a valid handle.
 *		EINVAL	- Invalid argument (nodeid < 0).
 *		EISCONN	- Destination is already connected.
 *		ENETDOWN- Connection to destination failed (all the paths
 *			  are down).
 *		ENOMEM	- Not enough memory.
 *		ENOTSUP	- Fail to connect, possibly due to version mismatch
 *			  of related software components.
 *	0 on success
 */

typedef struct {
	int		nodeid;  /* remote node id */
	uint32_t	portnum; /* remote port number */
} rsmrdt_connect_arg_t;


/*
 * RSMRDT_IOCTL_SENDMSG - send a message to the remote endpoint identified
 *	with RSMRDT_IOCTL_CONNECT ioctl.
 *	It is non-blocking and deals with one message at a time. It maintains
 *	message boundary, and guarantees either the whole message, or none of
 *	it is delivered to the destination successfully.
 *
 *    Calling:
 * 	rsmrdt_send_arg_t - struct that contains a pointer to data storage
 *		structure for I/O using uio and number of iovecs.
 *
 *    Returns:
 *	-1 on error, errno is set:
 *		EBADF		- hdl is not a valid handle.
 *		EFAULT		- Unable to copy in the arguments due to bad
 *				  address.
 *		EINVAL		- Invalid argument.
 *		EIO		- A non-recoverable I/O error is encountered.
 *		EMSGSIZE	- Message size is larger than the maximum msgs
 *				  size supported by the interconnect.
 *		ENOMEM		- Not enough memory.
 *		ENOTCONN	- The handle is not in a connected state.
 *		ESRCH		- Destination endpoint doesn't exist.
 *		EWOULDBLOCK	- sender is flow controlled.
 *	0 on success
 */

typedef struct {
	iovec_t		*iov;    /* pointer to array of iovecs */
	uint_t		iovcnt;  /* number of iovecs */
} rsmrdt_send_arg_t;

#if defined(_SYSCALL32)

/* Kernel's view of user ILP32 rsmrdt_send_arg_t struct */

typedef struct {
	uint32_t	iov;	/* ptr (4b) to array of iovecs (iovec32_t) */
	uint_t  	iovcnt;	/* number of iovecs */
} rsmrdt_send_arg32_t;

#endif  /* _SYSCALL32 */

/*
 * RSMRDT_IOCTL_RECVMSGS - receive messages.
 *	If a message is too long to fit in the supplied buffer, excessive
 *	bytes will be discarded.
 *
 *	In case of memory allocation errors, it will not drop the packets.
 *
 *    Calling:
 * 	rsmrdt_recvmsgs_arg_t - struct that contains a pointer to array of
 *		'rsmrdt_recvmsg_t' structure and length of message array.
 *		On return, 'msgcnt' will contain number of messages
 *		retrieved and 'bytes_recvd' will contain size of the message.
 *
 *    Returns:
 *	-1 on error, errno is set:
 *		EBADF	- hdl is not a valid handle.
 *		EFAULT	- Unable to copy in the arguments or copy out the
 *			  data due to bad address.
 *		EINTR	- The call is interrupted.
 *		EINVAL	- Invalid argument.
 *		ENOMEM	- Not enough memory.
 *		EWOULDBLOCK - No msg has arrived & the call is non-blocking.
 *	0 on success
 */

typedef struct {
	rdt_recvmsg_t 	*msg_iov; /* pointer to array of messages */
	uint_t		msgcnt;   /* IN/OUT; number of messages */
	int		timeout;  /* timeout in milliseconds */
} rsmrdt_recvmsgs_arg_t;


#if defined(_SYSCALL32)

/* Kernel's view of user ILP32 struct */

typedef struct {
	uint32_t  	iov;    /* ptr (4b) to array of iovecs (iovec32_t) */
	uint_t		iovcnt;		/* number of iovecs */
	uint32_t  	bytes_recvd;	/* number of bytes received */
	uint32_t	msglen;		/* original length of the msg */
	int		nodeid;		/* sender's nodeid */
	uint32_t	portnum;	/* sender's portnum */
} rsmrdt_recvmsg32_t;

typedef struct {
	uint32_t  	msg_iov; /* ptr (4b) to array rsmrdt_recvmsg32_t */
	uint_t  	msgcnt;  /* IN/OUT; number of messages */
	int   		timeout; /* timeout in milliseconds */
} rsmrdt_recvmsgs_arg32_t;

#endif  /* _SYSCALL32 */

/*
 * RSMRDT_IOCTL_GETPARAM, RSMRDT_IOCTL_SETPARAM - get/set parameters.
 *
 *    Calling:
 * 	rsmrdt_getsetparam_arg_t - struct that contains the command,
 *	pointer to get/set value and its size.
 *
 *    Returns:
 *	-1 on error, errno is set:
 *		EBADF	- hdl is not a valid handle.
 *		EFAULT	- Unable to copy out the data due to bad address.
 *		EINVAL	- Invalid argument.
 *		ENOMEM	- Not enough memory.
 *	0 on success
 */

typedef struct {
	uint32_t	cmd;	/* command */
	void *		value;  /* pointer to value */
	size_t		size; 	/* size of the value */
} rsmrdt_getsetparam_arg_t;

#if defined(_SYSCALL32)

/* Kernel's view of user ILP32 rsmrdt_getsetparam_arg_t struct */

typedef struct {
	uint32_t	cmd;	/* command */
	uint32_t	value;	/* pointer to value */
	int32_t		size;	/* size of the value */
} rsmrdt_getsetparam_arg32_t;

#endif  /* _SYSCALL32 */


#ifdef __cplusplus
}
#endif

#endif	/* _RSMRDT_IF_H_ */
