/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RSM_RSMRDT_PATH_INT_H
#define	_RSM_RSMRDT_PATH_INT_H

#pragma ident	"@(#)rsmrdt_path_int.h	1.20	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

#include <sys/rsm/rsm_common.h>
#include <sys/rsm/rsm.h>
#include <sys/rsm/rsmpi.h>

#include "rsmrdt.h"

/* Path (path_t) States */
#define	RSMRDT_PATH_DOWN	1
#define	RSMRDT_PATH_UP		2
#define	RSMRDT_PATH_FAILED	3

#define	RSMRDT_INTR_T_BASE	(0xB0)

#define	RSM_MAX_DESTADDR	1024

typedef struct path {
	struct path		*next_path;
	rsm_node_id_t		remote_node;
	int			remote_devinst;
	int			remote_adapterid;
	rsm_addr_t		remote_hwaddr;
	int			state;
	kmutex_t		mutex;
	struct adapter		*local_adapter;
} path_t;

typedef struct adapter {
	struct adapter		*next;
	struct adapter_listhead *listhead;
	kmutex_t		mutex;
	int			instance;
	int			adapterid;
	rsm_addr_t		hwaddr;
	path_t			*next_path;
	rsm_controller_attr_t	rsmrdt_attr;
	rsm_intr_t		rsmrdt_service_id;
	rsm_controller_object_t rsmrdt_ctlr_obj;
	struct opsrsm_dest 	*opsrsm_desttbl[RSM_MAX_DESTADDR];
	int			opsrsm_numdest; /* No. of entries in desttbl */
	kmutex_t		opsrsm_dest_lock; /* protect dest table */
	taskq_t			*opsrsm_taskq;
	struct opsrsm_dest	*opsrsm_runq;	/* service routine run queue */
	kmutex_t 		opsrsm_runq_lock;
					/* protects opsrsm_runq, among others */
	kcondvar_t 		opsrsm_uninit_cv;
	uint_t			sel_cnt;
} adapter_t;


/*
 * There is one adapter_listhead for each adapter devname. This
 * adapter_listhead stores the number of adapters belonging to
 * it. It also stores the number of paths for all the adapters
 * belonging to it.
 */
typedef struct adapter_listhead {
	struct adapter_listhead	*next_listhead;
	char			adapter_devname[MAXNAMELEN];
	adapter_t		*next_adapter;
	int			ref_cnt;
	kmutex_t		mutex;
	int			adapter_count;
	int			path_count;
} adapter_listhead_t;


struct adapter_listhead_list {
	adapter_listhead_t	*next;
	kmutex_t		listlock;
};


#ifdef	__cplusplus
}
#endif

#endif	/* _RSM_RSMRDT_PATH_INT_H */
