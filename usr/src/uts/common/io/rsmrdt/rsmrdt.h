/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RSMRDT_H_
#define	_RSMRDT_H_

#pragma ident	"@(#)rsmrdt.h	1.55	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/rsm/rsmpi.h>
#include "rsmrdt_if.h"
#include "rsmrdt_path_int.h"

/*
 * This driver is only supported on sparc systems, so there is no
 * need to worry about byte ordering issues.
 */
#define	OPSRSM_VERSION	(1)

/*
 * Segment ID range reserved for RDT
 */
#define	RSMRDT_SEGID_BASE	0x600000
#define	RSMRDT_SEGID_END	0x6fffff

/*
 * static limits
 */

#define	RSM_DLPI_QPRI	8	/* XXX what is the priority range?? */
#define	RSM_DLPI_QDEPTH 1024

#define	RSM_DLPI_QFLAGS	(RSM_INTR_SEND_Q_NO_FENCE)
#define	RSM_DLPI_SQFLAGS (RSM_INTR_SEND_QUEUE | RSM_INTR_SEND_POLL)
#define	RSM_DLPI_SCI_SQFLAGS (RSM_INTR_SEND_QUEUE | RSM_INTR_SEND_POLL)

#ifdef	_KERNEL

#define	RSMRDT_UNPUBLISH_TRY	300

#define	OPSRSM_CACHELINE_SIZE	(0x40)
#define	OPSRSM_CACHELINE_SHIFT	6
#define	OPSRSM_CACHELINE_OFFSET	(OPSRSM_CACHELINE_SIZE - 1)
#define	OPSRSM_CACHELINE_MASK	(~OPSRSM_CACHELINE_OFFSET)
#define	OPSRSM_CACHELINE_ROUNDUP(b) \
	(((uint64_t)(b) + OPSRSM_CACHELINE_OFFSET) & OPSRSM_CACHELINE_MASK)

/*
 * Definitions for module_info.
 */

#define	OPSRSMNAME	"rsmrdt"	/* module name */

#define	OPSRSM_DRIVER_MINOR 0

/*
 * Driver parameters, from .conf file
 */

struct opsrsm_param {
	/* Size of packet buffers (must be multiple of 64 bytes) */
	uint_t opsrsm_buffer_size;

	/* Mask of base ID bits in IP address */
	uint_t opsrsm_netmask;

	/* Number of packet buffers exported to each communicating peer */
	ushort_t opsrsm_buffers;

	/* Size of communications queues (must be at least opsrsm_buffers) */
	ushort_t opsrsm_queue_size;

	/* Number of buffers which won't be loaned upstream */
	ushort_t opsrsm_buffers_retained;

	/* Maximum # of queue packets per destination */
	ushort_t opsrsm_max_queued_pkts;

	/* Initial message timeout interval */
	int opsrsm_msg_init_tmo;

	/* Maximum message timeout interval */
	int opsrsm_msg_max_tmo;

	/* Time after which we drop connection instead of doing msg timeout */
	uint_t opsrsm_msg_drop_tmo;

	/* Acknowledgment timeout interval */
	int opsrsm_ack_tmo;

	/* Queue element sync timeout interval */
	uint_t opsrsm_sync_tmo;

	/* Number of free buffers to try and batch up in one transmission. */
	ushort_t opsrsm_fqe_sync_size;

	uint_t opsrsm_retry_limit;
	int	opsrsm_retry_delay;
	uint_t opsrsm_xmit_delay;
	uint_t opsrsm_coalesce_time;
	uint_t opsrsm_data_threshold;
	uint_t opsrsm_max_recv_msgs;
	ushort_t opsrsm_adaptive_intr;
	ushort_t opsrsm_adaptive_rate;
	ushort_t rsmrdt_enable_loadbalance;
	uint_t opsrsm_mem_hi_wat;
	uint_t opsrsm_mem_lo_wat;
	uint_t opsrsm_recv_hi_wat;
	uint_t opsrsm_recv_lo_wat;
	uint_t opsrsm_flow_tmo_int;
	uint_t opsrsm_max_loopback_pkts;
};

/*
 * Defaults and limits for parameters
 */

#define	OPSRSM_BUFFERS_DFLT		1024
#define	OPSRSM_BUFFER_SIZE_DFLT		4096
#define	OPSRSM_QUEUE_SIZE_DFLT		1025
#define	OPSRSM_BUFFERS_RETAINED_DFLT	1024
#define	OPSRSM_MAX_QUEUED_PKTS_DFLT	16384
#define	OPSRSM_MSG_INIT_TMO_DFLT	1
#define	OPSRSM_MSG_MAX_TMO_DFLT		128
#define	OPSRSM_MSG_DROP_TMO_DFLT	3000
#define	OPSRSM_ACK_TMO_DFLT		3000
#define	OPSRSM_SYNC_TMO_DFLT		3
#define	OPSRSM_FQE_SYNC_SIZE_DFLT	16
#define	OPSRSM_RETRY_LIMIT_DFLT		1000
#define	OPSRSM_RETRY_DELAY_DFLT		3
#define	OPSRSM_XMIT_DELAY_DFLT		1
#define	OPSRSM_DATA_THRESHOLD_DFLT	131072
#define	OPSRSM_MAX_RECV_MSGS_DFLT	40
#define	OPSRSM_ADAPTIVE_INTR_DFLT	1
#define	OPSRSM_ADAPTIVE_RATE_DFLT	15
#define	OPSRSM_MEM_HI_WAT_DFLT		25165824
#define	OPSRSM_MEM_LO_WAT_DFLT		16777216
#define	OPSRSM_RECV_HI_WAT_DFLT		67108864
#define	OPSRSM_RECV_LO_WAT_DFLT		50331648
#define	OPSRSM_FLOW_TMO_INT_DFLT	10
#define	OPSRSM_MAX_LOOPBACK_PKTS_DFLT	4096
#define	RSMRDT_ENABLE_LOADBALANCE_DFLT	1

#define	OPSRSM_MAX_BUFFER_SIZE_DFLT	65536
/*
 * Per-Device instance state information.
 *
 * Each instance is dynamically allocated on first attach.
 */
typedef struct opsrsm {
	dev_info_t	*opsrsm_dip;	/* dev info */

	/* Counters to keep stats for netstat support */
	uint64_t opsrsm_ipackets;	/* # packets received */
	uint32_t opsrsm_ierrors;	/* # total input errors */
	uint64_t opsrsm_opackets;	/* # packets sent */
	uint32_t opsrsm_oerrors;	/* # total output errors */
	uint32_t opsrsm_collisions;	/* # collisions (FQE waits) */

	uint32_t opsrsm_in_bytes;	/* # bytes input (32 bit) */
	uint32_t opsrsm_in_bytes64;	/* # bytes input (64 bit) */
	uint64_t opsrsm_out_bytes;	/* # bytes output (32 bit) */
	uint64_t opsrsm_out_bytes64;	/* # bytes output (64 bit) */

	/* Other counters, for internal use */
	uint32_t opsrsm_xfers;		/* # calls to opsrsm_xmit */
	uint32_t opsrsm_xfer_pkts;	/* # pkts sent out by xmit */
	uint32_t opsrsm_syncdqes;	/* # syncdqe-ints sent out by  xmit */
	uint32_t opsrsm_lbufs;		/* # times we loaned bufs */
	uint32_t opsrsm_nlbufs;		/* # times we had to alloc buf */
	uint32_t opsrsm_pullup;		/* # times we had to coalesce pkts */
	uint32_t opsrsm_pullup_fail;	/* # times we couldn't pullup */
	uint32_t opsrsm_starts;		/* # calls to opsrsmstart */
	uint32_t opsrsm_start_xfers;	/* # calls to opsrsmxfer from start */
	uint32_t opsrsm_fqetmo_hint;	/* # times fqe tmo ended by hint */
	uint32_t opsrsm_fqetmo_drops;	/* # pkts dropped by fqetmo */
	uint32_t opsrsm_no_fqes;	/* # times we run out of fqes */
	uint32_t opsrsm_pending_writes; /* # of concurrent writes */
	uint32_t opsrsm_pkts_queued;	/* # of pkts enqueued onto sendq */
	uint32_t opsrsm_pkts_discarded; /* # of pkts discarded */
	uint32_t opsrsm_pkts_pending;	/* # of pkts enqueued onto pendq */
	uint32_t opsrsm_last_sendq_len; /* # of pkts on sendq (for debug) */
	uint32_t opsrsm_last_pendq_len; /* # of pkts on pendq (for debug) */
	uint32_t opsrsm_last_wr_comp;	/* # of times blocked on flow ctl */
	uint32_t opsrsm_errs;		/* # errors on transfers */
	uint32_t opsrsm_intr_send_errs; /* # errors on interrupt send */

	struct opsrsm_param 	opsrsm_param;	/* parameters */
	struct kstat		*opsrsm_ksp;	/* our kstats */

	uint32_t		opsrsm_max_batch_size;
	uint32_t		opsrsm_min_batch_size;
	uint32_t		opsrsm_put_fqes;
	uint32_t		opsrsm_queued_fqes;
	uint32_t		opsrsm_packets_consumed;
} opsrsm_t;

/* Attach progress bitmask */
#define	OPSRSM_ATT_MINOR		0x01
#define	OPSRSM_ATT_KSTAT		0x02

#define	OPSRSM_ATT_ALL	\
	(OPSRSM_ATT_MINOR | OPSRSM_ATT_KSTAT)

/*
 * Export some of the error counters via the kstats mechanism.
 */
typedef struct opsrsm_stat {
	struct kstat_named rsm_ipackets;
	struct kstat_named rsm_ipackets64;
	struct kstat_named rsm_ierrors;
	struct kstat_named rsm_opackets;
	struct kstat_named rsm_opackets64;
	struct kstat_named rsm_oerrors;
	struct kstat_named rsm_collisions;
	struct kstat_named rsm_xfers;
	struct kstat_named rsm_xfer_pkts;
	struct kstat_named rsm_syncdqes;
	struct kstat_named rsm_lbufs;
	struct kstat_named rsm_nlbufs;
	struct kstat_named rsm_pullup;
	struct kstat_named rsm_pullup_fail;
	struct kstat_named rsm_starts;
	struct kstat_named rsm_start_xfers;
	struct kstat_named rsm_fqetmo_hint;
	struct kstat_named rsm_fqetmo_drops;
	struct kstat_named rsm_no_fqes;
	struct kstat_named rsm_pending_writes;
	struct kstat_named rsm_pkts_queued;
	struct kstat_named rsm_pkts_discarded;
	struct kstat_named rsm_pkts_pending;
	struct kstat_named rsm_last_sendq_len;
	struct kstat_named rsm_last_pendq_len;
	struct kstat_named rsm_last_wr_comp;
	struct kstat_named rsm_errs;
	struct kstat_named rsm_in_bytes;
	struct kstat_named rsm_in_bytes64;
	struct kstat_named rsm_out_bytes;
	struct kstat_named rsm_out_bytes64;
	struct kstat_named rsm_intr_send_errs;
	struct kstat_named rsm_max_batch_size;
	struct kstat_named rsm_min_batch_size;
	struct kstat_named rsm_put_fqes;
	struct kstat_named rsm_queued_fqes;
	struct kstat_named rsm_packets_consumed;
} opsrsm_stat_t;

/*
 * On some specific RSM interconnects, if there is a data delivery problem with
 * one of the 32 byte halves of a 64 byte write to the remote node, the remote
 * side writes all 0's to that 32 byte region of memory.  We guarantee that the
 * 4 byte fqe entries and 8 byte byte dqe entries (described below) are aligned
 * in a way that guarantees that each fits within a single 32 byte region, so
 * checking for any non-zero value within the entry is sufficient to guarantee
 * that the write was successful.
 *
 * We use the seqnum as the write validity check, which means it must never
 * be 0.  A non-0 value ensures that the remote write was successful.
 */

/*
 * Delivery Queue Entry, used to denote buffers containing new packets.
 */
#define	OPSRSM_DQE_SEQ_MASK	0xFF	/* All 1's sequence */
typedef union opsrsm_dqe {
	uint64_t align;		/* single 8-byte: sun4u */
	struct opsrsm_dqe_s {	/* actual structure */
		ushort_t dq_length;	/* True length of packet */
		ushort_t dq_sap;	/* Packet's SAP */
		uchar_t	dq_seqnum;	/* Sequence number - validity check */
		uchar_t	dq_offset;	/* Packet offset within buffer */
		ushort_t dq_bufnum;	/* Buffer number */
	} s;
} opsrsm_dqe_t;

/*
 * Free Queue Entry, used to denote buffers which are available to be filled.
 */
#define	OPSRSM_FQE_SEQ_MASK	0xFF	/* All 1's sequence */
typedef union opsrsm_fqe {
	uint32_t align;
	struct opsrsm_fqe_s {
		uchar_t	fq_seqnum;	/* Sequence number - validity check */
		uchar_t	fq_filler;	/* Unused */
		ushort_t fq_bufnum;	/* Buffer number */
	} s;
} opsrsm_fqe_t;

#define	OPSRSM_FLOW_CTL_SZ	128
typedef struct opsrsm_flow_ctl {
	uint32_t fc_stop;
} opsrsm_flow_ctl_t;

/*
 * Header for the data transfer segment.
 */
typedef struct opsrsm_xfer_hdr {
	size_t		rx_segsize;	/* size of segment */
	uint32_t	rx_cookie;	/* magic cookie */
	uint32_t	rx_bufsize;	/* size of buffers */
	ushort_t	rx_numbufs;	/* number of buffers */
	ushort_t	rx_numfqes;	/* number of elements in free queue */
	ushort_t	rx_numdqes;	/* num of elements in delivery queue */
	uint32_t	rx_buf_offset;	/* offset to start of buffers */
	uint32_t	rx_fq_offset;	/* offset to start of free queue */
	uint32_t	rx_dq_offset;	/* offset to start of delivery queue */
	uint32_t	rx_skey;
} opsrsm_xfer_hdr_t;

#define	OPSRSM_XFER_COOKIE	0x58664572	/* 'XfEr' */

/*
 * Structure describing a loaned-up buffer
 */

typedef struct opsrsmbuf {
	frtn_t	rb_frtn;		/* Pointer to our free routine */
	int	rb_bufnum;		/* Number of loaned buffer */
	struct opsrsm_dest *rb_rd;	/* Destination buffer belongs to */
} opsrsmbuf_t;

/*
 * OPSRSM message types
 */

#define	OPSRSM_MSG_REQ_CONNECT		1
#define	OPSRSM_MSG_CON_ACCEPT		2
#define	OPSRSM_MSG_CON_ACK		3
#define	OPSRSM_MSG_SYNC_DQE		4
#define	OPSRSM_MSG_RESET		5
#define	OPSRSM_MSG_HEARTBEAT		6
#define	OPSRSM_MSG_FINFO_DEMUX_DONE	7
#define	OPSRSM_MSG_FINFO_REPLY		8
#define	OPSRSM_MSG_FINFO_REXMIT_ACK	9
#define	RSMRDT_MSG_SEND_ERR		100
#define	OPSRSM_REXMIT			127
#define	OPSRSM_OPT_REXMIT_END		9

/*
 *
 *          R S M D   C O N N E C T I O N   P R O T O C O L
 *
 *
 * The connection protocol for the RSM DLPI driver is a follows:
 *
 * INITIATOR                              RESPONDER
 *
 * 1  Send RSDM_REQ_CONNECT
 *         Includes xfer segment ID
 *
 * 2                                      Send OPSRSM_CON_ACCEPT
 *                                        Includes xfer segment ID
 *
 * 3  Send OPSRSM_CON_ACK
 *
 * If an OPSRSM_REQ_CONNECT message is received while an
 * OPSRSM_REQ_CONNECT is outstanding to the same node ID:  if the
 * node receiving the duplicate OPSRSM_REQ_CONNECT has a higher
 * numbered ID, it will accept the connection.  The lower numbered
 * node will reject the duplicate.
 *
 * The special message type OPSRSM_REXMIT causes us to retransmit the
 * last message we sent (unsuccessfully or successfully), without
 * incrementing the sequence number on the message.  This is used when
 * we get a timeout waiting for a response to an OPSRSMM_REQ_CONNECT
 * request and want to resend it.
 *
 */

typedef struct opsrsm_msg_header {
	uint8_t opsrsm_version;	/* Increment when incompatible change made */
	uint8_t reqtype;	/* One of the above */
	uint16_t seqno;		/* Sequence number */
} opsrsm_msg_header_t;

typedef struct opsrsm_con_request {
	rsm_memseg_id_t send_segid; /* Segment you should use to talk to me */
} opsrsm_con_request_t;

typedef struct opsrsm_con_accept {
	rsm_memseg_id_t send_segid; /* Segment you should use to talk to me */
	rsm_memseg_id_t rcv_segid; /* Segment I use to talk to you */
} opsrsm_con_accept_t;

typedef struct opsrsm_con_ack {
	rsm_memseg_id_t send_segid; /* Segment you should use to talk to me */
	rsm_memseg_id_t rcv_segid; /* Segment I use to talk to you */
} opsrsm_con_ack_t;

typedef struct opsrsm_syncdqe {
	rsm_memseg_id_t rcv_segid; /* Segment I use to talk to you */
	uint32_t msg_cnt;
} opsrsm_syncdqe_t;

typedef struct opsrsm_finfoquery {
	uint32_t skey;
} opsrsm_finfoquery_t;

typedef struct rsmrdt_senderr {
	uint32_t sender_portnum;
	uint32_t errstate;
	uint32_t sender_pkey;
} rsmrdt_senderr_t;


typedef union opsrsm_msg {
	uint64_t align;
	struct {
		opsrsm_msg_header_t hdr;
		union {
			opsrsm_con_request_t	con_request;
			opsrsm_con_accept_t	con_accept;
			opsrsm_con_ack_t	con_ack;
			opsrsm_syncdqe_t	syncdqe;
			opsrsm_finfoquery_t	finfoquery;
			rsmrdt_senderr_t	senderr;
		} m;
	} p;
} opsrsm_msg_t;

typedef struct opsrsm_queue {
	mblk_t *q_head;
	mblk_t *q_tail;
	int q_len;
} opsrsm_queue_t;


typedef struct opsrsm_queued_msg {
	opsrsm_msg_t			qm_msg;
	int				qm_retries;
	struct opsrsm_queued_msg	*qm_next;
} opsrsm_queued_msg_t;

#define	OPSRSM_FQR_LOCKED	0x0001
typedef struct opsrsm_queued_fqe {
	int				qf_bufnum;
	struct opsrsm_queued_fqe	*qf_next;
} opsrsm_queued_fqe_t;

#define	OPSRSM_EVT_READY	0x0001
#define	OPSRSM_EVT_SYNC_FQE	0x0002
#define	OPSRSM_EVT_SYNC_DQE	0x0004
#define	OPSRSM_EVT_SEND_MSG	0x0008
#define	OPSRSM_EVT_STOP		0x0010
#define	OPSRSM_EVT_DONE		0x0020

/*
 * Structure describing someone else communicating with us (a destination)
 */
typedef struct opsrsm_resource opsrsmresource_t;
typedef struct opsrsm_dest {
	/* Basics */
	rsm_addr_t 	rd_rsm_addr;	/* Address of destination */
	struct adapter	*rd_adapter;	/* Pointer to adapter structure */
	int		rd_rem_adapterid; /* Adapter id of destination */

	/* Interrupt queue */
	rsm_send_q_handle_t rsm_sendq;
	opsrsm_msg_t 	rsm_previous_msg;
	int 		rsm_previous_msg_valid;

	/* Packet queue */
	mblk_t	*rd_queue_h,	/* queue of packets waiting to go out */
		*rd_queue_t;
	ushort_t rd_queue_len;	/* number of packets on above queue */


	/* Local transfer segment */
	caddr_t 			rd_rawmem_base_addr;
	size_t 				rd_rawmem_base_size;
	rsm_memory_local_t 		rd_memory;
	rsm_memseg_id_t 		rd_lxfersegid;
	rsm_memseg_export_handle_t 	rd_lxferhand;


	/* Remote transfer segment */
	opsrsm_xfer_hdr_t		rd_rxferhdr;
	int 				rd_rxferhdr_valid;
	off_t 				rd_rbufoff;
	boolean_t 			rd_segid_valid;
	rsm_memseg_id_t 		rd_rxfersegid;
	rsm_memseg_import_handle_t 	rd_rxferhand;
	uint16_t 			rd_lastconnmsg_seq;
	uint16_t			rd_lastdqemsg_seq;

	/*
	 * Free queue we're writing to (describing buffers on our node
	 * available to partner; lives on partner)
	 */
	off_t	rd_fqw_f_off;	/* First usable element in queue */
	ushort_t rd_fqw_seq;	/* Sequence number we will write next */
	ushort_t rd_num_fqws;	/* Number of usable elements in queue */


	/*
	 * Delivery queue that we're writing to (describing buffers on
	 * partner that we've filled with data; lives on partner)
	 */
	off_t	rd_dqw_f_off;	/* First usable element in queue */
	ushort_t rd_dqw_seq;	/* Sequence number we will write next */
	ushort_t rd_num_dqws;	/* Number of usable elements in queue */


	/* Buffers (on partner) that we're writing to */
	uint_t	rd_rbuflen;	/* Length of remote buffers */
	ushort_t rd_numrbuf;	/* Number of remote buffers */


	/*
	 * Free queue we're reading from (describing buffers on partner
	 * available to us; lives on our node)
	 */
	volatile opsrsm_fqe_t	/* Pointers to ... */
		*rd_fqr_f,	/* First usable element in queue */
		*rd_fqr_l,	/* Last usable element in queue */
		*rd_fqr_n;	/* Element we'll read next */
	ushort_t rd_fqr_seq;	/* Sequence number we expect to read next */
	ushort_t rd_num_fqrs;	/* Number of usable elements in queue */


	/*
	 * Delivery queue we're reading from (describing buffers on our
	 * node that the partner has filled with data; lives on our node)
	 */
	volatile opsrsm_dqe_t	/* Pointers to ... */
		*rd_dqr_f,	/* First usable element in queue */
		*rd_dqr_l,	/* Last usable element in queue */
		*rd_dqr_n;	/* Element we'll read next */
	ushort_t rd_dqr_seq;	/* Sequence number we expect to read next */
	ushort_t rd_num_dqrs;	/* Number of usable elements in queue */


	/* (Local) buffers we're reading from */
	volatile void *rd_lbuf;	/* Start of first local buffer */
	uint_t	rd_lbuflen;	/* Length of each local buffer */
	ushort_t rd_numlbufs;	/* Number of local buffers */
	opsrsmbuf_t *rd_bufbase; /* Local buffer description structures, */
				/*  for use in loaning buffers upward */


	/* Information on cached FQE's */
	ushort_t rd_cached_fqr_cnt;	/* number of cached fqe's */
	ushort_t *rd_cached_fqr;	/* buffer numbers from cached fqe's */


	/*
	 * Shadow free queue - local copy of free queue that lives on
	 * partner
	 */
	opsrsm_fqe_t		/* Pointers to ... */
		*rd_shdwfqw_f,	/* First usable element */
		*rd_shdwfqw_l,	/* Last usable element */
		*rd_shdwfqw_i,	/* Next element added to queue goes here */
		*rd_shdwfqw_o;	/* Next element transmitted comes from here */

	/*
	 * Shadow delivery queue - local copy of delivery queue that lives
	 * on partner
	 */
	opsrsm_dqe_t		/* Pointers to ... */
		*rd_shdwdqw_f,	/* First usable element */
		*rd_shdwdqw_l,	/* Last usable element */
		*rd_shdwdqw_i,	/* Next element added to queue goes here */
		*rd_shdwdqw_o;	/* Next element transmitted comes from here */

	ushort_t rd_shdwfqw_errflag;	/* If nonzero, we had an error last */
					/*  time we tried to write an FQE */
	ushort_t rd_shdwdqw_errflag;	/* If nonzero, we had an error last */
					/*  time we tried to write a DQE */
	ushort_t rd_stopq;		/* If nonzero, we shouldn't try to */
					/*  sync queues, the network is down */
	/* State information */
	ushort_t rd_state;	/* State (OPSRSM_STATE_xxx, see below) */
	ushort_t rd_estate;	/* Event State */
	ushort_t rd_sstate;	/* Segment state (bitmask of OPSRSM_RSMS_xxx) */
	ushort_t rd_dstate;	/* Delete state (0-2), != 0 means deleting */
	short	rd_refcnt;	/* Destination reference count */

	/* Command/sequence information */
	ushort_t rd_nseq;	/* Seq # we'll put on next message */
	uchar_t	rd_recvdack;	/* Nonzero if we've gotten a valid ACK */
	uchar_t	rd_sentconn;	/* Nonzero if we've sent a CONN */

	/* Last message transmitted, for rexmits if needed */
	opsrsm_msg_t rd_lastobmsg;	/* Last outbound message */

	/* Timeout information */
	timeout_id_t rd_tmo_id;	/* timeout ID for empty queue retry, etc. */
	int	rd_tmo_int;	/* backoff interval for timeout */
	int	rd_tmo_tot;	/* ticks we've waited so far this timeout */
	timeout_id_t	rd_sync_fqe_tmo_id;	/* timeout ID for fqesync */
	timeout_id_t	rd_sync_dqe_tmo_id;	/* timeout ID for dqesync */

	ushort_t rd_nlb;	/* number of outstanding loaned buffers */
	ushort_t rd_nlb_del;	/* if nonzero, we're being deleted, and are */
				/*  waiting for rd_nlb to go to 0 */

	kmutex_t rd_nlb_lock;	/* mutex to protect rd_nlb/rd_nlb_del */
	kmutex_t rd_lock;	/* mutex to protect this data structure */
	kmutex_t rd_net_lock;	/* mutex to protect segment data/pointers */
	kmutex_t rd_xmit_lock; 	/* mutex to protect xmit stuff */

	struct opsrsm_dest *rd_next;	/* ptrs for svc routine run queue */
	opsrsm_queue_t  rd_sendq;
	opsrsm_queue_t	rd_pendq;
	opsrsm_queue_t	rd_freeq;
	ushort_t	rd_xmit_state;
	rsm_barrier_t	rd_barrier;
	uint32_t	rd_start_time;
	uint32_t	rd_data_collected;
	uint32_t	rd_nretries;
	opsrsm_dqe_t	*rd_new_shdwdqw_o;
	ushort_t	rd_new_dqw_seq;
	timeout_id_t	rd_xmit_tmo_id;
	int		rd_xmit_tmo_int;
	timeout_id_t	rd_fqe_tmo_id;
	int		rd_fqe_tmo_int;
	kmutex_t	rd_tmo_lock;
	kmutex_t	rd_sendq_lock;
	pollhead_t	rd_pollhd;
	int		rd_events;
	int		rd_writes_completed;
	int		rd_active_threads;
	uint32_t	rd_adaptive_threshold;
	uint32_t	rd_last_sent;
	uint32_t	rd_pkt_freq;
	uint32_t	rd_next_lseqno;
	uint32_t	rd_next_rseqno;
	uint32_t	rd_local_skey;
	uint32_t	rd_remote_skey;
	uint32_t	rd_unpublish_errs;
	kcondvar_t	rd_conn_cv;
	kmutex_t	rd_freeq_lock;
	int		rd_nodeid;
	boolean_t	rd_retry_int;
	boolean_t	rd_freeq_freeze;
	boolean_t	rd_freed;
	uint_t		rd_buffer_size; /* Maximum message size */
	off_t		rd_remote_flow_ctl;
	uint32_t	rd_remote_flow_stop;
	timeout_id_t	rd_flow_tmo_id;
	volatile opsrsm_flow_ctl_t *rd_flow_ctl;
	uint32_t	rd_pkts_delivered;
	taskq_t		*rd_evt_taskq;
	kmutex_t	rd_evt_lock;
	kcondvar_t	rd_evt_cv;
	kcondvar_t	rd_evt_wait_cv;
	uint32_t	rd_evt_flags;
	kmutex_t	rd_msgs_lock;
	kmutex_t	rd_fqr_lock;
	uint32_t	rd_fqr_flags;
	/*
	 * Supress lint Info(793)
	 * [c:40]: ANSI limit of 127 'structure members' exceeded
	 */
	/*lint -e793 */
	opsrsm_queued_msg_t	*rd_msgs;
	opsrsm_queued_msg_t	*rd_msgs_tail;
	opsrsm_queued_fqe_t	*rd_queued_fqe_list;
	opsrsm_queued_fqe_t	*rd_queued_fqe_tail;
	opsrsm_queued_fqe_t	*rd_queued_fqe_array;
	opsrsm_queued_fqe_t	*rd_queued_fqe_freelist;
	int			rd_queued_fqe_cnt;
	timeout_id_t	rd_status_tmo_id;
	/*lint +e793 */
} opsrsm_dest_t;

/*
 * Run queue:
 *
 * Certain operations on destinations are performed by the driver's write
 * service routine (opsrsm_wsrv).  In order to arrange for this, there is a
 * queue of destinations waiting to be processed by the service routine.
 * Each device's opsrsm_runq points to the head of this queue of destinations,
 * which are linked together via rd_next.  Whenever the service routine
 * runs, after it has served its usual purpose of processing messages from
 * the stream's service queue, it traverses its list of destinations and
 * performs appropriate operations on them, depending on their state.
 *
 * The rd_next pointer is protected by the runq_lock everywhere but in the
 * middle of the service routine.  Essentially, the service routine takes a
 * whole chain of destination entries off of the run queue at once (inside
 * the runq_lock), and then traverses the list (outside the runq_lock).  Since
 * a scheduled destination should never be given a new state except by the
 * service routine, there should be no conflicting updates to rd_next.
 *
 * Destination states:
 *
 * A scheduled state means the destination is on the run queue; an unscheduled
 * state means the destination is not.  State transitions are always from
 * scheduled to unscheduled or vice versa.
 *
 * A state with a name of the form OPSRSM_STATE_S_xxx is a scheduled state where
 * the service routine is going to do xxx next.  These states have odd numbers.
 *
 * A state with a name of the form OPSRSM_STATE_W_xxx is an unscheduled state
 * where we are waiting for xxx to happen.  These states have even numbers.
 */

#define	OPSRSM_STATE_NEW			0	/* Newly created */
#define	OPSRSM_STATE_INPROGRESS		1000	/* Being processed */
#define	OPSRSM_STATE_DELETING		2000	/* Being deleted */

#define	OPSRSM_STATE_W_SCONNTMO		2	/* Waiting for conn rxmit tmo */
#define	OPSRSM_STATE_W_ACCEPT		4	/* Waiting for accept msg */
#define	OPSRSM_STATE_W_ACK		6	/* Waiting for ack msg */
#define	OPSRSM_STATE_W_READY		8	/* Connected, wait for pkt */
#define	OPSRSM_STATE_W_FQE		10	/* Waiting for fqe to xmit */

#define	OPSRSM_STATE_S_REQ_CONNECT	1	/* Srv: send conn request */
#define	OPSRSM_STATE_S_NEWCONN		3	/* Srv: setup/accept new conn */
#define	OPSRSM_STATE_S_CONNXFER_ACCEPT	5	/* Srv: connxfer, then accept */
#define	OPSRSM_STATE_S_CONNXFER_ACK	7	/* Srv: connxfer, then ack */
#define	OPSRSM_STATE_S_XFER		9	/* Srv: xfer data */
#define	OPSRSM_STATE_S_DELETE		11	/* Srv: delete this dest */
#define	OPSRSM_STATE_S_SCONN		13	/* Srv: resend last conn */

#define	OPSRSM_SCHED_STATE(s)	((s) & 1)

#define	OPSRSM_STATE_STR(x) (						\
	(x == OPSRSM_STATE_NEW) ? "OPSRSM_STATE_NEW" :			\
	(x == OPSRSM_STATE_INPROGRESS) ? "OPSRSM_STATE_INPROGRESS" :	\
	(x == OPSRSM_STATE_DELETING) ? "OPSRSM_STATE_DELETING" :	\
	(x == OPSRSM_STATE_W_SCONNTMO) ? "OPSRSM_STATE_W_SCONNTMO" :	\
	(x == OPSRSM_STATE_W_ACCEPT) ? "OPSRSM_STATE_W_ACCEPT" :	\
	(x == OPSRSM_STATE_W_ACK) ? "OPSRSM_STATE_W_ACK" :		\
	(x == OPSRSM_STATE_W_READY) ? "OPSRSM_STATE_W_READY" :		\
	(x == OPSRSM_STATE_W_FQE) ? "OPSRSM_STATE_W_FQE" :		\
	(x == OPSRSM_STATE_S_REQ_CONNECT) ? "OPSRSM_STATE_S_REQ_CONNECT" :\
	(x == OPSRSM_STATE_S_NEWCONN) ? "OPSRSM_STATE_S_NEWCONN" :	\
	(x == OPSRSM_STATE_S_CONNXFER_ACCEPT) ? \
	    "OPSRSM_STATE_S_CONNXFER_ACCEPT" : \
	(x == OPSRSM_STATE_S_CONNXFER_ACK) ? "OPSRSM_STATE_S_CONNXFER_ACK" : \
	(x == OPSRSM_STATE_S_XFER) ? "OPSRSM_STATE_S_XFER" :		\
	(x == OPSRSM_STATE_S_DELETE) ? "OPSRSM_STATE_S_DELETE" :	\
	(x == OPSRSM_STATE_S_SCONN) ? "OPSRSM_STATE_S_SCONN" :		\
	"unknown")

#define	OPSRSM_XMIT_UNINITIALIZED	10
#define	OPSRSM_XMIT_DISCONNECTED	20
#define	OPSRSM_XMIT_BARRIER_CLOSED	100
#define	OPSRSM_XMIT_BARRIER_OPENED	200
#define	OPSRSM_XMIT_RETRY_DATA		300


/*
 * RSM driver state - basically, what segments we've created/connected.  We
 * keep a bitmask of the ones we've done, so that when we delete a
 * destination we don't try and undo something we never did.  Also, we
 * sometimes check to make sure rd_sstate is OPSRSM_RSMS_ALL before trying to
 * perform an operation on a destination, to ensure we don't get ahead of
 * our initialization.
 */

#define	OPSRSM_RSMS_LXFER_C	0x01	/* Create local xfer segment */
#define	OPSRSM_RSMS_LXFER_P	0x02	/* Publish local xfer segment */
#define	OPSRSM_RSMS_RXFER_S	0x04	/* Create send queue to remote node */
#define	OPSRSM_RSMS_RXFER_C	0x10	/* Connect to remote xfer */

#define	OPSRSM_RSMS_ALL	\
	(OPSRSM_RSMS_LXFER_C | OPSRSM_RSMS_LXFER_P | OPSRSM_RSMS_RXFER_S | \
	OPSRSM_RSMS_RXFER_C)

/* resource state flags */
#define	OPSRSM_RS_POLL		0x0001
#define	OPSRSM_RS_BOUND		0x0002
#define	OPSRSM_RS_SIG		0x0004
#define	OPSRSM_RS_CONNECTING	0x0008
#define	OPSRSM_RS_REFDEST	0x0010
#define	OPSRSM_RS_FAILOVER	0x0040
#define	OPSRSM_RS_NORECVR	0x0100
#define	OPSRSM_RS_PKEYMISMATCH	0x0200

/* resource option flags */
#define	OPSRSM_RO_INCREFCNT	0x0004
#define	OPSRSM_RO_DEFAULT	0x0000

/*
 * Per fd resource structure
 */
struct opsrsm_resource {
	minor_t			rs_num;		/* (minor) number */
	kmutex_t 		rs_lock;
	opsrsm_dest_t		*rs_dest;
	pollhead_t		rs_pollhd;	/* poll head struct */
	uint32_t		rs_events;	/* bit map of occured events */
	int 			rs_nodeid;
	uint32_t 		rs_lportnum;
	uint32_t 		rs_rportnum;
	uint32_t		rs_state;
	int			rs_refcnt;
	int			rs_poll_index;
	opsrsm_queue_t		rs_recvq;
	kcondvar_t		rs_cv;
	kcondvar_t		rs_conn_cv;
	kcondvar_t		rs_close_cv;
	void 			*rs_rmptr;
	uint32_t		rs_pkey;
	uint32_t		rs_local_skey;
};

#define	OPSRSMRC_BLKSZ		16

typedef struct {
	int			opsrsmrcblk_avail;
	opsrsmresource_t	*opsrsmrcblk_blks[OPSRSMRC_BLKSZ];
} opsrsmresource_blk_t;

#define		OPSRSMRC_UNLOAD_INPROGRESS	0x01

struct opsrsmresource_table {
	krwlock_t	opsrsmrct_lock;
	int		opsrsmrc_len;
	int		opsrsmrc_sz;
	int		opsrsmrc_cnt;
	ushort_t	opsrsmrc_flag;
	opsrsmresource_blk_t **opsrsmrc_root;
};

/* The following header will be added at the beginning of the message */
typedef struct opsrsm_message_header {
	uint32_t	lportnum; /* senders port number */
	uint32_t	rportnum; /* remote port number */
	uint32_t	msg_sz;   /* message size in bytes */
	int		nodeid;   /* sender's nodeid */
	uint32_t	pkey;	  /* protection key */
	uint32_t	skey;	  /* sequence number key */
	uint32_t	seqno;    /* sequence number */
	uint32_t	option;
} opsrsm_message_header_t;

typedef struct opsrsm_failover_info {
	int		fi_nodeid;
	uint32_t	fi_remote_skey;
	uint32_t	fi_local_skey;
	uint32_t	fi_next_rseqno;
	int		fi_retval;
	uint32_t	fi_waiters;
	kcondvar_t	fi_cv;
	opsrsm_queue_t	fi_rexmitq;
	opsrsm_dest_t	*fi_dest;
	uint32_t	fi_status;
	clock_t		fi_last_accessed;
	kmutex_t	fi_lock;
	kcondvar_t	fi_wait_cv;
	struct opsrsm_failover_info *fi_next;
} opsrsm_failover_info_t;

/* fi_status flags */
#define	OPSRSM_FINFO_DEMUX_DONE		0x01
#define	OPSRSM_FINFO_REXMIT_DONE	0x02
#define	OPSRSM_FINFO_REXMIT_ACKED	0x04
#define	OPSRSM_FINFO_OLD_PROTO		0x08

typedef struct opsrsm_finfo_msg {
	opsrsm_dest_t	*fm_dest;
	uint32_t	fm_retries;
	uint32_t	fm_skey;
	uint8_t		fm_type;
} opsrsm_finfo_msg_t;

typedef struct opsrsm_ack_msg {
	uint32_t	am_skey;
} opsrsm_ack_msg_t;

typedef struct rsmrdt_errmsg {
	rsm_addr_t	err_rsm_addr;	/* Address of destination */
	struct adapter	*err_adapter;	/* Pointer to adapter structure */
	opsrsm_msg_t 	err_msg;
} rsmrdt_errmsg_t;

/* Macros for opsrsm message and buffer pointers */

#define	OPSRSM_MESSAGE_HDRSZ		sizeof (opsrsm_message_header_t)
#define	OPSRSM_MESSAGE_HDRPTR(mp)	((opsrsm_message_header_t *)mp->b_rptr)

#endif	/* _KERNEL */

#ifdef __cplusplus
}
#endif

#endif	/* _RSMRDT_H_ */
