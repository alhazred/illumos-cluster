/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SYS_DIDDEV_H
#define	_SYS_DIDDEV_H

#pragma ident	"@(#)diddev.h	1.22	08/05/20 SMI"

#include <sys/dditypes.h>
#include <sys/param.h>
#include <sys/dklabel.h>
#include <sys/open.h>

#ifdef __cplusplus
extern "C" {
#endif


/*
 * Dynamic MultiPathing
 * Device dependent software definitions.
 */

/*
 * Device names
 */
#define	DID_NAME	"did"
#define	DID_ADMINMINOR	 1
#define	DID_ADMINROOT	 0

#define	DID_EXCL	1
#define	DID_OPEN	1

/*
 * Maximum number of units (determined by minor device byte)
 */
#define	DID_MAXUNIT		128

#define	MAXDEVIDLEN		256

/*
 * Admin minor node value - from instance in .conf file
 */
#define	DID_ADMININSTANCE	0


#define	DIDUNIT_SHIFT	0x5
#define	DIDPART_MASK	0x1f
#define	DIDUNIT(dev)	(getminor((dev)) >> DIDUNIT_SHIFT)
#define	DIDPART(dev)	(getminor((dev)) &  DIDPART_MASK)
#define	GET_DEVT(path, mydev)	path->device[DIDPART(getminor(mydev))]


/*
 * Admin instance data structure: a linked list of mulitpathed disks. This
 * data structure is built during did initialization by the admin instance
 * in reponse to DID_INIT IOCTL calls.  Once complete the information it
 * contains is supplied to each instance as they attach
 */

typedef struct did_subpath {
	char	device_path[MAXPATHLEN];
	dev_t	device[DID_MAX_MINORS];
	int	state;
	int	error_count;	/* number of errors in buffer */
	int	first_error_index; /* circular buffer start */
	diderr_t errors;	/* a circular buffer w/ last 10 errors */
	iodidperf_t	    statistics;	/* io statistics */
	struct  did_subpath *next;
	struct 	did_subpath *prev;
} did_subpath_t;



/*
 * Used by admin to when building device list
 */
typedef struct did_device_list {
	ddi_devid_t		target_device_id;
	int			instance;
	int			type_num;
	char			node_name[MAXNAMELEN];
	did_subpath_t		*subpath_listp;
	struct did_device_list *next;
	struct did_device_list *prev;
} did_device_list_t;

typedef struct did_device_type_list {
	int				dev_num;
	char				dev_name[MAXPATHLEN];
	int				minor_count;
	char				minor_name[DID_MAX_MINORS][MAXNAMELEN];
	int				minor_type[DID_MAX_MINORS];
	minor_t				minor_number[DID_MAX_MINORS];
	int				default_nodes;
	int				start_default_nodes;
	int				next_default_node;
	int				last_default_node;
	int				cur_max_instance;
	struct did_device_type_list	*next;
} did_device_type_list_t;

/*
 * Entries in each instance's pending command list, one per outstanding command
 */

typedef struct did_pending_command {
	struct buf		*buf_ptr; 	/* origional io buf */
	struct buf		*client_buf_ptr; /* buf were are using */
	time_t			start_time;	/* command start time */
	int			partition;	/* disk partition */
	did_subpath_t		*subpath_ptr;	/* command sent via */
	struct did_pending_command	*next;
	struct did_pending_command	*prev;
} did_pending_command_t;


typedef struct did_ocinfo {
	uint_t lyr_open[DID_MAX_MINORS];	/* count of layered opens */
	uint_t reg_open[OTYPCNT - 1];	/* flags for non-layered device opens */
} did_ocinfo_t;



/* Software per-instance data structure, one per target device id */

typedef struct didstate {
	kmutex_t	mutex;
	did_ocinfo_t    un_ocmap;	/* open partition map */
	int		flags;		/* open() flags */
					/* linked list of pending commands */
	ddi_devid_t	target_device_id; /* device id of disk/esi device */
	int		subpath_count;	/* number of subpaths in list */
	did_subpath_t	*current_path;
	did_subpath_t	*subpath_listp; /* linked list of subpaths */
	int		pending_command_count[DID_MAXUNIT];
				/* number of commands pending per subpath */
	did_pending_command_t	*pendcmd_list_headp;
	did_pending_command_t	*pendcmd_list_tailp;
} didstate_t;


#ifdef __cplusplus
}
#endif

#endif	/* !_SYS_DIDDEV_H */
