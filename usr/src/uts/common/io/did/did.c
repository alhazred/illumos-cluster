/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)did.c	1.77	08/05/20 SMI"

/*
 * dynamic multipathing pseudo-driver
 */

#include <sys/types.h>
#include <sys/param.h>
#include <sys/buf.h>
#include <sys/conf.h>
#include <sys/proc.h>
#include <sys/user.h>
#include <sys/cred.h>
#include <sys/file.h>
#include <sys/open.h>
#include <sys/poll.h>
#include <sys/errno.h>
#include <sys/cmn_err.h>
#include <sys/kmem.h>
#include <sys/uio.h>
#include <sys/vnode.h>
#include <sys/pathname.h>
#include <sys/modctl.h>
#include <sys/stat.h>
#include <sys/dkio.h>
#include <sys/ddi.h>
#include <sys/sunddi.h>
#include <sys/sol_version.h>
#if (SOL_VERSION >= __s10)
#include <sys/esunddi.h>
#endif
#include <sys/debug.h>
#include <sys/ddi_impldefs.h>
#include <sys/autoconf.h>
#include <sys/sysmacros.h>
#include <sys/didio.h>
#include <sys/sol_version.h>
#include "diddev.h"

/* suppress lint warnings could be declared as pointing to const */
/*lint -e818 */

#if (SOL_VERSION >= __s9)
#define	DID_MULTI_INSTANCE
#endif

#ifdef DID_MULTI_INSTANCE
static dev_info_t *did_devinfos[DID_MAX_INSTANCE + 1];
#else
static dev_info_t *admin_dip = NULL;
#endif

#ifdef DEBUG
/*
 * DEBUG is defined when compiling SolarisCluster in debug mode. Debug messages
 * are * logged in a circular kernel debug buffer named did_dbg (mdb command is
 * did_dbg/s). By default, nothing is logged (debug level mask is 0). Debug
 * level can be set in /etc/system with set did:diddebug=0x(a mask value).
 * Can be useful when dtrace is not available (Solaris 8, 9). Log levels are:
 * 	0x1: open-close debug
 * 	0x2: attach-detach debug
 * 	0x4: ioctl debug
 * 	0x8: read/write debug (high impact on performances)
 */
#include <sys/varargs.h>
static void did_log(char *, ...);
#define	DIDDPRINTF1(n)	if (diddebug & 0x1) did_log n	/* open-close */
#define	DIDDPRINTF2(n)	if (diddebug & 0x2) did_log n	/* attach-detach */
#define	DIDDPRINTF4(n)	if (diddebug & 0x4) did_log n	/* ioctl */
#define	DIDDPRINTF8(n)	if (diddebug & 0x8) did_log n	/* read-write */
#define	DID_DBG_MSG_SIZE	1024
#define	DID_DBG_LOG_SIZE	(DID_DBG_MSG_SIZE * 256)
static kmutex_t diddbglock;		/* one message at time */
static char did_str[DID_DBG_MSG_SIZE];	/* The string to log */
static char did_dbg[DID_DBG_LOG_SIZE];	/* The log buffer */
static size_t diddbgnext = 0;
static int diddbglock_init = 0;
int	diddebug = 0;
int	diddebug_cleanup = 0;
#else
#define	DIDDPRINTF1(n)
#define	DIDDPRINTF2(n)
#define	DIDDPRINTF4(n)
#define	DIDDPRINTF8(n)
#endif


/*
 * config stuff
 */
#define	DID_LONGNAME	"Disk ID Driver 1.77 "
#define	DID_U2STR_LEN	40

static int didunloadflag = 0;

static int didopen(dev_t *devp, int flag, int otyp, cred_t *credp);
static int didclose(dev_t dev, int flag, int otyp, cred_t *credp);
static int didstrategy(struct buf *bp);
static int didprint(dev_t dev, char *str);
static int didread(dev_t dev, struct uio *uiop, cred_t *credp);
static int didwrite(dev_t dev, struct uio *uiop, cred_t *credp);
static int didaread(dev_t dev, struct aio_req *aio, cred_t *cred_p);
static int didawrite(dev_t dev, struct aio_req *aio, cred_t *cred_p);

static int didprop_op(dev_t dev, dev_info_t *dip, ddi_prop_op_t prop_op,
		int flags, char *name, caddr_t valuep, int *lengthp);
static int did_adminioctl(int cmd, intptr_t arg, int mode,
		cred_t *credp, int *rvalp);
static int didioctl(dev_t dev, int cmd, intptr_t arg, int mode,
		cred_t *credp, int *rvalp);

static int didinfo(dev_info_t *dip, ddi_info_cmd_t cmd, void *arg,
		    void **result);
static int didattach(dev_info_t *dip, ddi_attach_cmd_t cmd);
static int diddetach(dev_info_t *dip, ddi_detach_cmd_t cmd);

static int did_admin_attach(dev_info_t *dip, ddi_attach_cmd_t cmd);
static int did_admin_open(int flag, int otyp, cred_t *credp);
static int did_admin_close(dev_t dev, int flag, int otyp, cred_t *credp);

static int did_open(dev_t *devp, int flag, int otyp, cred_t *credp);
static int did_close(dev_t dev, int flag, int otyp, cred_t *credp);

static void create_did_instance(did_device_list_t *devicep);
#ifndef DID_MULTI_INSTANCE
static int make_did_type_nodes(did_device_type_list_t *devtype);
#endif

static did_device_list_t *get_device_data_by_instance(int instance);
static did_device_list_t *get_device_data_by_deviceid(ddi_devid_t device_id);
static did_device_list_t *build_new_device_data_entry(did_init_t *datap,
    did_device_type_list_t *devtypep);

static void add_subpath(did_subpath_t *subpath_listp, did_init_t *datap);
static void cleanup_subpaths(did_subpath_t *subpath_listheadp,
    did_init_t *datap);

static char *did_unit_to_str(dev_t dev, char *str);
static char *did_open_flg_str(int otyp);

static int did_choose_path(didstate_t *softstatep);
static int get_device_path_count(int instance);
static int get_device_instance_count(void);
static int get_subpath_count(did_subpath_t *subpath_listheadp);

static did_device_type_list_t *find_dev_type(did_init_t *device_data);
#ifdef DID_MULTI_INSTANCE
static void did_update_devid_prop(dev_info_t *, int);
static did_device_type_list_t *get_dev_type(char *);
#endif
static did_device_type_list_t *build_dev_type(did_init_type_t *device_type);
static int did_new_devt_mappings(did_init_t *device_data, did_subpath_t *subp);
static int did_dev_is_open(didstate_t *softstatep);
static int did_subdev_is_open(didstate_t *softstatep, dev_t *devp);

#ifdef _MULTI_DATAMODEL
static int expand_did_init(did_init_t *device_data, did_init32_t *dd32);
static int shrink_did_init(did_init_t *device_data, did_init32_t *dd32);
static int expand_did_init_type(did_init_type_t *device_type, did_init_type32_t
	*dd32);
#endif
/* suppress lint warnings for nodev/nulldev entries */
/*lint -e64 */
static struct cb_ops	did_cb_ops = {
	didopen,		/* open */
	didclose,		/* close */
	didstrategy,		/* strategy */
	didprint,		/* print */
	nodev,			/* dump */	/*lint !e64 */
	didread,		/* read */
	didwrite,		/* write */
	didioctl,		/* ioctl */
	nodev,			/* devmap */	/*lint !e64 */
	nodev,			/* mmap */	/*lint !e64 */
	nodev,			/* segmap */	/*lint !e64 */
	nochpoll,		/* poll */
	didprop_op,		/* prop_op */
	0,			/* streamtab */
	D_64BIT | D_MP | D_NEW, /* Driver compatibility flag */
	CB_REV,		 /* cb_rev */
	didaread,		/* async I/O read entry point */
	didawrite		/* async I/O write entry point */

};


static struct dev_ops	did_ops = {
	DEVO_REV,		/* rev */
	0,			/* refcnt */
	didinfo,		/* info */
	nulldev,		/* identify */
	nulldev,		/* probe */
	didattach,		/* attach */
	diddetach,		/* detach */
	nulldev,		/* reset */
	&did_cb_ops,		/* cb_ops */
	(struct bus_ops *)0,	/* bus_ops */
	nulldev			/* power */
};
/*lint +e64 */

static struct modldrv	did_driver_info = {
	&mod_driverops,		/* modops */
	DID_LONGNAME,		/* name */
	&did_ops,		/* dev_ops */
};

static struct modlinkage did_linkage = {
	MODREV_1,			/* rev */
	{				/* linkage */
		&did_driver_info,
		NULL,
		NULL,
		NULL,
	},
};

/*
 * Linked list of did devices and they're subpaths build by admin
 */
static did_device_list_t *did_devicep = 0;
static kmutex_t		  did_mutex;

/*
 * Linked list of did device types, built by IOCDID_INITTYPE
 * This list only contains devices which are directly attached
 * to the local node.
 */
static did_device_type_list_t *did_devlistp = 0;


/*
 * Base pointer to ddi soft state structure
 */
static void *did_softstate_basep = NULL;

static int  did_max_instance = 0;

/*
 * virtual driver loader entry points
 */

int
_init()
{
	int	error;

	DIDDPRINTF2(("_init\n"));

	/* initialize soft state */
	if ((error = ddi_soft_state_init(&did_softstate_basep,
					sizeof (didstate_t), 1)) != 0) {
		cmn_err(CE_WARN, "did: _init: ddi_soft_state_init error %d\n",
			error);
		return (error);
	}

	/* install module, return success */
	if ((error = mod_install(&did_linkage)) != DDI_SUCCESS) {
		cmn_err(CE_WARN,
		    "did: _init: mod_install error %d\n", error);
		ddi_soft_state_fini(&did_softstate_basep);
		return (error);
	}

	mutex_init(&did_mutex, "did_private_mutex", MUTEX_DRIVER, NULL);
	did_max_instance = 0;

	return (DDI_SUCCESS);
}

int
_fini()
{
	int		error;

	DIDDPRINTF2(("_fini\n"));

	if (didunloadflag == 0)
		return (DDI_FAILURE);

	/* unload module */
	if ((error = mod_remove(&did_linkage)) != DDI_SUCCESS) {
		DIDDPRINTF2(("_fini mod_remove error %d\n", error));
		return (error);
	}

	ddi_soft_state_fini(&did_softstate_basep);
	DIDDPRINTF2(("_fini module removed\n"));
	return (DDI_SUCCESS);
}

int
_info(struct modinfo *modinfop)
{
	DIDDPRINTF4(("_info modinfop %p\n", modinfop));
	return (mod_info(&did_linkage, modinfop));
}



/*
 * Driver administration entry points
 */

static int
didattach(dev_info_t *dip, ddi_attach_cmd_t cmd)
{
	int			instance = ddi_get_instance(dip);
#ifdef DID_MULTI_INSTANCE
	int			error;
	did_device_list_t	*devicep;
	char			minorBuf[MAXPATHLEN];
	int			i;
	did_device_type_list_t	*devtype;
	char			*data;
#endif

	DIDDPRINTF2(("didattach(%d) cmd %x info %p\n",
	    instance, (int)cmd, dip));

	/*
	 * Initial instance of the driver (only one in the .conf file)
	 * used to setup all other instances via IOCTL calls
	 */
	if (instance == DID_ADMININSTANCE) {
#ifdef DID_MULTI_INSTANCE
		did_devinfos[DID_ADMININSTANCE] = dip;
#else
		admin_dip = dip;
#endif
		return (did_admin_attach(dip, cmd));
	}

#ifdef DID_MULTI_INSTANCE

	if (instance > DID_MAX_INSTANCE) {
		return (DDI_FAILURE);
	}

	did_devinfos[instance] = dip;
#if defined(DDI_NO_AUTODETACH)
	if ((error = ddi_prop_create(DDI_DEV_T_NONE, dip, DDI_PROP_CANSLEEP,
		DDI_NO_AUTODETACH, NULL, 0)) != DDI_PROP_SUCCESS) {
		cmn_err(CE_WARN,
		    "didattach: ddi_prop_create %s error %d\n",
		    DDI_NO_AUTODETACH, error);
		did_devinfos[instance] = NULL;
		return (DDI_FAILURE);
	}
#endif

	/*
	 * Create minors here
	 */
	if ((ddi_prop_lookup_string(DDI_DEV_T_ANY, dip, 0, "device-type", &data)
	    != DDI_PROP_SUCCESS)) {
		cmn_err(CE_WARN, "didattach: inst %d - "
		    "\"device-type\" property lookup failed\n", instance);
		did_devinfos[instance] = NULL;
		return (DDI_FAILURE);
	}
	devtype = get_dev_type(data);
	if (devtype != NULL) {
		for (i = 0; i < devtype->minor_count; i++) {
			minor_t minor_num;
			minor_num = (((unsigned int)instance)<< DIDUNIT_SHIFT) +
			    devtype->minor_number[i];
			if (strcmp(data, "tape") == 0) {
				/* Numbering in /dev/did/rmt starts from 1 */
				(void) sprintf(minorBuf, "%d,%d%s", instance,
				    ((devtype->start_default_nodes -
				    instance) + 1),
				    devtype->minor_name[i]);
			} else {
				(void) sprintf(minorBuf, "%d,%d%s", instance,
					instance, devtype->minor_name[i]);
			}
			if (ddi_create_minor_node(dip, minorBuf,
			    devtype->minor_type[i], minor_num, DDI_PSEUDO, NULL)
			    == DDI_FAILURE) {
				cmn_err(CE_WARN, "did: could not "
				    "create minor node.");
				ddi_prop_free(data);
				did_devinfos[instance] = NULL;
				return (DDI_FAILURE);
			}
		}
		ddi_prop_free(data);
		DIDDPRINTF2(("didattach(%d) minors created\n", instance));
	} else {
		DIDDPRINTF2(("didattach(%d) type "
		    "\"%s\" not uploaded\n", instance, data));
		ddi_prop_free(data);
		did_devinfos[instance] = NULL;
		return (DDI_FAILURE);
	}

	devicep = get_device_data_by_instance(instance);
	if (devicep != NULL) {
		/*
		 * For Solaris 10, didattach() can be called after
		 * IOCDID_INITDONE ioctl and the call to create_did_instance()
		 * for 'instance'. So we register the deviceid here.
		 * There could be some did instances in did_instance file
		 * with no devid defined. In this case, just skip them.
		 */
		if (devicep->target_device_id != NULL) {
			int res;
			ddi_devid_unregister(did_devinfos[instance]);
			did_update_devid_prop(did_devinfos[instance], 0);
			res = ddi_devid_register(did_devinfos[instance],
			    devicep->target_device_id);
			DIDDPRINTF2(("didattach(%d) "
			    "devid register=%d\n", instance, res));
			if (res != DDI_SUCCESS) {
				did_devinfos[instance] = NULL;
				return (DDI_FAILURE);
			} else {
				did_update_devid_prop(
				    did_devinfos[instance], 1);
			}
		}
	}
	return (DDI_SUCCESS);
#else
	/*
	 * Should never get here. The only attach ever called by Solaris should
	 * be on the admin instance.
	 */
	return (DDI_FAILURE);
#endif
}


static int
diddetach(dev_info_t *dip, ddi_detach_cmd_t cmd)
{
	int			instance = ddi_get_instance(dip);

	DIDDPRINTF2(("diddetach(%d) cmd %x info %p\n",
	    instance, (int)cmd, dip));
	/*
	 * We never allow detach of a did instance.
	 */

	return (DDI_FAILURE);
}


/* ARGSUSED3 */
static int
didopen(dev_t *devp, int flag, int otyp, cred_t *credp)
{
	int		instance = DIDUNIT(*devp);
	int		error = 0;

	if (instance == DID_ADMININSTANCE) {
		error = did_admin_open(flag, otyp, credp);
	} else {
		error = did_open(devp, flag, otyp, credp);
	}
	return (error);
}



/* ARGSUSED3 */
static int
didclose(dev_t dev, int flag, int otyp, cred_t *credp)
{
	int		instance = DIDUNIT(dev);
	int		error = 0;

	if (instance == DID_ADMININSTANCE) {
		error = did_admin_close(dev, flag, otyp, credp);
	} else {
		error = did_close(dev, flag, otyp, credp);
	}
	return (error);
}



/* ARGSUSED */
static int
didinfo(dev_info_t	*dip,
	ddi_info_cmd_t	cmd,
	void		*arg,
	void		**result)
{
#ifdef DID_MULTI_INSTANCE
	dev_t	dev;
#else
	dev_t	dev, idev;
	didstate_t	*softstatep;
#endif
	int	instance;

	DIDDPRINTF4(("didinfo cmd %x arg %x (%d.%d) %p\n",
	    (int)cmd, (intptr_t)arg,
	    (int)getmajor((dev_t)arg), (int)getminor((dev_t)arg), dip));

	/* process command */
	switch (cmd) {

	/* translate dev_t */
	case DDI_INFO_DEVT2DEVINFO:
		dev = (dev_t)arg;
		instance = DIDUNIT(dev);
#ifdef DID_MULTI_INSTANCE
		*result = (void *)did_devinfos[instance];
		if (*result == NULL) {
			return (DDI_FAILURE);
		}
#else
		if ((softstatep = (didstate_t *)ddi_get_soft_state(
		    did_softstate_basep, instance)) == NULL) {
			/*
			 * It is possible for the Solaris framework
			 * to call thru this interface for global DID devices
			 * opened but not hosted on this node - for instance
			 * in the case of a devid_get call on the opened device.
			 * This happens because Solaris modctl always assumes
			 * the dev_t it has been given is local to this node.
			 * For a global device, this is not necessarily so.
			 * To accomodate this scenario we will fail silently.
			 */
			return (DDI_FAILURE);
		}
		/*
		 * We will just return NULL if it regards to sofstatep
		 * which isn't initialized (for instance 0).
		 */
		if (softstatep->subpath_listp) {
			idev = GET_DEVT(softstatep->subpath_listp, dev);
#if (SOL_VERSION >= __s10)
			dip = e_ddi_hold_devi_by_dev(idev, 0);
			*result = (void *)dip;
			if (dip != NULL) {
				/*
				 * Release the hold on the devi
				 */
				ddi_release_devi(dip);
			}
#else
			*result = (void *)dev_get_dev_info(idev, OTYP_CHR);
#endif
		} else {
			*result = (void *)NULL;
		}
#endif /* DID_MULTI_INSTANCE */
		return (DDI_SUCCESS);

	case DDI_INFO_DEVT2INSTANCE:
		dev = (dev_t)arg;
		instance = DIDUNIT(dev);
		*result  = (void *)instance;
		return (DDI_SUCCESS);

	default:
		cmn_err(CE_NOTE, "didinfo: unknown cmd %d\n", cmd);
		return (DDI_FAILURE);
	}
}

struct wrap_buf {
	struct buf	child_bp;
	struct buf	*parent_bp;
};

static int
did_done(struct buf *cb)
{
	struct wrap_buf *wbuf;
	struct buf *pb;

	wbuf = (struct wrap_buf *)cb;
	pb = wbuf->parent_bp;

	if (cb->b_flags & B_ERROR) {
		pb->b_flags |= B_ERROR;
		pb->b_error = cb->b_error;
	}
	pb->b_resid = cb->b_resid;
	bp_mapout(cb);
	biofini(&(wbuf->child_bp));
	kmem_free(wbuf, sizeof (struct wrap_buf));
	biodone(pb);

	return (0);
}

static int
didstrategy(struct buf	*bp)
{
	struct buf	*cb;
	int		instance = DIDUNIT(bp->b_edev);
	didstate_t	*softstatep;
	struct wrap_buf	*wbuf;
	char		str[DID_U2STR_LEN];

	DIDDPRINTF8(("didstrategy %s blk %d cnt %d\n",
	    did_unit_to_str(bp->b_edev, str), (int)bp->b_blkno, bp->b_bcount));

	if (instance == DID_ADMININSTANCE) {
		bp->b_flags |= B_ERROR;
		bp->b_error = ENXIO;
		bp->b_resid = bp->b_bcount;
		biodone(bp);
		return (0);
	}

	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, instance)) == NULL) {
		cmn_err(CE_WARN, "didstrategy: did_get_soft_state(%d) error\n",
				instance);
		bp->b_flags |= B_ERROR;
		bp->b_error = ENXIO;
		bp->b_resid = bp->b_bcount;
		biodone(bp);
		return (0);
	}

	wbuf = (struct wrap_buf *)kmem_alloc(sizeof (*wbuf), KM_SLEEP);
	wbuf->parent_bp = bp;
	bioinit(&(wbuf->child_bp));
	cb = bioclone(bp, 0, bp->b_bcount,
	    GET_DEVT(softstatep->current_path, bp->b_edev),
	    bp->b_blkno, did_done, &(wbuf->child_bp), KM_SLEEP);
	/* cb equals &(wbuf->child_bp), see bioclone code */

	DIDDPRINTF8(("didstrategy %s blk %d cnt %d %d.%d/%d.%d\n",
	    did_unit_to_str(bp->b_edev, str), (int)bp->b_blkno, bp->b_bcount,
	    getmajor(bp->b_edev), getminor(bp->b_edev),
	    getmajor(cb->b_edev), getminor(cb->b_edev)));
	return (bdev_strategy(cb));
}

static int
didprint(dev_t dev, char *str)
{
	char		unit_str[DID_U2STR_LEN];

	cmn_err(CE_WARN, "%s: %s: %s\n", DID_NAME,
	    did_unit_to_str(dev, unit_str), str);
	return (0);
}

static void
didmin(struct buf *bp)
{
	char		str[DID_U2STR_LEN];

	DIDDPRINTF8(("didmin %s blk %d cnt %d\n",
	    did_unit_to_str(bp->b_edev, str),
	    (int)bp->b_blkno, bp->b_bcount));

	if (bp->b_bcount > (size_t)maxphys)
		bp->b_bcount = (size_t)maxphys;
}

static int
didread(dev_t dev, struct uio *uiop, cred_t *credp)
{
	int		instance = DIDUNIT(dev);
	didstate_t	*softstatep;
	char		str[DID_U2STR_LEN];

	DIDDPRINTF8(("didread %s off %d mode %x resid %d\n",
	    did_unit_to_str(dev, str), (int)uiop->uio_offset,
	    uiop->uio_fmode, uiop->uio_resid));

	if (instance == DID_ADMININSTANCE)
		return (EIO);

	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, instance)) == NULL) {
		cmn_err(CE_WARN, "didread: did_get_soft_state(%d) error\n",
				instance);
		return (ENXIO);
	}

	return (cdev_read(GET_DEVT(softstatep->current_path, dev),
	    uiop, credp));
}


/* ARGSUSED */
static int
didaread(dev_t dev, struct aio_req *aiop, cred_t *cred_p)
{
	int		instance = DIDUNIT(dev);
	char		str[DID_U2STR_LEN];

	DIDDPRINTF8(("didaread %s\n", did_unit_to_str(dev, str)));

	if (instance == DID_ADMININSTANCE)
		return (EIO);

	if (ddi_get_soft_state(did_softstate_basep, instance) == NULL) {
		cmn_err(CE_WARN, "didread: did_get_soft_state(%d) error\n",
				instance);
		return (ENXIO);
	}

	return (aphysio((int (*)(void)) didstrategy, (int (*)(void)) anocancel,
	    dev, B_READ, (void (*)(void)) didmin, aiop));
}




static int
didwrite(dev_t dev, struct uio *uiop, cred_t *credp)
{
	int		instance = DIDUNIT(dev);
	didstate_t	*softstatep;
	char		str[DID_U2STR_LEN];

	DIDDPRINTF8(("didwrite %s off %d mode %x resid %d\n",
	    did_unit_to_str(dev, str), (int)uiop->uio_offset,
	    uiop->uio_fmode, uiop->uio_resid));

	if (instance == DID_ADMININSTANCE)
		return (EIO);

	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, instance)) == NULL) {
		cmn_err(CE_WARN, "didwrite: did_get_soft_state(%d) error\n",
				instance);
		return (ENXIO);
	}

	return (cdev_write(GET_DEVT(softstatep->current_path, dev),
	    uiop, credp));
}


/* ARGSUSED */
static int
didawrite(dev_t dev, struct aio_req *aiop, cred_t *cred_p)
{
	int		instance = DIDUNIT(dev);


	if (instance == DID_ADMININSTANCE)
		return (EIO);

	if (ddi_get_soft_state(did_softstate_basep, instance)
	    == NULL) {
		cmn_err(CE_WARN, "didawrite: did_get_soft_state(%d) error\n",
				instance);
		return (ENXIO);
	}

	return (aphysio((int (*)(void)) didstrategy, (int (*)(void)) anocancel,
	    dev, B_WRITE, (void (*)(void)) didmin, aiop));
}

/* ARGSUSED */
static int
didprop_op(
	dev_t		dev,
	dev_info_t	*dip,
	ddi_prop_op_t	prop_op,
	int		flags,
	char		*name,
	caddr_t		valuep,
	int		*lengthp)
{

	int		prop_result = DDI_PROP_SUCCESS;
	int		instance = NULL;
	dev_info_t	*physdip = (dev_info_t *)NULL;
	dev_t		transd_dev  = (dev_t)NULL;
	didstate_t	*softstatep;

	if (dev == DDI_DEV_T_ANY) {
		/*
		 * The dip being passed must either be the admin_dip (the only
		 * one this driver maps to) or the dip of the underlying
		 * driver instance which could be obtained by accessing the
		 * info interface of this driver. In either case the following
		 * call should suffice.
		 */
		return (ddi_prop_op(dev, dip, prop_op, flags, name,
		    valuep, lengthp));
	} else {
		instance = DIDUNIT(dev);
	}

	if (instance == DID_ADMININSTANCE) {

		/*
		 * Fall through for the admin instance. We don't want
		 * to pass these requests down to the underlying
		 * driver.
		 */
		return (ddi_prop_op(dev, dip, prop_op, flags, name,
		    valuep, lengthp));
	} else {
		if ((softstatep = (didstate_t *)ddi_get_soft_state(
		    did_softstate_basep, instance)) == NULL) {
			prop_result = DDI_PROP_NOT_FOUND;
		}
	}

	if (prop_result == DDI_PROP_SUCCESS) {

		transd_dev = GET_DEVT(softstatep->current_path, dev);
#if (SOL_VERSION >= __s10)
		physdip = e_ddi_hold_devi_by_dev(transd_dev, 0);
#else
		physdip = dev_get_dev_info(transd_dev, OTYP_CHR);
#endif
		if (physdip != NULL) {
#if (SOL_VERSION >= __s10)
			/*
			 * Release the hold on the devi
			 */
			ddi_release_devi(physdip);
#else
			/*
			 * "dev_get_dev_info()" returns with the driver
			 * "held" whenever it successfully translates a
			 * dev_t into a dip.
			 * Release the hold on the driver, only if
			 * we're not specifically holding the driver.
			 */
			if (!did_dev_is_open(softstatep))
				ddi_rele_driver(getmajor(transd_dev));
#endif
		} else {
			/*
			 * If we couldn't get a dip, we don't have
			 * a hold on the driver. So, we don't need
			 * to release it.
			 */
			prop_result = DDI_PROP_NOT_FOUND;
		}

		/*
		 * Redirect the property request on to the
		 * underlying physical driver.
		 */
		if (prop_result == DDI_PROP_SUCCESS) {
			prop_result = cdev_prop_op(transd_dev, physdip,
			    prop_op, flags, name, valuep, lengthp);
#if (SOL_VERSION >= __s10)
			if (prop_result != DDI_PROP_SUCCESS) {
				/*
				 * The above request could fail if the property
				 * requested is a static property of the
				 * underlying driver, yet the flags has the
				 * DDI_PROP_DYNAMIC flag on.
				 *
				 * Such property can be retrieved by passing the
				 * request to the underlying  driver, with the
				 * DDI_PROP_CONSUMER_TYPED flag on and the
				 * DDI_PROP_DYNAMIC flag off.
				 */
				if (flags & DDI_PROP_DYNAMIC) {
					/*
					 * Now search for static property. Retry
					 * the request with the DDI_PROP_DYNAMIC
					 * flag off and the
					 * DDI_PROP_CONSUMER_TYPED flag on.
					 */
					prop_result = ddi_prop_op(transd_dev,
					    physdip, prop_op,
					    flags & ~DDI_PROP_DYNAMIC |
					    DDI_PROP_CONSUMER_TYPED,
					    name, valuep, lengthp);
				}
			}
#endif
		}
	}
	DIDDPRINTF8(("didprop_op %p %p\n", dip, physdip));

	return (prop_result);
}


static int
didioctl(dev_t dev, int cmd, intptr_t arg, int mode, cred_t *credp, int *rvalp)
{
	int		error = 0;
	int		instance = DIDUNIT(dev);
	didstate_t	*softstatep;
	struct dk_cinfo dki_info;
	major_t		maj;
	char		*name;
	char		str[DID_U2STR_LEN];

	DIDDPRINTF4(("didioctl %s cmd %x arg %x mode %x\n",
	    did_unit_to_str(dev, str), cmd, arg, mode));

	if (instance == DID_ADMININSTANCE) {
		return (did_adminioctl(cmd, arg, mode, credp, rvalp));
	}

	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, instance)) == NULL) {
		cmn_err(CE_WARN, "didioctl: did_get_soft_state(%d) error\n",
				instance);
		return (ENXIO);
	}

	switch (cmd) {
	case DKIOCINFO:
		error = cdev_ioctl(GET_DEVT(softstatep->current_path, dev),
		    cmd, (intptr_t)&dki_info, mode|(int)FKIOCTL, credp, rvalp);
		if (!error) {
			(void) strcpy(dki_info.dki_dname, DID_NAME);
			if (ddi_copyout((caddr_t)&dki_info, (caddr_t)arg,
			    sizeof (struct dk_cinfo), mode))
				error = EFAULT;
		}
		break;
	case IOCDID_ISFIBRE:
		maj = getmajor(GET_DEVT(softstatep->current_path, dev));
		if ((name = ddi_major_to_name(maj)) == NULL) {
			/* Can't get the major name. Return a useful errno. */
			error = ENXIO;
		} else if (strcmp(name, "ssd") == 0) {
			/*
			 * Return success if target driver is ssd.
			 * This is the current metric for determining
			 * whether a disk is fibre or not. If we support
			 * more target drivers in the future, this logic
			 * will need to be updated.
			 */
			error = 0;
		} else {
			/*
			 * For all other target drivers than ssd,
			 * return failure.
			 */
			error = ENOENT;
		}
		break;
	default:
		error = cdev_ioctl(GET_DEVT(softstatep->current_path, dev), cmd,
		    arg, mode, credp, rvalp);
		break;
	}
	DIDDPRINTF4(("didioctl %s cmd %x arg %x=%d\n",
	    did_unit_to_str(dev, str), cmd, arg, error));

	return (error);
} /* didioctl */




/*
 * Admin routines
 */

static int
did_admin_attach(dev_info_t *dip, ddi_attach_cmd_t cmd)
{
	int	instance = ddi_get_instance(dip);
	int	error	 = DDI_SUCCESS;

	DIDDPRINTF2(("did_admin_attach info %p\n", dip));

	/* check command, setup dip and lock */
	if (cmd != DDI_ATTACH) {
		cmn_err(CE_NOTE,
		    "did_admin_attach: inst %d: unknown cmd %d\n", instance,
		    cmd);
		return (DDI_FAILURE);
	}
	if (instance != DID_ADMININSTANCE) {
		cmn_err(CE_NOTE,
		    "did_admin_attach: inst %d: unknown instance\n", instance);
		return (DDI_FAILURE);
	}

	if (did_devicep != NULL) {
		cmn_err(CE_WARN,
		    "did_admin_attach: inst %d: already attached.", instance);
		return (DDI_FAILURE);
	}

	if (ddi_soft_state_zalloc(did_softstate_basep, instance) !=
	    DDI_SUCCESS) {
		cmn_err(CE_WARN,
		    "did_admin_attach: ddi_soft_state_zalloc failed\n");
		return (DDI_FAILURE);
	}

	/*
	 * Create didadmin minor node
	 */
	if (ddi_create_minor_node(dip, "admin", S_IFCHR, (uint_t)instance,
				DDI_PSEUDO, NULL) == DDI_FAILURE) {
		cmn_err(CE_WARN,
		    "did_admin_attach:ddi_create_minor_node failed\n");
		ddi_remove_minor_node(dip, NULL);
		return (DDI_FAILURE);
	}


	/*
	 * we support kernel ioctls
	 */
	if ((error = ddi_prop_create(DDI_DEV_T_NONE, dip, DDI_PROP_CANSLEEP,
			DDI_KERNEL_IOCTL, NULL, 0)) != DDI_PROP_SUCCESS) {
		cmn_err(CE_WARN,
		    "did_admin_attach: ddi_prop_create %s error %d\n",
		    DDI_KERNEL_IOCTL, error);
	}
	/*
	 * disallow autodetach behavior
	 */
#if defined(DDI_NO_AUTODETACH)
	if ((error = ddi_prop_create(DDI_DEV_T_NONE, dip, DDI_PROP_CANSLEEP,
		DDI_NO_AUTODETACH, NULL, 0)) != DDI_PROP_SUCCESS) {
		cmn_err(CE_WARN,
		    "did_admin_attach: ddi_prop_create %s error %d\n",
		    DDI_NO_AUTODETACH, error);
	}
#endif
	return (error);
} /* did_admin_attach */



/* ARGSUSED2 */
static int
did_admin_open(int flag, int otyp, cred_t *credp)
{
	didstate_t	*softstatep;

	int instance 	= DID_ADMININSTANCE;


	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, instance)) == NULL) {
		cmn_err(CE_WARN,
		    "did_admin_open: did_get_soft_state error\n");
		return (ENXIO);
	}

	mutex_enter(&softstatep->mutex);

	/* check type and flags */
	if (otyp != OTYP_CHR) {
		cmn_err(CE_WARN, "did_admin_open: otyp not OTYP_CHR\n");
		mutex_exit(&softstatep->mutex);
		return (EINVAL);
	}
	if (((flag & FEXCL) && (did_dev_is_open(softstatep))) ||
	    (softstatep->flags & DID_EXCL)) {
		cmn_err(CE_WARN, "did_admin_open: FEXCL EBUSY\n");
		mutex_exit(&softstatep->mutex);
		return (EBUSY);
	}

	/* flag */
	softstatep->flags |= DID_EXCL;

	mutex_exit(&softstatep->mutex);

	return (0);
} /* did_admin_open */

/* ARGSUSED */
static int
did_adminioctl(int cmd, intptr_t arg, int mode, cred_t *credp,
    int *rvalp)
{
	int 			instance;
	did_init_t		*device_data;
	did_init_type_t		*device_type;
	did_device_list_t	*devicep, *prevp = NULL;
	did_device_type_list_t	*device_type_list;
	int			return_value = 0, len;
	impl_devid_t		*devidp;
	int			forced_update = 0;
#ifdef _MULTI_DATAMODEL
	model_t			model;
	did_init32_t 		*dd32;
#endif

#if defined(_SYSCALL32) && defined(_IPL32)
	/*
	 * Catch (some) if made only to one of the structs
	 * Note that since these are compile-time constants the code
	 * is eliminated when the assertion is true.
	 */
	ASSERT(sizeof (did_init_t) == sizeof (did_init32_t));
#endif

	DIDDPRINTF2(("adminioctl cmd %x arg %x mode %x\n",
	    cmd, arg, mode));

	/* CARRY OUT IOCTL COMMAND  */
	switch (cmd) {

	/*
	 * Read in all of the types of devices that the
	 * did command understands. This needs to be done
	 * before we read in any of the actual device
	 * data.
	 */
	case IOCDID_INITTYPE: {
		device_type = (did_init_type_t *)kmem_alloc(
		    sizeof (*device_type), KM_SLEEP);

#ifdef _MULTI_DATAMODEL
		if ((model = ddi_model_convert_from(mode & FMODELS)) ==
		    DDI_MODEL_ILP32) {
			did_init_type32_t *dd32;

			dd32 = (did_init_type32_t *)kmem_alloc(sizeof (*dd32),
			    KM_SLEEP);

			if (ddi_copyin((caddr_t)arg, (caddr_t)dd32,
			    sizeof (*dd32), mode))
				return_value = EFAULT;
			else {
				expand_did_init_type(device_type, dd32);
			}
			kmem_free(dd32, sizeof (*dd32));
		} else if (model != DDI_MODEL_NONE)
			return_value = EINVAL;
		else
#endif /* _MULTI_DATAMODEL */
			if (ddi_copyin((caddr_t)arg, (caddr_t)device_type,
			    sizeof (*device_type), mode))
				return_value = EFAULT;

		if (return_value != 0) {
			kmem_free(device_type, sizeof (*device_type));
			break;
		}

		(void) build_dev_type(device_type);
		kmem_free(device_type, sizeof (*device_type));
		break;
	}

	/*
	 * Read in all of the devices which the driver
	 * is going to manage.
	 */
	case IOCDID_FORCE_INIT:
		/*
		 * This flavor is called from the scdidadm -R command.
		 */
		forced_update++;

		/*FALLTHROUGH*/
	case IOCDID_INIT: {
		didstate_t	*softstatep;

		device_data = (did_init_t *)kmem_alloc(sizeof (*device_data),
		    KM_SLEEP);
		devidp = (impl_devid_t *)kmem_alloc(sizeof (impl_devid_t),
		    KM_SLEEP);
		/*
		 * Use subpath info in ioctl to build device list
		 */
#ifdef _MULTI_DATAMODEL
		if ((model = ddi_model_convert_from(mode & FMODELS)) ==
		    DDI_MODEL_ILP32) {
			did_init32_t *dd32;

			dd32 = (did_init32_t *)kmem_alloc(sizeof (*dd32),
			    KM_SLEEP);

			if (ddi_copyin((caddr_t)arg, (caddr_t)dd32,
			    sizeof (*dd32), mode))
				return_value = EFAULT;
			else {
				expand_did_init(device_data, dd32);
			}
			kmem_free(dd32, sizeof (*dd32));
		} else if (model != DDI_MODEL_NONE)
			return_value = EINVAL;
		else
#endif /* _MULTI_DATAMODEL */
			if (ddi_copyin((caddr_t)arg, (caddr_t)device_data,
			    sizeof (*device_data), mode))
				return_value = EFAULT;

		if (return_value != 0) {
			kmem_free(devidp, sizeof (impl_devid_t));
			kmem_free(device_data, sizeof (*device_data));
			break;
		}
		if (device_data->device_id != NULL) {

			/*
			 * Only do the copyin and checking of the device
			 * id if we actually have one.
			 */
			if (ddi_copyin(device_data->device_id, (caddr_t)devidp,
			    sizeof (impl_devid_t), mode)) {
				kmem_free(devidp, sizeof (impl_devid_t));
				kmem_free(device_data, sizeof (*device_data));
				return_value = EFAULT;
				break;
			}

			len = DEVID_GETLEN(devidp) + sizeof (*devidp) -
			    sizeof (char);
			ASSERT(len > 0 && len <= MAXDEVIDLEN);
			kmem_free(devidp, sizeof (impl_devid_t));
			devidp = (impl_devid_t *)kmem_alloc((size_t)len,
			    KM_SLEEP);
			if (ddi_copyin(device_data->device_id, (caddr_t)devidp,
			    (size_t)len, mode)) {
				kmem_free(devidp, (size_t)len);
				kmem_free(device_data, sizeof (*device_data));
				return_value = EFAULT;
				break;
			}
			device_data->device_id = (ddi_devid_t)devidp;
			if (ddi_devid_valid((ddi_devid_t)devidp) ==
			    DDI_FAILURE) {
				cmn_err(CE_NOTE,
				    "IOCDID_INIT: invalid device-id in %s",
				    device_data->subpath);
			}
		} else {

			/*
			 * If there isn't a device id for this device,
			 * we need to patch up the pointers so that
			 * they can be checked later.
			 */
			kmem_free(devidp, sizeof (impl_devid_t));
			devidp = NULL;
			len = 0;
			device_data->device_id = (ddi_devid_t)devidp;
		}

		DIDDPRINTF4(("IOCDID_INIT(%d) %s dev(%d,%d)\n",
		    device_data->instance, device_data->subpath,
		    getmajor(device_data->devtlist[0]),
		    getminor(device_data->devtlist[0])));

		if (forced_update || ((devicep = get_device_data_by_deviceid(
		    device_data->device_id)) == NULL)) {
			did_device_list_t *dlptr;
			/*
			 * If we couldn't find the device given the
			 * device id, look it up by instance. We only
			 * want to print out an error about the lack
			 * of a match if there was a device id, because
			 * devices with no device ids are assumed to be
			 * correct.
			 */
			if ((dlptr = get_device_data_by_instance(
			    device_data->instance)) != NULL) {
				/*
				 * Before we allow update of the in-kernel
				 * data make absolutely sure the old
				 * underlying driver isn't currently
				 * opened by us. We can determine this
				 * by looking at the softstate struct of
				 * the did instance for the "open" flag.
				 */
				if (forced_update) {
				    if ((softstatep =
					(didstate_t *)ddi_get_soft_state(
					did_softstate_basep,
					device_data->instance)) == NULL) {
					kmem_free(devidp, (size_t)len);
					kmem_free(device_data,
					    sizeof (*device_data));
					    return_value = ENXIO;
					    break;
				    }
				    if (did_dev_is_open(softstatep) &&
					did_new_devt_mappings(device_data,
					    softstatep->current_path)) {
					cmn_err(CE_WARN,
					    "did_ioctl: cannot update driver "
					    "data for did instance d%d, "
					    "because it is currently "
					    "held open\n",
					    device_data->instance);
					kmem_free(devidp, (size_t)len);
					kmem_free(device_data,
					    sizeof (*device_data));
					return_value = EBUSY;
					break;
				    }
				}
				if ((devidp != NULL) && (!forced_update)) {
					cmn_err(CE_CONT, "did: device id "
					    "registered with a different "
					    "instance: %s %d\n",
					    device_data->subpath,
					    device_data->instance);
					kmem_free(devidp, (size_t)len);
					kmem_free(device_data,
					    sizeof (*device_data));
					return_value = EINVAL;
					break;
				}
				if (devidp != NULL) {
					device_type_list =
						find_dev_type(device_data);
					if (device_type_list == NULL) {
						kmem_free(devidp, (size_t)len);
						kmem_free(device_data,
						    sizeof (*device_data));
						cmn_err(CE_NOTE,
						    "did: cannot find device "
						    "type %d for instance: %d",
						    device_data->dev_num,
						    device_data->instance);
						return_value = EINVAL;
						break;
					}
					if (device_data->state == 1) {
#ifdef DID_MULTI_INSTANCE
					    int res;
#endif
					/*
					 * First forced update
					 * call to this instance.
					 */
					    len = DEVID_GETLEN((impl_devid_t *)
						dlptr->target_device_id) +
						sizeof (impl_devid_t) -
						sizeof (char);
					    kmem_free(dlptr->target_device_id,
						(size_t)len);
#ifdef DID_MULTI_INSTANCE
					    if (did_devinfos[dlptr->instance]
						!= NULL) {
						ddi_devid_unregister(
						    did_devinfos[
						    dlptr->instance]);
						did_update_devid_prop(
						    did_devinfos[
						    dlptr->instance], 0);
					    }
#endif
					    dlptr->target_device_id =
						device_data->device_id;
#ifdef DID_MULTI_INSTANCE
					    if (did_devinfos[dlptr->instance]
						!= NULL) {
						res = ddi_devid_register(
						    did_devinfos[
						    dlptr->instance],
						    dlptr->target_device_id);
						DIDDPRINTF4(("IOCDID_INIT("
						    "%d) devid_register=%d\n",
						    dlptr->instance, res));
						if (res != DDI_SUCCESS) {
							cmn_err(CE_NOTE,
							    "IOCDID_INIT"
							    "instance %d "
							    "devid_register "
							    "error %d\n",
							    dlptr->instance,
							    res);
						} else {
						    did_update_devid_prop(
							did_devinfos[
							dlptr->instance], 1);
						}
					    }
#endif
					    cleanup_subpaths(
						dlptr->subpath_listp,
						device_data);
					} else {
					    kmem_free(devidp, (size_t)len);
					    add_subpath(
						dlptr->subpath_listp,
						device_data);
					}
					kmem_free(device_data,
					    sizeof (*device_data));
					break;
				}
			} else {
				/*
				 * Find the device type for this device
				 * and create a new entry in the linked
				 * list.
				 */
				device_type_list = find_dev_type(device_data);
				if (device_type_list == NULL) {
					if (devidp)
						kmem_free(devidp, (size_t)len);
					kmem_free(device_data,
					    sizeof (*device_data));
					cmn_err(CE_NOTE, "did: cannot find "
					    "device type %d for instance: %d",
					    device_data->dev_num,
					    device_data->instance);
					return_value = EINVAL;
					break;
				}
				devicep = build_new_device_data_entry(
				    device_data, device_type_list);
			}
		} else {
			/*
			 * We found some device data given the device
			 * id, now make sure that they match up. If they
			 * do match, add a subpath entry.
			 */
			if (device_data->instance != devicep->instance) {
				kmem_free(devidp, (size_t)len);
				kmem_free(device_data, sizeof (*device_data));
				cmn_err(CE_CONT, "did: subpath instances "
				    "do not match: %s %d %d\n",
				    device_data->subpath, device_data->instance,
				    devicep->instance);
				return_value = EINVAL;
				break;
			} else {
				kmem_free(devidp, (size_t)len);
				add_subpath(devicep->subpath_listp,
				    device_data);
			}
		}

		if ((device_data->instance != DID_ASSIGN_INSTANCE) &&
		    (device_data->instance > did_max_instance)) {
			did_max_instance = device_data->instance;
		}
		kmem_free(device_data, sizeof (*device_data));
		break;
	}
	case IOCDID_INITDONE:

		/*
		 * Walk device list: for every device with one or more subpaths
		 * without an instance : create an instance of the driver
		 * and it's minor nodes
		 */
		mutex_enter(&did_mutex);

		devicep = did_devicep;
		device_type_list = did_devlistp;

		while (devicep != NULL) {
			if (devicep->subpath_listp != (did_subpath_t *)NULL) {
				create_did_instance(devicep);
			}
			devicep = devicep->next;
		}

#ifndef DID_MULTI_INSTANCE
		while (device_type_list) {
			(void) make_did_type_nodes(device_type_list);
			device_type_list = device_type_list->next;

		}
#endif

		mutex_exit(&did_mutex);

		break;

	case IOCDID_INSTCNT:
		instance = get_device_instance_count();
		if (ddi_copyout(&instance, (caddr_t)arg,
		    sizeof (int), mode))
			return_value = EFAULT;
		break;
	case IOCDID_PATHCNT:
		if (ddi_copyin((caddr_t)arg, &instance,
		    sizeof (int), mode)) {
			return_value = EFAULT;
			break;
		}
		instance = get_device_path_count(instance);
		if (ddi_copyout(&instance, (caddr_t)arg,
		    sizeof (int), mode))
			return_value = EFAULT;
		break;
	case IOCDID_INSTINFO:
		device_data = (did_init_t *)kmem_alloc(sizeof (*device_data),
		    KM_SLEEP);
		devidp = (impl_devid_t *)kmem_alloc(sizeof (impl_devid_t),
		    KM_SLEEP);
		/*
		 * Use subpath info in ioctl to build device list
		 */
#ifdef _MULTI_DATAMODEL
		if ((model = ddi_model_convert_from(mode & FMODELS)) ==
		    DDI_MODEL_ILP32) {
			did_init32_t *dd32;

			dd32 = (did_init32_t *)kmem_alloc(sizeof (*dd32),
			    KM_SLEEP);

			if (ddi_copyin((caddr_t)arg, (caddr_t)dd32,
			    sizeof (*dd32), mode))
				return_value = EFAULT;
			else {
				expand_did_init(device_data, dd32);
			}
			kmem_free(dd32, sizeof (*dd32));
		} else if (model != DDI_MODEL_NONE)
			return_value = EINVAL;
		else
#endif /* _MULTI_DATAMODEL */
			if (ddi_copyin((caddr_t)arg, (caddr_t)device_data,
				sizeof (*device_data), mode))
				return_value = EINVAL;

		if (return_value != 0) {
			kmem_free(devidp, sizeof (impl_devid_t));
			kmem_free(device_data, sizeof (*device_data));
			break;
		}

		if (device_data->instance == 0 ||
		    device_data->instance == DID_ASSIGN_INSTANCE) {

			if (device_data->device_id == NULL) {
				kmem_free(devidp, sizeof (impl_devid_t));
				kmem_free(device_data, sizeof (*device_data));
				return_value = ESRCH;
				break;
			}

			if (ddi_copyin((caddr_t)device_data->device_id,
			    (caddr_t)devidp, sizeof (impl_devid_t), mode)) {
				kmem_free(devidp, sizeof (impl_devid_t));
				kmem_free(device_data, sizeof (*device_data));
				return_value = EFAULT;
				break;
			}

			len = DEVID_GETLEN(devidp) + sizeof (*devidp) -
			    sizeof (char);
			ASSERT(len > 0 && len <= MAXDEVIDLEN);
			kmem_free(devidp, sizeof (impl_devid_t));
			devidp = (impl_devid_t *)kmem_alloc((size_t)len,
			    KM_SLEEP);
			if (ddi_copyin((caddr_t)device_data->device_id,
			    (caddr_t)devidp, (size_t)len, mode)) {
				kmem_free(devidp, (size_t)len);
				kmem_free(device_data, sizeof (*device_data));
				return_value = EFAULT;
				break;
			}
			if (ddi_devid_valid((ddi_devid_t)devidp)
			    == DDI_FAILURE) {
				kmem_free(devidp, (size_t)len);
				kmem_free(device_data, sizeof (*device_data));
				cmn_err(CE_NOTE, "did: invalid device-id in %d",
				    device_data->instance);
				return_value = EFAULT;
				break;
			}
			if ((devicep = get_device_data_by_deviceid(
			    (ddi_devid_t)devidp)) == NULL) {

				kmem_free(devidp, (size_t)len);
				kmem_free(device_data, sizeof (*device_data));
				return_value = ESRCH;
				break;
			}
			device_data->instance = devicep->instance;
			if (ddi_copyout((caddr_t)device_data, (caddr_t)arg,
			    sizeof (*device_data), mode)) {
				return_value = EFAULT;
			}
			kmem_free(devidp, (size_t)len);
			kmem_free(device_data, sizeof (*device_data));
		} else {
			if ((devicep = get_device_data_by_instance(
			    device_data->instance)) == NULL) {
				kmem_free(devidp, sizeof (impl_devid_t));
				kmem_free(device_data, sizeof (*device_data));
				return_value = ESRCH;
			} else {
				if (devicep->target_device_id != NULL) {
					len = DEVID_GETLEN((impl_devid_t *)
					    devicep->target_device_id) +
					    sizeof (impl_devid_t) -
					    sizeof (char);
					if (ddi_copyout(
					    devicep->target_device_id,
					    device_data->device_id, (size_t)len,
					    mode)) {
						return_value = EFAULT;
					}
				}
				kmem_free(devidp, sizeof (impl_devid_t));
				kmem_free(device_data, sizeof (*device_data));
			}
		}
		break;
	case IOCDID_PATHINFO:
		device_data = (did_init_t *)kmem_alloc(sizeof (*device_data),
		    KM_SLEEP);
		devidp = (impl_devid_t *)kmem_alloc(sizeof (impl_devid_t),
		    KM_SLEEP);
		/*
		 * Use subpath info in ioctl to build device list
		 */
#ifdef _MULTI_DATAMODEL
		if ((model = ddi_model_convert_from(mode & FMODELS)) ==
		    DDI_MODEL_ILP32) {
			did_init32_t *dd32;

			dd32 = (did_init32_t *)kmem_alloc(sizeof (*dd32),
				KM_SLEEP);

			if (ddi_copyin((caddr_t)arg, (caddr_t)dd32,
			    sizeof (*dd32), mode))
				return_value = EFAULT;
			else {
				expand_did_init(device_data, dd32);
			}
			kmem_free(dd32, sizeof (*dd32));
		} else if (model != DDI_MODEL_NONE)
			return_value = EINVAL;
		else
#endif /* _MULTI_DATAMODEL */
			if (ddi_copyin((caddr_t)arg, (caddr_t)device_data,
			    sizeof (*device_data), mode))
				return_value = EINVAL;

		if (return_value != 0) {
			kmem_free(devidp, sizeof (impl_devid_t));
			kmem_free(device_data, sizeof (*device_data));
			break;
		}

		if (device_data->device_id != NULL) {
			if (ddi_copyin((caddr_t)device_data->device_id,
			    (caddr_t)devidp, sizeof (impl_devid_t), mode)) {
				kmem_free(devidp, sizeof (impl_devid_t));
				kmem_free(device_data, sizeof (*device_data));
				return_value = EFAULT;
				break;
			}
		}

		if (device_data->instance == 0 ||
		    device_data->instance == DID_ASSIGN_INSTANCE) {
			did_subpath_t *subpathp;

			if (device_data->device_id == NULL) {
				kmem_free(devidp, sizeof (impl_devid_t));
				kmem_free(device_data, sizeof (*device_data));
				return_value = ESRCH;
				break;
			}

			len = DEVID_GETLEN(devidp) + sizeof (*devidp) -
			    sizeof (char);
			ASSERT(len > 0 && len <= MAXDEVIDLEN);
			kmem_free(devidp, sizeof (impl_devid_t));
			devidp = (impl_devid_t *)kmem_alloc((size_t)len,
			    KM_SLEEP);
			if (ddi_copyin((caddr_t)device_data->device_id,
			    (caddr_t)devidp, (size_t)len, mode)) {
				kmem_free(devidp, (size_t)len);
				kmem_free(device_data, sizeof (*device_data));
				return_value = EFAULT;
				break;
			}
			if (ddi_devid_valid((ddi_devid_t)devidp)
			    == DDI_FAILURE) {
				kmem_free(devidp, (size_t)len);
				kmem_free(device_data, sizeof (*device_data));
				cmn_err(CE_NOTE, "did: invalid device-id in %d",
				    device_data->instance);
				return_value = EFAULT;
				break;
			}
			if ((devicep = get_device_data_by_deviceid(
			    (ddi_devid_t)devidp)) == NULL) {

				kmem_free(devidp, (size_t)len);
				kmem_free(device_data, sizeof (*device_data));
				return_value = ESRCH;
				break;
			}

			subpathp = devicep->subpath_listp;
			while (subpathp && --device_data->state) {
				subpathp = subpathp->next;
			}
			if (!subpathp) {
				kmem_free(devidp, (size_t)len);
				kmem_free(device_data, sizeof (*device_data));
				return_value = ESRCH;
				break;
			}
			device_data->state = subpathp->state;
			(void) strcpy(device_data->subpath,
			    subpathp->device_path);
#ifdef _MULTI_DATAMODEL
			if ((model = ddi_model_convert_from(mode & FMODELS)) ==
			    DDI_MODEL_ILP32) {
				dd32 = (did_init32_t *)kmem_alloc(
					sizeof (*dd32), KM_SLEEP);
				shrink_did_init(device_data, dd32);
				if (ddi_copyout((caddr_t)dd32, (caddr_t)arg,
					sizeof (*dd32), mode)) {
					return_value = EFAULT;
				}
				kmem_free(dd32, sizeof (*dd32));
			} else if (model != DDI_MODEL_NONE)
				return_value = EINVAL;
			else
#endif
			if (ddi_copyout((caddr_t)device_data, (caddr_t)arg,
			    sizeof (*device_data), mode))
				return_value = EFAULT;

			kmem_free(devidp, (size_t)len);
			kmem_free(device_data, sizeof (*device_data));
		} else {
			did_subpath_t *subpathp;

			if ((devicep = get_device_data_by_instance(
			    device_data->instance)) == NULL) {
				kmem_free(devidp, sizeof (impl_devid_t));
				kmem_free(device_data, sizeof (*device_data));
				return_value = ESRCH;
				break;
			}
			subpathp = devicep->subpath_listp;
			while (subpathp && --device_data->state) {
				subpathp = subpathp->next;
			}
			if (!subpathp) {
				kmem_free(devidp, sizeof (impl_devid_t));
				kmem_free(device_data, sizeof (*device_data));
				return_value = ESRCH;
				break;
			}
			device_data->state = subpathp->state;
			(void) strcpy(device_data->subpath,
			    subpathp->device_path);
#ifdef _MULTI_DATAMODEL
			if ((model = ddi_model_convert_from(mode & FMODELS)) ==
			    DDI_MODEL_ILP32) {
				dd32 = (did_init32_t *)kmem_alloc(
				    sizeof (*dd32), KM_SLEEP);
				shrink_did_init(device_data, dd32);
				if (ddi_copyout((caddr_t)dd32, (caddr_t)arg,
				    sizeof (*dd32), mode)) {
					return_value = EFAULT;
				}
				kmem_free(dd32, sizeof (*dd32));
			} else if (model != DDI_MODEL_NONE)
				return_value = EINVAL;
			else
#endif
			if (ddi_copyout((caddr_t)device_data, (caddr_t)arg,
			    sizeof (*device_data), mode))
				return_value = EFAULT;

			kmem_free(devidp, sizeof (impl_devid_t));
			kmem_free(device_data, sizeof (*device_data));
		}
		break;
	case IOCDID_STICK:
		didunloadflag = 0;
		break;
	case IOCDID_UNSTICK:
		didunloadflag = 1;
		break;
	case IOCDID_RMINST: {
		didstate_t	*softstatep;
		did_subpath_t	*subt, *subn;

		if (ddi_copyin((caddr_t)arg, &instance,
		    sizeof (int), mode)) {
			return_value = EFAULT;
			break;
		}

#ifdef DID_MULTI_INSTANCE
		/* Check if did instance exists */
		if (did_devinfos[instance] == NULL) {
			return_value = ENODEV;
			break;
		}
#endif

		if ((softstatep = (didstate_t *)ddi_get_soft_state(
		    did_softstate_basep, instance)) == NULL) {
			return_value = ENXIO;
			break;
		}

		/*
		 * See if this instance was previously removed, leaving an empty
		 * softstate.
		 */
		if (softstatep->subpath_listp == NULL) {
			return_value = ENXIO;
			break;
		}

		if (did_dev_is_open(softstatep)) {
			cmn_err(CE_WARN, "did_ioctl: cannot remove driver "
			    "data for did instance d%d, because it is "
			    "currently held open\n", instance);
			return_value = EBUSY;
			break;
		}

		devicep = did_devicep;
		while (devicep != NULL) {
			if (devicep->instance == instance) {
				if (devicep == did_devicep)
					did_devicep = devicep->next;
				else
					prevp->next = devicep->next;
				break;
			}
			prevp = devicep;
			devicep = devicep->next;
		}

#ifdef DID_MULTI_INSTANCE
		/* unregister the devid */
		ddi_devid_unregister(did_devinfos[instance]);
		did_update_devid_prop(did_devinfos[instance], 0);
#endif
		softstatep->current_path = NULL;
		softstatep->subpath_listp = NULL;

		/* free memory */
		subt = devicep->subpath_listp;
		while (subt != NULL) {
			subn = subt->next;
			kmem_free(subt, (sizeof (did_subpath_t)));
			subt = subn;
		}

		if (devicep->target_device_id != NULL) {
			len = DEVID_GETLEN(
			    (impl_devid_t *)devicep->target_device_id) +
			    sizeof (impl_devid_t) - sizeof (char);
			kmem_free(devicep->target_device_id, (size_t)len);
		}

		kmem_free(devicep, (sizeof (did_device_list_t)));

		break;
	}
	default:
		DIDDPRINTF2(("did_adminioctl unknown ioctl %x\n", cmd));
		return_value = ENOTTY;
		break;
	} /* switch */

	/* return success */
	return (return_value);
} /* did_adminioctl */


/* ARGSUSED */
static int
did_admin_close(dev_t dev, int flag, int otyp, cred_t *credp)
{
	didstate_t *softstatep;

	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, DID_ADMININSTANCE)) == NULL) {
		cmn_err(CE_WARN,
		    "did_admin_close:ddi_get_soft_state error\n");
		return (ENXIO);
	}

	mutex_enter(&softstatep->mutex);
	softstatep->flags &= ~DID_EXCL;
	mutex_exit(&softstatep->mutex);

	return (0);

} /* did_admin_close */




/*
 * Normal command processing routines
 */


/*
 * did isn't totally DDI compliant, the driver attaching is done
 * through did_state_init(), which is called through did_adminioctl().
 * There is truly only one instance of DID ever attached, the admin instance.
 */

static int
did_state_init(int instance)
{
	didstate_t 		*softstatep;
	did_device_list_t 	*device_datap;
	int			error = DDI_SUCCESS;
#ifndef DID_MULTI_INSTANCE
	int			len;
#endif

	DIDDPRINTF2(("did_state_init(%d)\n", instance));

	/*
	 * If subpath_listp is NULL, then this instance was previsouly removed
	 * by RMINST, leaving an 'empty' sofstate we can re-use.
	 */
	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, instance)) != NULL) {
		if (softstatep->subpath_listp != NULL)
			return (DDI_FAILURE);
	} else {
		if (ddi_soft_state_zalloc(did_softstate_basep, instance) !=
		    DDI_SUCCESS) {
			cmn_err(CE_WARN,
			    "did_attach: ddi_init_device_id error\n");
			return (DDI_FAILURE);
		}
	}

	/* Find this instance's private data built by ioctls */
	if ((device_datap = get_device_data_by_instance(instance)) == NULL) {
		cmn_err(CE_WARN,
			"did_attach: get_device_data_by_instance error\n");
		return (DDI_FAILURE);
	}


	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, instance)) == NULL) {
		cmn_err(CE_WARN, "did_attach: ddi_get_soft_state error\n");
		return (DDI_FAILURE);
	}

	/*
	 * Transfer data build by DIDIOC_INIT to the instance data structure
	 * including the entire subpath list
	 */
	mutex_init(&softstatep->mutex, "did instance mutex", MUTEX_DRIVER,
	    NULL);

	softstatep->subpath_listp = device_datap->subpath_listp;

#ifdef DID_MULTI_INSTANCE
	if (did_devinfos[instance] == NULL) {
		/*
		 * For Solaris 10, attach can be
		 * called after IOCDID_INITDONE
		 * and create_did_instance() call.
		 * deviceid will be registered
		 * during attach in this case.
		 */
		return (DDI_SUCCESS);
	}
#endif

	/*
	 * There could be some did instances in did_instance file
	 * with no devid defined. In this case, just skip them.
	 */
	if (device_datap->target_device_id != NULL) {
#ifdef DID_MULTI_INSTANCE
		int res;
		ddi_devid_unregister(did_devinfos[instance]);
		did_update_devid_prop(did_devinfos[instance], 0);
		res = ddi_devid_register(did_devinfos[instance],
			device_datap->target_device_id);
		DIDDPRINTF2(("did_state_init(%d) devid register=%d\n",
		    instance, res));
		if (res != DDI_SUCCESS) {
			cmn_err(CE_NOTE, "did_state_init inst %d "
			    "ddi_devid_register error %d\n", instance, res);
		} else {
			did_update_devid_prop(did_devinfos[instance], 1);
		}
#else
		len = DEVID_GETLEN(
		    (impl_devid_t *)device_datap->target_device_id);
		if (ddi_devid_init(admin_dip, DEVID_ENCAP, (ushort_t)len,
		    device_datap->target_device_id,
		    &softstatep->target_device_id) != DDI_SUCCESS) {
			cmn_err(CE_WARN,
			    "did_attach: ddi_init_device_id error\n");
			mutex_destroy(&softstatep->mutex);
			ddi_devid_free(softstatep->target_device_id);
			ddi_soft_state_free(did_softstate_basep, instance);
			return (DDI_FAILURE);
		}
#endif
	}

	return (error);
}


static int
did_open(dev_t *devp, int flag, int otyp, cred_t *credp)
{
	dev_t		dev;
	major_t		maj;
	minor_t		dmin;
	int		instance = DIDUNIT(*devp);
	int		error = 0;
	didstate_t	*softstatep;
	struct dev_ops	*ops = NULL;
	int		held = 0;
	char		str[DID_U2STR_LEN];

#ifdef DID_MULTI_INSTANCE
	if (did_devinfos[instance] == NULL) {
		/*
		 * This should not happen, something is wrong,
		 * mainly did.conf unable to be sync by scdidadm -i.
		 * Return an error instead of ASSERT or panic,
		 * so driver continues to work for other instances.
		 */
		return (ENODEV);
	}
#endif

	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, instance)) == NULL) {
		return (ENXIO);
	}
	/*
	 * XXXXX: race condition between softstate getting setup and
	 *	mutex getting initilized see did_state_init.
	 */
	mutex_enter(&softstatep->mutex);

	if (!softstatep->current_path) {
		if (did_choose_path(softstatep)) {
			mutex_exit(&softstatep->mutex);
			return (ENXIO);
		}
	}


	/*
	 * Return ENXIO for non-existent devices.
	 */
	if ((dev = GET_DEVT(softstatep->current_path, *devp)) == NODEV) {
		mutex_exit(&softstatep->mutex);
		DIDDPRINTF1(("did_open %p %s %s return ENXIO\n",
		    softstatep, did_unit_to_str(*devp, str),
		    did_open_flg_str(otyp)));
		return (ENXIO);
	}
	maj = getmajor(dev);
	dmin = getminor(dev);
	if (devopsp[maj]->devo_cb_ops == NULL)  {
		DIDDPRINTF1(("did_open devopsp[%u]->devo_cb_ops is NULL"
		    " %p %s %s return ENXIO\n",
		    maj, softstatep, did_unit_to_str(*devp, str),
		    did_open_flg_str(otyp)));
		mutex_exit(&softstatep->mutex);
		return (ENXIO);
	} else {
		DIDDPRINTF1(("did_open devopsp[%u] %p open %p\n",
		    maj, devopsp[maj]->devo_cb_ops,
		    devopsp[maj]->devo_cb_ops->cb_open));
	}

	DIDDPRINTF1(("did_open %p %s real=(%d,%d)\n",
	    softstatep, did_unit_to_str(*devp, str), maj, dmin));
	if (otyp == OTYP_LYR) {
		DIDDPRINTF1(("did_open %p %s %s lyr %u\n",
		    softstatep, did_unit_to_str(*devp, str),
		    did_open_flg_str(otyp),
		    softstatep->un_ocmap.lyr_open[DIDPART(*devp)]));
	} else {
		DIDDPRINTF1(("did_open %p %s reg[%s] %x\n",
		    softstatep, did_unit_to_str(*devp, str),
		    did_open_flg_str(otyp),
		    softstatep->un_ocmap.reg_open[otyp]));
	}

	/*
	 * Autoload, install and hold the driver.
	 */
	if (!did_dev_is_open(softstatep)) {
		if (((ops = ddi_hold_installed_driver(maj)) == NULL) ||
		    (ops->devo_cb_ops == NULL)) {
			error = ENXIO;
			cmn_err(CE_NOTE, "couldn't hold installed driver");
			mutex_exit(&softstatep->mutex);
			return (error);
		}
		DIDDPRINTF1(("did_open %p %s %s ddi_hold\n", softstatep,
		    did_unit_to_str(*devp, str), did_open_flg_str(otyp)));
		held = 1;
	}

	if (!did_subdev_is_open(softstatep, devp)) {
		error = dev_open((dev_t *)&GET_DEVT(softstatep->current_path,
		    *devp), flag, OTYP_LYR, credp);
		DIDDPRINTF1(("did_open %p %s dev_open(LYR)=%d\n", softstatep,
		    did_unit_to_str(*devp, str), error));
		if (error != 0) {
/*lint -e801 */
			goto open_fail;
		}
	}

	if (otyp == OTYP_LYR) {
		softstatep->un_ocmap.lyr_open[DIDPART(*devp)]++;
		DIDDPRINTF1(("did_open %p %s %s lyr=%u\n",
		    softstatep, did_unit_to_str(*devp, str),
		    did_open_flg_str(otyp),
		    softstatep->un_ocmap.lyr_open[DIDPART(*devp)]));
	} else {
		softstatep->un_ocmap.reg_open[otyp] |=
		    (1 << DIDPART(*devp));
		DIDDPRINTF1(("did_open %p %s reg[%s]=%x\n", softstatep,
		    did_unit_to_str(*devp, str), did_open_flg_str(otyp),
		    softstatep->un_ocmap.reg_open[otyp]));
	}

open_fail:
	mutex_exit(&softstatep->mutex);

	DIDDPRINTF1(("did_open dev(%d,%d)=%d\n",
	    getmajor(GET_DEVT(softstatep->current_path, *devp)),
	    getminor(GET_DEVT(softstatep->current_path, *devp)),
	    error));

	if (error && held) {
		DIDDPRINTF1(("did_open %p %s %s ddi_rele\n", softstatep,
		    did_unit_to_str(*devp, str), did_open_flg_str(otyp)));
		ddi_rele_driver(maj);
	}
	return (error);
}

static int
did_choose_path(didstate_t *softstatep)
{
	DIDDPRINTF2(("path (%d,%d) %s\n",
	    getmajor(softstatep->subpath_listp->device[0]),
	    getminor(softstatep->subpath_listp->device[0]),
	    softstatep->subpath_listp->device_path));

	if (softstatep->subpath_listp == NULL) {
		return (1);
	}
	softstatep->current_path = softstatep->subpath_listp;
	return (0);
}

static int
did_close(dev_t dev, int flag, int otyp, cred_t *credp)
{
	int		error = 0;
	int		instance = DIDUNIT(dev);
	major_t		maj;
	minor_t		dmin;
	didstate_t	*softstatep;
	char		str[DID_U2STR_LEN];

	if ((softstatep = (didstate_t *)ddi_get_soft_state(
	    did_softstate_basep, instance)) == NULL) {
		cmn_err(CE_WARN, "did_close: did_get_soft_state(%d) error\n",
				instance);
		return (ENXIO);
	}

	mutex_enter(&softstatep->mutex);

	if (softstatep->current_path != NULL) {
		maj = getmajor(GET_DEVT(softstatep->current_path, dev));
		dmin = getminor(GET_DEVT(softstatep->current_path, dev));
	} else {
		cmn_err(CE_WARN,
		    "did_close: instance (%d) current_path is NULL\n",
		    instance);
		mutex_exit(&softstatep->mutex);
		return (ENXIO);
	}

	DIDDPRINTF1(("did_close %p %s real=(%d,%d)\n",
	    softstatep, did_unit_to_str(dev, str), maj, dmin));
	if (otyp == OTYP_LYR) {
		DIDDPRINTF1(("did_close %p %s %s lyr %u\n",
		    softstatep, did_unit_to_str(dev, str),
		    did_open_flg_str(otyp),
		    softstatep->un_ocmap.lyr_open[DIDPART(dev)]));
	} else {
		DIDDPRINTF1(("did_close %p %s reg[%s] %x\n",
		    softstatep, did_unit_to_str(dev, str),
		    did_open_flg_str(otyp),
		    softstatep->un_ocmap.reg_open[otyp]));
	}

	if (otyp == OTYP_LYR) {
		if (softstatep->un_ocmap.lyr_open[DIDPART(dev)] > 0)  {
			softstatep->un_ocmap.lyr_open[DIDPART(dev)]--;
			DIDDPRINTF1(("did_close %p %s %s lyr=%u\n",
			    softstatep, did_unit_to_str(dev, str),
			    did_open_flg_str(otyp),
			    softstatep->un_ocmap.lyr_open[DIDPART(dev)]));
		}
	} else {
		softstatep->un_ocmap.reg_open[otyp] &=
		    (~((unsigned int)(1 << DIDPART(dev))));
		DIDDPRINTF1(("did_close %p %s reg[%s]=%x\n",
		    softstatep, did_unit_to_str(dev, str),
		    did_open_flg_str(otyp),
		    softstatep->un_ocmap.reg_open[otyp]));
	}

	if (!did_subdev_is_open(softstatep, &dev)) {
		error = dev_close(GET_DEVT(softstatep->current_path, dev),
		    flag, OTYP_LYR, credp);
		DIDDPRINTF1(("did_close %p %s dev_close(LYR)=%d\n", softstatep,
		    did_unit_to_str(dev, str), error));
	}

	if (!did_dev_is_open(softstatep)) {
		DIDDPRINTF1(("did_close %p %s %s ddi_rele\n", softstatep,
		    did_unit_to_str(dev, str), did_open_flg_str(otyp)));
		ddi_rele_driver(maj);
	}
	mutex_exit(&softstatep->mutex);

	return (error);
}

static int
get_device_path_count(int instance)
{
	did_device_list_t *returnp = did_devicep;

	mutex_enter(&did_mutex);

	while ((returnp != NULL) && (returnp->instance != instance)) {
		returnp = returnp->next;
	}

	mutex_exit(&did_mutex);

	if (returnp && returnp->subpath_listp)
		return (get_subpath_count(returnp->subpath_listp));
	else
		return (0);
}

static int
get_device_instance_count(void)
{
	int	instcount = 0;
	did_device_list_t *returnp = did_devicep;

	mutex_enter(&did_mutex);

	while (returnp != NULL) {
		instcount++;
		returnp = returnp->next;
	}

	mutex_exit(&did_mutex);

	return (instcount);
}

static did_device_list_t *
get_device_data_by_instance(int instance)
{

	did_device_list_t *returnp = did_devicep;

	mutex_enter(&did_mutex);

	while ((returnp != NULL) && (returnp->instance != instance)) {
		returnp = returnp->next;
	}

	mutex_exit(&did_mutex);

	return (returnp);
}


static did_device_list_t *
get_device_data_by_deviceid(ddi_devid_t device_id)
{

	did_device_list_t *returnp = did_devicep;

	/* If there is no device id, obviously, there is no match. */
	if (device_id == NULL)
		return (NULL);

	mutex_enter(&did_mutex);

	while (returnp != NULL) {
		if ((!returnp->target_device_id) ||
		    (ddi_devid_compare(returnp->target_device_id, device_id)))
			returnp = returnp->next;
		else {
			mutex_exit(&did_mutex);
			return (returnp);
		}
	}

	mutex_exit(&did_mutex);

	return (returnp);
}


static did_device_list_t *
build_new_device_data_entry(did_init_t *datap, did_device_type_list_t *devtypep)
{
	did_device_list_t *returnp;
	did_device_list_t *searchp;
	int	i;

	/*
	 * list entry
	 */
	returnp = (did_device_list_t *)
		kmem_zalloc(sizeof (did_device_list_t), KM_SLEEP);
	returnp->target_device_id = datap->device_id;
	returnp->instance = datap->instance;

	returnp->type_num = devtypep->dev_num;
	returnp->next = NULL;
	returnp->prev = NULL;

	returnp->subpath_listp = (did_subpath_t *)
		kmem_zalloc(sizeof (did_subpath_t), KM_SLEEP);
	(void) strcpy(returnp->subpath_listp->device_path, datap->subpath);

	returnp->subpath_listp->state = datap->state;
	returnp->subpath_listp->next = NULL;
	returnp->subpath_listp->prev = NULL;

	for (i = 0; i < devtypep->minor_count; i++)
		returnp->subpath_listp->device[i] = datap->devtlist[i];

	/*
	 * Find end of list and add list entry to it
	 */
	mutex_enter(&did_mutex);

	if (did_devicep == NULL) {
		did_devicep = returnp;
	} else {
		/* Go to end of device data linked list */
		searchp = did_devicep;
		while (searchp->next != NULL) {
			searchp = searchp->next;
		}

		searchp->next = returnp;
		returnp->prev = searchp;
	}

	mutex_exit(&did_mutex);

	return (returnp);

} /* build_new_device_data_entry */

static int
get_subpath_count(did_subpath_t *subpath_listheadp)
{
	did_subpath_t *subpathp;
	int	count = 0;

	ASSERT(subpath_listheadp != NULL);

	mutex_enter(&did_mutex);

	/*
	 * find the subpath
	 */
	subpathp = subpath_listheadp;
	do {
		count++;
	} while ((subpathp = subpathp->next) != NULL);

	mutex_exit(&did_mutex);

	return (count);
} /* get_subpath */

#ifdef _MULTI_DATAMODEL
static int
expand_did_init(did_init_t *device_data, did_init32_t *dd32)
{
	int i;

	device_data->device_id = (ddi_devid_t)dd32->device_id;
	device_data->instance = dd32->instance;
	for (i = 0; i < DID_MAX_MINORS; i++) {
		device_data->devtlist[i] = DEVEXPL(dd32->devtlist[i]);
	}
	strncpy(device_data->subpath, dd32->subpath,
	    sizeof (device_data->subpath));
	device_data->state = dd32->state;
	device_data->dev_num = dd32->dev_num;

	return (0);
}

static int
shrink_did_init(did_init_t *device_data, did_init32_t *dd32)
{
	int i;
	dd32->device_id = (caddr32_t)device_data->device_id;
	dd32->instance = device_data->instance;
	for (i = 0; i < DID_MAX_MINORS; i++) {
		dd32->devtlist[i] = DEVCMPL(device_data->devtlist[i]);
	}
	strncpy(dd32->subpath, device_data->subpath,
	    sizeof (dd32->subpath));
	dd32->state = device_data->state;
	dd32->dev_num = device_data->dev_num;

	return (0);
}

static int
expand_did_init_type(did_init_type_t *dev_type, did_init_type32_t *dd32)
{
	int i;

	dev_type->dev_num = dd32->dev_num;
	strncpy(dev_type->device_type, dd32->device_type,
	    sizeof (dev_type->device_type));
	dev_type->minor_count = dd32->minor_count;

	for (i = 0; i < dev_type->minor_count; i++) {
		strncpy(dev_type->minor_name[i], dd32->minor_name[i],
		    sizeof (dev_type->minor_name[i]) - 1);
		dev_type->minor_name[i][sizeof (dev_type->minor_name[i])-1] =
		    '\0';
		dev_type->minor_type[i] = dd32->minor_type[i];
		dev_type->minor_number[i] = dd32->minor_number[i];
	}
	dev_type->default_nodes = dd32->default_nodes;
	dev_type->start_default_nodes = dd32->start_default_nodes;
	dev_type->next_default_node = dd32->next_default_node;
	dev_type->cur_max_instance = dd32->cur_max_instance;

	return (0);
}
#endif

/*
 * This should be called the first time during forced_update a new devid
 * has been encountered for a given did instance. Subsequent forced_update
 * invocations on the same device will be handled by the add_subpath
 * interface.
 */
static void
cleanup_subpaths(did_subpath_t *subpath_listheadp, did_init_t *datap)
{
	did_subpath_t *subpathp, *nextp;
	int		i;

	ASSERT(subpath_listheadp != NULL);

	(void) strcpy(subpath_listheadp->device_path, datap->subpath);

	subpath_listheadp->state = datap->state;

	for (i = 0; i < DID_MAX_MINORS; i++)
		subpath_listheadp->device[i] = datap->devtlist[i];

	mutex_enter(&did_mutex);

	/*
	 * Free the rest of the subpath list.
	 */
	subpathp = subpath_listheadp->next;
	subpath_listheadp->next = NULL;

	while (subpathp != NULL) {
		nextp = subpathp->next;
		kmem_free(subpathp, (sizeof (did_subpath_t)));
		subpathp = nextp;
	}
	mutex_exit(&did_mutex);
}

static void
add_subpath(did_subpath_t *subpath_listheadp, did_init_t *datap)
{
	did_subpath_t *new_subpathp;
	did_subpath_t *subpathp;
	int		i;

	ASSERT(subpath_listheadp != NULL);

	new_subpathp = (did_subpath_t *)
		kmem_zalloc(sizeof (did_subpath_t), KM_SLEEP);
	(void) strcpy(new_subpathp->device_path, datap->subpath);

	new_subpathp->state = datap->state;
	new_subpathp->next = NULL;

	for (i = 0; i < DID_MAX_MINORS; i++)
		new_subpathp->device[i] = datap->devtlist[i];

	mutex_enter(&did_mutex);

	/*
	 * Go to end of subpath list
	 */
	subpathp = subpath_listheadp;

	if (strcmp(datap->subpath, subpathp->device_path) == 0) {
		kmem_free(new_subpathp, sizeof (did_subpath_t));
		goto done;
	}

	while (subpathp->next != NULL) {
		subpathp = subpathp->next;
		if (strcmp(datap->subpath, subpathp->device_path) == 0) {
			kmem_free(new_subpathp, sizeof (did_subpath_t));
			goto done;
		}
	}

	/*
	 * Fixup links in subpath linked list, installing new subpath at
	 * the end
	 */
	new_subpathp->prev = subpathp;
	subpathp->next = new_subpathp;

done:

	mutex_exit(&did_mutex);
}

static void
create_did_instance(did_device_list_t *devicep)
{

	if (devicep->instance == DID_ASSIGN_INSTANCE) {
		did_max_instance++;
		devicep->instance = did_max_instance;
		cmn_err(CE_NOTE, "did: new instance created (%d)",
		    did_max_instance);
	}

	mutex_exit(&did_mutex);
	(void) did_state_init(devicep->instance);
	mutex_enter(&did_mutex);
}

#ifndef DID_MULTI_INSTANCE
static int
make_did_type_nodes(did_device_type_list_t *devtype)
{

	char	minorBuf[MAXPATHLEN];
	int	i, instcount, namecount;
	int	namemax, namestart;
	int	curmaxcount;
	dev_info_t	*dip;
	/*
	 * We want to create did minor nodes in blocks of
	 * devices at a time. So, we only want to do the
	 * ddi_create_minor if we haven't already done it on
	 * that block of instances.
	 */

	if (devtype->next_default_node > 0) {
		if (devtype->cur_max_instance >= devtype->last_default_node) {
			curmaxcount = devtype->cur_max_instance -
			    devtype->start_default_nodes + 1;
			namestart = devtype->last_default_node -
			    devtype->start_default_nodes + 1;
			namemax = namestart;
			while (namemax <= curmaxcount)
				namemax = namemax + devtype->default_nodes;
		} else {
			return (DDI_SUCCESS);
		}
	} else if (devtype->next_default_node < 0) {
		if (devtype->cur_max_instance <= devtype->last_default_node) {
			curmaxcount = devtype->start_default_nodes -
			    devtype->cur_max_instance + 1;
			namestart = devtype->start_default_nodes -
			    devtype->last_default_node + 1;
			namemax = namestart;

			while (namemax <= curmaxcount)
				namemax = namemax + devtype->default_nodes;
		} else {
			return (DDI_SUCCESS);
		}
	} else {
		cmn_err(CE_WARN, "did: Cannot create multiple equivalent minor "
		    "nodes.");
		return (DDI_FAILURE);
	}

	for (namecount = namestart, instcount = devtype->last_default_node;
	    namecount < namemax; namecount++, instcount = instcount +
	    devtype->next_default_node) {
		dip = admin_dip;
		ASSERT(dip != NULL);

		for (i = 0; i < devtype->minor_count; i++) {
			minor_t minor_num =
			    ((uint_t)instcount << SUBDEVICE_BITS) |
			    devtype->minor_number[i];
			(void) sprintf(minorBuf, "%d,%d%s", instcount,
			    namecount, devtype->minor_name[i]);
			if (ddi_create_minor_node(dip, minorBuf,
			    devtype->minor_type[i], minor_num,
			    DDI_PSEUDO, NULL) == DDI_FAILURE) {
				cmn_err(CE_WARN, "did: could not create minor "
				    "node.");
				return (DDI_FAILURE);
			}
		}

		devtype->last_default_node = instcount;
		DIDDPRINTF2(("make_did_type_nodes inst %d minors created\n",
		    instcount));

	}
	devtype->last_default_node = devtype->last_default_node +
	    devtype->next_default_node;

	return (DDI_SUCCESS);

}
#endif

/*
 * Assumes str has preallocated space sufficient to hold the messages
 * herein - 40 characters (DID_U2STR_LEN) should suffice.
 */
static char *
did_unit_to_str(dev_t dev, char *str)
{
	int		instance = DIDUNIT(dev);

	if (getminor(dev) == DID_ADMINMINOR) {
		(void) strcpy(str, "admin");
	} else {
		(void) sprintf(str, "%d,%d", instance, DIDPART(dev));
	}
	return (str);
}

static char *
did_open_flg_str(int flag) {
	switch (flag) {
		case OTYP_BLK:
			return ("BLK");
		case OTYP_MNT:
			return ("MNT");
		case OTYP_CHR:
			return ("CHR");
		case OTYP_SWP:
			return ("SWP");
		case OTYP_LYR:
			return ("LYR");
		default:
			return ("UNKNOWN");
	}
}

/*
 * Find the device type for a given device.
 */
static did_device_type_list_t *
find_dev_type(did_init_t *device_data)
{
	did_device_type_list_t *devtypep = did_devlistp;

	for (devtypep = did_devlistp; devtypep; devtypep = devtypep->next) {
		if (devtypep->dev_num == device_data->dev_num)
			return (devtypep);
	}

	return (NULL);
}

#ifdef DID_MULTI_INSTANCE
/*
 * Update the did_device_ids property value
 */
static void
did_update_devid_prop(dev_info_t *dip, int value)
{
	int error;
	if ((error = ddi_prop_update_int(DDI_DEV_T_NONE, dip,
	    "did_device_ids", value)) != DDI_PROP_SUCCESS) {
		cmn_err(CE_WARN, "didattach: ddi_prop_update_int "
		    "did_device_ids error %d\n", error);
	}
}

/*
 * For a given device type, get the uploaded structure from the list.
 */
static did_device_type_list_t *
get_dev_type(char *data)
{
	did_device_type_list_t *devtypep = did_devlistp;

	for (devtypep = did_devlistp; devtypep; devtypep = devtypep->next) {
		if (strcmp(devtypep->dev_name, data) == 0)
			return (devtypep);
	}

	return (NULL);
}

#endif

/*
 * Initialize the device type and insert it into the global
 * list of device types, given a did_init_type pointer.
 */
static did_device_type_list_t *
build_dev_type(did_init_type_t *init_device_type)
{
	did_device_type_list_t *devlistp;
	int i;

	/*
	 * Look and see if there is already a device type
	 * with a matching number. If there is, don't
	 * allocate anything new, update cur_max_instance,
	 * then return that pointer.
	 */
	for (devlistp = did_devlistp; devlistp; devlistp = devlistp->next) {
		if (devlistp->dev_num == init_device_type->dev_num) {
			devlistp->cur_max_instance =
				init_device_type->cur_max_instance;
			return (devlistp);
		}
	}

	devlistp = (did_device_type_list_t *)kmem_alloc(sizeof (*did_devlistp),
	    KM_SLEEP);

	devlistp->dev_num = init_device_type->dev_num;
	(void) strcpy(devlistp->dev_name, init_device_type->device_type);
	devlistp->minor_count = init_device_type->minor_count;

	for (i = 0; i < devlistp->minor_count; i++) {
		(void) strcpy(devlistp->minor_name[i],
		    init_device_type->minor_name[i]);
		devlistp->minor_type[i] = init_device_type->minor_type[i];
		devlistp->minor_number[i] = init_device_type->minor_number[i];
	}

	devlistp->default_nodes = init_device_type->default_nodes;
	devlistp->start_default_nodes = init_device_type->start_default_nodes;
	devlistp->next_default_node = init_device_type->next_default_node;
	devlistp->last_default_node = init_device_type->start_default_nodes;
	devlistp->cur_max_instance = init_device_type->cur_max_instance;

	devlistp->next = did_devlistp;
	did_devlistp = devlistp;

	return (devlistp);

}

/*
 * First check the state field in device_data. If it's '1' then we are
 * about to blow away the first subpath entry for a given instance which,
 * based on the current algorithm, will be the underlying physical device
 * currently opened.
 * Return 1 if the dev_t mappings in device_data don't match those in
 * subp. Otherwise return 0.
 */
static int
did_new_devt_mappings(did_init_t *device_data, did_subpath_t *subp)
{
	ASSERT(subp != NULL);
	if (device_data->state != 1)
		/*
		 * We don't care about new dev_t mappings unless they
		 * involve altering the subpath entry for the currently
		 * opened path.
		 */
		return (0);
	/*
	 * We only need to check the first entry in the array.
	 */
	if (subp->device[0] != device_data->devtlist[0])
		return (1);
	else
		return (0);
}

static int
did_dev_is_open(didstate_t *softstatep)
{
	int	i;

	for (i = 0; i < DID_MAX_MINORS; i++) {
		if (softstatep->un_ocmap.lyr_open[i]) {
			return (1);
		}
	}

	for (i = 0; i < (OTYPCNT - 1); i++) {
		if (softstatep->un_ocmap.reg_open[i]) {
			return (1);
		}
	}
	return (0);
}


static int
did_subdev_is_open(didstate_t *softstatep, dev_t *devp)
{
	int	i;

	if (softstatep->un_ocmap.lyr_open[DIDPART(*devp)]) {
		return (1);
	}
	for (i = 0; i < (OTYPCNT - 1); i++) {
		if (softstatep->un_ocmap.reg_open[i] &
		    (1 << DIDPART(*devp))) {
			return (1);
		}
	}
	return (0);
}


#ifdef DEBUG

/* ARGSUSED */
static void
did_log(char *format, ...)
{
	char *tmp;
	static unsigned int index = 0;
	size_t length, remlen;
	int micro_time = (int)((gethrtime() / 10000) % 1000000000);
	va_list ap;

	if (diddbglock_init == 0) {
		mutex_init(&diddbglock, NULL, MUTEX_DRIVER, NULL);
		did_dbg[diddbgnext++] = '\n';
		diddbglock_init = 1;
	}

/*lint -e40 */
	va_start(ap, format);
/*lint -e40 */
	mutex_enter(&diddbglock);

	if (diddebug_cleanup != 0) {
		/*
		 * Cleanup the of the did debug buffer asked
		 */
		diddebug_cleanup = 0;
		diddbgnext = 0;
		index = 0;
		bzero(did_dbg, sizeof (did_dbg));
		did_dbg[diddbgnext++] = '\n';
	}

	tmp = did_str;
	(void) snprintf(tmp, DID_DBG_MSG_SIZE, "%9d:th %p tm %9d ",
		index++, curthread, micro_time);
	tmp += strlen(tmp);
	(void) vsnprintf(tmp, DID_DBG_MSG_SIZE, format, ap);
	tmp = did_str;

	/*
	 * Note the log is circular; if this string would run over the end,
	 * we copy the first piece to the end and then the last piece to
	 * the beginning of the log.
	 */
	length = strlen(tmp);

	remlen = (size_t)sizeof (did_dbg) - diddbgnext;

	if (length > remlen) {
		if (remlen)
			bcopy(tmp, did_dbg + diddbgnext, remlen);
		tmp += remlen;
		length -= remlen;
		diddbgnext = 0;
	}

	bcopy(tmp, did_dbg + diddbgnext, length);
	diddbgnext += length;

	if (diddbgnext >= sizeof (did_dbg))
		diddbgnext = 0;

	mutex_exit(&diddbglock);
	va_end(ap);
}
#endif
