/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)ffmod.c	1.5	08/08/01 SMI"

#include <sys/sol_version.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/open.h>
#include <sys/file.h>
#include <sys/modctl.h>
#include <sys/devops.h>
#include <sys/cmn_err.h>
#include <limits.h>
#include <sys/door.h>
#include "ffmod.h"

/* opaque handle top of state structs */
static void * ffmod_statep;
dev_info_t *ffmod_dip = NULL;
static kmutex_t ffmod_lock;

#if SOL_VERSION >= __s10
struct zarg {
	zoneid_t	zone_id;
	uint64_t	zone_uniqid;

};

static int64_t zoneid_to_zoneuniqid(zoneid_t zone_id);
static void reboot_zone(struct zarg *);

extern pri_t minclsyspri;
#endif

/* forward declaration of entry points */

static	int	ffmod_info(dev_info_t *, ddi_info_cmd_t, void *, void **);
static	int	ffmod_attach(dev_info_t *, ddi_attach_cmd_t);
static	int	ffmod_open(dev_t *, int, int, cred_t *);
static	int	ffmod_close(dev_t, int, int, cred_t *);
static	int	ffmod_ioctl(dev_t, int, intptr_t, int, cred_t *, int *);
static	int	ffmod_detach(dev_info_t *, ddi_detach_cmd_t);

/*
 * static declarations of cb_ops entry point functions
 * for character and lock devices
 * nodev   is for non-supported calls
 * nulldev is a zero-return function
 */
static struct cb_ops ffmod_cb_ops =
{
	ffmod_open,	/* open(9E) */
	ffmod_close,	/* close(9E) */
	nodev,		/* strategy(9E) */
	nodev,		/* print(9E) */
	nodev,		/* dump(9E) */
	nodev,		/* read(9E) */
	nodev,		/* write(9E) */
	ffmod_ioctl,	/* ioctl(9E) */
	nodev,		/* devmap(9E) */
	nodev,		/* mmap(9E) */
	nodev,		/* segmap(9E) */
	nochpoll,	/* chpoll(9E) */
	nodev,		/* prop_op(9E) */
	NULL,		/* streamtab(9S) */
	D_MP | D_64BIT,	/* cb_flag */
	CB_REV,		/* cb_ops version number */
	NULL,		/* aread */
	NULL		/* awrite */
};


/* static declarations of dev_ops entry point functions */
static struct dev_ops ffmod_dev_ops =
{
	DEVO_REV,	/* devo_rev */
	0,		/* devo_refcnt */
	ffmod_info,	/* get_info(9E) : mandatory for cb drivers */
	nulldev,	/* identify(9E) : no longer required */
	nulldev,	/* probe(9E) */
	ffmod_attach,	/* attach(9E) */
	ffmod_detach,	/* detach(9E) */
	nodev,		/* devo_reset */
	&ffmod_cb_ops,	/* devo_cb_ops */
	NULL,		/* devo_bus_ops  */
	NULL		/* power(9E) */
};

/* declare and initialize the module configuration section */

static struct modldrv ffmod_modldrv = {
	&mod_driverops,
	"Failfast module driver v1.0",
	&ffmod_dev_ops
};

static struct modlinkage ffmod_modlinkage = {
	MODREV_1,
	&ffmod_modldrv,
	NULL
};

static int
ffmod_busy(void)
{
	minor_t minor;

	mutex_enter(&ffmod_lock);

	/*
	 * We need to make sure no mappings exist - mod_remove won't
	 * help because the device isn't open.
	 */
	for (minor = 1; minor <= FFMOD_MAX_FF; minor++) {
		if (ddi_get_soft_state(ffmod_statep, (int)minor) != NULL) {
			mutex_exit(&ffmod_lock);
			return (EBUSY);
		}
	}

	mutex_exit(&ffmod_lock);
	return (0);
}


/*
 * Return the minor number 'name' is mapped to, if it is.
 */
static unsigned int
name_to_minor(const char *name)
{
	minor_t	minor;
	struct ffmod_state *fsp;

	ASSERT(mutex_owned(&ffmod_lock));
	for (minor = 1; minor <= FFMOD_MAX_FF; minor++) {
		fsp = ddi_get_soft_state(ffmod_statep,
		    (int)minor);
		if (fsp == NULL)
			continue;
		if (strcmp(fsp->ffmod_name, name) == 0)
			return (minor);
	}
	return (0);
}


static int
ffmod_create(dev_t dev, struct ffmod_ioctl *ufip)
{
	struct ffmod_ioctl *kfip;
	struct ffmod_state *fsp;
	int	error;
	char	namebuf[50];
	minor_t	newminor = 0;
	int zalloced = 0;

	kfip = kmem_alloc(sizeof (struct ffmod_ioctl), KM_SLEEP);
	error = copyin(ufip, kfip, sizeof (struct ffmod_ioctl));
	if (error) {
		kmem_free(kfip, sizeof (struct ffmod_ioctl));
		return (EFAULT);
	}
	/* make sure the name is always null-terminated */
	kfip->ffmod_name[MAXNAMELEN] = '\0';
	kfip->ffmod_minor = 0;

	mutex_enter(&ffmod_lock);

	if (name_to_minor(kfip->ffmod_name) != 0) {
		error = EBUSY;
		goto out;
	}

	/* Find a free minor */
	for (newminor = 1; newminor <= FFMOD_MAX_FF; newminor++)
		if (ddi_get_soft_state(ffmod_statep,
		    (int)newminor) == NULL)
			break;
	if (newminor >= FFMOD_MAX_FF) {
		error = EAGAIN;
		goto out;
	}
	(void) makedevice(getmajor(dev), newminor);

	error = ddi_soft_state_zalloc(ffmod_statep, (int)newminor);
	if (error == DDI_FAILURE) {
		error = ENOMEM;
		goto out;
	}
	zalloced = 1;
	(void) snprintf(namebuf, sizeof (namebuf), "%d", newminor);
	error = ddi_create_minor_node(ffmod_dip, namebuf, S_IFCHR, newminor,
	    DDI_PSEUDO, NULL);
	if (error != DDI_SUCCESS) {
		error = ENXIO;
		goto out;
	}

	fsp = ddi_get_soft_state(ffmod_statep, (int)newminor);

	(void) strcpy(fsp->ffmod_name, kfip->ffmod_name);

	fsp->opened = 0;
	fsp->armed = 0;
#if SOL_VERSION >= __s10
	fsp->zone_id = -1;
	fsp->zone_uniqid = 0;
#endif
	kfip->ffmod_minor = newminor;
	mutex_exit(&ffmod_lock);
	(void) copyout(kfip, ufip, sizeof (struct ffmod_ioctl));
	kmem_free(kfip, sizeof (struct ffmod_ioctl));
	return (0);
out:
	if (zalloced)
		ddi_soft_state_free(ffmod_statep,
		    (int)newminor);
	mutex_exit(&ffmod_lock);
	kmem_free(kfip, sizeof (struct ffmod_ioctl));
	return (error);
}

static int
ffmod_delete(struct ffmod_ioctl *ufip)
{
	struct ffmod_state *fsp;
	struct ffmod_ioctl *kfip;
	minor_t	minor;
	char	namebuf[20];
	int error;

	kfip = kmem_alloc(sizeof (struct ffmod_ioctl), KM_SLEEP);
	error = copyin(ufip, kfip, sizeof (struct ffmod_ioctl));
	if (error) {
		kmem_free(kfip, sizeof (struct ffmod_ioctl));
		return (EFAULT);
	}

	mutex_enter(&ffmod_lock);

	minor = kfip->ffmod_minor;

	if (minor == 0) {
		mutex_exit(&ffmod_lock);
		kmem_free(kfip, sizeof (struct ffmod_ioctl));
		return (ENXIO);
	}

	fsp = ddi_get_soft_state(ffmod_statep, (int)minor);
	if (fsp == NULL) {
		mutex_exit(&ffmod_lock);
		kmem_free(kfip, sizeof (struct ffmod_ioctl));
		return (ENXIO);
	}
	if (fsp->opened) {
		mutex_exit(&ffmod_lock);
		kmem_free(kfip, sizeof (struct ffmod_ioctl));
		return (EBUSY);
	}

	(void) snprintf(namebuf, sizeof (namebuf), "%d", minor);
	ddi_remove_minor_node(ffmod_dip, namebuf);

	ddi_soft_state_free(ffmod_statep, (int)minor);
	mutex_exit(&ffmod_lock);
	kmem_free(kfip, sizeof (struct ffmod_ioctl));
	return (0);
}

int
_init(void)
{
	int error;

	/*
	 * initialize the drivers soft state list
	 */
	if ((error = ddi_soft_state_init(&ffmod_statep,
	    sizeof (ffmod_state), FFMOD_MAX_FF)) != 0)
		return (error);

	mutex_init(&ffmod_lock, NULL, MUTEX_DRIVER, NULL);

	/*
	 * install the driver linkage into the system
	 */
	if ((error = mod_install(&ffmod_modlinkage)) != 0) {
		mutex_destroy(&ffmod_lock);
		ddi_soft_state_fini(&ffmod_statep);
	}
	return (error);
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&ffmod_modlinkage, modinfop));
}

int
_fini(void)
{
	int error;

	/*
	 * first attempt to uninstall the driver linkage.  If
	 * that fails, exit leaving the driver installed
	 */
	if ((error = mod_remove(&ffmod_modlinkage)) != 0)
		return (error);

	/*
	 * free the drivers soft state list
	 */
	ddi_soft_state_fini(&ffmod_statep);

	mutex_destroy(&ffmod_lock);

	return (error);
}

static int
ffmod_attach(dev_info_t *dip, ddi_attach_cmd_t cmd)
{
	int error;

	if (cmd != DDI_ATTACH)
		return (DDI_FAILURE);

	error = ddi_soft_state_zalloc(ffmod_statep, 0);
	if (error == DDI_FAILURE) {
		return (DDI_FAILURE);
	}

	error = ddi_create_minor_node(dip, "ctl", S_IFCHR, 0,
	    DDI_PSEUDO, NULL);

	if (error == DDI_FAILURE) {
		ddi_soft_state_free(ffmod_statep, 0);
		return (DDI_FAILURE);
	}

	ddi_report_dev(dip);

	ffmod_dip = dip;
	return (DDI_SUCCESS);
}

static int
ffmod_detach(dev_info_t *dip, ddi_detach_cmd_t cmd)
{
	if (cmd != DDI_DETACH)
		return (DDI_FAILURE);
	if (ffmod_busy())
		return (DDI_FAILURE);
	ffmod_dip = NULL;
	ddi_remove_minor_node(dip, NULL);
	ddi_soft_state_free(ffmod_statep, 0);
	return (DDI_SUCCESS);
}

static int
ffmod_info(dev_info_t *dip, ddi_info_cmd_t infocmd, void *arg,
    void **result)
{
	switch (infocmd) {
	case DDI_INFO_DEVT2DEVINFO:
		if (ffmod_dip == NULL)
			return (DDI_FAILURE);
		else {
			*result = (void *)ffmod_dip;
			return (DDI_SUCCESS);
		}
	case DDI_INFO_DEVT2INSTANCE:
		*result = 0;
		return (DDI_SUCCESS);
	default:
		/* Should never happen */
		return (DDI_FAILURE);
	}
}

static void
ffmod_failure(ffmod_state * state_p, minor_t minor)
{
#if SOL_VERSION >= __s10
	zone_t *zone_ref;
	char zone_name[ZONENAME_MAX + 1];
	struct zarg *zargp;
	char	namebuf[20];

	if (state_p->zone_id == GLOBAL_ZONEID) {
		(void) strcpy(zone_name, GLOBAL_ZONENAME);
	} else {
		zone_ref = zone_find_by_id(state_p->zone_id);
		(void) strcpy(zone_name, zone_ref->zone_name);
		zone_rele(zone_ref);
	}

	switch (state_p->failure_type) {
	case SEND_MESSAGE:
		zcmn_err(state_p->zone_id, CE_WARN, FFMOD_ERROR_STRING,
		    state_p->ffmod_name, zone_name, state_p->zone_id);
		break;
	case SEND_MESSAGE_AND_PANIC:
		if (state_p->zone_id == GLOBAL_ZONEID) {
			/*
			 * Panic the node if triggering
			 * for global zone
			 */;
			cmn_err(CE_PANIC, FFMOD_ERROR_STRING,
			    state_p->ffmod_name, zone_name, state_p->zone_id);
		} else {
			zcmn_err(state_p->zone_id, CE_WARN, FFMOD_ERROR_STRING,
			    state_p->ffmod_name, zone_name, state_p->zone_id);

			(void) snprintf(namebuf, sizeof (namebuf), "%d", minor);
			ddi_remove_minor_node(ffmod_dip, namebuf);
			ddi_soft_state_free(ffmod_statep, (int)minor);
			/*
			 * Non-global zone - Rebooting zone
			 * Now, create the thread to contact zoneadmd and do
			 * the rest of the work.
			 */
			zargp = kmem_alloc(sizeof (*zargp), KM_SLEEP);
			zargp->zone_id = state_p->zone_id;
			zargp->zone_uniqid = state_p->zone_uniqid;

			(void) thread_create(NULL, 0, reboot_zone, zargp, 0,
			    &p0, TS_RUN, minclsyspri);
		}
		break;
	default:
		break;
	}
#else
	switch (state_p->failure_type) {
	case SEND_MESSAGE:
		cmn_err(CE_WARN, FFMOD_ERROR_STRING, state_p->ffmod_name);
		break;
	case SEND_MESSAGE_AND_PANIC:
		cmn_err(CE_PANIC, FFMOD_ERROR_STRING, state_p->ffmod_name);
		break;
	default:
		break;
	}
#endif
}

static int
ffmod_open(dev_t *devp, int flag, int otyp, cred_t *credp)
{
	minor_t minor;
	struct ffmod_state *fsp;

	mutex_enter(&ffmod_lock);
	minor = getminor(*devp);

	if (minor == 0) {
		/* master control device */
		if (otyp != OTYP_CHR) {
			mutex_exit(&ffmod_lock);
			return (EINVAL);
		}
		fsp = ddi_get_soft_state(ffmod_statep, 0);
		if (fsp == NULL) {
			mutex_exit(&ffmod_lock);
			return (ENXIO);
		}
		if (fsp->opened) {
			mutex_exit(&ffmod_lock);
			return (EBUSY);
		}
		fsp->opened = 1;
		mutex_exit(&ffmod_lock);
		return (0);
	}

	/* otherwise, the mapping should already exist */
	fsp = ddi_get_soft_state(ffmod_statep, (int)minor);

	if (fsp == NULL) {
		mutex_exit(&ffmod_lock);
		return (EINVAL);
	}

	fsp->opened = 1;
#if SOL_VERSION >= __s10
		fsp->zone_id = getzoneid();
		fsp->zone_uniqid = zoneid_to_zoneuniqid(fsp->zone_id);
#endif
	mutex_exit(&ffmod_lock);
	return (0);
}

static int
ffmod_close(dev_t dev, int flag, int otyp, cred_t *credp)
{
	minor_t minor;
	ffmod_state * fsp;

	mutex_enter(&ffmod_lock);
	minor = getminor(dev);

	fsp = (ffmod_state *) ddi_get_soft_state(
		ffmod_statep, (int)minor);
	if (fsp == NULL) {
		mutex_exit(&ffmod_lock);
		return (EINVAL);
	}
	if (fsp->armed == 1) {
		/*
		 * abnormal exit : called failure function
		 */
		ffmod_failure(fsp, minor);
	}

	fsp->opened = 0;
#if SOL_VERSION >= __s10
	fsp->zone_id = -1;
	fsp->zone_uniqid = 0;
#endif
	mutex_exit(&ffmod_lock);
	return (0);
}

static int
ffmod_ioctl(dev_t dev, int cmd, intptr_t arg, int flag,
    cred_t *credp, int *rvalp)
{
	ffmod_state *fsp;
	minor_t minor;

	minor = getminor(dev);
	if (minor == 0) {
		struct ffmod_ioctl *fip = (struct ffmod_ioctl *)arg;
		switch (cmd) {
		case FFMOD_IOC_CREATE:
			if ((flag & FWRITE) == 0)
				return (EPERM);
			return (ffmod_create(dev, fip));
		case FFMOD_IOC_DELETE:
			if ((flag & FWRITE) == 0)
				return (EPERM);
			return (ffmod_delete(fip));
		default:
			return (ENOTTY);
		}
	}

	fsp = ddi_get_soft_state(ffmod_statep, (int)minor);
	if (fsp == NULL)
		return (ENXIO);

	switch (cmd) {
	case FFMOD_IOC_ARM:
		fsp->armed = 1;
		break;
	case FFMOD_IOC_DISARM:
		fsp->armed = 0;
		break;
	case FFMOD_IOC_SCONF:
		if (ddi_copyin((void *) arg,
		    &(fsp->failure_type),
		    sizeof (failure_function_type), flag) != 0) {
			return (EFAULT);
		}
		break;
	default:
		/* generic "ioctl unknown" error */
		return (ENOTTY);
	}
	return (0);
}

#if SOL_VERSION >= __s10
/* Helper function to get zone_uniqid, given the zone_id */
static int64_t
zoneid_to_zoneuniqid(zoneid_t zone_id)
{
	uint64_t zone_uniqid;
	zone_t *zone_ref = zone_find_by_id(zone_id);
	if (zone_ref == NULL) {
		return (-1);
	}
	zone_uniqid = zone_ref->zone_uniqid;
	zone_rele(zone_ref);
	return ((int64_t)zone_uniqid);
}

/* Function to reboot the zone having the given zone_id and zone_uniqid */
static void
reboot_zone(struct zarg *zargp)
{
	zone_t		*zone_ref;
	char		zone_name[ZONENAME_MAX + 1];
	char		doorpath[PATH_MAX + 1];
	door_handle_t	door;
	door_arg_t	darg;
	long		error_code;
	zoneid_t	zone_id;
	uint64_t	zone_uniqid;

	zone_id = zargp->zone_id;
	zone_uniqid = zargp->zone_uniqid;
	kmem_free(zargp, sizeof (*zargp));

	zone_ref = zone_find_by_id(zone_id);

	if (zone_ref == NULL) {
		cmn_err(CE_NOTE, "Failfast: zone id %d does not correspond "
		    "to any zone - No reboot possible\n", zone_id);
		return;
	}

	/* Do nothing in case the zone is already down. */
	if (zone_ref->zone_status >= ZONE_IS_DOWN) {
		zone_rele(zone_ref);
		return;
	}

	(void) strcpy(zone_name, zone_ref->zone_name);
	zone_rele(zone_ref);

	/*
	 * Check that we are failfasting the zone generation
	 * that was intended to be failfasted, and not any
	 * future generation of the same zone.
	 * zone_uniqid uniquely identifies a zone generation.
	 */
	if ((int64_t)zone_uniqid == zoneid_to_zoneuniqid(zone_id)) {
		cmn_err(CE_NOTE, "Failfast: Rebooting zone %s with "
		    "zone_id %d\n", zone_name, zone_id);

		/*
		 * 'zoneadmd' daemon creates a door at a well-known location,
		 * and acts as the door server. To reboot a zone, we can make
		 * a door upcall to the door for the target zone, and
		 * 'zoneadmd' will reboot the zone for us.
		 */

		/*
		 * Arguments required for the zone reboot request to 'zoneadmd'
		 */
		zone_cmd_arg_t zarg;
		zarg.cmd = Z_REBOOT;
		zarg.uniqid = zone_uniqid;
		(void) strcpy(zarg.locale, "C");

		/* Path to the file, acting as the door */
		if (snprintf(doorpath, (size_t)(PATH_MAX + 1),
		    ZONE_DOOR_PATH, zone_name) >= PATH_MAX) {
			/* NULL terminate on overflow */
			doorpath[PATH_MAX] = '\0';
		}

		if (error_code = door_ki_open(doorpath, &door)) {
			cmn_err(CE_WARN,
			    "Failfast : Failed to reboot zone %s (zone ID %d) "
			    "as opening of 'zoneadmd' door failed with error "
			    "code %d. Failfast will halt the zone now.\n",
			    zone_name, zone_id, error_code);

			/* Zone reboot failed, try to halt the zone. */
			error_code =
			    zone(ZONE_SHUTDOWN, (void *)zone_id,
				NULL, NULL, NULL);
			if (error_code) {
				cmn_err(CE_WARN, "Failfast : Zone halt failed "
				    "for zone %s with zone_id %d, error %d\n",
				    zone_name, zone_id, error_code);
			}

			return;
		}

		/* Door upcall data */
		darg.data_ptr = (char *)&zarg;
		darg.data_size = sizeof (zarg);
		darg.desc_ptr = NULL;
		darg.desc_num = 0;
		darg.rbuf = (char *)&zarg;
		darg.rsize = sizeof (zarg);

		/* Make the door upcall */
		if (error_code = door_ki_upcall(door, &darg) != 0) {
			door_ki_rele(door);
			cmn_err(CE_NOTE,
			    "Failfast : Failed to reboot zone %s (zone ID %d) "
			    "as door upcall to 'zoneadmd' door failed with "
			    "error code %d. Failfast will halt the zone now.\n",
			    zone_name, zone_id, error_code);

			/* Zone reboot failed, try to halt the zone. */
			error_code =
			    zone(ZONE_SHUTDOWN, (void *)zone_id,
				NULL, NULL, NULL);
			if (error_code) {
				cmn_err(CE_WARN, "Failfast : Zone halt failed "
				    "for zone %s with zone_id %d, error %d\n",
				    zone_name, zone_id, error_code);
			}

			return;
		}

		/* Release the reference to the door. */
		door_ki_rele(door);

	} else {
		cmn_err(CE_NOTE, "Failfast: Zone id %d doesn't correspond to "
		    "zone unique id %lld; zone %s might have been rebooted\n",
		    zone_id, zone_uniqid, zone_name);
	}
}
#endif	/* SOL_VERSION >= __s10 */
