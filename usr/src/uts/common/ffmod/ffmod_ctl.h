/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FFMOD_CTL_H
#define	_FFMOD_CTL_H

#pragma ident	"@(#)ffmod_ctl.h	1.3	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

#define	FFMOD_IOC	('F' << 8)

#define	FFMOD_IOC_ARM		(FFMOD_IOC | 1)
#define	FFMOD_IOC_DISARM	(FFMOD_IOC | 2)
#define	FFMOD_IOC_SCONF		(FFMOD_IOC | 3)
#define	FFMOD_IOC_CREATE	(FFMOD_IOC | 4)
#define	FFMOD_IOC_DELETE	(FFMOD_IOC | 5)

struct ffmod_ioctl {
	uint32_t ffmod_minor;
	char    ffmod_name[MAXNAMELEN + 1];
};

typedef enum failure_function_type
{
	SEND_MESSAGE = 0,
	SEND_MESSAGE_AND_PANIC,
	SEND_SYSEVENT
} failure_function_type;

#ifdef  __cplusplus
}
#endif

#endif /* _FFMOD_CTL_H */
