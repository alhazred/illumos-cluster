/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FFMOD_H
#define	_FFMOD_H

#pragma ident	"@(#)ffmod.h	1.5	08/05/20 SMI"

#ifdef  __cplusplus
extern "C" {
#endif

#include	"ffmod_ctl.h"

#include <sys/conf.h>

#if SOL_VERSION == __s10
#include <zone.h>
#endif
/* must be the two last includes */
#include <sys/ddi.h>
#include <sys/sunddi.h>

#define	FFMOD_MAX_FF	100
#if SOL_VERSION >= __s10
#define	FFMOD_ERROR_STRING \
"Failfast : timeout - unit %s of zone %s (zone ID %d) "\
"unreferenced while armed\n"
#else
#define	FFMOD_ERROR_STRING \
"Failfast : timeout - unit %s unreferenced while armed\n"
#endif

typedef struct ffmod_state
{
#if SOL_VERSION >= __s10
	/*
	 * On Solaris10, the ffmod_name is built as follows:
	 * Name of the client (MAXNAMELEN) + "_" + local zone name
	 * (ZONENAME_MAX)
	 */
	char			ffmod_name[MAXNAMELEN + ZONENAME_MAX + 2];
#else
	char			ffmod_name[MAXNAMELEN];
#endif
	int			armed;
	int			opened;
	failure_function_type	failure_type;
#if SOL_VERSION >= __s10
	zoneid_t		zone_id;
	uint64_t		zone_uniqid;
#endif
} ffmod_state;

#ifdef  __cplusplus
}
#endif

#endif /* _FFMOD_H */
