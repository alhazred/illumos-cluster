#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)Makefile.rules	1.13	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# uts/common/Makefile.rules
#
#	This Makefile defines all the file build rules for the directory
# uts/common and its children. These are the source files which may
# be considered common to all SunOS systems.
#

#
#	This was implemented by pattern matching rules of the form:
#
# $(OBJS_DIR)/%.o:	 $(UTSBASE)/common/os/%.c
#	$(COMPILE.c) -o $@ $<
#
#	For each directory containing source. This proved too slow under
#	Nse because it caused a significant additional number of stats
#	and stats are real slow under TFS. If TFS performance should
#	improve, one might consider returning to the former implementation.
#	In the former implementation, this file (Makefile.rules) was not
#	needed and the pattern matching rules were in Makefile.files.
#

#
#	C objects build rules
#
#	The following are alphabetized by source file path. Please maintain
# this ordering.
#
$(OBJS_DIR)/did.o:		$(UTSBASE)/common/io/did/did.c
	$(COMPILE.c) -o $@ $(UTSBASE)/common/io/did/did.c
	$(CTFCONVERT_O.c)

%.c_check: $(UTSBASE)/common/io/did/%.c
	$(DOT_C_CHECK)

%.h_check: $(UTSBASE)/common/io/did/%.h
	$(DOT_H_CHECK)
$(OBJS_DIR)/rsmrdt.o:		$(UTSBASE)/common/io/rsmrdt/rsmrdt.c
	$(COMPILE.c) -o $@ $(UTSBASE)/common/io/rsmrdt/rsmrdt.c
	$(CTFCONVERT_O.c)

$(OBJS_DIR)/rsmrdt_pm.o:	$(UTSBASE)/common/io/rsmrdt/rsmrdt_pm.c
	$(COMPILE.c) -o $@ $(UTSBASE)/common/io/rsmrdt/rsmrdt_pm.c
	$(CTFCONVERT_O.c)

$(OBJS_DIR)/rsmrdt_util.o:	$(UTSBASE)/common/io/rsmrdt/rsmrdt_util.c
	$(COMPILE.c) -o $@ $(UTSBASE)/common/io/rsmrdt/rsmrdt_util.c
	$(CTFCONVERT_O.c)

%.c_check: $(UTSBASE)/common/io/rsmrdt/%.c
	$(DOT_C_CHECK)

%.h_check: $(UTSBASE)/common/io/rsmrdt/%.h
	$(DOT_H_CHECK)

#
# Rules for Europa hbxdrv/hbxmod build
#

$(OBJS_DIR)/hbxdrv.o:		$(UTSBASE)/common/net/hbxdrv/hbxdrv.c
	$(COMPILE.c) -o $@ $(UTSBASE)/common/net/hbxdrv/hbxdrv.c
	$(CTFCONVERT_O.c)

$(OBJS_DIR)/hmac_sha1.o:	$(UTSBASE)/common/net/hbxdrv/hmac_sha1.c
	$(COMPILE.c) -o $@ $(UTSBASE)/common/net/hbxdrv/hmac_sha1.c
	$(CTFCONVERT_O.c)

$(OBJS_DIR)/sha1.o:		$(UTSBASE)/common/net/hbxdrv/sha1.c
	$(COMPILE.c) -o $@ $(UTSBASE)/common/net/hbxdrv/sha1.c
	$(CTFCONVERT_O.c)

%.c_check: $(UTSBASE)/common/net/hbxdrv/%.c
	$(DOT_C_CHECK)

%.h_check: $(UTSBASE)/common/net/hbxdrv/%.h
	$(DOT_H_CHECK)

$(OBJS_DIR)/hbxmod.o:		$(UTSBASE)/common/net/hbxmod/hbxmod.c
	$(COMPILE.c) -o $@ $(UTSBASE)/common/net/hbxmod/hbxmod.c
	$(CTFCONVERT_O.c)

%.c_check: $(UTSBASE)/common/net/hbxmod/%.c
	$(DOT_C_CHECK)

%.h_check: $(UTSBASE)/common/net/hbxmod/%.h
	$(DOT_H_CHECK)

$(OBJS_DIR)/ffmod.o:		$(UTSBASE)/common/ffmod/ffmod.c
	$(COMPILE.c) -o $@ $(UTSBASE)/common/ffmod/ffmod.c
	$(CTFCONVERT_O.c)

%.c_check: $(UTSBASE)/common/ffmod/%.c
	$(DOT_C_CHECK)

%.h_check: $(UTSBASE)/common/ffmod/%.h
	$(DOT_H_CHECK)
#
# Define preprocessing variable according to the targeted OS:
#
#	o Solaris for the SunOSs
#
#
$(EUROPA)CPPFLAGS	+= -DSolaris

#
# Add path to hbxmod/hbxdrv include file
#
$(EUROPA)CPPFLAGS	+= -I$(UTSBASE)/common/net

#
# Add pre-compile variable for EUROPA specific code
#
$(EUROPA)CPPFLAGS	+= -DEUROPA
