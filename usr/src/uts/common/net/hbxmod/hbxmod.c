/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma	ident	"@(#)hbxmod.c 1.3     08/05/20 SMI"

#include <sys/stream.h>
#include <sys/conf.h>
#include "./hbxmod.h"



/*
 * Functions prototypes
 */
static int hbxmod_open(queue_t *, dev_t *, int, int, cred_t *);
static int hbxmod_close(queue_t *, int, cred_t *);
static int hbxmod_rput(queue_t *, mblk_t *);
static int hbxmod_wput(queue_t *, mblk_t *);



/*
 * Global data
 */
static int hbxmod_opened = 0;
static hbxmod_ops_t *hbxmod_ops = NULL; /* hbxmod module hooks handling */
static int hbxmod_hooks = 0;

#ifdef	HBXMOD_DEBUG

/*
 * to see what's going during
 * network flooding in the upstream
 * record each packet arrival time
 * put hbxmod_do_display to 1
 * with mdb after a flooding
 */
#define	HBXMOD_DEBUG_PKTS	10240

static uint64_t hbxmod_up_cnt = 0;
static uint32_t hbxmod_dbg_pkt = 0;

typedef struct hbxmod_dbg_t {
	hrtime_t time;
} hbxmod_dbg_t;

hbxmod_dbg_t hbxmod_dbg[HBXMOD_DEBUG_PKTS];

int hbxmod_do_display = 0;

#endif



/*
 * Stream module definition
 */
static struct module_info hbxmod_info = {
	HBXMOD_ID,
	HBXMOD_NAME,
	HBXMOD_MIN_PKT,
	HBXMOD_MAX_PKT,
	HBXMOD_HIWATER,
	HBXMOD_LOWATER
};


static struct qinit hbxmod_rinit = {
	(int (*)())hbxmod_rput,
	(int (*)())NULL,
	hbxmod_open,
	hbxmod_close,
	nulldev,
	&hbxmod_info,
	NULL
};


static struct qinit hbxmod_winit = {
	(int (*)())hbxmod_wput,
	(int (*)())NULL,
	hbxmod_open,
	hbxmod_close,
	nulldev,
	&hbxmod_info,
	NULL
};


static struct streamtab hbxmodinfo = {
	&hbxmod_rinit,
	&hbxmod_winit,
	NULL,
	NULL
};


static struct fmodsw fsw = {
	HBXMOD_NAME,
	&hbxmodinfo,
	HBXMOD_MTFLAGS
};


/* Module linkage information for the kernel */
static struct modlstrmod modlstrmod = {
	&mod_strmodops, HBXMOD_NAME, &fsw
};


static struct modlinkage modlinkage = {
	MODREV_1, &modlstrmod, NULL
};



/*
 * Call at module init time
 */
int
_init(void)
{
	return (mod_install(&modlinkage));
}



/*
 * Call at module fini time
 */
int
_fini(void)
{
	return (mod_remove(&modlinkage));
}



/*
 * Call for module info
 */
int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}



/*
 * hbxmod_open - open routine gets called when the
 * module gets pushed onto the stream.
 */
/*ARGSUSED*/
static int
hbxmod_open(queue_t *q, dev_t	*dev, int flag, int sflag, cred_t *cr)
{
	int ret;

	/* Check if already open */
	if (q->q_ptr != NULL) {
		/*
		 * This instance has already been opened.
		 * Nothing to do.
		 */
		return (0);
	}

	if (sflag != MODOPEN) {
		return (ENXIO);
	}

	WR(q)->q_ptr = q->q_ptr = NULL;

	if ((hbxmod_hooks != 0) && (hbxmod_ops != NULL) &&
	    (hbxmod_ops->open != NULL)) {
		if ((ret = hbxmod_ops->open(q, dev)) != 0) {
			return (ret);
		}
		/*
		 * q->q_ptr must be valued non NULL by a succesfull
		 * hbxmod_ops->open(q, dev) call.
		 */
		ASSERT(q->q_ptr != NULL);
	}

	qprocson(q);

	if ((q->q_ptr != NULL) && (hbxmod_ops->physinfo != NULL) &&
	    ((ret = hbxmod_ops->physinfo(q, dev)) != 0)) {
		/*
		 * hbxmod_ops->physinfo(q, dev) must call
		 * qprocsoff in case of failure.
		 */
		return (ret);
	}

	hbxmod_opened++;

	return (0);
}



/*
 * hbxmodclose - This routine gets called when the module
 * gets popped off of the stream.
 */
/*ARGSUSED1*/
static int
hbxmod_close(queue_t *q, int flag, cred_t *cr)
{

	qprocsoff(q);

	if ((q->q_ptr != NULL) && (hbxmod_ops->close != NULL)) {
		(void) hbxmod_ops->close(q);
	}

	hbxmod_opened--;

	return (0);
}



/*
 * hbxmodrput - Module read queue put procedure.
 * This is called from the module downstream.
 */
static int
hbxmod_rput(queue_t *q, mblk_t *mp)
{

#ifdef	HBXMOD_DEBUG
	uint32_t i;
	hrtime_t prev = 0;

	hbxmod_up_cnt++;

	if (hbxmod_do_display != 0) {
		hbxmod_do_display = 0;
		for (i = 0; i < hbxmod_dbg_pkt; i++) {
			if (prev == 0) {
				printf("%d time %lld\n", i, hbxmod_dbg[i]);
			} else {
				printf("%d time %lld diff %lld\n",
					i, hbxmod_dbg[i], hbxmod_dbg[i] - prev);
			}
			prev = hbxmod_dbg[i];
		}
		hbxmod_dbg_pkt = 0;
		bzero((void *)&hbxmod_dbg, sizeof (hbxmod_dbg));
	}

	hbxmod_dbg[hbxmod_dbg_pkt].time = gethrtime();
	hbxmod_dbg_pkt++;
	if (hbxmod_dbg_pkt >= HBXMOD_DEBUG_PKTS) {
		hbxmod_dbg_pkt = 0;
	}
#endif

	if ((q->q_ptr != NULL) &&
	    (hbxmod_ops->rput != NULL) && (hbxmod_ops->rput(q, mp) != 0)) {
		freemsg(mp);
		return (0);
	}
	putnext(q, mp);
	return (0);
}



/*
 * hbxmodwput - Module write queue put procedure.
 * A simple pass-through.
 */
static int
hbxmod_wput(queue_t *q, mblk_t *mp)
{
	putnext(q, mp);
	return (0);
}



/*
 * hbxmod hooks are registered from 'outside' by invoking this function.
 * This function is not static: it will be called from the outside,
 * by the hbdrv driver, at hbdrv init time. hbdrv driver cannot be
 * removed once loaded, so hbxmod_ops is always valid, once set.
 */
int
hbxmod_set_hooks(hbxmod_ops_t *ops)
{
	if (ops == NULL) {
		return (EINVAL);
	}
	if (ops->hbxmod_rev == HBXMOD_HOOK_REV) {
		hbxmod_ops = ops;
		hbxmod_hooks = 1;
		return (0);
	}
	cmn_err(CE_WARN,
		"version mismatch: "
		" asked %d, me %d\n",
		ops->hbxmod_rev,
	    HBXMOD_HOOK_REV);
	return (EINVAL);
}
