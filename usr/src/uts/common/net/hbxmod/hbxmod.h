/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBXMOD_H
#define	_HBXMOD_H

#pragma ident	"@(#)hbxmod.h	1.3	08/05/20 SMI"


#ifdef  __cplusplus
extern "C" {
#endif

#include	<sys/debug.h>
#include	<sys/errno.h>
#include	<sys/modctl.h>
#include	<sys/ddi.h>
#include	<sys/cmn_err.h>

/*
 * Definitions
 */
#define	HBXMOD_HOOK_REV_1	1
#define	HBXMOD_HOOK_REV		HBXMOD_HOOK_REV_1

#define	HBXMOD_ID		0x1238
#define	HBXMOD_NAME		"hbxmod"
#define	HBXMOD_MIN_PKT		0
#define	HBXMOD_MAX_PKT		INFPSZ
#define	HBXMOD_HIWATER		0
#define	HBXMOD_LOWATER		0

#define	HBXMOD_MTFLAGS		(D_NEW | D_MP)



/* Hooks for the hb driver */
typedef struct hbxmod_ops_t {
	int	hbxmod_rev;
	int	(*rput)(queue_t *, mblk_t *);
	int	(*open)(queue_t *, dev_t *);
	int	(*physinfo)(queue_t *, dev_t *);
	int	(*close)(queue_t *);
} hbxmod_ops_t;


extern int hbxmod_set_hooks(hbxmod_ops_t *ops);



#ifdef  __cplusplus
}
#endif

#endif	/* !_HBXMOD_H */
