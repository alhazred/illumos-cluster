/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HBX_SHA1_H
#define	_HBX_SHA1_H

#pragma ident	"@(#)sha1.h	1.3	08/05/20 SMI"


#ifdef Solaris
#include <sys/types.h>
#endif
#ifdef linux
#include <stdint.h>
#endif

#ifdef	__cplusplus
extern "C" {
#endif


/* SHA-1 context. */
typedef struct 	{
	uint32_t state[5];	/* state (ABCDE) */
	uint32_t count[2];	/* number of bits, modulo 2^64 (msb first) */
	union 	{
		uint8_t		buf8[64];	/* undigested input */
		uint32_t	buf32[16];	/* realigned input */
	} buf_un;
} SHA1_CTX;

void
hbx_SHA1Init(SHA1_CTX *ctx);

void
hbx_SHA1Update(SHA1_CTX *ctx, const uint8_t *input, uint32_t input_len);

void
hbx_SHA1Final(uint8_t *digest, SHA1_CTX *ctx);

#ifdef	__cplusplus
}
#endif

#endif /* _HBX_SHA1_H */
