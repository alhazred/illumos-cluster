/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBR_H
#define	_HBR_H

#pragma ident	"@(#)hbr.h	1.3	08/05/20 SMI"


#include <hbxdrv/hbt.h>

#ifdef  __cplusplus
extern "C" {
#endif


/*
 * ***************************************************************************
 * Detected-host table management types and macros
 */
#define	HBX_DEFAULT_MAX_HOSTS	1024 /* default host hash table size */
#define	HBX_MIN_HOSTS		1    /* minimum host hash table size */

/*
 * Hash function used to handle host per device -------------------------------
 */
#define	HBX_HOST_HASH(addr, table_size) \
	(((addr) ^ ((addr) >> 8) ^ ((addr) >> 16) \
	^ ((addr) >> 24)) % table_size)

#define	HBX_HOST_FLAG_NONE	0x0 /* host entry is free */
#define	HBX_HOST_FLAG_USED	0x1 /* host entry is used */
#define	HBX_HOST_FLAG_COLL	0x2 /* host collision area is in use */

/*
 * hbdrv driver, host table per device
 */
typedef struct hbx_host_t {
	int			flag;		/* hash hosttable entry flag */
	enum hbx_host_state	hstate;		/* basically, UP or DOWN */
	ipaddr_t		addr;		/* hb emitter src addr */
	timeout_id_t		tid;		/* qtimeout timer id */
	clock_t			ticks;		/* detection delay, in ticks */
	hbx_delay_t		delay;		/* detection delay, in ms */
	struct hbx_host_t	*next;		/* list of host/device */
	struct hbx_host_t	*prev;		/* list of host/device */
	struct hbx_host_t	*c_next;	/* collision host list */
	struct hbx_host_t	*c_prev;	/* collision host list */
	struct hbx_device_t	*pdev;		/* back pointer to device */
	hbx_node_id_t		node_id;	/* node id */
	hbx_sn_t		sn;		/* hb msg sequence number */
	hbx_in_t		in;		/* host incarnation number */
	uint8_t			error;		/* signal in error hb msg */
} hbx_host_t;


/*
 * **************************************************************************
 * Error types, macro and definition
 */

#define	HBX_ERROR_SUCCESS	0
#define	HBX_ERROR_IN		1
#define	HBX_ERROR_UNEXP_IN	2
#define	HBX_ERROR_SN		3
#define	HBX_ERROR_AUTH		4
#define	HBX_ERROR_PROTO		5
#define	HBX_ERROR_DELAY		6
#define	HBX_ERROR_CLID_LEN	7
#define	HBX_ERROR_CLID		8
#define	HBX_ERROR_NODE_ID	9

static char *hbx_error[] = {
	"Success",
	"Unchanged incarnation number for host %x",
	"Unexpected incarnation number change for host %x",
	"Bad sequence number for host %x",
	"Signature check has failed for host %x",
	"Wrong protocol version for host %x",
	"Wrong detection delay for host %x",
	"Wrong cluster id length for host %x",
	"Wrong cluster id for host %x",
	"Unexpected node id change for host %x"
};

typedef struct hbx_error_t {
	ipaddr_t src;
	uint8_t  error_code;
} hbx_error_t;

#define	HBX_CREATE_ERROR(e, s, c) \
	e->src = s; e->error_code = c;

/*
 * Test error macro (for periodic error logging in syslog) --------------------
 */
#define	HBX_TEST_ERROR(h) \
	(h->error == hbx_error_limit) || (h->error == 0)


/*
 * **************************************************************************
 * HBR internal functions prototypes
 */

/*
 * Detected host management fucntions
 */
static void
hbx_add_host(queue_t *, unsigned int, ipaddr_t *, clock_t, hbx_hb_data_t *);
static int
hbx_del_host(hbx_device_t *, ipaddr_t *);
static int
hbx_init_hosttable(hbx_device_t *);
static void
hbx_free_hosttable(hbx_device_t *);
static hbx_host_t *
hbx_findhost(hbx_device_t *, ipaddr_t *);
static hbx_host_t *
hbx_search_host(queue_t *, ipaddr_t *, unsigned int);

/*
 * Heartbeat reception/supervision/filering/signalisation functions
 */
static int
hbx_filterheartbeat(queue_t *, mblk_t *);
static int
hbx_isheartbeat(queue_t *, mblk_t *, ipha_t *);
static void
hbx_hdlheartbeat(queue_t *, ipaddr_t *, hbx_hb_data_t *, hbx_error_t *);
static void
hbx_hbtimeout(void *);
static int
hbx_signalevt(hbx_host_t *, enum hbx_event_type);
static int
hbx_signalevtid(hbx_device_t *, enum hbx_event_type);

/*
 * Hearbeat message packet check functions
 */
static int
hbx_check_msg(queue_t *, hbx_msg_t *, ipaddr_t, hbx_error_t *);
static int
hbx_check_udph(struct udphdr *, ipaddr_t);
static int
hbx_check_iph(ipha_t *);
static int
hbx_check_signature(queue_t *, ipaddr_t, hbx_msg_t *, hbx_pkey_t *);

/*
 * Error management functions
 */
static void
hbx_log_error(hbx_error_t *hb_error, hbx_host_t *phost);


#ifdef  __cplusplus
}
#endif

#endif	/* !_HBR_H */
