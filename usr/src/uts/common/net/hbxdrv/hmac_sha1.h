/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HBX_HMAC_SHA1_H
#define	_HBX_HMAC_SHA1_H

#pragma ident	"@(#)hmac_sha1.h	1.3	08/05/20 SMI"


#include <sys/types.h>
#include <hbxdrv/sha1.h>

#ifdef	__cplusplus
extern "C" {
#endif

#define	HMAC_DIGEST_LEN	20

void
hbx_HMACInit(SHA1_CTX *sha1Context, const uchar_t *, size_t);

void
hbx_HMACUpdate(SHA1_CTX *sha1Context, const uchar_t *, size_t);

void
hbx_HMACFinal(SHA1_CTX *sha1Context, const uchar_t *, size_t,
    uchar_t digest[HMAC_DIGEST_LEN]);


#ifdef	__cplusplus
}
#endif

#endif /* _HBX_HMAC_SHA1_H */
