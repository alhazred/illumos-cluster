/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HBT_H
#define	_HBT_H

#pragma ident	"@(#)hbt.h	1.3	08/05/20 SMI"


#include <sys/hbx.h>
#include <hbxmod/hbxmod.h>
#include <sys/sunddi.h>
#include <sys/stream.h>
#include <netinet/udp.h>
#include <inet/common.h>
#include <inet/ip.h>

#ifdef  __cplusplus
extern "C" {
#endif

/*
 * hbxdrv driver setup ********************************************************
 */
#define	HBX_AWAIT_MAX		32		/* max msg bef. DL_INFO_ACK */
#define	HBX_MIN_DRIFT_FREQ	150000000	/* in nanoseconds = 150 ms */

/*
 * hbxdrv driver exports hooks, hooks versioning ------------------------------
 */
#define	HBX_HOOK_REV_1		1
#define	HBX_HOOK_REV		HBX_HOOK_REV_1

/*
 * hbxdrv driver device -------------------------------------------------------
 */
typedef struct hbx_device_t {
	queue_t			*q;		/* queue to the MAC driver */
	krwlock_t		lock;		/* protect this data struct */
	dev_t			dev;		/* device identification */
	unsigned int		id;		/* user pushed ident. */
	enum hbx_emit_state	state;		/* NONE, READY, RUN */
	mblk_t			*heartbeat;	/* heartbeat to send */
	mblk_t			*fp_heartbeat;	/* fast_path hb to send */
	hbx_sn_t		sn;		/* hb msg sequence number */
	hbx_in_t		in;		/* incarnation number */
	enum hbx_mode_t		mode;		/* rcv and or emt mode */
	unsigned int		mac_info;	/* MACinfo. get from driver? */
	unsigned int		mac_ok;		/* MAC is a supp. type&vers */
	unsigned int		mac_version;	/* MAC version: DLPI version */
	unsigned int		mac_type;	/* MAC type: ex DL_ETHER */
	unsigned int		mac_len;	/* MAC address lenght */
	unsigned int		mac_sap_len;	/* MAC sap address lenght */
	unsigned int		mac_sap_off;	/* MAC sap offset */
	hbx_mac_addr_t		mac_src;	/* heartbeat MAC src address */
	hbx_mac_addr_t		mac_dst;	/* heartbeat MAC dst address */
	hbx_mac_addr_t		mac_bca;	/* MAC bcst/mcst address */
	hbx_mac_addr_t		mac_sap;	/* MAC sap address */
	ipaddr_t		src;		/* heartbeat source IP addr */
	ipaddr_t		dst;		/* heartbeat dst IP address */
	struct hbx_host_t	*hosttable;	/* host table for that dev. */
	uint32_t		hosttable_size;	/* host table size */
	struct hbx_host_t	*hosts;		/* linked list of hosts */
	struct hbx_device_t	*next;		/* linked list of devices */
	struct hbx_device_t	*prev;		/* linked list of devices */
	unsigned int		max_hosts;	/* maximum # of hosts */
	unsigned int		host_nb;	/* current # of hosts */
	unsigned int		host_coll_nb;	/* current # of collisions */
} hbx_device_t;

/*
 * hbsdrv driver stream setup *************************************************
 */
#define	HBX_ID			0x1239
#define	HBX_NAME		"hbxdrv"
#define	HBX_MIN_PKT		0
#define	HBX_MAX_PKT		INFPSZ
#define	HBX_HIWATER		0
#define	HBX_LOWATER		0
#define	HBX_MTFLAGS		(D_NEW | D_MP | D_MTPERMOD)

/*
 * hbsdrv driver clients list -------------------------------------------------
 */
typedef struct hbx_upstream_t {
	queue_t			*q;
	struct hbx_upstream_t	*next;
	struct hbx_upstream_t	*prev;
} hbx_upstream_t;


/*
 * ***************************************************************************
 * HBR internal functions prototypes
 */

/*
 * Standard driver functions
 */
static int
hbx_open(queue_t *, dev_t *, int, int, cred_t *);
static int
hbx_close(queue_t *);
static int
hbx_wput(queue_t *, mblk_t *);
static void
hbx_ioctl(queue_t *q, mblk_t  *mp);
static int
hbx_attach(dev_info_t *, ddi_attach_cmd_t);
static int
hbx_detach(dev_info_t *, ddi_detach_cmd_t);
static int
hbx_getinfo(dev_info_t *, ddi_info_cmd_t, void *, void **);
static int
hbx_rput_hook(queue_t *, mblk_t *);
static int
hbx_open_hook(queue_t *, dev_t *);
static int
hbx_physinfo_hook(queue_t *, dev_t *);
static int
hbx_close_hook(queue_t *);
static void
hbx_open_error(queue_t *);

/*
 * DL management functions
 */
static mblk_t *
hbx_dlpi_comm(t_uscalar_t, size_t);
static void
hbx_fastpath_ask(hbx_device_t *);
static void
hbx_findipdevi(void);
#ifdef	HBX_CALLS_SETMULTICAST
static int
hbx_setmulticast(queue_t *, uchar_t *, unsigned int);
#endif

/*
 * Dev list table management functions
 */
static hbx_device_t *
hbx_finddev(hbx_dev_t *);

/*
 * Memory alloc/free/stat functions
 */
static void *
hbx_mem_alloc(size_t);
static void
hbx_mem_free(void *, size_t);
static mblk_t *
hbx_mblk_alloc(size_t);

/*
 * Misc function
 */
static int
hbx_miocpullup(mblk_t *, size_t);
static void
hbx_reboot(char *);

/*
 * Heartbeat emission functions
 */
static void
hbx_cyclic_add(void);
static void
hbx_cyclic_remove(void);
static void
hbx_send_heartbeat(void);
static void
hbx_change_state(hbx_device_t *, enum hbx_emit_state);

/*
 * Hearbeat message packet write functions
 */
static mblk_t *
hbx_create_heartbeat(hbx_device_t *);
static mblk_t *
hbx_create_fp_heartbeat(hbx_device_t *);
static uint32_t
hbx_compute_iph_checksum(uint16_t *);
static uint32_t
hbx_compute_udph_checksum(uint16_t *, uint16_t, uint16_t);
static void
hbx_write_udph(struct udphdr *, ipha_t *);
static void
hbx_write_iph(ipha_t *, hbx_device_t *);
static void
hbx_write_msg(hbx_msg_t *, hbx_device_t *);
static void
hbx_incr_heartbeat_sn(hbx_msg_t *, hbx_device_t *);
static void
hbx_sign_heartbeat(hbx_msg_t *, uchar_t *, hbx_pkey_t *);

#ifdef  __cplusplus
}
#endif

#endif	/* !_HBT_H */
