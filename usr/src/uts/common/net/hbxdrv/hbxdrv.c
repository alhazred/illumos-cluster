/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)hbxdrv.c	1.5	08/05/20 SMI"

#include <sys/stropts.h>
#include <sys/strsubr.h>
#include <sys/strsun.h>
#include <sys/ddi.h>
#include <sys/cyclic.h>
#include <sys/stat.h>
#include <sys/dlpi.h>
#include <sys/ksynch.h>
#include <sys/uadmin.h>
#include <sys/ethernet.h>
#include <hbxdrv/hbr.h>
#include <hbxdrv/hmac_sha1.h>

/*
 * IMPORTANT NOTE ABOUT cyclic_remove(): comments from cyclic.c:
 * "Cyclic removal is complicated by a guarantee made to the consumer of
 * the cyclic subsystem:  after cyclic_remove() returns, the cyclic handler
 * has returned and will never again be called."
 */

/*
 * Global data
 */

static int		hbx_unloadable = 0;		/* control unload */
static int		hbx_cyclic_run = 0;		/* Is cyclic running */
static hrtime_t		hbx_nano_freq = 0;		/* Emit. freq. in ns */
static cyclic_id_t	hbx_cyclic = 0;			/* A Solaris cyclic */
static hbx_device_t	*hbx_devs = NULL;		/* Device list */
static uint32_t		hbx_devs_nb = 0;		/* dev # in dev lst */
static krwlock_t	hbx_lock;			/* Protect dev lst */
static uint32_t		hbx_await_info_ack = 0;		/* wait INFO_ACK */
static uint32_t		hbx_await_fastpath_ack = 0;	/* wait HDR_INFO_ACK */
static dev_info_t	*hbx_devi = NULL;		/* driver devinfo */
static dev_info_t	*hbx_ipdevi = NULL;		/* IP module devinfo */
static hbx_upstream_t	*hbx_upstream = NULL;		/* hbxdrv clients */

/* Basic internal statistics */
static uint32_t		hbx_isopen = 0;		/* Driver user # */
static uint32_t		hbx_alloc_error = 0;	/* Allocations error # */
static uint32_t		hbx_memory = 0;		/* Mem used by the driver */
static uint32_t		hbx_highest_memory = 0;	/* High mem. water mark */

/* Time of last hb sent */
static volatile hrtime_t	hbx_sent_time = 0;
static kmutex_t			lock_time; /* Protect hbx_sent_time */

/* Heartbeat message parameters */
static hbx_ioc_freq_t	hbx_freq;	/* Frequency parameters */
static hbx_ioc_ident_t	hbx_ident;	/* Identity parameters */
static hbx_ioc_auth_t	hbx_auth;	/* Authentication parameters */

/* Heartbeat message UDP/IP parameters */
static in_port_t	hbx_udp_dst_port	= HBX_UDP_DST_PORT;
static uint8_t		hbx_ip_ttl		= HBX_IP_TTL;

/* Error logging and debug parameters */
static int	hbx_debug		= CE_IGNORE; /* Debug mode flag */
static uint32_t	hbx_error_limit		= 0; /* Error counter limit */
static uint32_t	hbx_syslog_period	= HBX_SYSLOG_PERIOD * 1000; /* in ms */

/*
 * Export hooks to hbxmod module
 */
char _depends_on[] = "strmod/hbxmod";


static hbxmod_ops_t ops_hook = {
	HBX_HOOK_REV,
	hbx_rput_hook,
	hbx_open_hook,
	hbx_physinfo_hook,
	hbx_close_hook
};


/*
 * hbxdrv driver configuration
 */
static struct module_info hbxdrvinfo = {
	HBX_ID,
	HBX_NAME,
	HBX_MIN_PKT,
	HBX_MAX_PKT,
	HBX_HIWATER,
	HBX_LOWATER
};


static struct qinit hbxdrvrinit = {
	NULL,		/* qi_putp */
	NULL,		/* qi_srvp */
	hbx_open,	/* qi_qopen */
	hbx_close,	/* qi_qclose */
	NULL,		/* qi_qadmin */
	&hbxdrvinfo,	/* qi_minfo */
	NULL		/* qi_mstat */
};


static struct qinit hbxdrvwinit = {
	hbx_wput,	/* qi_putp */
	NULL,		/* qi_srvp */
	hbx_open,	/* qi_qopen */
	hbx_close,	/* qi_qclose */
	NULL,		/* qi_qadmin */
	&hbxdrvinfo,	/* qi_minfo */
	NULL		/* qi_mstat */
};


static struct streamtab hbx_info = {
	&hbxdrvrinit,	/* st_rdinit */
	&hbxdrvwinit,	/* st_wrinit */
	NULL,		/* st_muxrinit */
	NULL		/* st_muxwrinit */
};


struct cb_ops hbx_cb_ops = {
	nodev,			/* open */
	nodev,			/* close */
	nodev,			/* strategy */
	nodev,			/* print */
	nodev,			/* dump */
	nodev,			/* read */
	nodev,			/* write */
	nodev,			/* ioctl */
	nodev,			/* devmap */
	nodev,			/* mmap */
	nodev,			/* segmap */
	nochpoll,		/* poll */
	ddi_prop_op,		/* prop_op */
	&hbx_info,		/* streamtab  */
	HBX_MTFLAGS		/* Driver flags */
};


struct dev_ops hbx_ops = {
	DEVO_REV,		/* devo_rev, */
	0,			/* refcnt  */
	hbx_getinfo,		/* get_dev_info */
	nulldev,		/* identify is obsolete */
	nulldev,		/* drv */
	hbx_attach,		/* attach */
	hbx_detach,		/* detach */
	nodev,			/* reset */
	&hbx_cb_ops,		/* driver operations */
	NULL,			/* no bus operations */
	NULL			/* no power operations */
};


extern struct mod_ops mod_driverops;

static struct modldrv modldrv = {
	&mod_driverops,
	HBX_NAME,
	&hbx_ops,
};


static struct modlinkage modlinkage = {
	MODREV_1,
	&modldrv,
	NULL
};


/*
 * Called at hbxdrv driver init time
 */
int
_init(void)
{
	int rc;

	/*
	 * Export hooks in the hbxmod module
	 */
	if ((rc = hbxmod_set_hooks(&ops_hook)) != 0) {
		return (rc);
	}
	/*
	 * Install the hbxdrv driver
	 */
	if ((rc = mod_install(&modlinkage)) != 0)  {
		return (rc);
	}
	/*
	 * Initialize global locks
	 */
	rw_init(&hbx_lock, NULL, RW_DEFAULT, NULL);
	mutex_init(&lock_time, NULL, MUTEX_DEFAULT, NULL);

	/* initialize global variables */
	bzero((void *)&hbx_freq, sizeof (hbx_ioc_freq_t));
	bzero((void *)&hbx_auth, sizeof (hbx_ioc_auth_t));
	bzero((void *)&hbx_ident, sizeof (hbx_ioc_ident_t));

	return (0);
}


/*
 * Called at hbxdrv driver fini time
 */
int
_fini(void)
{
	int rc;

	/*
	 * Do not allow to unload the driver,
	 * once loaded, except for debug or tests.
	 */
	if (hbx_unloadable == 0) {
		return (EBUSY);
	}
	/*
	 * Remove the hbxdrv driver, if possible
	 */
	if ((rc = mod_remove(&modlinkage)) == 0) {
		/*
		 * Destroy the cyclic, if in use
		 */
		hbx_cyclic_remove();
		/*
		 * Destroy the global locks
		 */
		rw_destroy(&hbx_lock);
		mutex_destroy(&lock_time);
		return (0);
	}
	return (rc);
}



/*
 * Called for hbxdrv driver module info
 */
int
_info(struct modinfo *modinfop)
{
	return (mod_info(&modlinkage, modinfop));
}



/*
 * Attach the hbxdrv driver to the system
 */
static int
hbx_attach(dev_info_t *devi, ddi_attach_cmd_t cmd)
{
	if (cmd != DDI_ATTACH) {
		return (DDI_FAILURE);
	}

	if (ddi_create_minor_node(devi, "", S_IFCHR,
	    0, DDI_PSEUDO, 0) == DDI_FAILURE) {
		return (DDI_FAILURE);
	}
	hbx_devi = devi;
	return (DDI_SUCCESS);
}

/*
 * Detach the hbxdrv driver from the system
 */
static int
hbx_detach(dev_info_t *devi, ddi_detach_cmd_t cmd)
{
	if (cmd != DDI_DETACH) {
		return (DDI_FAILURE);
	}
	ddi_remove_minor_node(devi, NULL);
	return (DDI_SUCCESS);
}



/*
 * Get hbxdrv driver information
 */
/* ARGSUSED */
static int
hbx_getinfo(dev_info_t *dip, ddi_info_cmd_t infocmd, void *arg, void **result)
{
	int error;

	switch (infocmd) {
	    case DDI_INFO_DEVT2DEVINFO:
		*result = (void *) hbx_devi;
		error = DDI_SUCCESS;
		break;
	    case DDI_INFO_DEVT2INSTANCE:
		*result = (void *)0;
		error = DDI_SUCCESS;
		break;
	    default:
		error = DDI_FAILURE;
	}

	return (error);
}



/*
 * Open the hbxdrv driver
 */
/* ARGSUSED */
static int
hbx_open(queue_t *rq, dev_t *devp, int flag, int sflag, cred_t *credp)
{
	hbx_upstream_t *up_new;
	hbx_upstream_t *up;
	unsigned int   minor_dev;

	if (drv_priv(credp) != 0) {
		/* Only root access is allowed */
		return (EPERM);
	}

	if (sflag != 0) {
		/*
		 * No CLONEOPEN, no MODOPEN, cloning is not handled
		 * through the clone driver
		 */
		return (ENXIO);
	}

	rw_enter(&hbx_lock, RW_WRITER);

	/*
	 * Minor device for clone handling,
	 * handle a chained list of hbxdrv driver clients
	 */
	minor_dev = 1;
	if (hbx_upstream == NULL) {
		hbx_upstream = (hbx_upstream_t *)
			hbx_mem_alloc(sizeof (hbx_upstream_t));
		if (hbx_upstream == NULL) {
			rw_exit(&hbx_lock);
			return (ENOMEM);
		}
		hbx_upstream->q = RD(rq);
		up_new = hbx_upstream;
	} else {
		up_new = (hbx_upstream_t *)
			hbx_mem_alloc(sizeof (hbx_upstream_t));
		if (up_new == NULL) {
			rw_exit(&hbx_lock);
			return (ENOMEM);
		}
		up_new->q = RD(rq);
		up = hbx_upstream;
		minor_dev++;
		while (up->next != NULL) {
			up = up->next;
			minor_dev++;
		}
		up->next = up_new;
		up_new->prev = up;
	}

	hbx_isopen++;

	rw_exit(&hbx_lock);

	/*
	 * Clone the hbxdrv driver device
	 */
	*devp = makedevice(getmajor(*devp), minor_dev);

	rq->q_ptr = (void *)(up_new);

	/*
	 * Let's rock
	 */
	qprocson(rq);

	return (0);
}



/*
 * Close the driver
 */
static int
hbx_close(queue_t *rq)
{
	hbx_upstream_t *up;
	hbx_device_t   *hbd;

	/*
	 * Let's no longer rock
	 */
	qprocsoff(rq);

	/*
	 * Handle a chained list of hbxdrv driver clients
	 */
	rw_enter(&hbx_lock, RW_WRITER);
	hbx_isopen--;
	ASSERT(hbx_isopen >= 0);
	ASSERT(hbx_upstream != NULL);
	up = (hbx_upstream_t *)rq->q_ptr;
	ASSERT(up != NULL);
	if (up == hbx_upstream) {
		hbx_upstream = up->next;
		if (hbx_upstream != NULL) {
			hbx_upstream->prev = NULL;
		}
	} else {
		if (up->next != NULL) {
			(up->next)->prev = up->prev;
		}
		ASSERT(up->prev != NULL);
		(up->prev)->next = up->next;
	}
	rw_exit(&hbx_lock);

	ASSERT(up != NULL);
	hbx_mem_free((void *)up, sizeof (hbx_upstream_t));

	return (0);
}



/*
 * hbxdrv driver read queue put procedure
 * is called from the hbxdrv driver downstream queue,
 * mainly, hbxdrv clients ioctl requests
 */
static int
hbx_wput(queue_t *q, mblk_t *mp)
{
	switch (DB_TYPE(mp)) {
		case M_IOCTL:
			hbx_ioctl(q, mp);
			break;
		/*
		 * No flush handling, because no service routines
		 */
		default:
			freemsg(mp);
			break;
	}
	return (0);
}

static void
hbx_rw_enter(krwlock_t *rwlp, krw_t enter_type)
{
	if (rw_read_locked(rwlp) == 0) {
		rw_enter(rwlp, enter_type);
	} else {
		cmn_err(CE_NOTE,
		    "Cannot rw_enter lock already hold");
	}
}

static void
hbx_rw_exit(krwlock_t *rwlp)
{
	if (rw_read_locked(rwlp) != 0) {
		rw_exit(rwlp);
	} else {
		cmn_err(CE_NOTE,
		    "Cannot rw_exit lock not hold");
	}
}

/*
 * hbxdrv driver ioctl request handling
 */
static void
hbx_ioctl(queue_t *q, mblk_t  *mp)
{
	struct iocblk	*iocp;
	int		error;
	int		mode;
	int		datamodel_not_native = 0;

	iocp = (struct iocblk *)mp->b_rptr;
	mode = iocp->ioc_flag;

	if ((mode & IOC_MODELS) != IOC_NATIVE) {
		datamodel_not_native = 1;
	}

	switch (iocp->ioc_cmd) {

	case HBX_IOC_ADD_DEV:
		miocnak(q, mp, 0, ENOSYS);
		return;

	case HBX_IOC_DEL_DEV:
		miocnak(q, mp, 0, ENOSYS);
		return;

	case HBX_IOC_EMIT_SET_FREQ:
	{
		hbx_ioc_freq_t *freq;
		hbx_device_t   *hbd;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_freq_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}

		/*
		 * A new heartbeat emission period setup
		 */
		freq  = (hbx_ioc_freq_t *)mp->b_cont->b_rptr;

		if ((freq->delay == 0) ||
		    (freq->numhb == 0)) {
			miocnak(q, mp, 0, EINVAL);
			return;
		}

		if ((freq->delay / freq->numhb) < HBX_MIN_FREQ) {
			miocnak(q, mp, 0, EINVAL);
			return;
		}

		rw_enter(&hbx_lock, RW_WRITER);

		/*
		 * Remove a previously set cyclic
		 */
		hbx_cyclic_remove();

		/*
		 * Register the new emission setup
		 */
		hbx_freq.delay = freq->delay;
		hbx_freq.numhb = freq->numhb;
		hbx_nano_freq =
			(hbx_freq.delay / hbx_freq.numhb) * 1000000;

		/* Reset to default if necessary */
		if (hbx_syslog_period <
		    (hbx_freq.delay / hbx_freq.numhb)) {
			hbx_syslog_period = HBX_SYSLOG_PERIOD * 1000;
		}

		/* Compute new error counter limit */
		hbx_error_limit = (hbx_syslog_period) /
		    (hbx_freq.delay / hbx_freq.numhb);

		/*
		 * Recreate the existing heartbeats, if any,
		 * with new emission delay
		 */
		hbd  = hbx_devs;
		while (hbd != NULL) {
			if (hbd->heartbeat != NULL) {
				freemsg(hbd->heartbeat);
			}
			if (hbd->fp_heartbeat != NULL) {
				freemsg(hbd->fp_heartbeat);
				hbd->fp_heartbeat = NULL;
			}
			/* Create msg only if all the parameters are set */
			if (hbx_ident.set) {
				hbd->heartbeat = hbx_create_heartbeat(hbd);
				if (hbd->heartbeat == NULL) {
					rw_exit(&hbx_lock);
					miocnak(q, mp, 0, ENOMEM);
					return;
				}
				hbx_fastpath_ask(hbd);
			}
			hbd = hbd->next;
		}

		hbx_freq.set = 1;

		rw_exit(&hbx_lock);

		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_EMIT_GET_FREQ:
	{
		hbx_ioc_freq_t *freq;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_freq_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}

		/*
		 * Return the current emission setup
		 */
		freq = (hbx_ioc_freq_t *)mp->b_cont->b_rptr;
		rw_enter(&hbx_lock, RW_READER);
		freq->delay = hbx_freq.delay;
		freq->numhb = hbx_freq.numhb;
		mutex_enter(&cpu_lock);
		freq->set = hbx_freq.set;
		mutex_exit(&cpu_lock);
		rw_exit(&hbx_lock);

		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_LIST_DEV:
	{
		hbx_ioc_list_dev_t	*ldev;
		hbx_device_t		*hbd;
		unsigned int		i;
		hbx_dev_t		*dev;
		uint64_t		addr64;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_list_dev_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}
		/*
		 * Return the current list of devices (the list of hbxmod
		 * modules pushed over drivers)
		 */
		ldev = (hbx_ioc_list_dev_t *)mp->b_cont->b_rptr;
		rw_enter(&hbx_lock, RW_READER);
		hbd  = hbx_devs;
		ASSERT(hbx_devs_nb == 0 ? hbd == NULL : hbd != NULL);
		if (datamodel_not_native) {
			hbx_ioc_list_dev_32_t *ldev32;
			/*
			 * 32 bit client - 64 bit kernel:
			 * memory address allocated in userland (i.e. by the
			 * client) is a 32bit value, the least significant
			 * bytes.
			 */
			ldev32 = (hbx_ioc_list_dev_32_t *)ldev;
			addr64 = ldev32->devp;
			dev = (hbx_dev_t *)addr64;
		} else {
			/*
			 * 32 bit client - 32 bit kernel or
			 * 64 bit client - 64 bit kernel
			 */
			dev = (hbx_dev_t *)ldev->dev;
		}
		if ((ldev->dev_nb != 0) && (dev == NULL)) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, EINVAL);
			return;
		}
		for (i = 0; (i < ldev->dev_nb) &&
			(i < hbx_devs_nb); i++) {
			addr64 = (uint64_t)hbd->dev;
			if ((error = ddi_copyout(
			    (void *)&(addr64),
			    (void *)dev, sizeof (hbx_dev_t), mode)) != 0) {
				rw_exit(&hbx_lock);
				miocnak(q, mp, 0, error);
				return;
			}
			hbd = hbd->next;
			dev++;
		}
		ldev->real_dev_nb = hbx_devs_nb;
		rw_exit(&hbx_lock);
		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_GET_DEV:
	{
		hbx_ioc_get_dev_t	*gdev;
		hbx_device_t		*hbd;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_get_dev_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}
		/*
		 * Return a per device current setup (a hbxmod module
		 * pushed over a driver current setup)
		 */
		gdev = (hbx_ioc_get_dev_t *)mp->b_cont->b_rptr;
		rw_enter(&hbx_lock, RW_READER);
		hbd  = hbx_finddev(&(gdev->dev));
		if (hbd == NULL) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, ENOENT);
			return;
		}
		rw_enter(&(hbd->lock), RW_READER);
		gdev->id = hbd->id;
		gdev->host_nb = hbd->host_nb;
		gdev->host_coll_nb = hbd->host_coll_nb;
		gdev->max_hosts = hbd->max_hosts;
		gdev->mac_info = hbd->mac_info;
		gdev->mac_version = hbd->mac_version;
		gdev->mac_ok = hbd->mac_ok;
		gdev->mac_type = hbd->mac_type;
		gdev->mac_len = hbd->mac_len;
		gdev->mac_sap_len = hbd->mac_sap_len;
		gdev->mac_sap_off = hbd->mac_sap_off;
		if (hbd->mac_len != 0) {
			bcopy((void *)&(hbd->mac_src[0]),
			(void *)&(gdev->mac_src[0]),
			hbd->mac_len);
			bcopy((void *)&(hbd->mac_bca[0]),
			(void *)&(gdev->mac_bca[0]),
			hbd->mac_len);
			bcopy((void *)&(hbd->mac_dst[0]),
			(void *)&(gdev->mac_dst[0]),
			hbd->mac_len);
		}
		if (hbd->mac_sap_len != 0) {
			bcopy((void *)&(hbd->mac_sap[0]),
			(void *)&(gdev->mac_sap[0]),
			hbd->mac_sap_len);
		}
		gdev->src = hbd->src;
		gdev->dst = hbd->dst;
		gdev->state = hbd->state;
		gdev->in = hbd->in;
		gdev->mode = hbd->mode;
		rw_exit(&(hbd->lock));
		rw_exit(&hbx_lock);
		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_SET_ID:
	{
		hbx_ioc_set_id_t	*sdev;
		hbx_device_t		*hbd;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_set_id_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}
		/*
		 * Set a per device (a hbxmod module
		 * pushed over a driver) user identification cookie
		 */
		sdev = (hbx_ioc_set_id_t *)mp->b_cont->b_rptr;
		rw_enter(&hbx_lock, RW_READER);
		hbd  = hbx_finddev(&(sdev->dev));
		if (hbd == NULL) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, ENOENT);
			return;
		}
		rw_enter(&(hbd->lock), RW_WRITER);
		hbd->id = sdev->id;
		rw_exit(&(hbd->lock));
		rw_exit(&hbx_lock);
		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_SET_IDENT:
	{
		hbx_ioc_ident_t *ident_addr;
		size_t		len = sizeof (hbx_ioc_ident_t);

		ident_addr = (hbx_ioc_ident_t *)mp->b_cont->b_rptr;
		ASSERT(ident_addr != NULL);

		error = hbx_miocpullup(mp, len);
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}

		rw_enter(&hbx_lock, RW_WRITER);

		/* Reset ident value */
		bzero((void *)&hbx_ident, sizeof (hbx_ioc_ident_t));

		/* Set node id */
		if (ident_addr->node_id == 0) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, EINVAL);
			return;
		}
		hbx_ident.node_id = ident_addr->node_id;

		/* Set cluster id */
		if (ident_addr->clid == 0) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, EINVAL);
			return;
		}
		hbx_ident.clid = ident_addr->clid;

		/* Set version */
		if (ident_addr->version == 0) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, EINVAL);
			return;
		}
		hbx_ident.version = ident_addr->version;

		/* Set init flag */
		hbx_ident.set = 1;

		rw_exit(&hbx_lock);

		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_GET_IDENT:
	{
		hbx_ioc_ident_t *ident_addr;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_ident_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}

		/*
		 * Return the current identity parameters
		 */
		ident_addr = (hbx_ioc_ident_t *)mp->b_cont->b_rptr;

		rw_enter(&hbx_lock, RW_READER);

		ident_addr->node_id = hbx_ident.node_id;
		ident_addr->clid = hbx_ident.clid;
		ident_addr->version = hbx_ident.version;
		ident_addr->set = hbx_ident.set;

		rw_exit(&hbx_lock);

		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_SET_DEBUG:
	{
		boolean_t *debug_flag_addr;

		error = hbx_miocpullup(mp, sizeof (boolean_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}

		rw_enter(&hbx_lock, RW_WRITER);

		debug_flag_addr = (boolean_t *)mp->b_cont->b_rptr;

		if (*debug_flag_addr == B_FALSE) {
			cmn_err(CE_NOTE, "Resetting debug mode\n");
			hbx_debug = CE_IGNORE;
		} else {
			cmn_err(CE_NOTE, "Setting debug mode\n");
			hbx_debug = CE_NOTE;
		}

		rw_exit(&hbx_lock);

		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_SET_SYSLOG_PERIOD:
	{
		hbx_delay_t *period_addr;

		error = hbx_miocpullup(mp, sizeof (hbx_delay_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}

		period_addr = (hbx_delay_t *)mp->b_cont->b_rptr;

		if (*period_addr == 0) {
			error = EINVAL;
			miocnak(q, mp, 0, error);
			return;
		}

		rw_enter(&hbx_lock, RW_WRITER);

		/* Could be readjust depending on the freq parameters */
		hbx_syslog_period = *period_addr * 1000;

		rw_exit(&hbx_lock);

		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_SET_AUTH:
	{
		hbx_ioc_auth_t	*auth_addr;
		size_t		len = sizeof (hbx_ioc_auth_t);
		hbx_device_t	*hbd = (hbx_device_t *)q->q_ptr;

		auth_addr = (hbx_ioc_auth_t *)mp->b_cont->b_rptr;
		ASSERT(auth_addr != NULL);

		error = hbx_miocpullup(mp, len);
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}
		rw_enter(&hbx_lock, RW_WRITER);

		/* Stop cyclic during key change */
		hbx_cyclic_remove();

		switch (auth_addr->cmd) {
		case HBX_AUTH_INIT_KEY:
			/* Reset auth data */
			bzero((void *)&hbx_auth, len);

			len = strlen((char *)&(auth_addr->pkey[0]));

			if ((len > HBX_PKEY_LEN) || (len == 0)) {
				error = EINVAL;
				break;
			}

			/* Set pkey */
			bcopy((void *)&(auth_addr->pkey[0]),
			    (void *)&hbx_auth.pkey[0], len);

			/* Set init flag */
			hbx_auth.set = 1;
			break;

		case HBX_AUTH_CHANGE_KEY:
			len = strlen((char*)&(auth_addr->alt_pkey[0]));

			if ((len > HBX_PKEY_LEN) || (len == 0)) {
				error = EINVAL;
				break;
			}

			/* Set alternate pkey */
			bcopy((void *)&(auth_addr->alt_pkey[0]),
			    (void *)&hbx_auth.alt_pkey[0], len);

			break;

		case HBX_AUTH_COMMIT_KEY:
			/* Reset pkey */
			bzero((void *)&hbx_auth.pkey[0],
			    sizeof (hbx_pkey_t));
			/* Swap pkeys */
			bcopy((void *)&hbx_auth.alt_pkey[0],
			    (void *)&hbx_auth.pkey[0], sizeof (hbx_pkey_t));
			/* Reset alt_key */
			bzero((void *)&hbx_auth.alt_pkey[0],
			    sizeof (hbx_pkey_t));

			break;

		default:
			error = EINVAL;
			break;
		}

		/* Restart cyclic if needed */
		hbd  = hbx_devs;
		while (hbd != NULL) {
			if (hbd->mode & HBX_EMT_MODE_ON) {
				hbx_cyclic_add();
				break;
			}
			hbd = hbd->next;
		}

		rw_exit(&hbx_lock);

		if (error != 0) {
			miocnak(q, mp, 0, error);
		} else {
			miocack(q, mp, msgsize(mp->b_cont), 0);
		}
		return;
	}

	case HBX_IOC_GET_AUTH:
	{
		hbx_ioc_auth_t *pauth;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_auth_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}

		/*
		 * Return the current authentication setup
		 */
		pauth = (hbx_ioc_auth_t *)mp->b_cont->b_rptr;

		rw_enter(&hbx_lock, RW_READER);
		pauth->set = hbx_auth.set;
		rw_exit(&hbx_lock);

		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_LIST_HOST:
	{
		hbx_ioc_list_host_t	*lhost;
		hbx_device_t		*hbd;
		hbx_host_t		*host;
		ipaddr_t		*uaddr;
		unsigned int		i;
		uint64_t		addr64;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_list_host_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}
		/*
		 * List the hosts that have emitted heartbeats, received on
		 * that device (a hbxmod module pushed over a driver)
		 */
		lhost = (hbx_ioc_list_host_t *)mp->b_cont->b_rptr;
		rw_enter(&hbx_lock, RW_READER);
		hbd = hbx_finddev(&(lhost->dev));
		if (hbd == NULL) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, ENOENT);
			return;
		}
		host = hbd->hosts;
		ASSERT(hbd->host_nb == 0 ? host == NULL : host != NULL);
		rw_enter(&(hbd->lock), RW_READER);

		if (datamodel_not_native) {
			hbx_ioc_list_host_32_t *lhost32;
			/*
			 * 32 bit client - 64 bit kernel:
			 * memory address allocated in userland is a 32bit
			 * value, the least significant bytes.
			 */
			lhost32 = (hbx_ioc_list_host_32_t *)lhost;
			addr64 = lhost32->hostp;
			uaddr = (ipaddr_t *)addr64;
		} else {
			/*
			 * 32 bit client - 32 bit kernel or
			 * 64 bit client - 64 bit kernel
			 */
			uaddr = (ipaddr_t *)lhost->host;
		}

		if ((lhost->host_nb != 0) && (uaddr == NULL)) {
			rw_exit(&(hbd->lock));
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, EINVAL);
			return;
		}

		for (i = 0; (i < hbd->host_nb) &&
			(i < lhost->host_nb); i++) {
			if ((error =
				ddi_copyout((void *)&(host->addr),
				    (void *)uaddr, sizeof (ipaddr_t), mode))
			    != 0) {
				rw_exit(&(hbd->lock));
				rw_exit(&hbx_lock);
				miocnak(q, mp, 0, error);
				return;
			}
			host = host->next;
			uaddr++;
		}
		lhost->real_host_nb = hbd->host_nb;
		rw_exit(&(hbd->lock));
		rw_exit(&hbx_lock);
		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_GET_HOST:
	{
		hbx_ioc_get_host_t	*ghost;
		hbx_host_t		*host;
		hbx_device_t		*hbd;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_get_host_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}
		/*
		 * Read a particular host state of a device (a hbxmod module
		 * pushed over a driver)
		 */
		ghost = (hbx_ioc_get_host_t *)mp->b_cont->b_rptr;
		rw_enter(&hbx_lock, RW_READER);
		hbd = hbx_finddev(&(ghost->dev));
		if (hbd == NULL) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, ENOENT);
			return;
		}
		rw_enter(&(hbd->lock), RW_READER);
		host = hbx_findhost(hbd, &(ghost->host));
		if (host == NULL) {
			rw_exit(&(hbd->lock));
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, ENOENT);
			return;
		}
		ghost->delay = host->delay;
		ghost->state = host->hstate;
		ghost->node_id = host->node_id;
		rw_exit(&(hbd->lock));
		rw_exit(&hbx_lock);
		miocack(q, mp, msgsize(mp->b_cont), 0);
		return;
	}

	case HBX_IOC_DEL_HOST:
	{
		hbx_ioc_del_host_t	*dhost;
		hbx_device_t		*hbd;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_del_host_t));
		if (error != 0) {
			miocnak(q, mp, 0, error);
			return;
		}
		/*
		 * Delete a particular host of a
		 * device (a hbxmod module pushed over a driver)
		 */
		dhost = (hbx_ioc_del_host_t *)mp->b_cont->b_rptr;
		rw_enter(&hbx_lock, RW_READER);
		hbd  = hbx_finddev(&(dhost->dev));
		if (hbd == NULL) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, ENOENT);
			return;
		}
		rw_enter(&(hbd->lock), RW_WRITER);
		if (hbx_del_host(hbd, &(dhost->host)) == 0) {
			rw_exit(&(hbd->lock));
			rw_exit(&hbx_lock);
			miocack(q, mp, 0, 0);
		} else {
			rw_exit(&(hbd->lock));
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, ENOENT);
		}
		return;
	}

	case HBX_IOC_EMIT_CTL:
	{
		hbx_ioc_emit_cmd_t	*emit;
		hbx_device_t		*hbd;

		error = hbx_miocpullup(mp, sizeof (hbx_ioc_emit_cmd_t));
		if (error != 0) {
			cmn_err(CE_NOTE, "HBX_IOC_EMIT_CTL miocpullup error");
			miocnak(q, mp, 0, error);
			return;
		}

		/*
		 * Heartbeat emission control per device
		 * (a hbxmod module pushed over a driver)
		 * Emission control per device state machine:
		 *
		 * init time -> +-------> state: HBX_EMIT_STATE_NONE
		 *		|		|
		 *		|		|
		 *		|	ioctl HBX_EMIT_CTL_SET_PARAMS
		 *		|		|
		 * ioctl HBX_EMIT_CTL_RESET   |
		 *		|		|
		 *		|		v
		 *		|---- state: HBX_EMIT_STATE_READY <---+
		 *		|		|			|
		 *		|		|			|
		 *		|	ioctl HBX_EMIT_CTL_START	|
		 *		|		|			|
		 *		|		v			|
		 *		+------- state: HBX_EMIT_STATE_RUN	|
		 *				|			|
		 *		heartbeats are sent on this device	|
		 *				|			|
		 *				v			|
		 *			ioctl HBX_EMIT_CTL_STOP	|
		 *				|			|
		 *				+-----------------------+
		 */
		emit = (hbx_ioc_emit_cmd_t *)mp->b_cont->b_rptr;

		rw_enter(&hbx_lock, RW_WRITER);

		hbd = hbx_finddev(&(emit->dev));
		if (hbd == NULL) {
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, ENOENT);
			return;
		}

		if ((hbd->state != HBX_EMIT_STATE_NONE) &&
		    (hbx_freq.set == 0)) {
			cmn_err(CE_NOTE,
			    "HBX_IOC_EMIT_CTL with hbx_freq.set = 0");
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, EINVAL);
			return;
		}

		/*
		 * Handle the emission machine state
		 */
		switch (emit->cmd) {

		case HBX_EMIT_CTL_SET_PARAMS:

			if (hbd->state == HBX_EMIT_STATE_RUN) {
				rw_exit(&hbx_lock);
				miocnak(q, mp, 0, EINVAL);
				return;
			}
			if (hbd->mac_ok != 1) {
				rw_exit(&hbx_lock);
				miocnak(q, mp, 0, EINVAL);
				return;
			}
			if ((emit->src == 0) || (emit->dst == 0)) {
				rw_exit(&hbx_lock);
				miocnak(q, mp, 0, EINVAL);
				return;
			}
			if (!((emit->mode | HBX_EMT_MODE_ON) ||
				(emit->mode | HBX_RCV_MODE_ON))) {
				rw_exit(&hbx_lock);
				miocnak(q, mp, 0, EINVAL);
				return;
			}
			hbd->src = emit->src;
			hbd->dst = emit->dst;
			if (IN_MULTICAST(ntohl(hbd->dst))) {
				/* Use multicast mac dst */
				HBX_ETHER_MAP_IP_MULTICAST(&(hbd->dst),
					&(hbd->mac_dst[0]));
#ifdef	HBX_CALLS_SETMULTICAST
				(void) hbx_setmulticast(hbd->q,
					hbd->mac_dst, hbd->mac_len);
#endif
			} else {
				/* Use mac broadcast as mac dst */
				bcopy((void *)&(hbd->mac_bca[0]),
					(void *)&(hbd->mac_dst[0]),
					hbd->mac_len);
			}
			/* Initialize emit and or rcv mode */
			hbd->mode = emit->mode;

			/* Initialize incarnation number */
			hbd->in = emit->in;

			/* If udp port is set assign it */
			if (emit->udp_dst_port != 0) {
				hbx_udp_dst_port = emit->udp_dst_port;
			}

			/* If ttl is set assign it */
			if (emit->ttl != 0) {
				hbx_ip_ttl = emit->ttl;
			}
			hbx_change_state(hbd, HBX_EMIT_STATE_READY);
			rw_exit(&hbx_lock);
			miocack(q, mp, 0, 0);
			return;

		case HBX_EMIT_CTL_START:

			if (hbd->state != HBX_EMIT_STATE_READY) {
				rw_exit(&hbx_lock);
				miocnak(q, mp, 0, EINVAL);
				return;
			}

			if (hbd->mode & HBX_EMT_MODE_ON) {
				/*
				 * Always remove cyclic before modifying
				 * heartbeat buffer
				 */
				hbx_cyclic_remove();

				/* Initialize sequence number */
				hbd->sn = 0;

				if (hbd->heartbeat == NULL) {
					hbd->heartbeat = hbx_create_heartbeat
					    (hbd);
					if (hbd->heartbeat == NULL) {
						rw_exit(&hbx_lock);
						miocnak(q, mp, 0, ENOMEM);
						return;
					}
				}
				if (hbd->fp_heartbeat == NULL) {
					hbx_fastpath_ask(hbd);
				}
			}

			if (hbd->mode & HBX_RCV_MODE_ON) {
				/*
				 * host table may have been
				 * freed after a reset
				 */
				if (hbd->hosttable == NULL) {
					if (hbx_init_hosttable(hbd) != 0) {
						rw_exit(&hbx_lock);
						miocnak(q, mp, 0, ENOMEM);
						return;
					}
				}
			}

			cmn_err(CE_NOTE, "Starting src %x using mode %s %s",
			    ntohl(hbd->src),
			    (hbd->mode & HBX_EMT_MODE_ON ?
				hbx_mode[HBX_EMT_MODE_ON]:""),
			    (hbd->mode & HBX_RCV_MODE_ON ?
				hbx_mode[HBX_RCV_MODE_ON]:""));

			hbx_change_state(hbd, HBX_EMIT_STATE_RUN);

			rw_exit(&hbx_lock);

			miocack(q, mp, 0, 0);
			return;

		case HBX_EMIT_CTL_STOP:

			if (hbd->state != HBX_EMIT_STATE_RUN) {
				rw_exit(&hbx_lock);
				miocnak(q, mp, 0, EINVAL);
				return;
			}

			if (hbd->mode & HBX_EMT_MODE_ON) {
				ASSERT(hbd->heartbeat != NULL);
				ASSERT(hbd->q != NULL);
				/*
				 * Always remove cyclic before modifying
				 * heartbeat buffer
				 */
				hbx_cyclic_remove();

				if (hbd->heartbeat != NULL) {
					freemsg(hbd->heartbeat);
					hbd->heartbeat = NULL;
				}
				if (hbd->fp_heartbeat != NULL) {
					freemsg(hbd->fp_heartbeat);
					hbd->fp_heartbeat = NULL;
				}
			}

			/* Reset hbxdrv mode */
			hbd->mode = HBX_MODE_OFF;

			cmn_err(CE_NOTE, "Stopping src %x",
			    ntohl(hbd->src));

			hbx_change_state(hbd, HBX_EMIT_STATE_READY);

			rw_exit(&hbx_lock);
			miocack(q, mp, 0, 0);
			return;

		case HBX_EMIT_CTL_RESET:

			ASSERT(hbd->q != NULL);

			cmn_err(CE_NOTE, "Resetting src %x",
			    ntohl(hbd->src));

			if (hbd->mode & HBX_EMT_MODE_ON) {
				ASSERT(hbd->heartbeat != NULL);
				ASSERT(hbd->q != NULL);
				/*
				 * Always remove cyclic before modifying
				 * heartbeat buffer
				 */
				hbx_cyclic_remove();

				if (hbd->heartbeat != NULL) {
					freemsg(hbd->heartbeat);
					hbd->heartbeat = NULL;
				}
				if (hbd->fp_heartbeat != NULL) {
					freemsg(hbd->fp_heartbeat);
					hbd->fp_heartbeat = NULL;
				}

				/* Reset emit parameters */
				hbd->src = 0;
				hbd->dst = 0;
				hbd->in = 0;
				bzero((void *)&(hbd->mac_dst[0]),
				    HBX_MAC_MAX_LEN);
			}

			/* Reset hbxdrv mode */
			hbd->mode = HBX_MODE_OFF;

			hbx_change_state(hbd, HBX_EMIT_STATE_NONE);

			rw_exit(&hbx_lock);
			miocack(q, mp, 0, 0);
			return;

		default:
			rw_exit(&hbx_lock);
			miocnak(q, mp, 0, EINVAL);
			return;
		}
	}

	default:
		miocnak(q, mp, 0, EINVAL);
		return;
	}
}

/*
 * Called each time a hbxmod module open is called,
 * ie: pushed onto a physical device.
 */
static int
hbx_open_hook(queue_t *q, dev_t *dev)
{
	hbx_device_t *pdev;
	hbx_device_t *hbd;

	ASSERT(q->q_ptr == NULL);
	/*
	 * Verify that the hbxmod module
	 * is not already pushed for that device:
	 * only one is allowed.
	 */
	rw_enter(&hbx_lock, RW_READER);
	hbd = hbx_devs;
	while (hbd != NULL) {
		if (hbd->dev == *dev) {
			rw_exit(&hbx_lock);
			return (EEXIST);
		}
		hbd = hbd->next;
	}
	rw_exit(&hbx_lock);

	/* Find the ip module devinfo */
	if (hbx_ipdevi == NULL) {
		hbx_findipdevi();
	}

	/* Allocate space for private data */
	pdev = hbx_mem_alloc(sizeof (hbx_device_t));
	if (pdev == NULL) {
		return (ENOMEM);
	}

	/* Initialize per device data aera */
	if (hbx_init_hosttable(pdev) != 0) {
		hbx_mem_free((void *)pdev,
			sizeof (hbx_device_t));
		return (ENOMEM);
	}

	ASSERT(dev != NULL);
	pdev->dev = *dev;
	pdev->q = q;

	/*
	 * Insert in linked list of devices (hbxmod modules pushed over
	 * a device, at tail
	 */
	rw_enter(&hbx_lock, RW_WRITER);

	hbx_cyclic_remove();

	hbd = hbx_devs;
	if (hbd == NULL) {
		hbx_devs = pdev;
		ASSERT(hbx_devs_nb == 0);
	} else {
		ASSERT(hbd->prev == NULL);
		while (hbd->next != NULL) {
			hbd = hbd->next;
		}
		hbd->next = pdev;
		pdev->prev = hbd;
	}
	pdev->id = (unsigned int)-1;
	hbx_devs_nb++;
	rw_init(&(pdev->lock), NULL, RW_DEFAULT, NULL);

	q->q_ptr = (void *)pdev;
	WR(q)->q_ptr = q->q_ptr;

	hbx_cyclic_add();

	rw_exit(&hbx_lock);

	return (0);
}

/*
 * Called each time a hbxmod module close is called,
 * ie: removed from a physical device path.
 */
static int
hbx_close_hook(queue_t *q)
{
	hbx_device_t *pdev;
	hbx_device_t *hbd;

	rw_enter(&hbx_lock, RW_WRITER);

	hbx_cyclic_remove();

	pdev = (hbx_device_t *)q->q_ptr;
	ASSERT(pdev != NULL);

	/* qprocsoff must have been called by caller */
	(void) hbx_signalevtid(pdev, EVT_DEV_REMOVED);

	/*
	 * Remove from linked list of devices (hbxmod modules pushed over
	 * a device
	 */
	hbd = hbx_devs;
	ASSERT(hbd != NULL);
	if (hbd == pdev) {
		ASSERT(pdev->prev == NULL);
		if (pdev->next != NULL) {
			(hbd->next)->prev = NULL;
		}
		hbx_devs = pdev->next;
	} else {
		do {
			hbd = hbd->next;
		} while (hbd != NULL && hbd != pdev);
		ASSERT(hbd != NULL);
		ASSERT(hbd->prev != NULL);
		(hbd->prev)->next = hbd->next;
		if (hbd->next != NULL) {
			(hbd->next)->prev = hbd->prev;
		}
	}
	hbx_devs_nb--;
	hbx_cyclic_add();
	rw_exit(&hbx_lock);

	/* Free all data previously allocated */
	if (pdev->heartbeat != NULL) {
		freemsg(pdev->heartbeat);
	}
	if (pdev->fp_heartbeat != NULL) {
		freemsg(pdev->fp_heartbeat);
	}
	hbx_free_hosttable(pdev);
	rw_destroy(&(pdev->lock));
	hbx_mem_free((void *)pdev,
		sizeof (hbx_device_t));

	WR(q)->q_ptr = q->q_ptr = NULL;

	return (0);
}

/*
 * Called each time a hbxmod module rput is called,
 * ie: a message from a physical device goes upstream
 */
static int
hbx_rput_hook(queue_t *q, mblk_t *mp)
{
	volatile hrtime_t tdiff = 0;

	/*
	 * Handle any clock drift, the emission cyclic not excuted
	 * on time can occur during high network interrupt load,
	 * this is detected here, by an incoming network packet,
	 * using an alternate time reference: gethrtime(), that is reliable.
	 * In this case, the cyclic handler is called  from here, at network
	 * interrupt level
	*/
	mutex_enter(&lock_time);
	if (hbx_sent_time != 0) {
		tdiff = gethrtime() - hbx_sent_time;
	}
	mutex_exit(&lock_time);
	if (tdiff > (hbx_nano_freq + HBX_MIN_DRIFT_FREQ)) {
		rw_enter(&hbx_lock, RW_WRITER);
		hbx_send_heartbeat();
		rw_exit(&hbx_lock);
	}

	/*
	 * Lookup if it is a heartbeat or not, if it is a hearbeat,
	 * handle it, and it will not go upstream to IP module, it will
	 * be freed by hbxmod module
	 */
	return (hbx_filterheartbeat(q, mp));
}

/*
 * Called each time a hbxdrv module is open,
 * ie: pushed onto a physical device.
 * This function is called after qprocson:
 * this is used to get the physical device infos,
 * and also allow a hbxmod module push to fail, for
 * example, if driver is not of type ETHER or not
 * a DLPI version 2 driver.
 */
/* ARGSUSED */
static int
hbx_physinfo_hook(queue_t *q, dev_t *dev)
{
	hbx_device_t *pdev;
	mblk_t *mpi;
	uint32_t cnt;

	pdev = (hbx_device_t *)q->q_ptr;
	ASSERT(pdev != NULL);
	mpi = hbx_dlpi_comm(DL_INFO_REQ, DL_INFO_REQ_SIZE);
	if (mpi != NULL) {
		if (canputnext(WR(q))) {
			atomic_add_32(&hbx_await_info_ack, 1);
			putnext(WR(q), mpi);
		} else {
			freemsg(mpi);
			goto hbx_physinfo_hook_exit;
		}
		cnt = 0;
		/* (hbx_await_info_ack > 0) in an atomic way */
		while (cas32(&hbx_await_info_ack, 0, 0) > 0) {
			qwait(q);
			/* a message is arrived in hbx_rput_hook() */
			if (++cnt > HBX_AWAIT_MAX) {
				atomic_add_32(&hbx_await_info_ack, -1);
				goto hbx_physinfo_hook_exit;
			}
		}
hbx_physinfo_hook_exit:
		rw_enter(&hbx_lock, RW_READER);
		rw_enter(&(pdev->lock), RW_READER);
		if (pdev->mac_info == 0) {
			rw_exit(&(pdev->lock));
			rw_exit(&hbx_lock);
			hbx_open_error(q);
			return (ENXIO);
		}
		(void) hbx_signalevtid(pdev, EVT_DEV_ADDED);
		rw_exit(&(pdev->lock));
		rw_exit(&hbx_lock);
		return (0);
	} else {
		hbx_open_error(q);
		return (ENOMEM);
	}
}


static int
hbx_check_udph(struct udphdr *udph, ipaddr_t src)
{
	int rc = 1;

	if (ntohs(udph->uh_sport) != 0) {
		cmn_err(hbx_debug, "Wrong src_port %x for host %x. "
		    "Expecting %x\n", ntohs(udph->uh_sport), src, 0);
		goto not_a_hb;
	}

	if (ntohs(udph->uh_dport) != hbx_udp_dst_port) {
		cmn_err(hbx_debug, "Wrong dst_port %x for host %x. "
		    "Expecting %x\n", ntohs(udph->uh_dport), src,
		    hbx_udp_dst_port);
		goto not_a_hb;
	}

	if (ntohs(udph->uh_ulen) !=
	    (HBX_MSG_SIZE + sizeof (struct udphdr))) {
		cmn_err(hbx_debug, "Wrong udph len %x for host %x\n",
		    ntohs(udph->uh_ulen), src);
		goto not_a_hb;
	}

	rc = 0;

not_a_hb:
	return (rc);
}

static int
hbx_check_iph(ipha_t *ipha)
{
	int rc = 1;
	uint32_t sum;

	if (IPH_HDR_VERSION(ipha) != IPV4_VERSION) {
		goto not_a_hb;
	}

	if (ipha->ipha_protocol != HBX_PROTOCOL) {
		goto not_a_hb;
	}

	if (IPH_HDR_LENGTH(ipha) != IP_SIMPLE_HDR_LENGTH) {
		goto not_a_hb;
	}

	/* verify the length */
	if (ntohs(ipha->ipha_length) !=
	    (HBX_ALL_HDR_SIZE + HBX_MSG_SIZE)) {
		goto not_a_hb;
	}

	/*
	 * From here, it is pretty sure to be heartbeat, do now
	 * all additional checks
	 */

	/* verify the ident */
	if (ipha->ipha_ident != 0) {
		goto not_a_hb;
	}

	/*
	 * Verify fragment and offset.
	 * IPH_DF ->  Don't fragment.
	 */
	if (ipha->ipha_fragment_offset_and_flags != htons(IPH_DF)) {
		goto not_a_hb;
	}

	/* verify the TTL */
/* 	if (ipha->ipha_ttl != 1) { */
/* 		goto not_a_hb; */
/* 	} */

	/* verify QOS */
	if (ipha->ipha_type_of_service != 0) {
		goto not_a_hb;
	}

	/* Check the IP header checksum.  */
#define	uph	((uint16_t *)ipha)
	sum = uph[0] + uph[1] + uph[2] + uph[3] + uph[4] + uph[5] + uph[6] +
		uph[7] + uph[8] + uph[9];
#undef	uph
	sum = (sum & 0xFFFF) + (sum >> 16);
	sum = ~(sum + (sum >> 16)) & 0xFFFF;

	if (sum && sum != 0xFFFF) {
		goto not_a_hb;
	}

	rc = 0;

not_a_hb:
	return (rc);
}

static int
hbx_check_signature(queue_t *q, ipaddr_t src, hbx_msg_t *hb, hbx_pkey_t *pkey)
{
	int		status;
	uchar_t		*digest_addr = HBX_GET_DIGEST_ADDR(hb);
	uchar_t		digest[HMAC_DIGEST_LEN];
	hbx_host_t	*host;

	/* Perform signature checks */
	(void) hbx_sign_heartbeat(hb, &digest[0], pkey);
	status = bcmp((void *)&digest[0], (void *)digest_addr,
	    HMAC_DIGEST_LEN);
	return (status);
}

static int
hbx_check_msg(queue_t *q, hbx_msg_t *hb, ipaddr_t src,
    hbx_error_t *hb_error)
{
	hbx_version_t	*version_addr = HBX_GET_VERSION_ADDR(hb);
	hbx_delay_t	*delay_addr = HBX_GET_DELAY_ADDR(hb);
	hbx_clid_t	*clid_addr = HBX_GET_CLID_ADDR(hb);
	hbx_version_t	version;
	hbx_clid_t	clid;
	hbx_delay_t	delay;
	int		rc = 1;
	hbx_host_t	*host;

	rw_enter(&hbx_lock, RW_READER);

	/* Try the alternate pkey first if defined */
	if (hbx_auth.alt_pkey[0] != 0) {
		if (hbx_check_signature
		    (q, src, hb, &hbx_auth.alt_pkey) != 0) {
			/* Try the committed pkey */
			if (hbx_check_signature
			    (q, src, hb, &hbx_auth.pkey) != 0) {
				HBX_CREATE_ERROR(hb_error, src,
				    HBX_ERROR_AUTH);
				goto not_a_hb;
			}
		}
	} else {
		/* Use the committed pkey */
		if (hbx_check_signature(q, src, hb, &hbx_auth.pkey) != 0) {
			HBX_CREATE_ERROR(hb_error, src, HBX_ERROR_AUTH);
			goto not_a_hb;
		}
	}

	bcopy((void *)version_addr, &version, sizeof (hbx_version_t));

	/* Check the heartbeat protocol version */
	if (ntohs(version) != hbx_ident.version) {
		HBX_CREATE_ERROR(hb_error, src, HBX_ERROR_PROTO);
		goto not_a_hb;
	}

	/* Check the detection delay */
	bcopy((void *)delay_addr, &delay, sizeof (hbx_delay_t));
	if ((delay == 0) || (ntohl(delay) < HBX_MIN_FREQ)) {
		HBX_CREATE_ERROR(hb_error, src, HBX_ERROR_DELAY);
		goto not_a_hb;
	}

	/* Check the cluster id */
	bcopy((void *)clid_addr, &clid, sizeof (hbx_clid_t));
	if (ntohl(clid) != hbx_ident.clid) {
		HBX_CREATE_ERROR(hb_error, src, HBX_ERROR_CLID);
		goto not_a_hb;
	}

	rc = 0;

not_a_hb:
	rw_exit(&hbx_lock);
	return (rc);
}

static void
hbx_write_msg(hbx_msg_t *hb, hbx_device_t *pdev)
{
	/* Compute heartbeat message fields addresses */
	hbx_in_t	*in_addr = HBX_GET_IN_ADDR(hb);
	hbx_delay_t	*delay_addr = HBX_GET_DELAY_ADDR(hb);
	hbx_version_t	*version_addr = HBX_GET_VERSION_ADDR(hb);
	hbx_node_id_t	*node_id_addr = HBX_GET_NODE_ID_ADDR(hb);
	hbx_clid_t	*clid_addr = HBX_GET_CLID_ADDR(hb);
	hbx_sn_t	*sn_addr = HBX_GET_SN_ADDR(hb);
	uchar_t		*digest_addr = HBX_GET_DIGEST_ADDR(hb);

	/* Set heartbeat message fields values */
	hbx_delay_t	delay = htonl(hbx_freq.delay);
	hbx_version_t	version = htons(hbx_ident.version);
	hbx_node_id_t	node_id = htonl(hbx_ident.node_id);
	hbx_clid_t	clid = htonl(hbx_ident.clid);
	hbx_in_t	in = htonl(pdev->in);
	hbx_sn_t	sn = htonl(pdev->sn);
	uchar_t		digest[HMAC_DIGEST_LEN];

	/* Put the heartbeat message fields into the message buffer */
/* 	cmn_err(hbx_debug, "Incarnation Number = %x\n", ntohl(in)); */
	bcopy((void *)&in, (void *)in_addr, sizeof (hbx_in_t));

/* 	cmn_err(hbx_debug, "Delay = %x\n", ntohl(delay)); */
	bcopy((void *)&delay, (void *)delay_addr, sizeof (hbx_delay_t));

/* 	cmn_err(hbx_debug, "Version Number = %x\n", ntohs(version)); */
	bcopy((void *)&version, (void *)version_addr, sizeof (hbx_version_t));

/* 	cmn_err(hbx_debug, "Node ID = %d\n", ntohl(node_id)); */
	bcopy((void *)&node_id, (void *)node_id_addr, sizeof (hbx_node_id_t));

/* 	cmn_err(hbx_debug, "Cluster ID = 0x%x\n", hbx_ident.clid); */
	bcopy((void *)&clid, (void *)clid_addr, sizeof (hbx_clid_t));

/* 	cmn_err(hbx_debug, "Sequence Number = %x\n", ntohl(sn)); */
	bcopy((void *)&sn, (void *)sn_addr, sizeof (hbx_sn_t));

	/* Generate the SHA1 digest optionally encrypted */
	hbx_sign_heartbeat(hb, &digest[0], &hbx_auth.pkey);

	/* Append the digest to the end of the message */
	bcopy((void *)digest, digest_addr, HMAC_DIGEST_LEN);
}

static void
hbx_incr_heartbeat_sn(hbx_msg_t *hb, hbx_device_t *hbd)
{
	hbx_sn_t *sn_addr = HBX_GET_SN_ADDR(hb);
	uchar_t *digest_addr = HBX_GET_DIGEST_ADDR(hb);
	uchar_t digest[HMAC_DIGEST_LEN];
	hbx_sn_t sn;

	/* Increment the sequence number */
	hbd->sn++;
	sn = htonl(hbd->sn);
	bcopy((void *)&sn, (void *)sn_addr, sizeof (hbx_sn_t));

	/* Generate new message signature */
	hbx_sign_heartbeat(hb, &digest[0], &hbx_auth.pkey);
	bcopy((void *)digest, (void *)digest_addr, HMAC_DIGEST_LEN);
}

static void
hbx_sign_heartbeat(hbx_msg_t *hb, uchar_t *digest, hbx_pkey_t *pkey)
{
	SHA1_CTX sha1_ctx;

	if (hbx_auth.set) {
		/* Generate a HMAC-SHA1 digest encrypted with pkey */
		hbx_HMACInit(&sha1_ctx, (uchar_t *)pkey,
		    strlen(*pkey));
		hbx_HMACUpdate(&sha1_ctx, (uchar_t *)hb, HBX_PAYLOAD_SIZE);
		hbx_HMACFinal(&sha1_ctx, (uchar_t *)pkey,
		    strlen(*pkey), digest);
	} else {
		/* Generate a simple SHA1 digest */
		hbx_SHA1Init(&sha1_ctx);
		hbx_SHA1Update(&sha1_ctx, (uchar_t *)hb, HBX_PAYLOAD_SIZE);
		hbx_SHA1Final(digest, &sha1_ctx);
	}
}

/*
 * Periodicaly send the heartbeats
 * Warning: cyclic must no run while
 * either hbx_devs is modified,
 * or a hbx_device_t is modified.
 * The cyclic must be stopped temporary and restarted
 * when the changes are done.
 */
static void
hbx_send_heartbeat(void)
{
	queue_t *q; /* driver emission queue */
	mblk_t *mp; /* message to sent to driver emission queue */
	hbx_device_t *hbd; /* device, a hbxmod module pushed over a driver */

	hbd = hbx_devs;
	while (hbd != NULL) {
		if ((hbd->state == HBX_EMIT_STATE_RUN) &&
		    (hbd->mode & HBX_EMT_MODE_ON)) {
			/*
			 * Emit heartbeats is asked on that link
			 */
			q = hbd->q; /* driver emission queue */
			ASSERT(q != NULL);
			ASSERT(WR(q) != NULL);
			ASSERT(WR(q)->q_next != NULL);
			if (hbd->fp_heartbeat != NULL) {
				mp = copymsg(hbd->fp_heartbeat);
				if (mp == NULL) {
					hbx_reboot("Unable to allocate"
					    " memory - Fastpath copymsg");
				} else {
					hbx_incr_heartbeat_sn(
					    HBX_GET_FP_HB_ADDR
					    (mp->b_rptr), hbd);
					if (canputnext(WR(q))) {
						putnext(WR(q), mp);
					} else {
						freemsg(mp);
					}
				}
			} else {
				ASSERT(hbd->heartbeat != NULL);
				mp = copymsg(hbd->heartbeat);
				if (mp != NULL) {
					hbx_incr_heartbeat_sn(
					    HBX_GET_HB_ADDR
					    (mp->b_rptr), hbd);
					if (canputnext(WR(q))) {
						putnext(WR(q), mp);
					} else {
						freemsg(mp);
					}
				} else {
					hbx_reboot("Unable to allocate"
					    " memory");
				}
			}
		}
		hbd = hbd->next;
	}
	mutex_enter(&lock_time);
	hbx_sent_time = gethrtime();
	mutex_exit(&lock_time);
}

/*
 * A timer for a registered heartbeat emitter source has failed
 * put a host from a device as DOWN and signal it
 */
static void
hbx_hbtimeout(void *ptr)
{
	struct hbx_host_t *host;

	rw_enter(&hbx_lock, RW_READER);
	host = (struct hbx_host_t *)ptr;
	ASSERT(host != NULL);
	ASSERT(host->pdev != NULL);
	ASSERT((host->pdev)->q != NULL);
	rw_enter(&((host->pdev)->lock), RW_WRITER);
	/* signal, we are down */
	host->hstate = HOST_STATE_DOWN;
	(void) hbx_signalevt(host, EVT_HOST_DOWN);
	rw_exit(&((host->pdev)->lock));
	rw_exit(&hbx_lock);
}

/*
 * Filter heartbeat IP packets received from the driver and handle heartbeats,
 */
static int
hbx_filterheartbeat(queue_t *q, mblk_t *mp)
{
	ipha_t *ipha;
	union DL_primitives *dlp;

	if (DB_TYPE(mp) == M_DATA) {
		/* Fast Path */
		ipha = (ipha_t *)(mp->b_rptr + sizeof (struct ether_header));
		return (hbx_isheartbeat(q, mp, ipha));
	}
	if ((DB_TYPE(mp) == M_PROTO) && (mp->b_cont != NULL)) {
		/*
		 * Non fast path, search for a
		 * DL_UNITDATA_IND DLIP request
		 * with data in b_cont
		 */
		dlp = (union DL_primitives *)mp->b_rptr;
		if (dlp->dl_primitive == DL_UNITDATA_IND) {
			ipha = (ipha_t *)((mp->b_cont)->b_rptr);
			return (hbx_isheartbeat(q, mp->b_cont, ipha));
		}
	}

	/*
	 * Handle fast path requests done by hbxdrv
	 */
	if ((cas32(&hbx_await_fastpath_ack, 0, 0) > 0) &&
	    (DB_TYPE(mp) == M_IOCACK || DB_TYPE(mp) == M_IOCNAK)) {
		if (((struct iocblk *)mp->b_rptr)->ioc_cmd ==
		    DL_IOC_HDR_INFO) {
			atomic_add_32(&hbx_await_fastpath_ack, -1);
			if (DB_TYPE(mp) == M_IOCACK) {
				hbx_device_t *pdev;
				pdev = (hbx_device_t *)q->q_ptr;
				ASSERT(pdev != NULL);
				hbx_cyclic_remove();
				pdev->sn = 0;
				pdev->fp_heartbeat =
					hbx_create_fp_heartbeat(pdev);
				hbx_cyclic_add();
				return (1);
			}
		}
	}

	/*
	 * Handle the DL_INFO_ACK that we are awaiting after
	 * our own DL_INFO_REQUEST messages.
	 */
	if ((DB_TYPE(mp) == M_PCPROTO) &&
	    /* Compute (hbx_await_info_ack > 0) in an atomic way */
	    (cas32(&hbx_await_info_ack, 0, 0) > 0)) {
		dlp = (union DL_primitives *)mp->b_rptr;
		if ((dlp->dl_primitive == DL_INFO_ACK) &&
		    /* LINTED checked */
		    ((mp->b_wptr - mp->b_rptr) >= DL_INFO_ACK_SIZE)) {
			uchar_t *addr;
			uchar_t *sap;
			dl_info_ack_t *info;
			ushort_t eth_sap;
			hbx_device_t *pdev;

			rw_enter(&hbx_lock, RW_WRITER);

			pdev = (hbx_device_t *)q->q_ptr;
			ASSERT(pdev != NULL);

			atomic_add_32(&hbx_await_info_ack, -1);
			info = (dl_info_ack_t *)mp->b_rptr;
			ASSERT(pdev->mac_info == 0);
			pdev->mac_info = 1;
			pdev->mac_type = info->dl_mac_type;
			pdev->mac_version = info->dl_version;
			if (pdev->mac_version != DL_VERSION_2) {
				rw_exit(&hbx_lock);
				return (1);
			}
			if (info->dl_sap_length < 0) {
				/* physical address + sap */
				pdev->mac_len = info->dl_addr_length +
					info->dl_sap_length;
				addr = (uchar_t *)mp->b_rptr +
					info->dl_addr_offset;
				pdev->mac_sap_len = (-info->dl_sap_length);
				pdev->mac_sap_off = pdev->mac_len;
				sap = addr + pdev->mac_sap_off;
			} else {
				/* sap + physical address */
				pdev->mac_len = info->dl_addr_length -
					info->dl_sap_length;
				addr = (uchar_t *)mp->b_rptr +
					+ info->dl_addr_offset
					+ info->dl_sap_length;
				pdev->mac_sap_len = info->dl_sap_length;
				pdev->mac_sap_off = 0;
				sap = (uchar_t *)mp->b_rptr +
					info->dl_addr_offset;
			}
			if (pdev->mac_len > HBX_MAC_MAX_LEN) {
				rw_exit(&hbx_lock);
				return (1);
			}
			if (pdev->mac_sap_len > HBX_MAC_MAX_LEN) {
				rw_exit(&hbx_lock);
				return (1);
			}

			switch (info->dl_mac_type) {
			case DL_METRO:
			case DL_TPB:
			case DL_TPR:
			case DL_CSMACD:
			case DL_ETHER:
			case DL_ETH_CSMA:
			case DL_100VG:
			case DL_100BT:
				bcopy((void *)addr,
					(void *)&(pdev->mac_src[0]),
					pdev->mac_len);
				addr = mp->b_rptr +
					info->dl_brdcst_addr_offset;
				bcopy((void *)addr,
					(void *)&(pdev->mac_bca[0]),
					pdev->mac_len);
				bcopy((void *)sap,
					(void *)&(pdev->mac_sap[0]),
					pdev->mac_sap_len);
				bcopy((void *)sap,
					(void *)&eth_sap,
					sizeof (ushort_t));
				if (eth_sap != ETHERTYPE_IP) {
					rw_exit(&hbx_lock);
					return (1);
				}
				pdev->mac_ok = 1;
				rw_exit(&hbx_lock);
				return (1);
			default:
				break;
			}
			rw_exit(&hbx_lock);
		}
	}

	return (0);
}

/*
 * Is a packet received from a driver a heartbeat ?
 */
static int
hbx_isheartbeat(queue_t *q, mblk_t *mp, ipha_t *ipha)
{
	int		len;
	hbx_version_t	*version_addr;
	hbx_delay_t	*delay_addr;
	hbx_sn_t	*sn_addr;
	hbx_in_t	*in_addr;
	hbx_node_id_t	*node_id_addr;
	hbx_hb_data_t	hb_data;
	hbx_msg_t	*hb;
	struct udphdr	*udph;
	ipaddr_t	src; /* the ip address in host order */
	hbx_device_t	*pdev = (hbx_device_t *)q->q_ptr;
	int		status;
	hbx_error_t	hb_error;

	/* Initialize error flag */
	hb_error.error_code = HBX_ERROR_SUCCESS;

	/* LINTED checked */
	len = mp->b_wptr - ((uchar_t *)ipha);

	if (len < 0) {
		/* bogus device drivers */
		status = 0;
		goto terminate;
	}

	if ((!OK_32PTR(((uchar_t *)ipha))) || (len < IP_SIMPLE_HDR_LENGTH)) {
		/* would be done by IP module anyway */
		if (!pullupmsg(mp, IP_SIMPLE_HDR_LENGTH)) {
			cmn_err(hbx_debug, "Pullupmsg has failed\n");
			status = 0;
			goto terminate;
		}
		ipha = (ipha_t *)mp->b_rptr;
		/* LINTED checked */
		len = mp->b_wptr - ((uchar_t *)ipha);
	}

	/* Avoid useless processing if receive mode is off */
	if ((pdev->mode & HBX_RCV_MODE_ON) ||
	    (hbx_debug == CE_NOTE)) {
		/* Check the IP header */
		if (hbx_check_iph(ipha)) {
			status = 0;
			goto terminate;
		}

		src = ntohl(ipha->ipha_src);

		/* Check the UDP header */
		udph = (struct udphdr *)((uchar_t *)ipha + sizeof (ipha_t));
		if (hbx_check_udph(udph, src)) {
			status = 0;
			goto terminate;
		}
	}

	if (len != (HBX_ALL_HDR_SIZE + HBX_MSG_SIZE)) {
		if (!pullupmsg(mp,
		    HBX_ALL_HDR_SIZE + HBX_MSG_SIZE)) {
		    cmn_err(hbx_debug, "Pullup has failed len = %d\n", len);
			status = 0;
			goto terminate;
		}
		ipha = (ipha_t *)mp->b_rptr;
	}

	/*
	 * Avoid useless processing if receive mode is off
	 */
	if ((pdev->mode & HBX_RCV_MODE_ON) ||
	    (hbx_debug == CE_NOTE)) {
		/* Check the heartbeat message */
		hb = (hbx_msg_t *)((uchar_t *)udph +
		    sizeof (struct udphdr));
		if (hbx_check_msg(q, hb, src, &hb_error)) {
			status = 0;
			goto terminate;
		}

		/* Get heartbeat message fields addresses */
		version_addr = HBX_GET_VERSION_ADDR(hb);
		delay_addr = HBX_GET_DELAY_ADDR(hb);
		sn_addr = HBX_GET_SN_ADDR(hb);
		in_addr = HBX_GET_IN_ADDR(hb);
		node_id_addr = HBX_GET_NODE_ID_ADDR(hb);

		/* Get heartbeat message data */
		bcopy((void *)sn_addr, (void *)&(hb_data.sn),
		    sizeof (hbx_sn_t));
		bcopy((void *)in_addr, (void *)&(hb_data.in),
		    sizeof (hbx_in_t));
		bcopy((void *)delay_addr, (void *)&(hb_data.delay),
		    sizeof (hbx_delay_t));
		bcopy((void *)version_addr, (void *)&(hb_data.version),
		    sizeof (hbx_version_t));
		bcopy((void *)node_id_addr, (void *)&(hb_data.node_id),
		    sizeof (hbx_node_id_t));

		/* Convert data to host representation */
		hb_data.in = ntohl(hb_data.in);
		hb_data.version = ntohs(hb_data.version);
		hb_data.delay = ntohl(hb_data.delay);
		hb_data.sn = ntohl(hb_data.sn);
		hb_data.node_id = ntohl(hb_data.node_id);
		status = 1;
	} else {
		/*
		 * Let the packet go upstream to IP module in the
		 * hbxmod module pushed over the driver
		 */
		status = 0;
	}

terminate:
	/*
	 * status = 0:
	 * Let the packet go upstream to IP module in the hbxmod module pushed
	 * over the driver
	 *
	 * status = 1:
	 * Hearbeat will not be sent upstream to IP module,
	 * by hbxmod module pushed over the driver, it will be released.
	 */
	if ((status) || (hb_error.error_code != HBX_ERROR_SUCCESS)) {
		hbx_hdlheartbeat(q, &src, &hb_data, &hb_error);
	}
	return (status);
}

static void
hbx_log_error(hbx_error_t *hb_error, hbx_host_t *phost)
{
	if (HBX_TEST_ERROR(phost)) {
		cmn_err(CE_WARN,
		    hbx_error[hb_error->error_code],
		    hb_error->src);
		phost->error = 1;
	} else {
		phost->error++;
	}
}

/*
 * Handle a heartbeat packet
 */
static void
hbx_hdlheartbeat(queue_t *q, ipaddr_t *src, hbx_hb_data_t *hb_data,
    hbx_error_t *hb_error)
{
	hbx_host_t	*host;
	unsigned int	index;
	hbx_device_t	*pdev;
	clock_t		cdelay;

	rw_enter(&hbx_lock, RW_READER);

	pdev = (hbx_device_t *)q->q_ptr;
	ASSERT(pdev != NULL);

	rw_enter(&(pdev->lock), RW_READER);

	index = HBX_HOST_HASH(*src, pdev->max_hosts);

	host = hbx_search_host(q, src, index);

	if (rw_tryupgrade(&(pdev->lock)) == 0) {
		rw_exit(&(pdev->lock));
		rw_enter(&(pdev->lock), RW_WRITER);
	}

	if (host == NULL) {
		/*
		 * The host does not exist:
		 *
		 * a) The new host is not in error (HBX_ERROR_SUCCESS)
		 *
		 *    It is created with the HOST_STATE_UP in the host table
		 *    and a EVT_HOST_DETECTED event is sent (see hbx_add_host)
		 *
		 * b) The new host is in error
		 *
		 *    It is created with the HOST_STATE_ERROR in the host table
		 *    (see hbx_add_host) and the error is logged
		 */
		if (hb_error->error_code == HBX_ERROR_SUCCESS) {
			cdelay = drv_usectohz(hb_data->delay * 1000);
			hbx_add_host(q, index, src, cdelay, hb_data);
		} else {
			hbx_add_host(q, index, src, 0, hb_data);
			host = hbx_search_host(q, src, index);
			ASSERT(host != NULL);
			hbx_log_error(hb_error, host);
		}
	} else {
		/*
		 * The host already exists:
		 *
		 * a) the host has the HOST_STATE_ERROR state
		 *
		 *    o The host is in error
		 *	the error is periodically logged
		 *
		 *    o The host is no more in error (HBX_ERROR_SUCCESS)
		 *	a new host is created in the table and an event
		 *	EVT_HOST_DETECTED is sent (see hbx_add_host)
		 *
		 * b) the host has the HOST_STATE_DOWN state
		 *
		 *    o The host is in error
		 *	the error is periodically logged
		 *
		 *    o The host is not in error
		 *	- the node id is checked (must be the same)
		 *	- the delay is checked (possibly changed)
		 *
		 *	If all checks are OK
		 *	- the sn is resetted
		 *	- the error flag is resetted
		 *	- an event EVT_HOST_UP is sent
		 *	- an event EVT_DELAY (optional)
		 *	- a supervision timer is armed
		 *
		 * c) the host has the HOST_STATE_UP state
		 *
		 *    o The host is in error
		 *	the error is periodically logged
		 *
		 *    o The host is not in error
		 *	- the in is checked (must be constant)
		 *	- the sn is checked (must be growing)
		 *	- the node id is checked (must be the same)
		 *	- the delay is checked (possibly changed)
		 *
		 *	If all checks are OK
		 *	- the new sn is stored
		 *	- the old supervision timer is cancelled
		 *	- a new supervision timer is armed
		 *	- an event EVT_DELAY is sent (optional)
		 *
		 */
		if (hb_error->error_code != HBX_ERROR_SUCCESS) {
			/*
			 * Log the error previously set during checks
			 */
			hbx_log_error(hb_error, host);
			goto terminate;
		} else {
			if (host->hstate == HOST_STATE_ERROR) {
				/*
				 * The host already exists but the data are
				 * not initialized and the heartbeat
				 * supervision has not been started.
				 * Delete the host and re-initialize it as if
				 * it was a new one
				 */
				if (hbx_del_host(pdev, src) != 0) {
					cmn_err(CE_PANIC,
					    "Cannot delete host %x\n", *src);
				}
				cdelay = drv_usectohz(hb_data->delay * 1000);
				hbx_add_host(q, index, src, cdelay, hb_data);
			} else if (host->hstate == HOST_STATE_DOWN) {
				/* Check that the node id has notchanged */
				if (host->node_id != hb_data->node_id) {
					HBX_CREATE_ERROR(hb_error, *src,
					    HBX_ERROR_NODE_ID);
					hbx_log_error(hb_error, host);
					goto terminate;
				}
				/* Look if the detection delay has changed */
				cdelay = drv_usectohz(hb_data->delay * 1000);
				if (host->ticks != cdelay) {
					host->ticks = cdelay;
					host->delay = hb_data->delay;
					hbx_signalevt(host, EVT_HOST_DELAY);
				}
				/* Reinitialize host data */
				host->sn = 0;
				host->in = hb_data->in;
				host->error = 0;
				host->hstate = HOST_STATE_UP;
				/* Signal, remote host is up again */
				hbx_signalevt(host, EVT_HOST_UP);
				/* Arm a new supervision timer */
				host->tid = qtimeout(q, hbx_hbtimeout,
				    host, host->ticks);
			} else if (host->hstate == HOST_STATE_UP) {
				/* Check that sn is growing */
				if (hb_data->sn <= host->sn) {
					HBX_CREATE_ERROR(hb_error, *src,
					    HBX_ERROR_SN);
					hbx_log_error(hb_error, host);
					goto terminate;
				}
				/* Check that host in has not changed */
				if (hb_data->in != host->in) {
					HBX_CREATE_ERROR(hb_error, *src,
					    HBX_ERROR_UNEXP_IN);
					hbx_log_error(hb_error, host);
					goto terminate;
				}
				/* Check that the node id has not changed */
				if (host->node_id != hb_data->node_id) {
					HBX_CREATE_ERROR(hb_error, *src,
					    HBX_ERROR_NODE_ID);
					hbx_log_error(hb_error, host);
					goto terminate;
				}
				/* Look if the detection delay has changed */
				cdelay = drv_usectohz(hb_data->delay * 1000);
				if (host->ticks != cdelay) {
					host->ticks = cdelay;
					host->delay = hb_data->delay;
					hbx_signalevt(host, EVT_HOST_DELAY);
				}
				/* Store new sn */
				host->sn = hb_data->sn;
				/* Do not signal heartbeat within delay */
				quntimeout(q, host->tid);
				/* Arm a new supervision timer */
				host->tid = qtimeout(q, hbx_hbtimeout,
				    host, host->ticks);
			} else {
				cmn_err(CE_PANIC,
				    "Unknown host state for host %x\n", *src);
			}
		}
	}

terminate:
	rw_exit(&(pdev->lock));
	rw_exit(&hbx_lock);
}

/*
 * hbxdrv driver memory allocator: allocate
 */
static void *
hbx_mem_alloc(size_t size)
{
	void *ret = kmem_zalloc(size, KM_NOSLEEP);

	if (ret != NULL) {
		/* Memory usage statistics */
		hbx_memory += size;
		if (hbx_memory > hbx_highest_memory) {
			hbx_highest_memory = hbx_memory;
		}
	} else {
		hbx_alloc_error++;
	}
	return (ret);
}

/*
 * hbxdrv driver memory allocator: release
 */
static void
hbx_mem_free(void *ptr, size_t size)
{
	kmem_free(ptr, size);
	/* Memory usage statistics */
	hbx_memory -= size;
}

/*
 * Search a heartbeat sender host in the per device host table
 */
static hbx_host_t *
hbx_search_host(queue_t *q, ipaddr_t *addr, unsigned int index)
{
	hbx_device_t *pdev;
	hbx_host_t *ptr;

	pdev = (hbx_device_t *)q->q_ptr;
	ASSERT(pdev != NULL);

	if (pdev->hosttable[index].flag == HBX_HOST_FLAG_NONE) {
		return (NULL);
	}

	if (pdev->hosttable[index].flag & HBX_HOST_FLAG_USED) {
		ASSERT(pdev->hosttable[index].addr != 0);
		if (pdev->hosttable[index].addr == *addr) {
			/* a direct hit */
			return (&(pdev->hosttable[index]));
		}
	}
	/* Collision case */
	ptr = pdev->hosttable[index].c_next;
	while (ptr != NULL) {
		ASSERT(ptr->addr != 0);
		if (ptr->addr == *addr) {
			/* found in collision linked list */
			break;
		}
		ptr = ptr->c_next;
	}
	return (ptr);
}

/*
 * Add a heartbeat host sender in the per device host table
 */
static void
hbx_add_host(queue_t *q, unsigned int index, ipaddr_t *addr, clock_t cdelay,
    hbx_hb_data_t *hb_data)
{
	hbx_device_t *pdev;
	hbx_host_t *host;

	pdev = (hbx_device_t *)q->q_ptr;
	ASSERT(pdev != NULL);

	if (pdev->hosttable[index].flag !=
	    HBX_HOST_FLAG_NONE) {
		hbx_host_t *ptr = &(pdev->hosttable[index]);
		for (;;) {
			if (ptr->addr == *addr) {
				return;
			}
			if (ptr->c_next == NULL) {
				break;
			} else {
				ptr = ptr->c_next;
			}
		}
		if (pdev->host_nb >= pdev->max_hosts) {
			return;
		}
		host = (hbx_host_t *)\
			hbx_mem_alloc(sizeof (hbx_host_t));
		if (host == NULL) {
			return;
		}
		ptr->c_next = host;
		host->c_prev = ptr;
		pdev->hosttable[index].flag |= HBX_HOST_FLAG_COLL;
		pdev->host_coll_nb++;
	} else {
		/* Direct hash hit is a free entry, record */
		ASSERT(pdev->hosttable[index].addr == 0);
		if (pdev->host_nb >= pdev->max_hosts) {
			return;
		}
		pdev->hosttable[index].flag |= HBX_HOST_FLAG_USED;
		host = &(pdev->hosttable[index]);
	}

	/*
	 * Linked list of hosts per device.
	 */
	if (pdev->hosts == NULL) {
		pdev->hosts = host;
		host->next = NULL;
		host->prev = NULL;
	} else {
		hbx_host_t *ptr;
		ptr = pdev->hosts;
		while (ptr->next != NULL) {
			ptr = ptr->next;
		}
		host->next = NULL;
		host->prev = ptr;
		ptr->next = host;
	}
	host->pdev = pdev;
	host->addr = *addr;
	host->ticks = cdelay;
	host->error = 0;
	/*
	 * Set the host up only if it is not in error
	 */
	if (cdelay != 0) {
		host->hstate = HOST_STATE_UP;
		host->delay = hb_data->delay;
		host->sn = 0;
		host->in = hb_data->in;
		host->node_id = hb_data->node_id;
		cmn_err(CE_NOTE,
		    "Creating host in state HOST_STATE_UP");
		/*
		 * Arm timer and signal new host
		 */
		host->tid = qtimeout(q, hbx_hbtimeout, host, host->ticks);
		/*
		 * Signal a new detected host on this device
		 */
		(void) hbx_signalevt(host, EVT_HOST_DETECTED);
	} else {
		host->hstate = HOST_STATE_ERROR;
		host->delay = 0;
		host->sn = 0;
		host->in = 0;
		host->node_id = 0;
		cmn_err(CE_NOTE,
		    "Creating host in state HOST_STATE_ERROR");
	}
	pdev->host_nb++;
}

/*
 * Delete a host in the per device host table
 */
static int
hbx_del_host(hbx_device_t *pdev, ipaddr_t *addr)
{
	hbx_host_t *host;
	hbx_host_t *ptr;
	unsigned int index;

	ASSERT(pdev != NULL);

	if (pdev->hosts == NULL) {
		return (1);
	}
	ASSERT(pdev->q != NULL);

	index = HBX_HOST_HASH(*addr, pdev->max_hosts);

	host = NULL;

	if (pdev->hosttable[index].flag & HBX_HOST_FLAG_USED) {
		if (pdev->hosttable[index].addr == *addr) {
			/* A direct hash hit */
			host = &(pdev->hosttable[index]);
			host->flag &= ~HBX_HOST_FLAG_USED;
			if (host->hstate == HOST_STATE_UP) {
				(void) quntimeout(pdev->q, host->tid);
			}
			host->pdev = NULL;
			host->hstate = 0;
			host->addr = 0;
		}
	}

	if (host == NULL) {
		ptr = pdev->hosttable[index].c_next;
		while (ptr != NULL) {
			ASSERT(ptr->addr != 0);
			if (ptr->addr == *addr) {
				if (ptr->c_next != NULL) {
					(ptr->c_next)->c_prev = ptr->c_prev;
				}
				ASSERT(ptr->c_prev != NULL);
				(ptr->c_prev)->c_next = ptr->c_next;
				host = ptr;
				if (host->hstate == HOST_STATE_UP) {
					(void) quntimeout(pdev->q, host->tid);
				}
				/* Release this collision aera */
				hbx_mem_free((void *)ptr,
					sizeof (hbx_host_t));
				if (pdev->hosttable[index].c_next == NULL) {
					pdev->hosttable[index].flag &= \
					~HBX_HOST_FLAG_COLL;
				}
				pdev->host_coll_nb--;
				break;
			}
			ptr = ptr->c_next;
		}
	}

	if (host == NULL) {
		return (1);
	}

	pdev->host_nb--;

	/*
	 * Linked list of hosts per device.
	 */
	if (pdev->hosts == host) {
		ASSERT(host->prev == NULL);
		pdev->hosts = host->next;
		if (host->next != NULL) {
			(host->next)->prev = NULL;
		}
	} else {
		ASSERT(host->prev != NULL);
		(host->prev)->next = host->next;
		if (host->next != NULL) {
			(host->next)->prev = host->prev;
		}
	}
	return (0);
}

/*
 * Create the per device host table
 */
static int
hbx_init_hosttable(hbx_device_t *pdev)
{
	size_t size;
	void *addr;

	ASSERT(pdev != NULL);

	if (hbx_ipdevi == NULL) {
		/* No IP module ??? */
		pdev->max_hosts = HBX_DEFAULT_MAX_HOSTS;
	} else {
		/*
		 * The default hash host table size can be changed
		 * with ndd -set /dev/ip  ip_hbx_max_hosts command
		 */
		pdev->max_hosts = (unsigned int)
			ddi_prop_get_int(DDI_DEV_T_ANY,
				hbx_ipdevi,
				DDI_PROP_DONTPASS,
				"ip_hbx_max_hosts",
				HBX_DEFAULT_MAX_HOSTS);
		if (pdev->max_hosts < HBX_MIN_HOSTS) {
			pdev->max_hosts = HBX_MIN_HOSTS;
		}
	}

	size = sizeof (hbx_host_t) * pdev->max_hosts;
	addr = hbx_mem_alloc(size);

	if (addr == NULL) {
		return (-1);
	}
	pdev->hosttable = (hbx_host_t *)addr;
	pdev->hosttable_size = size;
	return (0);
}

/*
 * Delete the per device host table, including hash collision aeras
 */
static void
hbx_free_hosttable(hbx_device_t *pdev)
{
	uint_t		i;
	hbx_host_t	*ptr;
	hbx_host_t	*sptr;

	ASSERT(pdev != NULL);

	if (pdev->hosttable != NULL) {
		for (i = 0; i < pdev->max_hosts; i++) {
			if (pdev->hosttable[i].addr != 0) {
				if (pdev->hosttable[i].hstate ==
				    HOST_STATE_UP) {
					(void) quntimeout(pdev->q,
					pdev->hosttable[i].tid);
				}
			}
			ptr = pdev->hosttable[i].c_next;
			while (ptr != NULL) {
				sptr = ptr->c_next;
				ASSERT(ptr->addr != 0);
				ASSERT(ptr->pdev != NULL);
				ASSERT((ptr->pdev)->q != NULL);
				if (ptr->hstate == HOST_STATE_UP) {
					(void) quntimeout(
						(ptr->pdev)->q, ptr->tid);
				}
				hbx_mem_free((void *)ptr,
					sizeof (hbx_host_t));
				ptr = sptr;
			}
		}
		hbx_mem_free((void *)pdev->hosttable,
			pdev->hosttable_size);

		pdev->hosttable = NULL;
		pdev->hosttable_size = 0;
	}
}

/*
 * Allocate an mblk_t (a stream message)
 */
static mblk_t *
hbx_mblk_alloc(size_t size)
{
	mblk_t *mp;
	mp = allocb(size, BPRI_HI);
	if (mp == NULL) {
		/* Does not return */
		hbx_reboot("Unable to sent heartbeats, no memory");
		/* If reboot behavior is removed ? */
		return (NULL);
	}
	bzero((void *)(mp->b_rptr), size);
	ASSERT(mp->b_rptr == mp->b_wptr);
	mp->b_wptr += size;
	return (mp);
}

/*
 * Signal a notification event to any applications
 * that has open hbxdrv driver through /dev/hbxdrv
 */
static int
hbx_signalevt(hbx_host_t *host, enum hbx_event_type type)
{
	hbx_device_t *pdev;
	hbx_event_t *pevt;
	mblk_t *mpi;
	mblk_t *mp;
	hbx_upstream_t *up;

	if (hbx_upstream == NULL) {
		return (0);
	}

	ASSERT(host != NULL);
	ASSERT(host->pdev != NULL);
	pdev = (hbx_device_t *)host->pdev;
	ASSERT(pdev != NULL);

	mpi = hbx_mblk_alloc(sizeof (hbx_event_t));
	if (mpi == NULL) {
		return (-1);
	}
	pevt = (hbx_event_t *)mpi->b_rptr;
	pevt->time = gethrtime();
	pevt->dev = (hbx_dev_t)pdev->dev;
	pevt->id = pdev->id;
	pevt->type = type;
	pevt->src = host->addr;
	pevt->state = host->hstate;
	pevt->node_id = host->node_id;
	pevt->delay = (unsigned int)
		(drv_hztousec(host->ticks) / 1000);
	pevt->in = host->in;

	up = hbx_upstream;

	if (up->next == NULL) {
		ASSERT(up->q != NULL);
		if (canputnext(up->q)) {
			putnext(up->q, mpi);
		} else {
			freemsg(mpi);
		}
	} else {
		while (up != NULL) {
			mp = dupmsg(mpi);
			if (mp != NULL) {
				ASSERT(up->q != NULL);
				if (canputnext(up->q)) {
					putnext(up->q, mp);
				} else {
					freemsg(mp);
				}
			}
			up = up->next;
		}
		freemsg(mpi);
	}

	return (0);
}

/*
 * Signal a global notification event to any applications
 * that has open hbxdrv driver through /dev/hbxdrv
 */
static int
hbx_signalevtid(hbx_device_t *pdev, enum hbx_event_type type)
{
	hbx_event_t *pevt;
	mblk_t *mpi;
	mblk_t *mp;
	hbx_upstream_t *up;

	if (hbx_upstream == NULL) {
		return (0);
	}
	ASSERT(pdev != NULL);

	mpi = hbx_mblk_alloc(sizeof (hbx_event_t));
	if (mpi == NULL) {
		return (-1);
	}
	pevt = (hbx_event_t *)mpi->b_rptr;
	pevt->time = gethrtime();
	pevt->dev = (hbx_dev_t)pdev->dev;
	pevt->id = pdev->id;
	pevt->type = type;

	up = hbx_upstream;

	if (up->next == NULL) {
		ASSERT(up->q != NULL);
		if (canputnext(up->q)) {
			putnext(up->q, mpi);
		} else {
			freemsg(mpi);
		}
	} else {
		while (up != NULL) {
			mp = dupmsg(mpi);
			if (mp != NULL) {
				ASSERT(up->q != NULL);
				if (canputnext(up->q)) {
					putnext(up->q, mp);
				} else {
					freemsg(mp);
				}
			}
			up = up->next;
		}
		freemsg(mpi);
	}
	return (0);
}

/*
 * Create a heartbeat DLPI message.
 */
static uint32_t
hbx_compute_iph_checksum(uint16_t *uph)
{
	/* Compute ip checksum from ip header */
	uint32_t sum = uph[0] + uph[1] + uph[2] + uph[3] + uph[4] +
	    uph[5] + uph[6] + uph[7] + uph[8] + uph[9];
	sum = (sum & 0xFFFF) + (sum >> 16);
	sum = ~(sum + (sum >> 16)) & 0xFFFF;
	if (sum == 0xffff) {
		sum = 0;
	}
	return (sum);
}

static uint32_t
hbx_compute_udph_checksum(uint16_t *uph, uint16_t udp_proto, uint16_t udp_len)
{
	uint32_t sum = uph[6] + uph[7] + uph[8] + uph[9] + udp_proto + udp_len;
	sum = (sum & 0xFFFF) + (sum >> 16);
	sum = ~(sum + (sum >> 16)) & 0xFFFF;
	if (sum == 0xffff) {
		sum = 0;
	}
	return (sum);
}

static void
hbx_write_iph(ipha_t *ipha, hbx_device_t *pdev)
{
	uint32_t sum;

	ipha->ipha_version_and_hdr_length = (IP_VERSION << 4) |
		IP_SIMPLE_HDR_LENGTH_IN_WORDS;
	ipha->ipha_type_of_service = 0;
	ipha->ipha_length = htons(HBX_ALL_HDR_SIZE + HBX_MSG_SIZE);
	ipha->ipha_ident = 0;
	/* Don't fragment */
	ipha->ipha_fragment_offset_and_flags = htons(IPH_DF);
	ipha->ipha_ttl = hbx_ip_ttl;
	ipha->ipha_protocol = HBX_PROTOCOL;
	ipha->ipha_hdr_checksum = 0;
	ipha->ipha_src = htonl(pdev->src);
	ipha->ipha_dst = htonl(pdev->dst);
	ipha->ipha_hdr_checksum = hbx_compute_iph_checksum((uint16_t *)ipha);
}

static void
hbx_write_udph(struct udphdr *udph, ipha_t *ipha)
{
	uint32_t sum;
	uint32_t udp_len = HBX_UDPHDR_SIZE + HBX_MSG_SIZE;
	uint16_t udp_proto = HBX_PROTOCOL;

	udp_proto = htons(HBX_PROTOCOL);
	udp_len = htons(udp_len);

	udph->uh_sport = 0;
	udph->uh_dport = htons(hbx_udp_dst_port);
	udph->uh_ulen = udp_len;

	/* Compute udp checksum from udp pseudo-header */
	udph->uh_sum = hbx_compute_udph_checksum((uint16_t *)ipha,
	    udp_proto, udp_len);
}

static mblk_t *
hbx_create_heartbeat(hbx_device_t *pdev)
{
	mblk_t			*mp, *mpd;
	ipha_t			*ipha;
	struct udphdr		*udph;
	dl_unitdata_req_t	*req;
	hbx_msg_t		*hb;
	uint16_t		sap;

	mpd = hbx_dlpi_comm(DL_UNITDATA_REQ,
		DL_UNITDATA_REQ_SIZE + pdev->mac_len + pdev->mac_sap_len);

	if (mpd == NULL) {
		return (NULL);
	}

	mp = hbx_mblk_alloc(HBX_HB_OFFSET + HBX_MSG_SIZE);
	if (mp == NULL) {
		freemsg(mpd);
		return (NULL);
	}

	/* The DLPI DL_UNITDATA_REQ request */
	req = (dl_unitdata_req_t *)mpd->b_rptr;
	mpd->b_datap->db_type = M_PROTO;
	req->dl_primitive = DL_UNITDATA_REQ;
	req->dl_dest_addr_length = (pdev->mac_len + pdev->mac_sap_len);
	req->dl_dest_addr_offset = DL_UNITDATA_REQ_SIZE;
	req->dl_priority.dl_min = 0;
	req->dl_priority.dl_max = 0;

	/* Swap the mac_sap bytes if necessary */
	sap = htons(*((uint16_t *)pdev->mac_sap));

	if (pdev->mac_sap_off == 0) {
		bcopy((void *)&sap,
			(void *)((uchar_t *)req + req->dl_dest_addr_offset),
			pdev->mac_sap_len);
		bcopy((void *)pdev->mac_dst,
			(void *)((uchar_t *)req +
				req->dl_dest_addr_offset + pdev->mac_sap_len),
			pdev->mac_len);
	} else {
		bcopy((void *)pdev->mac_dst,
			(void *)((uchar_t *)req + req->dl_dest_addr_offset),
			pdev->mac_len);
		bcopy((void *)&sap,
			(void *)((uchar_t *)req +
				req->dl_dest_addr_offset + pdev->mac_len),
			pdev->mac_sap_len);
	}

	/* Fill the IP header */
	ipha = (ipha_t *)(mp->b_rptr);
	hbx_write_iph(ipha, pdev);

	/* Fill the UDP header */
	udph = (struct udphdr *)((uchar_t *)ipha + sizeof (ipha_t));
	hbx_write_udph(udph, ipha);

	/* Write The hearbeat data */
	hb = (hbx_msg_t *)((uchar_t *)ipha + HBX_HB_OFFSET);
	hbx_write_msg(hb, pdev);

	ASSERT(mpd->b_cont == NULL);
	mpd->b_cont = mp;

	return (mpd);
}

/*
 * Create a fast path heartbeat DLPI message.
 */
static mblk_t *
hbx_create_fp_heartbeat(hbx_device_t *pdev)
{
	struct ether_header	*eth;
	ipha_t			*ipha;
	struct udphdr		*udph;
	hbx_msg_t		*hb;
	mblk_t			*mp;
	uint16_t		sap;

	mp = hbx_mblk_alloc(HBX_FP_HB_OFFSET + HBX_MSG_SIZE);

	if (mp == NULL) {
		return (NULL);
	}

	if ((pdev->mac_sap_len != sizeof (ushort_t)) ||
	    (pdev->mac_len != sizeof (struct  ether_addr))) {
		freemsg(mp);
		return (NULL);
	}

	/* Swap the mac_sap bytes if necessary */
	sap = htons(*((uint16_t *)pdev->mac_sap));

	eth = (struct ether_header *)mp->b_rptr;

	bcopy((void *)&sap, (void *)(&(eth->ether_type)), sizeof (ushort_t));
	bcopy((void *)&(pdev->mac_dst[0]),
		(void *)&(eth->ether_dhost),
		sizeof (struct  ether_addr));
	bcopy((void *)&(pdev->mac_src[0]),
		(void *)&(eth->ether_shost),
		sizeof (struct  ether_addr));

	/* Handle alignement pbm due to struct ether_header size */
	ipha = (ipha_t *)hbx_mem_alloc(HBX_ALL_HDR_SIZE + HBX_MSG_SIZE);

	if (ipha == NULL) {
		freemsg(mp);
		return (NULL);
	}

	/* Write the IP header */
	hbx_write_iph(ipha, pdev);

	/* Write the UDP header */
	udph = (struct udphdr *)((uchar_t *)ipha + sizeof (ipha_t));
	hbx_write_udph(udph, ipha);

	/* Write The hearbeat data */
	hb = (hbx_msg_t *)((uchar_t *)ipha + HBX_HB_OFFSET);
	hbx_write_msg(hb, pdev);

	/*
	 * Now copy the IP header and data
	 */
	bcopy((void *)ipha,
	    (void *)((uchar_t *)eth + sizeof (struct ether_header)),
	    HBX_ALL_HDR_SIZE + HBX_MSG_SIZE);
	/* Release memory */
	hbx_mem_free((void *)ipha,
	    HBX_ALL_HDR_SIZE + HBX_MSG_SIZE);

	return (mp);
}

/*
 * Lookup if a device exists
 */
static hbx_device_t *
hbx_finddev(hbx_dev_t *dev)
{
	hbx_device_t *hbd;

	hbd = hbx_devs;

	while (hbd != NULL && ((hbx_dev_t)hbd->dev) != *dev) {
		hbd = hbd->next;
	}
	return (hbd);
}

/*
 * Lookup for a host, for a given device
 */
static hbx_host_t *
hbx_findhost(hbx_device_t *pdev, ipaddr_t *addr)
{
	hbx_host_t *host;

	host = pdev->hosts;
	while (host != NULL && host->addr != *addr) {
		host = host->next;
	}
	return (host);
}

/*
 * Allocate a DLPI message
 */
static mblk_t *
hbx_dlpi_comm(t_uscalar_t prim, size_t size)
{
	mblk_t  *mp;
	dl_info_req_t *req;

	mp = hbx_mblk_alloc(size);
	if (mp != NULL) {
		/*
		 * DLPIv2 says that DL_INFO_REQ
		 * are sent with M_PCPROTO
		 */
		if (prim == DL_INFO_REQ) {
			DB_TYPE(mp) = M_PCPROTO;
		} else {
			DB_TYPE(mp) = M_PROTO;
		}
		req = (dl_info_req_t *)mp->b_rptr;
		req->dl_primitive = prim;
	}
	return (mp);
}

#ifdef	HBX_CALLS_SETMULTICAST
/*
 * Setup a driver to accept multicasts
 */
static int
hbx_setmulticast(queue_t *q, uchar_t *addr, unsigned int len)
{
	mblk_t *mp;
	dl_enabmulti_req_t *multi;

	ASSERT(q != NULL);
	ASSERT(q->q_ptr != NULL);
	ASSERT(addr != NULL);
	ASSERT(len != 0);

	mp = hbx_dlpi_comm(DL_ENABMULTI_REQ,
		DL_ENABMULTI_REQ_SIZE + len);
	if (mp != NULL) {
		multi = (dl_enabmulti_req_t *)mp->b_rptr;
		multi->dl_addr_offset = DL_ENABMULTI_REQ_SIZE;
		multi->dl_addr_length = len;
		bcopy((void *)addr,
			(void *)((uchar_t *)multi +
				DL_ENABMULTI_REQ_SIZE),
			len);
		if (canputnext(WR(q))) {
			putnext(WR(q), mp);
			return (0);
		} else {
			freemsg(mp);
			return (1);
		}
	} else {
		return (1);
	}
}
#endif

/*
 * hbxdrv open has failed, release
 * everything that was allocated
 */
static void
hbx_open_error(queue_t *q)
{
	hbx_device_t *pdev;
	hbx_device_t *hbd;

	qprocsoff(q);

	pdev = (hbx_device_t *)q->q_ptr;
	ASSERT(pdev != NULL);
	ASSERT(pdev->heartbeat == NULL);

	/* remove from linked list of devs */
	rw_enter(&hbx_lock, RW_WRITER);
	hbd = hbx_devs;
	ASSERT(hbd != NULL);
	if (hbd == pdev) {
		ASSERT(pdev->prev == NULL);
		if (pdev->next != NULL) {
			(hbd->next)->prev = NULL;
		}
		hbx_devs = pdev->next;
	} else {
		do {
			hbd = hbd->next;
		} while (hbd != NULL && hbd != pdev);
		ASSERT(hbd != NULL);
		ASSERT(hbd->prev != NULL);
		(hbd->prev)->next = hbd->next;
		if (hbd->next != NULL) {
			(hbd->next)->prev = hbd->prev;
		}
	}
	hbx_devs_nb--;
	rw_destroy(&(pdev->lock));
	rw_exit(&hbx_lock);

	hbx_free_hosttable(pdev);

	hbx_mem_free((void *)pdev,
		sizeof (hbx_device_t));

	WR(q)->q_ptr = q->q_ptr = NULL;
}

/*
 * Find the IP module devinfo
 */
static void
hbx_findipdevi()
{
	/*
	 * Find the ip module
	 */
	hbx_ipdevi = ddi_root_node();
	if (hbx_ipdevi != NULL) {
		hbx_ipdevi = ddi_get_child(hbx_ipdevi);
		while (hbx_ipdevi != NULL) {
			if (strcmp(ddi_get_name(hbx_ipdevi), "ip") == 0) {
				break;
			}
			if (strcmp(ddi_get_name(hbx_ipdevi), "pseudo") == 0) {
				hbx_ipdevi = ddi_get_child(hbx_ipdevi);
			} else {
				hbx_ipdevi =
					ddi_get_next_sibling(hbx_ipdevi);
			}
		}
	}
}

/*
 * miocpullup function is availabe in Solaris 9,
 * but it does not exists in Solaris 8.
 * So furnish here one, based on Solaris9 real miocpullup function.
 */
static int
hbx_miocpullup(mblk_t *iocmp, size_t size)
{
	struct iocblk	*iocp = (struct iocblk *)iocmp->b_rptr;
	mblk_t		*datamp = iocmp->b_cont;
	mblk_t		*newdatamp;

	/*
	 * We'd like to be sure that DB_TYPE(iocmp) == M_IOCTL, but some
	 * nitwit routines like ttycommon_ioctl() always reset the type of
	 * legitimate M_IOCTL messages to M_IOCACK as a "courtesy" to the
	 * caller, even when the routine does not understand the M_IOCTL.
	 * The ttycommon_ioctl() routine does us the additional favor of
	 * clearing ioc_count, so we cannot rely on it having a correct
	 * size either (blissfully, ttycommon_ioctl() does not screw with
	 * TRANSPARENT messages, so we can still sanity check for that).
	 */
	/* LINTED checked */
	ASSERT(MBLKL(iocmp) == sizeof (struct iocblk));
	/* LINTED checked */
	if (MBLKL(iocmp) != sizeof (struct iocblk)) {
		cmn_err(CE_WARN, "miocpullup: passed mblk_t %p is not an ioctl"
		    " mblk_t", (void *)iocmp);
		return (EINVAL);
	}

	if (iocp->ioc_count == TRANSPARENT)
		return (EINVAL);

	if (size == 0)
		return (0);

	if (datamp == NULL)
		return (EINVAL);

	/* LINTED checked */
	if (MBLKL(datamp) >= size)
		return (0);

	newdatamp = msgpullup(datamp, size);
	if (newdatamp == NULL)
		return (ENOMEM);

	iocmp->b_cont = newdatamp;
	freemsg(datamp);
	return (0);
}

/*
 * Reboot the host
 */
/* LINTED checked */
static void
hbx_reboot(char *str)
{
	cmn_err(CE_NOTE, "%s\n", str);
	prom_printf("hbxdrv: %s\n", str); /* on console, in any case */

/*
 * mdboot profile changes from Solaris 11 release
 */
#if SOL_VERSION >= 200710
	mdboot(A_REBOOT, AD_BOOT, NULL, B_FALSE);
#endif
#if SOL_VERSION < 200710
	mdboot(A_REBOOT, AD_BOOT, NULL);
#endif

}

/*
 * Add the hbxdrv cyclic
 */
static void
hbx_cyclic_add()
{
	cyc_handler_t hdlr;
	cyc_time_t when;

	if (hbx_freq.set == 0) {
		return;
	}

	hdlr.cyh_func = (cyc_func_t)hbx_send_heartbeat;
	hdlr.cyh_level = CY_LOCK_LEVEL;
	hdlr.cyh_arg = NULL;
	when.cyt_when = 0;
	when.cyt_interval = hbx_nano_freq;

	mutex_enter(&cpu_lock);
	if (hbx_cyclic_run == 1) {
		mutex_exit(&cpu_lock);
		return;
	}
	ASSERT(hbx_nano_freq != 0);
	hbx_cyclic_run = 1;
	hbx_cyclic = cyclic_add(&hdlr, &when);
	mutex_exit(&cpu_lock);
}

/*
 * Remove the hbxdrv cyclic
 */
static void
hbx_cyclic_remove()
{
	mutex_enter(&cpu_lock);
	if (hbx_cyclic_run == 1) {
		hbx_cyclic_run = 0;
		cyclic_remove(hbx_cyclic);
		hbx_sent_time = 0;
	}
	mutex_exit(&cpu_lock);
}



/*
 * Change the state, with hbxdrv cyclic not running
 */
static void
hbx_change_state(hbx_device_t *hbd, enum hbx_emit_state state)
{
	hbx_cyclic_remove();
	hbd->state = state;
	if (hbd->mode & HBX_EMT_MODE_ON) {
		hbx_cyclic_add();
	}
}

static void
hbx_fastpath_ask(hbx_device_t *hbd)
{
	mblk_t *iomp;
	mblk_t *mp;
	struct iocblk *iocp;
	dl_unitdata_req_t *dl_udata;
	uchar_t *where;

	if ((hbd->mac_sap_len != sizeof (ushort_t)) ||
		(hbd->mac_len != ETHERADDRL)) {
		return;
	}

	mp = hbx_dlpi_comm(DL_UNITDATA_REQ,
		DL_UNITDATA_REQ_SIZE + sizeof (ushort_t) + ETHERADDRL);
	if (mp == NULL) {
		return;
	}

	dl_udata = (dl_unitdata_req_t *)mp->b_rptr;
	dl_udata->dl_dest_addr_offset = (t_uscalar_t)sizeof (dl_unitdata_req_t);
	dl_udata->dl_priority.dl_min = 0;
	dl_udata->dl_priority.dl_max = 0;
	dl_udata->dl_dest_addr_length =
		(t_uscalar_t)(sizeof (ushort_t) + ETHERADDRL);

	where = (uchar_t *)dl_udata + sizeof (dl_unitdata_req_t);
	bcopy((void *)&(hbd->mac_dst[0]), (void *)where, ETHERADDRL);
	where += ETHERADDRL;
	bcopy((void *)&(hbd->mac_sap[0]), (void *)where, sizeof (ushort_t));

	if ((iomp = mkiocb(DL_IOC_HDR_INFO)) == NULL) {
		freemsg(mp);
		return;
	}

	iomp->b_cont = mp;
	iocp = (struct iocblk *)iomp->b_rptr;
	iocp->ioc_count = msgdsize(iomp->b_cont);
	if (canputnext(WR(hbd->q))) {
		atomic_add_32(&hbx_await_fastpath_ack, 1);
		putnext(WR(hbd->q), iomp);
		return;
	}
}
