#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.cl	1.25	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# uts/intel/Makefile.cl
#
#	This makefile contains the common definitions for all clustering
#	implementation architecture independent modules.
#
CPPFLAGS += -I$(SRC)/common/cl
CPPFLAGS += -I$(SRC)/common/cl/interfaces/$(CLASS)
CPPFLAGS += -D_KERNEL_ORB

CPPFLAGS += $(CL_CPPFLAGS)
CCFLAGS += $(CL_CCFLAGS)
CCFLAGS += -features=no%except  $(CCEXTERNINST)
CCEXTERNINST = -instances=extern
#
# We should change all instances to explicit, but don't wish to risk
# changing what we currently ship which has been built extern.  This
# change to explicit is currently only for AMD64, but we should change
# this to be consistent across all builds.
#
$(POST_S9_BUILD)CCEXTERNINST_32 = -instances=extern
$(POST_S9_BUILD)CCEXTERNINST_64 = -instances=explicit
$(POST_S9_BUILD)CCEXTERNINST = $(CCEXTERNINST_$(CLASS))
CCFLAGS += -library=no%Crun
CCFLAGS += -library=no%Cstd

CCYFLAG  =-I

CLOBBERFILES  += $(LINTFILES) $(CL_TEMPLATES) $(CL_BINARY)

.PARALLEL: $(OBJECTS) $(LINTS) DUMMY

$(USR_CL_DIR):
	-$(INS.dir)

#
# Flexelint doesn't like -D_ASM
#
lint.targ := AS_DEFS=
