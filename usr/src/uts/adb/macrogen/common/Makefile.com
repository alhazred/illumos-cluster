#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.10	08/05/20 SMI" 
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# uts/adb/macrogen/common/Makefile.com
#
#

include $(SRC)/Makefile.master

#
#	No text domain in the kernel.
#
DTEXTDOM =

.KEEP_STATE:

MACROGEN=	macrogen
MGENPP=		mgenpp
OBJECTS=	parser.o stabs.o gen_adb.o pat.o mem.o
CMNDIR=		../common
CHECKHDRS=	gen.h mem.h pat.h pat_impl.h stabs.h
COPTFLAG =	-g

#       Default build targets.
#
all install: $(MACROGEN) $(MGENPP)

$(MACROGEN): $(OBJECTS)
	$(CC) $(CFLAGS) -o $(MACROGEN) $(OBJECTS) -lm -ldl $(DEMANGLE)

$(MGENPP): $(CMNDIR)/$(MGENPP).sh
	$(RM) $@
	cat $(CMNDIR)/$(MGENPP).sh > $@

%.o: $(CMNDIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

check: $(CHECK_FILES)
	@$(CAT) $(CHECK_FILES) /dev/null

lint:

clean:
	-$(RM) $(MACROGEN) $(MGENPP)

clobber: clean
	-$(RM) $(OBJECTS) $(CHECK_FILES)
