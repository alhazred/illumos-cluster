/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _MEM_H
#define	_MEM_H

#pragma ident	"@(#)mem.h	1.7	08/05/20 SMI"

#include <sys/types.h>
#include <malloc.h>

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * This is the header corresponding to a set of routines which
 * interpose on malloc, realloc, and calloc.  These routines call
 * an error handler if they fail, allowing one to write code which
 * does not concern itself with out-of-memory conditions.
 *
 * The actual allocation routines have the same signatures as usual,
 * specified in malloc.h.
 *
 * This is the type for an error handler and a routine to get and set it.
 * The value passed to the handler is the number of bytes requested.  If
 * the handler returns 0, these routines return NULL (just as their normal
 * counterparts would); otherwise they attempt the allocation again.
 */
typedef int (*alloc_err_func_t)(size_t);
alloc_err_func_t set_alloc_err_func(alloc_err_func_t);

/*
 * If this package can't find malloc and realloc, put the path to the
 * dynamic object in this variable.
 */
extern char *alloc_func_object;

#ifdef	__cplusplus
}
#endif

#endif /* _MEM_H */
