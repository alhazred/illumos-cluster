/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_PAT_IMPL_H
#define	_PAT_IMPL_H

#pragma ident	"@(#)pat_impl.h	1.6	08/05/20 SMI"

#include "pat.h"

/*
 * A pattern gets compiled into a tree of integers, called cells; if a cell
 * is > 0, it is a character to be matched.  Otherwise it is an operator.
 * Some operators store offsets, in bytes, to another part of the pattern.
 * Here are the operator codes.
 */
#define	LEFT_PAREN  -1
#define	RIGHT_PAREN -2
#define	MATCH_ANY   -3
#define	MATCH_ONE   -4
#define	NOT	    -5	/* next cells contain offset to end, min, max */
#define	OR	    -6	/* next cells contain offsets to alternative 2, end */
#define	END	    -7

#endif	/* _PAT_IMPL_H */
