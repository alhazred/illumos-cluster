/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _COMMON_GEN_H
#define	_COMMON_GEN_H

#pragma ident	"@(#)gen.h	1.9	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/* A dimension_t tells the number of elements in one dimension of an array. */
typedef struct dimension_ dimension_t;
struct dimension_ {
	int		 num_elements;	/* # elements in this array dimension */
	dimension_t	*next;		/* info about further dimensions */
	dimension_t	*prev;		/* in a doubly-linked list */
};

extern void gen_struct_begin(
	char *,		/* struct name */
	char *,		/* prefix for members */
	char *,		/* file (macro name) to put the macro in */
	int		/* size of struct, in bytes */
);

extern void gen_struct_member(
	char *,		/* label */
	int,		/* offset, in bits */
	dimension_t *,	/* array info (most major dimension first) */
	int,		/* size of element, in bits */
	char *fmt	/* format */
);

extern void gen_struct_end(
);

#ifdef	__cplusplus
}
#endif

#endif	/* _COMMON_GEN_H */
