/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SYS_STABS_H
#define	_SYS_STABS_H

#pragma ident	"@(#)stabs.h	1.11	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <setjmp.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "pat.h"

#define	MAXLINE	10240

/* Hash tables for stabs info ???. */

#define	BUCKETS	128
#define	HASH(NUM)	((int)(NUM & (BUCKETS - 1)))

struct	tdesc	*hash_table[BUCKETS];
struct	tdesc	*name_table[BUCKETS];

/* Structures for holding user's specification for how to display a struct. */

struct node {	/* per struct */
	char		 *struct_name;	/* struct name */
	char		 *prefix;	/* member prefix; remove from names */
	char		 *name;		/* name to use */
	struct child	 *child;	/* NULL-term list of child nodes */
	struct child	**last_child_link; /* ptr to last link in child list */
};

struct child {	/* per struct member */
	pat_handle_t	 pattern;	/* spec for members to apply to */
	char		*format;	/* format to display it in */
	char		*label;		/* what to call it */
	struct child	*next;		/* next child in list */
};

/* Data structures to contain info derived from the stabs. */

enum type {
	INTRINSIC,
	POINTER,
	REFERENCE,
	ARRAY,
	FUNCTION,
	STRUCT,
	CLASS,
	TEMPLATE,
	UNION,
	ENUM,
	FORWARD,
	TYPEOF,
	VOLATILE,
	CONST,
	CLASSDECL
};

struct tdesc {
	char	*name;
	char	*rawname; /* The non-demangled name */
	struct	tdesc *next;
	enum	type type;
	int	size;
	union {
		struct	tdesc *tdesc;	/* *, f , to */
		struct	ardef *ardef;	/* ar */
		struct	mlist *members;	/* s, u */
		struct  elist *emem; /* e */
	} data;
	int	id;
	struct tdesc *hash;
};

struct elist {
	char	*name;
	int	number;
	struct elist *next;
};

struct element {
	struct tdesc *index_type;
	int	range_start;
	int	range_end;
};

struct ardef {
	struct tdesc	*contents;
	struct element	*indices;
};

struct mlist {	/* describes a struct/union member */
	int	offset;
	int	size;
	char	*name;
	struct	mlist *next;
	struct	tdesc *fdesc;		/* s, u */
	int	flags;	/* inherited */
};

#define	ALLOC(t)		((t *)malloc(sizeof (t)))

struct	tdesc *lookupname();

char *outfile;

#ifdef __cplusplus
}
#endif

#endif	/* _SYS_STABS_H */
