/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * For information on debugging this file, see adb/files/Makefile.common
 */

#pragma	ident	"@(#)parser.c	1.12	08/05/20 SMI"

#include <unistd.h>
#include "stabs.h"
#include <strings.h>
#include <demangle.h>

void reset_func(int n);
void parseline(char *cp);
void template(char *cp, struct tdesc **rtdp, char *w, int instance);
void tagadd(char *w, int h, struct tdesc *tdp);
int compute_sum(char *w);

#define	reset() reset_func(__LINE__)

#define	IDSCALE 10000

jmp_buf	resetbuf;

char	*whitesp(), *name(), *id(), *decl(), *number(), *offsize();
char	*tdefdecl(), *intrinsic(), *arraydef(), *uptodelim();
void	addhash();
struct tdesc *lookup(int h);
char	*tagdecl(), *soudef(), *enumdef();

static int line_number = 0;
static int debug_line  = 0;
static char linebuf[MAXLINE];

char *longname = NULL; /* Full demangled name */
char *rawname = NULL; /* Undemangled name */


static int cplpl = 0;

int ptrsize = -1;
int parsing = 0;


/* Report unexpected syntax in stabs. */
void
expected(
	char *who,	/* what function, or part thereof, is reporting */
	char *what,	/* what was expected */
	char *where)	/* where we were in the line of input */
{
	fprintf(stderr, "%s, input line %d: expecting \"%s\" at \"%s\"\n",
		who, line_number, what, where);
	parsing = 0;
	reset();  /* not sure it makes any sense to do this, but ... */
}

/*
 * Find the first position with \\, or return NULL
 */
char *
get_slashes(char *cp)
{
	char *p = cp;
	while ((p = index(p, '\\')) != NULL) {
		if (p[1] == '\\' && p[2] == '"') {
			return (p);
		}
		p++;
	}
	return (NULL);
}

/* Read a line from stdin into linebuf and increment line_number. */
char *
get_line()
{
	int i;
	char *retp = linebuf;
	char *cp = fgets(linebuf, MAXLINE, stdin);
	char *cont, *quote;
	if (cp == NULL) {
		return (NULL);
	}
	while (1) {
		line_number++;

		/*
		 * For debugging, you can set debug_line to a line to
		 * stop on.
		 */
		if (line_number == debug_line) {
			fprintf(stderr, "Hit debug line number %d\n",
				line_number);
			for (;;)
				sleep(1);
		}
		cont = get_slashes(cp);
		if (cont == NULL) break;
		fgets(cont, MAXLINE - (cont-cp), stdin);
		if (cont == NULL) {
			expected("get_line", "continuation", retp);
		}
		linebuf[MAXLINE-1] = '\0';
		if (strlen(linebuf) == MAXLINE-1) {
			fprintf(stderr, "Buffer overflow on %s\n", linebuf);
			exit(-1);
		}
		quote = index(cont, '"');
		if (quote == NULL) {
			expected("get_line", "\"", cont);
		}
		bcopy(quote+1, cont, strlen(quote+1));
	}
	return (retp);
}

/* Get the continuation of the current input line. */
char *
get_continuation()
{
	char *cp = get_line();
	if (!cp) {
		fprintf(stderr,
			"expecting continuation line, but end of input\n");
		exit(1);
	}

	/* Skip to the quoted stuff. */
	while (*cp++ != '"')
		;
	return (cp);
}

FILE *list = NULL;

void
parse_input()
{
	char *cp;
	int i = 0;

	if (outfile) {
		list = fopen(outfile, "w");
		if (list == NULL) {
			perror(outfile);
			exit(-1);
		}
	}

	while (i++ < BUCKETS) {
		hash_table[i] = NULL;
		name_table[i] = NULL;
	}

	/*
	 * get a line at a time from the .s stabs file and parse.
	 */
	while (cp = get_line())
		parseline(cp);

	if (list) {
		fclose(list);
	}
}

char *filename = "";

/*
 * Parse each line of the .s file (stabs entry) gather meaningful information
 * like name of type, size, offsets of fields etc.
 */
void
parseline(cp)
	char *cp;
{
	struct tdesc *tdp;
	char c, *w;
	int h, tagdef;
	int debug;
	char *quote;
	int type;
	int literal = 0; /* Flag indicating literal parsing */

	cplpl = 0;
	parsing = 0;

	/*
	 * setup for reset()
	 */
	if (setjmp(resetbuf))
		return;

	/*
	 * Look for lines of the form
	 *	.stabs	"str",n,n,n,n
	 * The part in '"' is then parsed.
	 */
	cp = whitesp(cp);
#define	STLEN	6
	if (strncmp(cp, ".stabs", STLEN) != 0)
		reset();
	cp += STLEN;
#undef STLEN
	parsing = 1;
	cp = whitesp(cp);
	if (*cp++ != '"')
		reset();

	quote = index(cp, '"');
	if (quote == NULL || quote[1] != ',') {
		expected("parseline", "closing quote", cp);
	}
	type = strtol(quote+2, NULL, 0);
	if (type == 130 || type == 132) { /* N_BINCL || N_SOL */
		/* filename */
		filename = (char *)malloc(quote-cp+1);
		strncpy(filename, cp, quote-cp);
		filename[quote-cp] = '\0';
		return;
	} else if (type == 198) { /* N_ISYM */
		/* Fall-through for type 198 */
	} else if (type == 200) { /* N_ESYM */
		/* Fall-through for type 200 */
	} else if (type == 128) { /* N_LSYM */
		/* Fall-through for type 128 */
	} else if (type == 32) { /* N_GSYM */
		/* Fall-through for global symbol (YTc) or G */
	} else if (type == 36) { /* N_FUN */
		/* Fall-through extern function */
	} else if (type == 38) { /* N_STSYM */
		/* data-segment file scope variable */
	} else if (type == 56) { /* N_OBJ */
		/* object file */
		return;
	} else if (type == 60) { /* N_OPT */
		/* Debugger options */
		return;
	} else if (type == 98) { /* N_ENDM */
		/* last stab for module */
		return;
	} else if (type == 100) { /* N_SO */
		/* source file */
		return;
	} else if (type == 160) { /* N_PSYM */
		/* fall-through for parameter */
		return;
	} else if (type == 162) { /* N_EINCL */
		/* end of include */
		return;
	} else if (type == 196) { /* N_USING */
		return;
	} else {
		fprintf(stderr, "Unexpected stabs code 0x%x, line %d\n", type,
			line_number);
		return;
	}

	/*
	 * name:type		variable (ignored)
	 * name:ttype		typedef
	 * name:Ttype		struct tag define
	 */
	/* Skip strange prefix */
	if (cp[0] == '$') {
		cp++;
		while (isalnum(*cp) || *cp == '_' || *cp == '$') {
			cp++;
		}
		if (cp[0] == '.') {
			cp++;
		} else {
			expected("parseline", "$init token", cp);
		}
	}

	cp = name(cp, &w, NULL);

	switch (c = *cp++) {
	case 'p': /* parameter */
		tagdef = 1;
		break;
	case 't': /* type */
		tagdef = 0;
		break;
	case '(': /* nothing */
		cp--;
		tagdef = 1;
		break;
	case 'T': /* struct, union, enum */
		tagdef = 1;
		break;
	case 'L': /* typedef in class */
		tagdef = 0;
		if (cp[0] == 'T' && cp[1] == 'c') {
			/* template line number declaration */
			return;
		} else if (cp[0] == 'T' && cp[1] == 'm') {
			/* template function line number declaration */
			return;
		} else {
			expected("parseline", "LTc/LTm", cp - 1);
		}
		break;
	case 'Y':
		if (cp[0] == 'y') {
			/* typedef in a class */
			tagdef = 0;
			cp++;
			break;
		} else if (cp[0] == 'T' && cp[1] == 'c') {
			/* template parameter declaration */
			cp += 2;
			template(cp, &tdp, w, 0);
			return;
		} else if (cp[0] == 'I' && cp[1] == 'c') {
			/* template instantiation */
			cp += 2;
			template(cp, &tdp, w, 1);
			return;
		} else if (cp[0] == 'I' && cp[1] == 'f' && cp[2] == 'T') {
			/* template instantiation */
			cp += 3;
			template(cp, &tdp, w, 0);
			return;
		} else if (cp[0] == 'T' && cp[1] == 's') {
			/* struct in a template */
			cp += 2;
			template(cp, &tdp, w, 0);
			return;
		} else {
			expected("parseline", "Yy/YTc/YIc/YIfT", cp - 1);
		}
		break;
	case 'S':
		/*
		 * Constant def.  Need to process because it may define
		 * something in turn
		 */
		if (type != 38) {
			expected("parseline", "type 38", cp-1);
		}
		tagdef = 2;
		break;
	case 'P':
	case 'f':
		if (type != 36) {
			expected("parseline", "type 36", cp-1);
		}
		break;
	case 'G':
		if (type != 32) {
			expected("parseline", "type 32", cp-1);
		}
		tagdef = 0;
		break;
	case 'U': /* Class declaration */
		cp = id(cp, &h);
		if (cp[0] != '"') {
			expected("parseline", "U eol quote", cp);
		}
		tdp = ALLOC(struct tdesc);
		tdp->type = CLASSDECL;
		tdp->name = w;
		tdp->rawname = rawname ? strdup(rawname) : "";
		tagadd(w, h, tdp);
		return;
	case 'E': /* External data */
		tagdef = 0;
		break;
	case 'l': /* Literal */
		tagdef = 0;
		literal = 1;
		return;
		break;
	default:
		fprintf(stderr, "Unexpected symbol descriptor '%c'\n", c);
		reset();
	}

	/*
	 * The type id and definition follow.
	 */
	if (type == 36 || type == 38) {
		do {
			cp = tdefdecl(cp, &tdp, w);
			if (cp[0] == '-') {
			    /* New compiler inserts -1, so skip - ? */
			    cp++;
			}
			if (isdigit(*cp)) {
			    /* New compiler inserts numbers? */
			    int dummy;
			    cp = number(cp, &dummy);
			}
			if (isalpha(cp[0]) || cp[0] == '_') {
			    cp = name(cp, &w, NULL);
			}
			if (cp[0] == ':') {
			    cp++;
			}
		} while (*cp++ == ';');
		if (cp[-1] != '"') {
			expected("parseline", "end quote", cp);
		}
		return;
	}

	cp = id(cp, &h);
	if (*cp++ != '=') {
		if (lookup(h) && (cp[-1] == '"' || literal)) {
			/* Already defined */
			return;
		}
		expected("parseline", "=", cp - 1);
	}

	if (tagdef == 1) {
		cp = tagdecl(cp, &tdp, h, w);
	} else {
		cp = tdefdecl(cp, &tdp, w);
		tagadd(w, h, tdp);
	}
	if (cp[0] != '"' && !literal) {
		expected("parseline", "eol quote", cp);
	}
}

/*
 * Check if we have this node in the hash table already
 */
struct tdesc *
lookup(int h)
{
	int hash = HASH(h);
	struct tdesc *tdp = hash_table[hash];

	while (tdp != NULL) {
		if (tdp->id == h)
			return (tdp);
		tdp = tdp->hash;
	}
	return (NULL);
}

char *
whitesp(cp)
	char *cp;
{
	char *orig, c;

	orig = cp;
	for (c = *cp++; isspace(c); c = *cp++)
		;
	if (--cp == orig)
		reset();
	return (cp);
}

/* Parse a name */
char *
name(cp, w, parent)
	char *cp, **w;
	struct tdesc *parent; /* Parent class, for diferential demangling */
{
	char *new, *orig, c;
	int len;
#define	NAME 500
	char buf[NAME];
	int err;

	free(rawname);
	rawname = NULL;

	orig = cp;
	c = *cp++;
	if (cplpl) {
		if (c == 'K') {
			fprintf(stderr, "Warning: virtual inheritance ***\n");
		}
		if (c != 'A' && c != 'B' && c != 'C' && c != 'S' && c != 'K') {
			expected("name", "Cname", cp-1);
		}
		if (c == 'S') {
			while (isdigit(c = *cp++)) {}
			orig = cp-1;
		} else {
			orig++;
			/* Remove extra c++ characters */
			if (cp[0] == 'd' && cp[1] == 'A' && isupper(cp[2])) {
			    /* e.g. AdAD...; step over d */
			    orig++;
			    cp++;
			}
			if (isupper(cp[0]) && isupper(cp[1])) {
			    /* e.g. FG...; step over FG */
			    orig++;
			    orig++;
			    cp++;
			    cp++;
			}
			c = *cp++;
		}
	}
	if (c == ':') {
		*w = NULL;
		free(longname);
		longname = NULL;
	} else if (isalpha(c) || c == '_' || c == '.') {
		char *cp2, *newstr;
		for (c = *cp++; isalnum(c) || c == ' ' || c == '_' ||
		    c == '-' || c == '.' || c == '$'; c = *cp++)
			;
		cp2 = cp; /* Pointer to first char after : or ( */
		len = cp - orig;
		newstr = (char *)malloc(len);
		strncpy(newstr, orig, len-1);
		newstr[len-1] = '\0';
		if (c == '(') { /* Differential demangling id */
		    int h;
		    cp2 = id(cp-1, &h);
		    parent = lookup(h);
		    c = *cp2++;
		}
		if (parent != NULL) {
		    /* Do the differential demangling */
		    char buf[1024];
		    char type;
		    type = newstr[0];
		    /* Copy parent string */
		    strcpy(buf, parent->rawname);
		    buf[strlen(buf)-1] = '\0';
		    /* Replace type */
		    buf[3] = type;
		    /* Append new string */
		    strcat(buf, newstr+1);
		    /* Add suffix */
		    strcat(buf, "_");
		    /* Replace newstr string */
		    free(newstr);
		    newstr = strdup(buf);
		}
		if (c == '"' || c == ';') {
		    cp2--; /* step back to quote */
		} else if (c != ':') {
			fprintf(stderr, "Looking for name at %s\n", orig);
			reset();
		}

		*w = newstr;
		rawname = strdup(newstr);

		err = cplus_demangle(*w, buf, NAME);
		if (err == 0) {
			char *lastbit = rindex(buf, ':');
			int len = strlen(buf);
			if (buf[len-1] == '_') {
			    buf[len-1] = '\0';
			}
			free(longname);
			free(*w);
			if (lastbit) {
				*w = strdup(lastbit+1);
				longname = strdup(buf);
			} else {
				*w = strdup(buf);
				longname = NULL;
			}
		} else {
			free(longname);
			longname = NULL;
		}
		cp = cp2;
	} else if (c == '.' && cp[0] == '.' && cp[1] == '.' && cp[2] == ':') {
		/* Special case for ... */
		longname = NULL;
		*w = "...";
		cp = cp+3;
	} else if (c == '/' || c == '"') {
		reset();
	} else {
		reset();
	}

	return (cp);
}

/* cp points to "stuff;" - return pointer after ; */
char *
uptodelim(cp, w)
	char *cp, **w;
{
	char *new, *orig, c;
	int len;

	orig = cp;
	c = *cp++;
	if (c == ';')
		*w = NULL;
	else {
		for (; c != ';'; c = *cp++) {
			if (c == '\0' || c == '"') {
				expected("uptodelim", ";", orig);
			}
		}
		len = cp - orig;
		new = (char *)malloc(len);
		while (orig < cp - 1)
			*new++ = *orig++;
		*new = '\0';
		*w = new - (len - 1);
	}

	return (cp);
}

char *
number(cp, n)
	char *cp;
	long *n;
{
	char *next;

	*n = strtol(cp, &next, 10);
	if (next == cp)
		expected("number", "<number>", cp);
	return (next);
}

char *
id(cp, h)
	char *cp;
	int *h;
{
	long n1, n2;

	if (*cp++ != '(')
		expected("id", "(", cp - 1);
	cp = number(cp, &n1);
	if (*cp++ != ',')
		expected("id", ",", cp - 1);
	cp = number(cp, &n2);
	if (*cp++ != ')')
		expected("id", ")", cp - 1);
	*h = n1 * IDSCALE + n2;
	return (cp);
}

/*
 * Handle a template instantiation.
 * _constructor:YIc(id,id);@;Tparam:(typeid);Tparam:(typeid);@;g;stuff..
 * where stuff is what is normally after the tagdecl =
 * Coming in, we point to the id
 */
void
template(cp, rtdp, w, instance)
	char *cp;
	struct tdesc **rtdp;
	char *w;
	int instance;

{
	char c;
	int h;
	cplpl = 1;
	cp = id(cp, &h);

	if (*rtdp = lookup(h)) {
		/*
		 * Same id used for typedef foo template<type>
		 * and instantiation of template<type>
		 */
	} else {
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->rawname = rawname ? strdup(rawname) : "";
		addhash(*rtdp, h);
	}
	(*rtdp)->data.members = NULL;

	(*rtdp)->name = w;
	if (*cp++ != ';')
		expected("template", ";", cp - 1);
	if (instance) {
		if (*cp++ != '@')
			expected("template", "@", cp - 1);
		if (*cp++ != ';')
			expected("template", ";", cp - 1);
	}
	cplpl = 0;
	while (1) {
		struct tdesc *tdp;
		if (*cp == '@') {
			cp++;
			break;
		}
		cp = name(cp + 1, &w, NULL); /* Parameter name */
		if (cp[0] == 't' && cp[1] == 'Y' && cp[2] == 'C') {
			/* Some template tYC thing */
			cp += 3;
			cp = id(cp, &h);
		} else if (cp[0] == '(') { /* id */
			cp = id(cp, &h);
			tdp = lookup(h);
			if (tdp == NULL) { /* not in hash list */
				if (*cp++ != '=') {
					expected("soudef", "=", cp - 1);
				}
				cp = tdefdecl(cp, &tdp, "");
				addhash(tdp, h);
			}
		} else if (isdigit(cp[0])) {
			int dummy;
			cp = number(cp, &dummy);
		} else if (isalnum(cp[0]) || cp[0] == '_') {
		    cp = name(cp, &w, NULL);
		} else {
			expected("template instantiation", "argument info",
			    cp - 1);
		}

		if (*cp++ != ';') {
			expected("template", ";", cp - 1);
		}
	}
	if (instance == 0) {
		return;
	}
	if (*cp++ != ';')
		expected("template", ";", cp - 1);
	if (*cp++ != 'g')
		expected("template", "g", cp - 1);
	if (*cp++ != ';')
		expected("template", ";", cp - 1);
	cplpl = 1;

	cp = soudef(cp, TEMPLATE, rtdp);

	cplpl = 0;
}

/* Add tdp to the hash table under id h */
void
tagadd(char *w, int h, struct tdesc *tdp)
{
	struct tdesc *otdp, *hash;

	tdp->name = w;
	tdp->id = h;
	if (!(otdp = lookup(h)))
		addhash(tdp, h);
	else if (otdp != tdp && strcmp(otdp->name, tdp->name) != 0) {
		fprintf(stderr, "duplicate entry\n");
		fprintf(stderr, "old: %s %d %d %d\n",
			otdp->name ? otdp->name : "NULL",
			otdp->type, otdp->id / IDSCALE, otdp->id % IDSCALE);
		fprintf(stderr, "new: %s %d %d %d\n",
			tdp->name ? tdp->name : "NULL",
			tdp->type, tdp->id / IDSCALE, tdp->id % IDSCALE);
	}
}

/*
 * Handle a class/struct/union tagdecl, i.e. name:(id)=stuff.
 * Come in after =
 */
char *
tagdecl(cp, rtdp, h, w)
	char *cp;
	struct tdesc **rtdp;
	int h;
	char *w;
{
	if (*rtdp = lookup(h)) {
		if ((*rtdp)->type != FORWARD && (*rtdp)->type != CLASSDECL)
			if (*cp == 'e') {
				/* C++ likes doing enums multiple times */
			} else {
				fprintf(stderr,
					"found but not forward: %s \n", cp);
			}
	} else {
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->name = w;
		(*rtdp)->rawname = rawname ? strdup(rawname) : "";
		addhash(*rtdp, h);
	}
	(*rtdp)->data.members = NULL;

	cplpl = 0;
	if (*cp == 'Y') {
		cplpl = 1;
		cp++;
	}
	switch (*cp++) {
	case 'c':
	case 'C':
		if (w && longname && list) {
			fprintf(list, "%s %s %s\n", filename, w, longname);
		} else if (w && list) {
			fprintf(list, "%s %s %s\n", filename, w, w);
		}
		cp = soudef(cp, CLASS, rtdp);
		break;
	case 's':
	case 'S':
		if (w && longname && list) {
			fprintf(list, "%s %s %s\n", filename, w, longname);
		} else if (w && list) {
			fprintf(list, "%s %s %s\n", filename, w, w);
		}
		cp = soudef(cp, STRUCT, rtdp);
		break;
	case 'u':
	case 'U':
		cp = soudef(cp, UNION, rtdp);
		break;
	case 'e':
		cp = enumdef(cp, rtdp);
		break;
	case 'n': /* Namespace */
		if (*cp++ != '0') {
			expected("Yn", "0", cp-1);
		}
		cplpl = 0;
		cp = name(cp, &w, NULL);
		if (*cp++ != ';') {
			expected("Yn", ";", cp-1);
		}
		break;
	default:
		expected("tagdecl", "<tag type s/u/e>", cp - 1);
	}
	cplpl = 0;
	return (cp);
}

/* Handle a typedef name:(id)=another type, come in after = */
/* Creates a tdp and puts it in rtdp, but doesn't put it in hash table */
char *
tdefdecl(cp, rtdp, inw)
	char *cp;
	struct tdesc **rtdp;
	char *inw;
{
	int oldcplpl;
	struct tdesc *tdp = 0, *ntdp = 0;
	char *w = 0;
	int c, h;
	int dummy;

	/* Type codes */
	switch (*cp) {
	case 'b': /* integer */
		c = *++cp;
		if (c != 's' && c != 'u')
			expected("tdefdecl/b", "[su]", cp - 1);
		c = *++cp;
		if (c == 'v' || c == 'c' || c == 'b') {
			cp++;
		}
		cp = intrinsic(cp, rtdp);
		/* Assume sizeof(ptr) == sizeof(long) */
		if (strcmp(inw, "long") == 0) {
			ptrsize = (*rtdp)->size;
		}
		cp = number(cp, &dummy);
		if (*cp++ != ';') {
			expected("tdefdecl", ";", cp-1);
		}
		cp = number(cp, &dummy);
		if (*cp == '"') {
			/* Missing closing ; is okay? */
			break;
		} else if (*cp++ != ';') {
			expected("tdefdecl", ";", cp-1);
		}
		break;
	case 'R': /* fp */
		cp += 3;
		cp = intrinsic(cp, rtdp);
		break;
	case '(': /* equiv to another type */
		cp = id(cp, &h);
		ntdp = lookup(h);
		if (ntdp == NULL || *cp == '=') {
			/* if that type isn't defined yet */
			if (*cp++ != '=') { /* better be defining it now */
				fprintf(stderr, "Missing id %d\n", h);
				expected("tdefdecl/'('", "=", cp - 1);
			}
			cp = tdefdecl(cp, &ntdp, "");
			addhash(ntdp, h); /* for *(x,y) types */
		}
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->name = inw;
		(*rtdp)->rawname = rawname ? strdup(rawname) : "";
		(*rtdp)->type = TYPEOF;
		(*rtdp)->size = 0;
		(*rtdp)->data.tdesc = ntdp;
		if (*cp == '=') {
		    expected("tdefdecl/'(id)'", "no =", cp - 1);
		}
		if (strncmp(cp, "__defarg", 8) == 0) {
			while (*cp != ';' && *cp != '\0' && *cp != '"') {
				cp++;
			}
		}
		break;
	case '*':
		cp = tdefdecl(cp + 1, &ntdp, "");
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->type = POINTER;
		if (ptrsize <= 0) {
			fprintf(stderr, "ptrsize undefined\n");
		}
		(*rtdp)->size = ptrsize;
		(*rtdp)->name = "pointer";
		(*rtdp)->name = inw;
		(*rtdp)->rawname = "";
		(*rtdp)->data.tdesc = ntdp;
		break;
	case '&':
		cp = tdefdecl(cp + 1, &ntdp, "");
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->type = REFERENCE;
		if (ptrsize <= 0) {
			fprintf(stderr, "ptrsize undefined\n");
		}
		(*rtdp)->size = ptrsize;
		(*rtdp)->name = "pointer";
		(*rtdp)->rawname = "";
		(*rtdp)->name = inw;
		(*rtdp)->data.tdesc = ntdp;
		break;
	case 'f':
		cp = tdefdecl(cp + 1, &ntdp, "");
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->type = FUNCTION;
		if (ptrsize <= 0) {
			fprintf(stderr, "ptrsize undefined\n");
		}
		(*rtdp)->size = ptrsize;
		(*rtdp)->name = "";
		(*rtdp)->rawname = "";
		(*rtdp)->name = inw;
		(*rtdp)->data.tdesc = ntdp;
		break;
	case 'a':
		cp++;
		if (*cp++ != 'r')
			expected("tdefdecl/a", "r", cp - 1);
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->type = ARRAY;
		(*rtdp)->name = "array";
		(*rtdp)->name = inw;
		(*rtdp)->rawname = "";
		cp = arraydef(cp, rtdp);
		break;
	case 'x':
		c = *++cp;
		if (c != 's' && c != 'u' && c != 'e')
			expected("tdefdecl/x", "[sue]", cp - 1);
		oldcplpl = cplpl;
		cplpl = 0;
		cp = name(cp + 1, &w, NULL);
		cplpl = oldcplpl;
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->type = FORWARD;
		(*rtdp)->name = w;
		(*rtdp)->rawname = "";
		break;
	case 'B': /* volatile */
		cp = tdefdecl(cp + 1, &ntdp, "");
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->type = VOLATILE;
		(*rtdp)->size = 0;
		(*rtdp)->name = "volatile";
		(*rtdp)->name = inw;
		(*rtdp)->rawname = "";
		(*rtdp)->data.tdesc = ntdp;
		break;
	case 'k': /* const */
		cp = tdefdecl(cp + 1, &ntdp, "");
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->type = CONST;
		(*rtdp)->size = 0;
		(*rtdp)->name = "const";
		(*rtdp)->name = inw;
		(*rtdp)->rawname = "";
		(*rtdp)->data.tdesc = ntdp;
		break;
	case 'g': /* Function with prototype info */
		cp = tdefdecl(cp + 1, &ntdp, "");
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->type = FUNCTION;
		if (ptrsize <= 0) {
			fprintf(stderr, "ptrsize undefined\n");
		}
		(*rtdp)->size = ptrsize;
		(*rtdp)->name = "";
		(*rtdp)->name = inw;
		(*rtdp)->rawname = "";
		(*rtdp)->data.tdesc = ntdp;
		while (cp[0] != '#') {
		    /* Process arguments up to # terminator */
		    cp = tdefdecl(cp, &ntdp, "");
		}
		cp++;
		break;

	case 'Y': /* C++ */
		cp++;
		c = *cp++;
		*rtdp = ALLOC(struct tdesc);
		(*rtdp)->type = CLASSDECL;
		(*rtdp)->name = "c++ class";
		(*rtdp)->rawname = "";
		(*rtdp)->size = 0;
		if (c != 'M' && c != 'D') {
			expected("tdefdecl", "YM or YD", cp - 1);
		}
		while (1) {
		    if (*cp == '#') {
			cp++;
			break;
		    }
		    /* Get type */
		    cp = id(cp, &h);
		    tdp = lookup(h);
		    if (tdp == NULL) { /* not in hash list */
			    if (*cp++ != '=') {
				    expected("soudef", "=", cp - 1);
			    }
			    cp = tdefdecl(cp, &tdp, "");
			    addhash(tdp, h);
		    }
		}
		return (cp);

	default:
		fprintf(stderr, "Unexpected type code '%c'\n", *cp);
		expected("tdefdecl", "<type code>", cp);
	}
	return (cp);
}

char *
intrinsic(cp, rtdp)
	char *cp;
	struct tdesc **rtdp;
{
	struct tdesc *tdp;
	long size;

	cp = number(cp, &size);
	tdp = ALLOC(struct tdesc);
	tdp->type = INTRINSIC;
	tdp->size = size;
	tdp->name = NULL;
	tdp->rawname = "";
	*rtdp = tdp;
	if (*cp == '"') {
		return (cp);
	} else if (*cp == ';') {
		return (cp+1);
	} else {
		expected("intrinsic", ";", cp);
	}
	return (cp);
}

/*
 * Return a func * tdesc for the vtbl
 */
struct tdesc *
funcptr()
{
	static struct tdesc *vtdp = NULL, *itdp = NULL, *ftdp = NULL;
	if (vtdp) {
		return (vtdp);
	}
	vtdp = ALLOC(struct tdesc);
	vtdp->type = POINTER;
	vtdp->size = ptrsize;
	vtdp->name = "vtbl";
	vtdp->rawname = "";
	vtdp->id = 999999;

	itdp = ALLOC(struct tdesc);
	itdp->type = FUNCTION;
	itdp->size = ptrsize;
	itdp->name = "";
	itdp->rawname = "";
	itdp->id = 999998;

	ftdp = ALLOC(struct tdesc);
	ftdp->type = INTRINSIC;
	ftdp->size = 0;
	ftdp->name = "void";
	ftdp->rawname = "";
	ftdp->id = 999997;

	itdp->data.tdesc = ftdp;
	vtdp->data.tdesc = itdp;
	return (vtdp);
}


#define	PRINT(x) ((x)?(x):"<nil>")

/* class stab */
/* See "Stabs for classes, structs, and non-anonymous unions" in the doc */
char *
soudef(cp, type, rtdp)
	char *cp;
	enum type type;
	struct tdesc **rtdp;
{
	struct mlist *mlp, **prev;
	char *w;
	int h, i = 0;
	long size, offset;
	struct tdesc *tdp;
	char *cname, *cinher, *cinher_old, *tmp;

	cp = number(cp, &size);
	(*rtdp)->size = size;
	(*rtdp)->type = type; /* s or u */
	/*
	 * An '@' here indicates a bitmask follows.   This is so the
	 * compiler can pass information to debuggers about how structures
	 * are passed in the v9 world.  We don't need this information
	 * so we skip over it.
	 */
	if (cp[0] == '@')
		cp += 3;

	prev = &((*rtdp)->data.members);

	if (cplpl) {
		/* Process the classname name */
		cp = uptodelim(cp, &cname);
		/* Process the inheritance thing */
		cp = uptodelim(cp, &cinher);
		cinher_old = cinher;
		while (cinher && *cinher) {
			mlp = ALLOC(struct mlist);
			mlp->flags = 1;
			*prev = mlp;
			cinher++; /* skip public/private */
			cinher = number(cinher, &offset);
			mlp->offset = offset*8;
			cinher = id(cinher, &h);
			/*
			 * find the tdesc struct in the hash table for this type
			 * and stick a ptr in here
			 */
			tdp = lookup(h);
			if (tdp == NULL) { /* not in hash list */
				if (*cinher++ != '=') {
					fprintf(stderr, "Undefined %d\n", h);
					expected("soudef", "=id", cinher - 1);
				}
				cinher = tdefdecl(cinher, &tdp, "");
				addhash(tdp, h);
			}

			mlp->name = tdp->name;
			mlp->size = tdp->size*8;
			mlp->fdesc = tdp;
			/* cp is now pointing to next field */
			prev = &mlp->next;
		}
	}
	/* now fill up the fields */
	while ((*cp != '"') && (*cp != ';')) { /* signifies end of fields */
		mlp = ALLOC(struct mlist);
		*prev = mlp;
		mlp->flags = 0;
		cp = name(cp, &w, *rtdp);
		mlp->name = w;
		cp = id(cp, &h);
		/*
		 * find the tdesc struct in the hash table for this type
		 * and stick a ptr in here
		 */
		tdp = lookup(h);
		if (tdp == NULL || cp[0] == '=') { /* not in hash list */
			/* Unclear why we'd hit = if tdp != NULL, but we do. */
			if (*cp++ != '=') {
				fprintf(stderr, "Undefined %d\n", h);
				expected("soudef", "=ID", cp - 1);
			}
			cp = tdefdecl(cp, &tdp, "");
			addhash(tdp, h);
		}

		mlp->fdesc = tdp;
		cp = offsize(cp, mlp);
		/* cp is now pointing to next field */
		prev = &mlp->next;
		/* could be a continuation */
		if (*cp == '\\')
			cp = get_continuation();
	}
	if (cplpl) {
		cp++;
		/*
		 * Additional fields
		 * 0 = methods, 1 = static variables, 2 = friends,
		 * 3 = inheritance,
		 * 4 = union definition
		 * 6 = virtual inheritance vtbl (not handled correctly)
		 */
		for (i = 0; i < 8; i++) {
			if (cp[0] == '"' && i == 7) {
			    /* Older compilers don't have the last one */
			    break;
			}
			cp = uptodelim(cp, &tmp);
			if (tmp && i == 5) {
				fprintf(stderr, "Unexpected field %s %d %s\n",\
					PRINT(cname), i, PRINT(tmp));
			}
			if (i == 3 && tmp) {
				/* Inherited vtbl */
				long n1, n2;
				char *tmp1 = tmp;
				/* Get algorithm number */
				tmp1 = number(tmp1, &n1);
				if (n1 != 1 && n1 != 0 && n1 != 2) {
					expected("soudef", "0, 1, 2", cp-2);
				}
				tmp1 = number(tmp1, &n2);
				mlp = ALLOC(struct mlist);
				*prev = mlp;
				mlp->flags = 0;
				mlp->offset = n2*8;

				mlp->name = "vtbl";
				mlp->fdesc = funcptr();
				mlp->size = mlp->fdesc->size*8;
				prev = &mlp->next;
			}
			free(tmp);
		}
	}
	free(cname);
	free(cinher_old);
	*prev = NULL;
	return (cp);
}

char *
offsize(cp, mlp)
	char *cp;
	struct mlist *mlp;
{
	long offset, size;

	if (*cp++ != ',')
		expected("offsize/1", ",", cp - 1);
	cp = number(cp, &offset);
	if (*cp++ != ',')
		expected("offsize/2", ",", cp - 1);
	cp = number(cp, &size);
	if (*cp++ != ';')
		expected("offsize/3", ";", cp - 1);
	mlp->offset = offset;
	mlp->size = size;
	return (cp);
}

/*
 * Skip over typedefs
 */
int contentsize(struct tdesc *tdp) {
	while (tdp != NULL && tdp->type == TYPEOF) {
		tdp = tdp->data.tdesc;
	}
	if (tdp == NULL) {
		return (0);
	} else {
		return (tdp->size);
	}
}

char *
arraydef(char *cp, struct tdesc **rtdp)
{
	int h;
	long start, end;

	cp = id(cp, &h);
	if (*cp++ != ';')
		expected("arraydef/1", ";", cp - 1);

	(*rtdp)->data.ardef = ALLOC(struct ardef);
	(*rtdp)->data.ardef->indices = ALLOC(struct element);
	(*rtdp)->data.ardef->indices->index_type = lookup(h);

	cp = number(cp, &start);
	if (*cp == '\\')
		cp = get_continuation();
	if (*cp++ != ';')
		expected("arraydef/2", ";", cp - 1);
	cp = number(cp, &end);
	if (*cp == '\\')
		cp = get_continuation();
	if (*cp++ != ';')
		expected("arraydef/3", ";", cp - 1);
	(*rtdp)->data.ardef->indices->range_start = start;
	(*rtdp)->data.ardef->indices->range_end = end;
	cp = tdefdecl(cp, &((*rtdp)->data.ardef->contents), "");
	(*rtdp)->size = contentsize((*rtdp)->data.ardef->contents) *
		(end - start + 1);
	if (*cp == '\\')
		cp = get_continuation();
	return (cp);
}

char *
enumdef(char *cp, struct tdesc **rtdp)
{
	char *next;
	struct elist *elp, **prev;
	char *w;

	(*rtdp)->type = ENUM;
	(*rtdp)->size = 0;
	(*rtdp)->data.emem = NULL;

	prev = &((*rtdp)->data.emem);
	while (*cp != ';') {
		elp = ALLOC(struct elist);
		elp->next = NULL;
		*prev = elp;
		cp = name(cp, &w, NULL);
		elp->name = w;
		cp = number(cp, &elp->number);
		prev = &elp->next;
		if (*cp++ != ',')
			expected("enumdef", ",", cp - 1);
		if (*cp == '\\')
			cp = get_continuation();
	}
	return (++cp);
}

/*
 * Add a node to the hash queues.
 */
void
addhash(tdp, num)
	struct tdesc *tdp;
	int num;
{
	int hash = HASH(num);

	tdp->id = num;
	tdp->hash = hash_table[hash];
	hash_table[hash] = tdp;

	if (tdp->name) {
		printf("Added name %s\n", tdp->name);
		hash = compute_sum(tdp->name);
		tdp->next = name_table[hash];
		name_table[hash] = tdp;
	}
}

struct tdesc *
lookupname(name)
	char *name;
{
	int hash = compute_sum(name);
	struct tdesc *tdp, *ttdp = NULL;

	for (tdp = name_table[hash]; tdp != NULL; tdp = tdp->next) {
		if (tdp->name != NULL && strcmp(tdp->name, name) == 0) {
			if (tdp->type == STRUCT || tdp->type == UNION ||
			    tdp->type == ENUM || tdp->type == CLASS ||
			    tdp->type == TEMPLATE)
				return (tdp);
			if (tdp->type == TYPEOF)
				ttdp = tdp;
		}
	}
	return (ttdp);
}

int
compute_sum(char *w)
{
	char c;
	int sum;

	for (sum = 0; c = *w; sum += c, w++)
		;
	return (HASH(sum));
}

void
reset_func(int n)
{
	if (parsing) {
		fprintf(stderr, "Unexpected reset in parser line %d "
			"input line %d parsing %s\n",
			n, line_number, linebuf);
		parsing = 0;
	}
	longjmp(resetbuf, 1);
	/* NOTREACHED */
}
