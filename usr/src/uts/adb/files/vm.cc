//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)vm.cc	1.6	08/05/20 SMI"

// Files from directory usr/src/common/cl/vm

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>

// Classes from coord_cluster_info.h
#include <vm/coord_cluster_info.h>
coord_cluster_info *var_coord_cluster_info;

// Classes from coord_vp.h
#include <vm/coord_vp.h>
coord_vp::node_entry *var_coord_vp_node_entry;
coord_vp *var_coord_vp;

// Classes from ucc_group.h
#include <vm/ucc_group.h>
ucc_group *var_ucc_group;

// Classes from version_prot_obj.h
#include <vm/version_prot_obj.h>
vp_rev_depend *var_vp_rev_depend;
version_prot_obj *var_version_prot_obj;

// Classes from version_range.h
#include <vm/version_range.h>
version_range *var_version_range;

// Classes from versioned_protocol.h
#include <vm/versioned_protocol.h>
versioned_protocol *var_versioned_protocol;

// Classes from vm_admin_impl.h
#include <vm/vm_admin_impl.h>
vm_admin_impl *var_vm_admin_impl;

// Classes from vm_comm.h
#include <vm/vm_comm.h>
vm_comm *var_vm_comm;

// Classes from vm_coord.h
#include <vm/vm_coord.h>
vm_coord *var_vm_coord;

// Classes from vm_internal_custom.h
#include <vm/vm_internal_custom.h>
vm_internal_custom *var_vm_internal_custom;

// Classes from vm_lca_impl.h
#include <vm/vm_lca_impl.h>
vm_lca_impl *var_vm_lca_impl;

// Classes from vm_yy_scanner_impl.h
#include <vm/vm_yy_scanner_impl.h>
vm_yy_scanner_impl *var_vm_yy_scanner_impl;

// Classes from vp_dependency.h
#include <vm/vp_dependency.h>
vp_dependency *var_vp_dependency;

// Classes from vp_rhs_version.h
#include <vm/vp_rhs_version.h>
vp_rhs_version *var_vp_rhs_version;

// Classes from vp_set.h
#include <vm/vp_set.h>
vp_set *var_vp_set;

// Classes from vp_set_key.h
#include <vm/vp_set_key.h>
vp_set_key::group_entry *var_vp_set_key_group_entry;
vp_set_key::node_entry *var_vp_set_key_node_entry;
vp_set_key *var_vp_set_key;

// Classes from vp_triad.h
#include <vm/vp_triad.h>
vp_triad *var_vp_triad;

// Classes from vp_upgrade.h
#include <vm/vp_upgrade.h>
vp_upgrade *var_vp_upgrade;

#endif /* _KERNEL */
