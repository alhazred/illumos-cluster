//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)mount.cc	1.6	08/05/20 SMI"

// Files from directory usr/src/common/cl/pxfs/mount

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from mount_client_impl.h
#include <pxfs/mount/mount_client_impl.h>
mount_client_impl *var_mount_client_impl;

// Classes from mount_replica_impl.h
#include <pxfs/mount/mount_replica_impl.h>
mount_replica_impl *var_mount_replica_impl;

// Classes from mount_server_impl.h
#include <pxfs/mount/mount_server_impl.h>
fs_elem *var_fs_elem;
devlock_elem *var_devlock_elem;
mount_client_elem *var_mount_client_elem;
mount_state *var_mount_state;
unmount_state *var_unmount_state;
dc_callback_impl *var_dc_callback_impl;
mount_server_impl *var_mount_server_impl;

#endif /* _KERNEL */
