//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)idl_datatypes.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/idl_datatypes

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from idl_strings.h
#include <orb/idl_datatypes/idl_strings.h>
_string_field *var__string_field;
_string_var *var__string_var;
_string_out *var__string_out;

// Classes from sequence.h
#include <orb/idl_datatypes/sequence.h>
GenericSequence *var_GenericSequence;

// Classes from string_seq.h
#include <orb/idl_datatypes/string_seq.h>
_string_seq *var__string_seq;
