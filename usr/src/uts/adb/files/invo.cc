//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)invo.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/invo

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

#ifdef _KERNEL_ORB

// Classes from arg_info.h
#include <orb/invo/arg_info.h>
arg_info *var_arg_info;

// Classes from argfuncs.h
#include <orb/invo/argfuncs.h>
MarshalInfo_complex *var_MarshalInfo_complex;
MarshalInfo_object *var_MarshalInfo_object;

// Classes from corba.h
#include <orb/invo/corba.h>
CORBA *var_CORBA;
CORBA::Object *var_Object;
CORBA::Exception *var_Exception;
CORBA::SystemException *var_SystemException;
CORBA::UserException *var_UserException;
CORBA::BAD_PARAM *var_BAD_PARAM;
CORBA::COMM_FAILURE *var_COMM_FAILURE;
CORBA::INV_OBJREF *var_INV_OBJREF;
CORBA::WOULDBLOCK *var_WOULDBLOCK;
CORBA::RETRY_NEEDED *var_RETRY_NEEDED;
CORBA::PRIMARY_FROZEN *var_PRIMARY_FROZEN;
CORBA::VERSION *var_VERSION;
CORBA::Environment *var_Environment;
CORBA::Object_stub *var_Object_stub;
exception_major_data *var_exception_major_data;

// Classes from invo_args.h
#include <orb/invo/invo_args.h>
arg_sizes *var_arg_sizes;
invo_args *var_invo_args;
arg_desc *var_arg_desc;

// Classes from invocation.h
#ifdef _KERNEL_ORB
#if defined(_FAULT_INJECTION)
#include <orb/invo/invocation.h>
service *var_service;
invocation *var_invocation;
service_oneway *var_service_oneway;
service_twoway *var_service_twoway;
#endif
#endif

// Classes from invocation_mode.h
#include <orb/invo/invocation_mode.h>
invocation_mode *var_invocation_mode;

// Classes from io_invos.h
#if defined(_FAULT_INJECTION)
#include <orb/invo/io_invos.h>
invo_base *var_invo_base;
outbound_invo *var_outbound_invo;
outbound_invo_table *var_outbound_invo_table;
inbound_invo *var_inbound_invo;
inbound_invo_table *var_inbound_invo_table;
#endif

#endif /* _KERNEL_ORB */
