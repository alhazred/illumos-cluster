//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)transports_unode.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/transports/unode

#include "dummy.h"

#ifndef	_KERNEL


#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from unode_transport.h
#include <transports/unode/unode_transport.h>
unode_stats_t *var_unode_stats_t;
unode_pathend *var_unode_pathend;
unode_endpoint *var_unode_endpoint;
unode_sendstream *var_unode_sendstream;
unode_recstream *var_unode_recstream;
unode_adapter *var_unode_adapter;
unode_transport *var_unode_transport;

#endif /* _KERNEL */

#endif /* _KERNEL */
