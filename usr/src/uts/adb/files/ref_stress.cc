//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ref_stress.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orbtest/ref_stress

#include "dummy.h"

#ifdef	_FAULT_INJECTION

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from client_impl.h
#include <orbtest/ref_stress/client_impl.h>
thread *var_thread;
client_impl *var_client_impl;

#ifndef	_KERNEL
// Classes from driver_support.h
#include <orbtest/ref_stress/driver_support.h>
iter_manip *var_iter_manip;
testobj_descr *var_testobj_descr;
rsrc_bal *var_rsrc_bal;
server_testobjs *var_server_testobjs;
server_info *var_server_info;
client_threads *var_client_threads;
client_info *var_client_info;
thread_info *var_thread_info;
#endif

// Classes from impl_common.h
#if defined(_KERNEL_ORB)
#if defined(_KERNEL)
#if defined(_KERNEL)
#if defined(_KERNEL)
#if defined(_KERNEL)
#include <orbtest/ref_stress/impl_common.h>
impl_common *var_impl_common;
#endif
#endif
#endif
#endif
#endif

// Classes from server_impl.h
#include <orbtest/ref_stress/server_impl.h>
server_impl *var_server_impl;

#endif /* _FAULT_INJECTION */
