//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)tcp.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/common/cl/transports/tcp

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#include <sys/socket.h>

// Classes from arp_cache_monitor.h
#include <transports/tcp/arp_cache_monitor.h>
acm_ipmac *var_acm_ipmac;
acm_defer_task *var_acm_defer_task;
arp_cache_monitor *var_arp_cache_monitor;

// Classes from raw_dlpi.h
#include <transports/tcp/raw_dlpi.h>
raw_dlpi_conn *var_raw_dlpi_conn;
raw_dlpi_conn_stats *var_raw_dlpi_conn_stats;
raw_dlpi_perhb_stats *var_raw_dlpi_perhb_stats;
raw_dlpi_adapter *var_raw_dlpi_adapter;
raw_dlpi *var_raw_dlpi;
raw_dlpi_info_s *var_raw_dlpi_info_s;
raw_dlpi_hb_hdr_s *var_raw_dlpi_hb_hdr_s;

// Classes from tcp_io.h
#include <transports/tcp/tcp_io.h>
rio_cookie *var_rio_cookie;
tcpmod_replyio_info *var_tcpmod_replyio_info;
tcp_rio_client *var_tcp_rio_client;
tcp_rio_task *var_tcp_rio_task;

// Classes from tcp_transport.h
#include <transports/tcp/tcp_transport.h>
#ifdef DEBUG
tcp_stats_t *var_tcp_stats_t;
#endif /* DEBUG */
pathend_udp_info *var_pathend_udp_info;
endpoint_udp_info *var_endpoint_udp_info;
init_msg *var_init_msg;
tcp_pathend *var_tcp_pathend;
tcp_message_header *var_tcp_message_header;
tcp_endpoint *var_tcp_endpoint;
tcp_sendstream *var_tcp_sendstream;
tcp_sendstream::tcp_MarshalStream *var_tcp_MarshalStream;
tcp_recstream *var_tcp_recstream;
tcp_adapter *var_tcp_adapter;
tcp_transport *var_tcp_transport;
start_server_task *var_start_server_task;
tcp_keep_active_task *var_tcp_keep_active_task;

// Classes from tcp_util.h
#include <transports/tcp/tcp_util.h>
tcp_util *var_tcp_util;

// Classes from tcpmod.h
#include <transports/tcp/tcpmod.h>
tcpmod_header_t *var_tcpmod_header_t;
tcpmod_rio_header *var_tcpmod_rio_header;
tcpmod_conn_info *var_tcpmod_conn_info;

#endif /* _KERNEL */
