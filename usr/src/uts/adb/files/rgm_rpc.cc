//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_rpc.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/head/rgm/rpc

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from fe.h
#include <head/rgm/fe.h>
fe_run_result *var_fe_run_result;
fe_result *var_fe_result;
fe_run_args *var_fe_run_args;
fe_args *var_fe_args;

// Classes from pmf.h
#include <head/rgm/pmf.h>
pmf_error *var_pmf_error;
pmf_cmd *var_pmf_cmd;
pmf_result *var_pmf_result;
pmf_status_result *var_pmf_status_result;
pmf_start_args *var_pmf_start_args;
pmf_modify_args *var_pmf_modify_args;
pmf_args *var_pmf_args;

// Classes from rgm_recep.h
#include <head/rgm/rgm_recep.h>
rs_open_args *var_rs_open_args;
rg_open_args *var_rg_open_args;
rt_open_args *var_rt_open_args;
scha_ssm_lb_args *var_scha_ssm_lb_args;
ssm_ip_args *var_ssm_ip_args;
get_err_string_args *var_get_err_string_args;
get_err_result_t *var_get_err_result_t;
open_result_t *var_open_result_t;
rs_open_result_t *var_rs_open_result_t;
scha_ssm_lb_result_t *var_scha_ssm_lb_result_t;
ssm_result_t *var_ssm_result_t;
rs_get_args *var_rs_get_args;
rs_get_state_status_args *var_rs_get_state_status_args;
rs_get_fail_status_args *var_rs_get_fail_status_args;
rt_get_args *var_rt_get_args;
rg_get_args *var_rg_get_args;
rg_get_state_args *var_rg_get_state_args;
get_result_t *var_get_result_t;
get_rt_name_result_t *var_get_rt_name_result_t;
get_ext_result_t *var_get_ext_result_t;
get_status_result_t *var_get_status_result_t;
rs_set_status_args *var_rs_set_status_args;
set_status_result_t *var_set_status_result_t;
scha_control_args *var_scha_control_args;
scha_result_t *var_scha_result_t;
cluster_get_args *var_cluster_get_args;
