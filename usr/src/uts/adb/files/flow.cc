//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)flow.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/flow

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from flow_stats.h
#include <orb/flow/flow_stats.h>
flow_stats *var_flow_stats;
cl_flow_stats *var_cl_flow_stats;

// Classes from resource.h
#ifdef _KERNEL_ORB
#ifdef _KERNEL_ORB
#include <orb/flow/resource.h>
resources *var_resources;
// resources::resources_msg *var_resources_msg;
resources_datagram *var_resources_datagram;
#endif
#endif

// Classes from resource_balancer.h
#include <orb/flow/resource_balancer.h>
resource_pool_static_values *var_resource_pool_static_values;
resource_balancer *var_resource_balancer;
// resource_balancer::resource_pool_info *var_resource_pool_info;
cl_flow_specs *var_cl_flow_specs;
cl_flow_threads_min *var_cl_flow_threads_min;

// Classes from resource_blocker.h
#include <orb/flow/resource_blocker.h>
disable_request *var_disable_request;
resource_blocker *var_resource_blocker;

// Classes from resource_defs.h
#include <orb/flow/resource_defs.h>
resource_defs *var_resource_defs;

// Classes from resource_done.h
#include <orb/flow/resource_done.h>
resource_done *var_resource_done;

// Classes from resource_mgr.h
#include <orb/flow/resource_mgr.h>
resource_grant *var_resource_grant;
resource_policy *var_resource_policy;
resource_mgr *var_resource_mgr;
oneway_flow_header *var_oneway_flow_header;

// Classes from resource_pool.h
#include <orb/flow/resource_pool.h>
resource_pool *var_resource_pool;

#endif /* _KERNEL */
