//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rpc.scadmd.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/cmd/rpc.scadmd

#include "dummy.h"

#ifndef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from rpc_scadmd.h
#include <cmd/rpc.scadmd/rpc_scadmd.h>
sc_cltr_epoint *var_sc_cltr_epoint;
sc_gdev_range_entry *var_sc_gdev_range_entry;
sc_cfg_prop_entry *var_sc_cfg_prop_entry;
sc_result *var_sc_result;
sc_result_addnode *var_sc_result_addnode;
sc_result_authtype *var_sc_result_authtype;
sc_result_ismember *var_sc_result_ismember;
sc_result_cname *var_sc_result_cname;
sc_result_file *var_sc_result_file;
sc_result_adapters *var_sc_result_adapters;
sc_result_trconfig *var_sc_result_trconfig;
sc_result_snoop *var_sc_result_snoop;
sc_result_autodiscover *var_sc_result_autodiscover;
sc_result_removenode *var_sc_result_removenode;
sc_result_major_number *var_sc_result_major_number;
scadmproc_add_cltr_adapter_1_argument *
var_scadmproc_add_cltr_adapter_1_argument;
scadmproc_add_cltr_cpoint_1_argument *var_scadmproc_add_cltr_cpoint_1_argument;
scadmproc_add_cltr_cable_1_argument *var_scadmproc_add_cltr_cable_1_argument;
scadmproc_remove_file_1_argument *var_scadmproc_remove_file_1_argument;
scadmproc_add_ds_1_argument *var_scadmproc_add_ds_1_argument;
scadmproc_change_ds_1_argument *var_scadmproc_change_ds_1_argument;
scadmproc_get_network_adapters_1_argument *
var_scadmproc_get_network_adapters_1_argument;
scadmproc_snoop_1_argument *var_scadmproc_snoop_1_argument;
scadmproc_autodiscover_1_argument *var_scadmproc_autodiscover_1_argument;

#endif /* _KERNEL */
