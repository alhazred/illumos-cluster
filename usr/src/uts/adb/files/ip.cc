//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ip.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/ip

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from fpconf.h
#include <orb/ip/fpconf.h>
fp_holder *var_fp_holder;
pernodepath *var_pernodepath;
fp_adapter *var_fp_adapter;
fpconf *var_fpconf;
cl_net_ns_if_impl_t *var_cl_net_ns_if_impl_t;

// Classes from ifkconfig.h
#include <orb/ip/ifkconfig.h>
ifk_interface *var_ifk_interface;

// Classes from ipconf.h
#include <orb/ip/ipconf.h>
ipconf_task *var_ipconf_task;
ipconf_path_elem *var_ipconf_path_elem;
ipconf *var_ipconf;
// ipconf::ipconf_adapter *var_ipconf_adapter;

#endif /* _KERNEL */
