//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_apid.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/cmd/cl_apid

#include "dummy.h"

#ifndef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from caapi_client.h
#include <cmd/cl_apid/caapi_client.h>
caapi_client *var_caapi_client;

// Classes from caapi_client_handle.h
#include <cmd/cl_apid/caapi_client_handle.h>
caapi_client_handle *var_caapi_client_handle;

// Classes from caapi_event.h
#include <cmd/cl_apid/caapi_event.h>
caapi_event *var_caapi_event;

// Classes from caapi_mapping.h
#include <cmd/cl_apid/caapi_mapping.h>
caapi_mapping *var_caapi_mapping;

// Classes from caapi_reg_cache.h
#include <cmd/cl_apid/caapi_reg_cache.h>
caapi_reg_cache *var_caapi_reg_cache;

// Classes from caapi_reg_io.h
#include <cmd/cl_apid/caapi_reg_io.h>
caapi_reg_io *var_caapi_reg_io;

// Classes from client_reg_info.h
#include <cmd/cl_apid/client_reg_info.h>
client_reg_info *var_client_reg_info;

// Classes from evt_comm_module.h
#include <cmd/cl_apid/evt_comm_module.h>
comm_req *var_comm_req;
client_connect *var_client_connect;

// Classes from nvstrpair.h
#include <cmd/cl_apid/nvstrpair.h>
nvstrpair *var_nvstrpair;

// Classes from sc_callback_info.h
#include <cmd/cl_apid/sc_callback_info.h>
sc_callback_info *var_sc_callback_info;

// Classes from sc_event.h
#include <cmd/cl_apid/sc_event.h>
sc_event *var_sc_event;

// Classes from sc_xml_event.h
#include <cmd/cl_apid/sc_xml_event.h>
sc_xml_event *var_sc_xml_event;

// Classes from sc_xml_instance.h
#include <cmd/cl_apid/sc_xml_instance.h>
sc_xml_instance *var_sc_xml_instance;

// Classes from sc_xml_reply.h
#include <cmd/cl_apid/sc_xml_reply.h>
sc_xml_reply *var_sc_xml_reply;

// Classes from smart_string.h
#include <cmd/cl_apid/smart_string.h>
smart_string *var_smart_string;

// Classes from string_buffer.h
#include <cmd/cl_apid/string_buffer.h>
string_buffer *var_string_buffer;

// Classes from tcp_wrapper.h
#include <cmd/cl_apid/tcp_wrapper.h>
tcp_wrapper *var_tcp_wrapper;

// Classes from xml_module.h
#include <cmd/cl_apid/xml_module.h>
xml_module *var_xml_module;

#endif /* _KERNEL */
