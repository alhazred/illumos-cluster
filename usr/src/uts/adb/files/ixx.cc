//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ixx.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/common/cl/interfaces/ixx

#include "dummy.h"

// ixx is a userland program, so we do not generate
// the macros for debugging it in the kernel context.
#if !defined(_KERNEL) && !defined(_KERNEL_ORB)
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#include <ixx/expr-impl.h>

// Classes from Declarator.h
#include <interfaces/ixx/Declarator.h>
Declarator *var_Declarator;

// Classes from ExceptDecl.h
#include <interfaces/ixx/ExceptDecl.h>
ExceptDecl *var_ExceptDecl;

// Classes from InterfaceDef.h
#include <interfaces/ixx/InterfaceDef.h>
OpInfo *var_OpInfo;
VersionInfo *var_VersionInfo;
InterfaceInfo *var_InterfaceInfo;
ParentImpl *var_ParentImpl;
InterfaceDef *var_InterfaceDef;

// Classes from Module.h
#include <interfaces/ixx/Module.h>
Module *var_Module;

// Classes from Operation.h
#include <interfaces/ixx/Operation.h>
Operation *var_Operation;

// Classes from Parameter.h
#include <interfaces/ixx/Parameter.h>
Parameter *var_Parameter;

// Classes from Resolver.h
#include <interfaces/ixx/Resolver.h>
Context *var_Context;
Resolver *var_Resolver;

// Classes from SequenceDecl.h
#include <interfaces/ixx/SequenceDecl.h>
SequenceDecl *var_SequenceDecl;

// Classes from StringDecl.h
#include <interfaces/ixx/StringDecl.h>
StringDecl *var_StringDecl;

// Classes from StructDecl.h
#include <interfaces/ixx/StructDecl.h>
StructDecl *var_StructDecl;

// Classes from StructMember.h
#include <interfaces/ixx/StructMember.h>
StructMember *var_StructMember;

// Classes from Symbol.h
#include <interfaces/ixx/Symbol.h>
Symbol *var_Symbol;

// Classes from SymbolTable.h
#include <interfaces/ixx/SymbolTable.h>
SymbolTable *var_SymbolTable;

// Classes from UnionDecl.h
#include <interfaces/ixx/UnionDecl.h>
UnionDecl *var_UnionDecl;

// Classes from UnionMember.h
#include <interfaces/ixx/UnionMember.h>
UnionMember *var_UnionMember;

// Classes from err.h
#include <interfaces/ixx/err.h>
SourcePosition *var_SourcePosition;
ErrorHandler *var_ErrorHandler;
ErrorHandlerKit *var_ErrorHandlerKit;

// Classes from expr-impl.h
#include <interfaces/ixx/expr-impl.h>
HashCode *var_HashCode;
ExprImpl *var_ExprImpl;
Scope *var_Scope;
IdentifierImpl *var_IdentifierImpl;
ExprKitImpl *var_ExprKitImpl;
RootExpr *var_RootExpr;
Accessor *var_Accessor;
Constant *var_Constant;
Unary *var_Unary;
Binary *var_Binary;
TypeName *var_TypeName;
UnsignedType *var_UnsignedType;
CaseElement *var_CaseElement;
DefaultLabel *var_DefaultLabel;
CaseLabel *var_CaseLabel;
EnumDecl *var_EnumDecl;
Enumerator *var_Enumerator;
boolLiteral *var_boolLiteral;
IntegerLiteral *var_IntegerLiteral;
FloatLiteral *var_FloatLiteral;
StringLiteral *var_StringLiteral;
CharLiteral *var_CharLiteral;
SrcPos *var_SrcPos;

// Classes from expr.h
#include <interfaces/ixx/expr.h>
ConfigInfo *var_ConfigInfo;
expr_descriptor *var_expr_descriptor;
Expr *var_Expr;

// Classes from exprkit.h
#include <interfaces/ixx/exprkit.h>
ExprKit *var_ExprKit;

// Classes from generator.h
#define	ADB_MACRO
#include <interfaces/ixx/generator.h>
Generator *var_Generator;
Generator::IdInfo *var_IdInfo;
Generator::IdTable *var_IdTable;

// Classes from scanner.h
#include <interfaces/ixx/scanner.h>
Scanner *var_Scanner;
ScannerKit *var_ScannerKit;

// Classes from types.h
#include <interfaces/ixx/types.h>
Memory *var_Memory;
String *var_String;
CopyString *var_CopyString;
NullTerminatedString *var_NullTerminatedString;
UniqueString *var_UniqueString;
#endif /* !defined(KERNEL) && !defined(_KERNEL_ORB) */
