//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)scadmin.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/head/scadmin

#include "dummy.h"

#ifndef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from scadmin.h
#include <head/scadmin/scadmin.h>
scadmin_namelist *var_scadmin_namelist;

// Classes from scconf.h
#include <head/scadmin/scconf.h>
scconf_namelist *var_scconf_namelist;
scconf_cfg_adapter *var_scconf_cfg_adapter;
scconf_cltr_propdes *var_scconf_cltr_propdes;
scconf_cltr_epoint *var_scconf_cltr_epoint;
scconf_cfg_prop *var_scconf_cfg_prop;
scconf_cfg_port *var_scconf_cfg_port;
scconf_cfg_cpoint *var_scconf_cfg_cpoint;
scconf_cfg_cltr_adap *var_scconf_cfg_cltr_adap;
scconf_cfg_node *var_scconf_cfg_node;
scconf_cfg_cable *var_scconf_cfg_cable;
scconf_cfg_qdevport *var_scconf_cfg_qdevport;
scconf_cfg_qdev *var_scconf_cfg_qdev;
scconf_cfg_cluster *var_scconf_cfg_cluster;
scconf_gdev_range *var_scconf_gdev_range;
scconf_cfg_devices *var_scconf_cfg_devices;
scconf_cfg_did *var_scconf_cfg_did;
scconf_cfg_ds *var_scconf_cfg_ds;
scconf_cfg_lhost *var_scconf_cfg_lhost;
scconf_cfg_shost *var_scconf_cfg_shost;

// Classes from scstat.h
#include <head/scadmin/scstat.h>
scstat_node_list_struct *var_scstat_node_list_struct;
scstat_node_access_struct *var_scstat_node_access_struct;
scstat_path_epoint *var_scstat_path_epoint;
scstat_ds_node_state_struct *var_scstat_ds_node_state_struct;
scstat_rg_status_struct *var_scstat_rg_status_struct;
scstat_rs_status_struct *var_scstat_rs_status_struct;
scstat_rs_struct *var_scstat_rs_struct;
scstat_node_struct *var_scstat_node_struct;
scstat_path_struct *var_scstat_path_struct;
scstat_transport_struct *var_scstat_transport_struct;
scstat_ds_struct *var_scstat_ds_struct;
scstat_quorumdev_struct *var_scstat_quorumdev_struct;
scstat_node_quorum_struct *var_scstat_node_quorum_struct;
scstat_quorum_struct *var_scstat_quorum_struct;
scstat_rg_struct *var_scstat_rg_struct;
scstat_cluster_struct *var_scstat_cluster_struct;
scstat_grp_stat_list *var_scstat_grp_stat_list;
scstat_ipmp_stat_list *var_scstat_ipmp_stat_list;

// Classes from scswitch.h
#include <head/scadmin/scswitch.h>
scswitch_errbuff *var_scswitch_errbuff;
scswitch_service_struct *var_scswitch_service_struct;

// Classes from scsymon_srv.h
#include <head/scadmin/scsymon_srv.h>
cluster_config_struct *var_cluster_config_struct;
cluster_status_struct *var_cluster_status_struct;
node_config_struct *var_node_config_struct;
node_status_struct *var_node_status_struct;
node_device_config_struct *var_node_device_config_struct;
node_device_status_struct *var_node_device_status_struct;
devgrp_config_struct *var_devgrp_config_struct;
devgrp_status_struct *var_devgrp_status_struct;
replica_status_struct *var_replica_status_struct;
qdev_config_struct *var_qdev_config_struct;
qdev_status_struct *var_qdev_status_struct;
qdevport_config_struct *var_qdevport_config_struct;
path_status_struct *var_path_status_struct;
transport_adapter_config_struct *var_transport_adapter_config_struct;
junction_config_struct *var_junction_config_struct;
cable_config_struct *var_cable_config_struct;
rt_config_struct *var_rt_config_struct;
rt_method_config_struct *var_rt_method_config_struct;
rt_param_config_struct *var_rt_param_config_struct;
rt_rs_config_struct *var_rt_rs_config_struct;
rg_config_struct *var_rg_config_struct;
rg_status_struct *var_rg_status_struct;
rs_config_struct *var_rs_config_struct;
rs_status_struct *var_rs_status_struct;
rs_prop_config_struct *var_rs_prop_config_struct;
scsymon_rgm_rt_struct *var_scsymon_rgm_rt_struct;
scsymon_rgm_rs_struct *var_scsymon_rgm_rs_struct;
scsymon_rgm_rg_rs_struct *var_scsymon_rgm_rg_rs_struct;

#endif /* _KERNEL */
