//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)repl_mgr.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/common/cl/repl/repl_mgr

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from repl_mgr_impl.h
#include <repl/repl_mgr/repl_mgr_impl.h>
repl_mgr_impl *var_repl_mgr_impl;
repl_mgr_impl::rcf_list *var_rcf_list;
repl_mgr_impl::add_service_recovery_data *var_add_service_recovery_data;
// repl_mgr_impl::periodic_cleanup_task *var_periodic_cleanup_task;

// Classes from repl_mgr_prov_impl.h
#include <repl/repl_mgr/repl_mgr_prov_impl.h>
repl_mgr_prov_impl *var_repl_mgr_prov_impl;

// Classes from rm_repl_service.h
#include <repl/repl_mgr/rm_repl_service.h>
rm_repl_service *var_rm_repl_service;
rm_repl_service::prov_reg_impl *var_prov_reg_impl;
rm_repl_service::service_recovery_data *var_service_recovery_data;
rm_repl_service::provider_recovery_data *var_provider_recovery_data;
prov_reg_list *var_prov_reg_list;
repl_service_list *var_repl_service_list;

// Classes from rm_state_mach.h
#include <repl/repl_mgr/rm_state_mach.h>
rm_state_machine *var_rm_state_machine;
// rm_state_machine::repl_prov_combo_score *var_repl_prov_combo_score;

// Classes from rm_trans_states.h
#include <repl/repl_mgr/rm_trans_states.h>
trans_state_register_service *var_trans_state_register_service;
trans_state_get_control *var_trans_state_get_control;
trans_state_add_rma *var_trans_state_add_rma;

// Classes from rs_trans_states.h
#include <repl/repl_mgr/rs_trans_states.h>
trans_state_reg_rma_repl_prov *var_trans_state_reg_rma_repl_prov;
trans_state_shutdown_service *var_trans_state_shutdown_service;

// Classes from service_admin_impl.h
#include <repl/repl_mgr/service_admin_impl.h>
service_admin_impl *var_service_admin_impl;
service_admin_impl::sa_state_count *var_sa_state_count;

// Classes from cb_coord_control_impl.h
#include <repl/repl_mgr/cb_coord_control_impl.h>
cb_coordinator *var_cb_coordinator;
cb_coord_control_impl *var_cb_coord_control_impl;

// Classes from dependency_mgr_impl.h
#include <repl/repl_mgr/dependency_mgr_impl.h>
dependency_mgr_impl *var_dependency_mgr_impl;

#endif /* _KERNEL */
