//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)handler.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/handler

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from combo_handler.h
#ifdef _KERNEL_ORB
#include <orb/handler/combo_handler.h>
combo_handler_kit *var_combo_handler_kit;
combo_handler *var_combo_handler;
combo_handler_server *var_combo_handler_server;
combo_handler_client *var_combo_handler_client;
#endif

// Classes from counter_handler.h
#include <orb/handler/counter_handler.h>
delegate_counter_handler *var_delegate_counter_handler;
counter_handler *var_counter_handler;

// Classes from gateway.h
#include <orb/handler/gateway.h>
gateway_handler *var_gateway_handler;
gateway_manager *var_gateway_manager;
gateway_proxy_data *var_gateway_proxy_data;
count_Buf *var_count_Buf;

// Classes from handler.h
#include <orb/handler/handler.h>
handler_kit *var_handler_kit;
handler *var_handler;
handler::revoke_state_t *var_revoke_state_t;
handler::handler_state_t *var_handler_state_t;
handler_repository *var_handler_repository;

// Classes from nil_handler.h
#include <orb/handler/nil_handler.h>
nil_handler_kit *var_nil_handler_kit;
nil_handler *var_nil_handler;

// Classes from remote_handler.h
#include <orb/handler/remote_handler.h>
remote_handler_kit *var_remote_handler_kit;
remote_handler *var_remote_handler;

// Classes from standard_handler.h
#include <orb/handler/standard_handler.h>
standard_handler_kit *var_standard_handler_kit;
standard_handler *var_standard_handler;
standard_handler_server *var_standard_handler_server;
standard_handler_client *var_standard_handler_client;

#endif /* _KERNEL */
