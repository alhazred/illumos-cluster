//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfs_client.cc	1.9	08/05/20 SMI"

// Files from directory usr/src/common/cl/pxfs/client

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#include <pxfs/client/pxfobj.h>
#define	DEF_IOV_MAX 16

// Classes from acccess_cache.h
#include <pxfs/client/access_cache.h>
struct acache_entry *var_acache_entry;

// Classes from aio_callback_impl.h
#include <pxfs/client/aio_callback_impl.h>
ha_async *var_ha_async;
aio_callback_impl *var_aio_callback_impl;

// Classes from fobj_client_impl.h
#include <pxfs/client/fobj_client_impl.h>
fobj_client_impl *var_fobj_client_impl;

// Classes from fsmgr_client_impl.h
#include <pxfs/client/fsmgr_client_impl.h>
fsmgr_client_impl *var_fsmgr_client_impl;

// Classes from mem_async.h
#include <pxfs/client/mem_async.h>
mem_async_write *var_mem_async_write;
mem_async_read *var_mem_async_read;

// Classes from pxchr.h
#include <pxfs/client/pxchr.h>
pxchr *var_pxchr;

// Classes from pxdir.h
#include <pxfs/client/pxdir.h>
pxdir *var_pxdir;

// Classes from pxfobj.h
#include <pxfs/client/pxfobj.h>
pxfobj *var_pxfobj;

// Classes from pxfobjplus.h
#include <pxfs/client/pxfobjplus.h>
pxfobjplus *var_pxfobjplus;

// Classes from pxlink.h
#include <pxfs/client/pxlink.h>
pxlink *var_pxlink;

// Classes from pxnode.h
#include <pxfs/client/pxnode.h>
pxnode *var_pxnode;

// Classes from pxreg.h
#include <pxfs/client/pxreg.h>
pxreg *var_pxreg;

// Classes from pxspecial.h
#include <pxfs/client/pxspecial.h>
pxspecial *var_pxspecial;

// Classes from pxvfs.h
#include <pxfs/client/pxvfs.h>
pxvfs_inactive_task *var_pxvfs_inactive_task;
pxvfs_inactive_threadpool *var_pxvfs_inactive_threadpool;
pxvfs_list_elem *var_pxvfs_list_elem;
pxvfs *var_pxvfs;
pxvfs::pxfobj_hash_bkt *var_pxfobj_hash_bkt;

#endif /* _KERNEL */
