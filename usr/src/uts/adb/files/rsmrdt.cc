//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmrdt.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/uts/common/io/rsmrdt

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#include <sys/taskq.h>

// Classes from rsmrdt.h
#ifdef	_KERNEL
#include <uts/common/io/rsmrdt/rsmrdt.h>
opsrsm_param *var_opsrsm_param;
opsrsm *var_opsrsm;
opsrsm_stat *var_opsrsm_stat;
opsrsm_flow_ctl *var_opsrsm_flow_ctl;
opsrsm_xfer_hdr *var_opsrsm_xfer_hdr;
opsrsmbuf *var_opsrsmbuf;
opsrsm_msg_header *var_opsrsm_msg_header;
opsrsm_con_request *var_opsrsm_con_request;
opsrsm_con_accept *var_opsrsm_con_accept;
opsrsm_con_ack *var_opsrsm_con_ack;
opsrsm_syncdqe *var_opsrsm_syncdqe;
rsmrdt_senderr *var_rsmrdt_senderr;
opsrsm_queue *var_opsrsm_queue;
opsrsm_dest *var_opsrsm_dest;
opsrsm_resource *var_opsrsm_resource;
opsrsmresource_table *var_opsrsmresource_table;
opsrsm_message_header *var_opsrsm_message_header;
opsrsm_failover_info *var_opsrsm_failover_info;
rsmrdt_errmsg *var_rsmrdt_errmsg;
#endif

// Classes from rsmrdt_path_int.h
#include <uts/common/io/rsmrdt/rsmrdt_path_int.h>
path *var_path;
adapter *var_adapter;
adapter_listhead *var_adapter_listhead;
adapter_listhead_list *var_adapter_listhead_list;
