//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cmm.cc	1.5	08/05/20 SMI"

// Files from directory usr/src/common/cl/cmm

#include "dummy.h"

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from automaton_impl.h
#include <cmm/automaton_impl.h>
automaton_impl *var_automaton_impl;
// automaton_impl::node_info_struct *var_node_info_struct;

// Classes from callback_registry_impl.h
#include <cmm/callback_registry_impl.h>
callback_registry_impl *var_callback_registry_impl;

// Classes from cmm_comm_impl.h
#include <cmm/cmm_comm_impl.h>
comm_message *var_comm_message;
sender_info *var_sender_info;
cmm_comm_impl *var_cmm_comm_impl;
cmm_message *var_cmm_message;

// Classes from cmm_impl.h
#include <cmm/cmm_impl.h>
cmm_impl *var_cmm_impl;
#ifdef	_KERNEL_ORB
kernel_ucmm_agent_impl *var_kernel_ucmm_agent_impl;
// kernel_ucmm_agent_impl::cmm_proxy_struct *var_cmm_proxy_struct;
#endif /* _KERNEL_ORB */

// Classes from cmm_ns.h
#include <cmm/cmm_ns.h>
cmm_ns *var_cmm_ns;

// Classes from control_impl.h
#include <cmm/control_impl.h>
control_impl *var_control_impl;

// Classes from ff_callout.h
#include <cmm/ff_callout.h>
ff_callout *var_ff_callout;
ff_callout_table *var_ff_callout_table;

// Classes from ff_impl.h
#include <cmm/ff_impl.h>
ff_impl *var_ff_impl;
ff_admin_impl *var_ff_admin_impl;

// Classes from qd_map.h
#if defined(FAULT_CMM) && defined(_KERNEL_ORB)
#include <quorum/common/qd_map.h>
qd_map *var_qd_map;
// qd_map::map *var_map;
#endif

// Classes from quorum_impl.h
#include <quorum/common/quorum_impl.h>
quorum_algorithm_impl *var_quorum_algorithm_impl;
// quorum_algorithm_impl::quorum_info_struct *var_quorum_info_struct;

// Classes from quorum_pgre.h
#include <quorum/common/quorum_pgre.h>
pgre_sector *var_pgre_sector;
pgre_data *var_pgre_data;

// Classes from ucmm_api.h
#include <cmm/ucmm_api.h>
clust_state *var_clust_state;

// Classes from ucmm_comm_impl.h
#include <cmm/ucmm_comm_impl.h>
cm_comm_impl *var_cm_comm_impl;

// Classes from ucmm_impl.h
#include <cmm/ucmm_impl.h>
ucmm_impl *var_ucmm_impl;
remote_ucmm_proxy_impl *var_remote_ucmm_proxy_impl;
