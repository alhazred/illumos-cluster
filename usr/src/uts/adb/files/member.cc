//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)member.cc	1.8	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/member

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from Node.h
#ifdef _KERNEL_ORB
#include <orb/member/Node.h>
ID_node *var_ID_node;
#endif

// Classes from callback_membership.h
#include <orb/member/callback_membership.h>
callback_membership *var_callback_membership;
lone_membership *var_lone_membership;

// Classes from cmm_callback_impl.h
#include <orb/member/cmm_callback_impl.h>
cmm_callback_impl *var_cmm_callback_impl;
cmm_state *var_cmm_state;
cmm_begin_state *var_cmm_begin_state;
cmm_step_state *var_cmm_step_state;
cmm_step_thread_state *var_cmm_step_thread_state;
cmm_step_thread *var_cmm_step_thread;

// Classes from cmm_callback_steps.h
#include <orb/member/cmm_callback_steps.h>
vm_bootstrap_calc_step *var_vm_bootstrap_calc_step;
vm_bootstrap_commit_step *var_vm_bootstrap_commit_step;
quiesce_step *var_quiesce_step;
resource_reallocation_step *var_resource_reallocation_step;
refcnt_cleanup_step *var_refcnt_cleanup_step;
rxdoor_cleanup_step *var_rxdoor_cleanup_step;
rmm_disqualification_vm_step *var_rmm_disqualification_vm_step;
rm_connect_vm_transmit_step *var_rm_connect_vm_transmit_step;
rm_promote_vm_calc_step *var_rm_promote_vm_calc_step;
rm_service_vm_commit_step *var_rm_service_vm_commit_step;
signal_cbc_rmm_cleanup_step *var_signal_cbc_rmm_cleanup_step;
rma_lca_register_step *var_rma_lca_register_step;
mark_joined_vm_step *var_mark_joined_vm_step;
quiesce_shutdown_step *var_quiesce_shutdown_step;
refcnt_cleanup_shutdown_step *var_refcnt_cleanup_shutdown_step;
rxdoor_cleanup_shutdown_step *var_rxdoor_cleanup_shutdown_step;
mark_joined_shutdown_step *var_mark_joined_shutdown_step;

#ifdef CMM_VERSION_0
rmm_disqualification_step *var_rmm_disqualification_step;
rm_primary_connect_step *var_rm_primary_connect_step;
rm_primary_promote_step *var_rm_primary_promote_step;
rm_service_start_step *var_rm_service_start_step;
rmm_cleanup_step *var_rmm_cleanup_step;
rma_register_step *var_rma_register_step;
mark_joined_step *var_mark_joined_step;
#endif // CMM_VERSION_0

// Classes from members.h
#include <orb/member/members.h>
members *var_members;

#endif /* _KERNEL */
