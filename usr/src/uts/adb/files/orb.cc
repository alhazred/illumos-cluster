//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orb.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/cmd/orb

#include "dummy.h"

#ifndef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from flow_spec.h
#include <cmd/orb/flow_spec.h>
flow_spec *var_flow_spec;

// Classes from orbadmin.h
#include <cmd/orb/orbadmin.h>
orbadmin_tool *var_orbadmin_tool;

// Classes from state_refcount.h
#include <cmd/orb/state_refcount.h>
state_refcount *var_state_refcount;

// Classes from state_resource_balancer.h
#include <cmd/orb/state_resource_balancer.h>
state_resource_balancer *var_state_resource_balancer;

// Classes from state_resource_pool.h
#include <cmd/orb/state_resource_pool.h>
state_resource_pool *var_state_resource_pool;

// Classes from state_unref_threadpool.h
#include <cmd/orb/state_unref_threadpool.h>
state_unref_threadpool *var_state_unref_threadpool;

// Classes from stats_flow.h
#include <cmd/orb/stats_flow.h>
stats_flow *var_stats_flow;

// Classes from stats_msg.h
#include <cmd/orb/stats_msg.h>
stats_msg *var_stats_msg;

// Classes from stats_unref.h
#include <cmd/orb/stats_unref.h>
stats_unref *var_stats_unref;

#endif /* _KERNEL */
