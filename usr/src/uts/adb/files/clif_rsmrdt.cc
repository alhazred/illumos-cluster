//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clif_rsmrdt.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/clif_rsmrdt

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
int clifrsmrdt_modunload_ok;
int clifrsmrdt_shutdown_done;

// Classes from clif_rsmrdt.h
#include <clif_rsmrdt/clif_rsmrdt.h>
clifrsmrdt_path_monitor *var_clifrsmrdt_path_monitor;
