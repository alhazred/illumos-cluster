//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_sys.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/sys

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from cladm_int.h
#include <sys/cladm_int.h>
clcluster_name *var_clcluster_name;
#ifdef _SYSCALL32
clcluster_name32 *var_clcluster_name32;
#endif /* _SYSCALL32 */
clnode_name *var_clnode_name;
#ifdef _SYSCALL32
clnode_name32 *var_clnode_name32;
#endif /* _SYSCALL32 */
cllocal_ns_data *var_cllocal_ns_data;

// Classes from clconf_int.h
#include <sys/clconf_int.h>
clconf_siter *var_clconf_siter;

// Classes from clconf_property.h
#include <sys/clconf_property.h>
clconf_propdesc *var_clconf_propdesc;

// Classes from dbg_printf.h
#ifdef	_KERNEL
#include <sys/dbg_printf.h>
dbg_print_buf *var_dbg_print_buf;
#endif

// Classes from didio.h
#include <sys/didio.h>
did_init *var_did_init;
#ifdef _SYSCALL32
did_init32 *var_did_init32;
#endif /* _SYSCALL32 */
did_init_type *var_did_init_type;
#ifdef _SYSCALL32
did_init_type32 *var_did_init_type32;
#endif /* _SYSCALL32 */
iodidstat *var_iodidstat;
iodidperf *var_iodidperf;
did_err *var_did_err;

// Classes from hashtab_def.h
#include <sys/hashtab_def.h>
_ListHashTable *var__ListHashTable;
_ListHashTable::hash_entry_t *var_hash_entry_t;
_ListHashTable::Iterator *var_Iterator;

// Classes from hashtable.h
#include <sys/hashtable.h>
base_hash_table *var_base_hash_table;
base_hash_table::hash_entry *var_hash_entry;
base_hash_table::iterator *var_iterator;
base_string_hash_table *var_base_string_hash_table;
base_obj_hash_table *var_base_obj_hash_table;

// Classes from heartbeat.h
#include <sys/heartbeat.h>
heartbeat_struc *var_heartbeat_struc;

// Classes from knewdel.h
#include <sys/knewdel.h>
knewdel *var_knewdel;

// Classes from list_def.h
#include <sys/list_def.h>
_DList *var__DList;
_DList::ListElem *var_ListElem;
_SList *var__SList;

// Classes from mc_sema_impl.h
#include <sys/mc_sema_impl.h>
mc_sema_server_impl *var_mc_sema_server_impl;
mc_sema_impl *var_mc_sema_impl;

// Classes from node_order.h
#include <sys/node_order.h>
node_order *var_node_order;

// Classes from nodeset.h
#include <sys/nodeset.h>
nodeset *var_nodeset;

// Classes from os.h
#ifdef	_KERNEL
#include <sys/os.h>
os *var_os;
os::mutex_t *var_mutex_t;
os::rwlock_t *var_rwlock_t;
os::condvar_t *var_condvar_t;
os::sem_t *var_sem_t;
os::notify_t *var_notify_t;
os::tsd *var_tsd;
os::sc_syslog_msg *var_sc_syslog_msg;
os::systime *var_systime;
os::ctype *var_ctype;
#endif

// Classes from perflib.h
#include <sys/perflib.h>
packer *var_packer;

// Classes from quorum_int.h
#include <sys/quorum_int.h>
node_info_struct *var_node_info_struct;
node_info_list *var_node_info_list;
quorum_device_info_struct *var_quorum_device_info_struct;
quorum_device_info_list *var_quorum_device_info_list;
quorum_table_struct *var_quorum_table_struct;
quorum_node_status_struct *var_quorum_node_status_struct;
quorum_device_status_struct *var_quorum_device_status_struct;
quorum_status_struct *var_quorum_status_struct;

// Classes from refcnt.h
#include <sys/refcnt.h>
refcnt *var_refcnt;

// Classes from rm_util.h
#include <sys/rm_util.h>
rm_util *var_rm_util;

// Classes from rnw.h
#include <sys/rnw.h>
rnw *var_rnw;

// Classes from threadpool.h
#include <sys/threadpool.h>
defer_task *var_defer_task;
transfer_task *var_transfer_task;
fence_task *var_fence_task;
work_task *var_work_task;
threadpool_worker_t *var_threadpool_worker_t;
threadpool *var_threadpool;
