//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)repl_sample.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orbtest/repl_sample

#include "dummy.h"

#ifdef	_FAULT_INJECTION
#ifdef	_KERNEL_ORB

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from repl_sample_impl.h
#include <orbtest/repl_sample/repl_sample_impl.h>
value_inc_state *var_value_inc_state;
persist_inc_state *var_persist_inc_state;
prov_in_mod *var_prov_in_mod;
repl_sample_prov *var_repl_sample_prov;
repl_value_obj_impl *var_repl_value_obj_impl;
persistent_store_impl *var_persistent_store_impl;
repl_persist_obj_impl *var_repl_persist_obj_impl;
factory_impl *var_factory_impl;

#endif /* _KERNEL_ORB */
#endif /* _FAULT_INJECTION */
