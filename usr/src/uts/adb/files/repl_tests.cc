//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)repl_tests.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orbtest/repl_tests

#include "dummy.h"

#ifdef	_FAULT_INJECTION
#ifdef	_KERNEL_ORB

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from repl_test_impl.h
#include <orbtest/repl_tests/repl_test_impl.h>
trans_state_put *var_trans_state_put;
test_server *var_test_server;
test_obj_repl_impl *var_test_obj_repl_impl;
test_mgr_repl_impl *var_test_mgr_repl_impl;
test_data *var_test_data;

// Classes from repl_test_switch.h
#include <orbtest/repl_tests/repl_test_switch.h>
opt_t *var_opt_t;

#endif /* _KERNEL_ORB */
#endif /* _FAULT_INJECTION */
