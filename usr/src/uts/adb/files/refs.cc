//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)refs.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/refs

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from refcount.h
#include <orb/refs/refcount.h>
refcnt_control *var_refcnt_control;
translate_ack *var_translate_ack;
nodejobs *var_nodejobs;
refcount *var_refcount;

// Classes from unref_stats.h
#include <orb/refs/unref_stats.h>
unref_stats *var_unref_stats;

// Classes from unref_threadpool.h
#include <orb/refs/unref_threadpool.h>
unref_task *var_unref_task;
unref_threadpool *var_unref_threadpool;

#endif /* _KERNEL */
