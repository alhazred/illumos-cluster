/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _ENERGIZER_H
#define	_ENERGIZER_H

#pragma ident	"@(#)energizer.h	1.8	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <errno.h>

#include <syslog.h>
#include <libintl.h>
#include <locale.h>

#include <limits.h>

#ifndef PATH_MAX
#define	PATH_MAX 	_POSIX_PATH_MAX
#endif

#define	MAXPORTLEN	10

#define	MAX_MESSAGE_ID_LENGTH	64
#define	MAX_MESSAGE_EXTENTION	16

typedef struct serialport {
	char host[PATH_MAX];	/* host to connect to */
	char tcnam[PATH_MAX];	/* terminal concentrator */
	char portnum[MAXPORTLEN];	/* port number on tc */
} sport_t;

typedef struct cluster {
	char **clusts; 	/* cluster names */
	char **hosts;	/* host names */
} clust_t;

typedef struct clusterEntry {
	char *clustername;	/* cluster name */
	char **hostnames;	/* host names */
	struct clusterEntry *next;
} clusterEntry_t;


/*
 * Put a formated error message into syslog
 */
void
log_error(
	int   syslog_priority,
	char *messageID,
	char *fmt,
	...
);

/*
 * Put a formated error message onto the console
 */
void
show_error(
	int   syslog_priority,
	char *messageID,
	char *fmt,
	...
);


/*
 * Get the extended error information (error numbers, to-fix info, etc)
 * for a given messageID
 */
int
more_error_info(
	const char *messageID,
	char *verbose_message, int vm_length,
	char *verbose_error,   int ve_length,
	char *verbose_fix, 	int vf_length
);


/*
 * Get the name of the terminal concentrator and port number for the
 * host. A proper entry will have the tuple <host>, <tc>, <port>, where
 * <host> is the name of the host to connect to, <tc> is the terminal
 * concentrator for that host, and <port> is the port number on <tc>
 * associated with <host>.
 */

extern sport_t *
getsportbyhost(char *host);

/*
 * Return a list of hosts associated with the specified cluster(s). We assume
 * that an input name that can't be located as a cluster must be a host.
 */

extern char **
gethostsbyclusts(char **clusters);

/*
 * Return a list of all clusters.
 */

extern clusterEntry_t *getallclusters(void);

/*
 * Tracing versions - also return the databases we searched.
 * NOT MT-SAFE
 */
extern sport_t *
getsportbyhost_t(char *host, char **srch_path);

extern char **
gethostsbyclusts_t(char **clusters, char **srch_path);
#ifdef __cplusplus
}
#endif

#endif /* _ENERGIZER_H */
