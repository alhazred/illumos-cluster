//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ident	"@(#)vm_vm.cc	1.3	08/05/20 SMI"

// Files from usr/src/common/cl/vm that need to define NOINLINES for macros.

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#define	NOINLINES

// Classes from vm_stream.h
#include <vm/vm_stream.h>
vm_stream *var_vm_stream;

// Classes from vp_constraint.h
#include <vm/vp_constraint.h>
vp_constraint *var_vp_constraint;

#endif /* _KERNEL */
