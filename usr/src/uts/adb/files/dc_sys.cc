//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_sys.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/common/cl/dc/sys

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from dc_contig_chunk.h
#include <dc/sys/dc_contig_chunk.h>
dc_contig_chunk *var_dc_contig_chunk;

// Classes from dc_key_alloc.h
#include <dc/sys/dc_key_alloc.h>
dc_key_alloc *var_dc_key_alloc;

// Classes from dc_lib.h
#include <dc/sys/dc_lib.h>
dclib *var_dclib;

// Classes from dc_num_alloc.h
#include <dc/sys/dc_num_alloc.h>
dc_num_alloc *var_dc_num_alloc;

#endif /* _KERNEL */
