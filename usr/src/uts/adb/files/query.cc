//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)query.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/query

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from adapter_state_proxy.h
#include <orb/query/adapter_state_proxy.h>
adapter_state_proxy *var_adapter_state_proxy;

// Classes from pathend_state_proxy.h
#include <orb/query/pathend_state_proxy.h>
pathend_state_proxy *var_pathend_state_proxy;
