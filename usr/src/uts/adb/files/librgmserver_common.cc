//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)librgmserver_common.cc	1.5	08/05/20 SMI"

// Files from directory usr/src/lib/librgmserver/common

#include "dummy.h"

#ifndef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#include <lib/librgmserver/common/rgm_state.h>

// Classes from cm_callback_impl.h
#include <lib/librgmserver/common/cm_callback_impl.h>
cm_callback_impl *var_cm_callback_impl;

// Classes from rgm_global_res_used.h
#include <lib/librgmserver/common/rgm_global_res_used.h>
service_state_callback_impl *var_service_state_callback_impl;

// Classes from rgm_state.h
#include <lib/librgmserver/common/rgm_state.h>
rgm_timelist *var_rgm_timelist;
rg_xstate *var_rg_xstate;
r_xstate *var_r_xstate;
rlist *var_rlist;
methodinvoc *var_methodinvoc;
rglist *var_rglist;
rgm_state *var_rgm_state;
launchargs *var_launchargs;
launchvalargs *var_launchvalargs;

// Classes from rgm_threads.h
#include <lib/librgmserver/common/rgm_threads.h>
// new_str is declared in .h file above, so we redefine it after all #includes
#define	new_str
state_machine_task *var_state_machine_task;
state_machine_threadpool *var_state_machine_threadpool;
notify_state_change_task *var_notify_state_change_task;
notify_state_change_threadpool *var_notify_state_change_threadpool;
failback_task *var_failback_task;
failback_threadpool *var_failback_threadpool;
launch_method_task *var_launch_method_task;
launch_method_threadpool *var_launch_method_threadpool;

// Classes from rgmd_util.h
#include <lib/librgmserver/common/rgmd_util.h>
rgstatename *var_rgstatename;
rstatename *var_rstatename;
rfmstatusname *var_rfmstatusname;
updatable_prop *var_updatable_prop;
changed_rg_prop *var_changed_rg_prop;

#endif /* _KERNEL */
