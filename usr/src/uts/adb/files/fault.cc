//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)fault.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/fault

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from fault_flowcontrol.h
#if defined(_FAULT_INJECTION)
#include <orb/fault/fault_flowcontrol.h>
fault_flowcontrol *var_fault_flowcontrol;
fault_flowcontrol::flowcontrol_arg_t *var_flowcontrol_arg_t;
#endif

// Classes from fault_functions.h
#if defined(_FAULT_INJECTION)
#include <orb/fault/fault_functions.h>
FaultFunctions *var_FaultFunctions;
FaultFunctions::counter_t *var_counter_t;
FaultFunctions::generic_arg_t *var_generic_arg_t;
FaultFunctions::wait_for_arg_t *var_wait_for_arg_t;
FaultFunctions::cmm_reboot_arg_t *var_cmm_reboot_arg_t;
FaultFunctions::ha_dep_reboot_arg_t *var_ha_dep_reboot_arg_t;
FaultFunctions::svc_reboot_arg_t *var_svc_reboot_arg_t;
FaultFunctions::mc_sema_arg_t *var_mc_sema_arg_t;
FaultFunctions::sys_exception_arg_t *var_sys_exception_arg_t;
FaultFunctions::switchover_arg_t *var_switchover_arg_t;
FaultFunctions::failpath_t *var_failpath_t;
#endif

// Classes from fault_injection.h
#if defined(_FAULT_INJECTION)
#include <orb/fault/fault_injection.h>
FaultTriggers *var_FaultTriggers;
// FaultTriggers::trigger_t *var_trigger_t;
InvoTriggers *var_InvoTriggers;
InvoTriggers::fi_header_t *var_fi_header_t;
NodeTriggers *var_NodeTriggers;
// NodeTriggers::_cl_get_nodetrig_t *var__cl_get_nodetrig_t;
BootTriggers *var_BootTriggers;
// BootTriggers::_fault_header_t *var__fault_header_t;
#endif

// Classes from fault_rpc.h
#if defined(_FAULT_INJECTION)
#include <orb/fault/fault_rpc.h>
fault_rpc *var_fault_rpc;
fault_rpc::cladm_header_t *var_cladm_header_t;
fault_rpc::fault_header_t *var_fault_header_t;
#endif

// Classes from fault_unref.h
#if defined(_FAULT_INJECTION)
#include <orb/fault/fault_unref.h>
fault_unref *var_fault_unref;
fault_unref::unref_arg_t *var_unref_arg_t;
#endif
