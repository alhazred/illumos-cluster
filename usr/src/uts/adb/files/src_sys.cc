//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)src_sys.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/sys

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from scha_lb.h
#include <sys/scha_lb.h>
dist_info *var_dist_info;
weight_info *var_weight_info;
perf_mon_parameters *var_perf_mon_parameters;

// Classes from scha_types.h
#include <sys/scha_types.h>
scha_str_array *var_scha_str_array;
scha_uint_array *var_scha_uint_array;
scha_status_value *var_scha_status_value;
scha_extprop_value *var_scha_extprop_value;
