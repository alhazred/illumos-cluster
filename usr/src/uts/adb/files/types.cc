//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)types.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orbtest/types

#include "dummy.h"

#ifdef	_FAULT_INJECTION

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from client_impl.h
#include <orbtest/types/client_impl.h>
client_impl *var_client_impl;

// Classes from impl_common.h
#if defined(_KERNEL_ORB) && !defined(_KERNEL)
#include <orbtest/types/impl_common.h>
impl_common *var_impl_common;
Obj_impl *var_Obj_impl;
InhObjA_impl *var_InhObjA_impl;
InhObjB_impl *var_InhObjB_impl;
InhObjC_impl *var_InhObjC_impl;
InhObjD_impl *var_InhObjD_impl;
InhObjE_impl *var_InhObjE_impl;
#endif

// Classes from server_impl.h
#include <orbtest/types/server_impl.h>
server_impl *var_server_impl;

#endif /* _FAULT_INJECTION */
