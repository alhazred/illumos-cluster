//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_net.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/common/cl/cl_net

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/list_def.h>
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#include <repl/service/replica_tmpl.h>
#include <cl_net/netlib.h>
typedef uint32_t xdoor_id;

// Classes from bind_table.h
#include <cl_net/bind_table.h>
bind_event *var_bind_event;
gif_bind_elem_t *var_gif_bind_elem_t;
gif_bind_table_t *var_gif_bind_table_t;
closeconn_timer *var_closeconn_timer;
bind_elem_t *var_bind_elem_t;
proxy_bind_table_t *var_proxy_bind_table_t;
closeconn_timer_manager *var_closeconn_timer_manager;

// Classes from lbobj.h
#include <cl_net/lbobj.h>
lbobj *var_lbobj;
lbobj_set_weight_state *var_lbobj_set_weight_state;

// Classes from ma_pdt.h
#include <cl_net/ma_pdt.h>
ma_pdt *var_ma_pdt;

// Classes from ma_service.h
#include <cl_net/ma_service.h>
ma_servicegrp *var_ma_servicegrp;
ma_serviceobj *var_ma_serviceobj;

// Classes from mcnet_node_impl.h
#include <cl_net/mcnet_node_impl.h>
nodes_cleanup *var_nodes_cleanup;

// Classes from mcobj_impl.h
#include <cl_net/mcobj_impl.h>
close_conn *var_close_conn;
ourproto_ipaddr_t *var_ourproto_ipaddr_t;
mcobj_impl_t *var_mcobj_impl_t;

// Classes from muxq_impl.h
#ifdef _KERNEL
#ifdef _KERNEL
#ifdef _KERNEL
#include <cl_net/muxq_impl.h>
fragment *var_fragment;
muxq_impl_t *var_muxq_impl_t;
#endif
#endif
#endif

// Classes from netlib.h
#include <cl_net/netlib.h>
netlib *var_netlib;
pdte_hash_t *var_pdte_hash_t;
pdt *var_pdt;
frwd_cid *var_frwd_cid;
frwd_info_t *var_frwd_info_t;
pdte_frwd_t *var_pdte_frwd_t;
servicegrp *var_servicegrp;
serviceobj *var_serviceobj;
instanceobj *var_instanceobj;

// Classes from pdts_impl.h
#include <cl_net/pdts_impl.h>
register_gifnode_state *var_register_gifnode_state;
register_pxqobj_state *var_register_pxqobj_state;
attach_mcobj_state *var_attach_mcobj_state;
deregister_inst_state *var_deregister_inst_state;
register_inst_state *var_register_inst_state;
set_primary_gifnode_state *var_set_primary_gifnode_state;
attach_gifnode_state *var_attach_gifnode_state;
add_scalable_service_state *var_add_scalable_service_state;
add_nodeid_state *var_add_nodeid_state;
remove_nodeid_state *var_remove_nodeid_state;
rem_scal_service_state *var_rem_scal_service_state;
create_scal_srv_grp_state *var_create_scal_srv_grp_state;
config_sticky_srv_grp_state *var_config_sticky_srv_grp_state;
delete_scal_srv_grp_state *var_delete_scal_srv_grp_state;

// Classes from pdts_prov_impl.h
#include <cl_net/pdts_prov_impl.h>
pdts_prov_impl_t *var_pdts_prov_impl_t;

// Classes from sl_pdt.h
#include <cl_net/sl_pdt.h>
sl_pdt *var_sl_pdt;

// Classes from sl_service.h
#include <cl_net/sl_service.h>
sl_servicegrp_t *var_sl_servicegrp_t;
sl_serviceobj_t *var_sl_serviceobj_t;

#endif /* _KERNEL */
