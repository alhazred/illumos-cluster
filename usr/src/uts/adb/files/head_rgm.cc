//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)head_rgm.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/head/rgm

#include "dummy.h"

#ifndef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from ff.h
#include <head/rgm/ff.h>
ff_name *var_ff_name;

// Classes from haip.h
#include <head/rgm/haip.h>

// Classes from libdsdev.h
#include <head/rgm/libdsdev.h>
scds_net_resource *var_scds_net_resource;
scds_net_resource_list *var_scds_net_resource_list;
scds_port *var_scds_port;
scds_port_list *var_scds_port_list;
scds_netaddr *var_scds_netaddr;
scds_netaddr_list *var_scds_netaddr_list;

// Classes from pnm.h
#include <head/rgm/pnm.h>
pnm_status *var_pnm_status;
pnm_callback *var_pnm_callback;
pnm_adapterinfo *var_pnm_adapterinfo;
pnm_adp_status *var_pnm_adp_status;
pnm_grp_status *var_pnm_grp_status;
pnm_grp *var_pnm_grp;
pnm_ip_check_grplist *var_pnm_ip_check_grplist;

// Classes from rgm_comm_impl.h
#include <head/rgm/rgm_comm_impl.h>
rgm_comm_impl *var_rgm_comm_impl;

// Classes from rgm_common.h
#include <head/rgm/rgm_common.h>
namelist *var_namelist;
namelist_all *var_namelist_all;
rgm_methods *var_rgm_methods;
rgm_param *var_rgm_param;
rgm_dependencies *var_rgm_dependencies;
rgm_rt_upgrade *var_rgm_rt_upgrade;
rgm_rt *var_rgm_rt;
rgm_property *var_rgm_property;
rgm_property_list *var_rgm_property_list;
rgm_resource *var_rgm_resource;
rgm_rg *var_rgm_rg;
scha_property *var_scha_property;
scha_properties *var_scha_properties;

// Classes from scha_priv.h
#include <head/rgm/scha_priv.h>
scha_errmsg *var_scha_errmsg;

#endif /* _KERNEL */
