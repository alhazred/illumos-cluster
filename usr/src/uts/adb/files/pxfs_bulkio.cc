//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfs_bulkio.cc	1.8	08/05/20 SMI"

// Files from directory usr/src/common/cl/pxfs/bulkio

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from bulkio_handler.h
#include <pxfs/bulkio/bulkio_handler.h>

bulkio_handler_kit *var_bulkio_handler_kit;
bulkio_handler *var_bulkio_handler;
bulkio_handler_inuio *var_bulkio_handler_inuio;
bulkio_handler_inoutuio *var_bulkio_handler_inoutuio;
bulkio_handler_inaio *var_bulkio_handler_inaio;
bulkio_handler_inoutuiowrite *var_bulkio_handler_inoutuiowrite;
bulkio_handler_inoutaiowrite *var_bulkio_handler_inoutaiowrite;
bulkio_handler_inoutpageswrite *var_bulkio_handler_inoutpageswrite;
bulkio_handler_in_pages *var_bulkio_handler_in_pages;
bulkio_handler_inout_pages *var_bulkio_handler_inout_pages;
bulkio_handler_in_aio_pages *var_bulkio_handler_in_aio_pages;

// Classes from bulkio_holder_impl.h
#include <pxfs/bulkio/bulkio_holder_impl.h>
holder_impl *var_holder_impl;

// Classes from bulkio_impl.h
#include <pxfs/bulkio/bulkio_impl.h>
bulkio_impl *var_bulkio_impl;
bulkio_impl::exception *var_exception;
bulkio_impl::pgio_vn *var_pgio_vn;
bulkio_impl::in_uio *var_in_uio;
bulkio_impl::in_aio *var_in_aio;
bulkio_impl::uio_write_obj *var_uio_write_obj;
bulkio_impl::pages_write_obj *var_pages_write_obj;
bulkio_impl::aio_write_obj *var_aio_write_obj;
bulkio_impl::uio_clnt_cmn *var_uio_clnt_cmn;
bulkio_impl::uio_srvr_cmn *var_uio_srvr_cmn;
bulkio_impl::in_uio_clnt *var_in_uio_clnt;
bulkio_impl::in_uio_srvr *var_in_uio_srvr;
bulkio_impl::in_aio_clnt *var_in_aio_clnt;
bulkio_impl::in_aio_srvr *var_in_aio_srvr;
bulkio_impl::inout_uio_clnt *var_inout_uio_clnt;
bulkio_impl::inout_uio_srvr *var_inout_uio_srvr;
bulkio_impl::uio_write_obj_clnt *var_uio_write_obj_clnt;
bulkio_impl::uio_write_obj_srvr *var_uio_write_obj_srvr;
bulkio_impl::aio_write_obj_clnt *var_aio_write_obj_clnt;
bulkio_impl::aio_write_obj_srvr *var_aio_write_obj_srvr;
bulkio_impl::pages_write_obj_clnt *var_pages_write_obj_clnt;
bulkio_impl::pages_write_obj_srvr *var_pages_write_obj_srvr;
bulkio_impl::in_pages_clnt *var_in_pages_clnt;
bulkio_impl::in_pages_srvr *var_in_pages_srvr;
bulkio_impl::inout_pages_clnt *var_inout_pages_clnt;
bulkio_impl::inout_pages_srvr *var_inout_pages_srvr;
bulkio_impl::in_aio_pages_clnt *var_in_aio_pages_clnt;
bulkio_impl::in_aio_pages_srvr *var_in_aio_pages_srvr;

#endif /* _KERNEL */
