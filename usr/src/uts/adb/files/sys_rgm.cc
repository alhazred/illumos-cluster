//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)sys_rgm.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/sys/rgm

#include "dummy.h"

#ifndef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from fecl.h
#include <sys/rgm/fecl.h>
fe_cmd *var_fe_cmd;

// Classes from rtreg.h
#include <sys/rgm/rtreg.h>
RtrString *var_RtrString;
Rtr_T *var_Rtr_T;
RtrUpg_T *var_RtrUpg_T;
RtrParam_T *var_RtrParam_T;
RtrFileResult *var_RtrFileResult;

#endif /* _KERNEL */
