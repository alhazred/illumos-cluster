#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)merge.ksh	1.7	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# uts/adb/files/merge.ksh
#
# The purpose of this file is to merge the adb macros
# in the current working directory with the common set
# of adb macros in the parent directory.
#
# The optional first argument specifies a suffix
# that is appended to the name of each adb macro.
# This is useful for differentiating different versions.
#
GREP=/usr/bin/grep

#
# Process each adb macro
#
for adb_file in `ls . | \
    ${GREP} -v "\.txt" | \
    ${GREP} -v Makefile | \
    ${GREP} -v .make.state | \
    ${GREP} -v SCCS | \
    ${GREP} -v manifest `
	do
	#
	# Move the adb macro to the parent directory
	#
	mv ${adb_file} ../${adb_file}
	mv ${adb_file}.txt ../${adb_file}.txt
	#
	# Add the file names to the manifest in the parent directory
	#
	echo "./${adb_file}" >> ../manifest
	echo "./${adb_file}.txt" >> ../manifest
	done
