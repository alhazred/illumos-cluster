//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dev_tests.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orbtest/dev_tests

#include "dummy.h"

#ifdef	_FAULT_INJECTION

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from test_mgr_impl.h
#include <orbtest/dev_tests/test_mgr_impl.h>
test_obj_impl1 *var_test_obj_impl1;
test_mgr_impl *var_test_mgr_impl;

// Classes from test_obj_impl.h
#include <orbtest/dev_tests/test_obj_impl.h>
test_obj_impl *var_test_obj_impl;

// Classes from test_obj_revoke.h
#include <orbtest/dev_tests/test_obj_revoke.h>
test_obj_impl2 *var_test_obj_impl2;

// Classes from test_params_impl.h
#include <orbtest/dev_tests/test_params_impl.h>
test_params_impl *var_test_params_impl;

#endif /* _FAULT_INJECTION */
