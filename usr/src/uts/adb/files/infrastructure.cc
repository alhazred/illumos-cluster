//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)infrastructure.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/infrastructure

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from addrspace_impl.h
#include <orb/infrastructure/addrspace_impl.h>
addrspace_impl *var_addrspace_impl;

// Classes from common_threadpool.h
#include <orb/infrastructure/common_threadpool.h>
common_threadpool *var_common_threadpool;

// Classes from copy.h
#include <orb/infrastructure/copy.h>
copy_args *var_copy_args;
copy *var_copy;

// Classes from fork.h
#include <orb/infrastructure/fork.h>
pthread_atfork_handler *var_pthread_atfork_handler;

// Classes from freeze_lock.h
#include <orb/infrastructure/freeze_lock.h>
freeze_lock *var_freeze_lock;

// Classes from orb.h
#include <orb/infrastructure/orb.h>
ORB *var_ORB;

// Classes from orb_conf.h
#ifdef _KERNEL_ORB
#include <orb/infrastructure/orb_conf.h>
orb_conf *var_orb_conf;
#endif

// Classes from orbthreads.h
#include <orb/infrastructure/orbthreads.h>
orbthread *var_orbthread;
orbthreadlist *var_orbthreadlist;

// Classes from sigs.h
#include <orb/infrastructure/sigs.h>
signals *var_signals;

#endif /* _KERNEL */
