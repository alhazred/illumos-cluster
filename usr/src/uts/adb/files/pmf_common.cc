//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pmf_common.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/cmd/pmf/common

#include "dummy.h"

#ifndef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#if DOOR_IMPL
#include <head/rgm/pmf.h>
#else
#include <head/rgm/rpc/pmf.h>
#endif

// Classes from pmfd.h
#include <cmd/pmf/common/pmfd_procfs.h>
#include <cmd/pmf/common/pmfd.h>
pmf_handle *var_pmf_handle;
pmf_process_tag *var_pmf_process_tag;
pmf_threads *var_pmf_threads;

#endif /* _KERNEL */
