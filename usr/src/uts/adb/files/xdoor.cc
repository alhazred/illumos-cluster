//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)xdoor.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/xdoor

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Class from door_header.h : present in kernel and userland
#include <orb/xdoor/door_header.h>
door_msg_hdr_t	*var_door_msg_hdr_t;

// Classes from Xdoor.h
#ifdef _KERNEL_ORB
#include <orb/xdoor/Xdoor.h>
tr_out_info *var_tr_out_info;
Xdoor_manager *var_Xdoor_manager;
Xdoor *var_Xdoor;
#endif

// Classes from rxdoor.h
#include <orb/xdoor/rxdoor.h>
rxdoor_id *var_rxdoor_id;
rxdoor_bucket *var_rxdoor_bucket;
rxdoor_node *var_rxdoor_node;
notify_membership_change *var_notify_membership_change;
rxdoor_membership_manager *var_rxdoor_membership_manager;
rxdoor *var_rxdoor;

// Classes from rxdoor_defs.h
#include <orb/xdoor/rxdoor_defs.h>
rxdoor_descriptor *var_rxdoor_descriptor;
rxdoor_ref *var_rxdoor_ref;

// Classes from rxdoor_header.h
#include <orb/xdoor/rxdoor_header.h>
rxdoor_invo_header *var_rxdoor_invo_header;
rxdoor_oneway_header *var_rxdoor_oneway_header;
rxdoor_twoway_header *var_rxdoor_twoway_header;
rxdoor_reply_header *var_rxdoor_reply_header;

// Classes from rxdoor_kit.h
#include <orb/xdoor/rxdoor_kit.h>
rxdoor_kit *var_rxdoor_kit;
rxdoor_to_server_kit *var_rxdoor_to_server_kit;
rxdoor_to_client_kit *var_rxdoor_to_client_kit;
rxdoor_from_server_kit *var_rxdoor_from_server_kit;

// Classes from rxdoor_mgr.h
#include <orb/xdoor/rxdoor_mgr.h>
rxdoor_hash_table *var_rxdoor_hash_table;
rxdoor_manager *var_rxdoor_manager;

// Classes from simple_xdoor.h
#include <orb/xdoor/simple_xdoor.h>
simple_xdoor_kit *var_simple_xdoor_kit;
simple_xdoor_to_client_kit *var_simple_xdoor_to_client_kit;
simple_xdoor_from_server_kit *var_simple_xdoor_from_server_kit;
simple_xdoor_to_server_kit *var_simple_xdoor_to_server_kit;
simple_xdoor *var_simple_xdoor;
simple_xdoor_server *var_simple_xdoor_server;
simple_xdoor_client *var_simple_xdoor_client;

// Classes from solaris_xdoor.h
#ifndef _KERNEL_ORB
#include <orb/xdoor/solaris_xdoor.h>
solaris_xdoor_kit *var_solaris_xdoor_kit;
solaris_xdoor *var_solaris_xdoor;
#endif /* _KERNEL_ORB */

// Classes from standard_xdoor.h
#include <orb/xdoor/standard_xdoor.h>
standard_xdoor_kit *var_standard_xdoor_kit;
standard_xdoor_to_server_kit *var_standard_xdoor_to_server_kit;
standard_xdoor_to_client_kit *var_standard_xdoor_to_client_kit;
standard_xdoor_from_server_kit *var_standard_xdoor_from_server_kit;
standard_xdoor *var_standard_xdoor;
standard_xdoor_server *var_standard_xdoor_server;
standard_xdoor_client *var_standard_xdoor_client;

// Classes from sxdoor.h
#ifndef _KERNEL_ORB
#include <orb/xdoor/sxdoor.h>
sxdoor_table *var_sxdoor_table;
sxdoor_manager *var_sxdoor_manager;
#endif /* _KERNEL_ORB */

// Classes from translate_mgr.h
#include <orb/xdoor/translate_mgr.h>
translate_mgr *var_translate_mgr;
// translate_mgr::tr_node *var_tr_node;

// Classes from xdoor_kit.h
#ifdef _KERNEL_ORB
#include <orb/xdoor/xdoor_kit.h>
xdoor_kit *var_xdoor_kit;
xdoor_repository *var_xdoor_repository;
#endif

#endif /* _KERNEL */
