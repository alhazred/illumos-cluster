//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)transport.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/transport

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from endpoint_registry.h
#include <orb/transport/endpoint_registry.h>
er_if *var_er_if;
endpoint_registry *var_endpoint_registry;
// endpoint_registry::ninfo_t *var_ninfo_t;

// Classes from hb_threadpool.h
#include <orb/transport/hb_threadpool.h>
hb_threadpool *var_hb_threadpool;

// Classes from nil_endpoint.h
#include <orb/transport/nil_endpoint.h>
nil_recstream *var_nil_recstream;
nil_sendstream *var_nil_sendstream;
nil_endpoint *var_nil_endpoint;

// Classes from path_manager.h
#ifdef	_KERNEL
#if defined(_KERNEL)
#include <orb/transport/path_manager.h>
pm_if *var_pm_if;
path_record *var_path_record;
path_group *var_path_group;
adapter_group *var_adapter_group;
path_manager *var_path_manager;
#endif
#endif

// Classes from pm_client.h
#include <orb/transport/pm_client.h>
pm_client *var_pm_client;

// Classes from sdoor_transport.h
#include <orb/transport/sdoor_transport.h>
DoorOverflowBuf *var_DoorOverflowBuf;
sdoor_sendstream *var_sdoor_sendstream;
sdoor_recstream *var_sdoor_recstream;
sdoor_transport *var_sdoor_transport;

// Classes from tick_stat.h
#ifdef	_KERNEL_ORB	// unode or kernel
#include <orb/transport/tick_stat.h>
tick_stat *var_tick_stat;
#endif

// Classes from transport.h
#ifdef	_KERNEL		// don't check for sendthread running in unode mode
#include <orb/transport/transport.h>
pathend_defer_task *var_pathend_defer_task;
pm_send_defer_task *var_pm_send_defer_task;
get_pathend_refcnt *var_get_pathend_refcnt;
pathend *var_pathend;
endpoint_defer_task *var_endpoint_defer_task;
endpoint_delete_defer_task *var_endpoint_delete_defer_task;
endpoint *var_endpoint;
adapter *var_adapter;
transport *var_transport;
#endif

#endif /* _KERNEL */
