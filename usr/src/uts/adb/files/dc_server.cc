//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)dc_server.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/dc/server

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#define	DEF_IOV_MAX 16

// Classes from dc_config_impl.h
#include <dc/server/dc_config_impl.h>
reservation_lock *var_reservation_lock;
dc_config_impl *var_dc_config_impl;
dc_config_impl::dsm_info_t *var_dsm_info_t;

// Classes from dc_major_store.h
#include <dc/server/dc_major_store.h>
dc_minor_range *var_dc_minor_range;
dc_major_store *var_dc_major_store;

// Classes from dc_mapper_impl.h
#include <dc/server/dc_mapper_impl.h>
dc_mapper_impl *var_dc_mapper_impl;

// Classes from dc_repl_server.h
#include <dc/server/dc_repl_server.h>
dc_repl_server *var_dc_repl_server;

// Classes from dc_service.h
#include <dc/server/dc_service.h>
dc_service_class *var_dc_service_class;
dc_callback_info *var_dc_callback_info;
dc_service *var_dc_service;

// Classes from dc_services.h
#include <dc/server/dc_services.h>
dc_services *var_dc_services;

// Classes from dc_storage.h
#include <dc/server/dc_storage.h>
dc_storage *var_dc_storage;

// Classes from dc_trans_states.h
#include <dc/server/dc_trans_states.h>
global_minor_state *var_global_minor_state;
retry_state *var_retry_state;

#endif /* _KERNEL */
