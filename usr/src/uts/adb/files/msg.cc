//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)msg.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/msg

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from message_mgr.h
#include <orb/msg/message_mgr.h>
msg_mgr *var_msg_mgr;
send_msg_mgr *var_send_msg_mgr;
recv_msg_mgr *var_recv_msg_mgr;
msgmgrref *var_msgmgrref;
message_mgr_slot *var_message_mgr_slot;
node_msg_mgr *var_node_msg_mgr;
message_mgr *var_message_mgr;

// Classes from msg_stats.h
#include <orb/msg/msg_stats.h>
msg_stats *var_msg_stats;
cl_msg_stats *var_cl_msg_stats;

// Classes from orb_msg.h
#include <orb/msg/orb_msg.h>
orb_msg *var_orb_msg;

#endif /* _KERNEL */
