//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfs_idl.cc	1.7	08/05/20 SMI"

// Files from directory usr/src/common/cl/interfaces/64/h

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from pxfs_v1.h
#include <h/pxfs_v1.h>
pxfs_v1 *var_pxfs_v1;
pxfs_v1::blocked *var_blocked;
pxfs_v1::invalid_cb *var_invalid_cb;
pxfs_v1::secattr *var_secattr;
pxfs_v1::fs_info *var_fs_info;
pxfs_v1::fobj_fid_t *var_fobj_fid_t;
pxfs_v1::fobj_info *var_fobj_info;
pxfs_v1::pvnode *var_pvnode;
pxfs_v1::bind_fobj_info *var_bind_fobj_info;
pxfs_v1::bind_info *var_bind_info;
pxfs_v1::recovery_info *var_recovery_info;
pxfs_v1::fobj *var_fobj;
pxfs_v1::fobj_stub *var_fobj_stub;
pxfs_v1::fobjplus *var_fobjplus;
pxfs_v1::fobjplus_stub *var_fobjplus_stub;
pxfs_v1::special *var_special;
pxfs_v1::special_stub *var_special_stub;
pxfs_v1::io *var_io;
pxfs_v1::io_stub *var_io_stub;
pxfs_v1::file *var_file;
pxfs_v1::file_stub *var_file_stub;
pxfs_v1::unixdir *var_unixdir;
pxfs_v1::unixdir::direntry_t *var_direntry_t;
pxfs_v1::unixdir_stub *var_unixdir_stub;
pxfs_v1::symbolic_link *var_symbolic_link;
pxfs_v1::symbolic_link_stub *var_symbolic_link_stub;
pxfs_v1::fsmgr_client *var_fsmgr_client;
pxfs_v1::fsmgr_client_stub *var_fsmgr_client_stub;
pxfs_v1::fsmgr_server *var_fsmgr_server;
pxfs_v1::fsmgr_server_stub *var_fsmgr_server_stub;
pxfs_v1::filesystem *var_filesystem;
pxfs_v1::filesystem_stub *var_filesystem_stub;
pxfs_v1::fobj_client *var_fobj_client;
pxfs_v1::fobj_client_stub *var_fobj_client_stub;
pxfs_v1::pxfs_llm_callback *var_pxfs_llm_callback;

// Classes from repl_pxfs_v1.h
#include <h/repl_pxfs_v1.h>
repl_pxfs_v1 *var_repl_pxfs_v1;
repl_pxfs_v1::lock_info_t *var_lock_info_t;
repl_pxfs_v1::fs_replica *var_fs_replica;
repl_pxfs_v1::fs_replica_stub *var_fs_replica_stub;
