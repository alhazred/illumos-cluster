//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)nslib.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/nslib

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from binding_iter_handler.h
#include <nslib/binding_iter_handler.h>
binding_iter_handler_kit *var_binding_iter_handler_kit;
binding_iter_handler *var_binding_iter_handler;

// Classes from binding_iter_impl.h
#include <nslib/binding_iter_impl.h>
binding_iterator_impl *var_binding_iterator_impl;

// Classes from data_container_handler.h
#include <nslib/data_container_handler.h>
data_container_handler_kit *var_data_container_handler_kit;
data_container_handler *var_data_container_handler;

// Classes from data_container_impl.h
#include <nslib/data_container_impl.h>
data_container_impl *var_data_container_impl;

// Classes from naming_context_impl.h
#include <nslib/naming_context_impl.h>
naming_context_ii *var_naming_context_ii;
naming_context_repl_impl *var_naming_context_repl_impl;
dump_info *var_dump_info;
dump_info::unbnd_ctx *var_unbnd_ctx;
naming_context_impl *var_naming_context_impl;
naming_context_root *var_naming_context_root;

// Classes from ns.h
#include <nslib/ns.h>
ns *var_ns;

// Classes from ns_trans_states.h
#include <nslib/ns_trans_states.h>
ns_op_state *var_ns_op_state;
ns_new_context_state *var_ns_new_context_state;

// Classes from repl_ns_server.h
#include <nslib/repl_ns_server.h>
repl_ns_server *var_repl_ns_server;

// Classes from store.h
#ifdef _KERNEL_ORB
#include <nslib/store.h>
binding *var_binding;
volatile_binding *var_volatile_binding;
binding_store *var_binding_store;
volatile_store *var_volatile_store;
volatile_store::bnode *var_bnode;
#endif

#endif /* _KERNEL */
