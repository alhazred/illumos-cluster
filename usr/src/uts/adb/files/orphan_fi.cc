//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)orphan_fi.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orbtest/orphan_fi

#include "dummy.h"

#ifdef	_FAULT_INJECTION

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from server_impl.h
#include <orbtest/orphan_fi/server_impl.h>
server_impl *var_server_impl;

// Classes from support.h
#if defined(_KERNEL)
#if defined(_KERNEL_ORB)
#include <orbtest/orphan_fi/support.h>
impl_common *var_impl_common;
#endif
#endif

#endif /* _FAULT_INJECTION */
