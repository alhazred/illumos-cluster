//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clconf.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/clconf

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from ccr_cctree.h
#include <clconf/ccr_cctree.h>
ccr_node *var_ccr_node;

// Classes from clconf_ccr.h
#include <clconf/clconf_ccr.h>
clconf_infr_callback_impl *var_clconf_infr_callback_impl;
clconf_ccr_callback_impl *var_clconf_ccr_callback_impl;
clconf_ccr *var_clconf_ccr;

// Classes from clconf_file_io.h
#include <clconf/clconf_file_io.h>
clconf_file_io *var_clconf_file_io;

// Classes from clconf_io.h
#include <clconf/clconf_io.h>
clconf_io *var_clconf_io;

// Classes from clnode.h
#include <clconf/clnode.h>
cl_current_tree *var_cl_current_tree;
clrefcnt *var_clrefcnt;
clnode *var_clnode;

// Classes from comp_state_proxy.h
#include <clconf/comp_state_proxy.h>
component_state_proxy *var_component_state_proxy;

// Classes from comp_state_reg.h
#include <clconf/comp_state_reg.h>
component_state_reg *var_component_state_reg;
