//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rsmt.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/transports/rsm

#include "dummy.h"

#include <sys/os.h>
#include <orb/invo/corba.h>

#ifdef	_KERNEL

#define	NOINLINES

#include <transports/rsm/rsmt_adapter.h>
rsmt_adapter	*var_rsmt_adapter;

#include <transports/rsm/rsmt_bio.h>
rsmt_bio_mgr	*var_rsmt_bio_mgr;
rsmt_bio_procmsg_defer_task	*var_rsmt_bio_procmsg_defer_task;

#include <transports/rsm/rsmt_bio_buf.h>
rsmt_bio_buf	*var_rsmt_bio_buf;

#include <transports/rsm/rsmt_bio_jumbo_seg.h>
rsmt_bio_jumbo_seggrp	*var_rsmt_bio_jumbo_seggrp;
rsmt_bio_jumbo_seg	*var_rsmt_bio_jumbo_seg;
rsmt_bio_jumbo_rseg	*var_rsmt_bio_jumbo_rseg;
rsmt_bio_jumbo_sseg	*var_rsmt_bio_jumbo_sseg;
rsmt_bio_jumbo_buf	*var_rsmt_bio_jumbo_buf;

#include <transports/rsm/rsmt_bio_seg.h>
rsmt_bio_seg	*var_rsmt_bio_seg;
rsmt_bio_rseg	*var_rsmt_bio_rseg;
rsmt_bio_sseg	*var_rsmt_bio_sseg;
rsmt_bio_seggrp	*var_rsmt_bio_seggrp;

#include <transports/rsm/rsmt_cmf.h>
rsmt_cmf_adapter	*var_rsmt_cmf_adapter;
rsmt_cmf_defer_task	*var_rsmt_cmf_defer_task;
rsmt_cmf_mgr	*var_rsmt_cmf_mgr;

#include <transports/rsm/rsmt_cmf_client.h>
rsmt_cmf_client	*var_rsmt_cmf_client;
rsmt_cmf_procmsg_defer_task	*var_rsmt_cmf_procmsg_defer_task;
rsmt_cmf_sendq	*var_rsmt_cmf_sendq;

#include <transports/rsm/rsmt_endpoint.h>
rsmt_endpoint	*var_rsmt_endpoint;

#include <transports/rsm/rsmt_pathend.h>
rsmt_pathend	*var_rsmt_pathend;

#include <transports/rsm/rsmt_resource.h>
rsmt_resc_registry	*var_rsmt_resc_registry;
rsmt_segresc_registry	*var_rsmt_segresc_registry;
rsmt_memresc_registry	*var_rsmt_memresc_registry;
rsmt_resource	*var_rsmt_resource;

#include <transports/rsm/rsmt_resource_client.h>
rsmt_resource_client	*var_rsmt_resource_client;

#include <transports/rsm/rsmt_rio.h>
rsmt_rio_client	*var_rsmt_rio_client;
rsmt_rio_server	*var_rsmt_rio_server;

#include <transports/rsm/rsmt_rio_seg.h>
rsmt_rio_client_seg	*var_rsmt_rio_client_seg;
rsmt_rio_server_seg	*var_rsmt_rio_server_seg;
rsmt_rio_seg_pool	*var_rsmt_rio_seg_pool;
rsmt_rio_seg_mgr	*var_rsmt_rio_seg_mgr;
rsmt_rio_seg_scanner_dt	*var_rsmt_rio_seg_scanner_dt;
rsmt_rio_cseg_dt	*var_rsmt_rio_cseg_dt;
rsmt_rio_sseg_dt	*var_rsmt_rio_sseg_dt;

#include <transports/rsm/rsmt_rio_trashmem.h>
rsmt_rio_trashmem	*var_rsmt_rio_trashmem;
rsmt_rio_trashmem_server	*var_rsmt_rio_trashmem_server;


#include <transports/rsm/rsmt_seg_mgr.h>
rsmt_seg_mgr	*var_rsmt_seg_mgr;
rsmt_connect_defer_task	*var_rsmt_connect_defer_task;
rsmt_finalizer	*var_rsmt_finalizer;

#include <transports/rsm/rsmt_segid_mgr.h>
rsmt_segid_mgr	*var_rsmt_segid_mgr;

#include <transports/rsm/rsmt_segment.h>
rsmt_segment	*var_rsmt_segment;
rsmt_export_segment	*var_rsmt_export_segment;
rsmt_import_segment	*var_rsmt_import_segment;

#include <transports/rsm/rsmt_streams.h>
rsmt_sendstream	*var_rsmt_sendstream;
rsmt_sendstream::rsmt_MarshalStream	*var_rsmt_MarshalStream;
rsmt_recstream	*var_rsmt_recstream;

#include <transports/rsm/rsmt_transport.h>
rsmt_transport	*var_rsmt_transport;

#endif /* _KERNEL */
