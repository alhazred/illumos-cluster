//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)service.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/repl/service

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from ckpt_handler.h
#include <repl/service/ckpt_handler.h>
ckpt_handler_kit *var_ckpt_handler_kit;
ckpt_handler *var_ckpt_handler;
ckpt_elem *var_ckpt_elem;
ckpt_list *var_ckpt_list;
ckpt_handler_server *var_ckpt_handler_server;
ckpt_handler_client *var_ckpt_handler_client;
ckpt_handler_client::ckpt_handler_client_invoke *
var_ckpt_handler_client_invoke;

// Classes from ha_tid_factory.h
#include <repl/service/ha_tid_factory.h>
ha_tid_factory *var_ha_tid_factory;

// Classes from multi_ckpt_handler.h
#include <repl/service/multi_ckpt_handler.h>
sleep_elem *var_sleep_elem;
delete_elem *var_delete_elem;
ckpt_info *var_ckpt_info;
multi_ckpt_handler *var_multi_ckpt_handler;

// Classes from periodic_thread.h
#include <repl/service/periodic_thread.h>
periodic_thread *var_periodic_thread;
periodic_thread::periodic_job *var_periodic_job;

// Classes from repl_service.h
#ifdef _KERNEL_ORB
#ifdef _KERNEL_ORB
#include <repl/service/repl_service.h>
service_unref *var_service_unref;
push_ckpt_task *var_push_ckpt_task;
generic_repl_prov *var_generic_repl_prov;
repl_service *var_repl_service;
repl_service_manager *var_repl_service_manager;
#endif
#endif

// Classes from repl_tid_impl.h
#include <repl/service/repl_tid_impl.h>
kernel_tid_factory *var_kernel_tid_factory;
secondary_state_elem *var_secondary_state_elem;
ha_transaction_id *var_ha_transaction_id;
client_died_task *var_client_died_task;

// Classes from replica_handler.h
#ifdef _KERNEL_ORB
#include <repl/service/replica_handler.h>
replica_handler_kit *var_replica_handler_kit;
replica_handler *var_replica_handler;
trans_ctx *var_trans_ctx;
#endif

// Classes from transaction_state.h
#include <repl/service/transaction_state.h>
state_hash_table_t *var_state_hash_table_t;
register_state_task *var_register_state_task;
orphan_thread *var_orphan_thread;

#endif /* _KERNEL */
