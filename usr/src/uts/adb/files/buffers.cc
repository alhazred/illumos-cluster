//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)buffers.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orb/buffers

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#include <orb/buffers/Buf.h>

// Classes from Buf.h
#ifdef _KERNEL
#include <orb/buffers/Buf.h>
Buf *var_Buf;
GWBuf *var_GWBuf;
nil_Buf *var_nil_Buf;
CBuf *var_CBuf;
MBlkBuf *var_MBlkBuf;
PpBuf *var_PpBuf;
UBuf *var_UBuf;
UioBuf *var_UioBuf;
cursor *var_cursor;
Region *var_Region;
mblk_chain *var_mblk_chain;
#endif

// Classes from marshalstream.h
#ifdef _KERNEL_ORB
#include <orb/buffers/marshalstream.h>
MarshalStream *var_MarshalStream;
sendstream *var_sendstream;
serv2_transport_info *var_serv2_transport_info;
recstream *var_recstream;
#endif

// Classes from replyio.h
#include <orb/buffers/replyio.h>
replyio_client_t *var_replyio_client_t;
replyio_server_t *var_replyio_server_t;
