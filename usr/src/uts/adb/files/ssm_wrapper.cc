//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ssm_wrapper.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/cmd/ssm_wrapper

#include "dummy.h"

#ifndef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from ssm_util.h
#include <cmd/ssm_wrapper/ssm_util.h>
code_table *var_code_table;
ssm_port_and_proto *var_ssm_port_and_proto;
ssm_nodeweight *var_ssm_nodeweight;
ssm_node_list *var_ssm_node_list;
ssm_net_resource *var_ssm_net_resource;
ssm_net_resource_list *var_ssm_net_resource_list;

#endif /* _KERNEL */
