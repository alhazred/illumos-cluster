/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _DUMMY_H
#define	_DUMMY_H

#pragma ident	"@(#)dummy.h	1.5	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * Dummy structure for macrogen to determine whether it's operating in
 * LP64 mode or ILP32 mode.
 *
 * No need for any filler after int_val, 'cos we're not concerned about
 * the structure size
 */
struct __dummy {
	long	long_val;
	char	*ptr_val;
	int	int_val;
} __dummy;

#ifdef	__cplusplus
}
#endif

#endif	/* _DUMMY_H */
