//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)h.cc	1.12	08/08/01 SMI"

// Files from directory usr/src/common/cl/interfaces/64/h

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from addrspc.h
#include <h/addrspc.h>
addrspc *var_addrspc;
addrspc::addrspace *var_addrspace;
addrspc::addrspace_stub *var_addrspace_stub;

// Classes from bulkio.h
#include <h/bulkio.h>
bulkio *var_bulkio;
bulkio::file_hole *var_file_hole;
bulkio::in_uio *var_in_uio;
bulkio::in_uio_stub *var_in_uio_stub;
bulkio::inout_uio *var_inout_uio;
bulkio::inout_uio_stub *var_inout_uio_stub;
bulkio::in_aio *var_in_aio;
bulkio::in_aio_stub *var_in_aio_stub;
bulkio::uio_write_obj *var_uio_write_obj;
bulkio::uio_write_obj_stub *var_uio_write_obj_stub;
bulkio::aio_write_obj *var_aio_write_obj;
bulkio::aio_write_obj_stub *var_aio_write_obj_stub;
bulkio::pages_write_obj *var_pages_write_obj;
bulkio::in_pages *var_in_pages;
bulkio::in_pages_stub *var_in_pages_stub;
bulkio::inout_pages *var_inout_pages;
bulkio::inout_pages_stub *var_inout_pages_stub;
bulkio::in_aio_pages *var_in_aio_pages;
bulkio::in_aio_pages_stub *var_in_aio_pages_stub;
bulkio_holder *var_bulkio_holder;
bulkio_holder::holder *var_holder;
bulkio_holder::holder_stub *var_holder_stub;

// Classes from ccr.h
#include <h/ccr.h>
ccr *var_ccr;
ccr::no_such_table *var_no_such_table;
ccr::table_exists *var_table_exists;
ccr::invalid_table_name *var_invalid_table_name;
ccr::is_metadata *var_is_metadata;
ccr::no_such_key *var_no_such_key;
ccr::NoMoreElements *var_NoMoreElements;
ccr::key_exists *var_key_exists;
ccr::callback_exists *var_callback_exists;
ccr::no_such_callback *var_no_such_callback;
ccr::table_invalid *var_table_invalid;
ccr::system_error *var_system_error;
ccr::table_modified *var_table_modified;
ccr::readonly_access *var_readonly_access;
ccr::out_of_service *var_out_of_service;
ccr::database_invalid *var_database_invalid;
ccr::lost_quorum *var_lost_quorum;
ccr::table_element *var_table_element;
ccr::directory *var_directory;
ccr::directory_stub *var_directory_stub;
ccr::readonly_table *var_readonly_table;
ccr::updatable_table *var_updatable_table;
ccr::callback *var_callback;
ccr::callback_stub *var_callback_stub;

// Classes from ccr_data.h
#include <h/ccr_data.h>
ccr_data *var_ccr_data;
ccr_data::tm_not_found *var_tm_not_found;
ccr_data::register_failed *var_register_failed;
ccr_data::consis *var_consis;
ccr_data::data_server *var_data_server;
ccr_data::data_server_stub *var_data_server_stub;
ccr_data::updatable_table_copy *var_updatable_table_copy;

// Classes from ccr_trans.h
#include <h/ccr_trans.h>
ccr_trans *var_ccr_trans;
ccr_trans::comm_error *var_comm_error;
ccr_trans::ds_exists *var_ds_exists;
ccr_trans::no_such_ds *var_no_such_ds;
ccr_trans::handle *var_handle;
ccr_trans::handle_stub *var_handle_stub;
ccr_trans::manager *var_manager;
ccr_trans::manager_stub *var_manager_stub;
ccr_trans::ckpt *var_ckpt;
ccr_trans::ckpt_stub *var_ckpt_stub;

// Classes from clevent.h
#include <h/clevent.h>
clevent *var_clevent;
clevent::clevent_comm *var_clevent_comm;
clevent::clevent_comm_stub *var_clevent_comm_stub;

// Classes from cmm.h
#include <h/cmm.h>
cmm *var_cmm;
cmm::membership_t *var_membership_t;
cmm::force_return *var_force_return;
cmm::unstable_membership *var_unstable_membership;
cmm::not_start_state *var_not_start_state;
cmm::no_resources *var_no_resources;
cmm::invalid_parameter *var_invalid_parameter;
cmm::callback_info *var_callback_info;
cmm::callback_registry *var_callback_registry;
cmm::cluster_state_t *var_cluster_state_t;
cmm::cluster_info_t *var_cluster_info_t;
cmm::control *var_control;
cmm::control_stub *var_control_stub;
cmm::message_t *var_message_t;
cmm::comm *var_comm;
cmm::comm_stub *var_comm_stub;
cmm::automaton *var_automaton;
cmm::automaton_stub *var_automaton_stub;
cmm::remote_ucmm_proxy *var_remote_ucmm_proxy;
cmm::userland_cmm *var_userland_cmm;
cmm::userland_cmm_stub *var_userland_cmm_stub;
cmm::kernel_ucmm_agent *var_kernel_ucmm_agent;

// Classes from component_state.h
#include <h/component_state.h>
component_state *var_component_state;
component_state::component_state_t *var_component_state_t;
component_state::registry *var_registry;
component_state::registry::component_does_not_exist *
var_component_does_not_exist;
component_state::registry_stub *var_registry_stub;

// Classes from data_container.h
#include <h/data_container.h>
data_container *var_data_container;
data_container::invalid_data *var_invalid_data;
data_container::data *var_data;
data_container::data_stub *var_data_stub;

// Classes from dc.h
#include <h/dc.h>
mdc *var_mdc;
mdc::dcs_error *var_dcs_error;
mdc::device_service_info *var_device_service_info;
mdc::node_preference *var_node_preference;
mdc::dev_range *var_dev_range;
mdc::service_property *var_service_property;
mdc::dc_config *var_dc_config;
mdc::dc_config_stub *var_dc_config_stub;
mdc::dc_mapper *var_dc_mapper;
mdc::dc_mapper_stub *var_dc_mapper_stub;
mdc::device_server *var_device_server;
mdc::device_server_stub *var_device_server_stub;
mdc::device_service_manager *var_device_service_manager;
mdc::reservation_client *var_reservation_client;

// Classes from ff.h
#include <h/ff.h>
ff *var_ff;
ff::failfast *var_failfast;
ff::failfast_stub *var_failfast_stub;
ff::failfast_admin *var_failfast_admin;

// Classes from flowcontrol_fi.h
#include <h/flowcontrol_fi.h>
flowcontrol_fi *var_flowcontrol_fi;
flowcontrol_fi::Errmsg *var_Errmsg;
flowcontrol_fi::server *var_server;
flowcontrol_fi::server_stub *var_server_stub;
flowcontrol_fi::node *var_node;
flowcontrol_fi::node_stub *var_node_stub;

// Classes from ha_stress.h
#include <h/ha_stress.h>
ha_stress *var_ha_stress;
ha_stress::already_stopped *var_already_stopped;
ha_stress::client *var_client;
ha_stress::client_stub *var_client_stub;

// Classes from mc_sema.h
#include <h/mc_sema.h>
mc_sema *var_mc_sema;
mc_sema::invalid_cookie *var_invalid_cookie;
mc_sema::sema *var_sema;
mc_sema::sema_stub *var_sema_stub;

// Classes from naming.h
#include <h/naming.h>
naming *var_naming;
naming::binding *var_binding;
naming::not_found *var_not_found;
naming::cannot_proceed *var_cannot_proceed;
naming::invalid_name *var_invalid_name;
naming::already_bound *var_already_bound;
naming::naming_context *var_naming_context;
naming::binding_iterator *var_binding_iterator;

// Classes from network.h
#include <h/network.h>
network *var_network;
network::service_sap_t *var_service_sap_t;
network::group_t *var_group_t;
network::sticky_config_t *var_sticky_config_t;
network::cid_t *var_cid_t;
network::hashinfo_t *var_hashinfo_t;
network::pdtinfo_t *var_pdtinfo_t;
network::fwd_t *var_fwd_t;
network::bind_t *var_bind_t;
network::gns_bind_t *var_gns_bind_t;
network::srvinfo_t *var_srvinfo_t;
network::grpinfo_t *var_grpinfo_t;
network::macinfo_t *var_macinfo_t;
network::gifinfo_t *var_gifinfo_t;
network::dist_info_t *var_dist_info_t;
network::print_state_cmd_t *var_print_state_cmd_t;
network::mcobj *var_mcobj;
network::mcobj_stub *var_mcobj_stub;
network::lbobj_i *var_lbobj_i;
network::lbobj_i_stub *var_lbobj_i_stub;
network::weighted_lbobj *var_weighted_lbobj;
network::weighted_lbobj::state_changed *var_state_changed;
network::weighted_lbobj_stub *var_weighted_lbobj_stub;
network::perf_mon_policy *var_perf_mon_policy;
network::perf_mon_policy::node_metrics *var_node_metrics;
network::perf_mon_policy::perf_mon_parameters_t *var_perf_mon_parameters_t;
network::perf_mon_policy_stub *var_perf_mon_policy_stub;
network::user_policy *var_user_policy;
network::user_policy_stub *var_user_policy_stub;
network::PDTServer *var_PDTServer;
network::PDTServer_stub *var_PDTServer_stub;
network::mcnet_node *var_mcnet_node;
network::mcnet_node_stub *var_mcnet_node_stub;
network::cl_net_ns_if *var_cl_net_ns_if;
network::cl_net_ns_if_stub *var_cl_net_ns_if_stub;

// Classes from orbmsg.h
#include <h/orbmsg.h>
orbmsg *var_orbmsg;
orbmsg::message_mgr *var_message_mgr;
orbmsg::message_mgr_stub *var_message_mgr_stub;

// Classes from orbtest.h
#include <h/orbtest.h>
orbtest *var_orbtest;
orbtest::test_obj *var_test_obj;
orbtest::test_obj_stub *var_test_obj_stub;
orbtest::test_mgr *var_test_mgr;
orbtest::test_mgr_stub *var_test_mgr_stub;
orbtest::test_params *var_test_params;
orbtest::test_params_stub *var_test_params_stub;
orbtest::test_ckpt *var_test_ckpt;
orbtest::test_ckpt_stub *var_test_ckpt_stub;

// Classes from orphan_fi.h
#include <h/orphan_fi.h>
orphan_fi *var_orphan_fi;
orphan_fi::common *var_common;
orphan_fi::common_stub *var_common_stub;
orphan_fi::testobj *var_testobj;
orphan_fi::testobj_stub *var_testobj_stub;
orphan_fi::server::TestException *var_TestException;

// Classes from perf.h
#include <h/perf.h>
perf *var_perf;
perf::stats *var_stats;
perf::stats_stub *var_stats_stub;
perf::group *var_group;
perf::group_stub *var_group_stub;
perf::cbk *var_cbk;
perf::cbk_stub *var_cbk_stub;
perf::kstats *var_kstats;
perf::kstats_stub *var_kstats_stub;

// Classes from px_aio.h
#include <h/px_aio.h>
pxfs_aio *var_pxfs_aio;
pxfs_aio::aio_callback *var_aio_callback;
pxfs_aio::aio_callback_stub *var_aio_callback_stub;

// Classes from pxfs.h
#include <h/pxfs.h>
fs *var_fs;
fs::dc_callback *var_dc_callback;
fs::dc_callback_stub *var_dc_callback_stub;
fs::mount_server *var_mount_server;
fs::mount_server::mount_err *var_mount_err;
fs::mount_server_stub *var_mount_server_stub;
fs::mount_client *var_mount_client;
fs::mount_client_stub *var_mount_client_stub;
fs::mount_client_died *var_mount_client_died;
fs::fs_collection *var_fs_collection;
fs::fs_collection_stub *var_fs_collection_stub;

// Classes from quorum.h
#include <h/quorum.h>
quorum *var_quorum;
quorum::qd_scrub_failed *var_qd_scrub_failed;
quorum::quorum_result_t *var_quorum_result_t;
quorum::reservation_key_t *var_reservation_key_t;
quorum::node_config_t *var_node_config_t;
quorum::node_status_t *var_node_status_t;
quorum::quorum_device_config_t *var_quorum_device_config_t;
quorum::quorum_device_status_t *var_quorum_device_status_t;
quorum::quorum_status_t *var_quorum_status_t;
quorum::quorum_algorithm *var_quorum_algorithm;

// Classes from ref_stress.h
#include <h/ref_stress.h>
ref_stress *var_ref_stress;
ref_stress::test_param *var_test_param;
ref_stress::rsrc_balancer *var_rsrc_balancer;
ref_stress::rsrc_balancer_stub *var_rsrc_balancer_stub;

// Classes from repl_dc.h
#include <h/repl_dc.h>
repl_dc *var_repl_dc;
repl_dc::repl_dev_server_ckpt *var_repl_dev_server_ckpt;
repl_dc::repl_dc_ckpt *var_repl_dc_ckpt;

// Classes from repl_dc_data.h
#include <h/repl_dc_data.h>
repl_dc_data *var_repl_dc_data;
repl_dc_data::dc_contig_chunk_info *var_dc_contig_chunk_info;
repl_dc_data::dc_min_alloc_info *var_dc_min_alloc_info;
repl_dc_data::dc_minor_alloc_info *var_dc_minor_alloc_info;
repl_dc_data::dc_map_file_ent_info *var_dc_map_file_ent_info;
repl_dc_data::dc_map_file_info *var_dc_map_file_info;
repl_dc_data::dc_storage_info *var_dc_storage_info;
repl_dc_data::dc_instance_alloc_info *var_dc_instance_alloc_info;
repl_dc_data::dc_inst_alloc_info *var_dc_inst_alloc_info;

// Classes from repl_ns.h
#include <h/repl_ns.h>
repl_ns *var_repl_ns;
repl_ns::ns_replica *var_ns_replica;
repl_ns::ns_replica_stub *var_ns_replica_stub;

// Classes from repl_pxfs.h
#include <h/repl_pxfs.h>
repl_pxfs *var_repl_pxfs;
repl_pxfs::mount_replica *var_mount_replica;
repl_pxfs::ha_mounter *var_ha_mounter;
repl_pxfs::ha_mounter::MountFailed *var_MountFailed;
repl_pxfs::ha_mounter_stub *var_ha_mounter_stub;

// Classes from remote_exec.h
#include <h/remote_exec.h>
remote_exec::cl_exec *var_cl_exec;
remote_exec::cl_exec_stub *var_cl_exec_stub;

// Classes from repl_rm.h
#include <h/repl_rm.h>
repl_rm *var_repl_rm;
repl_rm::rm_ckpt *var_rm_ckpt;
repl_rm::rm_ckpt_stub *var_rm_ckpt_stub;

// Classes from repl_sample.h
#include <h/repl_sample.h>
repl_sample *var_repl_sample;
repl_sample::could_not_update *var_could_not_update;
repl_sample::persistent_store *var_persistent_store;
repl_sample::value_obj *var_value_obj;
repl_sample::value_obj_stub *var_value_obj_stub;
repl_sample::persist_obj *var_persist_obj;
repl_sample::persist_obj_stub *var_persist_obj_stub;
repl_sample::factory *var_factory;
repl_sample::factory_stub *var_factory_stub;

// Classes from replica.h
#include <h/replica.h>
replica *var_replica;
replica::prov_dependency *var_prov_dependency;
replica::service_busy *var_service_busy;
replica::checkpoint *var_checkpoint;
replica::checkpoint_stub *var_checkpoint_stub;
replica::service_failed_by_repl_prov *var_service_failed_by_repl_prov;
replica::repl_prov_failed *var_repl_prov_failed;
replica::become_secondary_failed *var_become_secondary_failed;
replica::invalid_repl_prov_state *var_invalid_repl_prov_state;
replica::repl_prov_info *var_repl_prov_info;
replica::repl_prov *var_repl_prov;
replica::repl_prov_stub *var_repl_prov_stub;
replica::trans_state *var_trans_state;
replica::trans_state_stub *var_trans_state_stub;
replica::transaction_id *var_transaction_id;
replica::service_info *var_service_info;
replica::priority_info *var_priority_info;
replica::service_already_exists *var_service_already_exists;
replica::too_many_services *var_too_many_services;
replica::depends_on *var_depends_on;
replica::uninitialized_service *var_uninitialized_service;
replica::failed_service *var_failed_service;
replica::deleted_service *var_deleted_service;
replica::unknown_service *var_unknown_service;
replica::invalid_numsecs *var_invalid_numsecs;
replica::invalid_repl_prov *var_invalid_repl_prov;
replica::dependent_prov *var_dependent_prov;
replica::repl_prov_already_exists *var_repl_prov_already_exists;
replica::invalid_dependency *var_invalid_dependency;
replica::dependency_not_satisfied *var_dependency_not_satisfied;
replica::dependency_cycle *var_dependency_cycle;
replica::dependency_failed *var_dependency_failed;
replica::rm_admin *var_rm_admin;
replica::rm_admin_stub *var_rm_admin_stub;
replica::service_admin *var_service_admin;
replica::service_admin::service_changed *var_service_changed;
replica::service_admin_stub *var_service_admin_stub;
replica::repl_prov_reg *var_repl_prov_reg;
replica::repl_prov_reg_stub *var_repl_prov_reg_stub;
replica::service_state_info *var_service_state_info;
replica::service_state_callback *var_service_state_callback;
replica::rma_public *var_rma_public;
replica::rma_public_stub *var_rma_public_stub;

// Classes from replica_int.h
#include <h/replica_int.h>
replica_int *var_replica_int;
replica_int::primary_info *var_primary_info;
replica_int::init_service_info *var_init_service_info;
replica_int::rma_reconf *var_rma_reconf;
replica_int::rma_reconf_stub *var_rma_reconf_stub;
replica_int::handler_repl_prov *var_handler_repl_prov;
replica_int::tid_factory *var_tid_factory;
replica_int::tid_factory_stub *var_tid_factory_stub;
replica_int::reconnect_object *var_reconnect_object;
replica_int::rma_repl_prov *var_rma_repl_prov;
replica_int::rma_repl_prov_stub *var_rma_repl_prov_stub;
replica_int::rma_admin *var_rma_admin;
replica_int::rma_admin_stub *var_rma_admin_stub;
replica_int::rm_service_admin *var_rm_service_admin;
replica_int::rm *var_rm;
replica_int::rm_stub *var_rm_stub;
replica_int::rmm_prov_rank *var_rmm_prov_rank;
replica_int::bad_exchange_time *var_bad_exchange_time;
replica_int::rmm *var_rmm;
replica_int::rmm_stub *var_rmm_stub;

// Classes from rgm.h
#include <h/rgm.h>
rgm *var_rgm;
rgm::r_state *var_r_state;
rgm::rg_state *var_rg_state;
rgm::node_state *var_node_state;
rgm::idl_nameval *var_idl_nameval;
rgm::idl_rs_get_state_status_args *var_idl_rs_get_state_status_args;
rgm::idl_get_status_result_t *var_idl_get_status_result_t;
rgm::idl_rs_set_status_args *var_idl_rs_set_status_args;
rgm::idl_set_status_result_t *var_idl_set_status_result_t;
rgm::idl_rs_get_fail_status_args *var_idl_rs_get_fail_status_args;
rgm::idl_rg_get_args *var_idl_rg_get_args;
rgm::idl_rg_get_state_args *var_idl_rg_get_state_args;
rgm::idl_get_result_t *var_idl_get_result_t;
rgm::idl_cluster_get_args *var_idl_cluster_get_args;
rgm::idl_rt_update_instnode_args *var_idl_rt_update_instnode_args;
rgm::idl_rg_create_args *var_idl_rg_create_args;
rgm::idl_rg_update_prop_args *var_idl_rg_update_prop_args;
rgm::idl_rg_remove_args *var_idl_rg_remove_args;
rgm::idl_rs_add_args *var_idl_rs_add_args;
rgm::idl_rs_update_prop_args *var_idl_rs_update_prop_args;
rgm::idl_validate_args *var_idl_validate_args;
rgm::idl_rs_delete_args *var_idl_rs_delete_args;
rgm::idl_scswitch_primaries_args *var_idl_scswitch_primaries_args;
rgm::idl_scha_control_args *var_idl_scha_control_args;
rgm::idl_scha_control_result *var_idl_scha_control_result;
rgm::idl_scha_control_checkall_args *var_idl_scha_control_checkall_args;
rgm::idl_regis_result_t *var_idl_regis_result_t;
rgm::rgm_intention *var_rgm_intention;
rgm::idl_scswitch_onoff_args *var_idl_scswitch_onoff_args;
rgm::idl_scswitch_clear_args *var_idl_scswitch_clear_args;
rgm::rgm_comm *var_rgm_comm;
rgm::rgm_comm_stub *var_rgm_comm_stub;

// Classes from sol.h
#include <h/sol.h>
sol *var_sol;
sol::op_e *var_op_e;
sol::timestruc_t *var_timestruc_t;
sol::vattr_t *var_vattr_t;
sol::aclent_t *var_aclent_t;
sol::statvfs64_t *var_statvfs64_t;
sol::fsid_t *var_fsid_t;
sol::mounta *var_mounta;
sol::flock64_t *var_flock64_t;
sol::shrlock_t *var_shrlock_t;
sol::procset_t *var_procset_t;
sol::sigsend_t *var_sigsend_t;

// Classes from solobj.h
#include <h/solobj.h>
solobj *var_solobj;
solobj::cred *var_cred;
solobj::cred_stub *var_cred_stub;

// Classes from streams.h
#include <h/streams.h>
streams *var_streams;
streams::msg *var_msg;
streams::msg_stub *var_msg_stub;
streams::queue *var_queue;
streams::queue_stub *var_queue_stub;

// Classes from test_kproxy.h
#include <h/test_kproxy.h>
test_kproxy *var_test_kproxy;
test_kproxy::fname_not_found *var_fname_not_found;

// Classes from typetest_0.h
#include <h/typetest_0.h>
typetest *var_typetest;
typetest::Obj *var_Obj;
typetest::Obj_stub *var_Obj_stub;
typetest::InhObjA *var_InhObjA;
typetest::InhObjA_stub *var_InhObjA_stub;
typetest::InhObjB *var_InhObjB;
typetest::InhObjB_stub *var_InhObjB_stub;
typetest::InhObjC *var_InhObjC;
typetest::InhObjC_stub *var_InhObjC_stub;
typetest::InhObjD *var_InhObjD;
typetest::InhObjD_stub *var_InhObjD_stub;
typetest::Fstruct *var_Fstruct;
typetest::Vstruct *var_Vstruct;
typetest::Funion *var_Funion;
typetest::Vunion *var_Vunion;
typetest::emptyException *var_emptyException;
typetest::basicException *var_basicException;
typetest::complexException *var_complexException;
typetest::stringException *var_stringException;

// Classes from unref_fi.h
#include <h/unref_fi.h>
unref_fi *var_unref_fi;

// Classes from version.h
#include <h/version.h>
inf_version_tbl_entry *var_inf_version_tbl_entry;
inf_descriptor *var_inf_descriptor;
idlversion *var_idlversion;
idlversion_stub *var_idlversion_stub;
