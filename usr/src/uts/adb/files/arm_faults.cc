//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)arm_faults.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/orbtest/arm_faults

#include "dummy.h"

#ifdef	_FAULT_INJECTION

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
#include <sys/types.h>

// Classes from base_defs.h
#if defined(_KERNEL_ORB)
#include <orbtest/arm_faults/base_defs.h>
arm_fault *var_arm_fault;
sysex_fault *var_sysex_fault;
retry_unsent_fault *var_retry_unsent_fault;
retry_sent_fault *var_retry_sent_fault;
vanilla_fault *var_vanilla_fault;
#endif

// Classes from fault_defs.h
#include <orbtest/arm_faults/fault_defs.h>
retry_unsent_invo *var_retry_unsent_invo;
retry_sent_invo *var_retry_sent_invo;
retry_unsent_reply *var_retry_unsent_reply;
retry_sent_reply *var_retry_sent_reply;
retry_unsent_refjob *var_retry_unsent_refjob;
retry_sent_refjob *var_retry_sent_refjob;
retry_unsent_cmm *var_retry_unsent_cmm;
retry_sent_cmm *var_retry_sent_cmm;
retry_unsent_signal *var_retry_unsent_signal;
retry_sent_signal *var_retry_sent_signal;
retry_unsent_ckpt *var_retry_unsent_ckpt;
retry_sent_ckpt *var_retry_sent_ckpt;
retry_unsent_flow *var_retry_unsent_flow;
retry_sent_flow *var_retry_sent_flow;
retry_unsent_oneway *var_retry_unsent_oneway;
retry_sent_oneway *var_retry_sent_oneway;
retry_unsent_faultmsg *var_retry_unsent_faultmsg;
retry_sent_faultmsg *var_retry_sent_faultmsg;
retry_unsent_maybemsg *var_retry_unsent_maybemsg;
retry_sent_maybemsg *var_retry_sent_maybemsg;
tick_delay_panic *var_tick_delay_panic;
delay_send_thread *var_delay_send_thread;
delay_clock_thread *var_delay_clock_thread;
pathend_fail *var_pathend_fail;
stop_pm_send *var_stop_pm_send;
path_down_panic *var_path_down_panic;

#endif /* _FAULT_INJECTION */
