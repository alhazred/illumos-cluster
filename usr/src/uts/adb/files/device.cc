//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)device.cc	1.6	08/05/20 SMI"

// Files from directory usr/src/common/cl/pxfs/device

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from device_replica_impl.h
#include <pxfs/device/device_replica_impl.h>
device_replica_impl *var_device_replica_impl;

// Classes from device_server_impl.h
#include <pxfs/device/device_server_impl.h>
device_server_ii *var_device_server_ii;
device_server_norm_impl *var_device_server_norm_impl;
device_server_repl_impl *var_device_server_repl_impl;

// Classes from device_service_mgr.h
#include <pxfs/device/device_service_mgr.h>
device_service_mgr *var_device_service_mgr;
// device_service_mgr::replica_startup_params *var_replica_startup_params;
// device_service_mgr::switchback_params *var_switchback_params;

#endif /* _KERNEL */
