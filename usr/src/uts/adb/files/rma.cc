//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rma.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/repl/rma

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from hxdoor.h
#include <repl/rma/hxdoor.h>
hxdoor_id *var_hxdoor_id;
hxdoor_manager *var_hxdoor_manager;
hxdoor *var_hxdoor;

// Classes from hxdoor_service.h
#include <repl/rma/hxdoor_service.h>
hxdoor_service *var_hxdoor_service;

// Classes from rma.h
#include <repl/rma/rma.h>
reconnect_object_impl *var_reconnect_object_impl;
rma_prov_impl *var_rma_prov_impl;
rma *var_rma;
rma::reconf_impl *var_reconf_impl;
rma::admin_impl *var_admin_impl;
rma::init_service_task *var_init_service_task;
rma::state_change_task *var_state_change_task;

#endif /* _KERNEL */
