//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)pxfs_server.cc	1.9	08/05/20 SMI"

// Files from directory usr/src/common/cl/pxfs/server

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

#include <sys/lockfs.h>

#include <pxfs/server/async_io.h>
#include <pxfs/server/file_impl.h>
#include <pxfs/server/fobj_impl.h>
#include <pxfs/server/frange_lock.h>
#include <pxfs/server/fobjplus_impl.h>
#include <pxfs/server/fobj_trans_states.h>
#include <pxfs/server/fs_base_impl.h>
#include <pxfs/server/fs_dependent_impl.h>
#include <pxfs/server/fs_impl.h>
#include <pxfs/server/fsmgr_server_impl.h>
#include <pxfs/server/hsfs_dependent_impl.h>
#include <pxfs/server/io_impl.h>
#include <pxfs/server/io_trans_states.h>
#include <pxfs/server/prov_common.h>
#include <pxfs/server/repl_pxfs_server.h>
#include <pxfs/server/special_impl.h>
#include <pxfs/server/symlink_impl.h>
#include <pxfs/server/ufs_dependent_impl.h>
#include <pxfs/server/unixdir_ckpt.h>
#include <pxfs/server/unixdir_impl.h>

#ifndef VXFS_DISABLED
#include <pxfs/server/vxfs_dependent_impl.h>
#endif

// Classes from async_io.h
io_async *var_io_async;

// Classes from file_impl.h
file_ii *var_file_ii;
file_norm_impl *var_file_norm_impl;
file_repl_impl *var_file_repl_impl;

// Classes from fobj_impl.h
fobj_ii *var_fobj_ii;

// Classes from frange_lock.h
frange_lock *var_frange_lock;

// Classed from fobjplus_impl.h
fobjplus_ii *var_fobjplus_ii;

// Classes from fobj_trans_states.h
fobj_lockfs_state *var_fobj_lockfs_state;
fobj_retry_state *var_fobj_retry_state;

// Classes from fs_base_impl.h
release_info_t *var_release_info_t;
fs_base_ii *var_fs_base_ii;

// Classes from fs_dependent_impl.h
fs_dependent_impl *var_fs_dependent_impl;

// Classes from fs_impl.h
fs_ii *var_fs_ii;
fs_norm_impl *var_fs_norm_impl;
fs_repl_impl *var_fs_repl_impl;

// Classes from fsmgr_server_impl.h
fsmgr_server_ii *var_fsmgr_server_ii;
fsmgr_server_norm_impl *var_fsmgr_server_norm_impl;
fsmgr_server_repl_impl *var_fsmgr_server_repl_impl;

// Classes from hsfs_dependent_impl.h
hsfs_dependent_impl *var_hsfs_dependent_impl;

// Classes from io_impl.h
io_ii *var_io_ii;
io_norm_impl *var_io_norm_impl;
io_repl_impl *var_io_repl_impl;
// io_repl_impl::open_params_t *var_open_params_t;

// Classes from io_trans_states.h
io_ioctl_state *var_io_ioctl_state;
io_close_state *var_io_close_state;

// Classes from prov_common.h
prov_common *var_prov_common;
prov_common_set *var_prov_common_set;
prov_common_iter *var_prov_common_iter;

// Classes from repl_pxfs_server.h
repl_pxfs_server *var_repl_pxfs_server;

// Classes from special_impl.h
special_ii *var_special_ii;
special_norm_impl *var_special_norm_impl;
special_repl_impl *var_special_repl_impl;

// Classes from symlink_impl.h
symlink_ii *var_symlink_ii;
symlink_norm_impl *var_symlink_norm_impl;
symlink_repl_impl *var_symlink_repl_impl;

// Classes from ufs_dependent_impl.h
ufs_dependent_impl *var_ufs_dependent_impl;

// Classes from unixdir_ckpt.h
unixdir_state *var_unixdir_state;

// Classes from unixdir_impl.h
unixdir_ii *var_unixdir_ii;
unixdir_norm_impl *var_unixdir_norm_impl;
unixdir_repl_impl *var_unixdir_repl_impl;

#ifndef VXFS_DISABLED
// Classes from vxfs_dependent_impl.h
vxfs_dependent_impl *var_vxfs_dependent_impl;
#endif

#endif /* _KERNEL */
