//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)tm.cc	1.4	08/05/20 SMI"

// Files from directory usr/src/common/cl/tm

#include "dummy.h"
#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES

// Classes from common_tm.h
#ifdef _KERNEL_ORB
#ifdef _KERNEL
#include <tm/common_tm.h>
tm_point *var_tm_point;
tm_domain *var_tm_domain;
tm_path *var_tm_path;
topology_manager *var_topology_manager;
#endif
#endif

// Classes from kernel_sci.h
#ifdef _KERNEL
#include <tm/kernel_sci.h>
clconfig_ops *var_clconfig_ops;
tm_sci_device *var_tm_sci_device;
#endif

// Classes from kernel_tm.h
#ifdef _KERNEL
#include <tm/kernel_tm.h>
tm_device *var_tm_device;
kernel_tm *var_kernel_tm;
#endif

// Classes from user_sci.h
#include <tm/user_sci.h>
tm_sci_point *var_tm_sci_point;

// Classes from user_tm.h
#include <tm/user_tm.h>
tm_user_domain *var_tm_user_domain;
user_tm *var_user_tm;
