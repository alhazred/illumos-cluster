//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)ccr.cc	1.3	08/05/20 SMI"

// Files from directory usr/src/common/cl/ccr

#include "dummy.h"

#ifdef	_KERNEL

#include <sys/os.h>
#include <orb/invo/corba.h>
#define	NOINLINES
typedef uint32_t xdoor_id;

// Classes from common.h
#include <ccr/common.h>
dir_elem_seq *var_dir_elem_seq;

// Classes from data_server_impl.h
#include <ccr/data_server_impl.h>
data_server_impl *var_data_server_impl;
table_access *var_table_access;

// Classes from directory_impl.h
#include <ccr/directory_impl.h>
directory_impl *var_directory_impl;

// Classes from fault_ccr.h
#if defined(_FAULT_INJECTION)
#include <ccr/fault_ccr.h>
fault_ccr *var_fault_ccr;
fault_ccr::ccr_arg_t *var_ccr_arg_t;
#endif

// Classes from manager_impl.h
#include <ccr/manager_impl.h>
manhandle *var_manhandle;
ds_info_t *var_ds_info_t;
manager_impl *var_manager_impl;
simple_state *var_simple_state;
reg_state *var_reg_state;

// Classes from persistent_table.h
#include <ccr/persistent_table.h>
table_element_t *var_table_element_t;
readonly_persistent_table *var_readonly_persistent_table;
updatable_persistent_table *var_updatable_persistent_table;
ccrlib *var_ccrlib;

// Classes from readonly_copy_impl.h
#include <ccr/readonly_copy_impl.h>
readonly_copy_impl *var_readonly_copy_impl;

// Classes from transaction_impl.h
#include <ccr/transaction_impl.h>
update_info *var_update_info;
table_trans_impl *var_table_trans_impl;
update_state *var_update_state;
commit_state *var_commit_state;
abort_state *var_abort_state;

// Classes from updatable_copy_impl.h
#include <ccr/updatable_copy_impl.h>
updatable_copy_impl *var_updatable_copy_impl;

#endif /* _KERNEL */
