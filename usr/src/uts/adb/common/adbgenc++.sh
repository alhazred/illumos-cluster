#! /bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# @(#)adbgenc++.sh 1.4 08/05/20 SMI
#
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
case $1 in
-d)
	DEBUG=:
	shift;;
-*)
	flag=$1
	shift;;
esac
ADBDIR=/usr/lib/adb
PATH=$PATH:$ADBDIR
for file in $*
do
	if [ `expr "XX$file" : ".*\.adb"` -eq 0 ]
	then
		echo File $file invalid.
		exit 1
	fi
	if [ $# -gt 1 ]
	then
		echo $file:
	fi
	file=`expr "XX$file" : "XX\(.*\)\.adb"`
	if adbgen1 $flag < $file.adb > $file.adb.c
	then
		if ${CCC} -w -D${ARCH:-`uname -m`} \
			-I/usr/share/src/uts/${ARCH:-`uname -m`} \
			-o $file.run $file.adb.c $ADBDIR/adbsub.o
		then
			$file.run | adbgen3 | adbgen4 > $file
			$DEBUG rm -f $file.run $file.adb.C $file.adb.c $file.adb.o
		else
			$DEBUG rm -f $file.run $file.adb.C $file.adb.c $file.adb.o
			echo compile failed
			exit 1
		fi
	else
		$DEBUG rm -f $file.adb.C
		echo adbgen1 failed
		exit 1
	fi
done
