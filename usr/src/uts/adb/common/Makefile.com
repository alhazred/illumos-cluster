#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)Makefile.com	1.8	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# uts/adb/common/Makefile.com
#

PROGS		= adbgen adbgen1 adbgen3 adbgen4
OBJS		= adbsub.o

SCRIPTS		= $(SRCS:.adb=)

include $(ADB_BASE_DIR)/../Makefile.uts

# The following definitions are needed because of x86. It could have been
# simplified to
# INCLUDES	= -I${SYSDIR}/${MACH} -I${SYSDIR}/sun
INCLUDES-i386	= -I${SYSDIR}/i86
INCLUDES-sparc	= -I${SYSDIR}/${MACH} -I${SYSDIR}/sun -I${SYSDIR}/common -I$(SRC)
INCLUDES-ppc	= -I${SYSDIR}/${MACH}
INCLUDES	= ${INCLUDES-${MACH}}
INCDIR		= ${SYSDIR}/common
NATIVEDEFS	= -D${MACH} -D__${MACH} -D_KERNEL

ROOTUSRDIR	= $(VROOT)/usr
ROOTLIBDIR	= $(ROOTUSRDIR)/lib
ROOTADBDIR	= $(ROOTLIBDIR)/adb

ROOTPROGS	= $(PROGS:%=$(ROOTADBDIR)/%)
ROOTOBJS	= $(OBJS:%=$(ROOTADBDIR)/%)
ROOTSCRIPTS	= $(SCRIPTS:%=$(ROOTADBDIR)/%)

LDLIBS 		= $(ENVLDLIBS1)  $(ENVLDLIBS2)  $(ENVLDLIBS3)
LDFLAGS 	= $(STRIPFLAG) $(ENVLDFLAGS1) $(ENVLDFLAGS2) $(ENVLDFLAGS3)
CPPFLAGS	= $(CPPFLAGS.master)

.KEEP_STATE:

MACH_FLAG=	__$(MACH)
NATIVEDEFS-ppc=	-D__LITTLE_ENDIAN -D$(MACH_FLAG) -Ui386
NATIVEDEFS-i386=
NATIVEDEFS-sparc=
NATIVEDEFS =	-D${MACH} -D_KERNEL
NATIVEDEFS +=	${NATIVEDEFS-${MACH}}
NATIVEDIR	= $(ADB_BASE_DIR)/native/${NATIVE_MACH}
NATIVEPROGS	= $(PROGS:%=$(NATIVEDIR)/%)
NATIVEOBJS	= $(OBJS:%=$(NATIVEDIR)/%)

.PARALLEL: $(PROGS) $(NATIVEPROGS) $(OBJS) $(NATIVEOBJS) $(SCRIPTS)

all lint: $(PROGS) $(OBJS) \
	$(NATIVEDIR) .WAIT \
	$(NATIVEPROGS) $(NATIVEOBJS) .WAIT \
	$(SCRIPTS)

install: all .WAIT $(ROOTADBDIR) .WAIT $(ROOTPROGS) $(ROOTOBJS) $(ROOTSCRIPTS)

clean:
	-$(RM) $(OBJS) $(NATIVEOBJS)
	-$(RM) $(SCRIPTS:=.adb.c) $(SCRIPTS:=.run) $(SCRIPTS:=.adb.o)

clobber: clean
	-$(RM) $(PROGS) $(NATIVEPROGS)
	-$(RM) $(SCRIPTS)

# installation things

$(ROOTADBDIR)/%: %
	$(INS.file)

# specific build rules

adbgen:		$(COMMONDIR)/adbgen.sh
	$(RM) $@
	cat $(COMMONDIR)/adbgen.sh >$@
	$(CHMOD) +x $@

adbgen%:	$(COMMONDIR)/adbgen%.c
	$(LINK.c) -o $@ $< $(LDLIBS)
	$(POST_PROCESS)

adbsub.o:	$(COMMONDIR)/adbsub.c
	$(COMPILE.c) $(OUTPUT_OPTION) $(COMMONDIR)/adbsub.c
	$(POST_PROCESS_O)

$(NATIVEDIR)/adbgen:	$(COMMONDIR)/adbgen.sh
	$(RM) $@
	cat $(COMMONDIR)/adbgen.sh >$@
	$(CHMOD) +x $@

$(NATIVEDIR)/adbgen%:	$(COMMONDIR)/adbgen%.c
	$(NATIVECC) -o $@ $<
	$(POST_PROCESS)

$(NATIVEDIR)/adbsub.o:	$(COMMONDIR)/adbsub.c $(NATIVEDIR)
	$(NATIVECC) -c -o $@ $(COMMONDIR)/adbsub.c
	$(POST_PROCESS_O)

#
# the following section replaces the actions of adbgen.sh.
#
# there are two reuseable build macros, pattern-matching rules for kernel
# architectures, and required explicit dependencies.
#
BUILD.run= (unset LD_LIBRARY_PATH; \
	$(NATIVECC) ${OPTION_DEFINES} ${ARCHOPTS} $(NATIVEDEFS) ${INCLUDES} \
	$(CCYFLAG)${INCDIR} -o $@.run $@.adb.c $(NATIVEDIR)/adbsub.o)

BUILDC.run= (unset LD_LIBRARY_PATH; \
	$(NATIVECCC) ${ARCHOPTS} $(NATIVEDEFS) ${INCLUDES} \
	-I${INCDIR} -o $@.run $@.adb.c $(NATIVEDIR)/adbsub.o)

#
# note that we *deliberately* use the '-e' flag here to force the
# build to break if warnings result.  the right way to fix this
# is to repair the macro (or the header!), NOT to take the '-e' away.
#
BUILD.adb= ./$@.run -e > $@.runout && \
	$(NATIVEDIR)/adbgen3 < $@.runout | $(NATIVEDIR)/adbgen4 > $@

% : $(MCDIR)/%.adb
	$(NATIVEDIR)/adbgen1 < $< > $@.adb.c 
	(cat $(MCDIR)/adb.h $@.adb.c > $@.adb.in.c; \
	mv $@.adb.in.c $@.adb.c;)
	$(BUILDC.run)
	$(BUILD.adb)
	-$(RM) $@.adb.c $@.run $@.adb.o $@.runout

% : $(ISADIR)/%.adb
	$(NATIVEDIR)/adbgen1 < $< > $@.adb.c
	$(BUILD.run)
	$(BUILD.adb)
	-$(RM) $@.adb.c $@.run $@.adb.o $@.runout

% : $(COMMONDIR)/%.adb
	$(NATIVEDIR)/adbgen1 < $< > $@.adb.c
	$(BUILD.run)
	$(BUILD.adb)
	-$(RM) $@.adb.c $@.run $@.adb.o $@.runout

check:
	@echo $(SCRIPTS) | tr ' ' '\012' | sed 's/$$/&.adb/' |\
		sort > script.files
	@(cd $(ADB_BASE_DIR); ls *.adb) > actual.files
	diff script.files actual.files
	-$(RM) script.files actual.files

# the macro list is platform-architecture specific too.

maclist1:
	@(dir=`pwd`; \
	for i in $(SCRIPTS); do \
		echo "$$dir $$i"; \
	done)

