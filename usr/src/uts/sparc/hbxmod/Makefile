#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile 1.3     08/05/20 SMI"
#
# uts/sparc/hbxmod/Makefile
#

#
#	This makefile drives the production of the hbxmod `drv'
#	kernel module.
#

#
#	Path to the base of the uts directory tree (usually /usr/src/uts).
#
UTSBASE	= $(SRC)/uts

#
#	Define the module and object file sets.
#
MODULE		= hbxmod
OBJECTS		= $(HBXMOD_OBJS:%=$(OBJS_DIR)/%)
LINTFILES	= $(MODULE).ln
ROOTMODULE	= $(ROOT_STRMOD_DIR)/$(MODULE)
CONF_SRCDIR	= $(UTSBASE)/common/net/hbxmod

#
#	Include common rules.
#
include $(UTSBASE)/sparc/Makefile.sparc

CLOBBERFILES	+= $(LINTFILES)

# Set CHECK_FILES for cstyle/header check
CHECKHDRS 	= hbxmod.h
CHECK_FILES	+= $(HBXMOD_OBJS:%.o=%.c_check)

#
#	Define targets
#
ALL_TARGET	= $(BINARY)
INSTALL_TARGET	= $(BINARY) $(ROOTMODULE)

#
#	lint pass one enforcement
#
CFLAGS		+= -v
lint		:= AS_DEFS=

#
#	Default build targets.
#
.KEEP_STATE:

def:		$(DEF_DEPS)

all:		$(ALL_DEPS)

lint:		$(LINTFILES)

clean:		$(CLEAN_DEPS)

clobber:	$(CLOBBER_DEPS)

install:  	$(INSTALL_DEPS)

#
#	Include common targets.
#
include $(UTSBASE)/sparc/Makefile.targ

CCYFLAG=-I

%.ln:	$(CONF_SRCDIR)/%.c
	$(LINT.c) $(CPPFLAGS.master) $< > $@
	$(CAT) $@

include $(UTSBASE)/sparc/Makefile.cl
