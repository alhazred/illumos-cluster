#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile	1.9	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# uts/sparc/rsmrdt
#

#
#	This makefile drives the production of the did `drv'
#	kernel module.
#

#
#	Path to the base of the uts directory tree (usually /usr/src/uts).
#
UTSBASE	= $(SRC)/uts

#
#	Include common rules.
#
include $(UTSBASE)/sparc/Makefile.sparc

#
#	Define the module and object file sets.
#
MODULE		= rsmrdt 
OBJECTS		= $(RSMRDT_OBJS:%=$(OBJS_DIR)/%)
LINTFILES	= $(RSMRDT_OBJS:%.o=%.ln)
ROOTMODULE	= $(USR_DRV_DIR)/$(MODULE)
CONF_SRCDIR	= $(UTSBASE)/common/io/rsmrdt

CLOBBERFILES	+= $(LINTFILES)

# Set files for header/cstyle check
CHECKHDRS	= rsmrdt.h rsmrdt_if.h rsmrdt_path_int.h
CHECK_FILES	+= $(RSMRDT_OBJS:%.o=%.c_check)

#
#	Define targets
#
ALL_TARGET	= $(BINARY) $(SRC_CONFFILE)
INSTALL_TARGET	= $(BINARY) $(ROOTMODULE) $(ROOT_CONFFILE)

#
# path for rdtapi.h
#
INCLUDE_PATH += -I $(UTSBASE)/../lib/librdt/inc

#
#	lint pass one enforcement
#
CFLAGS		+= -v
lint	:= AS_DEFS=

#
#	Default build targets.
#
.KEEP_STATE:

def:		$(DEF_DEPS)

all:		$(ALL_DEPS)

lint:		$(LINTFILES)

clean:		$(CLEAN_DEPS)

clobber:	$(CLOBBER_DEPS)

install:  	$(INSTALL_DEPS)

#
#	Include common targets.
#
include $(UTSBASE)/sparc/Makefile.targ

CCYFLAG=-I

%.ln:	$(CONF_SRCDIR)/%.c
	$(LINT.c) $(CPPFLAGS.master) $< > $@
	cat $@

include $(UTSBASE)/sparc/Makefile.cl
