#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)SUNWscz.ips-processing.ksh	1.6	09/03/16 SMI"
#


PKGNAME=SUNWscz
PROG=${PKGNAME}.ips-processing.ksh
STARTTAG=$(starttag ${PKGNAME})
ENDTAG=$(endtag ${PKGNAME})


###########################################
#
# add entries to /etc/services
#
###########################################
update_etc_services()
{
    errs=0
    update_file=/etc/services

    # remove old entries from this package, if any
    remove_SC_edits ${update_file}
	
    # add new entries
    append_line_to_file ${update_file} '# ${STARTTAG}'	|| errs=$(expr ${errs} + 1)
    append_line_to_file ${update_file} 'pnmd		6499/tcp	# Sun Cluster pnm daemon' || errs=$(expr ${errs} + 1)
    append_line_to_file ${update_file} '# ${ENDTAG}'	|| errs=$(expr ${errs} + 1)

    if [ ${errs} -ne 0 ]; then
	echo "${PROG}: unable to update ${update_file}"
	return 1
    fi

    return 0
}

##################################################
# is_exclusive_zone()
# 
# Check if the zone is global or exclusive-ip zone.
#
# Return 0 if being installed in an exclusive-ip zone or global zone.
# Return 1 otherwise.
#
# Notes on coding:
#	* "read" has to be run inside a sub-shell (...), to avoid
#         variable assigments being lost
#	* "read" prefixed with '\' to avoid pkgmk WARNINGs
###################################################
is_exclusive_zone()
{
	zone_name=`/usr/bin/zonename`

	# Is this being run inside a non-global zone?
	if [ "${zone_name}" != "global" ]; then
		/usr/sbin/zoneadm -z ${zone_name} list -p | \
		(
			IFS=':' \read zoneid zone_name state zonepath uuid brand iptype endofline
			[ "${iptype}" = "shared" ] && return 1
			return 0
		) || return 1
	fi

	# is global or exclusive-ip zone
	return 0
}

###########################################
#
# postinstall
#
#	* Apply rbac updates (see sc_package_processing.ksh)
#	* Update /etc/services if global or exclusive zone
#
###########################################
ips_postinstall()
{
    errs=0

    # catch common signals
    trap 'echo "${PROG}: caught signal";  cleanup 3' 1 2 3 15

    rbac_add SUNWscz/auth_attr		|| errs=$(expr ${errs} + 1)
    rbac_add SUNWscz/exec_attr		|| errs=$(expr ${errs} + 1)
    rbac_add SUNWscz/prof_attr		|| errs=$(expr ${errs} + 1)

    is_exclusive_zone && (update_etc_services	|| errs=$(expr ${errs} + 1))

    if [ ${errs} -ne 0 ]; then
	cleanup 1
    else
	cleanup 0
    fi
} # ips_postinstall

###########################################################################
#
# preremove
#
#	* rbac updates are NOT removed (see sc_package_processing_lib.ksh)
#	* Remove file /var/cluster/nss_updated
#       * Remove our changes to /etc/services
#
###########################################################################
ips_preremove()
{
    errs=0

    # catch common signals
    trap 'echo "${PROG}: caught signal";  cleanup 3' 1 2 3 15

    #
    # Remove the nss_updated file for the zones. svc_ng_zones service
    # creates this file to record that nsswitch.conf file updates are
    # are already done. This file will be removed as part of this
    # pkg removal. This file is only available on non-global zones.
    #

    if [[ -f ${NSS_FILE} ]]
    then
        /usr/bin/rm -rf ${NSS_FILE}
    fi

    remove_SC_edits /etc/services	|| errs=$(expr ${errs} + 1)

    # services delivered in SUNWscz manifest_com
    # do smf manifest removal:
    #	disable, unregister and delete the service
    #	See also SUNWsc
    smf_manifest_rm \
	svc:/system/cluster/rpc-pmf:default \
	/var/svc/manifest/system/cluster/rpc_pmf.xml

    smf_manifest_rm \
	svc:/system/cluster/pnm:default \
	/var/svc/manifest/system/cluster/cl_pnm.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_failfast:default \
	/var/svc/manifest/system/cluster/sc_failfast.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_zc_member:default \
	/var/svc/manifest/system/cluster/sc_zc_member.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_pmmd:default \
	/var/svc/manifest/system/cluster/sc_pmmd.xml

    smf_manifest_rm \
	svc:/system/cluster/cl_execd:default \
	/var/svc/manifest/system/cluster/cl_execd.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_restarter:default \
	/var/svc/manifest/system/cluster/sc_delegated_restarter.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_ng_zones:default \
	/var/svc/manifest/system/cluster/sc_ng_zones.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_ifconfig_proxy:default \
	/var/svc/manifest/system/cluster/sc_ifconfig_proxy.xml

    smf_manifest_rm \
	svc:/system/cluster/cl-svc-enable:default \
	/var/svc/manifest/system/cluster/cl_svc_enable.xml

#    smf_manifest_rm \
#	svc:/system/cluster/sc_pnm_proxy_server:default \
#	/var/svc/manifest/system/cluster/sc_pnm_proxy_server.xml

    smf_manifest_rm \
	svc:/system/cluster/cl_boot_check:default \
	/var/svc/manifest/system/cluster/cl_boot_check.xml

    smf_manifest_rm \
	svc:/system/cluster/cl_boot_check_enabler:default \
	/var/svc/manifest/system/cluster/cl_boot_check_enabler.xml

    if [ ${errs} -ne 0 ]; then
	cleanup 1
    else
	cleanup 0
    fi
} # ips_preremove

###########################################
#
# postremove
#
###########################################
ips_postremove()
{
    return 0
} # ips_postremove
