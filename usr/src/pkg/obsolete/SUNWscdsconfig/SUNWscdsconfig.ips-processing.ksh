#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident   "@(#)SUNWscdsconfig.ips-processing.ksh 1.1     09/01/26 SMI"
#

PKGNAME=SUNWscdsconfig
PROG=${PKGNAME}.ips-processing.ksh


###########################################
#
# postinstall
#
###########################################
ips_postinstall()
{
    return 0

}

###########################################################################
#
# preremove for SUNWscdsconfig
#
#	* Remove the statefiles created by wizards
#
###########################################################################

ips_preremove()
{
    if [ -d /usr/cluster/lib/ds/history ]; then
	echo "Removing wizard state files"
	/usr/bin/rm -f /usr/cluster/lib/ds/history/* 2> /dev/null
    fi

    return 0
} # preremove

###########################################
#
# postremove
#
###########################################
ips_postremove()
{
    return 0
}
