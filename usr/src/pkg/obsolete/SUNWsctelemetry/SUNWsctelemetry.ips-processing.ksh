#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)SUNWsctelemetry.ips-processing.ksh	1.3	09/01/14 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Description:  preremove script for SUNsctelemetry
#

PKGNAME=SUNWsctelemetry
PROG=${PKGNAME}.ips-processing.ksh

RM=/usr/bin/rm
BACKLOG=/var/tmp/telemetry_backlog

###########################################
#
# postinstall
#
###########################################
ips_postinstall()
{
    return 0

}

#################################
#
#   preremove()
#
#################################
ips_preremove()
{
    #
    # remove the backlog file
    #

    if [ -f $BACKLOG ] ; then
	echo "Removing $BACKLOG"
	$RM -f $BACKLOG
    fi

    return 0
} # ips_preremove

###########################################
#
# postremove
#
###########################################
ips_postremove()
{
    return 0
}
