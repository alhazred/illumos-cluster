#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident "@(#)SUNWmdm.ips-processing.ksh   1.6     09/04/15 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

PKGNAME=SUNWmdm
PROG=${PKGNAME}.ips-processing.ksh
STARTTAG=$(starttag ${PKGNAME})
ENDTAG=$(endtag ${PKGNAME})


###########################################
#
# add entries to /etc/rpc
#
###########################################
update_etc_rpc()
{
    update_file=/etc/rpc

    # remove old entries, if any
    remove_SC_edits ${update_file}
	
    # add the new entries to file
    append_line_to_file ${update_file} "# ${STARTTAG}"
    append_line_to_file ${update_file} "metamedd        100242  metamedd	# Solaris Volume Manager mediator"
    append_line_to_file ${update_file} "metacld         100281  metacld	# Solaris Volume Manager cluster control"
    append_line_to_file ${update_file} "# ${ENDTAG}"

    return 0
}


###########################################
#
# postinstall
#
###########################################
ips_postinstall()
{
    # catch common signals
    trap 'echo "${PROG}: caught signal";  cleanup 3' 1 2 3 15

    update_etc_rpc
    cleanup 0

} # ips_ postinstall

###########################################
#
# preremove
#
###########################################
ips_preremove()
{
    # services delivered in SUNWmdm manifest_com
    # do smf manifest removal:
    #	disable, unregister and delete the service
    smf_manifest_rm \
	svc:/network/rpc/metacld:default \
	/var/svc/manifest/network/rpc/metacld.xml

    return 0
}

###########################################
#
# postremove
#
###########################################
ips_postremove()
{

    # catch common signals
    trap 'echo "Caught Signal"; cleanup 3' 1 2 3

    remove_SC_edits /etc/rpc	|| errs=`expr ${errs} + 1`

    if [ ${errs} -ne 0 ]; then
	cleanup 1
    else
	cleanup 0
    fi
} # ips_postremove
