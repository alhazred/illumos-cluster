#! /usr/xpg4/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)SUNWsc.ips-processing.ksh	1.5	09/04/15 SMI"
#


PKGNAME=SUNWsc
PROG=${PKGNAME}.ips-processing.ksh
STARTTAG=$(starttag ${PKGNAME})
ENDTAG=$(endtag ${PKGNAME})

TMPFILE1=/tmp/devlink_update.$$
TMPFILE2=/tmp/new.iu.ap.$$
PUBLIC_NETWORK_DEVICES="hme qfe eri ge ce bge e1000g ixge ixgb nge ipge vnet vsw"



###########################################
#
# add entries to /etc/devlinks.tab
#
###########################################
update_etc_devlinks()
{
	update_file=/etc/devlink.tab

	# remove the old entries
	remove_SC_edits ${update_file}

	# remove any errant did entries
	/usr/bin/grep -v "^type=ddi_pseudo;name=did;minor" ${update_file} > ${TMPFILE1}

	if [ ! -s ${TMPFILE1} ]; then
	    return 1
	fi

	/usr/bin/mv ${TMPFILE1} ${update_file}

	# add the new entries to the copy
	# not using append_line_to_file() just to keep this section more readable
/usr/bin/cat << EOF >> ${update_file}
# ${STARTTAG}
type=ddi_pseudo;name=did;minor=admin	did/admin
type=ddi_pseudo;name=did;minor3=blk	did/dsk/d\M2
type=ddi_pseudo;name=did;minor3=raw	did/rdsk/d\M2
type=ddi_pseudo;name=did;minor3=tp	did/rmt/\M2
# ${ENDTAG}
EOF

	if [ $? -ne 0 ]; then
		echo "${PROG}: unable to update ${update_file}"
		return 1
	fi

	return 0
}

###########################################
#
# Make /global/.devices/node@<ID> directories.
# /global and /global/.devices should have already
# been created as a result of entries in the package map.
#
###########################################
make_globaldevdirs()
{
	nodeid=1
	mkdirerrs=0
	chgerr=0
	dir=

	while [ ${nodeid} -le ${SC_MAXNODEID} ]
	do
		dir=/global/.devices/node@${nodeid}
		if [ ! -d ${dir} ]; then
			/usr/bin/mkdir -m 0755 ${dir}
			if [ $? -ne 0 ]; then
				mkdirerrs=`expr ${mkdirerrs} + 1`
				echo "${PROG}: failed to mkdir ${dir}"
			else
				chgerr=0
				/usr/bin/chown root ${dir} || `expr ${chgerr} + 1`
				/usr/bin/chgrp sys  ${dir} || `expr ${chgerr} + 1`
				if [ ${chgerr} -ne 0 ]; then
					mkdirerrs=`expr ${mkdirerrs} + 1`
					echo "${PROG}: failed to change group/owner for ${dir}"
				fi
			fi
		fi

		nodeid=`expr ${nodeid} + 1`
	done

	return ${mkdirerrs}
}

###########################################
#
# Insert "sd_retry_on_reservation_conflict=0"
# in sd.conf or ssd.conf.  The SCSI disk driver
# must not do retries when encountering reservation
# conflict conditions.
# Filename to update specified as $1.
###########################################
update_sd_conf()
{
	update_file=$1

	# If sd.conf or ssd.conf do not exist, we are done
	if [ ! -f "${update_file}" ]; then
		return 0
	fi

	# remove the old entries
	remove_SC_edits ${update_file}

	# insert the new line near the top of the file
	# not using append_line_to_file() because we want
	# specific placement in the file
	/usr/bin/ed -s ${update_file} << EOF >/dev/null 2>&1
/name=/i
# ${STARTTAG}
sd_retry_on_reservation_conflict=0;
# ${ENDTAG}
.
w
q
EOF

	# report any errors
	if [ $? -ne 0 ]; then
		echo "${PROG}: problem updating ${update_file}"
		return 1
	fi

	return 0
}

###########################################
#
# Convert to decimal by the specified base
# and value
#
###########################################
to_decimal()
{
	if [ -z "$1" ] || [ -z "$2" ]; then
		echo 0
	else
		new_hex=`echo $1 | /usr/xpg4/bin/tr '[a-f]' '[A-F]'`
		echo "ibase=$2; $new_hex" | /usr/bin/bc
	fi
}

###########################################
#
# Update /etc/system with entries
# rpcmod:svc_default_stksize and
# ge:ge_intr_mode
# ce:ce_taskq_disable (commented out)
# ipge:ipge_taskq_disable (commented out)
# ixge:ixge_taskq_disable (commented out)
#
# Also, enable IPv6 plumbing on interconnect if needed
#
###########################################
update_etc_system()
{
	update_file=/etc/system

	svc_stksize="0x6000"
	svc_value=`echo "${svc_stksize}" | /usr/bin/sed 's/^0x//'`
	svc_decimal=`expr 1 + \`to_decimal ${svc_value} "16"\` 2>/dev/null`

	# Find the max existing settings
	if [ -f ${update_file} ]; then
		all_svc_settings=`/usr/bin/cat ${update_file} | \
		    /usr/bin/grep '^set rpcmod:svc_default_stksize=' |\
		    /usr/bin/nawk -F'=' '{print $2}' 2>/dev/null`

		for val in ${all_svc_settings}
		do
			if echo "${val}" | /usr/bin/grep '^0x' > /dev/null; then
				setting=`echo "${val}" | sed 's/^0x//'`
				old_decimal=`expr 1 + \`to_decimal ${setting} "16"\` 2>/dev/null`
			elif echo "${val}" | /usr/bin/grep '^0' > /dev/null; then
				setting=`echo "${val}" | sed 's/^0//'`
				old_decimal=`expr 1 + \`to_decimal ${setting} "8"\` 2>/dev/null`
			else
				old_decimal=`expr 1 + ${val} 2>/dev/null`
			fi

			if [ ${old_decimal} -gt ${svc_decimal} ]; then
				svc_decimal=${old_decimal}
				svc_stksize=${val}
			fi
		done
	fi

	#
	# Check if IPv6 scalable service has been configured by looking
	# for "tcp6" and "udp6" in resource group config files.
	# If so, keep ifk_disable_v6 off so that these services wouldn't
	# break after install.
	#
	CCRDIR=/etc/cluster/ccr/global
	if [ -d $CCRDIR ]; then
		fnamelist=`/usr/bin/ls $CCRDIR/rgm_rg* 2>/dev/null |\
			/usr/bin/grep -v "\.bak$"`

		for fname in $fnamelist; do
			/usr/bin/egrep -e "tcp6|udp6" $fname >/dev/null 2>&1
			if [ $? -eq 0 ]; then
				dont_disable_v6=true
				break
			fi
		done
	fi

	# remove the old entries
	remove_SC_edits ${update_file}

	# add the new entries
	# not using append_line_to_file() to keep section more readable

/usr/bin/cat << EOF >> ${update_file}
* ${STARTTAG}
set rpcmod:svc_default_stksize=${svc_stksize}
set ge:ge_intr_mode=0x833
* Disable task queues and send all packets up to Layer 3
* in interrupt context.
* Uncomment the appropriate line below to use the corresponding 
* network interface as a Sun Cluster private interconnect. This 
* change will affect all corresponding network-interface instances. 
* For more information about performance tuning, see 
* http://www.sun.com/blueprints/0404/817-6925.pdf
* set ipge:ipge_taskq_disable=1
* set ixge:ixge_taskq_disable=1
* set ce:ce_taskq_disable=1

EOF

	if [ -n "$dont_disable_v6" ]; then
/usr/bin/cat << EOF >> ${update_file}
set cl_comm:ifk_disable_v6=0
EOF
	fi

/usr/bin/cat << EOF >> ${update_file}
* ${ENDTAG}
EOF

	if [ $? -ne 0 ]; then
		echo "${PROG}: unable to update ${update_file}"
		return 1
	fi

	return 0
}

###########################################
#
# Set up a link in the appropriate rcX.d directory for
# did_update_vfstab.  The script will remove the link
# after the system has run once in clustered mode.
#
###########################################
link_did_rcscript()
{

	rcfile=/etc/rcS.d/S68did_update_vfstab

	if [ -f "${rcfile}" ]; then
		rm -f ${rcfile}
	fi

	/usr/bin/ln /etc/init.d/did_update_vfstab ${rcfile}

	if [ $? -ne 0 ]; then
		echo "${PROG}: problem linking did_update_vfstab"
		return 1
	fi

	return 0
}

###########################################
#
# Update /etc/iu.ap file. Known public network
# devices will be configured to have the clhbsndr
# streams module autopushed.
#
###########################################
update_etc_iu_ap()
{
	update_file=/etc/iu.ap

	# If /etc/iu.ap does not exist, create it
	if [ ! -f "${update_file}" ]; then
		/usr/bin/cat << EOF >> ${update_file}
#
# Autopush setup
#

EOF
		if [ $? -ne 0 ]; then
			echo "${PROG}: problem updating ${update_file}"
			return 1
		fi
	fi

	# remove the old entries
	remove_SC_edits ${update_file}

	#
	# Add an entry for each known public network device to the the
	# /etc/iu.ap file. The streams module clhbsndr is configured to
	# be pushed right above the device driver. If an entry already
	# exists, the entry is updated to include the clhbsndr module.
	# The following awk script creates the new /etc/iu.ap file after
	# consulting the existing file. The main block updates existing
	# entries. The END block adds entries for devices that did not
	# already exist in the file.
	#
	/usr/bin/awk 'BEGIN {modified = 0; ndevs = 0} \
		   { \
			if (NR == 1) ndevs = split(devices, dev, " "); \
			for (i = 1; i <= ndevs; i++) { \
				if ($1 == dev[i]) { \
					found[dev[i]] = 1 \
				} \
			} \
			if (found[$1] != 1) { \
				print \
			} else if ($4 != "clhbsndr") { \
				modified++; \
				for (i = 1; i <= 3; i++) printf("\t%s", $i); \
				printf("\tclhbsndr"); \
				for (i = 4; i <= NF; i++) printf(" %s", $i); \
				printf("\n"); \
			} else { \
				print \
			} \

		   } \
	     END   { \
			if (ndevs == 0) ndevs = split(devices, dev, " "); \
			printf("# %s\n", st); \
			if (modified > 0) { \
				printf("# In addition to these new lines,"); \
				printf(" some preexisting lines above\n"); \
				printf("# might have been modified to"); \
				printf(" include the module clhbsndr\n"); \
			} \
			for (i = 1; i <= ndevs; i++) { \
				if (found[dev[i]] != 1) { \
					printf("\t%s\t-1\t0\tclhbsndr\n", dev[i]) \
				} \
			} \
			printf("# %s\n", et) \
		   }' "devices=$PUBLIC_NETWORK_DEVICES" "st=$STARTTAG" \
			 "et=$ENDTAG" ${update_file} > ${TMPFILE2} \
			2>/dev/null

	if [ $? -ne 0 ]; then
		echo "${PROG}: problem updating ${update_file}"
		return 1
	fi

	/usr/bin/mv -f ${TMPFILE2} ${update_file}

	if [ $? -ne 0 ]; then
		echo "${PROG}: problem updating ${update_file}"
		return 1
	fi

	return 0
}

###########################################
#
# add entries to /kernel/drv/log.conf to turn on
# Solaris message id logging.
#
###########################################
update_kernel_drv_log_conf()
{
    update_file=/kernel/drv/log.conf

    # remove the old entries
    remove_SC_edits ${update_file}

    # add the new entries to file
    append_line_to_file ${update_file} "# ${STARTTAG}"
    append_line_to_file ${update_file} "msgid=1;"
    append_line_to_file ${update_file} "# ${ENDTAG}" 

    return 0
}

###########################################
#
# add entries to /var/spool/cron/crontabs/root
#
###########################################
update_root_crontab()
{
	update_file=/var/spool/cron/crontabs/root

	# remove the old entries
	remove_SC_edits ${update_file}

	# add the new entries to the copy
/usr/bin/cat << EOF >> ${update_file}
# ${STARTTAG}
20 4 * * 0 /usr/cluster/lib/sc/newcleventlog /var/cluster/logs/eventlog
20 4 * * 0 /usr/cluster/lib/sc/newcleventlog /var/cluster/logs/DS
20 4 * * 0 /usr/cluster/lib/sc/newcleventlog /var/cluster/logs/commandlog
# ${ENDTAG}
EOF

	if [ $? -ne 0 ]; then
		echo "${PROG}: unable to update ${update_file}"
		return 1
	fi

	return 0
}


###########################################
#
# add entries to /etc/rpc
#
###########################################
update_etc_rpc()
{
	update_file=/etc/rpc

	# remove the old entries
	remove_SC_edits ${update_file}
	
	# add the new entries to the copy
/usr/bin/cat << EOF >> ${update_file}
# ${STARTTAG}
scadmd		100145
scrcmd		100533
# ${ENDTAG}
EOF

	if [ $? -ne 0 ]; then
		echo "${PROG}: unable to update ${update_file}"
		return 1
	fi

	return 0
}

###########################################
#
# Remove /global/.devices/node@<ID> directories.
# /global and /global/.devices will be removed
# as a result of entries in the package map.
#
###########################################
remove_globaldevdirs()
{
	nodeid=1
	dir=

	while [ ${nodeid} -le ${SC_MAXNODEID} ]
	do
		dir=/global/.devices/node@${nodeid}
		/usr/bin/rmdir ${dir} 2>/dev/null
		nodeid=`expr ${nodeid} + 1`
	done

	return 0
}

###########################################
#
# Remove Sun Cluster servicetag
#
###########################################
remove_svtag() 
{
	STCLIENT=/usr/bin/stclient
	CL_URN_FILE=/var/sadm/servicetag/cl.urn

	if [ -f ${CL_URN_FILE} ]; then
	   # read the urn from the file
	   URN=`/usr/bin/cat ${CL_URN_FILE}`
	   if [ -f ${STCLIENT} ]; then
	      ${STCLIENT} -d -i ${URN} >/dev/null 2>&1
	   fi
	   rm -f ${CL_URN_FILE}
	fi
	return 0
}

###########################################
#
# postinstall
#
#	* Update /etc/devlinks
#	* Make /global/.devices/node@${nodeid} directories
#	* Insert "sd_retry_on_reservation_conflict=0" in sd.conf
#	* Insert "sd_retry_on_reservation_conflict=0" in ssd.conf
#	* Add appropriate lines to /etc/system
#	* Set up a temporary did_update_vfstab link in rc directory
#	* Update /etc/iu.ap file to include clhbsndr for public net devices
#	* add entries to /var/spool/cron/crontabs/root
#	* add entry to /kernel/drv/log.conf to turn on message id logging.
#	* Update /etc/rpc
#	* Update /etc/services
#
###########################################
ips_postinstall()
{
    errs=0

    # catch common signals
    trap 'echo "${PROG}: caught signal";  cleanup 3' 1 2 3 15

    update_etc_devlinks		|| errs=$(expr ${errs} + 1)
    make_globaldevdirs		|| errs=$(expr ${errs} + 1)
    update_sd_conf /kernel/drv/sd.conf	|| errs=$(expr ${errs} + 1)
    update_sd_conf /kernel/drv/ssd.conf	|| errs=$(expr ${errs} + 1)
    update_etc_system		|| errs=$(expr ${errs} + 1)
    link_did_rcscript		|| errs=$(expr ${errs} + 1)
    update_etc_iu_ap		|| errs=$(expr ${errs} + 1)
    update_root_crontab		|| errs=$(expr ${errs} + 1)
    update_kernel_drv_log_conf	|| errs=$(expr ${errs} + 1)
    update_etc_rpc		|| errs=$(expr ${errs} + 1)

    # make sure next reboot is a reconfig reboot
    touch /reconfigure

    if [ ${errs} -ne 0 ]; then
	cleanup 1
    else
	cleanup 0
    fi
} # ips_postinstall

###########################################
#
# preremove
#
###########################################
ips_preremove()
{
    # services delivered in SUNWsc manifest_com
    # do smf manifest removal:
    #	disable, unregister and delete the service
    #	See also SUNWscz

    smf_manifest_rm \
	svc:/network/rpc/scadmd:default \
	/var/svc/manifest/network/rpc/scadmd.xml

    smf_manifest_rm \
	svc:/network/rpc/scrcmd:default \
	/var/svc/manifest/network/rpc/scrcmd.xml

    smf_manifest_rm \
	svc:/network/multipath:cluster \
	/var/svc/manifest/network/network-mpathd.xml

    smf_manifest_rm \
	svc:/system/cluster/cl-ccra:default \
	/var/svc/manifest/system/cluster/cl_ccra.xml

    smf_manifest_rm \
	svc:/system/cluster/scmountdev:default \
	/var/svc/manifest/system/cluster/scmountdev.xml

    smf_manifest_rm \
	svc:/system/cluster/loaddid:default \
	/var/svc/manifest/system/cluster/loaddid.xml

    smf_manifest_rm \
	svc:/system/cluster/bootcluster:default \
	/var/svc/manifest/system/cluster/bootcluster.xml

    smf_manifest_rm \
	svc:/system/cluster/clexecd:default \
	/var/svc/manifest/system/cluster/clexecd.xml

    smf_manifest_rm \
	svc:/system/cluster/initdid:default \
	/var/svc/manifest/system/cluster/initdid.xml

    smf_manifest_rm \
	svc:/system/cluster/globaldevices:default \
	/var/svc/manifest/system/cluster/globaldevices.xml

    smf_manifest_rm \
	svc:/system/cluster/gdevsync:default \
	/var/svc/manifest/system/cluster/gdevsync.xml

    smf_manifest_rm \
	svc:/system/cluster/ql_upgrade:default \
	/var/svc/manifest/system/cluster/ql_upgrade.xml

    smf_manifest_rm \
	svc:/system/cluster/ql_rgm:default \
	/var/svc/manifest/system/cluster/ql_rgm.xml

    smf_manifest_rm \
	svc:/system/cluster/mountgfs:default \
	/var/svc/manifest/system/cluster/mountgfs.xml

    smf_manifest_rm \
	svc:/system/cluster/clusterdata:default \
	/var/svc/manifest/system/cluster/clusterdata.xml

    smf_manifest_rm \
	svc:/system/cluster/cl-event:default \
	/var/svc/manifest/system/cluster/cl_event.xml

    smf_manifest_rm \
	svc:/system/cluster/cl-eventlog:default \
	/var/svc/manifest/system/cluster/cl_eventlog.xml

    smf_manifest_rm \
	svc:/system/cluster/rgm-starter:default \
	/var/svc/manifest/system/cluster/rgm_starter.xml

    smf_manifest_rm \
	svc:/system/cluster/scslm:default \
	/var/svc/manifest/system/cluster/slm.xml

    smf_manifest_rm \
	svc:/system/cluster/scslmclean:default \
	/var/svc/manifest/system/cluster/slmclean.xml

    smf_manifest_rm \
	svc:/system/cluster/rpc-fed:default \
	/var/svc/manifest/system/cluster/rpc_fed.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_zones:default \
	/var/svc/manifest/system/cluster/sc_zones.xml

    smf_manifest_rm \
	svc:/system/cluster/scprivipd:default \
	/var/svc/manifest/system/cluster/scprivipd.xml

    smf_manifest_rm \
	svc:/system/cluster/scdpm:default \
	/var/svc/manifest/system/cluster/scdpm.xml

    smf_manifest_rm \
	svc:/system/cluster/cznetd:default \
	/var/svc/manifest/system/cluster/cznetd.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_ifconfig_server:default \
	/var/svc/manifest/system/cluster/sc_ifconfig_server.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_rtreg_server:default \
	/var/svc/manifest/system/cluster/sc_rtreg_server.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_pnm_proxy_server:default \
	/var/svc/manifest/system/cluster/sc_pnm_proxy_server.xml

    smf_manifest_rm \
	svc:/system/cluster/zc_cmd_log_replay:default \
	/var/svc/manifest/system/cluster/zc_cmd_log_replay.xml

    smf_manifest_rm \
	svc:/system/cluster/cl-svc-cluster-milestone:default \
	/var/svc/manifest/system/cluster/cl_svc_cluster_milestone.xml

    smf_manifest_rm \
	svc:/system/cluster/sc_svtag:default \
	/var/svc/manifest/system/cluster/sc_svtag.xml

    smf_manifest_rm \
	svc:/system/cluster/scqdm:default \
	/var/svc/manifest/system/cluster/scqdm.xml

    return 0
}

###########################################
#
# postremove for SUNWsc
#
#	* Remove /global/.devices/node@${nodeid} directories
#	* Remove servicetag
#	* Remove our changes to files:
#		/etc/devlink.tab
#		/kernel/drv/sd.conf
#		/kernel/drv/ssd.conf
#		/etc/system
#		/etc/iu.ap
#		/kernel/drv/log.conf
#		/var/spool/cron/crontabs/root
#		/etc/inetd.conf
#		/etc/rpc
#		/etc/services
#
###########################################
ips_postremove()
{
    errs=0

    # catch common signals
    trap 'echo "${PROG}: caught signal";  cleanup 3' 1 2 3 15

    remove_globaldevdirs	|| errs=$(expr ${errs} + 1)
    remove_svtag		|| errs=$(expr ${errs} + 1)

    remove_SC_edits /etc/devlink.tab	|| errs=$(expr ${errs} + 1)
    remove_SC_edits /kernel/drv/sd.conf	|| errs=$(expr ${errs} + 1)
    remove_SC_edits /kernel/drv/ssd.conf	|| errs=$(expr ${errs} + 1)
    remove_SC_edits /etc/system		|| errs=$(expr ${errs} + 1)
    remove_SC_edits /etc/iu.ap		|| errs=$(expr ${errs} + 1)
    remove_SC_edits /kernel/drv/log.conf	|| errs=$(expr ${errs} + 1)
    remove_SC_edits /var/spool/cron/crontabs/root || errs=$(expr ${errs} + 1)

    remove_SC_edits /etc/rpc		|| errs=$(expr ${errs} + 1)
    remove_SC_edits /etc/services	|| errs=$(expr ${errs} + 1)

    # this file is a link to a file in /etc/init.d
    # the link is created in SUNWsc.postinstall and self-deletes
    # on first execution
    # perform an insurance rm here just in case it never ran
    # and consequently didn't self-delete
    /usr/bin/rm -f /etc/rcS.d/S68did_update_vfstab

    if [ ${errs} -ne 0 ]; then
	cleanup 1
    else
	cleanup 0
    fi
} # ips_postremove
