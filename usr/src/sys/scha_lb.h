/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCHA_LB_H_
#define	_SCHA_LB_H_

#pragma ident	"@(#)scha_lb.h	1.11	08/08/01 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * This part of the library provides an interface between client applications
 * and the networking subsystem in order to control the load balancing.
 */

#include <netinet/in.h>

/*
 * dist_info holds information on the current load distribution weights.
 * It also holds a status vector of which nodes have running instances,
 * failed instances, or no instances.
 * num_nodes will be big enough to hold information on all the service
 * instances running, but may be bigger or smaller than the actual number
 * of nodes in the cluster.
 * Networking passes a dist_info to applications.  For suitable policies,
 * the application returns the appropriate weights.
 *
 * Note: the actual data is stored following the specified fields in the
 * structure, so a single free can be used to free the structure and data.
 * Note: since the node numbers start at node 1, the nodes and weights arrays
 * have an unused first position.
 */

/*
 * instance_up indicates an instance is running on the node
 * instance_down indicates the node is configured with an instance,
 *	but the instance or node is down.
 * no_instance indicates the node is not configured for an instance.
 */

enum node_status {instance_up = 0, instance_down = 1, no_instance = 2};

struct dist_info {
	int num_nodes;		/* Number of entries in the lists */
	enum node_status *nodes;	/* Pointer to list of node status */
	int *weights;		/* Pointer to list of weights */
};

struct weight_info {
	int num_nodes;		/* Number of entries in the lists */
	int *weights;		/* Pointer to list of weights */
};

/*
 *	Get the current policy type.
 *	Passes result out in type.
 */
enum policy_type {LB_WEIGHTED, LB_PERF, LB_ST, LB_SW};
int scha_ssm_lb_get_policy_type(const char *resource, int *type);
int scha_ssm_lb_get_policy_type_zone(const char *, const char *, int *);

/*
 *	Get the current distribution.  dist_info must be freed with free when
 *	done.
 */
int scha_ssm_lb_get_distribution(const char *resource, struct dist_info **info);
int scha_ssm_lb_get_distribution_zone(const char *,
    const char *resource, struct dist_info **info);

/*
 *	Parse a load balancing info string.
 *	Sets buf to point to a malloc'd buffer, which must be freed later.
 */
int scha_ssm_lb_parse_lb_info(const char *string, enum policy_type policy,
	void **buf);
int scha_ssm_lb_parse_lb_info_zone(const char *,
    const char *string, enum policy_type policy,
	void **buf);

/*
 *	Set the load balancer from a previously parsed string.
 */
int scha_ssm_lb_set_lb_info(const char *resource, enum policy_type policy,
	void *buf);
int scha_ssm_lb_set_lb_info_zone(const char *,
    const char *resource, enum policy_type policy,
	void *buf);

/*
 *	The following only work with appropriate load balancing policies.
 */

/*
 *	Set the distribution.
 */
int scha_ssm_lb_set_distribution_zone(const char *, const char *resource,
	struct weight_info *info);
int scha_ssm_lb_set_distribution(const char *resource,
	struct weight_info *info);

/*
 *	Set the distribution weight for one node only.
 */
int scha_ssm_lb_set_one_distribution_zone(const char *,
    const char *resource, int node,
	int weight);
int scha_ssm_lb_set_one_distribution(const char *resource, int node,
	int weight);

/*
 *	The following only work with performance monitor policies.
 *	The performance monitor policy adjusts the weights based on
 *	performance monitor measurements.  Nodes with above-average
 *	measurements get their weights decreased, while nodes with below-
 *	average measurements get their weights increased.  That is,
 *	higher is worse for the measurements.
 */

/*
 *	Set the perf_mon status associated with the specified node,
 *	when a response-time policy is in use.  The interpretation of
 *	load is up to the policy.
 */
int scha_ssm_lb_set_perf_mon_status_zone(const char *,
    const char *resource, int node,
	unsigned int load);
int scha_ssm_lb_set_perf_mon_status(const char *resource, int node,
	unsigned int load);

/*
 *	Parameters controlling the perf_mon policy.
 *	The perf_mon policy balances load by computing the average load.
 *	Each node with load above average gets its weighting dropped, while
 *	each node with load below average gets its weighting increased.
 */
struct perf_mon_parameters {
/*
 *	Update frequency, in seconds, specifies how often the load balancer
 *	re-balancs the load
 */
	unsigned int	update_frequency;	/* In seconds */
/*
 *	scale_rate specifies how many units difference from the average load
 *	are required to cause a change of 1 in the weighting.
 */
	unsigned int	scale_rate;
/*
 *	min_weight and max_weight restrict the output weights to the given
 *	range. This keeps a lightly loaded node from getting arbitrarily
 *	large weights, and keeps a more heavily loaded node from losing all
 *	its weight.
 */
	unsigned int	min_weight;
	unsigned int	max_weight;
/*
 *	If a node is within quiet_width units of the average load, its weight
 *	is not changed.  This keeps the load balancer from shifting load due
 *	to small fluctuations that are basically noise.
 */
	unsigned int	quiet_width;
/*
 *	If a node's load is below floor, its weight is not decreased.  This
 *	keeps the load balancer from shifting load due to fluctuations in
 *	nodes that are basically unloaded.
 */
	unsigned int	floor;
};

/*
 * Modify the policy parameters.
 */
int scha_ssm_lb_set_perf_mon_parameters(const char *resource,
	struct perf_mon_parameters *params);
int scha_ssm_lb_set_perf_mon_parameters_zone(const char *, const char *resource,
	struct perf_mon_parameters *params);

/*
 * Get the policy parameters.
 * The parameters struct needs to be free'd by the caller.
 */
int scha_ssm_lb_get_perf_mon_parameters(const char *resource,
	struct perf_mon_parameters **params);
int scha_ssm_lb_get_perf_mon_parameters_zone(const char *, const char *resource,
	struct perf_mon_parameters **params);

#ifdef __cplusplus
}
#endif

#endif	/* !_SCHA_LB_H_ */
