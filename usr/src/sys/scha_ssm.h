/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCHA_SSM_H_
#define	_SCHA_SSM_H_

#pragma ident	"@(#)scha_ssm.h	1.18	08/08/01 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/* Include for sa_family_t */
#include <netinet/in.h>

/*
 * Exceptions raised by this interface.
 * NOTE: If any additional error codes are defined then a compliant
 * change has to be made in the routine ssm_err_code_to_mesg() to
 * be able to covert the new error code to an error message. This
 * routine can be updated in src/clconf/ssm.cc.
 */

typedef enum {
	SCHA_SSM_SUCCESS,
	SCHA_SSM_NO_GROUP,
	SCHA_SSM_INVALID_SERVICE_GROUP,
	SCHA_SSM_GROUP_EXISTS,
	SCHA_SSM_SERVICE_EXISTS,
	SCHA_SSM_INVALID_PROTOCOL,
	SCHA_SSM_INVALID_ADDRESS,
	SCHA_SSM_INVALID_PORTNUM,
	SCHA_SSM_INVALID_POLICY,
	SCHA_SSM_INVALID_NODEID,
	SCHA_SSM_NO_SERVICE,
	SCHA_SSM_NO_LBSTRING,
	SCHA_SSM_NOSUCH_SERVICE,
	SCHA_SSM_NODE_EXISTS,
	SCHA_SSM_NO_NODEID,
	SCHA_SSM_NO_MEMORY,
	SCHA_SSM_INTERNAL_ERROR,
	SCHA_SSM_CANNOT_ADD_NODEID,
	SCHA_SSM_COMM_ERROR,
	SCHA_SSM_SEQ_ERROR,
	SCHA_SSM_MOD_NOTLOADED,
	SCHA_SSM_SERVICE_CONFLICTS,
	SCHA_SSM_INVALID_STICKY_TIMEOUT
} scha_ssm_exceptions;

/*
 * Load balancing policy
 */
typedef enum {
	scha_ssm_lb_weighted,  /* Load-balancing using specified node weights */
	scha_ssm_lb_perf, /* Load-balancing based on perf_mon monitor */
	scha_ssm_lb_user, /* Load-balancing based on user-def. policies */
	scha_ssm_lb_st,   /* Restricted load balancing */
	scha_ssm_lb_sw    /* Restricted load balancing (all ports) */
} scha_ssm_policy_t;

/*
 * Interface functions for SSM operations
 */

scha_ssm_exceptions
scha_ssm_address_ip(const char *rs_name, const char *rg_name, int protocol,
	int nodeid);
scha_ssm_exceptions
scha_ssm_address_ip_zone(const char *,
    const char *rs_name, const char *rg_name, int protocol,
	int nodeid);

#ifdef __cplusplus
}
#endif

#endif	/* !_SCHA_SSM_H_ */
