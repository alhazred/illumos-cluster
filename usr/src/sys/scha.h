/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCHA_H_
#define	_SCHA_H_

#pragma ident	"@(#)scha.h	1.50	08/07/28 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <scha_tags.h>
#include <scha_types.h>
#include <scha_err.h>

/*
 * Access functions for information on resources, resource types,
 * resource groups and general cluster information.
 * The information to be accessed is indicated by the tag arguments.
 * Valid tags are declared in scha_tags.h
 * Values, of type determined by the tag argument, are returned in
 * variable argument out parameters.
 *
 * Open functions set up connections and memory management.
 * Close functions disconnect and reclaim memory.
 * Get functions access information indicated by operation tag value.
 *
 */

scha_err_t scha_resource_open(const char *rname, const char *rgname,
    scha_resource_t *handle);
scha_err_t scha_resource_close(scha_resource_t handle);
scha_err_t scha_resource_get(scha_resource_t handle,
    const char *tag, ...);
scha_err_t scha_resource_open_zone(const char *cluster,
    const char *rname, const char *rgname, scha_resource_t *handle);
#define	scha_resource_close_zone(a, b)	scha_resource_close(b);
scha_err_t scha_resource_get_zone(const char *cluster, scha_resource_t handle,
    const char *tag, ...);

scha_err_t scha_resourcetype_open(const char *rtname,
    scha_resourcetype_t *handle);
scha_err_t scha_resourcetype_close(scha_resourcetype_t handle);
scha_err_t scha_resourcetype_get(scha_resourcetype_t handle,
    const char *tag, ...);
scha_err_t scha_resourcetype_open_zone(const char *cluster, const char *rtname,
    scha_resourcetype_t *handle);
#define	scha_resourcetype_close_zone(a, b)	scha_resourcetype_close(b);
scha_err_t scha_resourcetype_get_zone(const char *cluster,
    scha_resourcetype_t handle, const char *tag, ...);

extern scha_err_t scha_resourcegroup_open(const char *rgname,
    scha_resourcegroup_t *handle);
extern scha_err_t scha_resourcegroup_close(scha_resourcegroup_t handle);
extern scha_err_t scha_resourcegroup_get(scha_resourcegroup_t handle,
    const char *tag, ...);
extern scha_err_t scha_resourcegroup_open_zone(const char *cluster,
    const char *rgname, scha_resourcegroup_t *handle);
#define	scha_resourcegroup_close_zone(a, b)	scha_resourcegroup_close(b);
scha_err_t scha_resourcegroup_get_zone(const char *cluster,
    scha_resourcegroup_t handle,
    const char *tag, ...);

scha_err_t scha_cluster_open(scha_cluster_t *handle);
scha_err_t scha_cluster_close(scha_cluster_t handle);
scha_err_t scha_cluster_get(scha_cluster_t, const char *, ...);
scha_err_t scha_cluster_open_zone(const char *, scha_cluster_t *);
#define	scha_cluster_close_zone(a, b) scha_cluster_close(b);
scha_err_t scha_cluster_get_zone(
    const char *, scha_cluster_t, const char *, ...);

/*
 * Access functions for cluster logging facility and local node name.
 */
scha_err_t scha_cluster_getlogfacility(int *logfacility);
scha_err_t scha_cluster_getnodename(char **nodename);
scha_err_t scha_cluster_getzone(char **nodename);
#define	scha_cluster_getlogfacility_zone(x, y) scha_cluster_getlogfacility(y)
scha_err_t scha_cluster_getnodename_zone(const char *, char **);

/*
 * Set status of a resource - for use by resource monitors
 */
scha_err_t scha_resource_setstatus(const char *rname,
    const char *rgname,	scha_rsstatus_t status,	const char *status_msg);
scha_err_t scha_resource_setstatus_zone(const char *rname,
    const char *rgname,	const char *zonename, scha_rsstatus_t status,
    const char *status_msg);

/*
 * scha_control() - request a resource group GIVEOVER or RESTART
 */

scha_err_t scha_control(const char *tag, const char *rgname,
    const char *rname);

scha_err_t scha_control_zone(const char *tag, const char *rgname,
    const char *rname, const char *zonename);

/*
 * scha_strerror_i18n()
 * Converts the given scha_err_t error code into an
 * internationalized error message.
 * Caller should not free the returned string.
 *
 */
char *scha_strerror_i18n(scha_err_t err_code);

/*
 * scha_strerror()
 *
 * Converts the given scha_err_t error code into an error message,
 * which is a non-internationalized string.
 * Caller should not free the returned string.
 *
 */
char *scha_strerror(scha_err_t err_code);

#ifdef __cplusplus
}
#endif

#endif	/* _SCHA_H_ */
