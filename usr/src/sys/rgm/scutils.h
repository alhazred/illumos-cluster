/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCUTILS_H
#define	_SCUTILS_H

#pragma ident	"@(#)scutils.h	1.19	08/05/20 SMI"

/*
 * scutils.h
 * header file for libscutils
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <syslog.h>
#include <stdarg.h>

#ifdef linux
#include <signal.h>
#endif

/* for tracking of strings that are not translated */
#define		NOGET(s)	s


/*
 * function defs
 */
/* debug and error reporting functions */
void dbg_msgout(const char *msg, ...);
void dbg_msgout_nosyslog(const char *msg, ...);
void dbg_msgout_onlysyslog(int priority, const char *msg, ...);

/* general server initialization functions */
char *getlocalhostname(void);
int svc_init(int dbg);
#ifdef linux
int svc_sigprocmask(sigset_t *set);
int svc_sigrestoremask(void);
#endif
int svc_restore_priority(void);
void *svc_alloc(size_t size, int *alloc_cnt);
void svc_free(void *old, int *alloc_cnt);
char *svc_strdup(const char *str, int *alloc_cnt);
void make_daemon(void);
void make_unique(char *);
int svc_setschedprio_proc(pid_t pid);
int set_microstate_accounting(void);

/* pre-allocation utility */
int pre_alloc_heap(size_t max_swap_size, size_t min_swap_size);

#ifdef __cplusplus
}
#endif

#endif	/* !_SCUTILS_H */
