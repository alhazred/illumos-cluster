/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCZONES_H
#define	_SCZONES_H

#pragma ident	"@(#)sczones.h	1.6	08/07/21 SMI"

#include <sys/sol_version.h>
#include <sys/priocntl.h>

/*
 * sczones.h
 *
 * public header file for libsczones
 *
 * Should be included by clients of libsczones.
 */

#ifdef __cplusplus
extern "C" {
#endif

#if SOL_VERSION >= __s10
typedef void (*zone_state_callback_t)(const char *);

/*
 * register_zone_state_callbacks
 * -----------------------------
 * Registers with the sc_zonesd to receive callbacks when zones on the
 * system change state.
 *
 * Arguments:
 * callback_ptrs is an array of pointers to functions to handle the
 * varous state changes. The current implementation takes at most five
 * function ptrs, to handle the STARTING, UP, DOWN, SHUTTING_DOWN, and STUCK
 * zone state changes, in that order. That is, the first ptr in the array
 * will be called for STARTING transitions, the second for UP, etc. NULL can
 * be used if you do not wish to receive callbacks for a particular
 * transition.
 *
 * Note that a zone is not considered UP until it has reached the multi-user
 * milestone and all necessary clustering infrastructure is up and running.
 *
 * The num_callback_ptrs argument specifies the size of the callback_ptrs
 * array. Currently at most 5 elements are supported.
 *
 * The callbacks take one argument: zonename. The memory of this string
 * will go away as soon as the callback returns, so clients should copy it
 * right away.
 */
int
register_zone_state_callbacks(zone_state_callback_t callback_ptrs[],
    int num_callback_ptrs);

int
unregister_zone_state_callbacks(void);
#endif

#ifndef linux
int sc_get_rtparameters(pcparms_t *pc_parms);
int sc_priocntl(pid_t pid, pcparms_t *parms);
#endif

/*
 * the function is called by cluster commands to verify global zone privileges
 * if called from non global zone, prints error message and exits with error
 */
int
sc_zonescheck(void);

/* non verbose version of above sc_zonescheck() */
int
sc_nv_zonescheck(void);

/*
 * this function is called called by rgm routines to determine whether the
 * zone is configured on the current node ir not
 */
boolean_t
sc_zone_configured_on_cur_node(const char *zname);

#ifdef __cplusplus
}
#endif

#endif	/* !_SCZONES_H */
