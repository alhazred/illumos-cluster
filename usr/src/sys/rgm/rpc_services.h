/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RPC_SERVICES_H
#define	_RPC_SERVICES_H

#pragma ident	"@(#)rpc_services.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <rpc/rpc.h>

typedef enum rpc_error {
	RPC_SERVICES_OK	= 0,		/* OK */
	RPC_SERVICES_ERROR = 1,		/* Error */
	RPC_SERVICES_EPERM = 2,		/* permission */
	RPC_SERVICES_ENOMEM = 3,	/* Not enough memory */
	RPC_SERVICES_EINVAL = 4,	/* Invalid param */
	RPC_SERVICES_ETIMEDOUT = 5,	/* Timeout */
	RPC_SERVICES_ENOSYS = 6,	/* No system */
	RPC_SERVICES_EDEADLK = 7,	/* Dead lock */
	RPC_SERVICES_EUNEXPECTED = 8	/* Unexpected */
} rpc_error_t;

/*
 *  for BUG 5018264 Can't pass NULL param to rpc calls
 */
#define	xdr_string(a, b, c) linux_xdr_string(a, b, c)

typedef bool_t(*dispatch_call_t) (char *, void *, struct svc_req *);

rpc_error_t rpc_init_rpc_mgr(int thread_count, int debug, int stksz_flag);
rpc_error_t rpc_cleanup_rpc_mgr(int thread_count);
rpc_error_t rpc_req_queue_append(register SVCXPRT *transp, int arg_size,
    xdrproc_t xdr_arg, xdrproc_t xdr_result, int res_size,
    dispatch_call_t func_ptr, struct svc_req *svc_reqs);

#ifdef __cplusplus
}
#endif

#endif /* _RPC_SERVICES_H */
