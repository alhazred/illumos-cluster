/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_FECL_H
#define	_FECL_H

#pragma ident	"@(#)fecl.h	1.16	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * fecl.h
 * client header file for fork execution daemon
 */
#include "rgm/security.h"
#if DOOR_IMPL
#include "rgm/fe.h"
#else
#include "rgm/rpc/fe.h"
#endif
#include "rgm/scutils.h"

/*
 * enumerate existing command types
 */
enum fe_cmd_type {
	FE_NULL = 0,
	FE_RUN,		/* execve a method */
	FE_SUSPEND,	/* suspend an existing running method */
	FE_RESUME,	/* resume an existing suspended method */
	FE_KILL,	/* kill an inflight running method */
	/* SC SLM addon start */
	FE_SLM_UPDATE_ARGS,		/* update a RG SC SLM property */
	FE_SLM_CONF_ARGS,		/* update a SLM conf property */
	FE_SLM_DEBUG,			/* display SLM status in fed */
	FE_SLM_CHECK			/* SLM check in fed */
	/* SC SLM addon end */
};
typedef enum fe_cmd_type fe_cmd_type;

/*
 * info passed to fed library functions
 */
struct fe_cmd {
	char 	*host;		/* host name */
	int	timeout;	/* timeout value */
	int 	grace_period;	/* grace period */
	SEC_TYPE	security;
	int	flags;		/* other fe_run options */
	char	*corepath;
	char	*zonename;
#if SOL_VERSION >= __s10
	char	*zonepath;
#endif
	char	*project_name;	/* project name */
	/* SC SLM addon start */
	int	slm_flags;		/* RGM method or conf type */
	char	*ssm_wrapper_zone;	/* ssm_wrapper zone */
	char	*r_name;		/* resource name */
	char	*rg_name;		/* resource group name */
	char	*rg_slm_type;		/* SLM type */
	char	*rg_slm_pset_type;	/* SLM pset type */
	int	rg_slm_cpu;		/* RG_SLM_CPU_SHARES value */
	int	rg_slm_cpu_min;		/* RG_SLM_PSET_MIN value */
	unsigned int	global_zone_shares;	/* SLM conf */
	unsigned int	pset_min;		/* SLM conf */
	/* SC SLM addon end */
};
typedef struct fe_cmd fe_cmd;

/* Used by fe_set_env_vars and its clients */
#define	NUM_ENV_VARS 9

/*
 * client library functions that make the RPC calls
 */
extern int fe_run(char *id, fe_cmd *cmd_opts, char *cmd_path, char *argv[],
char *env[], fe_run_result *res);
extern int fe_suspend(char *id, fe_cmd *cmd_opts, fe_run_result *res);
extern int fe_resume(char *id, fe_cmd *cmd_opts, fe_run_result *res);
extern int fe_kill(char *id, fe_cmd *cmd_opts, fe_run_result *res);

extern char **fe_set_env_vars(void);
extern char *fe_method_full_name(char *basedir, char *method_name);

/* SC SLM addon start */
extern int fe_slm_update(char *, fe_cmd *, fe_run_result *);
extern int fe_slm_debug(char *, fe_cmd *, fe_run_result *);
extern int fe_slm_check(char *, fe_cmd *, fe_run_result *);
extern int fe_slm_conf_global_zone_shares(char *, fe_cmd *, fe_run_result *);
extern int fe_slm_conf_pset_min(char *, fe_cmd *, fe_run_result *);
/* SC SLM addon end */

#ifdef __cplusplus
}
#endif

#endif	/* _FECL_H */
