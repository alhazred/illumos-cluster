/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SECURITY_H
#define	_SECURITY_H

#pragma ident	"@(#)security.h	1.14	08/07/28 SMI"

#include <sys/os_compat.h>

/*
 * security.h - part of libsecurity
 * Header file for security library
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <rpc/types.h>
#include <rpc/rpc.h>
#include <sys/cl_rgm.h>

typedef enum {
	/*
	 * Unix weak: only desired client uid is matched;
	 * used for noninvasive calls like query.
	 */
	SEC_UNIX_WEAK,
	/*
	 * Unix strong: in addition to same matching as Unix weak, checks that:
	 * - call is local
	 * - transport is loopback
	 * - local uid from get_local_uid() matches client call uid
	 * Used for invasive calls like start, stop process.
	 */
	SEC_UNIX_STRONG,
	/*
	 * DES: this type is not yet implemented.
	 */
	SEC_DES
} SEC_TYPE;

/*
 * security error codes returned by server
 */
typedef enum {
	SEC_OK,		/* everything worked OK */
	/* error in weak unix security authentication function */
	SEC_EUNIXW,
	/* error in strong unix security authentication function */
	SEC_EUNIXS,
	SEC_EUNKN	/* unknown security flavor */
} SEC_ERRCODE;

/*
 * door/rpc implementation independent security credential data.
 */
struct security_cred_t {
	uid_t   aup_uid;
	gid_t   aup_gid;
#if 0
	/* present in predecessor authunix_parms, but unused until now */
	uint_t   aup_time;
	char    *aup_machname;
	uint_t   aup_len;
	gid_t   *aup_gids;
#endif
};
typedef struct security_cred_t  security_cred_t;

#if DOOR_IMPL
/* DOOR Specific Code */
#include <door.h>
#include <sys/mman.h>
#include <signal.h>

/* equivalents of rpc program names */
typedef enum rgm_codes {
	RGM_SCHA_PROGRAM_CODE = 0,
	RGM_ZONEUP_CODE = 1,
	RGM_SYSEVENT_PROXY_CODE = 2,
	RGM_FE_PROGRAM_CODE = 3,
	RGM_PMF_PROGRAM_CODE = 4
} rgm_door_code_t;

#define	RGM_MAX_DOOR_CODE	4


/*
 * Door Call Error Codes
 * ---------------------
 * DOOR_CALL_SUCCESS - The door call has been successful
 *	and the returned data has been successfully decoded
 *
 * UNABLE_TO_MAKE_DOOR_CALL - The client is unable to make
 *	a door call to the door server.
 *
 * XDR_ERROR_AT_CLIENT - The door_call returned successfully
 *	but the client was unable to decode the returned data.
 *
 * XDR_ERROR_AT_SERVER - The door_call was successful but the
 *	door server was unable to either encode or decode the
 * 	data.
 *
 * FAIL_RET_FROM_API_SERVER_FN - At the server end the function
 *	call that was made by the door_server returned FALSE
 *	value indicating failure of the function.
 *
 * DOOR_INVALID_HANDLE - The handle used to make the call is
 *	NULL and hence invalid.
 *
 * DOOR_INCOMING_ARG_SIZE_NULL - The argument received at the
 *	door server has size zero.
 *
 * UNABLE_TO_ALLOCATE_MEM - Unable to allocate memory at the
 *	door server.
 *
 * INVALID_API_NAME - The api called at the server end is
 * 	invalid and unrecognized.
 *
 * INVALID_CREDENTIALS - The credentials for making the door
 * 	call are not valid.
 *
 */
#define	DOOR_CALL_SUCCESS		0
#define	UNABLE_TO_MAKE_DOOR_CALL	1
#define	XDR_ERROR_AT_CLIENT		2
#define	XDR_ERROR_AT_SERVER		3
#define	FALSE_RET_FROM_API_FN		4
#define	DOOR_INVALID_HANDLE		5
#define	DOOR_INCOMING_ARG_SIZE_NULL	6
#define	UNABLE_TO_ALLOCATE_MEM		7
#define	INVALID_API_NAME		8
#define	INVALID_CREDENTIALS		9

#define	safe_strlen(s)	((s != NULL) ? strlen(s) + 1 : 0)

#define	EXTRA_BUFFER_SIZE		2000
#define	XDR_NUM_RETRIES			3

extern int make_door_call(int, door_arg_t *);

typedef void door_server(void *cookie, char *dataptr, size_t data_size,
    door_desc_t *desc_ptr, uint_t ndesc);



typedef struct clnt_handlerecord {
	int door_desc;
	char *program_name;
	SEC_TYPE  sec_type;	/* to be made SEC_TYPE */
} client_handle;

/*
 * Initialize and start server side of security module.
 * Performed at server start-up.
 */
extern int
security_svc_reg(
	door_server *proc,		/* program */
	rgm_door_code_t program_code,	/* program name */
	int dbg, char *);

#if SOL_VERSION >= __s10
int security_svc_add_zone(rgm_door_code_t program_code, const char *zonename);
#endif

int get_door_syscall(rgm_door_code_t, char *);
int get_door_direct(rgm_door_code_t, char *);

/*
 * server authentication function
 * performed at beginning of each call
 */
extern SEC_ERRCODE
security_svc_authenticate(
	door_cred_t	*d_cred,
	/*
	 * boolean user_is_root denotes user id to use for authentication
	 * if true desired user id is root (0)
	 * if false desired user id is the user id of the client
	 * - use true for servers like fed and receptionist, where the caller
	 * has to be root
	 * - use false for servers like pmf where the caller can be anybody.
	 *
	 * Note: we can make this more flexible in the future, to
	 * allow the user to specify a specific uid or range of uids to ask
	 * for or to avoid
	 */
	bool_t user_is_root);

/*
 * Client authentication function
 * performed instead of normal clnt_create.
 */
extern bool_t /* was bool_t, changed for compilation errors */
security_clnt_connect(
	client_handle **door_handle,
	rgm_door_code_t program_num,	/* program name */
	SEC_TYPE sec_type,	/* security type */
	int	debug,	/* if 1, print debugging messages */
	/* if 1, print messages to stderr, otherwise print to syslog */
	int	output_type,
	char *zonename);

/*
 * client authentication cleanup function
 * performed at end of each call instead of clnt_destroy.
 */
extern void security_clnt_freebinding(
	client_handle *door_handle);	/* client handle */

#if SOL_VERSION >= __s10
/*
 * get_client_zonename
 * This function can be used by the to get the client zonename
 * from which the call has come over the door interface.
 * Note, that this function call is valid only if there is
 * an active door call, and can be called by the door server
 * code only.
 * The function expects an allocated buffer(zonename) of
 * size ZONENAME_MAX from the caller.
 */
boolean_t
get_doorclient_zonename(char *zonename);
#endif

/* DOOR Specific Code Ends */
#else
/* RPC Specific Code */

/*
 * Prototypes of public library functions
 */
/*
 * Initialize and start server side of security module.
 * Performed at server start-up.
 */
extern int
security_svc_reg(
	/* program */
	void (*svc_program)(struct svc_req *, SVCXPRT *),
	const char *program_name,	/* program name */
	const ulong_t program_num,	/* program number */
	const ulong_t version,		/* version number */
	int debug);			/* to debug or not */

/*
 * server authentication function
 * performed at beginning of each call
 */
extern SEC_ERRCODE
security_svc_authenticate(
	/*
	 * Client call data
	 */
	struct svc_req *rqstp,
	/*
	 * Unix credentials
	 * This is an output parameter, used in subsequent functions to
	 * provide the client uid and gid.
	 */
	struct authunix_parms *unix_cred,
	/*
	 * security type:
	 * strong UNIX security or weak unix security.
	 */
	SEC_TYPE sec_type,
	/*
	 * boolean user_is_root denotes user id to use for authentication
	 * if true desired user id is root (0)
	 * if false desired user id is the user id of the client
	 * - use true for servers like fed and receptionist, where the caller
	 * has to be root
	 * - use false for servers like pmf where the caller can be anybody.
	 *
	 * Note: we can make this more flexible in the future, to
	 * allow the user to specify a specific uid or range of uids to ask
	 * for or to avoid
	 */
	bool_t user_is_root);

/*
 * Client authentication function
 * performed instead of normal clnt_create.
 */
extern bool_t security_clnt_connect(
	CLIENT **clnt,	/* client handle */
	char *remHostName,	/* server host name */
	const char *program_name,	/* program name */
	ulong_t program_num,	/* program num */
	ulong_t version,	/* program version */
	SEC_TYPE sec_type,	/* security type */
	int debug,	/* if 1, print debugging messages */
	/* if 1, print messages to stderr, otherwise print to syslog */
	int output_type);

/*
 * client authentication cleanup function
 * performed at end of each call instead of clnt_destroy.
 */
extern void security_clnt_freebinding(
    CLIENT *clnt);  /* client handle */

/* RPC Specific Code Ends */
#endif

/* Code Common to RPC and DOOR Implementation */


/*
 * Client reporting function of server security error code returned;
 * performed after a call that returned a security error code.
 * The client prints the string and frees the memory.
 */
extern char *security_get_errormsg(SEC_ERRCODE rc);

/*
 * custom xdr function to be used in .x files that include error codes of type
 * SEC_ERRCODE (they should also include security.h)
 */
extern bool_t xdr_SEC_ERRCODE(register XDR *xdrs, SEC_ERRCODE *objp);



#ifdef __cplusplus
}
#endif

#endif	/* !_SECURITY_H */
