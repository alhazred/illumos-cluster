/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RTREG_H
#define	_RTREG_H

#pragma ident	"@(#)rtreg.h	1.38	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 *	Resource Type Registration File Facility.
 *
 *	Reads in registration file, invokes yacc parser, and builds
 *	a parse data structure of file contents.
 *
 *	USAGE:
 *		#include rtreg.h
 *
 *		RtrFileResult *rtr_results;
 *
 *		const char *filepath = <read rtreg filename>
 *
 *		int status = rtreg_parse(filepath, &rtr_results);
 *
 *		// process info in rtr_result
 *
 *		if (rtr_results) {
 *			// Free data structures and input buffer
 *			delete_RtrFileResult(rtr_results);
 *		}
 *
 *	The value of "status" indicates if the parse was successful,
 *	and "rtr_results" contains the parse information or error
 *	information.
 *
 */

typedef struct RtrFileResult RtrFileResult;


/*
 * Resource Type Registration File Parser entry point.
 * Input argument: path to registration file.
 * Returns status,
 * and parse results through the rtr_results reference parameter.
 * The client is responsible for deleting rtr_results
 * using delete_RtrFileResult()
 *
 * delete_RtrFileResult will free all constituent
 * parts that are refered-to by ponters, including
 * free-ing the buffer holding the file contents,
 * and deleting the paramtab entries.
 */

extern int	rtreg_parse(const char *filepath, RtrFileResult **rtr_results);

extern void	delete_RtrFileResult(RtrFileResult *rtr_results);

extern boolean_t is_restart_only_usable(void);

/* status values returned by rtreg_parse */
#define	RTR_STATUS_PARSE_OK	0  /* parse results in rtr_results */
#define	RTR_STATUS_FILE_ERR	1  /* can't open file, rtr_results is NULL */
#define	RTR_STATUS_PARSE_ERR	2  /* parse failed, details in rtr_results */
#define	RTR_STATUS_RTNAME_ERR	3  /* RT name not accepted */

/*
 * RTR_STATUS_INVALID_PROPVAL : This error code will be set, if there is
 * invalid value for the property. This property vlaue may be either for the
 * property which is not in the paramtable like API_VERSION
 */
#define	RTR_STATUS_INVALID_PROPVAL	4  /* Invalid property value */

/*
 * RTR_STATUS_INVALID_SYSDEF : This error code will be set if there is any
 * entry in the paramtable which has no EXTENSION value set and which is not
 * in the predefined properties list
 */
#define	RTR_STATUS_INVALID_SYSDEF	5  /* Invalid system defined property */

/*
 * RTR_STATUS_INVALID_ATTR : This error code will be set if unrecognized
 * attribute is set. The attributes are all the entries between left brace
 * and right brace of the param table entries (other than the PROPERTY)
 * like MIN
 */
#define	RTR_STATUS_INVALID_ATTR		6  /* Invalid attribute */

/*
 * RTR_STATUS_INVALID_ATTRVAL : This error code will be set if an invalid value
 * is specified for attributes. For example, TYPE is set for system defined
 * property.
 */
#define	RTR_STATUS_INVALID_ATTRVAL	7  /* Invalid attribue value */

#define	RTR_STATUS_INTERNAL_ERR		8

/*
 * RTR_STATUS_INVALID_PARAM : This error code will be returned if there
 * is an error in a paramtable entry, such as a duplicate entry, or
 * an invalid PROPERTY name.
 */
#define	RTR_STATUS_INVALID_PARAM	9

/*
 * RTR_STATUS_INVALID_PROP : This error code will be returned if there
 * is an error in a RT property entry, such as a duplicate entry, or
 * a missing method
 */
#define	RTR_STATUS_INVALID_PROP		10

/*
 * RTR_Value are symbolic values for different RT properties
 * and paramtable attributes
 */
typedef enum RTR_Value {
	RTR_NONE = 0,	/* property or attribute unset in rtr file */
	RTR_NULL,	/* explicit NULL value set in rtr file */
	RTR_TRUE,
	RTR_FALSE,
	RTR_AT_CREATION,
	RTR_ANYTIME,
	RTR_WHEN_DISABLED,
	RTR_WHEN_UNMANAGED,
	RTR_WHEN_UNMONITORED,
	RTR_WHEN_OFFLINE,
	RTR_RG_PRIMARIES,
	RTR_RT_INSTALLED_NODES,
	RTR_INT,
	RTR_BOOLEAN,
	RTR_STRING,
	RTR_ENUM,
	RTR_STRINGARRAY,
	RTR_LOGICAL_HOSTNAME,
	RTR_SHARED_ADDRESS,
	/*
	 * RTR_FALSE_UNSET is a value for the
	 * Rtr_param_T::extension  field indicating a
	 * system_defined property that was not declared
	 * in the paramtable
	 */
	RTR_FALSE_UNSET
} RTR_Value;

/*
 * RTR_Sysparam values, used for the RTR_Param_T::sysparamid field,
 * indicate which system_define property was recognized in non-extension
 * paramtable entries.
 */
typedef enum RTR_Sysparam {
	RTR_SP_NONE = 0,
	RTR_SP_START_TIMEOUT,
	RTR_SP_STOP_TIMEOUT,
	RTR_SP_VALIDATE_TIMEOUT,
	RTR_SP_UPDATE_TIMEOUT,
	RTR_SP_INIT_TIMEOUT,
	RTR_SP_FINI_TIMEOUT,
	RTR_SP_BOOT_TIMEOUT,
	RTR_SP_MONITOR_START_TIMEOUT,
	RTR_SP_MONITOR_STOP_TIMEOUT,
	RTR_SP_MONITOR_CHECK_TIMEOUT,
	RTR_SP_PRENET_START_TIMEOUT,
	RTR_SP_POSTNET_STOP_TIMEOUT,
	RTR_SP_FAILOVER_MODE,
	RTR_SP_NETWORK_RESOURCES_USED,
	RTR_SP_SCALABLE,
	RTR_SP_PORT_LIST,
	RTR_SP_LOAD_BALANCING_POLICY,
	RTR_SP_LOAD_BALANCING_WEIGHTS,
	RTR_SP_AFFINITY_TIMEOUT,
	RTR_SP_UDP_AFFINITY,
	RTR_SP_WEAK_AFFINITY,
	RTR_SP_GENERIC_AFFINITY,
	RTR_SP_ROUND_ROBIN,
	RTR_SP_CONN_THRESHOLD,
	RTR_SP_CHEAP_PROBE_INTERVAL,
	RTR_SP_THOROUGH_PROBE_INTERVAL,
	RTR_SP_RETRY_COUNT,
	RTR_SP_RETRY_INTERVAL,
	RTR_SP_GLOBAL_ZONE_OVERRIDE
} RTR_Sysparam;

/*
 * The RtrString::value field is set to RTR_NULL
 * if it the string value is explicitly set to NULL.
 * It is set to RTR_NONE if the string is unset,
 * and RTR_STRING, if there is a non-null string value.
 * The RtrString::lineno field, if set to 1 or greater,
 * is the line in the RTR file where the string was
 * identified. lineno may be 0 if the string is a constant.
 */
typedef struct RtrString {
	const char *start;
	int len;
	RTR_Value value;
	int lineno;
} RtrString;

typedef struct Rtr_T {
	RtrString rtname;
	RtrString vendor_id;
		/*
		 * vendor_id is not by itself a property:
		 * it is a field in the registration file
		 * that supports a vendor-prefix convention for
		 * resource-type names.
		 *
		 * if vendor_id is provided, the registered rtname
		 * should be formed as <vendor_id>.<rtname>
		 */

	RtrString basedir;

	RtrString start;
	RtrString stop;
	RtrString abort;
	RtrString validate;
	RtrString update;
	RtrString init;
	RtrString fini;
	RtrString boot;
	RtrString monitor_start;
	RtrString monitor_stop;
	RtrString monitor_check;
	RtrString prenet_start;
	RtrString postnet_stop;
	RtrString postnet_abort;

	RtrString version;
	RtrString rt_description;
	/*
	 * flags to indicate if numeric apiversion field is set.
	 * it will be 0 if associated integer field is unset,
	 * 1 if they are set.
	 */
	char	  apiversion_isset;
	int	  apiversion;
	RTR_Value init_nodes;
	RTR_Value single_instance;
	RTR_Value failover;
	RTR_Value globalzone;	/* if methods should run in global zone */
	RTR_Value proxy;
	RTR_Value sysdefined_type;
	RTR_Value rt_system;
	RtrString *pkglist;
	int	   pkglistsz;

} Rtr_T;

typedef struct RtrUpg_T {
	RtrString	upg_version;
	RTR_Value	upg_tunability;
} RtrUpg_T;

typedef struct RtrParam_T {
	RtrString	propname;

	RTR_Value	extension;
	RTR_Value	per_node;
	RTR_Sysparam	sysparamid;
	RTR_Value	type;
	RtrString	*enumlist;
	int		enumlistsz;
	/*
	 * flags to indicate if numeric fields were set.
	 * These will be 0 if associated integer field is unset,
	 * 1 if they are set.
	 */
	char		rtr_min_isset;
	char		rtr_max_isset;
	char		arraymin_isset;
	char		arraymax_isset;
	char		defaultint_isset;

	int		rtr_min;
			/* min INT type , minlen for STRING and STRINGARRY */
	int		rtr_max;
			/* max INT type , maxlen for STRING and STRINGARRY */
	int		arraymin;
			/* minimum array size for STRINGARRAY type */
	int		arraymax;
			/* maximum array size for STRINGARRAY type */

	RtrString	defaultstr;
			/*
			 * The string version of the default value
			 * This is always available even for
			 * int-type  and symbolic values.
			 */
	int		defaultint;
			/* numeric form for int-type default values. */
	RTR_Value	defaultboolean;
			/* Should be RTR_TRUE or RTR_FALSE for booleans */

	RtrString	*defaultarray;
	int		defaultarraysz;

	RtrString description;
	RTR_Value tunable;

} RtrParam_T;

struct RtrFileResult {
	int	status;
		/* The return code from rtr_parse that created the results */
	char 	*buffer;
		/*
		 * Null terminated buffer holding read-in file.
		 * RtrString values point into this buffer.
		 */
	const char	*error_bufloc;
		/*
		 * Pointer into buffer on failed parse or value error.
		 * The error occured at or before this location, but
		 * probably at the token just before.
		 */
	int 	error_lineno;
		/* line number of error */
	Rtr_T	*rt_registration;
		/* Resource-Type properties */
	boolean_t	sc30_rtr;
		/* True if rtr file is sc30 version */
	boolean_t	nru_tunablity_changed;
		/* True if NRU is set to anything other than TUNE_ANYTIME */
	int	upgradesz;
		/* Number of #$upgrade_from directives in rtr. */
	RtrUpg_T	**upgradearray;
		/* Array of pointers to #$upgrade_from entries. */
	int	downgradesz;
		/* Number of #$downgrade_to directives in rtr. */
	RtrUpg_T	**downgradearray;
		/* Array of pointers to #$downgrade_to entries. */
	int	paramtabsz;
		/* Number of elements in paramtab */
	RtrParam_T	**paramtab;
		/*
		 * A paramtabsz array of pointers to
		 * paramtable entries.
		 */

};

#ifdef	__cplusplus
}
#endif

#endif /* _RTREG_H */
