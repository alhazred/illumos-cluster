/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SC_EVENT_PROXY_PUBLIC_H
#define	_SC_EVENT_PROXY_PUBLIC_H

#pragma ident	"@(#)sc_event_proxy_impl.h	1.2	08/05/20 SMI"

/*
 * sc_event_proxy_public.h - part of libclevent
 * Header file for event proxy
 */


#include <sys/types.h>
#include <rpc/types.h>
#include <rpc/rpc.h>
#include <zone.h>
#include <errno.h>
#include <unistd.h>
#include <libnvpair.h>
#include <sys/cl_eventdefs.h>

#ifdef __cplusplus
extern "C" {
#endif


/*
 * The EVENT_PROXY_PADDING_SPACE is used as a padding in
 * between the xdr encoded data and the packed nvlist.
 * This padding is needed so that there is no overlapping
 * amongst the two differently data. So we can decode them
 * cleanly.
 * As of now we dont know of a way to determine the amount
 * of padding needed. So we use a padding that we consider
 * bug enough to take care of the possible overlap.
 * We should try to find a way to get to know how much
 * padding is sufficient and implement it that way.
 */
#define	EVENT_PROXY_PADDING_SPACE 100
#define	SYSEVENT_PROXY "sysevent_proxy"
#define	SYSTEXT(s) s

/*
 * Proxy function which is used to forward the
 * request to log sysevent in the global zone
 */
cl_event_error_t
sc_publish_event_proxy(const char *ev_subclass, const char *ev_publisher,
    nvlist_t *attr_list);

size_t
proxy_event_data_xdr_sizeof(proxy_event_data event_data);

#ifdef __cplusplus
}
#endif

#endif	/* _SC_EVENT_PROXY_PUBLIC_H */
