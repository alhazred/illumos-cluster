/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)IntegerDocument.java 1.7   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.scdsbuilder;

import java.awt.*;

import javax.swing.text.*;


class IntegerDocument extends PlainDocument {
    public void insertString(int offset, String s, AttributeSet attributeSet)
        throws BadLocationException {

        if (!s.equals("")) {

            try {
                Integer.parseInt(s);
            } catch (Exception ex) { // only allow integer values
                Toolkit.getDefaultToolkit().beep();

                return;
            }
        }

        super.insertString(offset, s, attributeSet);
    }
}
