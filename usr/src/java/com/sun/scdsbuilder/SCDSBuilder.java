/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)SCDSBuilder.java 1.41   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.scdsbuilder;

import java.awt.*;
import java.awt.event.*;

import java.io.*;

import java.text.MessageFormat;

import java.util.*;

import javax.accessibility.*;

import javax.swing.*;
import javax.swing.text.*;


class SCDSBuilder extends JFrame implements ActionListener, Runnable {

    private String cmd[];

    private ConfigInfo config;

    private boolean isBusy = false;

    private boolean ccFound = true;

    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem loadItem;
    private JMenuItem exitItem;
    private JMenu editMenu;
    private JMenuItem clearItem;
    private JMenuItem saveItem;
    private JLabel label;

    private JFileChooser fileChooser = new JFileChooser(System.getProperty(
                "user.dir"));

    private JTextField tfVendorName;
    private JTextField tfRTName;
    private JTextField tfRTVersion;
    private JTextField tfWorkingDir;
    private JCheckBox cbNetworkAware;
    private ButtonGroup bgType;
    private JRadioButton rbScalable;
    private JRadioButton rbFailover;

    private ButtonGroup bgSourceType;
    private JRadioButton rbC;
    private JRadioButton rbKsh;
    private JRadioButton rbGDS;
    private JRadioButton rbPerl;

    private JTextField tfStartCmd;
    private JTextField tfStartTO;

    private JTextField tfStopCmd;
    private JTextField tfStopTO;

    private JTextField tfValidateCmd;
    private JTextField tfValidateTO;

    private JTextField tfProbeCmd;
    private JTextField tfProbeTO;

    private JButton btnSelectInsDir;
    private JButton btnSelectStartCmd;
    private JButton btnSelectStopCmd;
    private JButton btnSelectValidateCmd;
    private JButton btnSelectProbeCmd;

    private JLabel stepLabel;

    private AccessibleRelationSet tfRelSet;
    private AccessibleRelationSet stepLabelRelSet;

    private JButton btnNext;
    private JButton btnPrev;
    private JButton btnExit;
    private JButton btnExec;

    private JTextArea taOutputLog;

    private JPanel mainPanel;
    private CardLayout card;
    private JPanel topPanel;
    private JPanel createPanel;
    private JPanel configurePanel;

    private ResourceBundle messages;
    private MessageFormat mf = new MessageFormat("");

    private String screenName = "create";

    public SCDSBuilder(String srcType) {
        messages = ResourceBundle.getBundle(
                "com.sun.scdsbuilder.MessagesBundle", Locale.getDefault());

        char mchar;

        setTitle(messages.getString("builder"));
        getAccessibleContext().setAccessibleDescription(messages.getString(
                "builder"));

        setBounds(100, 50, 820, 520);
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        menuBar = new JMenuBar();
        fileMenu = new JMenu(messages.getString("filemenu"));
        fileMenu.setMnemonic(messages.getString("filemenumnemonic").charAt(0));
        menuBar.add(fileMenu);

        mchar = messages.getString("loadrtmnemonic").charAt(0);
        loadItem = new JMenuItem(messages.getString("loadrt"), mchar);
        loadItem.getAccessibleContext().setAccessibleDescription(messages
            .getString("loadrt"));
        fileMenu.add(loadItem);
        loadItem.addActionListener(this);
        fileMenu.addSeparator();
        mchar = messages.getString("exitmnemonic").charAt(0);
        exitItem = new JMenuItem(messages.getString("exit"), mchar);
        exitItem.getAccessibleContext().setAccessibleDescription(messages
            .getString("exit"));
        fileMenu.add(exitItem);
        exitItem.addActionListener(this);

        editMenu = new JMenu(messages.getString("editmenu"));
        editMenu.setMnemonic(messages.getString("editmenumnemonic").charAt(0));
        menuBar.add(editMenu);

        mchar = messages.getString("clearmnemonic").charAt(0);
        clearItem = new JMenuItem(messages.getString("clear"), mchar);
        clearItem.getAccessibleContext().setAccessibleDescription(messages
            .getString("clear"));
        editMenu.add(clearItem);
        clearItem.addActionListener(this);
        mchar = messages.getString("savemnemonic").charAt(0);
        saveItem = new JMenuItem(messages.getString("save"), mchar);
        saveItem.getAccessibleContext().setAccessibleDescription(messages
            .getString("save"));
        editMenu.add(saveItem);
        saveItem.addActionListener(this);
        getRootPane().setJMenuBar(menuBar);

        topPanel = new JPanel();
        topPanel.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();

        ImagePanel sidePanel = new ImagePanel(
                "/usr/cluster/lib/scdsbuilder/classes/com/sun/scdsbuilder/solaris.gif");

        sidePanel.setBorder(BorderFactory.createEtchedBorder());

        mainPanel = new JPanel();
        card = new CardLayout();
        mainPanel.setLayout(card);

        // mainPanel contains two panels -- createPanel and
        // configurePanel which are managed by the CardLayout
        createPanel = new JPanel();

        createPanel.setLayout(new GridBagLayout());

        // create the createPanel and fill it up with various
        // components
        createPanel.setBorder(BorderFactory.createEtchedBorder());

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(2, 10, 2, 10);
        label = new JLabel(messages.getString("vendor"));
        addgb(label, gbc, createPanel, 0, 0, 1, 1);
        tfVendorName = new JTextField("", 15);
        tfVendorName.setDocument(new IdentifierDocument());
        tfVendorName.setToolTipText(messages.getString("vendortip"));
        tfVendorName.getAccessibleContext().setAccessibleName(messages
            .getString("vendor"));
        addgb(tfVendorName, gbc, createPanel, 0, 1, 1, 1);
        tfRelSet = tfVendorName.getAccessibleContext()
            .getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setDisplayedMnemonic(messages.getString("vendormnemonic").charAt(
                0));
        label.setLabelFor(tfVendorName);

        label = new JLabel(messages.getString("rtname"));
        addgb(label, gbc, createPanel, 1, 0, 1, 1);
        tfRTName = new JTextField("", 10);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.WEST;
        tfRTName.setDocument(new IdentifierDocument());
        tfRTName.setToolTipText(messages.getString("rttip"));
        tfRTName.getAccessibleContext().setAccessibleName(messages.getString(
                "rtname"));
        addgb(tfRTName, gbc, createPanel, 1, 1, 1, 1);
        tfRelSet = tfRTName.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setDisplayedMnemonic(messages.getString("rtmnemonic").charAt(0));
        label.setLabelFor(tfRTName);

        label = new JLabel(messages.getString("rtversion"));
        addgb(label, gbc, createPanel, 2, 0, 1, 1);
        tfRTVersion = new JTextField("", 5);
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
// tfRTVersion.setDocument(new IdentifierDocument());
        tfRTVersion.setToolTipText(messages.getString("rtversiontip"));
        tfRTVersion.getAccessibleContext().setAccessibleName(messages.getString(
                "rtversion"));
        tfRTVersion.setText("1.0");
        addgb(tfRTVersion, gbc, createPanel, 2, 1, 1, 1);
        tfRelSet = tfRTVersion.getAccessibleContext()
            .getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setDisplayedMnemonic(messages.getString("rtversionmnemonic")
            .charAt(0));
        label.setLabelFor(tfRTVersion);

        label = new JLabel(messages.getString("workingdir"));
        addgb(label, gbc, createPanel, 0, 2, 1, 1);
        tfWorkingDir = new JTextField("", 25);

        // Instantiate the tfWorkingDir with the user's current
        // directory
        tfWorkingDir.setText(System.getProperty("user.dir"));
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.WEST;
        tfWorkingDir.setToolTipText(messages.getString("workingdirtip"));
        tfWorkingDir.getAccessibleContext().setAccessibleName(messages
            .getString("workingdir"));
        addgb(tfWorkingDir, gbc, createPanel, 0, 3, 2, 1);
        tfRelSet = tfWorkingDir.getAccessibleContext()
            .getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setDisplayedMnemonic(messages.getString("workingdirmnemonic")
            .charAt(0));
        label.setLabelFor(tfWorkingDir);

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        btnSelectInsDir = new JButton(messages.getString("browse"));
        btnSelectInsDir.addActionListener(this);
        btnSelectInsDir.setMnemonic(messages.getString("browsemnemonic").charAt(
                0));
        btnSelectInsDir.getAccessibleContext().setAccessibleDescription(messages
            .getString("browse"));
        addgb(btnSelectInsDir, gbc, createPanel, 2, 3, 1, 1);

        JPanel rbTypePanel = new JPanel();
        rbTypePanel.setBorder(BorderFactory.createEtchedBorder());
        bgType = new ButtonGroup();
        rbScalable = new JRadioButton(messages.getString("scalable"));
        rbScalable.setToolTipText(messages.getString("apptypetip"));
        bgType.add(rbScalable);
        rbTypePanel.add(rbScalable);
        rbFailover = new JRadioButton(messages.getString("failover"));
        bgType.add(rbFailover);
        rbFailover.setToolTipText(messages.getString("apptypetip"));
        rbTypePanel.add(rbFailover);
        rbTypePanel.setToolTipText(messages.getString("apptypetip"));

        cbNetworkAware = new JCheckBox(messages.getString("networkaware"));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        cbNetworkAware.setToolTipText(messages.getString("networkawaretip"));
        addgb(rbTypePanel, gbc, createPanel, 0, 4, 1, 1);
        addgb(cbNetworkAware, gbc, createPanel, 1, 4, 1, 1);

        JPanel rbSourceTypePanel = new JPanel();
        rbSourceTypePanel.setBorder(BorderFactory.createEtchedBorder());
        label = new JLabel(messages.getString("sourcetype"));
        rbSourceTypePanel.add(label);
        bgSourceType = new ButtonGroup();
        tfRelSet = rbSourceTypePanel.getAccessibleContext()
            .getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setDisplayedMnemonic(messages.getString("sourcetypemnemonic")
            .charAt(0));
        label.setLabelFor(rbSourceTypePanel);

        rbC = new JRadioButton(messages.getString("c"));
        bgSourceType.add(rbC);
        rbSourceTypePanel.add(rbC);
        rbC.addActionListener(this);
        rbC.setToolTipText(messages.getString("codetypetip"));

        rbKsh = new JRadioButton(messages.getString("ksh"));
        bgSourceType.add(rbKsh);
        rbSourceTypePanel.add(rbKsh);
        rbKsh.addActionListener(this);
        rbKsh.setToolTipText(messages.getString("codetypetip"));

        rbGDS = new JRadioButton(messages.getString("gds"));
        bgSourceType.add(rbGDS);
        rbSourceTypePanel.add(rbGDS);
        rbGDS.addActionListener(this);
        rbGDS.setToolTipText(messages.getString("codetypetip"));

        /*
         * rbPerl = new JRadioButton(messages.getString("perl"));
         * bgSourceType.add(rbPerl);
         * rbSourceTypePanel.add(rbPerl);
         * rbPerl.setEnabled(false);
         * rbPerl.setToolTipText("Perl code planned for the future");
         * rbPerl.addActionListener(this);
         */

        addgb(rbSourceTypePanel, gbc, createPanel, 0, 5, 3, 1);

        mainPanel.add(createPanel, messages.getString("create"));

        gbc.insets = new Insets(5, 10, 5, 10);

        // create the configurePanel and fill it up with its
        // components.
        configurePanel = new JPanel();
        configurePanel.setLayout(new GridBagLayout());
        configurePanel.setBorder(BorderFactory.createEtchedBorder());

        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.WEST;
        label = new JLabel(messages.getString("startcommand"));
        label.setDisplayedMnemonic(messages.getString("startcommandmnemonic")
            .charAt(0));
        addgb(label, gbc, configurePanel, 0, 0, 1, 1);
        tfStartCmd = new JTextField("", 30);
        tfStartCmd.setDocument(new CmdDocument(this));
        tfStartCmd.setToolTipText(messages.getString("startcommandtip"));
        tfStartCmd.getAccessibleContext().setAccessibleName(messages.getString(
                "startcommand"));
        addgb(tfStartCmd, gbc, configurePanel, 0, 1, 1, 1);
        tfRelSet = tfStartCmd.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfStartCmd);

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        btnSelectStartCmd = new JButton(messages.getString("browse"));
        btnSelectStartCmd.addActionListener(this);
        btnSelectStartCmd.setMnemonic(messages.getString("browsemnemonic")
            .charAt(0));
        btnSelectStartCmd.getAccessibleContext().setAccessibleDescription(
            messages.getString("browse"));
        addgb(btnSelectStartCmd, gbc, configurePanel, 1, 1, 1, 1);

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        label = new JLabel(messages.getString("timeout"));
        label.setDisplayedMnemonic(messages.getString("starttimemnemonic")
            .charAt(0));
        addgb(label, gbc, configurePanel, 2, 0, 1, 1);
        tfStartTO = new JTextField("", 10);
        tfStartTO.setDocument(new IntegerDocument());
        tfStartTO.setText("300");
        tfStartTO.setToolTipText(messages.getString("starttimetip"));
        tfStartTO.getAccessibleContext().setAccessibleName(messages.getString(
                "timeout"));
        addgb(tfStartTO, gbc, configurePanel, 2, 1, 1, 1);
        tfRelSet = tfStartTO.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfStartTO);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.WEST;
        label = new JLabel(messages.getString("stopcommand"));
        label.setDisplayedMnemonic(messages.getString("stopcommandmnemonic")
            .charAt(0));
        addgb(label, gbc, configurePanel, 0, 2, 1, 1);
        tfStopCmd = new JTextField("", 30);
        tfStopCmd.setDocument(new CmdDocument(this));
        tfStopCmd.setToolTipText(messages.getString("stopcommandtip"));
        tfStopCmd.getAccessibleContext().setAccessibleName(messages.getString(
                "stopcommand"));
        addgb(tfStopCmd, gbc, configurePanel, 0, 3, 1, 1);
        tfRelSet = tfStartCmd.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfStopCmd);

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        btnSelectStopCmd = new JButton(messages.getString("browse"));
        btnSelectStopCmd.setMnemonic(messages.getString("browsemnemonic")
            .charAt(0));
        btnSelectStopCmd.addActionListener(this);
        btnSelectStopCmd.getAccessibleContext().setAccessibleDescription(
            messages.getString("browse"));
        addgb(btnSelectStopCmd, gbc, configurePanel, 1, 3, 1, 1);

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        label = new JLabel(messages.getString("stoptime"));
        label.setDisplayedMnemonic(messages.getString("stoptimemnemonic")
            .charAt(0));
        addgb(label, gbc, configurePanel, 2, 2, 1, 1);
        tfStopTO = new JTextField("", 10);
        tfStopTO.setDocument(new IntegerDocument());
        tfStopTO.setText("300");
        tfStopTO.setToolTipText(messages.getString("stoptimetip"));
        tfStopTO.getAccessibleContext().setAccessibleName(messages.getString(
                "timeout"));
        addgb(tfStopTO, gbc, configurePanel, 2, 3, 1, 1);
        tfRelSet = tfStopTO.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfStopTO);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.WEST;
        label = new JLabel(messages.getString("validatecommand"));
        label.setDisplayedMnemonic(messages.getString(
                "validatecommandmnemonic").charAt(0));
        addgb(label, gbc, configurePanel, 0, 4, 1, 1);
        tfValidateCmd = new JTextField("", 30);
        tfValidateCmd.setDocument(new CmdDocument(this));
        tfValidateCmd.setToolTipText(messages.getString("validatecommandtip"));
        tfValidateCmd.getAccessibleContext().setAccessibleName(messages
            .getString("validatecommand"));
        addgb(tfValidateCmd, gbc, configurePanel, 0, 5, 1, 1);
        tfRelSet = tfValidateCmd.getAccessibleContext()
            .getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfValidateCmd);

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        btnSelectValidateCmd = new JButton(messages.getString("browse"));
        btnSelectValidateCmd.setMnemonic(messages.getString("browsemnemonic")
            .charAt(0));
        btnSelectValidateCmd.addActionListener(this);
        btnSelectValidateCmd.getAccessibleContext().setAccessibleDescription(
            messages.getString("browse"));
        addgb(btnSelectValidateCmd, gbc, configurePanel, 1, 5, 1, 1);

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        label = new JLabel(messages.getString("validatetime"));
        label.setDisplayedMnemonic(messages.getString("validatetimemnemonic")
            .charAt(0));
        addgb(label, gbc, configurePanel, 2, 4, 1, 1);
        tfValidateTO = new JTextField("", 10);
        tfValidateTO.setDocument(new IntegerDocument());
        tfValidateTO.setText("300");
        tfValidateTO.setToolTipText(messages.getString("validatetimetip"));
        tfValidateTO.getAccessibleContext().setAccessibleName(messages
            .getString("timeout"));
        addgb(tfValidateTO, gbc, configurePanel, 2, 5, 1, 1);
        tfRelSet = tfValidateTO.getAccessibleContext()
            .getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));

        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.WEST;
        label = new JLabel(messages.getString("probecommand"));
        label.setDisplayedMnemonic(messages.getString("probecommandmnemonic")
            .charAt(0));
        addgb(label, gbc, configurePanel, 0, 6, 1, 1);
        tfProbeCmd = new JTextField("", 30);
        tfProbeCmd.setDocument(new CmdDocument(this));
        tfProbeCmd.setToolTipText(messages.getString("probecommandtip"));
        tfProbeCmd.getAccessibleContext().setAccessibleName(messages.getString(
                "probecommand"));
        addgb(tfProbeCmd, gbc, configurePanel, 0, 7, 1, 1);
        tfRelSet = tfProbeCmd.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfProbeCmd);

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        btnSelectProbeCmd = new JButton(messages.getString("browse"));
        btnSelectProbeCmd.setMnemonic(messages.getString("browsemnemonic")
            .charAt(0));
        btnSelectProbeCmd.addActionListener(this);
        btnSelectProbeCmd.getAccessibleContext().setAccessibleDescription(
            messages.getString("browse"));
        addgb(btnSelectProbeCmd, gbc, configurePanel, 1, 7, 1, 1);

        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.anchor = GridBagConstraints.WEST;
        label = new JLabel(messages.getString("probetime"));
        label.setDisplayedMnemonic(messages.getString("probetimemnemonic")
            .charAt(0));
        addgb(label, gbc, configurePanel, 2, 6, 1, 1);
        tfProbeTO = new JTextField("", 10);
        tfProbeTO.setDocument(new IntegerDocument());
        tfProbeTO.setText("30");
        tfProbeTO.setToolTipText(messages.getString("probetimetip"));
        tfProbeTO.getAccessibleContext().setAccessibleName(messages.getString(
                "probetime"));
        addgb(tfProbeTO, gbc, configurePanel, 2, 7, 1, 1);
        tfRelSet = tfProbeTO.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfProbeTO);

        mainPanel.add(configurePanel, messages.getString("configure"));

        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 0);

        // Create the ButtonPanel and the buttons to it.
        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        stepLabel = new JLabel(messages.getString("stepone"));
        stepLabel.setDisplayedMnemonic(messages.getString("steponemnemonic")
            .charAt(0));
        btnPanel.add(stepLabel);
        tfRelSet = btnPanel.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                stepLabel));
        stepLabel.setLabelFor(btnPanel);

        btnExec = new JButton(messages.getString("create"));
        btnExec.addActionListener(this);
        btnExec.setMnemonic(messages.getString("createmnemonic").charAt(0));
        btnExec.setToolTipText(messages.getString("createtip"));
        btnPanel.add(btnExec);

        btnPrev = new JButton(messages.getString("previous"));
        btnPrev.addActionListener(this);
        btnPrev.setMnemonic(messages.getString("previousmnemonic").charAt(0));
        btnPanel.add(btnPrev);
        btnPrev.setToolTipText(messages.getString("previoustip"));
        btnPrev.setEnabled(false);

        btnNext = new JButton(messages.getString("next"));
        btnNext.addActionListener(this);
        btnNext.setMnemonic(messages.getString("nextmnemonic").charAt(0));
        btnNext.setToolTipText(messages.getString("nexttip"));
        btnPanel.add(btnNext);

        btnExit = new JButton(messages.getString("exit"));
        btnExit.addActionListener(this);
        btnExit.setMnemonic(messages.getString("exitmnemonic").charAt(0));
        btnExit.setToolTipText(messages.getString("exittip"));
        btnPanel.add(btnExit);

        // Create a panel for output
        JPanel bottomPanel = new JPanel();
        taOutputLog = new JTextArea(5, 60);
        taOutputLog.setEditable(false);
        taOutputLog.getAccessibleContext().setAccessibleName(messages.getString(
                "output"));
        taOutputLog.getAccessibleContext().setAccessibleDescription(messages
            .getString("output"));

        JScrollPane sPane = new JScrollPane(taOutputLog,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        bottomPanel.add(sPane);
        bottomPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(),
                messages.getString("output")));

        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        addgb(sidePanel, gbc, topPanel, 0, 0, 1, 5);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.CENTER;
        addgb(mainPanel, gbc, topPanel, 1, 0, 4, 4);

        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.CENTER;
        addgb(btnPanel, gbc, topPanel, 1, 4, 4, 1);

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                topPanel, bottomPanel);

        splitPane.setOneTouchExpandable(true);
        getContentPane().add(splitPane, BorderLayout.CENTER);

        if ((srcType != null) && srcType.equals("-k")) {
            ccFound = false;
        }

        // Determine if the rtconfig file exists in the current dir
        File configFile = new File(System.getProperty("user.dir") +
                "/rtconfig");

        if (configFile.exists()) {

            // if exists, disable Create, enable next
            // Also, if there exists a rtconfig file in the
            // current dir then the wrapper script would have
            // checked and complained about the non-existence
            // of cc, if it was not found. In other words, if
            // we get here we can safely assume the availability
            // of cc.
            btnNext.setEnabled(true);
            btnExec.setEnabled(false);
        } else {

            // if the rtconfig file doesn't exist
            // then disable next and enable Create
            btnNext.setEnabled(false);
            btnExec.setEnabled(true);
        }

        config = new ConfigInfo(System.getProperty("user.dir"));

        if (ccFound == false) {

            // The scdsbuilder script ensures that
            // confing.sourceType != "C" in this case
            rbC.setSelected(false);
            rbC.setEnabled(false);
            config.sourceType = new String("KSH");
        }

        resetValues();

        setVisible(true);
    }


    // Utility routine for adding Components to a Container having
    // GridBagLayout as its manager.
    private void addgb(Component c, GridBagConstraints gbc, Container con,
        int x, int y, int w, int h) {
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.gridwidth = w;
        gbc.gridheight = h;
        gbc.weightx = 100;
        gbc.weighty = 100;
        con.add(c, gbc);
    }


    // The catch-all routine for the action events from all the buttons
    // in the gui.
    public void actionPerformed(ActionEvent evt) {

        if (evt.getSource() == btnNext) {
            card.show(mainPanel, messages.getString("configure"));
            btnExec.setText(messages.getString("configure"));
            screenName = "configure";
            btnNext.setEnabled(false);
            btnPrev.setEnabled(true);
            stepLabel.setText(messages.getString("steptwo"));
            btnExec.setToolTipText(messages.getString("configuretip"));
            btnExec.setEnabled(true);
        } else if (evt.getSource() == btnPrev) {
            card.show(mainPanel, messages.getString("create"));
            btnExec.setText(messages.getString("create"));
            screenName = "create";
            btnPrev.setEnabled(false);
            btnNext.setEnabled(true);
            stepLabel.setText(messages.getString("stepone"));
            btnExec.setToolTipText(messages.getString("createtip"));
            btnExec.setEnabled(false);
        } else if ((evt.getSource() == btnSelectInsDir) ||
                (evt.getSource() == btnSelectStartCmd) ||
                (evt.getSource() == btnSelectStopCmd) ||
                (evt.getSource() == btnSelectValidateCmd) ||
                (evt.getSource() == btnSelectProbeCmd) ||
                (evt.getSource() == saveItem) ||
                (evt.getSource() == loadItem)) {
            selectFile((JComponent) evt.getSource());
        } else if (evt.getSource() == btnExec) {

            if (screenName.equals("create"))
                createRT();
            else
                configureRT();
        } else if (evt.getSource() == btnExit) {
            System.exit(0);
        } else if ((evt.getSource() == exitItem) && !isBusy) {
            System.exit(0);
        } else if (evt.getSource() == clearItem) {
            taOutputLog.setText("");
        } else if (evt.getSource() == rbGDS) {
            tfRTVersion.setText("6");
        } else if ((evt.getSource() == rbC) || (evt.getSource() == rbKsh)) {
            tfRTVersion.setText("1.0");
        }
    }

    private void selectFile(JComponent fileSelectBtn) {
        String title = "";
        JTextField targetTextField = null;
        int state = JFileChooser.CANCEL_OPTION;

        fileChooser.setSelectedFile(null);


        if (fileSelectBtn == btnSelectStartCmd) {
            title = messages.getString("selectstart");
            fileChooser.setToolTipText(messages.getString("selectstart"));
            targetTextField = tfStartCmd;
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setDialogTitle(title);
            fileChooser.getAccessibleContext().setAccessibleDescription(messages
                .getString("selectstart"));
            state = fileChooser.showDialog(this, messages.getString("select"));
        } else if (fileSelectBtn == btnSelectStopCmd) {
            title = messages.getString("selectstop");
            fileChooser.setToolTipText(messages.getString("selectstop"));
            targetTextField = tfStopCmd;
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setDialogTitle(title);
            fileChooser.getAccessibleContext().setAccessibleDescription(messages
                .getString("selectstop"));
            state = fileChooser.showDialog(this, messages.getString("select"));
        } else if (fileSelectBtn == btnSelectValidateCmd) {
            title = messages.getString("selectvalidate");
            fileChooser.setToolTipText(messages.getString("selectvalidate"));
            targetTextField = tfValidateCmd;
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setDialogTitle(title);
            fileChooser.getAccessibleContext().setAccessibleDescription(messages
                .getString("selectvalidate"));
            state = fileChooser.showDialog(this, messages.getString("select"));
        } else if (fileSelectBtn == btnSelectProbeCmd) {
            title = messages.getString("selectprobe");
            fileChooser.setToolTipText(messages.getString("selectprobe"));
            targetTextField = tfProbeCmd;
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setDialogTitle(title);
            fileChooser.getAccessibleContext().setAccessibleDescription(messages
                .getString("selectprobe"));
            state = fileChooser.showDialog(this, messages.getString("select"));
        } else if (fileSelectBtn == btnSelectInsDir) {
            title = messages.getString("selectworkingdir");
            fileChooser.setToolTipText(messages.getString("selectworkingdir"));
            targetTextField = tfWorkingDir;
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setDialogTitle(title);
            fileChooser.getAccessibleContext().setAccessibleDescription(messages
                .getString("selectworkingdir"));
            state = fileChooser.showOpenDialog(this);
        } else if (fileSelectBtn == loadItem) {
            title = messages.getString("selectloaddir");
            fileChooser.setToolTipText(messages.getString("selectloaddir"));
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            targetTextField = tfWorkingDir;
            fileChooser.setDialogTitle(title);
            fileChooser.getAccessibleContext().setAccessibleDescription(messages
                .getString("selectloaddir"));
            fileChooser.setApproveButtonMnemonic(messages.getString(
                    "loadmnemonic").charAt(0));
            state = fileChooser.showDialog(this, messages.getString("load"));
        } else if (fileSelectBtn == saveItem) {
            title = messages.getString("logfile");
            fileChooser.setToolTipText(messages.getString("logfile"));
            fileChooser.getAccessibleContext().setAccessibleDescription(messages
                .getString("logfile"));
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setDialogTitle(title);
            state = fileChooser.showSaveDialog(this);
        }

        File file = fileChooser.getSelectedFile();

        if (state != JFileChooser.CANCEL_OPTION) {

            if (fileSelectBtn == saveItem) {

                try {
                    BufferedWriter br = new BufferedWriter(new FileWriter(
                                file));
                    br.write(taOutputLog.getText());
                    br.close();
                } catch (IOException e) {
                    System.out.println(e);
                }

                return;
            }

            if ((fileSelectBtn == btnSelectStartCmd) ||
                    (fileSelectBtn == btnSelectStopCmd) ||
                    (fileSelectBtn == btnSelectValidateCmd) ||
                    (fileSelectBtn == btnSelectProbeCmd) ||
                    (fileSelectBtn == btnSelectInsDir))
                targetTextField.setText(file.getPath());

            if ((fileSelectBtn == btnSelectInsDir) ||
                    (fileSelectBtn == loadItem)) {
                File configFile = new File(file.getPath() + "/rtconfig");

                // Determine if the rtconfig file exists in
                // the working dir selected
                if (configFile.exists()) {
                    ConfigInfo tmpConfig = new ConfigInfo(file.getPath());

                    targetTextField.setText(file.getPath());

                    Object compilererrorArguments[] = { tmpConfig.RTName };

                    if (tmpConfig.sourceType.equals("C") && !ccFound) {
                        mf.applyPattern(messages.getString("compilererror"));
                        errorMsg(mf.format(compilererrorArguments));

                        return;
                    }

                    // if rtconfig exists, then
                    // instantiate the local config
                    // object, disable Create, enable
                    // next, and reset values
                    config = tmpConfig;
                    btnNext.setEnabled(true);
                    btnExec.setEnabled(false);
                    resetValues();
                } else {

                    if (fileSelectBtn == loadItem) {
                        errorMsg("No Resource Type " + "found in " +
                            file.getPath());
                    } else {

                        // if the rtconfig file
                        // doesn't exist then disable
                        // next and enable Create
                        btnNext.setEnabled(false);
                        btnExec.setEnabled(true);
                    }
                }
            }
        }
    }

    public void errorMsg(String eMsg) {
        Toolkit.getDefaultToolkit().beep();
        JOptionPane.showMessageDialog(this, eMsg, messages.getString("error"),
            JOptionPane.ERROR_MESSAGE);
    }

    public void infoMsg(String iMsg) {
        JOptionPane.showMessageDialog(this, iMsg, messages.getString("success"),
            JOptionPane.INFORMATION_MESSAGE);
    }

    private void createRT() {
        int i = 0;

        String RTFullName = new String(tfVendorName.getText() +
                tfRTName.getText());

        if (RTFullName.length() > 32) {
            errorMsg(messages.getString("namelengtherror"));

            return;
        }

        // First count the total no. of "arguments" with
        // which creatert needs to be called.
        // Note that the "arguments" don't include the
        // command itself.
        // For the options that take arguments the count
        // is incremented by 2; for those which don't take
        // any arguments (e.g. -a ) the count is increased
        // by only 1.
        if ((tfVendorName.getText() == null) ||
                tfVendorName.getText().equals("")) {
            errorMsg(messages.getString("vendorerror"));

            return;
        } else
            i += 2;

        if ((tfRTName.getText() == null) || tfRTName.getText().equals("")) {
            errorMsg(messages.getString("rterror"));

            return;
        } else
            i += 2;

        if ((tfRTVersion.getText() == null) ||
                tfRTVersion.getText().equals("")) {
            errorMsg(messages.getString("rtversionerror"));

            return;
        } else
            i += 2;

        if ((tfWorkingDir.getText() == null) ||
                tfWorkingDir.getText().equals("")) {
            errorMsg(messages.getString("workingdirerror"));

            return;
        } else
            i += 2;

        if (rbScalable.isSelected())
            i++;

        if (!cbNetworkAware.isSelected())
            i++;

        if (rbKsh.isSelected())
            i++;

        if (rbGDS.isSelected())
            i++;

        // Now form the actual command to be run. This is
        // done by filling out the cmd array. But we first
        // need to the size of the array. The size of the
        // array is one more than the no. of "arguments"
        // counted above. The "+1" for the command string
        // itself.
        cmd = new String[i + 1];

        int j = 0;
        cmd[j++] = "/usr/cluster/bin/scdscreate";

        cmd[j++] = "-V";
        cmd[j++] = tfVendorName.getText();

        cmd[j++] = "-T";
        cmd[j++] = tfRTName.getText();

        cmd[j++] = "-n";
        cmd[j++] = tfRTVersion.getText();

        cmd[j++] = "-d";
        cmd[j++] = tfWorkingDir.getText();

        if (rbScalable.isSelected())
            cmd[j++] = "-s";

        if (!cbNetworkAware.isSelected())
            cmd[j++] = "-a";

        if (rbKsh.isSelected())
            cmd[j++] = "-k";
        else if (rbGDS.isSelected())
            cmd[j++] = "-g";

        this.start();
    }

    private void configureRT() {
        int i = 0;

        // first perform some validation of the strings entered
        // for the start, stop and probe commands

        if ((tfStartCmd.getText() == null) || tfStartCmd.getText().equals("")) {
            errorMsg(messages.getString("startcommanderror"));

            return;
        }

        if (!(tfStartCmd.getText() == null) &&
                !tfStartCmd.getText().equals("") &&
                (tfStartCmd.getText().charAt(0) != '/')) {
            errorMsg(messages.getString("startpatherror"));

            return;
        }

        if (!(tfStopCmd.getText() == null) && !tfStopCmd.getText().equals("") &&
                (tfStopCmd.getText().charAt(0) != '/')) {
            errorMsg(messages.getString("stoppatherror"));

            return;
        }

        if (!(tfProbeCmd.getText() == null) &&
                !tfProbeCmd.getText().equals("") &&
                (tfProbeCmd.getText().charAt(0) != '/')) {
            errorMsg(messages.getString("probepatherror"));

            return;
        }

        // First count the total no. of "arguments" with
        // which configurert needs to be called.
        // Note that the "arguments" don't include the
        // command itself.
        // All current options take a single argument,
        // so the count is incremented by 2 (for the option
        // and its argument). If there were any options
        // which did not take an argument, the count would
        // be incremented by 1 for these options.

        // +2 for the start command, which has already been
        // ensured to be there.
        i += 2;

        if ((tfStartTO.getText() != null) && !tfStartTO.getText().equals(""))
            i += 2;

        if ((tfValidateCmd.getText() != null) &&
                !tfValidateCmd.getText().equals(""))
            i += 2;

        if ((tfValidateTO.getText() != null) &&
                !tfValidateTO.getText().equals(""))
            i += 2;

        if ((tfStopCmd.getText() != null) && !tfStopCmd.getText().equals(""))
            i += 2;

        if ((tfStopTO.getText() != null) && !tfStopTO.getText().equals(""))
            i += 2;

        if ((tfProbeCmd.getText() != null) && !tfProbeCmd.getText().equals(""))
            i += 2;

        if ((tfProbeTO.getText() != null) && !tfProbeTO.getText().equals(""))
            i += 2;

        if ((tfWorkingDir.getText() == null) ||
                tfWorkingDir.getText().equals("")) {
            errorMsg(messages.getString("Workingdirerror"));

            return;
        } else
            i += 2;

        // Now form the actual command to be run. This is
        // done by filling out the cmd array. But we first
        // need to find the size of the array. The size of the
        // array is one more than the no. of "arguments"
        // counted above. The "+1" for the command string
        // itself.
        cmd = new String[i + 1];

        int j = 0;
        cmd[j++] = "/usr/cluster/bin/scdsconfig";

        cmd[j++] = "-s";
        cmd[j++] = tfStartCmd.getText();


        if ((tfStartTO.getText() != null) && !tfStartTO.getText().equals("")) {
            cmd[j++] = "-u";
            cmd[j++] = tfStartTO.getText();
        }

        if ((tfValidateCmd.getText() != null) &&
                !tfValidateCmd.getText().equals("")) {
            cmd[j++] = "-e";
            cmd[j++] = tfValidateCmd.getText();
        }

        if ((tfValidateTO.getText() != null) &&
                !tfValidateTO.getText().equals("")) {
            cmd[j++] = "-y";
            cmd[j++] = tfValidateTO.getText();
        }

        if ((tfStopCmd.getText() != null) && !tfStopCmd.getText().equals("")) {
            cmd[j++] = "-t";
            cmd[j++] = tfStopCmd.getText();
        }

        if ((tfStopTO.getText() != null) && !tfStopTO.getText().equals("")) {
            cmd[j++] = "-v";
            cmd[j++] = tfStopTO.getText();
        }

        if ((tfProbeCmd.getText() != null) &&
                !tfProbeCmd.getText().equals("")) {
            cmd[j++] = "-m";
            cmd[j++] = tfProbeCmd.getText();
        }

        if ((tfProbeTO.getText() != null) && !tfProbeTO.getText().equals("")) {
            cmd[j++] = "-n";
            cmd[j++] = tfProbeTO.getText();
        }

        cmd[j++] = "-d";
        cmd[j++] = tfWorkingDir.getText();

        this.start();
    }

    public void start() {
        Thread runner = new Thread(this);
        runner.start();
    }

    public void run() {
        isBusy = true;
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        topPanel.setEnabled(false);

        try {
            Process process = Runtime.getRuntime().exec(cmd);
            BufferedReader brOut = new BufferedReader(new InputStreamReader(
                        process.getInputStream()));

            BufferedReader brErr = new BufferedReader(new InputStreamReader(
                        process.getErrorStream()));

            String line;

            while ((line = brOut.readLine()) != null)
                taOutputLog.append(line + "\n");

            brOut.close();

            if (process.waitFor() != 0) {

                while ((line = brErr.readLine()) != null)
                    taOutputLog.append(line + "\n");

                brErr.close();

                /*
                 * Execution of the command is failed.
                 * Check whether the current operation
                 * is create or configure and print the
                 * appropriate error message.
                 */

                if (screenName.equals("create")) {

                    Object createerrorArguments[] = {
                            tfVendorName.getText(), tfRTName.getText()
                        };

                    mf.applyPattern(messages.getString("createerror"));
                    errorMsg(mf.format(createerrorArguments));
                } else {

                    // Configuaration error
                    Object configerrorArguments[] = {
                            tfVendorName.getText(), tfRTName.getText()
                        };

                    mf.applyPattern(messages.getString("configerror"));
                    errorMsg(mf.format(configerrorArguments));
                }

            } else {

                /*
                 * The command execution is successful.
                 * Check whether the current operation
                 * is create or configure and print the
                 * appropriate informational message.
                 */
                if (screenName.equals("create")) {

                    // create ...
                    btnExec.setEnabled(false);
                    btnNext.setEnabled(true);

                    Object createsuccessArguments[] = {
                            tfVendorName.getText(), tfRTName.getText(),
                        };

                    mf.applyPattern(messages.getString("createsuccess"));
                    infoMsg(mf.format(createsuccessArguments));
                } else {

                    // config ...
                    Object configsuccessArguments[] = {
                            tfVendorName.getText(), tfRTName.getText(),
                            tfWorkingDir.getText(), tfVendorName.getText(),
                            tfRTName.getText(), "pkg"
                        };
                    mf.applyPattern(messages.getString("configsuccess"));
                    infoMsg(mf.format(configsuccessArguments));
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        topPanel.setEnabled(true);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        isBusy = false;
    }

    private void resetValues() {

        if ((config.vendorName != null) && !config.vendorName.equals(""))
            tfVendorName.setText(config.vendorName);

        if ((config.RTName != null) && !config.RTName.equals(""))
            tfRTName.setText(config.RTName);

        if ((config.RTVersion != null) && !config.RTVersion.equals(""))
            tfRTVersion.setText(config.RTVersion);

        rbScalable.setSelected(config.scalable);
        rbFailover.setSelected(!config.scalable);

        if (config.sourceType.equals("C"))
            rbC.setSelected(true);
        else if (config.sourceType.equals("KSH")) {
            rbKsh.setSelected(true);
        } else {

            // config.sourceType is "GDS"
            rbGDS.setSelected(true);
        }

        cbNetworkAware.setSelected(config.networkAware);

        if ((config.startCmd != null) && !config.startCmd.equals(""))
            tfStartCmd.setText(config.startCmd);

        if ((config.startTO != null) && !config.startTO.equals(""))
            tfStartTO.setText(config.startTO);

        if ((config.validateCmd != null) && !config.validateCmd.equals(""))
            tfValidateCmd.setText(config.validateCmd);

        if ((config.validateTO != null) && !config.validateTO.equals(""))
            tfValidateTO.setText(config.validateTO);

        if ((config.stopCmd != null) && !config.stopCmd.equals(""))
            tfStopCmd.setText(config.stopCmd);

        if ((config.stopTO != null) && !config.stopTO.equals(""))
            tfStopTO.setText(config.stopTO);

        if ((config.probeCmd != null) && !config.probeCmd.equals(""))
            tfProbeCmd.setText(config.probeCmd);

        if ((config.probeTO != null) && !config.probeTO.equals(""))
            tfProbeTO.setText(config.probeTO);
    }

    public static void main(String args[]) {

        if (args.length > 0)
            new SCDSBuilder(args[0]);
        else
            new SCDSBuilder(null);
    }
}
