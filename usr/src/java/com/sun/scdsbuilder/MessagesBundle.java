/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)MessagesBundle.java 1.19   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.scdsbuilder;

import java.util.ListResourceBundle;


public class MessagesBundle extends java.util.ListResourceBundle {

    static Object contents[][] = {
            { "c", "C" },
            { "ksh", "ksh" },
            { "gds", "GDS" },
            { "builder", "Sun Cluster Agent Builder" },
            { "filemenu", "File" },
            { "filemenumnemonic", "f" },
            { "loadrt", "Load Resource Type..." },
            { "loadrtmnemonic", "o" },
            { "exit", "Exit" },
            { "exitmnemonic", "x" },
            { "editmenu", "Edit" },
            { "editmenumnemonic", "e" },
            { "clear", "Clear Output Log" },
            { "clearmnemonic", "l" },
            { "save", "Save Output Log..." },
            { "savemnemonic", "s" },
            { "vendor", "Vendor Name:" },
            { "vendormnemonic", "v" },
            { "vendortip", "typically a stock symbol" },
            { "rtname", "Application Name:" },
            { "rtmnemonic", "a" },
            { "rttip", "Name of the Application" },
            { "rtversion", "RT Version:" },
            { "rtversionmnemonic", "r" },
            { "rtversiontip", "Resource Type Version" },
            { "workingdir", "Working Directory:" },
            { "workingdirmnemonic", "w" },
            { "workingdirtip", "location of the generated resource type" },
            { "browse", "Browse ..." },
            { "browsemnemonic", "w" },
            { "scalable", "Scalable" },
            { "failover", "Failover" },
            {
                "apptypetip",
                "whether the application is failover or " + "scalable"
            },
            { "networkaware", "Network Aware" },
            { "networkawaretip", "check if application is client-server" },
            {
                "sourcetype",
                "Type of the generated source " + "for the Resource Type"
            },
            { "sourcetypemnemonic", "t" },
            {
                "codetypetip",
                "Select the type of the generated source " + "code"
            },
            { "create", "Create" },
            { "createmnemonic", "c" },
            { "startcommand", "Start Command (or file):" },
            { "startcommandmnemonic", "s" },
            { "startcommandtip", "command to start the application" },
            { "timeout", "Timeout (in Secs):" },
            { "starttimemnemonic", "i" },
            { "starttimetip", "Timeout for the start command" },
            { "stopcommand", "Stop Command (optional):" },
            { "stopcommandmnemonic", "t" },
            { "stopcommandtip", "command to stop the application" },
            { "stoptime", "Timeout (in Secs):" },
            { "stoptimemnemonic", "m" },
            { "stoptimetip", "Timeout for the stop command" },
            { "validatecommand", "Validate Command (optional):" },
            { "validatecommandmnemonic", "v" },
            { "validatecommandtip", "command to perform validations" },
            { "validatetime", "Timeout (in Secs):" },
            { "validatetimemnemonic", "x" },
            { "validatetimetip", "Timeout for the validate command" },
            { "probecommand", "Probe Command (optional):" },
            { "probecommandmnemonic", "r" },
            {
                "probecommandtip",
                "command to do periodic health checks " + "on application"
            },
            { "probetime", "Timeout (in Secs):" },
            { "probetimemnemonic", "b" },
            { "probetimetip", "Timeout for the probe command" },
            { "probetimemnemonic", "u" },
            { "configure", "Configure" },
            { "stepone", "Step 1 of 2:" },
            { "steponemnemonic", "s" },
            { "createtip", "create the resource type" },
            { "previous", "<<Previous" },
            { "previousmnemonic", "p" },
            { "previoustip", "Create Screen" },
            { "next", "Next>>" },
            { "nextmnemonic", "n" },
            { "nexttip", "Configure Screen" },
            { "exittip", "Quit the SCDSBuilder" },
            { "output", "Output Log" },
            { "steptwo", "Step 2 of 2:" },
            { "configuretip", "configure the resource type" },
            { "selectstart", "Select the Start Command" },
            { "select", "Select" },
            { "selectstop", "Select the Stop Command" },
            { "selectvalidate", "Select the Validate Command" },
            { "selectprobe", "Select the Probe Command" },
            { "selectworkingdir", "Select the Working Directory" },
            {
                "selectloaddir",
                "Select the directory to load Resource " + "Type from"
            },
            { "load", "Load" },
            { "loadmnemonic", "l" },
            { "logfile", "Select the file to save output log in" },
            { "error", "Error!" },
            { "success", "Success!" },
            {
                "namelengtherror",
                "The combined length of Vendor Name and " +
                "Resource Type Name\nmust be less than or equal to 32 " +
                "characters."
            },
            { "vendorerror", "Vendor Name must be specified." },
            { "rterror", "Resource Type Name must be specified." },
            { "rtversionerror", "Resource Type Version must be specified." },
            { "workingdirerror", "Working Directory must be specified." },
            { "startcommanderror", "Start Command must be specified." },
            {
                "startpatherror",
                "Fully qualified path name must be " +
                "specified for the start command."
            },
            {
                "stoppatherror",
                "Fully qualified path name must be " +
                "specified for the stop command."
            },
            {
                "probepatherror",
                "Fully qualified path name must be " +
                "specified for the probe command."
            },
            {
                "doublequoteserror",
                "Double quotes (\" \") not permitted " +
                "in the command line.\nUse single quotes (' ') instead."
            },
            {
                "semicolonerror",
                "Semi colon (;) not permitted in the " +
                "command line.\nYou may put the comamnd in a file instead."
            },
            {
                "compilererror",
                "Resource Type {0} requires C compiler, " +
                "which was not found in your PATH.\nPlease make sure cc " +
                "is in your path and restart the Builder before loading " +
                "this resource type."
            },
            {
                "createerror",
                "Creation of Resource Type {0}.{1} failed.\n" +
                "Check output log for more details."
            },
            {
                "configerror",
                "Configuration of Resource Type {0}.{1} " +
                "failed.\nCheck output log for more details."
            },
            {
                "createsuccess",
                "Creation of Resource Type {0}.{1} " + "completed successfully."
            },
            {
                "configsuccess",
                "Configuration of Resource Type {0}.{1} " +
                "completed successfully.\nThe package is in {2}/{3}{4}/{5}."
            }
        };

    public Object[][] getContents() {
        return contents;
    }
}
