/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)CreatePanel.java 1.10   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.scdsbuilder_nb;

import org.openide.TopManager;

import org.openide.filesystems.FileObject;

import org.openide.util.HttpServer;
import org.openide.util.NbBundle;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import java.io.File;

import java.lang.Throwable;

import java.net.URL;

import javax.accessibility.AccessibleRelation;
import javax.accessibility.AccessibleRelationSet;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


public class CreatePanel extends JPanel
    implements javax.swing.event.DocumentListener,
        java.awt.event.ActionListener {

    /**
     * The wizard panel descriptor associated with this GUI panel. If you need
     * to fire state changes or something similar, you can use this handle to do
     * so.
     */
    private final CreatePanelDescriptor panel;

    private JTextField tfVendorName;
    private JTextField tfRTName;
    private JTextField tfWorkingDir;
    private JButton btnSelectInsDir;
    private JCheckBox cbNetworkAware;

    private JRadioButton rbScalable;
    private JRadioButton rbFailover;
    private ButtonGroup bgType;

    private JRadioButton rbC;
    private JRadioButton rbKsh;
    private JRadioButton rbGDS;
    private ButtonGroup bgSourceType;

    private JFileChooser fileChooser = new JFileChooser(System.getProperty(
                "user.dir"));

    private AccessibleRelationSet tfRelSet;

    private ConfigInfo config;
    private boolean needCreate;

    /**
     * Creates a new instance of CreatePanel
     */
    public CreatePanel(CreatePanelDescriptor desc) {
        panel = desc;

        this.setLayout(new GridBagLayout());

        // create the createPanel and fill it up with various
        // components
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        gbc.insets = new Insets(2, 10, 2, 10);

        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        gbc.insets = new Insets(2, 10, 2, 10);

        JLabel label = new JLabel(i18msg("vendor"));
        addgb(label, gbc, this, 0, 0, 1, 1);
        tfVendorName = new JTextField("", 10);
        tfVendorName.setDocument(new IdentifierDocument());
        tfVendorName.setToolTipText(i18msg("vendortip"));
        tfVendorName.getAccessibleContext().setAccessibleName(i18msg("vendor"));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(tfVendorName, gbc, this, 0, 1, 1, 1);
        tfRelSet = tfVendorName.getAccessibleContext()
            .getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setDisplayedMnemonic(i18msg("vendormnemonic").charAt(0));
        label.setLabelFor(tfVendorName);
        tfVendorName.getDocument().addDocumentListener(this);

        label = new JLabel(i18msg("rtname"));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        addgb(label, gbc, this, 1, 0, 1, 1);
        tfRTName = new JTextField("", 15);
        tfRTName.setDocument(new IdentifierDocument());
        tfRTName.setToolTipText(i18msg("rttip"));
        tfRTName.getAccessibleContext().setAccessibleName(i18msg("rtname"));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(tfRTName, gbc, this, 1, 1, 1, 1);
        tfRelSet = tfRTName.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setDisplayedMnemonic(i18msg("rtmnemonic").charAt(0));
        label.setLabelFor(tfRTName);
        tfRTName.getDocument().addDocumentListener(this);

        label = new JLabel(i18msg("workingdir"));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        addgb(label, gbc, this, 0, 2, 1, 1);
        tfWorkingDir = new JTextField("", 30);

        // Instantiate the tfWorkingDir with the user's current
        // directory
        tfWorkingDir.setText(System.getProperty("user.dir"));

        tfWorkingDir.setToolTipText(i18msg("workingdirtip"));
        tfWorkingDir.getAccessibleContext().setAccessibleName(i18msg(
                "workingdir"));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(tfWorkingDir, gbc, this, 0, 3, 2, 1);
        tfRelSet = tfWorkingDir.getAccessibleContext()
            .getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setDisplayedMnemonic(i18msg("workingdirmnemonic").charAt(0));
        label.setLabelFor(tfWorkingDir);
        tfWorkingDir.getDocument().addDocumentListener(this);

        btnSelectInsDir = new JButton(i18msg("browse"));
        btnSelectInsDir.addActionListener(this);
        btnSelectInsDir.setMnemonic(i18msg("browsemnemonic").charAt(0));
        btnSelectInsDir.getAccessibleContext().setAccessibleDescription(i18msg(
                "browse"));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(btnSelectInsDir, gbc, this, 2, 3, 1, 1);

        JPanel rbTypePanel = new JPanel();
        rbTypePanel.setBorder(BorderFactory.createEtchedBorder());
        bgType = new ButtonGroup();
        rbScalable = new JRadioButton(i18msg("scalable"));
        rbScalable.setToolTipText(i18msg("apptypetip"));
        bgType.add(rbScalable);
        rbTypePanel.add(rbScalable);
        rbFailover = new JRadioButton(i18msg("failover"));
        bgType.add(rbFailover);
        rbFailover.setToolTipText(i18msg("apptypetip"));
        rbTypePanel.add(rbFailover);
        rbTypePanel.setToolTipText(i18msg("apptypetip"));

        cbNetworkAware = new JCheckBox(i18msg("networkaware"));
        cbNetworkAware.setToolTipText(i18msg("networkawaretip"));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(rbTypePanel, gbc, this, 0, 4, 1, 1);
        addgb(cbNetworkAware, gbc, this, 1, 4, 1, 1);

        JPanel rbSourceTypePanel = new JPanel();
        rbSourceTypePanel.setBorder(BorderFactory.createEtchedBorder());
        label = new JLabel(i18msg("sourcetype"));
        rbSourceTypePanel.add(label);
        bgSourceType = new ButtonGroup();
        tfRelSet = rbSourceTypePanel.getAccessibleContext()
            .getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setDisplayedMnemonic(i18msg("sourcetypemnemonic").charAt(0));
        label.setLabelFor(rbSourceTypePanel);

        rbC = new JRadioButton(i18msg("c"));
        bgSourceType.add(rbC);
        rbSourceTypePanel.add(rbC);
        rbC.addActionListener(this);
        rbC.setToolTipText(i18msg("codetypetip"));

        rbKsh = new JRadioButton(i18msg("ksh"));
        bgSourceType.add(rbKsh);
        rbSourceTypePanel.add(rbKsh);
        rbKsh.addActionListener(this);
        rbKsh.setToolTipText(i18msg("codetypetip"));

        rbGDS = new JRadioButton(i18msg("gds"));
        bgSourceType.add(rbGDS);
        rbSourceTypePanel.add(rbGDS);
        rbGDS.addActionListener(this);
        rbGDS.setToolTipText(i18msg("codetypetip"));

        addgb(rbSourceTypePanel, gbc, this, 0, 5, 3, 1);
    }

    // Utility routine for adding Components to a Container having
    // GridBagLayout as its manager.
    private void addgb(Component c, GridBagConstraints gbc, Container con,
        int x, int y, int w, int h) {
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.gridwidth = w;
        gbc.gridheight = h;
        gbc.weightx = 100;
        gbc.weighty = 100;
        con.add(c, gbc);
    }

    public void changedUpdate(javax.swing.event.DocumentEvent documentEvent) {
        panel.fireChangeEvent();
    }

    public void insertUpdate(javax.swing.event.DocumentEvent documentEvent) {

        if (documentEvent.getDocument() == tfWorkingDir.getDocument()) {
            resetValues();
        }

        panel.fireChangeEvent();
    }

    public void removeUpdate(javax.swing.event.DocumentEvent documentEvent) {

        if (documentEvent.getDocument() == tfWorkingDir.getDocument()) {
            resetValues();
        }

        panel.fireChangeEvent();
    }

    public String i18msg(String key) {
        return NbBundle.getMessage(AgentBuilderWizardIterator.class, key);
    }

    // The catch-all routine for the action events from all the buttons
    // in the gui.
    public void actionPerformed(ActionEvent evt) {

        if (evt.getSource() == btnSelectInsDir) {
            selectFile((JComponent) evt.getSource());
        } else if ((evt.getSource() == rbKsh) || (evt.getSource() == rbGDS)) {
            cbNetworkAware.setSelected(true);
            cbNetworkAware.setEnabled(false);
        } else if (evt.getSource() == rbC) {
            cbNetworkAware.setEnabled(true);
        }
    }

    private void selectFile(JComponent fileSelectBtn) {
        String title = "";
        JTextField targetTextField = null;
        int state = JFileChooser.CANCEL_OPTION;

        fileChooser.setSelectedFile(null);

        title = i18msg("selectworkingdir");
        fileChooser.setToolTipText(i18msg("selectworkingdir"));
        targetTextField = tfWorkingDir;
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setDialogTitle(title);
        fileChooser.getAccessibleContext().setAccessibleDescription(i18msg(
                "selectworkingdir"));
        state = fileChooser.showOpenDialog(this);

        File file = fileChooser.getSelectedFile();

        if (state != JFileChooser.CANCEL_OPTION) {
            targetTextField.setText(file.getPath());
            resetValues();
        }
    }

    private void resetValues() {

        File configFile = new File(tfWorkingDir.getText() + "/rtconfig");

        // Determine if the rtconfig file exists in
        // the working dir selected
        if (configFile.exists()) {
            config = new ConfigInfo(tfWorkingDir.getText());
            needCreate = false;
        } else {
            config = null;
            needCreate = true;

            return;
        }

        if ((config.vendorName != null) && !config.vendorName.equals(""))
            tfVendorName.setText(config.vendorName);

        if ((config.RTName != null) && !config.RTName.equals(""))
            tfRTName.setText(config.RTName);

        rbScalable.setSelected(config.scalable);
        rbFailover.setSelected(!config.scalable);

        if (config.sourceType.equals("C"))
            rbC.setSelected(true);
        else if (config.sourceType.equals("KSH")) {
            rbKsh.setSelected(true);
            cbNetworkAware.setSelected(true);
            cbNetworkAware.setEnabled(false);
        } else {

            // config.sourceType is "GDS"
            rbGDS.setSelected(true);
            cbNetworkAware.setSelected(true);
            cbNetworkAware.setEnabled(false);
        }

        cbNetworkAware.setSelected(config.networkAware);
    }

    public boolean fieldsFilled() {
        boolean filled = false;

        if ((getVendor() != null) && (getApplication() != null) &&
                (getDirectory() != null) && (getVendor().length() > 0) &&
                (getApplication().length() > 0) &&
                (getDirectory().length() > 0))
            filled = true;

        return filled;
    }

    public void setConfig(ConfigInfo cf) {
        config = cf;
    }

    public ConfigInfo getConfig() {
        return config;
    }

    public void setVendor(String vendor) {
        tfVendorName.setText(vendor);
    }

    public String getVendor() {
        return tfVendorName.getText();
    }

    public void setApplication(String app) {
        tfRTName.setText(app);
    }

    public String getApplication() {
        return tfRTName.getText();
    }

    public void setDirectory(String dir) {

        if ((dir != null) && !dir.equals(tfWorkingDir.getText())) {
            tfWorkingDir.setText(dir);
        }
    }

    public String getDirectory() {
        return tfWorkingDir.getText();
    }

    public void setScalable(boolean scalable) {
        rbScalable.setSelected(scalable);
        rbFailover.setSelected(!scalable);
    }

    public boolean getScalable() {
        return rbScalable.isSelected();
    }

    public void setNetworkAware(boolean aware) {
        cbNetworkAware.setSelected(aware);
    }

    public boolean getNetworkAware() {
        return cbNetworkAware.isSelected();
    }

    public boolean getNeedCreate() {
        return needCreate;
    }

    public void setNeedCreate(boolean val) {
        needCreate = val;
    }

    public void setCodeType(int ctype) {

        switch (ctype) {

        case AgentBuilderWizardIterator.CCodeType:
            rbC.setSelected(true);

            break;

        case AgentBuilderWizardIterator.KshCodeType:
            rbKsh.setSelected(true);

            break;

        case AgentBuilderWizardIterator.GDSCodeType:
            rbGDS.setSelected(true);

            break;
        }
    }

    public int getCodeType() {

        if (rbC.isSelected()) {
            return AgentBuilderWizardIterator.CCodeType;
        } else if (rbKsh.isSelected()) {
            return AgentBuilderWizardIterator.KshCodeType;
        } else {
            return AgentBuilderWizardIterator.GDSCodeType;
        }
    }
}
