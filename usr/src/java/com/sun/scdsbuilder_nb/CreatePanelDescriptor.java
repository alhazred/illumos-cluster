/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)CreatePanelDescriptor.java 1.9   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.scdsbuilder_nb;

import org.openide.WizardDescriptor;

import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

import java.awt.Component;

import java.io.File;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * A single panel descriptor for a wizard. You probably want to make a wizard
 * iterator to hold it.
 */
public class CreatePanelDescriptor implements WizardDescriptor.Panel
    /* .FinishPanel */ {

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private CreatePanel component;

    // public final void addChangeListener(ChangeListener l) {}
    // public final void removeChangeListener(ChaimplngeListener l) {}

    private final Set listeners = new HashSet(1); // Set<ChangeListener>

    /**
     * Create the wizard panel descriptor.
     */
    public CreatePanelDescriptor() {
    }

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    public Component getComponent() {

        if (component == null) {
            component = new CreatePanel(this);
        }

        return component;
    }

    public HelpCtx getHelp() {

        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
            // If you have context help:
            // return new HelpCtx (CreatePanelDescriptor.class);
    }

    public boolean isValid() {

        // If it is always OK to press Next or Finish, then:
        return component.fieldsFilled();
            // If it depends on some condition (form filled out...), then:
            // return someCondition ();
            // and when this condition changes (last form field filled in...)
            // then:
            // fireChangeEvent ();
            // and uncomment the complicated stuff below.
    }

    public final void addChangeListener(ChangeListener l) {

        synchronized (listeners) {
            listeners.add(l);
        }
    }

    public final void removeChangeListener(ChangeListener l) {

        synchronized (listeners) {
            listeners.remove(l);
        }
    }

    protected final void fireChangeEvent() {
        Iterator it;

        synchronized (listeners) {
            it = new HashSet(listeners).iterator();
        }

        ChangeEvent ev = new ChangeEvent(this);

        while (it.hasNext()) {
            ((ChangeListener) it.next()).stateChanged(ev);
        }
    }

    // You can use a settings object to keep track of state.
    // Normally the settings object will be the WizardDescriptor,
    // so you can use WizardDescriptor.getProperty & putProperty
    // to store information entered by the user.
    public void readSettings(Object settings) {
        WizardDescriptor desc = (WizardDescriptor) settings;
        component.setVendor((String) desc.getProperty("Vendor"));
        component.setApplication((String) desc.getProperty("Application"));
        component.setDirectory((String) desc.getProperty("Directory"));

        Boolean scalable = (Boolean) desc.getProperty("Scalable");
        component.setScalable(scalable.booleanValue());

        Boolean netaware = (Boolean) desc.getProperty("NetworkAware");
        component.setNetworkAware(netaware.booleanValue());

        Integer codeType = (Integer) desc.getProperty("CodeType");
        component.setCodeType(codeType.intValue());
        component.setConfig((ConfigInfo) desc.getProperty("Config"));

        Boolean needCreate = (Boolean) desc.getProperty("NeedCreate");
        component.setNeedCreate(needCreate.booleanValue());
    }

    public void storeSettings(Object settings) {
        WizardDescriptor desc = (WizardDescriptor) settings;
        desc.putProperty("Vendor", component.getVendor());
        desc.putProperty("Application", component.getApplication());
        desc.putProperty("Directory", component.getDirectory());
        desc.putProperty("Scalable", new Boolean(component.getScalable()));
        desc.putProperty("NetworkAware",
            new Boolean(component.getNetworkAware()));
        desc.putProperty("CodeType", new Integer(component.getCodeType()));
        desc.putProperty("Config", component.getConfig());
        desc.putProperty("NeedCreate", new Boolean(component.getNeedCreate()));
        desc.putProperty("ProbeEnabled",
            new Boolean(component.getNetworkAware()));
    }

}
