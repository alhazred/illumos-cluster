/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)ConfigPanel.java 1.10   08/05/20 SMI
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.scdsbuilder_nb;

import org.openide.util.NbBundle;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import java.io.File;

import javax.accessibility.AccessibleRelation;
import javax.accessibility.AccessibleRelationSet;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


public class ConfigPanel extends JPanel
    implements javax.swing.event.DocumentListener,
        java.awt.event.ActionListener {

    /**
     * The wizard panel descriptor associated with this GUI panel. If you need
     * to fire state changes or something similar, you can use this handle to do
     * so.
     */
    private final ConfigPanelDescriptor panel;

    private JTextField tfStartCmd;
    private JButton btnSelectStartCmd;
    private JTextField tfStartTO;

    private JTextField tfStopCmd;
    private JButton btnSelectStopCmd;
    private JTextField tfStopTO;

    private JTextField tfProbeCmd;
    private JButton btnSelectProbeCmd;
    private JTextField tfProbeTO;

    private JFileChooser fileChooser = new JFileChooser(System.getProperty(
                "user.dir"));

    private boolean probeEnabled;

    /**
     * Creates a new instance of ConfigPanel
     */
    public ConfigPanel(ConfigPanelDescriptor desc) {
        panel = desc;

        this.setLayout(new GridBagLayout());

        // create the configPanel and fill it up with various
        // components
        GridBagConstraints gbc = new GridBagConstraints();
        this.setLayout(new GridBagLayout());

        AccessibleRelationSet tfRelSet;
        JLabel label = new JLabel(i18msg("startcommand"));
        label.setDisplayedMnemonic(i18msg("startcommandmnemonic").charAt(0));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        gbc.insets = new Insets(2, 10, 2, 10);
        addgb(label, gbc, this, 0, 0, 1, 1);
        tfStartCmd = new JTextField("", 30);
        tfStartCmd.setDocument(new CmdDocument());
        tfStartCmd.setToolTipText(i18msg("startcommandtip"));
        tfStartCmd.getAccessibleContext().setAccessibleName(i18msg(
                "startcommand"));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(tfStartCmd, gbc, this, 0, 1, 1, 1);
        tfRelSet = tfStartCmd.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfStartCmd);
        tfStartCmd.getDocument().addDocumentListener(this);

        btnSelectStartCmd = new JButton(i18msg("browse"));
        btnSelectStartCmd.addActionListener(this);
        btnSelectStartCmd.setMnemonic(i18msg("browsemnemonic").charAt(0));
        btnSelectStartCmd.getAccessibleContext().setAccessibleDescription(
            i18msg("browse"));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(btnSelectStartCmd, gbc, this, 1, 1, 1, 1);

        label = new JLabel(i18msg("timeout"));
        label.setDisplayedMnemonic(i18msg("starttimemnemonic").charAt(0));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        addgb(label, gbc, this, 2, 0, 1, 1);
        tfStartTO = new JTextField("", 10);
        tfStartTO.setDocument(new IntegerDocument());
        tfStartTO.setToolTipText(i18msg("starttimetip"));
        tfStartTO.getAccessibleContext().setAccessibleName(i18msg("timeout"));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(tfStartTO, gbc, this, 2, 1, 1, 1);
        tfRelSet = tfStartTO.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfStartTO);

        label = new JLabel(i18msg("stopcommand"));
        label.setDisplayedMnemonic(i18msg("stopcommandmnemonic").charAt(0));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        addgb(label, gbc, this, 0, 2, 1, 1);
        tfStopCmd = new JTextField("", 30);
        tfStopCmd.setDocument(new CmdDocument());
        tfStopCmd.setToolTipText(i18msg("stopcommandtip"));
        tfStopCmd.getAccessibleContext().setAccessibleName(i18msg(
                "stopcommand"));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(tfStopCmd, gbc, this, 0, 3, 1, 1);
        tfRelSet = tfStartCmd.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfStopCmd);
        tfStopCmd.getDocument().addDocumentListener(this);

        btnSelectStopCmd = new JButton(i18msg("browse"));
        btnSelectStopCmd.setMnemonic(i18msg("browsemnemonic").charAt(0));
        btnSelectStopCmd.addActionListener(this);
        btnSelectStopCmd.getAccessibleContext().setAccessibleDescription(i18msg(
                "browse"));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(btnSelectStopCmd, gbc, this, 1, 3, 1, 1);

        label = new JLabel(i18msg("stoptime"));
        label.setDisplayedMnemonic(i18msg("stoptimemnemonic").charAt(0));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        addgb(label, gbc, this, 2, 2, 1, 1);
        tfStopTO = new JTextField("", 10);
        tfStopTO.setDocument(new IntegerDocument());
        tfStopTO.setToolTipText(i18msg("stoptimetip"));
        tfStopTO.getAccessibleContext().setAccessibleName(i18msg("timeout"));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(tfStopTO, gbc, this, 2, 3, 1, 1);
        tfRelSet = tfStopTO.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfStopTO);

        label = new JLabel(i18msg("probecommand"));
        label.setDisplayedMnemonic(i18msg("probecommandmnemonic").charAt(0));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        addgb(label, gbc, this, 0, 4, 1, 1);
        tfProbeCmd = new JTextField("", 30);
        tfProbeCmd.setDocument(new CmdDocument());
        tfProbeCmd.setToolTipText(i18msg("probecommandtip"));
        tfProbeCmd.getAccessibleContext().setAccessibleName(i18msg(
                "probecommand"));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(tfProbeCmd, gbc, this, 0, 5, 1, 1);
        tfRelSet = tfProbeCmd.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfProbeCmd);
        tfProbeCmd.getDocument().addDocumentListener(this);

        btnSelectProbeCmd = new JButton(i18msg("browse"));
        btnSelectProbeCmd.setMnemonic(i18msg("browsemnemonic").charAt(0));
        btnSelectProbeCmd.addActionListener(this);
        btnSelectProbeCmd.getAccessibleContext().setAccessibleDescription(
            i18msg("browse"));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(btnSelectProbeCmd, gbc, this, 1, 5, 1, 1);

        label = new JLabel(i18msg("probetime"));
        label.setDisplayedMnemonic(i18msg("probetimemnemonic").charAt(0));
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.SOUTHWEST;
        addgb(label, gbc, this, 2, 4, 1, 1);
        tfProbeTO = new JTextField("", 10);
        tfProbeTO.setDocument(new IntegerDocument());
        tfProbeTO.setToolTipText(i18msg("probetimetip"));
        tfProbeTO.getAccessibleContext().setAccessibleName(i18msg("probetime"));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        addgb(tfProbeTO, gbc, this, 2, 5, 1, 1);
        tfRelSet = tfProbeTO.getAccessibleContext().getAccessibleRelationSet();
        tfRelSet.add(new AccessibleRelation(AccessibleRelation.LABELED_BY,
                label));
        label.setLabelFor(tfProbeTO);
    }

    // Utility routine for adding Components to a Container having
    // GridBagLayout as its manager.
    private void addgb(Component c, GridBagConstraints gbc, Container con,
        int x, int y, int w, int h) {
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.gridwidth = w;
        gbc.gridheight = h;
        gbc.weightx = 100;
        gbc.weighty = 100;
        con.add(c, gbc);
    }

    public void changedUpdate(javax.swing.event.DocumentEvent documentEvent) {
        panel.fireChangeEvent();
    }

    public void insertUpdate(javax.swing.event.DocumentEvent documentEvent) {
        panel.fireChangeEvent();
    }

    public void removeUpdate(javax.swing.event.DocumentEvent documentEvent) {
        panel.fireChangeEvent();
    }

    public String i18msg(String key) {
        return NbBundle.getMessage(AgentBuilderWizardIterator.class, key);
    }

    // The catch-all routine for the action events from all the buttons
    // in the gui.
    public void actionPerformed(ActionEvent evt) {

        if (evt.getSource() == btnSelectStartCmd) {
            selectFile((JComponent) evt.getSource(), tfStartCmd, "selectstart");
        } else if (evt.getSource() == btnSelectStopCmd) {
            selectFile((JComponent) evt.getSource(), tfStopCmd, "selectstop");
        } else {
            selectFile((JComponent) evt.getSource(), tfProbeCmd, "selectprobe");
        }
    }

    private void selectFile(JComponent fileSelectBtn, JTextField tf,
        String msgkey) {
        int state = JFileChooser.CANCEL_OPTION;

        fileChooser.setSelectedFile(null);

        String msg = i18msg(msgkey);
        fileChooser.setToolTipText(msg);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setDialogTitle(msg);
        fileChooser.getAccessibleContext().setAccessibleDescription(msg);
        state = fileChooser.showDialog(this, i18msg("select"));

        File file = fileChooser.getSelectedFile();

        if (state != JFileChooser.CANCEL_OPTION) {
            tf.setText(file.getPath());
        }
    }

    public boolean fieldsFilled() {
        boolean filled = false;

        if ((getStartCmd() != null) && (getStartCmd().length() > 0))
            filled = true;

        return filled;
    }

    public void setStartCmd(String cmd) {
        tfStartCmd.setText(cmd);
    }

    public String getStartCmd() {
        return tfStartCmd.getText();
    }

    public void setStartTO(String to) {
        tfStartTO.setText(to);
    }

    public String getStartTO() {
        return tfStartTO.getText();
    }

    public void setStopCmd(String cmd) {
        tfStopCmd.setText(cmd);
    }

    public String getStopCmd() {
        return tfStopCmd.getText();
    }

    public void setStopTO(String to) {
        tfStopTO.setText(to);
    }

    public String getStopTO() {
        return tfStopTO.getText();
    }

    public void setProbeCmd(String cmd) {
        tfProbeCmd.setText(cmd);
    }

    public String getProbeCmd() {
        return tfProbeCmd.getText();
    }

    public void setProbeTO(String to) {
        tfProbeTO.setText(to);
    }

    public String getProbeTO() {
        return tfProbeTO.getText();
    }

    public void setProbeEnabled(boolean val) {
        probeEnabled = val;

        if (probeEnabled) {
            tfProbeCmd.setEnabled(true);
            btnSelectProbeCmd.setEnabled(true);
            tfProbeTO.setEnabled(true);
        } else {
            setProbeCmd("");
            tfProbeCmd.setEnabled(false);
            btnSelectProbeCmd.setEnabled(false);
            tfProbeTO.setEnabled(false);
        }
    }

    public void setConfig(ConfigInfo cf) {

        if (cf != null) {
            setStartCmd(cf.startCmd);
            setStartTO(cf.startTO);
            setStopCmd(cf.stopCmd);
            setStopTO(cf.stopTO);
            setProbeCmd(cf.probeCmd);
            setProbeTO(cf.probeTO);
        }
    }
}
