/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)ConfigInfo.java 1.7   08/05/20 SMI
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.scdsbuilder_nb;

import java.io.*;

import java.util.*;


class ConfigInfo {

    public String vendorName;
    public String RTName;
    public boolean scalable = false;
    public boolean networkAware = true;
    public String sourceType = "C";
    public String startCmd;
    public String startTO;
    public String stopCmd;
    public String stopTO;
    public String probeCmd;
    public String probeTO;

    public ConfigInfo(String dirName) {

        try {
            File configFile = new File(dirName + "/rtconfig");

            if (configFile.exists()) {

                // If config file does not exist, then this
                // is a brand new resource type.
                // If it exists, read the config info from
                // config file.
                BufferedReader in = new BufferedReader(new FileReader(
                            configFile));

                String line;

                while ((line = in.readLine()) != null) {
                    StringTokenizer st = new StringTokenizer(line, "=");

                    // Ignore the = token.
                    String param = st.nextToken();
                    String value = "";

                    // There may not be any more tokens,
                    // in case none was specified
                    // originally.
                    if (st.hasMoreTokens())
                        value = st.nextToken();

                    if (param.equals("RT_NAME"))
                        RTName = value;
                    else if (param.equals("RT_VENDOR"))
                        vendorName = value;
                    else if (param.equals("SOURCE_TYPE"))
                        sourceType = value;
                    else if (param.equals("START_COMMAND"))
                        startCmd = value;
                    else if (param.equals("START_TIMEOUT"))
                        startTO = value;
                    else if (param.equals("STOP_COMMAND"))
                        stopCmd = value;
                    else if (param.equals("STOP_TIMEOUT"))
                        stopTO = value;
                    else if (param.equals("PROBE_COMMAND"))
                        probeCmd = value;
                    else if (param.equals("PROBE_TIMEOUT"))
                        probeTO = value;
                    else if (param.equals("SCALABLE"))
                        scalable = Boolean.valueOf(value).booleanValue();
                    else if (param.equals("STAND_ALONE")) {

                        // note that networkAware and
                        // STAND_ALONE (which appears
                        // in the config file) have
                        // inverse semantics
                        networkAware = !(Boolean.valueOf(value).booleanValue());
                    }
                }

                in.close();
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
