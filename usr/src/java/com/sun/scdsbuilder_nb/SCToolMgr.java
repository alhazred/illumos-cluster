/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)SCToolMgr.java 1.10   08/05/20 SMI
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.scdsbuilder_nb;

import org.openide.TopManager;

import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;

import java.io.InputStream;
import java.io.InputStreamReader;

import java.lang.String;


public class SCToolMgr extends java.lang.Object {

    String working_dir;
    OutputWriter display;

    /**
     * Creates a new instance of SCToolMgr
     */
    public SCToolMgr(String directory) throws java.io.IOException {
        working_dir = directory;

        InputOutput display_tab = TopManager.getDefault().getIO(
                "Building Agent", false);
        display_tab.select();
        display = display_tab.getOut();
        display.reset();
    }

    // Properties from System.getProperty that we may want to use:
    //
    // netbeans.home: netbeans installation dir (e.g., /usr/ffj)
    // forte.fcc: /opt/SUNWspro or something like it

    void create(String vendor, String name, boolean net_aware, boolean scalable,
        boolean korn, boolean gds) throws java.io.IOException {
        int args = 7;

        if (scalable) {
            args++;
        }

        if (!net_aware) {
            args++;
        }

        if (korn || gds) {
            args++;
        }

        String cmd[] = new String[args];
        cmd[0] = "scdscreate";
        cmd[1] = "-V";
        cmd[2] = vendor;
        cmd[3] = "-T";
        cmd[4] = name;
        cmd[5] = "-d";
        cmd[6] = working_dir;

        int arg = 7;

        if (!net_aware) {
            cmd[arg++] = "-a";
        }

        if (korn) {
            cmd[arg++] = "-k";
        } else if (gds) {
            cmd[arg++] = "-g";
        }

        if (scalable) {
            cmd[arg++] = "-s";
        }

        runcmd(cmd);
    }

    void config(String startcmd, String stopcmd, String probecmd,
        String startTO, String stopTO, String probeTO)
        throws java.io.IOException {
        int args = 5;

        if ((startTO != null) && (startTO.length() > 0))
            args += 2;

        if ((stopcmd != null) && (stopcmd.length() > 0))
            args += 2;

        if ((stopTO != null) && (stopTO.length() > 0))
            args += 2;

        if ((probecmd != null) && (probecmd.length() > 0))
            args += 2;

        if ((probeTO != null) && (probeTO.length() > 0))
            args += 2;

        String cmd[] = new String[args];
        cmd[0] = "scdsconfig";
        cmd[1] = "-s";
        cmd[2] = startcmd;

        int arg = 3;

        if ((startTO != null) && (startTO.length() > 0)) {
            cmd[arg++] = "-u";
            cmd[arg++] = startTO;
        }

        if ((stopcmd != null) && (stopcmd.length() > 0)) {
            cmd[arg++] = "-t";
            cmd[arg++] = stopcmd;
        }

        if ((stopTO != null) && (stopTO.length() > 0)) {
            cmd[arg++] = "-v";
            cmd[arg++] = stopTO;
        }

        if ((probecmd != null) && (probecmd.length() > 0)) {
            cmd[arg++] = "-m";
            cmd[arg++] = probecmd;
        }

        if ((probeTO != null) && (probeTO.length() > 0)) {
            cmd[arg++] = "-n";
            cmd[arg++] = probeTO;
        }

        cmd[arg++] = "-d";
        cmd[arg++] = working_dir;
        runcmd(cmd);
    }

    void runcmd(String cmd[]) throws java.io.IOException {

        for (int i = 0; i < cmd.length; i++) {
            display.write(quote(cmd[i]) + " ");
        }

        display.write("\n");

        Process proc = java.lang.Runtime.getRuntime().exec(cmd);
        InputStreamReader pstdout = new InputStreamReader(proc
                .getInputStream());
        InputStreamReader pstderr = new InputStreamReader(proc
                .getErrorStream());

        class StreamShow extends Thread {
            StreamShow(InputStreamReader r) {
                reader = r;
            }

            public void run() {

                try {
                    showrun(reader);
                } catch (java.io.IOException e) {
                }
            }

            private InputStreamReader reader;
        }

        StreamShow showerr = new StreamShow(pstderr);
        showrun(pstdout);
    }

    String quote(String s) {

        if (s.indexOf(' ') != -1) {
            return "\"" + s + "\"";
        } else {
            return s;
        }
    }

    void showrun(InputStreamReader reader) throws java.io.IOException {
        char buffer[] = new char[100];
        int n = reader.read(buffer, 0, 100);

        while (n > 0) {
            display.write(buffer, 0, n);
            n = reader.read(buffer, 0, 100);
        }
    }
}
