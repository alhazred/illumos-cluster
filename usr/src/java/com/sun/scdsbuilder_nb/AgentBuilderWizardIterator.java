/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)AgentBuilderWizardIterator.java 1.10   08/05/20 SMI
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.scdsbuilder_nb;

import org.openide.NotifyDescriptor;
import org.openide.TopManager;
import org.openide.WizardDescriptor;

import org.openide.cookies.OpenCookie;

import org.openide.filesystems.LocalFileSystem;
import org.openide.filesystems.Repository;

import org.openide.loaders.*;

import org.openide.util.NbBundle;

import java.awt.Component;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * A template wizard iterator (sequence of panels). Used to fill in the second
 * and subsequent panels in the New wizard. Associate this to a template inside
 * a layer using the Sequence of Panels extra property. Create one or more
 * panels from template as needed too.
 */
public class AgentBuilderWizardIterator implements TemplateWizard.Iterator {

    public static final int CCodeType = 0;
    public static final int KshCodeType = 1;
    public static final int GDSCodeType = 2;

    // --- The rest probably does not need to be touched. ---

    private transient int index;
    private transient WizardDescriptor.Panel panels[];
    private transient TemplateWizard wiz;

    // You should define what panels you want to use here:

    protected WizardDescriptor.Panel[] createPanels() {
        wiz.putProperty("Scalable", new Boolean(false));
        wiz.putProperty("NetworkAware", new Boolean(true));
        wiz.putProperty("CodeType", new Integer(0));
        wiz.putProperty("StartTO", "300");
        wiz.putProperty("StopTO", "300");
        wiz.putProperty("ProbeTO", "30");
        wiz.putProperty("NeedCreate", new Boolean(true));
        wiz.putProperty("ProbeEnabled", new Boolean(true));

        return new WizardDescriptor.Panel[] {
                new CreatePanelDescriptor(), new ConfigPanelDescriptor()
            };
    }

    // And the list of step names:

    protected String[] createSteps() {
        return new String[] {
                NbBundle.getMessage(AgentBuilderWizardIterator.class,
                    "LBL_panel_1"),
                NbBundle.getMessage(AgentBuilderWizardIterator.class,
                    "LBL_panel_2")
            };
    }


    public Set instantiate(TemplateWizard wiz) throws IOException {

        // Here is the default plain behavior. Simply takes the selected
        // template (you need to have included the standard second panel
        // in createPanels(), or at least set the properties targetName and
        // targetFolder correctly), instantiates it in the provided
        // position, and returns the result.
        // More advanced wizards can create multiple objects from template
        // (return them all in the result of this method), populate file
        // contents on the fly, etc.
        String vendorName = (String) wiz.getProperty("Vendor");
        String appName = (String) wiz.getProperty("Application");
        String targetdir = (String) wiz.getProperty("Directory");
        Boolean scalable = (Boolean) wiz.getProperty("Scalable");
        Boolean netaware = (Boolean) wiz.getProperty("NetworkAware");
        Integer codeType = (Integer) wiz.getProperty("CodeType");
        String startcmd = (String) wiz.getProperty("StartCmd");
        String startTO = (String) wiz.getProperty("StartTO");
        String stopcmd = (String) wiz.getProperty("StopCmd");
        String stopTO = (String) wiz.getProperty("StopTO");
        String probecmd = (String) wiz.getProperty("ProbeCmd");
        String probeTO = (String) wiz.getProperty("ProbeTO");
        ConfigInfo config = (ConfigInfo) wiz.getProperty("Config");
        Boolean need_create = (Boolean) wiz.getProperty("NeedCreate");

        SCToolMgr mgr = new SCToolMgr(targetdir);

        if (need_create.booleanValue()) {
            mgr.create(vendorName, appName, netaware.booleanValue(),
                scalable.booleanValue(), codeType.intValue() == KshCodeType,
                codeType.intValue() == GDSCodeType);
        }

        mgr.config(startcmd, stopcmd, probecmd, startTO, stopTO, probeTO);

        // Make sure the created files appear in a mounted file system
        Repository rep = TopManager.getDefault().getRepository();
        Enumeration allfs = rep.getFileSystems();
        boolean alreadyMounted = false;

        while (allfs.hasMoreElements()) {
            Object o = allfs.nextElement();

            if (o instanceof LocalFileSystem) {
                LocalFileSystem lfs = (LocalFileSystem) o;

                if (lfs.getRootDirectory().getPath().startsWith(targetdir)) {
                    alreadyMounted = true;

                    break;
                }
            }
        }

        if (!alreadyMounted) {
            LocalFileSystem lfs = new LocalFileSystem();
            boolean it_worked = true;

            try {
                lfs.setRootDirectory(new File(targetdir));
            } catch (Throwable o) {

                // What do we do now??
                it_worked = false;
            }

            if (it_worked) {
                rep.addFileSystem(lfs);
            }
        }

        return null;
    }

    // You can keep a reference to the TemplateWizard which can
    // provide various kinds of useful information such as
    // the currently selected target name.
    // Also the panels will receive wiz as their "settings" object.
    public void initialize(TemplateWizard wiz) {
        this.wiz = wiz;
        index = 0;
        panels = createPanels();

        // Make sure list of steps is accurate.
        String steps[] = createSteps();

        for (int i = 0; i < panels.length; i++) {
            Component c = panels[i].getComponent();

            if (steps[i] == null) {

                // Default step name to component name of panel.
                // Mainly useful for getting the name of the target
                // chooser to appear in the list of steps.
                steps[i] = c.getName();
            }

            if (c instanceof JComponent) { // assume Swing components

                JComponent jc = (JComponent) c;

                // Step #.
                jc.putClientProperty("WizardPanel_contentSelectedIndex",
                    new Integer(i)); // NOI18N

                // Step name (actually the whole list for reference).
                jc.putClientProperty("WizardPanel_contentData", steps); // NOI18N
            }
        }
    }

    public void uninitialize(TemplateWizard wiz) {
        this.wiz = null;
        panels = null;
    }

    // --- WizardDescriptor.Iterator METHODS: ---
    // Note that this is very similar to WizardDescriptor.Iterator, but with a
    // few more options for customization. If you e.g. want to make panels
    // appear
    // or disappear dynamically, go ahead.

    public String name() {
        return NbBundle.getMessage(AgentBuilderWizardIterator.class,
                "TITLE_x_of_y", new Integer(index + 1),
                new Integer(panels.length));
    }

    public boolean hasNext() {
        return index < (panels.length - 1);
    }

    public boolean hasPrevious() {
        return index > 0;
    }

    public void nextPanel() {

        if (!hasNext())
            throw new NoSuchElementException();

        index++;
    }

    public void previousPanel() {

        if (!hasPrevious())
            throw new NoSuchElementException();

        index--;
    }

    public WizardDescriptor.Panel current() {
        return panels[index];
    }

    // If nothing unusual changes in the middle of the wizard, simply:
    public final void addChangeListener(ChangeListener l) {
    }

    public final void removeChangeListener(ChangeListener l) {
    }
}
