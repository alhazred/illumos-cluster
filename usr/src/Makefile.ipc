#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.ipc	1.4	08/05/20 SMI"
#
# Makefile.ipc: definitions for inter-process communication used between
# commands and rgmd, pmfd, and fed.
#
# Note: this Makefile should always be included after Makefile.master
# and after lib/Makefile.lib
#

#
# The following line distinguishes between the use of
# doors and rpc communication for libscha, libfe, and libpmf.
#
# To use doors, set RPC_IMPL= $(POUND_SIGN).
# Then the RPC code will not  be used and doors will be used. 
# This is required for supporting zones on S10.
#
# To use rpc code, set RPC_IMPL=
#
# Currently the rpc code is not used in Sun Cluster. Doors are used on all
# platforms. However, we expect the Europa project to use both rpc and doors
# code simultaneously, so we can't get rid of the rpc code. This mechanism
# of choosing the implementation in the Makefile is a temporary hack to
# allow us to ensure that the rpc build continues to work while we use
# the doors-based approach.
#
RPC_IMPL=	$(POUND_SIGN)

#
# In addition to the macro for Makefiles, the C and C++ code needs to know
# if it's using doors or rpc, so specify a command-line define also.
# The value of the define is taken from the value of RPC_IMPL above.
#
CFLAGS.door=-DDOOR_IMPL=1
$(RPC_IMPL)CFLAGS.door=-DDOOR_IMPL=0

CFLAGS += $(CFLAGS.door)
CFLAGS64 += $(CFLAGS.door)
CL_CCFLAGS += $(CFLAGS.door)

# Needed for some lint targets
CPPFLAGS += $(CFLAGS.door)

CCFLAGS += $(CFLAGS.door)

#
# If compiling for doors, we need to link with libdoor
#
DOOR_LDLIBS = -ldoor
$(RPC_IMPL)DOOR_LDLIBS =
